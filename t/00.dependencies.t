use Mojo::Base -strict;
use Test::More;

BEGIN {
    my @modules = qw{
        Array::Utils
        Clone
        Date::Simple
        JSON
        Mojolicious::Plugin::JSONConfig
        Mojolicious::Plugin::Number::Commify
        };

    for my $module ( @modules ) {
        use_ok( $module );
    }
}

done_testing();
