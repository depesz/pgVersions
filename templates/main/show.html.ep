% layout 'default';
% my $ver_from = param('from');
% my $ver_to = param('to');

<div id="diff">
    <div id="upIndex">
        <p>Jump to:</p>
        <ul>
% if (stash('security')) {
<li><a href="#security">Security fixes</a></li>
% }
% my $cc = stash('config_changes');
% if ($cc) {
<li>
    <a href="#config_changes">Configuration changes</a>
    <ul>
    % if ( $cc->{'removed'} ) {
<li><a href="#cc_removed">Removed</a></li>
    % }
    % if ( $cc->{'added'} ) {
<li><a href="#cc_added">Added</a></li>
    % }
    % if ( $cc->{'modified'} ) {
<li><a href="#cc_modified">Changed default value</a></li>
    % }
    </ul>
</li>
% }
% if ( ( $cc ) || ( stash('security') ) ) {
    <li><a href="#loc">List of changes</a></li>
% } else {
    <li><a href="#diffList">List of changes</a></li>
% }
    % for my $diff ( @{ stash('diffs') } ) {
    <li><a href="#p<%= $diff->{'version'} %>">... to <%= $diff->{'version'} %> from <%= $diff->{'released'} %></a></li>
    % }
        </ul>
    </div>

    <h1>Upgrading from <span id="vfrom"><%= $ver_from %></span> to <span id="vto"><%= $ver_to %></span> gives you 
% if ( stash('timeDiff') ) {
    <span id="timeDiff"><%= stash('timeDiff') %></span> worth of
    fixes (<%= stash('all_changes') %> of them)
% } else {
    <%= stash('all_changes') %> fixes
% }
% if ( ( !stash('config_changes')) && (!stash('security')) ) {
:
% }
    </h1>

% if (stash('security')) {
    <div id="security">
        <h2><a href="#top">⇑</a> Security fixes:</h2>
        <ul id="secDiff">
% for my $item ( @{ stash('security') } ) {
<li><%== $item %></li>
% }
        </ul>
    </div>
% }

% if ($cc) {
    <div id="config_changes">
        <h2><a href="#top">⇑</a> Configuration changes:</h2>
        % if ( $cc->{'removed'} ) {
        <h3><a href="#top">⇑</a> Removed config parameters:</h3>
        <table id="cc_removed">
            <thead>
                <tr>
                    <th>Config parameter:</th>
                    <th>Default value:</th>
                </tr>
            </thead>
            <tbody>
            % for my $item ( @{ $cc->{'removed'} } ) {
            % my $url = app->db->get_guc_doc_link( $ver_from, $item->[0] );
            <tr>
                <td class="guc-name">
                    % if ( $url ) {
                        <a href="<%= $url %>"><%= $item->[0] %></a>
                    % } else {
                        <%= $item->[0] %>
                    % }
                </td>
                <td class="guc-value">
                    <%= $item->[1] %>
                </td>
            </tr>
            % }
            </tbody>
        </table>
        % }
        % if ( $cc->{'added'} ) {
        <h3><a href="#top">⇑</a> Added config parameters:</h3>
        <table id="cc_added">
            <thead>
                <tr>
                    <th>Config parameter:</th>
                    <th>Default value:</th>
                </tr>
            </thead>
            <tbody>
            % for my $item ( @{ $cc->{'added'} } ) {
            % my $url = app->db->get_guc_doc_link( $ver_to, $item->[0] );
            <tr>
                <td class="guc-name">
                    % if ( $url ) {
                        <a href="<%= $url %>"><%= $item->[0] %></a>
                    % } else {
                        <%= $item->[0] %>
                    % }
                </td>
                <td class="guc-value">
                    <%= $item->[1] %>
                </td>
            </tr>
            % }
            </tbody>
        </table>
        % }
        % if ( $cc->{'modified'} ) {
        <h3><a href="#top">⇑</a> Config parameters with changed default value:</h3>
        <table id="cc_modified">
            <thead>
                <tr>
                    <th>Config parameter:</th>
                    <th>Default value in Pg <%= $ver_from %>:</th>
                    <th>Default value in Pg <%= $ver_to %>:</th>
                </tr>
            </thead>
            <tbody>
            % for my $item ( @{ $cc->{'modified'} } ) {
            % my $url = app->db->get_guc_doc_link( $ver_to, $item->[0] );
            <tr>
                <td class="guc-name">
                    % if ( $url ) {
                        <a href="<%= $url %>"><%= $item->[0] %></a>
                    % } else {
                        <%= $item->[0] %>
                    % }
                </td>
                <td class="guc-value">
                    <%= $item->[1] %>
                </td>
                <td class="guc-value">
                    <%= $item->[2] %>
                </td>
            </tr>
            % }
            </tbody>
        </table>
        % }
    </div>
% }

% if ( ( stash('security') ) || (stash('config_changes')) ) {
<h2 id="loc">List of changes:</h2>
% }

    <ul id="diffList">
% for my $diff ( @{ stash('diffs') } ) {
<li>
    <p class="v_header" id="p<%= $diff->{'version'} %>">
    <a href="#top">⇑</a>
    Upgrade to <%= $diff->{'version'} %> released on <%= $diff->{'released'} %>
    -
    <a href="https://www.postgresql.org/docs/<%= $diff->{'major'} %>/release-<%= $diff->{'dashed_version'} %>.html">docs</a>
    </p>
    <ul>
% for my $change ( @{$diff->{'changes'}} ) {
<li>
<%== $change %>
</li>
% }
    </ul>
</li>
% }
    </ul>
</div>
