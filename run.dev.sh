#!/usr/bin/env bash
# make sure that current dir is project top dir
this_script="${BASH_SOURCE[0]}"
script_directory="$( dirname "${this_script}" )"
work_dir="$( readlink -f "${script_directory}" )"
cd "$work_dir"
# make sure that current dir is project top dir

# make sure that log directory exists
[[ -e log ]] || mkdir log

# Settings
project_name="$( basename "${work_dir}" )"
main_script="pg_why_upgrade"
morbo_port=22475

# I use ssh-ident tool (https://github.com/ccontavalli/ssh-ident), so I should
# set some env variables.
ssh_ident_agent_env="${HOME}/.ssh/agents/agent-priv-$( hostname -s )"
[[ -e "${ssh_ident_agent_env}" ]] && . "${ssh_ident_agent_env}" > /dev/null

# Check if the session already exist, and if yes - attach, with no changes
tmux has-session -t "${project_name}" 2> /dev/null && exec tmux attach-session -t "${project_name}"

tmux new-session -d -s "${project_name}" -n "morbo"

tmux bind-key c new-window -c "#{pane_current_path}" -a

tmux new-window -t "${project_name}:2" -n shell

tmux split-window -d -t morbo
tmux select-pane -t "${project_name}:morbo.0"
tmux send-keys -t "${project_name}:morbo.0" "morbo -w lib -w templates -w experiments.development.json -v -l http://*:${morbo_port} script/${main_script}" Enter
tmux send-keys -t "${project_name}:morbo.1" "tail -F log/development.log" Enter

tmux new-window  -d   -n "lib"        -t "${project_name}:10"  -c "${work_dir}/lib/"
tmux new-window  -d   -n "templates"  -t "${project_name}:20"  -c "${work_dir}/templates/"
tmux new-window  -d   -n "css"        -t "${project_name}:30"  -c "${work_dir}/public/css"
tmux new-window  -d   -n "js"         -t "${project_name}:40"  -c "${work_dir}/public/js"

for w in lib templates css js
do
    tmux send-keys -t "${project_name}:${w}" "ls -l" Enter
done

tmux attach-session -t "${project_name}"
