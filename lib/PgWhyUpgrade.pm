package PgWhyUpgrade;
use Mojo::Base 'Mojolicious';
use Mojo::Home;

# This method will run once at server start
sub startup {
    my $self = shift;

    $self->plugin( 'JSONConfig' );

    my $home = Mojo::Home->new;

    $self->plugin( 'PgWhyUpgrade::Plugin::DB' => { 'db' => $home->child( 'db' ) } );
    $self->plugin( 'PgWhyUpgrade::Plugin::Git' );
    $self->plugin( 'Number::Commify' );

    # Router
    my $r = $self->routes;

    # Normal route to controller
    $r->get( '/' )->to( 'main#home' )->name( 'home' );
    $r->get( '/error' )->to( 'main#error' )->name( 'error' );
    $r->get( '/show' )->to( 'main#show' )->name( 'show' );
    $r->get( '/metainfo' )->to( 'main#metainfo' )->name( 'metainfo' );
}

1;
