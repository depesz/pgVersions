package Pg::Version;
use strict;

use Carp;
use English qw( -no_match_vars );

sub new {
    my $class = shift;
    my $self  = bless {}, $class;

    my $ver = shift;
    return unless defined $ver;
    return unless $ver =~ m{\A[1-9]\d*(?:\.\d+){0,2}\z};
    $self->{ 'as_string' } = $ver;
    $self->{ 'as_parts' }  = [ split /\./, $ver ];

    if ( $self->part( 0 ) < 10 ) {
        return unless defined $self->part( 1 );
    }

    return $self;
}

sub str {
    my $self = shift;
    return $self->{ 'as_string' };
}

sub part {
    my $self = shift;
    my $idx  = shift;
    return $self->{ 'as_parts' }->[ $idx ];
}

sub parts {
    my $self = shift;
    return scalar @{ $self->{ 'as_parts' } };
}

sub major {
    my $self = shift;
    return $self->part( 0 ) if $self->part( 0 ) >= 10;
    return join( '.', $self->part( 0 ), $self->part( 1 ) );
}

sub minor {
    my $self = shift;
    return ( $self->part( 1 ) // 0 ) if $self->part( 0 ) >= 10;
    return ( $self->part( 2 ) // 0 );
}

sub cmp {
    my $self  = shift;
    my $ver_b = shift;

    my $common_parts = $self->parts > $ver_b->parts ? $self->parts : $ver_b->parts;

    for ( my $i = 0 ; $i < $common_parts ; $i++ ) {
        return 1  if $self->part( $i ) > $ver_b->part( $i );
        return -1 if $self->part( $i ) < $ver_b->part( $i );
    }

    return 1  if $self->parts + 1 > $common_parts;
    return -1 if $ver_b->parts + 1 > $common_parts;
    return 0;
}

1;
