package PgWhyUpgrade::Plugin::Git;

use Mojo::Base 'Mojolicious::Plugin';

use Carp;
use English qw( -no_match_vars );

has git_dir => undef;
has head    => undef;

sub register {
    my ( $self, $app, $config ) = @_;

    $self->_find_git_dir( $app->home );
    $self->_precache();

    # register helper
    $app->helper( git => sub { return $self; } );

    return;
}

sub _find_git_dir {
    my $self = shift;
    my $dir  = shift;
    while ( '/' ne $dir->to_string ) {
        my $gitdir = $dir->child( '.git' );
        if (   ( -e $gitdir->to_string )
            && ( -d $gitdir->to_string ) )
        {
            $self->git_dir( $gitdir );
            return;
        }
        $dir = $dir->dirname;
    }
}

sub _precache {
    my $self = shift;
    return unless $self->git_dir;

    open my $fh, '<', $self->git_dir->child( 'HEAD' ) or return;
    my $line = <$fh>;
    close $fh;

    return unless $line =~ m{\Aref:\s+(refs/\S+)\s*\z};

    my $ref = $self->git_dir->child( $1 );

    open $fh, '<', $ref or return;
    $line = <$fh>;
    close $fh;

    return unless $line =~ m{\A([a-f0-9]{40})\s*\z};
    $self->head( $1 );
}

1;
