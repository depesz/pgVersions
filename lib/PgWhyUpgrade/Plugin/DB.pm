package PgWhyUpgrade::Plugin::DB;

use Mojo::Base 'Mojolicious::Plugin';
use Pg::Version;
use Date::Simple;
use Clone qw( clone );
use Array::Utils qw( intersect unique );

use JSON;

use Carp;
use English qw( -no_match_vars );

has db_dir       => undef;
has index_mtime  => -1;
has cache        => sub { {} };
has releases     => sub { {} };
has release_list => sub { [] };
has major_list   => sub { [] };
has major_index  => sub { {} };
has search_idx   => sub { {} };
has search_mtime => -1;
has app          => undef;

sub register {
    my ( $self, $app, $config ) = @_;
    $self->app( $app );

    # data source name
    $self->db_dir( $config->{ 'db' } );

    for my $sub_file ( qw( index.js search.idx ) ) {
        my $real_file = $self->db_dir->child( 'index.js' );
        unless ( -f $real_file->path ) {
            $app->log->fatal( "DB file doesn't exist: " . $real_file->path );
            exit 1;
        }
        unless ( -r $real_file->path ) {
            $app->log->fatal( "DB file can't be read: " . $real_file->path );
            exit 2;
        }
    }

    # register helper
    $app->helper( db => sub { $self->reload_index(); $self->reload_search(); return $self; } );

    return;
}

sub get_metainfo {
    my $self = shift;
    my $info = {};
    $info->{ 'oldest_release' } = $self->releases->{ $self->release_list->[ -1 ] };
    $info->{ 'newest_release' } = $self->releases->{ $self->release_list->[ 0 ] };
    $info->{ 'release_count' }  = scalar @{ $self->release_list };
    $info->{ 'total_changes' }  = 0;

    my $majors = [];
    for my $release ( map { $self->releases->{ $_ } } @{ $self->release_list } ) {
        my $major = $release->{ 'major' };
        if (   ( 0 == scalar @{ $majors } )
            || ( $majors->[ -1 ]->{ 'major' } ne $major ) )
        {
            push @{ $majors }, { 'major' => $major, 'releases' => 0, 'first' => undef, 'last' => $release, 'changes' => 0 };
        }
        $majors->[ -1 ]->{ 'releases' }++;
        $majors->[ -1 ]->{ 'first' } = $release;
        $majors->[ -1 ]->{ 'changes' } += $release->{ 'changes' };
        $info->{ 'total_changes' } += $release->{ 'changes' };
    }
    $info->{ 'majors' }      = $majors;
    $info->{ 'major_count' } = scalar @{ $majors };
    return $info;
}

sub reload_search {
    my $self = shift;

    my @stat  = stat( $self->db_dir->child( 'search.idx' ) );
    my $mtime = $stat[ 9 ];

    return if $mtime == $self->search_mtime;

    $self->search_mtime( $mtime );

    my $words = {};

    my $index = JSON->new()->utf8()->decode( $self->db_dir->child( 'search.idx' )->slurp() );

    while ( my ( $word, $data ) = each %{ $index } ) {
        my @all_diffs = ();
        while ( my ( $ver, $diffs ) = each %{ $data } ) {
            push @all_diffs, map { $ver . ':' . $_ } @{ $diffs };
        }
        $words->{ $word } = \@all_diffs;
    }

    $self->search_idx( $words );

    return;
}

sub reload_index {
    my $self = shift;

    my @stat  = stat( $self->db_dir->child( 'index.js' ) );
    my $mtime = $stat[ 9 ];

    return if $mtime == $self->index_mtime;

    $self->index_mtime( $mtime );
    $self->cache( {} );

    my $index_json = JSON->new()->utf8();
    my $index_data = $index_json->decode( $self->db_dir->child( 'index.js' )->slurp() );

    while ( my ( $key, $value ) = each %{ $index_data } ) {
        my $ver_object = Pg::Version->new( $key );
        return unless $ver_object;
        next   unless $value->[ 0 ] =~ m{\A(never released|\d\d\d\d-\d\d-\d\d)\z};
        $self->releases->{ $key } = {
            'ver'     => $ver_object,
            'release' => $value->[ 0 ],
            'changes' => $value->[ 1 ],
            'major'   => $ver_object->major,
        };
    }

    $self->release_list(
        [
            map      { $_->str }
                sort { $b->cmp( $a ) }
                map  { $self->releases->{ $_ }->{ 'ver' } }
                keys %{ $self->releases }
        ]
    );

    for my $i ( 0 .. $#{ $self->release_list } ) {
        my $v     = $self->release_list->[ $i ];
        my $major = $self->releases->{ $v }->{ 'major' };
        $self->releases->{ $v }->{ 'index' } = $i;
        if (   ( 0 == scalar @{ $self->major_list } )
            || ( $major ne $self->{ 'major_list' }->[ -1 ] ) )
        {
            push @{ $self->{ 'major_list' } }, $major;
            $self->major_index->{ $major } = $#{ $self->{ 'major_list' } };
        }
    }

    return;
}

sub valid_release {
    my $self = shift;
    my $rel  = shift;
    return 1 if $self->releases->{ $rel };
    return;
}

sub get_time_diff {
    my $self = shift;
    my ( $from, $to ) = @_;
    my $from_date = Date::Simple->new( $self->releases->{ $from }->{ 'release' } );
    my $to_date   = Date::Simple->new( $self->releases->{ $to }->{ 'release' } );
    return $to_date - $from_date;
}

sub get_next_version {
    my $self = shift;
    my ( $from, $to ) = @_;

    return if $from eq $to;
    my $to_major = $self->releases->{ $to }->{ 'major' };

    my $current_major = $self->releases->{ $from }->{ 'major' };

    my $from_index  = $self->releases->{ $from }->{ 'index' };
    my $next_direct = $self->release_list->[ $from_index - 1 ];
    if ( $to_major eq $current_major ) {
        return $next_direct;
    }

    my $major_index = $self->major_index->{ $current_major };
    my $next_major  = $self->major_list->[ $major_index - 1 ];

    my $direct_date = $self->releases->{ $next_direct }->{ 'release' };
    my $major_date  = $self->releases->{ $next_major }->{ 'release' };

    if ( $direct_date lt $major_date ) {
        return $next_direct;
    }
    return $next_major;
}

sub get_changes_for_single_upgrade {
    my $self = shift;
    my $ver  = shift;

    if ( !$self->cache->{ $ver } ) {
        my $db_file = $self->db_dir->child( $ver . '.js' );
        my $json    = JSON->new()->utf8();
        $self->cache->{ $ver } = [];
        for my $item ( @{ $json->decode( $db_file->slurp() ) } ) {
            my $full_item = join( '', @{ $item } );
            $full_item =~ s{(CVE-\d{4}-\d+)}{<a href="https://nvd.nist.gov/vuln/detail/$1">$1</a>}g;
            $full_item =~ s{>\r?\n\s*}{>\n}g;
            push @{ $self->cache->{ $ver } }, $full_item;
        }
    }

    return $self->cache->{ $ver };
}

sub get_shortened {
    my $self   = shift;
    my $change = lc shift;
    $change =~ s/<[^>]*>//g;
    $change =~ s/\s+//g;
    $change =~ s/("|“|”)//g;
    return $change;
}

sub get_excludes_from_changelog {
    my $self = shift;
    my ( $from, $to ) = @_;
    my $from_data  = $self->releases->{ $from };
    my $to_data    = $self->releases->{ $to };
    my $from_major = $from_data->{ 'major' };
    my $to_major   = $to_data->{ 'major' };
    return if $from_major eq $to_major;
    return if $from_data->{ 'release' } lt $to_data->{ 'release' };

    my %excludes = ();

    my $base_index = $from_data->{ 'index' };

    while ( $base_index < scalar @{ $self->release_list } ) {
        my $current_v = $self->release_list->[ $base_index ];
        my $current_d = $self->releases->{ $current_v };
        last if $current_d->{ 'release' } lt $to_data->{ 'release' };

        for my $change ( @{ $self->get_changes_for_single_upgrade( $current_v ) } ) {
            $excludes{ $self->get_shortened( $change ) } = 1;
        }

        $base_index++;
    }
    return \%excludes;
}

sub normalize_keywords {
    my $self     = shift;
    my $keywords = lc shift;

    my @extracted_words = $keywords =~ m{([[:word:]]{2,}\*?)}g;
    my @words           = ();

    for my $word ( @extracted_words ) {
        if ( $word =~ s/\*\z// ) {
            my @all_potential = grep { /\A$word/ } keys %{ $self->search_idx };
            push @words, \@all_potential;
        }
        else {
            push @words, $word;
        }
    }

    return @words;
}

sub search_for_single {
    my $self    = shift;
    my $keyword = shift;

    return clone( $self->search_idx->{ $keyword } ) if '' eq ref $keyword;

    return [ unique( map { @{ $self->search_idx->{ $_ } } } @{ $keyword } ) ];
}

sub search_for {
    my $self     = shift;
    my @keywords = @_;

    my $idx = $self->search_idx;

    my $first    = shift @keywords;
    my $matching = $self->search_for_single( $first );
    return unless $matching;

    for my $keyword ( @keywords ) {
        my $new = $self->search_for_single( $keyword );

        $matching = [ intersect( @{ $matching }, @{ $new } ) ];

        return if 0 == scalar @{ $matching };
    }

    my $real_matching = {};
    for my $item ( @{ $matching } ) {
        my ( $ver, $diff ) = split( /:/, $item );
        $real_matching->{ $ver } = [] unless $real_matching->{ $ver };
        push @{ $real_matching->{ $ver } }, $diff;
    }
    return $real_matching;
}

sub get_all_diffs {
    my $self = shift;
    my ( $from, $to, $keywords ) = @_;
    my $search_results;
    my @normalized_keywords;
    my $keywords_re;

    if ( $keywords ) {
        @normalized_keywords = $self->normalize_keywords( $keywords );
        $keywords            = '' if 0 == scalar @normalized_keywords;
        $search_results      = $self->search_for( @normalized_keywords );
        $keywords_re         = join( '|', map { '' eq ref $_ ? $_ : join( '|', @{ $_ } ) } @normalized_keywords );
    }

    my $ret = { 'diffs' => [], 'security' => [] };

    my $excludes = {};

    # Preload excludes from all minor releases in $from-major
    my $v_from = Pg::Version->new( $from );
    for my $exclude_from ( $v_from->major, map { $v_from->major . "." . $_ } 1 .. $v_from->minor ) {
        for my $change ( @{ clone $self->get_changes_for_single_upgrade( $exclude_from ) } ) {
            $excludes->{ $self->get_shortened( $change ) }++;
        }
    }

    my $all_changes = 0;

    my $current = $from;
    while ( my $new_version = $self->get_next_version( $current, $to ) ) {

        $current = $new_version;

        my $dashed_version = $new_version;
        $dashed_version =~ s/\./-/g;

        my $changes = clone $self->get_changes_for_single_upgrade( $new_version );

        if ( $keywords ) {
            next unless $search_results->{ $new_version };
            my @selected_changes = @{ $changes }[ @{ $search_results->{ $new_version } } ];
            for my $item ( @selected_changes ) {
                my @parts = split m{(<(?:[^>]*|"[^"]*"|'[^']*')+>)}, $item;
                for my $element ( @parts ) {
                    next if $element =~ m{\A<};
                    $element =~ s{\b($keywords_re)\b}{<span class="searchhilight">$1</span>}gi;
                }
                $item = join '', @parts;
            }
            $changes = \@selected_changes;
        }

        $changes = [
            map  { $_->[ 1 ] }
            grep { !$excludes->{ $_->[ 0 ] }++ }
            map  { [ $self->get_shortened( $_ ), $_ ] } @{ $changes }
        ];

        push @{ $ret->{ 'security' } }, grep { m{CVE-\d{4}-\d+} } @{ $changes };

        push @{ $ret->{ 'diffs' } },
            {
            'version'        => $new_version,
            'major'          => $self->releases->{ $new_version }->{ 'major' },
            'released'       => $self->releases->{ $new_version }->{ 'release' },
            'dashed_version' => $dashed_version,
            'changes'        => $changes,
            };

        $all_changes += scalar @{ $changes };

    }

    delete $ret->{ 'security' } if 0 == scalar @{ $ret->{ 'security' } };

    $ret->{ 'all_changes' } = $all_changes;

    if ( $keywords ) {
        $ret->{ 'keywords' }            = $keywords;
        $ret->{ 'normalized_keywords' } = \@normalized_keywords;
    }

    my $config_changes = $self->get_config_changes( $from, $to );
    $ret->{ 'config_changes' } = $config_changes if $config_changes;

    return $ret;
}

sub get_guc_doc_link {
    my $self = shift;
    my ( $version, $guc ) = @_;
    $guc = lc $guc;
    my $major = Pg::Version->new( $version )->major;
    $self->load_guc_doc_links( $major );
    my $C = $self->cache->{ ':guc-docs' }->{ $major };
    return unless $C->{ 'gucs' }->{ $guc };
    my ( $page_no, $fragment ) = @{ $C->{ 'gucs' }->{ $guc } };
    my $page_name = $C->{ 'pages' }->[ $page_no ];
    return sprintf( 'https://www.postgresql.org/docs/%s/%s#%s', $major, $page_name, $fragment );
}

sub load_guc_doc_links {
    my $self    = shift;
    my $version = shift;
    return if $self->cache->{ ':guc-docs' }->{ $version };
    my $content = $self->db_dir->child( 'guc-docs' )->child( $version . '.js' )->slurp();
    return unless $content;
    $self->cache->{ ':guc-docs' }->{ $version } = JSON->new()->utf8()->decode( $content );
    return;
}

sub get_config_changes {
    my $self = shift;
    my ( $from, $to ) = @_;
    my $cache_key = join ':', $from, $to;

    if ( !$self->cache->{ ':config' }->{ $cache_key } ) {

        my $config_from = $self->get_config_for( $from );
        return unless $config_from;
        my $config_to = $self->get_config_for( $to );
        return unless $config_to;

        my $diff    = {};
        my @removed = map { [ $_, $config_from->{ $_ } ] }
            sort
            grep { !defined $config_to->{ $_ } }
            keys %{ $config_from };
        my @added = map { [ $_, $config_to->{ $_ } ] }
            sort
            grep { !defined $config_from->{ $_ } }
            keys %{ $config_to };
        my @modified = map { [ $_, $config_from->{ $_ }, $config_to->{ $_ } ] }
            sort
            grep { $config_to->{ $_ } ne $config_from->{ $_ } }
            grep { defined $config_from->{ $_ } }
            keys %{ $config_to };

        $diff->{ 'removed' }  = \@removed  if 0 < scalar @removed;
        $diff->{ 'added' }    = \@added    if 0 < scalar @added;
        $diff->{ 'modified' } = \@modified if 0 < scalar @modified;
        return if 0 == scalar keys %{ $diff };
        $self->cache->{ ':config' }->{ $cache_key } = $diff;
    }
    return $self->cache->{ ':config' }->{ $cache_key };
}

sub get_config_for {
    my $self = shift;
    my $ver  = shift;
    if ( !$self->cache->{ ':config' }->{ $ver } ) {
        my $content = $self->db_dir->child( 'config' )->child( $ver . '.js' )->slurp();
        return unless $content;
        my $data = JSON->new()->utf8()->decode( $content );
        return unless $data;
        $self->cache->{ ':config' }->{ $ver } = $data;
    }
    return $self->cache->{ ':config' }->{ $ver };
}

1;
