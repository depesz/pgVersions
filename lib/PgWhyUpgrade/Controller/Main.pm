package PgWhyUpgrade::Controller::Main;
use Mojo::Base 'Mojolicious::Controller';

use Pg::Version;

sub home {
    my $self = shift;
    my $url  = $self->url_for( 'show' );
    $url->query(
        from => $self->db->release_list->[ 1 ],
        to   => $self->db->release_list->[ 0 ]
    );
    return $self->redirect_to( $url );
}

sub show_validate {
    my $self = shift;

    my $from     = $self->param( 'from' )     // '';
    my $to       = $self->param( 'to' )       // '';
    my $keywords = $self->param( 'keywords' ) // '';

    if ( $keywords ) {
        if ( ( !$from ) || ( !$to ) ) {
            my $url = $self->url_for( 'show' );
            $url->query(
                from     => $from || $self->db->release_list->[ -1 ],
                to       => $to   || $self->db->release_list->[ 0 ],
                keywords => $keywords
            );
            return $self->redirect_to( $url );
        }
    }

    # Validate that the versions given as from/to are ok
    my @bad_versions = ();
    unless ( ( $from =~ m{\A\d+(\.\d+){0,2}\z} )
        && ( $self->db->valid_release( $from ) ) )
    {
        push @bad_versions, $from;
    }
    unless ( ( $to =~ m{\A\d+(\.\d+){0,2}\z} )
        && ( $self->db->valid_release( $to ) ) )
    {
        push @bad_versions, $to;
    }
    if ( 0 < scalar @bad_versions ) {
        $self->flash( 'msg'  => 'bad_versions' );
        $self->flash( 'vers' => \@bad_versions );
        return $self->redirect_to( 'error' );
    }

    if ( $from eq $to ) {
        $self->flash( 'msg' => 'same_version' );
        $self->flash( 'ver' => $from );
        return $self->redirect_to( 'error' );
    }

    # Check if we don't need to swap from/to
    my $o_from = Pg::Version->new( $from );
    my $o_to   = Pg::Version->new( $to );
    if ( $o_from->cmp( $o_to ) > 0 ) {
        my $url = $self->url_for( 'show' );
        $url->query(
            from => $to,
            to   => $from,
        );
        return $self->redirect_to( $url );
    }

    return;
}

sub metainfo {
    my $self = shift;
    $self->stash->{ 'metainfo' }   = $self->db->get_metainfo();
    $self->stash->{ 'git_commit' } = $self->git->head();
}

sub show {
    my $self = shift;
    my $val  = $self->show_validate();
    return $val if defined $val;

    my $from      = $self->param( 'from' );
    my $to        = $self->param( 'to' );
    my $keywords  = $self->param( 'keywords' ) // '';
    my $time_diff = $self->db->get_time_diff( $from, $to );

    if ( $time_diff <= 0 ) {
        $self->stash->{ 'timeDiff' } = undef;
    }
    elsif ( $time_diff < 40 ) {
        $self->stash->{ 'timeDiff' } = sprintf '%d days', $time_diff;
    }
    elsif ( $time_diff < 400 ) {
        $self->stash->{ 'timeDiff' } = sprintf '%.1f months', $time_diff / 30;
    }
    else {
        $self->stash->{ 'timeDiff' } = sprintf '%.1f years', $time_diff / 365;
    }

    my $data = $self->db->get_all_diffs( $from, $to, $keywords );

    $self->stash->{ 'all_changes' }         = $data->{ 'all_changes' };
    $self->stash->{ 'diffs' }               = $data->{ 'diffs' };
    $self->stash->{ 'security' }            = $data->{ 'security' }            if $data->{ 'security' };
    $self->stash->{ 'keywords' }            = $data->{ 'keywords' }            if $data->{ 'keywords' };
    $self->stash->{ 'normalized_keywords' } = $data->{ 'normalized_keywords' } if $data->{ 'normalized_keywords' };
    $self->stash->{ 'config_changes' }      = $data->{ 'config_changes' }      if $data->{ 'config_changes' };

    return;
}

1;
