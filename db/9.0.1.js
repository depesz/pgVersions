[
  [
    "<p>Use a separate interpreter for each calling SQL userid\n          in PL/Perl and PL/Tcl (Tom Lane)</p>",
    "<p>This change prevents security problems that can be\n          caused by subverting Perl or Tcl code that will be\n          executed later in the same session under another SQL user\n          identity (for example, within a <code class=\"literal\">SECURITY DEFINER</code> function). Most\n          scripting languages offer numerous ways that that might\n          be done, such as redefining standard functions or\n          operators called by the target function. Without this\n          change, any SQL user with Perl or Tcl language usage\n          rights can do essentially anything with the SQL\n          privileges of the target function's owner.</p>",
    "<p>The cost of this change is that intentional\n          communication among Perl and Tcl functions becomes more\n          difficult. To provide an escape hatch, PL/PerlU and\n          PL/TclU functions continue to use only one interpreter\n          per session. This is not considered a security issue\n          since all such functions execute at the trust level of a\n          database superuser already.</p>",
    "<p>It is likely that third-party procedural languages\n          that claim to offer trusted execution have similar\n          security issues. We advise contacting the authors of any\n          PL you are depending on for security-critical\n          purposes.</p>",
    "<p>Our thanks to Tim Bunce for pointing out this issue\n          (CVE-2010-3433).</p>"
  ],
  [
    "<p>Improve <code class=\"function\">pg_get_expr()</code>\n          security fix so that the function can still be used on\n          the output of a sub-select (Tom Lane)</p>"
  ],
  [
    "<p>Fix incorrect placement of placeholder evaluation (Tom\n          Lane)</p>",
    "<p>This bug could result in query outputs being non-null\n          when they should be null, in cases where the inner side\n          of an outer join is a sub-select with non-strict\n          expressions in its output list.</p>"
  ],
  [
    "<p>Fix join removal's handling of placeholder expressions\n          (Tom Lane)</p>"
  ],
  [
    "<p>Fix possible duplicate scans of <code class=\"literal\">UNION ALL</code> member relations (Tom\n          Lane)</p>"
  ],
  [
    "<p>Prevent infinite loop in ProcessIncomingNotify() after\n          unlistening (Jeff Davis)</p>"
  ],
  [
    "<p>Prevent show_session_authorization() from crashing\n          within autovacuum processes (Tom Lane)</p>"
  ],
  [
    "<p>Re-allow input of Julian dates prior to 0001-01-01 AD\n          (Tom Lane)</p>",
    "<p>Input such as <code class=\"literal\">'J100000'::date</code> worked before 8.4, but\n          was unintentionally broken by added error-checking.</p>"
  ],
  [
    "<p>Make psql recognize <code class=\"command\">DISCARD\n          ALL</code> as a command that should not be encased in a\n          transaction block in autocommit-off mode (Itagaki\n          Takahiro)</p>"
  ],
  [
    "<p>Update build infrastructure and documentation to\n          reflect the source code repository's move from CVS to Git\n          (Magnus Hagander and others)</p>"
  ]
]