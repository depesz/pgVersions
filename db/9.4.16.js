[
  [
    "<p>Ensure that all temporary files made by <span class=\"APPLICATION\">pg_upgrade</span> are non-world-readable (Tom Lane, Noah Misch)</p>",
    "<p><span class=\"APPLICATION\">pg_upgrade</span> normally restricts its temporary files to be readable and writable only by the calling user. But the temporary file containing <tt class=\"LITERAL\">pg_dumpall -g</tt> output would be group- or world-readable, or even writable, if the user's <tt class=\"LITERAL\">umask</tt> setting allows. In typical usage on multi-user machines, the <tt class=\"LITERAL\">umask</tt> and/or the working directory's permissions would be tight enough to prevent problems; but there may be people using <span class=\"APPLICATION\">pg_upgrade</span> in scenarios where this oversight would permit disclosure of database passwords to unfriendly eyes. (CVE-2018-1053)</p>"
  ],
  [
    "<p>Fix vacuuming of tuples that were updated while key-share locked (Andres Freund, Álvaro Herrera)</p>",
    "<p>In some cases <tt class=\"COMMAND\">VACUUM</tt> would fail to remove such tuples even though they are now dead, leading to assorted data corruption scenarios.</p>"
  ],
  [
    "<p>Fix inadequate buffer locking in some LSN fetches (Jacob Champion, Asim Praveen, Ashwin Agrawal)</p>",
    "<p>These errors could result in misbehavior under concurrent load. The potential consequences have not been characterized fully.</p>"
  ],
  [
    "<p>Avoid unnecessary failure in a query on an inheritance tree that occurs concurrently with some child table being removed from the tree by <tt class=\"COMMAND\">ALTER TABLE NO INHERIT</tt> (Tom Lane)</p>"
  ],
  [
    "<p>Fix spurious deadlock failures when multiple sessions are running <tt class=\"COMMAND\">CREATE INDEX CONCURRENTLY</tt> (Jeff Janes)</p>"
  ],
  [
    "<p>Repair failure with correlated sub-<tt class=\"LITERAL\">SELECT</tt> inside <tt class=\"LITERAL\">VALUES</tt> inside a <tt class=\"LITERAL\">LATERAL</tt> subquery (Tom Lane)</p>"
  ],
  [
    "<p>Fix <span class=\"QUOTE\">\"could not devise a query plan for the given query\"</span> planner failure for some cases involving nested <tt class=\"LITERAL\">UNION ALL</tt> inside a lateral subquery (Tom Lane)</p>"
  ],
  [
    "<p>Fix logical decoding to correctly clean up disk files for crashed transactions (Atsushi Torikoshi)</p>",
    "<p>Logical decoding may spill WAL records to disk for transactions generating many WAL records. Normally these files are cleaned up after the transaction's commit or abort record arrives; but if no such record is ever seen, the removal code misbehaved.</p>"
  ],
  [
    "<p>Fix walsender timeout failure and failure to respond to interrupts when processing a large transaction (Petr Jelinek)</p>"
  ],
  [
    "<p>Fix <code class=\"FUNCTION\">has_sequence_privilege()</code> to support <tt class=\"LITERAL\">WITH GRANT OPTION</tt> tests, as other privilege-testing functions do (Joe Conway)</p>"
  ],
  [
    "<p>In databases using UTF8 encoding, ignore any XML declaration that asserts a different encoding (Pavel Stehule, Noah Misch)</p>",
    "<p>We always store XML strings in the database encoding, so allowing libxml to act on a declaration of another encoding gave wrong results. In encodings other than UTF8, we don't promise to support non-ASCII XML data anyway, so retain the previous behavior for bug compatibility. This change affects only <code class=\"FUNCTION\">xpath()</code> and related functions; other XML code paths already acted this way.</p>"
  ],
  [
    "<p>Provide for forward compatibility with future minor protocol versions (Robert Haas, Badrul Chowdhury)</p>",
    "<p>Up to now, <span class=\"PRODUCTNAME\">PostgreSQL</span> servers simply rejected requests to use protocol versions newer than 3.0, so that there was no functional difference between the major and minor parts of the protocol version number. Allow clients to request versions 3.x without failing, sending back a message showing that the server only understands 3.0. This makes no difference at the moment, but back-patching this change should allow speedier introduction of future minor protocol upgrades.</p>"
  ],
  [
    "<p>Cope with failure to start a parallel worker process (Amit Kapila, Robert Haas)</p>",
    "<p>Parallel query previously tended to hang indefinitely if a worker could not be started, as the result of <tt class=\"LITERAL\">fork()</tt> failure or other low-probability problems.</p>"
  ],
  [
    "<p>Prevent stack-overflow crashes when planning extremely deeply nested set operations (<tt class=\"LITERAL\">UNION</tt>/<tt class=\"LITERAL\">INTERSECT</tt>/<tt class=\"LITERAL\">EXCEPT</tt>) (Tom Lane)</p>"
  ],
  [
    "<p>Fix null-pointer crashes for some types of LDAP URLs appearing in <tt class=\"FILENAME\">pg_hba.conf</tt> (Thomas Munro)</p>"
  ],
  [
    "<p>Fix sample <code class=\"FUNCTION\">INSTR()</code> functions in the PL/pgSQL documentation (Yugo Nagata, Tom Lane)</p>",
    "<p>These functions are stated to be <span class=\"TRADEMARK\">Oracle</span>® compatible, but they weren't exactly. In particular, there was a discrepancy in the interpretation of a negative third parameter: Oracle thinks that a negative value indicates the last place where the target substring can begin, whereas our functions took it as the last place where the target can end. Also, Oracle throws an error for a zero or negative fourth parameter, whereas our functions returned zero.</p>",
    "<p>The sample code has been adjusted to match Oracle's behavior more precisely. Users who have copied this code into their applications may wish to update their copies.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_dump</span> to make ACL (permissions), comment, and security label entries reliably identifiable in archive output formats (Tom Lane)</p>",
    "<p>The <span class=\"QUOTE\">\"tag\"</span> portion of an ACL archive entry was usually just the name of the associated object. Make it start with the object type instead, bringing ACLs into line with the convention already used for comment and security label archive entries. Also, fix the comment and security label entries for the whole database, if present, to make their tags start with <tt class=\"LITERAL\">DATABASE</tt> so that they also follow this convention. This prevents false matches in code that tries to identify large-object-related entries by seeing if the tag starts with <tt class=\"LITERAL\">LARGE OBJECT</tt>. That could have resulted in misclassifying entries as data rather than schema, with undesirable results in a schema-only or data-only dump.</p>",
    "<p>Note that this change has user-visible results in the output of <tt class=\"COMMAND\">pg_restore --list</tt>.</p>"
  ],
  [
    "<p>In <span class=\"APPLICATION\">ecpg</span>, detect indicator arrays that do not have the correct length and report an error (David Rader)</p>"
  ],
  [
    "<p>Avoid triggering a libc assertion in <tt class=\"FILENAME\">contrib/hstore</tt>, due to use of <code class=\"FUNCTION\">memcpy()</code> with equal source and destination pointers (Tomas Vondra)</p>"
  ],
  [
    "<p>Provide modern examples of how to auto-start Postgres on macOS (Tom Lane)</p>",
    "<p>The scripts in <tt class=\"FILENAME\">contrib/start-scripts/osx</tt> use infrastructure that's been deprecated for over a decade, and which no longer works at all in macOS releases of the last couple of years. Add a new subdirectory <tt class=\"FILENAME\">contrib/start-scripts/macos</tt> containing scripts that use the newer <span class=\"APPLICATION\">launchd</span> infrastructure.</p>"
  ],
  [
    "<p>Fix incorrect selection of configuration-specific libraries for OpenSSL on Windows (Andrew Dunstan)</p>"
  ],
  [
    "<p>Support linking to MinGW-built versions of libperl (Noah Misch)</p>",
    "<p>This allows building PL/Perl with some common Perl distributions for Windows.</p>"
  ],
  [
    "<p>Fix MSVC build to test whether 32-bit libperl needs <tt class=\"LITERAL\">-D_USE_32BIT_TIME_T</tt> (Noah Misch)</p>",
    "<p>Available Perl distributions are inconsistent about what they expect, and lack any reliable means of reporting it, so resort to a build-time test on what the library being used actually does.</p>"
  ],
  [
    "<p>On Windows, install the crash dump handler earlier in postmaster startup (Takayuki Tsunakawa)</p>",
    "<p>This may allow collection of a core dump for some early-startup failures that did not produce a dump before.</p>"
  ],
  [
    "<p>On Windows, avoid encoding-conversion-related crashes when emitting messages very early in postmaster startup (Takayuki Tsunakawa)</p>"
  ],
  [
    "<p>Use our existing Motorola 68K spinlock code on OpenBSD as well as NetBSD (David Carlier)</p>"
  ],
  [
    "<p>Add support for spinlocks on Motorola 88K (David Carlier)</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"APPLICATION\">tzdata</span> release 2018c for DST law changes in Brazil, Sao Tome and Principe, plus historical corrections for Bolivia, Japan, and South Sudan. The <tt class=\"LITERAL\">US/Pacific-New</tt> zone has been removed (it was only an alias for <tt class=\"LITERAL\">America/Los_Angeles</tt> anyway).</p>"
  ]
]