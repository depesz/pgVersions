[
  [
    "<p>Fix persistence marking of shared buffers during WAL\n          replay (Jeff Davis)</p>",
    "<p>This mistake can result in buffers not being written\n          out during checkpoints, resulting in data corruption if\n          the server later crashes without ever having written\n          those buffers. Corruption can occur on any server\n          following crash recovery, but it is significantly more\n          likely to occur on standby slave servers since those\n          perform much more WAL replay. There is a low probability\n          of corruption of btree and GIN indexes. There is a much\n          higher probability of corruption of table <span class=\"quote\">&#x201C;<span class=\"quote\">visibility\n          maps</span>&#x201D;</span>. Fortunately, visibility maps are\n          non-critical data in 9.1, so the worst consequence of\n          such corruption in 9.1 installations is transient\n          inefficiency of vacuuming. Table data proper cannot be\n          corrupted by this bug.</p>",
    "<p>While no index corruption due to this bug is known to\n          have occurred in the field, as a precautionary measure it\n          is recommended that production installations <code class=\"command\">REINDEX</code> all btree and GIN indexes at a\n          convenient time after upgrading to 9.1.6.</p>",
    "<p>Also, if you intend to do an in-place upgrade to\n          9.2.X, before doing so it is recommended to perform a\n          <code class=\"command\">VACUUM</code> of all tables while\n          having <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-client.html#GUC-VACUUM-FREEZE-TABLE-AGE\"><code class=\"varname\">\n          vacuum_freeze_table_age</code></a> set to zero. This will\n          ensure that any lingering wrong data in the visibility\n          maps is corrected before 9.2.X can depend on it.\n          <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-resource.html#GUC-VACUUM-COST-DELAY\"><code class=\"varname\">\n          vacuum_cost_delay</code></a> can be adjusted to reduce\n          the performance impact of vacuuming, while causing it to\n          take longer to finish.</p>"
  ],
  [
    "<p>Fix planner's assignment of executor parameters, and\n          fix executor's rescan logic for CTE plan nodes (Tom\n          Lane)</p>",
    "<p>These errors could result in wrong answers from\n          queries that scan the same <code class=\"literal\">WITH</code> subquery multiple times.</p>"
  ],
  [
    "<p>Fix misbehavior when <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-client.html#GUC-DEFAULT-TRANSACTION-ISOLATION\">\n          <code class=\"varname\">default_transaction_isolation</code></a> is set\n          to <code class=\"literal\">serializable</code> (Kevin\n          Grittner, Tom Lane, Heikki Linnakangas)</p>",
    "<p>Symptoms include crashes at process start on Windows,\n          and crashes in hot standby operation.</p>"
  ],
  [
    "<p>Improve selectivity estimation for text search queries\n          involving prefixes, i.e. <em class=\"replaceable\"><code>word</code></em><code class=\"literal\">:*</code> patterns (Tom Lane)</p>"
  ],
  [
    "<p>Improve page-splitting decisions in GiST indexes\n          (Alexander Korotkov, Robert Haas, Tom Lane)</p>",
    "<p>Multi-column GiST indexes might suffer unexpected\n          bloat due to this error.</p>"
  ],
  [
    "<p>Fix cascading privilege revoke to stop if privileges\n          are still held (Tom Lane)</p>",
    "<p>If we revoke a grant option from some role <em class=\"replaceable\"><code>X</code></em>, but <em class=\"replaceable\"><code>X</code></em> still holds that option\n          via a grant from someone else, we should not recursively\n          revoke the corresponding privilege from role(s)\n          <em class=\"replaceable\"><code>Y</code></em> that\n          <em class=\"replaceable\"><code>X</code></em> had granted\n          it to.</p>"
  ],
  [
    "<p>Disallow extensions from containing the schema they\n          are assigned to (Thom Brown)</p>",
    "<p>This situation creates circular dependencies that\n          confuse <span class=\"application\">pg_dump</span> and\n          probably other things. It's confusing for humans too, so\n          disallow it.</p>"
  ],
  [
    "<p>Improve error messages for Hot Standby\n          misconfiguration errors (Gurjeet Singh)</p>"
  ],
  [
    "<p>Make <span class=\"application\">configure</span> probe\n          for <code class=\"function\">mbstowcs_l</code> (Tom\n          Lane)</p>",
    "<p>This fixes build failures on some versions of AIX.</p>"
  ],
  [
    "<p>Fix handling of <code class=\"literal\">SIGFPE</code>\n          when PL/Perl is in use (Andres Freund)</p>",
    "<p>Perl resets the process's <code class=\"literal\">SIGFPE</code> handler to <code class=\"literal\">SIG_IGN</code>, which could result in crashes\n          later on. Restore the normal Postgres signal handler\n          after initializing PL/Perl.</p>"
  ],
  [
    "<p>Prevent PL/Perl from crashing if a recursive PL/Perl\n          function is redefined while being executed (Tom Lane)</p>"
  ],
  [
    "<p>Work around possible misoptimization in PL/Perl (Tom\n          Lane)</p>",
    "<p>Some Linux distributions contain an incorrect version\n          of <code class=\"filename\">pthread.h</code> that results\n          in incorrect compiled code in PL/Perl, leading to crashes\n          if a PL/Perl function calls another one that throws an\n          error.</p>"
  ],
  [
    "<p>Fix bugs in <code class=\"filename\">contrib/pg_trgm</code>'s <code class=\"literal\">LIKE</code> pattern analysis code (Fujii\n          Masao)</p>",
    "<p><code class=\"literal\">LIKE</code> queries using a\n          trigram index could produce wrong results if the pattern\n          contained <code class=\"literal\">LIKE</code> escape\n          characters.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_upgrade</span>'s\n          handling of line endings on Windows (Andrew Dunstan)</p>",
    "<p>Previously, <span class=\"application\">pg_upgrade</span> might add or remove\n          carriage returns in places such as function bodies.</p>"
  ],
  [
    "<p>On Windows, make <span class=\"application\">pg_upgrade</span> use backslash path\n          separators in the scripts it emits (Andrew Dunstan)</p>"
  ],
  [
    "<p>Remove unnecessary dependency on <span class=\"application\">pg_config</span> from <span class=\"application\">pg_upgrade</span> (Peter Eisentraut)</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2012f for DST law\n          changes in Fiji</p>"
  ]
]