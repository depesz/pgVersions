[
  [
    "<p>Enforce standard locking protocol for TOAST table updates, to prevent problems with <code class=\"command\">REINDEX CONCURRENTLY</code> (Michael Paquier)</p>",
    "<p>If applied to a TOAST table or TOAST table's index, <code class=\"command\">REINDEX CONCURRENTLY</code> tended to produce a corrupted index. This happened because sessions updating TOAST entries released their <code class=\"literal\">ROW EXCLUSIVE</code> locks immediately, rather than holding them until transaction commit as all other updates do. The fix is to make TOAST updates hold the table lock according to the normal rule. Any existing corrupted indexes can be repaired by reindexing again.</p>"
  ],
  [
    "<p>Avoid null-pointer crash in <code class=\"command\">ALTER STATISTICS</code> when the statistics object is dropped concurrently (Tomas Vondra)</p>"
  ],
  [
    "<p>Fix incorrect plan creation for parallel single-child Append nodes (David Rowley)</p>",
    "<p>In some cases the Append would be simplified away when it should not be, leading to wrong query results (duplicated rows).</p>"
  ],
  [
    "<p>Fix index-only scan plans for cases where not all index columns can be returned (Tom Lane)</p>",
    "<p>If an index has both returnable and non-returnable columns, and one of the non-returnable columns is an expression using a table column that appears in a returnable index column, then a query using that expression could result in an index-only scan plan that attempts to read the non-returnable column, instead of recomputing the expression from the returnable column as intended. The non-returnable column would read as NULL, resulting in wrong query results.</p>"
  ],
  [
    "<p>Ensure that casting to an unspecified typmod generates a RelabelType node rather than a length-coercion function call (Tom Lane)</p>",
    "<p>While the coercion function should do the right thing (nothing), this translation is undesirably inefficient.</p>"
  ],
  [
    "<p>Fix checking of <code class=\"type\">anycompatible</code>-family data type matches (Tom Lane)</p>",
    "<p>In some cases the parser would think that a function or operator with <code class=\"type\">anycompatible</code>-family polymorphic parameters matches a set of arguments that it really shouldn't match. In reported cases, that led to matching more than one operator to a call, leading to ambiguous-operator errors; but a failure later on is also possible.</p>"
  ],
  [
    "<p>Fix WAL replay failure when database consistency is reached exactly at a WAL page boundary (Álvaro Herrera)</p>"
  ],
  [
    "<p>Fix startup of a physical replica to tolerate transaction ID wraparound (Abhijit Menon-Sen, Tomas Vondra)</p>",
    "<p>If a replica server is started while the set of active transactions on the primary crosses a wraparound boundary (so that there are some newer transactions with smaller XIDs than older ones), the replica would fail with <span class=\"quote\">“<span class=\"quote\">out-of-order XID insertion in KnownAssignedXids</span>”</span>. The replica would retry, but could never get past that error.</p>"
  ],
  [
    "<p>In logical replication, avoid double transmission of a child table's data (Hou Zhijie)</p>",
    "<p>If a publication includes both child and parent tables, and has the <code class=\"literal\">publish_via_partition_root</code> option set, subscribers uselessly initiated synchronization on both child and parent tables. Ensure that only the parent table is synchronized in such cases.</p>"
  ],
  [
    "<p>Remove lexical limitations for SQL commands issued on a logical replication connection (Tom Lane)</p>",
    "<p>The walsender process would fail for a SQL command containing an unquoted semicolon, or with dollar-quoted literals containing odd numbers of single or double quote marks, or when the SQL command starts with a comment. Moreover, faulty error recovery could lead to unexpected errors in later commands too.</p>"
  ],
  [
    "<p>Fix possible loss of the commit timestamp for the last subtransaction of a transaction (Alex Kingsborough, Kyotaro Horiguchi)</p>"
  ],
  [
    "<p>Be sure to <code class=\"function\">fsync</code> the <code class=\"filename\">pg_logical/mappings</code> subdirectory during checkpoints (Nathan Bossart)</p>",
    "<p>On some filesystems this oversight could lead to losing logical rewrite status files after a system crash.</p>"
  ],
  [
    "<p>Build extended statistics for partitioned tables (Justin Pryzby)</p>",
    "<p>A previous bug fix disabled building of extended statistics for old-style inheritance trees, but it also prevented building them for partitioned tables, which was an unnecessary restriction. This change allows <code class=\"command\">ANALYZE</code> to compute values for statistics objects for partitioned tables. (But note that autovacuum does not process partitioned tables as such, so you must periodically issue manual <code class=\"command\">ANALYZE</code> on the partitioned table if you want to maintain such statistics.)</p>"
  ],
  [
    "<p>Ignore extended statistics for inheritance trees (Justin Pryzby)</p>",
    "<p>Currently, extended statistics values are only computed locally for each table, not for entire inheritance trees. However the values were mistakenly consulted when planning queries across inheritance trees, possibly resulting in worse-than-default estimates.</p>"
  ],
  [
    "<p>Disallow altering data type of a partitioned table's columns when the partitioned table's row type is used as a composite type elsewhere (Tom Lane)</p>",
    "<p>This restriction has long existed for regular tables, but through an oversight it was not checked for partitioned tables.</p>"
  ],
  [
    "<p>Disallow <code class=\"literal\">ALTER TABLE ... DROP NOT NULL</code> for a column that is part of a replica identity index (Haiying Tang, Hou Zhijie)</p>",
    "<p>The same prohibition already existed for primary key indexes.</p>"
  ],
  [
    "<p>Correctly update cached table state during <code class=\"command\">ALTER TABLE ADD PRIMARY KEY USING INDEX</code> (Hou Zhijie)</p>",
    "<p>Concurrent sessions failed to update their opinion of whether the table has a primary key, possibly causing incorrect logical replication behavior.</p>"
  ],
  [
    "<p>Correctly update cached table state when switching <code class=\"literal\">REPLICA IDENTITY</code> index (Tang Haiying, Hou Zhijie)</p>",
    "<p>Concurrent sessions failed to update their opinion of which index is the replica identity one, possibly causing incorrect logical replication behavior.</p>"
  ],
  [
    "<p>Allow parallel vacuuming and concurrent index building to be ignored while computing oldest xmin (Masahiko Sawada)</p>",
    "<p>Non-parallelized instances of these operations were already ignored, but the logic did not work for parallelized cases. Holding back the xmin horizon has undesirable effects such as delaying vacuum cleanup.</p>"
  ],
  [
    "<p>Avoid leaking memory during <code class=\"command\">REASSIGN OWNED BY</code> operations that reassign ownership of many objects (Justin Pryzby)</p>"
  ],
  [
    "<p>Improve performance of walsenders sending logical changes by avoiding unnecessary cache accesses (Hou Zhijie)</p>"
  ],
  [
    "<p>Fix display of <code class=\"literal\">cert</code> authentication method's options in <code class=\"structname\">pg_hba_file_rules</code> view (Magnus Hagander)</p>",
    "<p>The <code class=\"literal\">cert</code> authentication method implies <code class=\"literal\">clientcert=verify-full</code>, but the <code class=\"structname\">pg_hba_file_rules</code> view incorrectly reported <code class=\"literal\">clientcert=verify-ca</code>.</p>"
  ],
  [
    "<p>Fix display of whole-row variables appearing in <code class=\"literal\">INSERT ... VALUES</code> rules (Tom Lane)</p>",
    "<p>A whole-row variable would be printed as <span class=\"quote\">“<span class=\"quote\">var.*</span>”</span>, but that allows it to be expanded to individual columns when the rule is reloaded, resulting in different semantics. Attach an explicit cast to prevent that, as we do elsewhere.</p>"
  ],
  [
    "<p>Fix one-byte buffer overrun when applying Unicode string normalization to an empty string (Michael Paquier)</p>",
    "<p>The practical impact of this is limited thanks to alignment considerations; but in debug builds, a warning was raised.</p>"
  ],
  [
    "<p>Fix or remove some incorrect assertions (Simon Riggs, Michael Paquier, Alexander Lakhin)</p>",
    "<p>These errors should affect only debug builds, not production.</p>"
  ],
  [
    "<p>Fix race condition that could lead to failure to localize error messages that are reported early in multi-threaded use of <span class=\"application\">libpq</span> or <span class=\"application\">ecpglib</span> (Tom Lane)</p>"
  ],
  [
    "<p>Avoid calling <code class=\"function\">strerror</code> from <span class=\"application\">libpq</span>'s <code class=\"function\">PQcancel</code> function (Tom Lane)</p>",
    "<p><code class=\"function\">PQcancel</code> is supposed to be safe to call from a signal handler, but <code class=\"function\">strerror</code> is not safe. The faulty usage only occurred in the unlikely event of failure to send the cancel message to the server, perhaps explaining the lack of reports.</p>"
  ],
  [
    "<p>Make <span class=\"application\">psql</span>'s <code class=\"command\">\\password</code> command default to setting the password for <code class=\"literal\">CURRENT_USER</code>, not the connection's original user name (Tom Lane)</p>",
    "<p>This agrees with the documented behavior, and avoids probable permissions failure if <code class=\"command\">SET ROLE</code> or <code class=\"command\">SET SESSION AUTHORIZATION</code> has been done since the session began. To prevent confusion, the role name to be acted on is now included in the password prompt.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">psql</span> <code class=\"literal\">\\d</code> command's query for identifying parent triggers (Justin Pryzby)</p>",
    "<p>The previous coding failed with <span class=\"quote\">“<span class=\"quote\">more than one row returned by a subquery used as an expression</span>”</span> if a partition had triggers and there were unrelated statement-level triggers of the same name on some parent partitioned table.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">psql</span>'s tab-completion of label values for enum types (Tom Lane)</p>"
  ],
  [
    "<p>In <span class=\"application\">psql</span> and some other client programs, avoid trying to invoke <code class=\"function\">gettext()</code> from a control-C signal handler (Tom Lane)</p>",
    "<p>While no reported failures have been traced to this mistake, it seems highly unlikely to be a safe thing to do.</p>"
  ],
  [
    "<p>Allow canceling the initial password prompt in <span class=\"application\">pg_receivewal</span> and <span class=\"application\">pg_recvlogical</span> (Tom Lane, Nathan Bossart)</p>",
    "<p>Previously it was impossible to terminate these programs via control-C while they were prompting for a password.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_dump</span>'s dump ordering for user-defined casts (Tom Lane)</p>",
    "<p>In rare cases, the output script might refer to a user-defined cast before it had been created.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_dump</span>'s <code class=\"option\">--inserts</code> and <code class=\"option\">--column-inserts</code> modes to handle tables containing both generated columns and dropped columns (Tom Lane)</p>"
  ],
  [
    "<p>Fix possible mis-reporting of errors in <span class=\"application\">pg_dump</span> and <span class=\"application\">pg_basebackup</span> (Tom Lane)</p>",
    "<p>The previous code failed to check for errors from some kernel calls, and could report the wrong errno values in other cases.</p>"
  ],
  [
    "<p>Fix results of index-only scans on <code class=\"filename\">contrib/btree_gist</code> indexes on <code class=\"type\">char(<em class=\"replaceable\"><code>N</code></em>)</code> columns (Tom Lane)</p>",
    "<p>Index-only scans returned column values with trailing spaces removed, which is not the expected behavior. That happened because that's how the data was stored in the index. This fix changes the code to store <code class=\"type\">char(<em class=\"replaceable\"><code>N</code></em>)</code> values with the expected amount of space padding. The behavior of such an index will not change immediately unless you <code class=\"command\">REINDEX</code> it; otherwise space-stripped values will be gradually replaced over time during updates. Queries that do not use index-only scan plans will be unaffected in any case.</p>"
  ],
  [
    "<p>Change <span class=\"application\">configure</span> to use Python's <span class=\"application\">sysconfig</span> module, rather than the deprecated <span class=\"application\">distutils</span> module, to determine how to build PL/Python (Peter Eisentraut, Tom Lane, Andres Freund)</p>",
    "<p>With Python 3.10, this avoids <span class=\"application\">configure</span>-time warnings about <span class=\"application\">distutils</span> being deprecated and scheduled for removal in Python 3.12. Presumably, once 3.12 is out, <code class=\"literal\">configure --with-python</code> would fail altogether. This future-proofing does come at a cost: <span class=\"application\">sysconfig</span> did not exist before Python 2.7, nor before 3.2 in the Python 3 branch, so it is no longer possible to build PL/Python against long-dead Python versions.</p>"
  ],
  [
    "<p>Fix PL/Perl compile failure on Windows with Perl 5.28 and later (Victor Wagner)</p>"
  ],
  [
    "<p>Fix PL/Python compile failure with Python 3.11 and later (Peter Eisentraut)</p>"
  ],
  [
    "<p>Add support for building with Visual Studio 2022 (Hans Buschmann)</p>"
  ],
  [
    "<p>Allow the <code class=\"filename\">.bat</code> wrapper scripts in our MSVC build system to be called without first changing into their directory (Anton Voloshin, Andrew Dunstan)</p>"
  ]
]