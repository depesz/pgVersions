[
  [
    "<p>Ensure that <code class=\"literal\">INSERT ... ON CONFLICT DO UPDATE</code> checks table permissions and RLS policies in all cases (Dean Rasheed)</p>",
    "<p>The update path of <code class=\"literal\">INSERT ... ON CONFLICT DO UPDATE</code> requires <code class=\"literal\">SELECT</code> permission on the columns of the arbiter index, but it failed to check for that in the case of an arbiter specified by constraint name. In addition, for a table with row level security enabled, it failed to check updated rows against the table's <code class=\"literal\">SELECT</code> policies (regardless of how the arbiter index was specified). (CVE-2017-15099)</p>"
  ],
  [
    "<p>Fix crash due to rowtype mismatch in <code class=\"function\">json{b}_populate_recordset()</code> (Michael Paquier, Tom Lane)</p>",
    "<p>These functions used the result rowtype specified in the <code class=\"literal\">FROM ... AS</code> clause without checking that it matched the actual rowtype of the supplied tuple value. If it didn't, that would usually result in a crash, though disclosure of server memory contents seems possible as well. (CVE-2017-15098)</p>"
  ],
  [
    "<p>Fix sample server-start scripts to become <code class=\"literal\">$PGUSER</code> before opening <code class=\"literal\">$PGLOG</code> (Noah Misch)</p>",
    "<p>Previously, the postmaster log file was opened while still running as root. The database owner could therefore mount an attack against another system user by making <code class=\"literal\">$PGLOG</code> be a symbolic link to some other file, which would then become corrupted by appending log messages.</p>",
    "<p>By default, these scripts are not installed anywhere. Users who have made use of them will need to manually recopy them, or apply the same changes to their modified versions. If the existing <code class=\"literal\">$PGLOG</code> file is root-owned, it will need to be removed or renamed out of the way before restarting the server with the corrected script. (CVE-2017-12172)</p>"
  ],
  [
    "<p>Fix BRIN index summarization to handle concurrent table extension correctly (Álvaro Herrera)</p>",
    "<p>Previously, a race condition allowed some table rows to be omitted from the index. It may be necessary to reindex existing BRIN indexes to recover from past occurrences of this problem.</p>"
  ],
  [
    "<p>Fix possible failures during concurrent updates of a BRIN index (Tom Lane)</p>",
    "<p>These race conditions could result in errors like <span class=\"quote\">“<span class=\"quote\">invalid index offnum</span>”</span> or <span class=\"quote\">“<span class=\"quote\">inconsistent range map</span>”</span>.</p>"
  ],
  [
    "<p>Prevent logical replication from setting non-replicated columns to nulls when replicating an <code class=\"command\">UPDATE</code> (Petr Jelinek)</p>"
  ],
  [
    "<p>Fix logical replication to fire <code class=\"literal\">BEFORE ROW DELETE</code> triggers when expected (Masahiko Sawada)</p>",
    "<p>Previously, that failed to happen unless the table also had a <code class=\"literal\">BEFORE ROW UPDATE</code> trigger.</p>"
  ],
  [
    "<p>Fix crash when logical decoding is invoked from a SPI-using function, in particular any function written in a PL language (Tom Lane)</p>"
  ],
  [
    "<p>Ignore CTEs when looking up the target table for <code class=\"command\">INSERT</code>/<code class=\"command\">UPDATE</code>/<code class=\"command\">DELETE</code>, and prevent matching schema-qualified target table names to trigger transition table names (Thomas Munro)</p>",
    "<p>This restores the pre-v10 behavior for CTEs attached to DML commands.</p>"
  ],
  [
    "<p>Avoid evaluating an aggregate function's argument expression(s) at rows where its <code class=\"literal\">FILTER</code> test fails (Tom Lane)</p>",
    "<p>This restores the pre-v10 (and SQL-standard) behavior.</p>"
  ],
  [
    "<p>Fix incorrect query results when multiple <code class=\"literal\">GROUPING SETS</code> columns contain the same simple variable (Tom Lane)</p>"
  ],
  [
    "<p>Fix query-lifespan memory leakage while evaluating a set-returning function in a <code class=\"command\">SELECT</code>'s target list (Tom Lane)</p>"
  ],
  [
    "<p>Allow parallel execution of prepared statements with generic plans (Amit Kapila, Kuntal Ghosh)</p>"
  ],
  [
    "<p>Fix incorrect parallelization decisions for nested queries (Amit Kapila, Kuntal Ghosh)</p>"
  ],
  [
    "<p>Fix parallel query handling to not fail when a recently-used role is dropped (Amit Kapila)</p>"
  ],
  [
    "<p>Fix crash in parallel execution of a bitmap scan having a BitmapAnd plan node below a BitmapOr node (Dilip Kumar)</p>"
  ],
  [
    "<p>Fix <code class=\"function\">json_build_array()</code>, <code class=\"function\">json_build_object()</code>, and their <code class=\"type\">jsonb</code> equivalents to handle explicit <code class=\"literal\">VARIADIC</code> arguments correctly (Michael Paquier)</p>"
  ],
  [
    "<p>Fix autovacuum's <span class=\"quote\">“<span class=\"quote\">work item</span>”</span> logic to prevent possible crashes and silent loss of work items (Álvaro Herrera)</p>"
  ],
  [
    "<p>Fix corner-case crashes when columns have been added to the end of a view (Tom Lane)</p>"
  ],
  [
    "<p>Record proper dependencies when a view or rule contains <code class=\"structname\">FieldSelect</code> or <code class=\"structname\">FieldStore</code> expression nodes (Tom Lane)</p>",
    "<p>Lack of these dependencies could allow a column or data type <code class=\"command\">DROP</code> to go through when it ought to fail, thereby causing later uses of the view or rule to get errors. This patch does not do anything to protect existing views/rules, only ones created in the future.</p>"
  ],
  [
    "<p>Correctly detect hashability of range data types (Tom Lane)</p>",
    "<p>The planner mistakenly assumed that any range type could be hashed for use in hash joins or hash aggregation, but actually it must check whether the range's subtype has hash support. This does not affect any of the built-in range types, since they're all hashable anyway.</p>"
  ],
  [
    "<p>Correctly ignore <code class=\"structname\">RelabelType</code> expression nodes when examining functional-dependency statistics (David Rowley)</p>",
    "<p>This allows, e.g., extended statistics on <code class=\"type\">varchar</code> columns to be used properly.</p>"
  ],
  [
    "<p>Prevent sharing transition states between ordered-set aggregates (David Rowley)</p>",
    "<p>This causes a crash with the built-in ordered-set aggregates, and probably with user-written ones as well. v11 and later will include provisions for dealing with such cases safely, but in released branches, just disable the optimization.</p>"
  ],
  [
    "<p>Prevent <code class=\"varname\">idle_in_transaction_session_timeout</code> from being ignored when a <code class=\"varname\">statement_timeout</code> occurred earlier (Lukas Fittl)</p>"
  ],
  [
    "<p>Fix low-probability loss of <code class=\"command\">NOTIFY</code> messages due to XID wraparound (Marko Tiikkaja, Tom Lane)</p>",
    "<p>If a session executed no queries, but merely listened for notifications, for more than 2 billion transactions, it started to miss some notifications from concurrently-committing transactions.</p>"
  ],
  [
    "<p>Reduce the frequency of data flush requests during bulk file copies to avoid performance problems on macOS, particularly with its new APFS file system (Tom Lane)</p>"
  ],
  [
    "<p>Allow <code class=\"command\">COPY</code>'s <code class=\"literal\">FREEZE</code> option to work when the transaction isolation level is <code class=\"literal\">REPEATABLE READ</code> or higher (Noah Misch)</p>",
    "<p>This case was unintentionally broken by a previous bug fix.</p>"
  ],
  [
    "<p>Fix <code class=\"function\">AggGetAggref()</code> to return the correct <code class=\"structname\">Aggref</code> nodes to aggregate final functions whose transition calculations have been merged (Tom Lane)</p>"
  ],
  [
    "<p>Fix insufficient schema-qualification in some new queries in <span class=\"application\">pg_dump</span> and <span class=\"application\">psql</span> (Vitaly Burovoy, Tom Lane, Noah Misch)</p>"
  ],
  [
    "<p>Avoid use of <code class=\"literal\">@&gt;</code> operator in <span class=\"application\">psql</span>'s queries for <code class=\"command\">\\d</code> (Tom Lane)</p>",
    "<p>This prevents problems when the <span class=\"application\">parray_gin</span> extension is installed, since that defines a conflicting operator.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_basebackup</span>'s matching of tablespace paths to canonicalize both paths before comparing (Michael Paquier)</p>",
    "<p>This is particularly helpful on Windows.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">libpq</span> to not require user's home directory to exist (Tom Lane)</p>",
    "<p>In v10, failure to find the home directory while trying to read <code class=\"filename\">~/.pgpass</code> was treated as a hard error, but it should just cause that file to not be found. Both v10 and previous release branches made the same mistake when reading <code class=\"filename\">~/.pg_service.conf</code>, though this was less obvious since that file is not sought unless a service name is specified.</p>"
  ],
  [
    "<p>In ecpglib, correctly handle backslashes in string literals depending on whether <code class=\"varname\">standard_conforming_strings</code> is set (Tsunakawa Takayuki)</p>"
  ],
  [
    "<p>Make ecpglib's Informix-compatibility mode ignore fractional digits in integer input strings, as expected (Gao Zengqi, Michael Meskes)</p>"
  ],
  [
    "<p>Fix missing temp-install prerequisites for <code class=\"literal\">check</code>-like Make targets (Noah Misch)</p>",
    "<p>Some non-default test procedures that are meant to work like <code class=\"literal\">make check</code> failed to ensure that the temporary installation was up to date.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2017c for DST law changes in Fiji, Namibia, Northern Cyprus, Sudan, Tonga, and Turks &amp; Caicos Islands, plus historical corrections for Alaska, Apia, Burma, Calcutta, Detroit, Ireland, Namibia, and Pago Pago.</p>"
  ],
  [
    "<p>In the documentation, restore HTML anchors to being upper-case strings (Peter Eisentraut)</p>",
    "<p>Due to a toolchain change, the 10.0 user manual had lower-case strings for intrapage anchors, thus breaking some external links into our website documentation. Return to our previous convention of using upper-case strings.</p>"
  ]
]