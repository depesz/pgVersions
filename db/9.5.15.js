[
  [
    "<p>Fix corner-case failures in <code class=\"FUNCTION\">has_<tt class=\"REPLACEABLE c2\">foo</tt>_privilege()</code> family of functions (Tom Lane)</p>",
    "<p>Return NULL rather than throwing an error when an invalid object OID is provided. Some of these functions got that right already, but not all. <code class=\"FUNCTION\">has_column_privilege()</code> was additionally capable of crashing on some platforms.</p>"
  ],
  [
    "<p>Avoid O(N^2) slowdown in regular expression match/split functions on long strings (Andrew Gierth)</p>"
  ],
  [
    "<p>Fix parsing of standard multi-character operators that are immediately followed by a comment or <tt class=\"LITERAL\">+</tt> or <tt class=\"LITERAL\">-</tt> (Andrew Gierth)</p>",
    "<p>This oversight could lead to parse errors, or to incorrect assignment of precedence.</p>"
  ],
  [
    "<p>Avoid O(N^3) slowdown in lexer for long strings of <tt class=\"LITERAL\">+</tt> or <tt class=\"LITERAL\">-</tt> characters (Andrew Gierth)</p>"
  ],
  [
    "<p>Fix mis-execution of SubPlans when the outer query is being scanned backwards (Andrew Gierth)</p>"
  ],
  [
    "<p>Fix failure of <tt class=\"COMMAND\">UPDATE/DELETE ... WHERE CURRENT OF ...</tt> after rewinding the referenced cursor (Tom Lane)</p>",
    "<p>A cursor that scans multiple relations (particularly an inheritance tree) could produce wrong behavior if rewound to an earlier relation.</p>"
  ],
  [
    "<p>Fix <code class=\"FUNCTION\">EvalPlanQual</code> to handle conditionally-executed InitPlans properly (Andrew Gierth, Tom Lane)</p>",
    "<p>This resulted in hard-to-reproduce crashes or wrong answers in concurrent updates, if they contained code such as an uncorrelated sub-<tt class=\"LITERAL\">SELECT</tt> inside a <tt class=\"LITERAL\">CASE</tt> construct.</p>"
  ],
  [
    "<p>Fix character-class checks to not fail on Windows for Unicode characters above U+FFFF (Tom Lane, Kenji Uno)</p>",
    "<p>This bug affected full-text-search operations, as well as <tt class=\"FILENAME\">contrib/ltree</tt> and <tt class=\"FILENAME\">contrib/pg_trgm</tt>.</p>"
  ],
  [
    "<p>Ensure that sequences owned by a foreign table are processed by <tt class=\"LITERAL\">ALTER OWNER</tt> on the table (Peter Eisentraut)</p>",
    "<p>The ownership change should propagate to such sequences as well, but this was missed for foreign tables.</p>"
  ],
  [
    "<p>Ensure that the server will process already-received <tt class=\"LITERAL\">NOTIFY</tt> and <tt class=\"LITERAL\">SIGTERM</tt> interrupts before waiting for client input (Jeff Janes, Tom Lane)</p>"
  ],
  [
    "<p>Fix over-allocation of space for <code class=\"FUNCTION\">array_out()</code>'s result string (Keiichi Hirobe)</p>"
  ],
  [
    "<p>Fix memory leak in repeated SP-GiST index scans (Tom Lane)</p>",
    "<p>This is only known to amount to anything significant in cases where an exclusion constraint using SP-GiST receives many new index entries in a single command.</p>"
  ],
  [
    "<p>Ensure that <code class=\"FUNCTION\">ApplyLogicalMappingFile()</code> closes the mapping file when done with it (Tomas Vondra)</p>",
    "<p>Previously, the file descriptor was leaked, eventually resulting in failures during logical decoding.</p>"
  ],
  [
    "<p>Fix logical decoding to handle cases where a mapped catalog table is repeatedly rewritten, e.g. by <tt class=\"LITERAL\">VACUUM FULL</tt> (Andres Freund)</p>"
  ],
  [
    "<p>Prevent starting the server with <tt class=\"VARNAME\">wal_level</tt> set to too low a value to support an existing replication slot (Andres Freund)</p>"
  ],
  [
    "<p>Avoid crash if a utility command causes infinite recursion (Tom Lane)</p>"
  ],
  [
    "<p>When initializing a hot standby, cope with duplicate XIDs caused by two-phase transactions on the master (Michael Paquier, Konstantin Knizhnik)</p>"
  ],
  [
    "<p>Fix event triggers to handle nested <tt class=\"COMMAND\">ALTER TABLE</tt> commands (Michael Paquier, Álvaro Herrera)</p>"
  ],
  [
    "<p>Propagate parent process's transaction and statement start timestamps to parallel workers (Konstantin Knizhnik)</p>",
    "<p>This prevents misbehavior of functions such as <code class=\"FUNCTION\">transaction_timestamp()</code> when executed in a worker.</p>"
  ],
  [
    "<p>Fix WAL file recycling logic to work correctly on standby servers (Michael Paquier)</p>",
    "<p>Depending on the setting of <tt class=\"VARNAME\">archive_mode</tt>, a standby might fail to remove some WAL files that could be removed.</p>"
  ],
  [
    "<p>Fix handling of commit-timestamp tracking during recovery (Masahiko Sawada, Michael Paquier)</p>",
    "<p>If commit timestamp tracking has been turned on or off, recovery might fail due to trying to fetch the commit timestamp for a transaction that did not record it.</p>"
  ],
  [
    "<p>Randomize the <code class=\"FUNCTION\">random()</code> seed in bootstrap and standalone backends, and in <span class=\"APPLICATION\">initdb</span> (Noah Misch)</p>",
    "<p>The main practical effect of this change is that it avoids a scenario where <span class=\"APPLICATION\">initdb</span> might mistakenly conclude that POSIX shared memory is not available, due to name collisions caused by always using the same random seed.</p>"
  ],
  [
    "<p>Allow DSM allocation to be interrupted (Chris Travers)</p>"
  ],
  [
    "<p>Properly handle turning <tt class=\"VARNAME\">full_page_writes</tt> on dynamically (Kyotaro Horiguchi)</p>"
  ],
  [
    "<p>Avoid possible buffer overrun when replaying GIN page recompression from WAL (Alexander Korotkov, Sivasubramanian Ramasubramanian)</p>"
  ],
  [
    "<p>Fix missed fsync of a replication slot's directory (Konstantin Knizhnik, Michael Paquier)</p>"
  ],
  [
    "<p>Fix unexpected timeouts when using <tt class=\"VARNAME\">wal_sender_timeout</tt> on a slow server (Noah Misch)</p>"
  ],
  [
    "<p>Ensure that hot standby processes use the correct WAL consistency point (Alexander Kukushkin, Michael Paquier)</p>",
    "<p>This prevents possible misbehavior just after a standby server has reached a consistent database state during WAL replay.</p>"
  ],
  [
    "<p>Ensure background workers are stopped properly when the postmaster receives a fast-shutdown request before completing database startup (Alexander Kukushkin)</p>"
  ],
  [
    "<p>Don't run atexit callbacks when servicing <tt class=\"LITERAL\">SIGQUIT</tt> (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Don't record foreign-server user mappings as members of extensions (Tom Lane)</p>",
    "<p>If <tt class=\"COMMAND\">CREATE USER MAPPING</tt> is executed in an extension script, an extension dependency was created for the user mapping, which is unexpected. Roles can't be extension members, so user mappings shouldn't be either.</p>"
  ],
  [
    "<p>Make syslogger more robust against failures in opening CSV log files (Tom Lane)</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">psql</span>, as well as documentation examples, to call <code class=\"FUNCTION\">PQconsumeInput()</code> before each <code class=\"FUNCTION\">PQnotifies()</code> call (Tom Lane)</p>",
    "<p>This fixes cases in which <span class=\"APPLICATION\">psql</span> would not report receipt of a <tt class=\"LITERAL\">NOTIFY</tt> message until after the next command.</p>"
  ],
  [
    "<p>Fix possible inconsistency in <span class=\"APPLICATION\">pg_dump</span>'s sorting of dissimilar object names (Jacob Champion)</p>"
  ],
  [
    "<p>Ensure that <span class=\"APPLICATION\">pg_restore</span> will schema-qualify the table name when emitting <tt class=\"LITERAL\">DISABLE</tt>/<tt class=\"LITERAL\">ENABLE TRIGGER</tt> commands (Tom Lane)</p>",
    "<p>This avoids failures due to the new policy of running restores with restrictive search path.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_upgrade</span> to handle event triggers in extensions correctly (Haribabu Kommi)</p>",
    "<p><span class=\"APPLICATION\">pg_upgrade</span> failed to preserve an event trigger's extension-membership status.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_upgrade</span>'s cluster state check to work correctly on a standby server (Bruce Momjian)</p>"
  ],
  [
    "<p>Enforce type <tt class=\"TYPE\">cube</tt>'s dimension limit in all <tt class=\"FILENAME\">contrib/cube</tt> functions (Andrey Borodin)</p>",
    "<p>Previously, some cube-related functions could construct values that would be rejected by <code class=\"FUNCTION\">cube_in()</code>, leading to dump/reload failures.</p>"
  ],
  [
    "<p>Fix <tt class=\"FILENAME\">contrib/unaccent</tt>'s <code class=\"FUNCTION\">unaccent()</code> function to use the <tt class=\"LITERAL\">unaccent</tt> text search dictionary that is in the same schema as the function (Tom Lane)</p>",
    "<p>Previously it tried to look up the dictionary using the search path, which could fail if the search path has a restrictive value.</p>"
  ],
  [
    "<p>Fix build problems on macOS 10.14 (Mojave) (Tom Lane)</p>",
    "<p>Adjust <span class=\"APPLICATION\">configure</span> to add an <tt class=\"OPTION\">-isysroot</tt> switch to <tt class=\"VARNAME\">CPPFLAGS</tt>; without this, PL/Perl and PL/Tcl fail to configure or build on macOS 10.14. The specific sysroot used can be overridden at configure time or build time by setting the <tt class=\"VARNAME\">PG_SYSROOT</tt> variable in the arguments of <span class=\"APPLICATION\">configure</span> or <span class=\"APPLICATION\">make</span>.</p>",
    "<p>It is now recommended that Perl-related extensions write <tt class=\"LITERAL\">$(perl_includespec)</tt> rather than <tt class=\"LITERAL\">-I$(perl_archlibexp)/CORE</tt> in their compiler flags. The latter continues to work on most platforms, but not recent macOS.</p>",
    "<p>Also, it should no longer be necessary to specify <tt class=\"OPTION\">--with-tclconfig</tt> manually to get PL/Tcl to build on recent macOS releases.</p>"
  ],
  [
    "<p>Fix MSVC build and regression-test scripts to work on recent Perl versions (Andrew Dunstan)</p>",
    "<p>Perl no longer includes the current directory in its search path by default; work around that.</p>"
  ],
  [
    "<p>On Windows, allow the regression tests to be run by an Administrator account (Andrew Dunstan)</p>",
    "<p>To do this safely, <span class=\"APPLICATION\">pg_regress</span> now gives up any such privileges at startup.</p>"
  ],
  [
    "<p>Support building on Windows with Visual Studio 2015 or Visual Studio 2017 (Michael Paquier, Haribabu Kommi)</p>"
  ],
  [
    "<p>Allow btree comparison functions to return <tt class=\"LITERAL\">INT_MIN</tt> (Tom Lane)</p>",
    "<p>Up to now, we've forbidden datatype-specific comparison functions from returning <tt class=\"LITERAL\">INT_MIN</tt>, which allows callers to invert the sort order just by negating the comparison result. However, this was never safe for comparison functions that directly return the result of <code class=\"FUNCTION\">memcmp()</code>, <code class=\"FUNCTION\">strcmp()</code>, etc, as POSIX doesn't place any such restriction on those functions. At least some recent versions of <code class=\"FUNCTION\">memcmp()</code> can return <tt class=\"LITERAL\">INT_MIN</tt>, causing incorrect sort ordering. Hence, we've removed this restriction. Callers must now use the <tt class=\"LITERAL\">INVERT_COMPARE_RESULT()</tt> macro if they wish to invert the sort order.</p>"
  ],
  [
    "<p>Fix recursion hazard in shared-invalidation message processing (Tom Lane)</p>",
    "<p>This error could, for example, result in failure to access a system catalog or index that had just been processed by <tt class=\"COMMAND\">VACUUM FULL</tt>.</p>",
    "<p>This change adds a new result code for <code class=\"FUNCTION\">LockAcquire</code>, which might possibly affect external callers of that function, though only very unusual usage patterns would have an issue with it. The API of <code class=\"FUNCTION\">LockAcquireExtended</code> is also changed.</p>"
  ],
  [
    "<p>Save and restore SPI's global variables during <code class=\"FUNCTION\">SPI_connect()</code> and <code class=\"FUNCTION\">SPI_finish()</code> (Chapman Flack, Tom Lane)</p>",
    "<p>This prevents possible interference when one SPI-using function calls another.</p>"
  ],
  [
    "<p>Provide <tt class=\"LITERAL\">ALLOCSET_DEFAULT_SIZES</tt> and sibling macros in back branches (Tom Lane)</p>",
    "<p>These macros have existed since 9.6, but there were requests to add them to older branches to allow extensions to rely on them without branch-specific coding.</p>"
  ],
  [
    "<p>Avoid using potentially-under-aligned page buffers (Tom Lane)</p>",
    "<p>Invent new union types <tt class=\"TYPE\">PGAlignedBlock</tt> and <tt class=\"TYPE\">PGAlignedXLogBlock</tt>, and use these in place of plain char arrays, ensuring that the compiler can't place the buffer at a misaligned start address. This fixes potential core dumps on alignment-picky platforms, and may improve performance even on platforms that allow misalignment.</p>"
  ],
  [
    "<p>Make <tt class=\"FILENAME\">src/port/snprintf.c</tt> follow the C99 standard's definition of <code class=\"FUNCTION\">snprintf()</code>'s result value (Tom Lane)</p>",
    "<p>On platforms where this code is used (mostly Windows), its pre-C99 behavior could lead to failure to detect buffer overrun, if the calling code assumed C99 semantics.</p>"
  ],
  [
    "<p>When building on i386 with the <span class=\"APPLICATION\">clang</span> compiler, require <tt class=\"OPTION\">-msse2</tt> to be used (Andres Freund)</p>",
    "<p>This avoids problems with missed floating point overflow checks.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">configure</span>'s detection of the result type of <code class=\"FUNCTION\">strerror_r()</code> (Tom Lane)</p>",
    "<p>The previous coding got the wrong answer when building with <span class=\"APPLICATION\">icc</span> on Linux (and perhaps in other cases), leading to <span class=\"APPLICATION\">libpq</span> not returning useful error messages for system-reported errors.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"APPLICATION\">tzdata</span> release 2018g for DST law changes in Chile, Fiji, Morocco, and Russia (Volgograd), plus historical corrections for China, Hawaii, Japan, Macau, and North Korea.</p>"
  ]
]