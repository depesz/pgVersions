[
  [
    "<p>Fix a race condition that could cause indexes built with <tt class=\"COMMAND\">CREATE INDEX CONCURRENTLY</tt> to be corrupt (Pavan Deolasee, Tom Lane)</p>",
    "<p>If <tt class=\"COMMAND\">CREATE INDEX CONCURRENTLY</tt> was used to build an index that depends on a column not previously indexed, then rows updated by transactions that ran concurrently with the <tt class=\"COMMAND\">CREATE INDEX</tt> command could have received incorrect index entries. If you suspect this may have happened, the most reliable solution is to rebuild affected indexes after installing this update.</p>"
  ],
  [
    "<p>Ensure that the special snapshot used for catalog scans is not invalidated by premature data pruning (Tom Lane)</p>",
    "<p>Backends failed to account for this snapshot when advertising their oldest xmin, potentially allowing concurrent vacuuming operations to remove data that was still needed. This led to transient failures along the lines of <span class=\"QUOTE\">\"cache lookup failed for relation 1255\"</span>.</p>"
  ],
  [
    "<p>Fix incorrect WAL logging for BRIN indexes (Kuntal Ghosh)</p>",
    "<p>The WAL record emitted for a BRIN <span class=\"QUOTE\">\"revmap\"</span> page when moving an index tuple to a different page was incorrect. Replay would make the related portion of the index useless, forcing it to be recomputed.</p>"
  ],
  [
    "<p>Unconditionally WAL-log creation of the <span class=\"QUOTE\">\"init fork\"</span> for an unlogged table (Michael Paquier)</p>",
    "<p>Previously, this was skipped when <a href=\"https://www.postgresql.org/docs/9.6/runtime-config-wal.html#GUC-WAL-LEVEL\">wal_level</a> = <tt class=\"LITERAL\">minimal</tt>, but actually it's necessary even in that case to ensure that the unlogged table is properly reset to empty after a crash.</p>"
  ],
  [
    "<p>If the stats collector dies during hot standby, restart it (Takayuki Tsunakawa)</p>"
  ],
  [
    "<p>Ensure that hot standby feedback works correctly when it's enabled at standby server start (Ants Aasma, Craig Ringer)</p>"
  ],
  [
    "<p>Check for interrupts while hot standby is waiting for a conflicting query (Simon Riggs)</p>"
  ],
  [
    "<p>Avoid constantly respawning the autovacuum launcher in a corner case (Amit Khandekar)</p>",
    "<p>This fix avoids problems when autovacuum is nominally off and there are some tables that require freezing, but all such tables are already being processed by autovacuum workers.</p>"
  ],
  [
    "<p>Disallow setting the <tt class=\"REPLACEABLE c2\">num_sync</tt> field to zero in <a href=\"https://www.postgresql.org/docs/9.6/runtime-config-replication.html#GUC-SYNCHRONOUS-STANDBY-NAMES\">synchronous_standby_names</a> (Fujii Masao)</p>",
    "<p>The correct way to disable synchronous standby is to set the whole value to an empty string.</p>"
  ],
  [
    "<p>Don't count background worker processes against a user's connection limit (David Rowley)</p>"
  ],
  [
    "<p>Fix check for when an extension member object can be dropped (Tom Lane)</p>",
    "<p>Extension upgrade scripts should be able to drop member objects, but this was disallowed for serial-column sequences, and possibly other cases.</p>"
  ],
  [
    "<p>Fix tracking of initial privileges for extension member objects so that it works correctly with <tt class=\"COMMAND\">ALTER EXTENSION ... ADD/DROP</tt> (Stephen Frost)</p>",
    "<p>An object's current privileges at the time it is added to the extension will now be considered its default privileges; only later changes in its privileges will be dumped by subsequent <span class=\"APPLICATION\">pg_dump</span> runs.</p>"
  ],
  [
    "<p>Make sure <tt class=\"COMMAND\">ALTER TABLE</tt> preserves index tablespace assignments when rebuilding indexes (Tom Lane, Michael Paquier)</p>",
    "<p>Previously, non-default settings of <a href=\"https://www.postgresql.org/docs/9.6/runtime-config-client.html#GUC-DEFAULT-TABLESPACE\">default_tablespace</a> could result in broken indexes.</p>"
  ],
  [
    "<p>Fix incorrect updating of trigger function properties when changing a foreign-key constraint's deferrability properties with <tt class=\"COMMAND\">ALTER TABLE ... ALTER CONSTRAINT</tt> (Tom Lane)</p>",
    "<p>This led to odd failures during subsequent exercise of the foreign key, as the triggers were fired at the wrong times.</p>"
  ],
  [
    "<p>Prevent dropping a foreign-key constraint if there are pending trigger events for the referenced relation (Tom Lane)</p>",
    "<p>This avoids <span class=\"QUOTE\">\"could not find trigger <tt class=\"REPLACEABLE c2\">NNN</tt>\"</span> or <span class=\"QUOTE\">\"relation <tt class=\"REPLACEABLE c2\">NNN</tt> has no triggers\"</span> errors.</p>"
  ],
  [
    "<p>Fix <tt class=\"COMMAND\">ALTER TABLE ... SET DATA TYPE ... USING</tt> when child table has different column ordering than the parent (Álvaro Herrera)</p>",
    "<p>Failure to adjust the column numbering in the <tt class=\"LITERAL\">USING</tt> expression led to errors, typically <span class=\"QUOTE\">\"attribute <tt class=\"REPLACEABLE c2\">N</tt> has wrong type\"</span>.</p>"
  ],
  [
    "<p>Fix processing of OID column when a table with OIDs is associated to a parent with OIDs via <tt class=\"COMMAND\">ALTER TABLE ... INHERIT</tt> (Amit Langote)</p>",
    "<p>The OID column should be treated the same as regular user columns in this case, but it wasn't, leading to odd behavior in later inheritance changes.</p>"
  ],
  [
    "<p>Ensure that <tt class=\"COMMAND\">CREATE TABLE ... LIKE ... WITH OIDS</tt> creates a table with OIDs, whether or not the <tt class=\"LITERAL\">LIKE</tt>-referenced table(s) have OIDs (Tom Lane)</p>"
  ],
  [
    "<p>Fix <tt class=\"COMMAND\">CREATE OR REPLACE VIEW</tt> to update the view query before attempting to apply the new view options (Dean Rasheed)</p>",
    "<p>Previously the command would fail if the new options were inconsistent with the old view definition.</p>"
  ],
  [
    "<p>Report correct object identity during <tt class=\"COMMAND\">ALTER TEXT SEARCH CONFIGURATION</tt> (Artur Zakirov)</p>",
    "<p>The wrong catalog OID was reported to extensions such as logical decoding.</p>"
  ],
  [
    "<p>Fix commit timestamp mechanism to not fail when queried about the special XIDs <tt class=\"LITERAL\">FrozenTransactionId</tt> and <tt class=\"LITERAL\">BootstrapTransactionId</tt> (Craig Ringer)</p>"
  ],
  [
    "<p>Fix incorrect use of view reloptions as regular table reloptions (Tom Lane)</p>",
    "<p>The symptom was spurious <span class=\"QUOTE\">\"ON CONFLICT is not supported on table ... used as a catalog table\"</span> errors when the target of <tt class=\"COMMAND\">INSERT ... ON CONFLICT</tt> is a view with cascade option.</p>"
  ],
  [
    "<p>Fix incorrect <span class=\"QUOTE\">\"target lists can have at most <tt class=\"REPLACEABLE c2\">N</tt> entries\"</span> complaint when using <tt class=\"LITERAL\">ON CONFLICT</tt> with wide tables (Tom Lane)</p>"
  ],
  [
    "<p>Fix spurious <span class=\"QUOTE\">\"query provides a value for a dropped column\"</span> errors during <tt class=\"COMMAND\">INSERT</tt> or <tt class=\"COMMAND\">UPDATE</tt> on a table with a dropped column (Tom Lane)</p>"
  ],
  [
    "<p>Prevent multicolumn expansion of <tt class=\"REPLACEABLE c2\">foo</tt><tt class=\"LITERAL\">.*</tt> in an <tt class=\"COMMAND\">UPDATE</tt> source expression (Tom Lane)</p>",
    "<p>This led to <span class=\"QUOTE\">\"UPDATE target count mismatch --- internal error\"</span>. Now the syntax is understood as a whole-row variable, as it would be in other contexts.</p>"
  ],
  [
    "<p>Ensure that column typmods are determined accurately for multi-row <tt class=\"LITERAL\">VALUES</tt> constructs (Tom Lane)</p>",
    "<p>This fixes problems occurring when the first value in a column has a determinable typmod (e.g., length for a <tt class=\"TYPE\">varchar</tt> value) but later values don't share the same limit.</p>"
  ],
  [
    "<p>Throw error for an unfinished Unicode surrogate pair at the end of a Unicode string (Tom Lane)</p>",
    "<p>Normally, a Unicode surrogate leading character must be followed by a Unicode surrogate trailing character, but the check for this was missed if the leading character was the last character in a Unicode string literal (<tt class=\"LITERAL\">U&amp;'...'</tt>) or Unicode identifier (<tt class=\"LITERAL\">U&amp;\"...\"</tt>).</p>"
  ],
  [
    "<p>Fix execution of <tt class=\"LITERAL\">DISTINCT</tt> and ordered aggregates when multiple such aggregates are able to share the same transition state (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Fix implementation of phrase search operators in <tt class=\"TYPE\">tsquery</tt> (Tom Lane)</p>",
    "<p>Remove incorrect, and inconsistently-applied, rewrite rules that tried to transform away AND/OR/NOT operators appearing below a PHRASE operator; instead upgrade the execution engine to handle such cases correctly. This fixes assorted strange behavior and possible crashes for text search queries containing such combinations. Also fix nested PHRASE operators to work sanely in combinations other than simple left-deep trees, correct the behavior when removing stopwords from a phrase search clause, and make sure that index searches behave consistently with simple sequential-scan application of such queries.</p>"
  ],
  [
    "<p>Ensure that a purely negative text search query, such as <tt class=\"LITERAL\">!foo</tt>, matches empty <tt class=\"TYPE\">tsvector</tt>s (Tom Dunstan)</p>",
    "<p>Such matches were found by GIN index searches, but not by sequential scans or GiST index searches.</p>"
  ],
  [
    "<p>Prevent crash when <code class=\"FUNCTION\">ts_rewrite()</code> replaces a non-top-level subtree with an empty query (Artur Zakirov)</p>"
  ],
  [
    "<p>Fix performance problems in <code class=\"FUNCTION\">ts_rewrite()</code> (Tom Lane)</p>"
  ],
  [
    "<p>Fix <code class=\"FUNCTION\">ts_rewrite()</code>'s handling of nested NOT operators (Tom Lane)</p>"
  ],
  [
    "<p>Improve speed of user-defined aggregates that use <code class=\"FUNCTION\">array_append()</code> as transition function (Tom Lane)</p>"
  ],
  [
    "<p>Fix <code class=\"FUNCTION\">array_fill()</code> to handle empty arrays properly (Tom Lane)</p>"
  ],
  [
    "<p>Fix possible crash in <code class=\"FUNCTION\">array_position()</code> or <code class=\"FUNCTION\">array_positions()</code> when processing arrays of records (Junseok Yang)</p>"
  ],
  [
    "<p>Fix one-byte buffer overrun in <code class=\"FUNCTION\">quote_literal_cstr()</code> (Heikki Linnakangas)</p>",
    "<p>The overrun occurred only if the input consisted entirely of single quotes and/or backslashes.</p>"
  ],
  [
    "<p>Prevent multiple calls of <code class=\"FUNCTION\">pg_start_backup()</code> and <code class=\"FUNCTION\">pg_stop_backup()</code> from running concurrently (Michael Paquier)</p>",
    "<p>This avoids an assertion failure, and possibly worse things, if someone tries to run these functions in parallel.</p>"
  ],
  [
    "<p>Disable transform that attempted to remove no-op <tt class=\"LITERAL\">AT TIME ZONE</tt> conversions (Tom Lane)</p>",
    "<p>This resulted in wrong answers when the simplified expression was used in an index condition.</p>"
  ],
  [
    "<p>Avoid discarding <tt class=\"TYPE\">interval</tt>-to-<tt class=\"TYPE\">interval</tt> casts that aren't really no-ops (Tom Lane)</p>",
    "<p>In some cases, a cast that should result in zeroing out low-order <tt class=\"TYPE\">interval</tt> fields was mistakenly deemed to be a no-op and discarded. An example is that casting from <tt class=\"TYPE\">INTERVAL MONTH</tt> to <tt class=\"TYPE\">INTERVAL YEAR</tt> failed to clear the months field.</p>"
  ],
  [
    "<p>Fix crash if the number of workers available to a parallel query decreases during a rescan (Andreas Seltenreich)</p>"
  ],
  [
    "<p>Fix bugs in transmitting GUC parameter values to parallel workers (Michael Paquier, Tom Lane)</p>"
  ],
  [
    "<p>Allow statements prepared with <tt class=\"COMMAND\">PREPARE</tt> to be given parallel plans (Amit Kapila, Tobias Bussmann)</p>"
  ],
  [
    "<p>Fix incorrect generation of parallel plans for semi-joins (Tom Lane)</p>"
  ],
  [
    "<p>Fix planner's cardinality estimates for parallel joins (Robert Haas)</p>",
    "<p>Ensure that these estimates reflect the number of rows predicted to be seen by each worker, rather than the total.</p>"
  ],
  [
    "<p>Fix planner to avoid trying to parallelize plan nodes containing initplans or subplans (Tom Lane, Amit Kapila)</p>"
  ],
  [
    "<p>Ensure that cached plans are invalidated by changes in foreign-table options (Amit Langote, Etsuro Fujita, Ashutosh Bapat)</p>"
  ],
  [
    "<p>Fix the plan generated for sorted partial aggregation with a constant <tt class=\"LITERAL\">GROUP BY</tt> clause (Tom Lane)</p>"
  ],
  [
    "<p>Fix <span class=\"QUOTE\">\"could not find plan for CTE\"</span> planner error when dealing with a <tt class=\"LITERAL\">UNION ALL</tt> containing CTE references (Tom Lane)</p>"
  ],
  [
    "<p>Fix mishandling of initplans when forcibly adding a Material node to a subplan (Tom Lane)</p>",
    "<p>The typical consequence of this mistake was a <span class=\"QUOTE\">\"plan should not reference subplan's variable\"</span> error.</p>"
  ],
  [
    "<p>Fix foreign-key-based join selectivity estimation for semi-joins and anti-joins, as well as inheritance cases (Tom Lane)</p>",
    "<p>The new code for taking the existence of a foreign key relationship into account did the wrong thing in these cases, making the estimates worse not better than the pre-9.6 code.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_dump</span> to emit the data of a sequence that is marked as an extension configuration table (Michael Paquier)</p>"
  ],
  [
    "<p>Fix mishandling of <tt class=\"COMMAND\">ALTER DEFAULT PRIVILEGES ... REVOKE</tt> in <span class=\"APPLICATION\">pg_dump</span> (Stephen Frost)</p>",
    "<p><span class=\"APPLICATION\">pg_dump</span> missed issuing the required <tt class=\"LITERAL\">REVOKE</tt> commands in cases where <tt class=\"COMMAND\">ALTER DEFAULT PRIVILEGES</tt> had been used to reduce privileges to less than they would normally be.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_dump</span> to dump user-defined casts and transforms that use built-in functions (Stephen Frost)</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_restore</span> with <tt class=\"OPTION\">--create --if-exists</tt> to behave more sanely if an archive contains unrecognized <tt class=\"COMMAND\">DROP</tt> commands (Tom Lane)</p>",
    "<p>This doesn't fix any live bug, but it may improve the behavior in future if <span class=\"APPLICATION\">pg_restore</span> is used with an archive generated by a later <span class=\"APPLICATION\">pg_dump</span> version.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_basebackup</span>'s rate limiting in the presence of slow I/O (Antonin Houska)</p>",
    "<p>If disk I/O was transiently much slower than the specified rate limit, the calculation overflowed, effectively disabling the rate limit for the rest of the run.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_basebackup</span>'s handling of symlinked <tt class=\"FILENAME\">pg_stat_tmp</tt> and <tt class=\"FILENAME\">pg_replslot</tt> subdirectories (Magnus Hagander, Michael Paquier)</p>"
  ],
  [
    "<p>Fix possible <span class=\"APPLICATION\">pg_basebackup</span> failure on standby server when including WAL files (Amit Kapila, Robert Haas)</p>"
  ],
  [
    "<p>Improve <span class=\"APPLICATION\">initdb</span> to insert the correct platform-specific default values for the <tt class=\"REPLACEABLE c2\">xxx</tt><tt class=\"LITERAL\">_flush_after</tt> parameters into <tt class=\"FILENAME\">postgresql.conf</tt> (Fabien Coelho, Tom Lane)</p>",
    "<p>This is a cleaner way of documenting the default values than was used previously.</p>"
  ],
  [
    "<p>Fix possible mishandling of expanded arrays in domain check constraints and <tt class=\"LITERAL\">CASE</tt> execution (Tom Lane)</p>",
    "<p>It was possible for a PL/pgSQL function invoked in these contexts to modify or even delete an array value that needs to be preserved for additional operations.</p>"
  ],
  [
    "<p>Fix nested uses of PL/pgSQL functions in contexts such as domain check constraints evaluated during assignment to a PL/pgSQL variable (Tom Lane)</p>"
  ],
  [
    "<p>Ensure that the Python exception objects we create for PL/Python are properly reference-counted (Rafa de la Torre, Tom Lane)</p>",
    "<p>This avoids failures if the objects are used after a Python garbage collection cycle has occurred.</p>"
  ],
  [
    "<p>Fix PL/Tcl to support triggers on tables that have <tt class=\"LITERAL\">.tupno</tt> as a column name (Tom Lane)</p>",
    "<p>This matches the (previously undocumented) behavior of PL/Tcl's <tt class=\"COMMAND\">spi_exec</tt> and <tt class=\"COMMAND\">spi_execp</tt> commands, namely that a magic <tt class=\"LITERAL\">.tupno</tt> column is inserted only if there isn't a real column named that.</p>"
  ],
  [
    "<p>Allow DOS-style line endings in <tt class=\"FILENAME\">~/.pgpass</tt> files, even on Unix (Vik Fearing)</p>",
    "<p>This change simplifies use of the same password file across Unix and Windows machines.</p>"
  ],
  [
    "<p>Fix one-byte buffer overrun if <span class=\"APPLICATION\">ecpg</span> is given a file name that ends with a dot (Takayuki Tsunakawa)</p>"
  ],
  [
    "<p>Fix incorrect error reporting for duplicate data in <span class=\"APPLICATION\">psql</span>'s <tt class=\"COMMAND\">\\crosstabview</tt> (Tom Lane)</p>",
    "<p><span class=\"APPLICATION\">psql</span> sometimes quoted the wrong row and/or column values when complaining about multiple entries for the same crosstab cell.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">psql</span>'s tab completion for <tt class=\"COMMAND\">ALTER DEFAULT PRIVILEGES</tt> (Gilles Darold, Stephen Frost)</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">psql</span>'s tab completion for <tt class=\"COMMAND\">ALTER TABLE t ALTER c DROP ...</tt> (Kyotaro Horiguchi)</p>"
  ],
  [
    "<p>In <span class=\"APPLICATION\">psql</span>, treat an empty or all-blank setting of the <tt class=\"ENVAR\">PAGER</tt> environment variable as meaning <span class=\"QUOTE\">\"no pager\"</span> (Tom Lane)</p>",
    "<p>Previously, such a setting caused output intended for the pager to vanish entirely.</p>"
  ],
  [
    "<p>Improve <tt class=\"FILENAME\">contrib/dblink</tt>'s reporting of low-level <span class=\"APPLICATION\">libpq</span> errors, such as out-of-memory (Joe Conway)</p>"
  ],
  [
    "<p>Teach <tt class=\"FILENAME\">contrib/dblink</tt> to ignore irrelevant server options when it uses a <tt class=\"FILENAME\">contrib/postgres_fdw</tt> foreign server as the source of connection options (Corey Huinker)</p>",
    "<p>Previously, if the foreign server object had options that were not also <span class=\"APPLICATION\">libpq</span> connection options, an error occurred.</p>"
  ],
  [
    "<p>Fix portability problems in <tt class=\"FILENAME\">contrib/pageinspect</tt>'s functions for GIN indexes (Peter Eisentraut, Tom Lane)</p>"
  ],
  [
    "<p>Fix possible miss of socket read events while waiting on Windows (Amit Kapila)</p>",
    "<p>This error was harmless for most uses, but it is known to cause hangs when trying to use the pldebugger extension.</p>"
  ],
  [
    "<p>On Windows, ensure that environment variable changes are propagated to DLLs built with debug options (Christian Ullrich)</p>"
  ],
  [
    "<p>Sync our copy of the timezone library with IANA release tzcode2016j (Tom Lane)</p>",
    "<p>This fixes various issues, most notably that timezone data installation failed if the target directory didn't support hard links.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"APPLICATION\">tzdata</span> release 2016j for DST law changes in northern Cyprus (adding a new zone Asia/Famagusta), Russia (adding a new zone Europe/Saratov), Tonga, and Antarctica/Casey. Historical corrections for Italy, Kazakhstan, Malta, and Palestine. Switch to preferring numeric zone abbreviations for Tonga.</p>"
  ]
]