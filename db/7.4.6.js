[
  [
    "<p>Repair possible failure to update hint bits on\n          disk</p>",
    "<p>Under rare circumstances this oversight could lead to\n          <span class=\"quote\">&#x201C;<span class=\"quote\">could not access\n          transaction status</span>&#x201D;</span> failures, which\n          qualifies it as a potential-data-loss bug.</p>"
  ],
  [
    "<p>Ensure that hashed outer join does not miss tuples</p>",
    "<p>Very large left joins using a hash join plan could\n          fail to output unmatched left-side rows given just the\n          right data distribution.</p>"
  ],
  [
    "<p>Disallow running <span class=\"application\">pg_ctl</span> as root</p>",
    "<p>This is to guard against any possible security\n          issues.</p>"
  ],
  [
    "<p>Avoid using temp files in <code class=\"filename\">/tmp</code> in <code class=\"command\">make_oidjoins_check</code></p>",
    "<p>This has been reported as a security issue, though\n          it's hardly worthy of concern since there is no reason\n          for non-developers to use this script anyway.</p>"
  ],
  [
    "<p>Prevent forced backend shutdown from re-emitting prior\n          command result</p>",
    "<p>In rare cases, a client might think that its last\n          command had succeeded when it really had been aborted by\n          forced database shutdown.</p>"
  ],
  [
    "<p>Repair bug in <code class=\"function\">pg_stat_get_backend_idset</code></p>",
    "<p>This could lead to misbehavior in some of the\n          system-statistics views.</p>"
  ],
  [
    "<p>Fix small memory leak in postmaster</p>"
  ],
  [
    "<p>Fix <span class=\"quote\">&#x201C;<span class=\"quote\">expected\n          both swapped tables to have TOAST tables</span>&#x201D;</span>\n          bug</p>",
    "<p>This could arise in cases such as CLUSTER after ALTER\n          TABLE DROP COLUMN.</p>"
  ],
  [
    "<p>Prevent <code class=\"literal\">pg_ctl restart</code>\n          from adding <code class=\"literal\">-D</code> multiple\n          times</p>"
  ],
  [
    "<p>Fix problem with NULL values in GiST indexes</p>"
  ],
  [
    "<p><code class=\"literal\">::</code> is no longer\n          interpreted as a variable in an ECPG prepare\n          statement</p>"
  ]
]