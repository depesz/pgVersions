[
  [
    "<p>Avoid possible crash when client disconnects just before the authentication timeout expires (Benkocs Norbert Attila)</p>",
    "<p>If the timeout interrupt fired partway through the session shutdown sequence, SSL-related state would be freed twice, typically causing a crash and hence denial of service to other sessions. Experimentation shows that an unauthenticated remote attacker could trigger the bug somewhat consistently, hence treat as security issue. (CVE-2015-3165)</p>"
  ],
  [
    "<p>Improve detection of system-call failures (Noah Misch)</p>",
    "<p>Our replacement implementation of <code class=\"FUNCTION\">snprintf()</code> failed to check for errors reported by the underlying system library calls; the main case that might be missed is out-of-memory situations. In the worst case this might lead to information exposure, due to our code assuming that a buffer had been overwritten when it hadn't been. Also, there were a few places in which security-relevant calls of other system library functions did not check for failure.</p>",
    "<p>It remains possible that some calls of the <code class=\"FUNCTION\">*printf()</code> family of functions are vulnerable to information disclosure if an out-of-memory error occurs at just the wrong time. We judge the risk to not be large, but will continue analysis in this area. (CVE-2015-3166)</p>"
  ],
  [
    "<p>In <tt class=\"FILENAME\">contrib/pgcrypto</tt>, uniformly report decryption failures as <span class=\"QUOTE\">\"Wrong key or corrupt data\"</span> (Noah Misch)</p>",
    "<p>Previously, some cases of decryption with an incorrect key could report other error message texts. It has been shown that such variance in error reports can aid attackers in recovering keys from other systems. While it's unknown whether <tt class=\"FILENAME\">pgcrypto</tt>'s specific behaviors are likewise exploitable, it seems better to avoid the risk by using a one-size-fits-all message. (CVE-2015-3167)</p>"
  ],
  [
    "<p>Protect against wraparound of multixact member IDs (Álvaro Herrera, Robert Haas, Thomas Munro)</p>",
    "<p>Under certain usage patterns, the existing defenses against this might be insufficient, allowing <tt class=\"FILENAME\">pg_multixact/members</tt> files to be removed too early, resulting in data loss. The fix for this includes modifying the server to fail transactions that would result in overwriting old multixact member ID data, and improving autovacuum to ensure it will act proactively to prevent multixact member ID wraparound, as it does for transaction ID wraparound.</p>"
  ],
  [
    "<p>Fix incorrect declaration of <tt class=\"FILENAME\">contrib/citext</tt>'s <code class=\"FUNCTION\">regexp_matches()</code> functions (Tom Lane)</p>",
    "<p>These functions should return <tt class=\"TYPE\">setof text[]</tt>, like the core functions they are wrappers for; but they were incorrectly declared as returning just <tt class=\"TYPE\">text[]</tt>. This mistake had two results: first, if there was no match you got a scalar null result, whereas what you should get is an empty set (zero rows). Second, the <tt class=\"LITERAL\">g</tt> flag was effectively ignored, since you would get only one result array even if there were multiple matches.</p>",
    "<p>While the latter behavior is clearly a bug, there might be applications depending on the former behavior; therefore the function declarations will not be changed by default until <span class=\"PRODUCTNAME\">PostgreSQL</span> 9.5. In pre-9.5 branches, the old behavior exists in version 1.0 of the <tt class=\"LITERAL\">citext</tt> extension, while we have provided corrected declarations in version 1.1 (which is <span class=\"emphasis EMPHASIS c2\">not</span> installed by default). To adopt the fix in pre-9.5 branches, execute <tt class=\"LITERAL\">ALTER EXTENSION citext UPDATE TO '1.1'</tt> in each database in which <tt class=\"LITERAL\">citext</tt> is installed. (You can also <span class=\"QUOTE\">\"update\"</span> back to 1.0 if you need to undo that.) Be aware that either update direction will require dropping and recreating any views or rules that use <tt class=\"FILENAME\">citext</tt>'s <code class=\"FUNCTION\">regexp_matches()</code> functions.</p>"
  ],
  [
    "<p>Render infinite dates and timestamps as <tt class=\"LITERAL\">infinity</tt> when converting to <tt class=\"TYPE\">json</tt>, rather than throwing an error (Andrew Dunstan)</p>"
  ],
  [
    "<p>Fix <tt class=\"TYPE\">json</tt>/<tt class=\"TYPE\">jsonb</tt>'s <code class=\"FUNCTION\">populate_record()</code> and <code class=\"FUNCTION\">to_record()</code> functions to handle empty input properly (Andrew Dunstan)</p>"
  ],
  [
    "<p>Fix incorrect checking of deferred exclusion constraints after a HOT update (Tom Lane)</p>",
    "<p>If a new row that potentially violates a deferred exclusion constraint is HOT-updated (that is, no indexed columns change and the row can be stored back onto the same table page) later in the same transaction, the exclusion constraint would be reported as violated when the check finally occurred, even if the row(s) the new row originally conflicted with had been deleted.</p>"
  ],
  [
    "<p>Fix behavior when changing foreign key constraint deferrability status with <tt class=\"LITERAL\">ALTER TABLE ... ALTER CONSTRAINT</tt> (Tom Lane)</p>",
    "<p>Operations later in the same session or concurrent sessions might not honor the status change promptly.</p>"
  ],
  [
    "<p>Fix planning of star-schema-style queries (Tom Lane)</p>",
    "<p>Sometimes, efficient scanning of a large table requires that index parameters be provided from more than one other table (commonly, dimension tables whose keys are needed to index a large fact table). The planner should be able to find such plans, but an overly restrictive search heuristic prevented it.</p>"
  ],
  [
    "<p>Prevent improper reordering of antijoins (NOT EXISTS joins) versus other outer joins (Tom Lane)</p>",
    "<p>This oversight in the planner has been observed to cause <span class=\"QUOTE\">\"could not find RelOptInfo for given relids\"</span> errors, but it seems possible that sometimes an incorrect query plan might get past that consistency check and result in silently-wrong query output.</p>"
  ],
  [
    "<p>Fix incorrect matching of subexpressions in outer-join plan nodes (Tom Lane)</p>",
    "<p>Previously, if textually identical non-strict subexpressions were used both above and below an outer join, the planner might try to re-use the value computed below the join, which would be incorrect because the executor would force the value to NULL in case of an unmatched outer row.</p>"
  ],
  [
    "<p>Fix GEQO planner to cope with failure of its join order heuristic (Tom Lane)</p>",
    "<p>This oversight has been seen to lead to <span class=\"QUOTE\">\"failed to join all relations together\"</span> errors in queries involving <tt class=\"LITERAL\">LATERAL</tt>, and that might happen in other cases as well.</p>"
  ],
  [
    "<p>Ensure that row locking occurs properly when the target of an <tt class=\"COMMAND\">UPDATE</tt> or <tt class=\"COMMAND\">DELETE</tt> is a security-barrier view (Stephen Frost)</p>"
  ],
  [
    "<p>Use a file opened for read/write when syncing replication slot data during database startup (Andres Freund)</p>",
    "<p>On some platforms, the previous coding could result in errors like <span class=\"QUOTE\">\"could not fsync file \"pg_replslot/...\": Bad file descriptor\"</span>.</p>"
  ],
  [
    "<p>Fix possible deadlock at startup when <tt class=\"LITERAL\">max_prepared_transactions</tt> is too small (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Don't archive useless preallocated WAL files after a timeline switch (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Recursively <code class=\"FUNCTION\">fsync()</code> the data directory after a crash (Abhijit Menon-Sen, Robert Haas)</p>",
    "<p>This ensures consistency if another crash occurs shortly later. (The second crash would have to be a system-level crash, not just a database crash, for there to be a problem.)</p>"
  ],
  [
    "<p>Fix autovacuum launcher's possible failure to shut down, if an error occurs after it receives SIGTERM (Álvaro Herrera)</p>"
  ],
  [
    "<p>Fix failure to handle invalidation messages for system catalogs early in session startup (Tom Lane)</p>",
    "<p>This oversight could result in failures in sessions that start concurrently with a <tt class=\"COMMAND\">VACUUM FULL</tt> on a system catalog.</p>"
  ],
  [
    "<p>Fix crash in <code class=\"FUNCTION\">BackendIdGetTransactionIds()</code> when trying to get status for a backend process that just exited (Tom Lane)</p>"
  ],
  [
    "<p>Cope with unexpected signals in <code class=\"FUNCTION\">LockBufferForCleanup()</code> (Andres Freund)</p>",
    "<p>This oversight could result in spurious errors about <span class=\"QUOTE\">\"multiple backends attempting to wait for pincount 1\"</span>.</p>"
  ],
  [
    "<p>Fix crash when doing <tt class=\"LITERAL\">COPY IN</tt> to a table with check constraints that contain whole-row references (Tom Lane)</p>",
    "<p>The known failure case only crashes in 9.4 and up, but there is very similar code in 9.3 and 9.2, so back-patch those branches as well.</p>"
  ],
  [
    "<p>Avoid waiting for WAL flush or synchronous replication during commit of a transaction that was read-only so far as the user is concerned (Andres Freund)</p>",
    "<p>Previously, a delay could occur at commit in transactions that had written WAL due to HOT page pruning, leading to undesirable effects such as sessions getting stuck at startup if all synchronous replicas are down. Sessions have also been observed to get stuck in catchup interrupt processing when using synchronous replication; this will fix that problem as well.</p>"
  ],
  [
    "<p>Avoid busy-waiting with short <tt class=\"LITERAL\">recovery_min_apply_delay</tt> values (Andres Freund)</p>"
  ],
  [
    "<p>Fix crash when manipulating hash indexes on temporary tables (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Fix possible failure during hash index bucket split, if other processes are modifying the index concurrently (Tom Lane)</p>"
  ],
  [
    "<p>Fix memory leaks in GIN index vacuum (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Check for interrupts while analyzing index expressions (Jeff Janes)</p>",
    "<p><tt class=\"COMMAND\">ANALYZE</tt> executes index expressions many times; if there are slow functions in such an expression, it's desirable to be able to cancel the <tt class=\"COMMAND\">ANALYZE</tt> before that loop finishes.</p>"
  ],
  [
    "<p>Ensure <tt class=\"STRUCTFIELD\">tableoid</tt> of a foreign table is reported correctly when a <tt class=\"LITERAL\">READ COMMITTED</tt> recheck occurs after locking rows in <tt class=\"COMMAND\">SELECT FOR UPDATE</tt>, <tt class=\"COMMAND\">UPDATE</tt>, or <tt class=\"COMMAND\">DELETE</tt> (Etsuro Fujita)</p>"
  ],
  [
    "<p>Add the name of the target server to object description strings for foreign-server user mappings (Álvaro Herrera)</p>"
  ],
  [
    "<p>Include the schema name in object identity strings for conversions (Álvaro Herrera)</p>"
  ],
  [
    "<p>Recommend setting <tt class=\"LITERAL\">include_realm</tt> to 1 when using Kerberos/GSSAPI/SSPI authentication (Stephen Frost)</p>",
    "<p>Without this, identically-named users from different realms cannot be distinguished. For the moment this is only a documentation change, but it will become the default setting in <span class=\"PRODUCTNAME\">PostgreSQL</span> 9.5.</p>"
  ],
  [
    "<p>Remove code for matching IPv4 <tt class=\"FILENAME\">pg_hba.conf</tt> entries to IPv4-in-IPv6 addresses (Tom Lane)</p>",
    "<p>This hack was added in 2003 in response to a report that some Linux kernels of the time would report IPv4 connections as having IPv4-in-IPv6 addresses. However, the logic was accidentally broken in 9.0. The lack of any field complaints since then shows that it's not needed anymore. Now we have reports that the broken code causes crashes on some systems, so let's just remove it rather than fix it. (Had we chosen to fix it, that would make for a subtle and potentially security-sensitive change in the effective meaning of IPv4 <tt class=\"FILENAME\">pg_hba.conf</tt> entries, which does not seem like a good thing to do in minor releases.)</p>"
  ],
  [
    "<p>Fix status reporting for terminated background workers that were never actually started (Robert Haas)</p>"
  ],
  [
    "<p>After a database crash, don't restart background workers that are marked <tt class=\"LITERAL\">BGW_NEVER_RESTART</tt> (Amit Khandekar)</p>"
  ],
  [
    "<p>Report WAL flush, not insert, position in <tt class=\"LITERAL\">IDENTIFY_SYSTEM</tt> replication command (Heikki Linnakangas)</p>",
    "<p>This avoids a possible startup failure in <span class=\"APPLICATION\">pg_receivexlog</span>.</p>"
  ],
  [
    "<p>While shutting down service on Windows, periodically send status updates to the Service Control Manager to prevent it from killing the service too soon; and ensure that <span class=\"APPLICATION\">pg_ctl</span> will wait for shutdown (Krystian Bigaj)</p>"
  ],
  [
    "<p>Reduce risk of network deadlock when using <span class=\"APPLICATION\">libpq</span>'s non-blocking mode (Heikki Linnakangas)</p>",
    "<p>When sending large volumes of data, it's important to drain the input buffer every so often, in case the server has sent enough response data to cause it to block on output. (A typical scenario is that the server is sending a stream of NOTICE messages during <tt class=\"LITERAL\">COPY FROM STDIN</tt>.) This worked properly in the normal blocking mode, but not so much in non-blocking mode. We've modified <span class=\"APPLICATION\">libpq</span> to opportunistically drain input when it can, but a full defense against this problem requires application cooperation: the application should watch for socket read-ready as well as write-ready conditions, and be sure to call <code class=\"FUNCTION\">PQconsumeInput()</code> upon read-ready.</p>"
  ],
  [
    "<p>In <span class=\"APPLICATION\">libpq</span>, fix misparsing of empty values in URI connection strings (Thomas Fanghaenel)</p>"
  ],
  [
    "<p>Fix array handling in <span class=\"APPLICATION\">ecpg</span> (Michael Meskes)</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">psql</span> to sanely handle URIs and conninfo strings as the first parameter to <tt class=\"COMMAND\">\\connect</tt> (David Fetter, Andrew Dunstan, Álvaro Herrera)</p>",
    "<p>This syntax has been accepted (but undocumented) for a long time, but previously some parameters might be taken from the old connection instead of the given string, which was agreed to be undesirable.</p>"
  ],
  [
    "<p>Suppress incorrect complaints from <span class=\"APPLICATION\">psql</span> on some platforms that it failed to write <tt class=\"FILENAME\">~/.psql_history</tt> at exit (Tom Lane)</p>",
    "<p>This misbehavior was caused by a workaround for a bug in very old (pre-2006) versions of <span class=\"APPLICATION\">libedit</span>. We fixed it by removing the workaround, which will cause a similar failure to appear for anyone still using such versions of <span class=\"APPLICATION\">libedit</span>. Recommendation: upgrade that library, or use <span class=\"APPLICATION\">libreadline</span>.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_dump</span>'s rule for deciding which casts are system-provided casts that should not be dumped (Tom Lane)</p>"
  ],
  [
    "<p>In <span class=\"APPLICATION\">pg_dump</span>, fix failure to honor <tt class=\"LITERAL\">-Z</tt> compression level option together with <tt class=\"LITERAL\">-Fd</tt> (Michael Paquier)</p>"
  ],
  [
    "<p>Make <span class=\"APPLICATION\">pg_dump</span> consider foreign key relationships between extension configuration tables while choosing dump order (Gilles Darold, Michael Paquier, Stephen Frost)</p>",
    "<p>This oversight could result in producing dumps that fail to reload because foreign key constraints are transiently violated.</p>"
  ],
  [
    "<p>Avoid possible <span class=\"APPLICATION\">pg_dump</span> failure when concurrent sessions are creating and dropping temporary functions (Tom Lane)</p>"
  ],
  [
    "<p>Fix dumping of views that are just <tt class=\"LITERAL\">VALUES(...)</tt> but have column aliases (Tom Lane)</p>"
  ],
  [
    "<p>Ensure that a view's replication identity is correctly set to <tt class=\"LITERAL\">nothing</tt> during dump/restore (Marko Tiikkaja)</p>",
    "<p>Previously, if the view was involved in a circular dependency, it might wind up with an incorrect replication identity property.</p>"
  ],
  [
    "<p>In <span class=\"APPLICATION\">pg_upgrade</span>, force timeline 1 in the new cluster (Bruce Momjian)</p>",
    "<p>This change prevents upgrade failures caused by bogus complaints about missing WAL history files.</p>"
  ],
  [
    "<p>In <span class=\"APPLICATION\">pg_upgrade</span>, check for improperly non-connectable databases before proceeding (Bruce Momjian)</p>"
  ],
  [
    "<p>In <span class=\"APPLICATION\">pg_upgrade</span>, quote directory paths properly in the generated <tt class=\"LITERAL\">delete_old_cluster</tt> script (Bruce Momjian)</p>"
  ],
  [
    "<p>In <span class=\"APPLICATION\">pg_upgrade</span>, preserve database-level freezing info properly (Bruce Momjian)</p>",
    "<p>This oversight could cause missing-clog-file errors for tables within the <tt class=\"LITERAL\">postgres</tt> and <tt class=\"LITERAL\">template1</tt> databases.</p>"
  ],
  [
    "<p>Run <span class=\"APPLICATION\">pg_upgrade</span> and <span class=\"APPLICATION\">pg_resetxlog</span> with restricted privileges on Windows, so that they don't fail when run by an administrator (Muhammad Asif Naeem)</p>"
  ],
  [
    "<p>Improve handling of <code class=\"FUNCTION\">readdir()</code> failures when scanning directories in <span class=\"APPLICATION\">initdb</span> and <span class=\"APPLICATION\">pg_basebackup</span> (Marco Nenciarini)</p>"
  ],
  [
    "<p>Fix slow sorting algorithm in <tt class=\"FILENAME\">contrib/intarray</tt> (Tom Lane)</p>"
  ],
  [
    "<p>Fix compile failure on Sparc V8 machines (Rob Rowan)</p>"
  ],
  [
    "<p>Silence some build warnings on macOS (Tom Lane)</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"APPLICATION\">tzdata</span> release 2015d for DST law changes in Egypt, Mongolia, and Palestine, plus historical changes in Canada and Chile. Also adopt revised zone abbreviations for the America/Adak zone (HST/HDT not HAST/HADT).</p>"
  ]
]