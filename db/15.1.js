[
  [
    "<p>Fix failure to remove non-first segments of large tables (Tom Lane)</p>",
    "<p><span class=\"productname\">PostgreSQL</span> splits large tables into multiple files (normally with 1GB per file). The logic for dropping a table was broken and would miss removing all but the first such file, in two cases: drops of temporary tables and WAL replay of drops of regular tables. Applications that routinely create multi-gigabyte temporary tables could suffer significant disk space leakage.</p>",
    "<p>Orphaned temporary-table files are removed during postmaster start, so the mere act of updating to 15.1 is sufficient to clear any leaked temporary-table storage. However, if you suffered any database crashes while using 15.0, and there might have been large tables dropped just before such crashes, it's advisable to check the database directories for files named according to the pattern <code class=\"literal\"><em class=\"replaceable\"><code>NNNN</code></em>.<em class=\"replaceable\"><code>NN</code></em></code>. If there is no matching file named just <code class=\"literal\"><em class=\"replaceable\"><code>NNNN</code></em></code> (without the <code class=\"literal\">.<em class=\"replaceable\"><code>NN</code></em></code> suffix), these files should be removed manually.</p>"
  ],
  [
    "<p>Fix handling of <code class=\"literal\">DEFAULT</code> tokens that appear in a multi-row <code class=\"literal\">VALUES</code> clause of an <code class=\"command\">INSERT</code> on an updatable view (Tom Lane)</p>",
    "<p>This oversight could lead to <span class=\"quote\">“<span class=\"quote\">cache lookup failed for type</span>”</span> errors, or in older branches even to crashes.</p>"
  ],
  [
    "<p>Disallow rules named <code class=\"literal\">_RETURN</code> that are not <code class=\"literal\">ON SELECT</code> (Tom Lane)</p>",
    "<p>This avoids confusion between a view's <code class=\"literal\">ON SELECT</code> rule and any other rules it may have.</p>"
  ],
  [
    "<p>Avoid failure in <code class=\"command\">EXPLAIN VERBOSE</code> for a query using <code class=\"literal\">SEARCH BREADTH FIRST</code> with constant initial values (Tom Lane)</p>"
  ],
  [
    "<p>Prevent use of <code class=\"command\">MERGE</code> on a partitioned table with foreign-table partitions (Álvaro Herrera)</p>",
    "<p>The case isn't supported, and previously threw an incomprehensible error.</p>"
  ],
  [
    "<p>Fix construction of per-partition foreign key constraints while doing <code class=\"command\">ALTER TABLE ATTACH PARTITION</code> (Jehan-Guillaume de Rorthais, Álvaro Herrera)</p>",
    "<p>Previously, incorrect or duplicate constraints could be constructed for the newly-added partition.</p>"
  ],
  [
    "<p>Fix planner failure with extended statistics on partitioned or inherited tables (Richard Guo, Justin Pryzby)</p>",
    "<p>Some cases failed with <span class=\"quote\">“<span class=\"quote\">cache lookup failed for statistics object</span>”</span>.</p>"
  ],
  [
    "<p>Fix mis-ordering of WAL operations in fast insert path for GIN indexes (Matthias van de Meent, Zhang Mingli)</p>",
    "<p>This mistake is not known to have any negative consequences within core <span class=\"productname\">PostgreSQL</span>, but it did cause issues for some extensions.</p>"
  ],
  [
    "<p>Fix bugs in logical decoding when replay starts from a point between the beginning of a transaction and the beginning of its subtransaction (Masahiko Sawada, Kuroda Hayato)</p>",
    "<p>These errors could lead to assertion failures in debug builds, and otherwise to memory leaks.</p>"
  ],
  [
    "<p>Accept interrupts in more places during logical decoding (Amit Kapila, Masahiko Sawada)</p>",
    "<p>This ameliorates problems with slow shutdown of replication workers.</p>"
  ],
  [
    "<p>Prevent attempts to replicate into a foreign-table partition in replication workers (Shi Yu, Tom Lane)</p>",
    "<p>Although partitioned tables can have foreign tables as partitions, replicating into such a partition isn't currently supported. The logical replication worker process would crash if it was attempted. Now, an error is thrown.</p>"
  ],
  [
    "<p>Avoid crash after function syntax error in replication workers (Maxim Orlov, Anton Melnikov, Masahiko Sawada, Tom Lane)</p>",
    "<p>If a syntax error occurred in a SQL-language or PL/pgSQL-language <code class=\"command\">CREATE FUNCTION</code> or <code class=\"command\">DO</code> command executed in a logical replication worker, the worker process would crash with a null pointer dereference or assertion failure.</p>"
  ],
  [
    "<p>Avoid double call of the shutdown callback of an archiver module (Nathan Bossart, Bharath Rupireddy)</p>"
  ],
  [
    "<p>Add plan-time check for attempted access to a table that has no table access method (Tom Lane)</p>",
    "<p>This prevents a crash in some catalog-corruption scenarios, for example use of a view whose <code class=\"literal\">ON SELECT</code> rule is missing.</p>"
  ],
  [
    "<p>Prevent postmaster crash when shared-memory state is corrupted (Tom Lane)</p>",
    "<p>The postmaster process is supposed to survive and initiate a database restart if shared memory becomes corrupted, but one bit of code was being insufficiently cautious about that.</p>"
  ],
  [
    "<p>In <span class=\"application\">libpq</span>, handle single-row mode correctly when pipelining (Denis Laxalde)</p>",
    "<p>The single-row flag was not reset at the correct time if pipeline mode was also active.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">psql</span>'s exit status when a command-line query is canceled (Peter Eisentraut)</p>",
    "<p><code class=\"literal\">psql -c <em class=\"replaceable\"><code>query</code></em></code> would exit successfully if the query was canceled. Fix it to exit with nonzero status, as in other error cases.</p>"
  ],
  [
    "<p>Allow cross-platform tablespace relocation in <span class=\"application\">pg_basebackup</span> (Robert Haas)</p>",
    "<p>Allow the remote path in <code class=\"option\">--tablespace-mapping</code> to be either a Unix-style or Windows-style absolute path, since the source server could be on a different OS than the local system.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_dump</span>'s failure to dump comments attached to some <code class=\"literal\">CHECK</code> constraints (Tom Lane)</p>"
  ],
  [
    "<p>Fix <code class=\"command\">CREATE DATABASE</code> to allow its <code class=\"literal\">oid</code> parameter to exceed 2<sup>31</sup> (Tom Lane)</p>",
    "<p>This oversight prevented <span class=\"application\">pg_upgrade</span> from succeeding when the source installation contained databases with OIDs larger than that.</p>"
  ],
  [
    "<p>In <span class=\"application\">pg_stat_statements</span>, fix access to already-freed memory (zhaoqigui)</p>",
    "<p>This occurred if <span class=\"application\">pg_stat_statements</span> tracked a <code class=\"command\">ROLLBACK</code> command issued via extended query protocol. In debug builds it consistently led to an assertion failure. In production builds there would often be no visible ill effect; but if the freed memory had already been reused, the likely result would be to store garbage for the query string.</p>"
  ],
  [
    "<p>Fix incompatibilities with LLVM 15 (Thomas Munro, Andres Freund)</p>"
  ],
  [
    "<p>Allow use of <code class=\"function\">__sync_lock_test_and_set()</code> for spinlocks on any machine (Tom Lane)</p>",
    "<p>This eases porting to new machine architectures, at least if you're using a compiler that supports this GCC builtin function.</p>"
  ],
  [
    "<p>Rename symbol <code class=\"literal\">REF</code> to <code class=\"literal\">REF_P</code> to avoid compile failure on recent macOS (Tom Lane)</p>"
  ],
  [
    "<p>Avoid using <code class=\"function\">sprintf</code>, to avoid compile-time deprecation warnings (Tom Lane)</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2022f for DST law changes in Chile, Fiji, Iran, Jordan, Mexico, Palestine, and Syria, plus historical corrections for Chile, Crimea, Iran, and Mexico.</p>",
    "<p>Also, the Europe/Kiev zone has been renamed to Europe/Kyiv. Also, the following zones have been merged into nearby, more-populous zones whose clocks have agreed with them since 1970: Antarctica/Vostok, Asia/Brunei, Asia/Kuala_Lumpur, Atlantic/Reykjavik, Europe/Amsterdam, Europe/Copenhagen, Europe/Luxembourg, Europe/Monaco, Europe/Oslo, Europe/Stockholm, Indian/Christmas, Indian/Cocos, Indian/Kerguelen, Indian/Mahe, Indian/Reunion, Pacific/Chuuk, Pacific/Funafuti, Pacific/Majuro, Pacific/Pohnpei, Pacific/Wake and Pacific/Wallis. (This indirectly affects zones that were already links to one of these: Arctic/Longyearbyen, Atlantic/Jan_Mayen, Iceland, Pacific/Ponape, Pacific/Truk, and Pacific/Yap.) America/Nipigon, America/Rainy_River, America/Thunder_Bay, Europe/Uzhgorod, and Europe/Zaporozhye were also merged into nearby zones after discovering that their claimed post-1970 differences from those zones seem to have been errors. In all these cases, the previous zone name remains as an alias; but the actual data is that of the zone that was merged into.</p>",
    "<p>These zone mergers result in loss of pre-1970 timezone history for the merged zones, which may be troublesome for applications expecting consistency of <code class=\"type\">timestamptz</code> display. As an example, the stored value <code class=\"literal\">1944-06-01 12:00 UTC</code> would previously display as <code class=\"literal\">1944-06-01 13:00:00+01</code> if the Europe/Stockholm zone is selected, but now it will read out as <code class=\"literal\">1944-06-01 14:00:00+02</code>.</p>",
    "<p>It is possible to build the time zone data files with options that will restore the older zone data, but that choice also inserts a lot of other old (and typically poorly-attested) zone data, resulting in more total changes from the previous release than accepting these upstream changes does. <span class=\"productname\">PostgreSQL</span> has chosen to ship the <span class=\"productname\">tzdb</span> data as-recommended, and so far as we are aware most major operating system distributions are doing likewise. However, if these changes cause significant problems for your application, a possible solution is to install a local build of the time zone data files using <span class=\"productname\">tzdb</span>'s backwards-compatibility options (see their <code class=\"literal\">PACKRATDATA</code> and <code class=\"literal\">PACKRATLIST</code> options).</p>"
  ]
]