[
  [
    "<p>Enforce restrictions in <code class=\"literal\">plperl</code> using an opmask applied to the\n          whole interpreter, instead of using <code class=\"filename\">Safe.pm</code> (Tim Bunce, Andrew Dunstan)</p>",
    "<p>Recent developments have convinced us that\n          <code class=\"filename\">Safe.pm</code> is too insecure to\n          rely on for making <code class=\"literal\">plperl</code>\n          trustable. This change removes use of <code class=\"filename\">Safe.pm</code> altogether, in favor of using a\n          separate interpreter with an opcode mask that is always\n          applied. Pleasant side effects of the change include that\n          it is now possible to use Perl's <code class=\"literal\">strict</code> pragma in a natural way in\n          <code class=\"literal\">plperl</code>, and that Perl's\n          <code class=\"literal\">$a</code> and <code class=\"literal\">$b</code> variables work as expected in sort\n          routines, and that function compilation is significantly\n          faster. (CVE-2010-1169)</p>"
  ],
  [
    "<p>Prevent PL/Tcl from executing untrustworthy code from\n          <code class=\"structname\">pltcl_modules</code> (Tom)</p>",
    "<p>PL/Tcl's feature for autoloading Tcl code from a\n          database table could be exploited for trojan-horse\n          attacks, because there was no restriction on who could\n          create or insert into that table. This change disables\n          the feature unless <code class=\"structname\">pltcl_modules</code> is owned by a\n          superuser. (However, the permissions on the table are not\n          checked, so installations that really need a\n          less-than-secure modules table can still grant suitable\n          privileges to trusted non-superusers.) Also, prevent\n          loading code into the unrestricted <span class=\"quote\">&#x201C;<span class=\"quote\">normal</span>&#x201D;</span> Tcl\n          interpreter unless we are really going to execute a\n          <code class=\"literal\">pltclu</code> function.\n          (CVE-2010-1170)</p>"
  ],
  [
    "<p>Do not allow an unprivileged user to reset\n          superuser-only parameter settings (Alvaro)</p>",
    "<p>Previously, if an unprivileged user ran <code class=\"literal\">ALTER USER ... RESET ALL</code> for himself, or\n          <code class=\"literal\">ALTER DATABASE ... RESET ALL</code>\n          for a database he owns, this would remove all special\n          parameter settings for the user or database, even ones\n          that are only supposed to be changeable by a superuser.\n          Now, the <code class=\"command\">ALTER</code> will only\n          remove the parameters that the user has permission to\n          change.</p>"
  ],
  [
    "<p>Avoid possible crash during backend shutdown if\n          shutdown occurs when a <code class=\"literal\">CONTEXT</code> addition would be made to log\n          entries (Tom)</p>",
    "<p>In some cases the context-printing function would fail\n          because the current transaction had already been rolled\n          back when it came time to print a log message.</p>"
  ],
  [
    "<p>Update PL/Perl's <code class=\"filename\">ppport.h</code> for modern Perl versions\n          (Andrew)</p>"
  ],
  [
    "<p>Fix assorted memory leaks in PL/Python (Andreas\n          Freund, Tom)</p>"
  ],
  [
    "<p>Prevent infinite recursion in <span class=\"application\">psql</span> when expanding a variable that\n          refers to itself (Tom)</p>"
  ],
  [
    "<p>Ensure that <code class=\"filename\">contrib/pgstattuple</code> functions respond\n          to cancel interrupts promptly (Tatsuhito Kasahara)</p>"
  ],
  [
    "<p>Make server startup deal properly with the case that\n          <code class=\"function\">shmget()</code> returns\n          <code class=\"literal\">EINVAL</code> for an existing\n          shared memory segment (Tom)</p>",
    "<p>This behavior has been observed on BSD-derived kernels\n          including macOS. It resulted in an entirely-misleading\n          startup failure complaining that the shared memory\n          request size was too large.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2010j for DST law\n          changes in Argentina, Australian Antarctic, Bangladesh,\n          Mexico, Morocco, Pakistan, Palestine, Russia, Syria,\n          Tunisia; also historical corrections for Taiwan.</p>"
  ]
]