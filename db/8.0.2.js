[
  [
    "<p>Increment the major version number of all interface\n          libraries (Bruce)</p>",
    "<p>This should have been done in 8.0.0. It is required so\n          7.4.X versions of PostgreSQL client applications, like\n          <span class=\"application\">psql</span>, can be used on the\n          same machine as 8.0.X applications. This might require\n          re-linking user applications that use these\n          libraries.</p>"
  ],
  [
    "<p>Add Windows-only <code class=\"varname\">wal_sync_method</code> setting of <code class=\"option\">fsync_writethrough</code> (Magnus, Bruce)</p>",
    "<p>This setting causes <span class=\"productname\">PostgreSQL</span> to write through any\n          disk-drive write cache when writing to WAL. This behavior\n          was formerly called <code class=\"option\">fsync</code>,\n          but was renamed because it acts quite differently from\n          <code class=\"option\">fsync</code> on other platforms.</p>"
  ],
  [
    "<p>Enable the <code class=\"varname\">wal_sync_method</code> setting of <code class=\"option\">open_datasync</code> on Windows, and make it the\n          default for that platform (Magnus, Bruce)</p>",
    "<p>Because the default is no longer <code class=\"option\">fsync_writethrough</code>, data loss is possible\n          during a power failure if the disk drive has write\n          caching enabled. To turn off the write cache on Windows,\n          from the <span class=\"application\">Device Manager</span>,\n          choose the drive properties, then <code class=\"literal\">Policies</code>.</p>"
  ],
  [
    "<p>New cache management algorithm <acronym class=\"acronym\">2Q</acronym> replaces <acronym class=\"acronym\">ARC</acronym> (Tom)</p>",
    "<p>This was done to avoid a pending US patent on\n          <acronym class=\"acronym\">ARC</acronym>. The\n          <acronym class=\"acronym\">2Q</acronym> code might be a few\n          percentage points slower than <acronym class=\"acronym\">ARC</acronym> for some work loads. A better\n          cache management algorithm will appear in 8.1.</p>"
  ],
  [
    "<p>Planner adjustments to improve behavior on\n          freshly-created tables (Tom)</p>"
  ],
  [
    "<p>Allow plpgsql to assign to an element of an array that\n          is initially <code class=\"literal\">NULL</code> (Tom)</p>",
    "<p>Formerly the array would remain <code class=\"literal\">NULL</code>, but now it becomes a\n          single-element array. The main SQL engine was changed to\n          handle <code class=\"command\">UPDATE</code> of a null\n          array value this way in 8.0, but the similar case in\n          plpgsql was overlooked.</p>"
  ],
  [
    "<p>Convert <code class=\"literal\">\\r\\n</code> and\n          <code class=\"literal\">\\r</code> to <code class=\"literal\">\\n</code> in plpython function bodies (Michael\n          Fuhr)</p>",
    "<p>This prevents syntax errors when plpython code is\n          written on a Windows or Mac client.</p>"
  ],
  [
    "<p>Allow SPI cursors to handle utility commands that\n          return rows, such as <code class=\"command\">EXPLAIN</code>\n          (Tom)</p>"
  ],
  [
    "<p>Fix <code class=\"command\">CLUSTER</code> failure after\n          <code class=\"command\">ALTER TABLE SET WITHOUT OIDS</code>\n          (Tom)</p>"
  ],
  [
    "<p>Reduce memory usage of <code class=\"command\">ALTER\n          TABLE ADD COLUMN</code> (Neil)</p>"
  ],
  [
    "<p>Fix <code class=\"command\">ALTER LANGUAGE RENAME</code>\n          (Tom)</p>"
  ],
  [
    "<p>Document the Windows-only <code class=\"literal\">register</code> and <code class=\"literal\">unregister</code> options of <span class=\"application\">pg_ctl</span> (Magnus)</p>"
  ],
  [
    "<p>Ensure operations done during backend shutdown are\n          counted by statistics collector</p>",
    "<p>This is expected to resolve reports of <span class=\"application\">pg_autovacuum</span> not vacuuming the\n          system catalogs often enough &#x2014; it was not being told\n          about catalog deletions caused by temporary table removal\n          during backend exit.</p>"
  ],
  [
    "<p>Change the Windows default for configuration parameter\n          <code class=\"varname\">log_destination</code> to\n          <code class=\"option\">eventlog</code> (Magnus)</p>",
    "<p>By default, a server running on Windows will now send\n          log output to the Windows event logger rather than\n          standard error.</p>"
  ],
  [
    "<p>Make Kerberos authentication work on Windows\n          (Magnus)</p>"
  ],
  [
    "<p>Allow <code class=\"command\">ALTER DATABASE\n          RENAME</code> by superusers who aren't flagged as having\n          CREATEDB privilege (Tom)</p>"
  ],
  [
    "<p>Modify WAL log entries for <code class=\"command\">CREATE</code> and <code class=\"command\">DROP\n          DATABASE</code> to not specify absolute paths (Tom)</p>",
    "<p>This allows point-in-time recovery on a different\n          machine with possibly different database location. Note\n          that <code class=\"command\">CREATE TABLESPACE</code> still\n          poses a hazard in such situations.</p>"
  ],
  [
    "<p>Fix crash from a backend exiting with an open\n          transaction that created a table and opened a cursor on\n          it (Tom)</p>"
  ],
  [
    "<p>Fix <code class=\"function\">array_map()</code> so it\n          can call PL functions (Tom)</p>"
  ],
  [
    "<p>Several <code class=\"filename\">contrib/tsearch2</code>\n          and <code class=\"filename\">contrib/btree_gist</code>\n          fixes (Teodor)</p>"
  ],
  [
    "<p>Fix crash of some <code class=\"filename\">contrib/pgcrypto</code> functions on some\n          platforms (Marko Kreen)</p>"
  ],
  [
    "<p>Fix <code class=\"filename\">contrib/intagg</code> for\n          64-bit platforms (Tom)</p>"
  ],
  [
    "<p>Fix ecpg bugs in parsing of <code class=\"command\">CREATE</code> statement (Michael)</p>"
  ],
  [
    "<p>Work around gcc bug on powerpc and amd64 causing\n          problems in ecpg (Christof Petig)</p>"
  ],
  [
    "<p>Do not use locale-aware versions of <code class=\"function\">upper()</code>, <code class=\"function\">lower()</code>, and <code class=\"function\">initcap()</code> when the locale is\n          <code class=\"literal\">C</code> (Bruce)</p>",
    "<p>This allows these functions to work on platforms that\n          generate errors for non-7-bit data when the locale is\n          <code class=\"literal\">C</code>.</p>"
  ],
  [
    "<p>Fix <code class=\"function\">quote_ident()</code> to\n          quote names that match keywords (Tom)</p>"
  ],
  [
    "<p>Fix <code class=\"function\">to_date()</code> to behave\n          reasonably when <code class=\"literal\">CC</code> and\n          <code class=\"literal\">YY</code> fields are both used\n          (Karel)</p>"
  ],
  [
    "<p>Prevent <code class=\"function\">to_char(interval)</code> from failing when\n          given a zero-month interval (Tom)</p>"
  ],
  [
    "<p>Fix wrong week returned by <code class=\"function\">date_trunc('week')</code> (Bruce)</p>",
    "<p><code class=\"function\">date_trunc('week')</code>\n          returned the wrong year for the first few days of January\n          in some years.</p>"
  ],
  [
    "<p>Use the correct default mask length for class\n          <code class=\"literal\">D</code> addresses in <code class=\"type\">INET</code> data types (Tom)</p>"
  ]
]