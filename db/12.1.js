[
  [
    "<p>Fix crash when <code class=\"command\">ALTER TABLE</code> adds a column without a default value along with making other changes that require a table rewrite (Andres Freund)</p>"
  ],
  [
    "<p>Fix lock handling in <code class=\"command\">REINDEX CONCURRENTLY</code> (Michael Paquier)</p>",
    "<p><code class=\"command\">REINDEX CONCURRENTLY</code> neglected to take a session-level lock on the new index version, potentially allowing other sessions to manipulate it too soon. Also, a query-cancel or session-termination interrupt arriving at the wrong time could result in failure to release the session-level locks that <code class=\"command\">REINDEX CONCURRENTLY</code> does hold.</p>"
  ],
  [
    "<p>Avoid crash due to race condition when reporting the progress of a <code class=\"command\">CREATE INDEX CONCURRENTLY</code> or <code class=\"command\">REINDEX CONCURRENTLY</code> command (Álvaro Herrera)</p>"
  ],
  [
    "<p>Avoid creating duplicate dependency entries during <code class=\"command\">REINDEX CONCURRENTLY</code> (Michael Paquier)</p>",
    "<p>This bug resulted in bloat in <code class=\"structname\">pg_depend</code>, but no worse consequences than that.</p>"
  ],
  [
    "<p>Prevent <code class=\"command\">VACUUM</code> from trying to freeze an old multixact ID involving a still-running transaction (Nathan Bossart, Jeremy Schneider)</p>",
    "<p>This case would lead to <code class=\"command\">VACUUM</code> failing until the old transaction terminates.</p>"
  ],
  [
    "<p>Fix <span class=\"quote\">“<span class=\"quote\">wrong type of slot</span>”</span> error when trying to <code class=\"command\">CLUSTER</code> on an expression index (Andres Freund)</p>"
  ],
  [
    "<p><code class=\"command\">SET CONSTRAINTS ... DEFERRED</code> failed on partitioned tables, incorrectly complaining about lack of triggers (Álvaro Herrera)</p>"
  ],
  [
    "<p>Fix failure when creating indexes for a partition, if the parent partitioned table contains any dropped columns (Michael Paquier)</p>"
  ],
  [
    "<p>Fix dropping of indexed columns in partitioned tables (Amit Langote, Michael Paquier)</p>",
    "<p>Previously this might fail with an error message complaining about the dependencies of the indexes. It should automatically drop the indexes, instead.</p>"
  ],
  [
    "<p>Ensure that a partition index can be dropped after a failure to reindex it concurrently (Michael Paquier)</p>",
    "<p>The index's <code class=\"structname\">pg_class</code>.<code class=\"structfield\">relispartition</code> flag was left in the wrong state in such a case, causing <code class=\"command\">DROP INDEX</code> to fail.</p>"
  ],
  [
    "<p>Fix handling of equivalence class members for partition-wise joins (Amit Langote)</p>",
    "<p>This oversight could lead either to failure to use a feasible partition-wise join plan, or to a <span class=\"quote\">“<span class=\"quote\">could not find pathkey item to sort</span>”</span> planner failure.</p>"
  ],
  [
    "<p>Ensure that offset expressions in <code class=\"literal\">WINDOW</code> clauses are processed when a query's expressions are manipulated (Andrew Gierth)</p>",
    "<p>This oversight could result in assorted failures when the offsets are nontrivial expressions. One example is that a function parameter reference in such an expression would fail if the function was inlined.</p>"
  ],
  [
    "<p>Avoid postmaster failure if a parallel query requests a background worker when no postmaster child process array slots remain free (Tom Lane)</p>"
  ],
  [
    "<p>Fix crash triggered by an EvalPlanQual recheck on a table with a <code class=\"literal\">BEFORE UPDATE</code> trigger (Andres Freund)</p>"
  ],
  [
    "<p>Fix <span class=\"quote\">“<span class=\"quote\">unexpected relkind</span>”</span> error when a query tries to access a TOAST table (John Hsu, Michael Paquier, Tom Lane)</p>",
    "<p>The error should say that permission is denied, but this case got broken during code refactoring.</p>"
  ],
  [
    "<p>Provide a relevant error context line when an error occurs while setting GUC parameters during parallel worker startup (Thomas Munro)</p>"
  ],
  [
    "<p>Ensure that <code class=\"function\">fsync()</code> is applied only to files that are opened read/write (Andres Freund, Michael Paquier)</p>",
    "<p>Some code paths tried to do this after opening a file read-only, but on some platforms that causes <span class=\"quote\">“<span class=\"quote\">bad file descriptor</span>”</span> or similar errors.</p>"
  ],
  [
    "<p>Allow encoding conversion to succeed on longer strings than before (Álvaro Herrera, Tom Lane)</p>",
    "<p>Previously, there was a hard limit of 0.25GB on the input string, but now it will work as long as the converted output is not over 1GB.</p>"
  ],
  [
    "<p>Avoid creating unnecessarily-bulky tuple stores for window functions (Andrew Gierth)</p>",
    "<p>In some cases the tuple storage would include all columns of the source table(s), not just the ones that are needed by the query.</p>"
  ],
  [
    "<p>Allow <code class=\"function\">repalloc()</code> to give back space when a large chunk is reduced in size (Tom Lane)</p>"
  ],
  [
    "<p>Ensure that temporary WAL and history files are removed at the end of archive recovery (Sawada Masahiko)</p>"
  ],
  [
    "<p>Avoid failure in archive recovery if <code class=\"varname\">recovery_min_apply_delay</code> is enabled (Fujii Masao)</p>",
    "<p><code class=\"varname\">recovery_min_apply_delay</code> is not typically used in this configuration, but it should work.</p>"
  ],
  [
    "<p>Ignore <code class=\"varname\">restore_command</code>, <code class=\"varname\">recovery_end_command</code>, and <code class=\"varname\">recovery_min_apply_delay</code> settings during crash recovery (Fujii Masao)</p>",
    "<p>Now that these settings can be specified in <code class=\"filename\">postgresql.conf</code>, they could be turned on during crash recovery, but honoring them then is undesirable. Ignore these settings until crash recovery is complete.</p>"
  ],
  [
    "<p>Fix logical replication failure when publisher and subscriber have different ideas about a table's replica identity columns (Jehan-Guillaume de Rorthais, Peter Eisentraut)</p>",
    "<p>Declaring a column as part of the replica identity on the subscriber, when it does not exist at all on the publisher, led to <span class=\"quote\">“<span class=\"quote\">negative bitmapset member not allowed</span>”</span> errors.</p>"
  ],
  [
    "<p>Avoid unwanted delay during shutdown of a logical replication walsender (Craig Ringer, Álvaro Herrera)</p>"
  ],
  [
    "<p>Fix timeout handling in logical replication walreceiver processes (Julien Rouhaud)</p>",
    "<p>Erroneous logic prevented <code class=\"varname\">wal_receiver_timeout</code> from working in logical replication deployments.</p>"
  ],
  [
    "<p>Correctly time-stamp replication messages for logical decoding (Jeff Janes)</p>",
    "<p>This oversight resulted, for example, in <code class=\"structname\">pg_stat_subscription</code>.<code class=\"structfield\">last_msg_send_time</code> usually reading as NULL.</p>"
  ],
  [
    "<p>Fix race condition during backend exit, when the backend process has previously waited for synchronous replication to occur (Dongming Liu)</p>"
  ],
  [
    "<p>Avoid logging complaints about abandoned connections when using PAM authentication (Tom Lane)</p>",
    "<p>libpq-based clients will typically make two connection attempts when a password is required, since they don't prompt their user for a password until their first connection attempt fails. Therefore the server is coded not to generate useless log spam when a client closes the connection upon being asked for a password. However, the PAM authentication code hadn't gotten that memo, and would generate several messages about a phantom authentication failure.</p>"
  ],
  [
    "<p>Fix misbehavior of <code class=\"function\">bitshiftright()</code> (Tom Lane)</p>",
    "<p>The bitstring right shift operator failed to zero out padding space that exists in the last byte of the result when the bitstring length is not a multiple of 8. While invisible to most operations, any nonzero bits there would result in unexpected comparison behavior, since bitstring comparisons don't bother to ignore the extra bits, expecting them to always be zero.</p>",
    "<p>If you have inconsistent data as a result of saving the output of <code class=\"function\">bitshiftright()</code> in a table, it's possible to fix it with something like</p>",
    "<pre class=\"programlisting\">\nUPDATE mytab SET bitcol = ~(~bitcol) WHERE bitcol != ~(~bitcol);\n</pre>"
  ],
  [
    "<p>Fix result of text <code class=\"function\">position()</code> function (also known as <code class=\"function\">strpos()</code>) for an empty search string (Tom Lane)</p>",
    "<p>Historically, and per the SQL standard, the result should be one in such cases, but 12.0 returned zero.</p>"
  ],
  [
    "<p>Fix detection of edge-case integer overflow in interval multiplication (Yuya Watari)</p>"
  ],
  [
    "<p>Avoid crashes if <code class=\"literal\">ispell</code> text search dictionaries contain wrong affix data (Arthur Zakirov)</p>"
  ],
  [
    "<p>Avoid memory leak while vacuuming a GiST index (Dilip Kumar)</p>"
  ],
  [
    "<p>On Windows, recognize additional spellings of the <span class=\"quote\">“<span class=\"quote\">Norwegian (Bokmål)</span>”</span> locale name (Tom Lane)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">libpq</span> to allow trailing whitespace in the string values of integer parameters (Michael Paquier)</p>",
    "<p>Version 12 tightened <span class=\"application\">libpq</span>'s validation of integer parameters, but disallowing trailing whitespace seems undesirable.</p>"
  ],
  [
    "<p>In <span class=\"application\">libpq</span>, correctly report <code class=\"literal\">CONNECTION_BAD</code> connection status after a failure caused by a syntactically invalid <code class=\"option\">connect_timeout</code> parameter value (Lars Kanis)</p>"
  ],
  [
    "<p>Avoid compile failure if an ECPG client includes <code class=\"filename\">ecpglib.h</code> while having <code class=\"literal\">ENABLE_NLS</code> defined (Tom Lane)</p>",
    "<p>This risk was created by a misplaced declaration: <code class=\"function\">ecpg_gettext()</code> should not be visible to client code.</p>"
  ],
  [
    "<p>Fix scheduling of parallel restore of a foreign key constraint on a partitioned table (Álvaro Herrera)</p>",
    "<p><span class=\"application\">pg_dump</span> failed to emit full dependency information for partitioned tables' foreign keys. This could allow parallel <span class=\"application\">pg_restore</span> to try to recreate a foreign key constraint too soon.</p>"
  ],
  [
    "<p>In <span class=\"application\">pg_dump</span>, ensure stable output order for similarly-named triggers and row-level-security policy objects (Benjie Gillam)</p>",
    "<p>Previously, if two triggers on different tables had the same names, they would be sorted in OID-based order, which is less desirable than sorting them by table name. Likewise for RLS policies.</p>"
  ],
  [
    "<p>In <span class=\"application\">pg_upgrade</span>, reject tables with columns of type <code class=\"type\">sql_identifier</code>, as that has changed representation in version 12 (Tomas Vondra)</p>"
  ],
  [
    "<p>Improve <span class=\"application\">pg_upgrade</span>'s checks for the use of a data type that has changed representation, such as <code class=\"type\">line</code> (Tomas Vondra)</p>",
    "<p>The previous coding could be fooled by cases where the data type of interest underlies a stored column of a domain or composite type.</p>"
  ],
  [
    "<p>In <span class=\"application\">pg_rewind</span> with the <code class=\"option\">--dry-run</code> option, avoid updating <code class=\"filename\">pg_control</code> (Alexey Kondratov)</p>",
    "<p>This could lead to failures in subsequent <span class=\"application\">pg_rewind</span> attempts.</p>"
  ],
  [
    "<p>Fix failure in <span class=\"application\">pg_waldump</span> with the <code class=\"option\">-s</code> option, when a continuation WAL record ends exactly at a page boundary (Andrey Lepikhov)</p>"
  ],
  [
    "<p>In <span class=\"application\">pg_waldump</span> with the <code class=\"option\">--bkp-details</code> option, avoid emitting extra newlines for WAL records involving full-page writes (Andres Freund)</p>"
  ],
  [
    "<p>Fix small memory leak in <span class=\"application\">pg_waldump</span> (Andres Freund)</p>"
  ],
  [
    "<p>Put back <code class=\"function\">pqsignal()</code> as an exported <span class=\"application\">libpq</span> symbol (Tom Lane)</p>",
    "<p>This function was removed on the grounds that no clients should be using it, but that turns out to break usage of current <span class=\"application\">libpq</span> with very old versions of <span class=\"application\">psql</span>, and perhaps other applications.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">configure</span>'s test for presence of libperl so that it works on recent Red Hat releases (Tom Lane)</p>",
    "<p>Previously, it could fail if the user sets <code class=\"literal\">CFLAGS</code> to <code class=\"literal\">-O0</code>.</p>"
  ],
  [
    "<p>Ensure correct code generation for spinlocks on PowerPC (Noah Misch)</p>",
    "<p>The previous spinlock coding allowed the compiler to select register zero for use with an assembly instruction that does not accept that register, causing a build failure. We have seen only one long-ago report that matches this bug, but it could cause problems for people trying to build modified <span class=\"productname\">PostgreSQL</span> code or use atypical compiler options.</p>"
  ],
  [
    "<p>On AIX, don't use the compiler option <code class=\"option\">-qsrcmsg</code> (Noah Misch)</p>",
    "<p>This avoids an internal compiler error with xlc v16.1.0, with little consequence other than changing the format of compiler error messages.</p>"
  ],
  [
    "<p>Fix MSVC build process to cope with spaces in the file path of OpenSSL (Andrew Dunstan)</p>"
  ]
]