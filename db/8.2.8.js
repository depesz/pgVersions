[
  [
    "<p>Fix <code class=\"literal\">ERRORDATA_STACK_SIZE\n          exceeded</code> crash that occurred on Windows when using\n          UTF-8 database encoding and a different client encoding\n          (Tom)</p>"
  ],
  [
    "<p>Fix <code class=\"command\">ALTER TABLE ADD COLUMN ...\n          PRIMARY KEY</code> so that the new column is correctly\n          checked to see if it's been initialized to all non-nulls\n          (Brendan Jurd)</p>",
    "<p>Previous versions neglected to check this requirement\n          at all.</p>"
  ],
  [
    "<p>Fix possible <code class=\"command\">CREATE TABLE</code>\n          failure when inheriting the <span class=\"quote\">&#x201C;<span class=\"quote\">same</span>&#x201D;</span>\n          constraint from multiple parent relations that inherited\n          that constraint from a common ancestor (Tom)</p>"
  ],
  [
    "<p>Fix <code class=\"function\">pg_get_ruledef()</code> to\n          show the alias, if any, attached to the target table of\n          an <code class=\"command\">UPDATE</code> or <code class=\"command\">DELETE</code> (Tom)</p>"
  ],
  [
    "<p>Fix GIN bug that could result in a <code class=\"literal\">too many LWLocks taken</code> failure\n          (Teodor)</p>"
  ],
  [
    "<p>Avoid possible crash when decompressing corrupted data\n          (Zdenek Kotala)</p>"
  ],
  [
    "<p>Repair two places where SIGTERM exit of a backend\n          could leave corrupted state in shared memory (Tom)</p>",
    "<p>Neither case is very important if SIGTERM is used to\n          shut down the whole database cluster together, but there\n          was a problem if someone tried to SIGTERM individual\n          backends.</p>"
  ],
  [
    "<p>Fix conversions between ISO-8859-5 and other encodings\n          to handle Cyrillic <span class=\"quote\">&#x201C;<span class=\"quote\">Yo</span>&#x201D;</span> characters (<code class=\"literal\">e</code> and <code class=\"literal\">E</code>\n          with two dots) (Sergey Burladyan)</p>"
  ],
  [
    "<p>Fix several datatype input functions, notably\n          <code class=\"function\">array_in()</code>, that were\n          allowing unused bytes in their results to contain\n          uninitialized, unpredictable values (Tom)</p>",
    "<p>This could lead to failures in which two apparently\n          identical literal values were not seen as equal,\n          resulting in the parser complaining about unmatched\n          <code class=\"literal\">ORDER BY</code> and <code class=\"literal\">DISTINCT</code> expressions.</p>"
  ],
  [
    "<p>Fix a corner case in regular-expression substring\n          matching (<code class=\"literal\">substring(<em class=\"replaceable\"><code>string</code></em> from <em class=\"replaceable\"><code>pattern</code></em>)</code>)\n          (Tom)</p>",
    "<p>The problem occurs when there is a match to the\n          pattern overall but the user has specified a\n          parenthesized subexpression and that subexpression hasn't\n          got a match. An example is <code class=\"literal\">substring('foo' from 'foo(bar)?')</code>. This\n          should return NULL, since <code class=\"literal\">(bar)</code> isn't matched, but it was\n          mistakenly returning the whole-pattern match instead (ie,\n          <code class=\"literal\">foo</code>).</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2008c (for DST law\n          changes in Morocco, Iraq, Choibalsan, Pakistan, Syria,\n          Cuba, and Argentina/San_Luis)</p>"
  ],
  [
    "<p>Fix incorrect result from <span class=\"application\">ecpg</span>'s <code class=\"function\">PGTYPEStimestamp_sub()</code> function\n          (Michael)</p>"
  ],
  [
    "<p>Fix broken GiST comparison function for <code class=\"filename\">contrib/tsearch2</code>'s <code class=\"type\">tsquery</code> type (Teodor)</p>"
  ],
  [
    "<p>Fix possible crashes in <code class=\"filename\">contrib/cube</code> functions (Tom)</p>"
  ],
  [
    "<p>Fix core dump in <code class=\"filename\">contrib/xml2</code>'s <code class=\"function\">xpath_table()</code> function when the input\n          query returns a NULL value (Tom)</p>"
  ],
  [
    "<p>Fix <code class=\"filename\">contrib/xml2</code>'s\n          makefile to not override <code class=\"literal\">CFLAGS</code> (Tom)</p>"
  ],
  [
    "<p>Fix <code class=\"literal\">DatumGetBool</code> macro to\n          not fail with <span class=\"application\">gcc</span> 4.3\n          (Tom)</p>",
    "<p>This problem affects <span class=\"quote\">&#x201C;<span class=\"quote\">old style</span>&#x201D;</span> (V0) C functions that\n          return boolean. The fix is already in 8.3, but the need\n          to back-patch it was not realized at the time.</p>"
  ]
]