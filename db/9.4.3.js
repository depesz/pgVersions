[
  [
    "<p>Avoid failures while <code class=\"FUNCTION\">fsync</code>'ing data directory during crash restart (Abhijit Menon-Sen, Tom Lane)</p>",
    "<p>In the previous minor releases we added a patch to <code class=\"FUNCTION\">fsync</code> everything in the data directory after a crash. Unfortunately its response to any error condition was to fail, thereby preventing the server from starting up, even when the problem was quite harmless. An example is that an unwritable file in the data directory would prevent restart on some platforms; but it is common to make SSL certificate files unwritable by the server. Revise this behavior so that permissions failures are ignored altogether, and other types of failures are logged but do not prevent continuing.</p>",
    "<p>Also apply the same rules in <tt class=\"LITERAL\">initdb --sync-only</tt>. This case is less critical but it should act similarly.</p>"
  ],
  [
    "<p>Fix <code class=\"FUNCTION\">pg_get_functiondef()</code> to show functions' <tt class=\"LITERAL\">LEAKPROOF</tt> property, if set (Jeevan Chalke)</p>"
  ],
  [
    "<p>Fix <code class=\"FUNCTION\">pushJsonbValue()</code> to unpack <tt class=\"TYPE\">jbvBinary</tt> objects (Andrew Dunstan)</p>",
    "<p>This change does not affect any behavior in the core code as of 9.4, but it avoids a corner case for possible third-party callers.</p>"
  ],
  [
    "<p>Remove <span class=\"APPLICATION\">configure</span>'s check prohibiting linking to a threaded <span class=\"APPLICATION\">libpython</span> on <span class=\"SYSTEMITEM\">OpenBSD</span> (Tom Lane)</p>",
    "<p>The failure this restriction was meant to prevent seems to not be a problem anymore on current <span class=\"SYSTEMITEM\">OpenBSD</span> versions.</p>"
  ]
]