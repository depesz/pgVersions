[
  [
    "<p>Support explicit placement of the temporary-table\n          schema within <code class=\"varname\">search_path</code>,\n          and disable searching it for functions and operators\n          (Tom)</p>",
    "<p>This is needed to allow a security-definer function to\n          set a truly secure value of <code class=\"varname\">search_path</code>. Without it, an unprivileged\n          SQL user can use temporary objects to execute code with\n          the privileges of the security-definer function\n          (CVE-2007-2138). See <code class=\"command\">CREATE\n          FUNCTION</code> for more information.</p>"
  ],
  [
    "<p><code class=\"filename\">/contrib/tsearch2</code> crash\n          fixes (Teodor)</p>"
  ],
  [
    "<p>Require <code class=\"command\">COMMIT PREPARED</code>\n          to be executed in the same database as the transaction\n          was prepared in (Heikki)</p>"
  ],
  [
    "<p>Fix potential-data-corruption bug in how <code class=\"command\">VACUUM FULL</code> handles <code class=\"command\">UPDATE</code> chains (Tom, Pavan Deolasee)</p>"
  ],
  [
    "<p>Planner fixes, including improving outer join and\n          bitmap scan selection logic (Tom)</p>"
  ],
  [
    "<p>Fix PANIC during enlargement of a hash index (bug\n          introduced in 8.1.6) (Tom)</p>"
  ],
  [
    "<p>Fix POSIX-style timezone specs to follow new USA DST\n          rules (Tom)</p>"
  ]
]