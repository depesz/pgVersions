[
  [
    "<p>Prevent access to external files/URLs via XML entity\n          references (Noah Misch, Tom Lane)</p>",
    "<p><code class=\"function\">xml_parse()</code> would\n          attempt to fetch external files or URLs as needed to\n          resolve DTD and entity references in an XML value, thus\n          allowing unprivileged database users to attempt to fetch\n          data with the privileges of the database server. While\n          the external data wouldn't get returned directly to the\n          user, portions of it could be exposed in error messages\n          if the data didn't parse as valid XML; and in any case\n          the mere ability to check existence of a file might be\n          useful to an attacker. (CVE-2012-3489)</p>"
  ],
  [
    "<p>Prevent access to external files/URLs via <code class=\"filename\">contrib/xml2</code>'s <code class=\"function\">xslt_process()</code> (Peter Eisentraut)</p>",
    "<p><span class=\"application\">libxslt</span> offers the\n          ability to read and write both files and URLs through\n          stylesheet commands, thus allowing unprivileged database\n          users to both read and write data with the privileges of\n          the database server. Disable that through proper use of\n          <span class=\"application\">libxslt</span>'s security\n          options. (CVE-2012-3488)</p>",
    "<p>Also, remove <code class=\"function\">xslt_process()</code>'s ability to fetch\n          documents and stylesheets from external files/URLs. While\n          this was a documented <span class=\"quote\">&#x201C;<span class=\"quote\">feature</span>&#x201D;</span>, it was long regarded as a\n          bad idea. The fix for CVE-2012-3489 broke that\n          capability, and rather than expend effort on trying to\n          fix it, we're just going to summarily remove it.</p>"
  ],
  [
    "<p>Prevent too-early recycling of btree index pages (Noah\n          Misch)</p>",
    "<p>When we allowed read-only transactions to skip\n          assigning XIDs, we introduced the possibility that a\n          deleted btree page could be recycled while a read-only\n          transaction was still in flight to it. This would result\n          in incorrect index search results. The probability of\n          such an error occurring in the field seems very low\n          because of the timing requirements, but nonetheless it\n          should be fixed.</p>"
  ],
  [
    "<p>Fix crash-safety bug with newly-created-or-reset\n          sequences (Tom Lane)</p>",
    "<p>If <code class=\"command\">ALTER SEQUENCE</code> was\n          executed on a freshly created or reset sequence, and then\n          precisely one <code class=\"function\">nextval()</code>\n          call was made on it, and then the server crashed, WAL\n          replay would restore the sequence to a state in which it\n          appeared that no <code class=\"function\">nextval()</code>\n          had been done, thus allowing the first sequence value to\n          be returned again by the next <code class=\"function\">nextval()</code> call. In particular this\n          could manifest for <code class=\"type\">serial</code>\n          columns, since creation of a serial column's sequence\n          includes an <code class=\"command\">ALTER SEQUENCE OWNED\n          BY</code> step.</p>"
  ],
  [
    "<p>Ensure the <code class=\"filename\">backup_label</code>\n          file is fsync'd after <code class=\"function\">pg_start_backup()</code> (Dave Kerr)</p>"
  ],
  [
    "<p>Back-patch 9.1 improvement to compress the fsync\n          request queue (Robert Haas)</p>",
    "<p>This improves performance during checkpoints. The 9.1\n          change has now seen enough field testing to seem safe to\n          back-patch.</p>"
  ],
  [
    "<p>Only allow autovacuum to be auto-canceled by a\n          directly blocked process (Tom Lane)</p>",
    "<p>The original coding could allow inconsistent behavior\n          in some cases; in particular, an autovacuum could get\n          canceled after less than <code class=\"literal\">deadlock_timeout</code> grace period.</p>"
  ],
  [
    "<p>Improve logging of autovacuum cancels (Robert\n          Haas)</p>"
  ],
  [
    "<p>Fix log collector so that <code class=\"literal\">log_truncate_on_rotation</code> works during\n          the very first log rotation after server start (Tom\n          Lane)</p>"
  ],
  [
    "<p>Fix <code class=\"literal\">WITH</code> attached to a\n          nested set operation (<code class=\"literal\">UNION</code>/<code class=\"literal\">INTERSECT</code>/<code class=\"literal\">EXCEPT</code>) (Tom Lane)</p>"
  ],
  [
    "<p>Ensure that a whole-row reference to a subquery\n          doesn't include any extra <code class=\"literal\">GROUP\n          BY</code> or <code class=\"literal\">ORDER BY</code>\n          columns (Tom Lane)</p>"
  ],
  [
    "<p>Disallow copying whole-row references in <code class=\"literal\">CHECK</code> constraints and index definitions\n          during <code class=\"command\">CREATE TABLE</code> (Tom\n          Lane)</p>",
    "<p>This situation can arise in <code class=\"command\">CREATE TABLE</code> with <code class=\"literal\">LIKE</code> or <code class=\"literal\">INHERITS</code>. The copied whole-row variable\n          was incorrectly labeled with the row type of the original\n          table not the new one. Rejecting the case seems\n          reasonable for <code class=\"literal\">LIKE</code>, since\n          the row types might well diverge later. For <code class=\"literal\">INHERITS</code> we should ideally allow it,\n          with an implicit coercion to the parent table's row type;\n          but that will require more work than seems safe to\n          back-patch.</p>"
  ],
  [
    "<p>Fix memory leak in <code class=\"literal\">ARRAY(SELECT\n          ...)</code> subqueries (Heikki Linnakangas, Tom Lane)</p>"
  ],
  [
    "<p>Fix extraction of common prefixes from regular\n          expressions (Tom Lane)</p>",
    "<p>The code could get confused by quantified\n          parenthesized subexpressions, such as <code class=\"literal\">^(foo)?bar</code>. This would lead to incorrect\n          index optimization of searches for such patterns.</p>"
  ],
  [
    "<p>Fix bugs with parsing signed <em class=\"replaceable\"><code>hh</code></em><code class=\"literal\">:</code><em class=\"replaceable\"><code>mm</code></em> and <em class=\"replaceable\"><code>hh</code></em><code class=\"literal\">:</code><em class=\"replaceable\"><code>mm</code></em><code class=\"literal\">:</code><em class=\"replaceable\"><code>ss</code></em> fields in <code class=\"type\">interval</code> constants (Amit Kapila, Tom\n          Lane)</p>"
  ],
  [
    "<p>Report errors properly in <code class=\"filename\">contrib/xml2</code>'s <code class=\"function\">xslt_process()</code> (Tom Lane)</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2012e for DST law\n          changes in Morocco and Tokelau</p>"
  ]
]