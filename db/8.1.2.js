[
  [
    "<p>Fix Windows code so that postmaster will continue\n          rather than exit if there is no more room in\n          ShmemBackendArray (Magnus)</p>",
    "<p>The previous behavior could lead to a\n          denial-of-service situation if too many connection\n          requests arrive close together. This applies <span class=\"emphasis\"><em>only</em></span> to the Windows port.</p>"
  ],
  [
    "<p>Fix bug introduced in 8.0 that could allow ReadBuffer\n          to return an already-used page as new, potentially\n          causing loss of recently-committed data (Tom)</p>"
  ],
  [
    "<p>Fix for protocol-level Describe messages issued\n          outside a transaction or in a failed transaction\n          (Tom)</p>"
  ],
  [
    "<p>Fix character string comparison for locales that\n          consider different character combinations as equal, such\n          as Hungarian (Tom)</p>",
    "<p>This might require <code class=\"command\">REINDEX</code> to fix existing indexes on\n          textual columns.</p>"
  ],
  [
    "<p>Set locale environment variables during postmaster\n          startup to ensure that <span class=\"application\">plperl</span> won't change the locale\n          later</p>",
    "<p>This fixes a problem that occurred if the <span class=\"application\">postmaster</span> was started with\n          environment variables specifying a different locale than\n          what <span class=\"application\">initdb</span> had been\n          told. Under these conditions, any use of <span class=\"application\">plperl</span> was likely to lead to corrupt\n          indexes. You might need <code class=\"command\">REINDEX</code> to fix existing indexes on\n          textual columns if this has happened to you.</p>"
  ],
  [
    "<p>Allow more flexible relocation of installation\n          directories (Tom)</p>",
    "<p>Previous releases supported relocation only if all\n          installation directory paths were the same except for the\n          last component.</p>"
  ],
  [
    "<p>Prevent crashes caused by the use of <code class=\"literal\">ISO-8859-5</code> and <code class=\"literal\">ISO-8859-9</code> encodings (Tatsuo)</p>"
  ],
  [
    "<p>Fix longstanding bug in strpos() and regular\n          expression handling in certain rarely used Asian\n          multi-byte character sets (Tatsuo)</p>"
  ],
  [
    "<p>Fix bug where COPY CSV mode considered any\n          <code class=\"literal\">\\.</code> to terminate the copy\n          data</p>",
    "<p>The new code requires <code class=\"literal\">\\.</code>\n          to appear alone on a line, as per documentation.</p>"
  ],
  [
    "<p>Make COPY CSV mode quote a literal data value of\n          <code class=\"literal\">\\.</code> to ensure it cannot be\n          interpreted as the end-of-data marker (Bruce)</p>"
  ],
  [
    "<p>Various fixes for functions returning <code class=\"literal\">RECORD</code>s (Tom)</p>"
  ],
  [
    "<p>Fix processing of <code class=\"filename\">postgresql.conf</code> so a final line with no\n          newline is processed properly (Tom)</p>"
  ],
  [
    "<p>Fix bug in <code class=\"filename\">/contrib/pgcrypto</code> gen_salt, which\n          caused it not to use all available salt space for MD5 and\n          XDES algorithms (Marko Kreen, Solar Designer)</p>",
    "<p>Salts for Blowfish and standard DES are\n          unaffected.</p>"
  ],
  [
    "<p>Fix autovacuum crash when processing expression\n          indexes</p>"
  ],
  [
    "<p>Fix <code class=\"filename\">/contrib/dblink</code> to\n          throw an error, rather than crashing, when the number of\n          columns specified is different from what's actually\n          returned by the query (Joe)</p>"
  ]
]