[
  [
    "<p>Fix failure of <tt class=\"COMMAND\">ALTER TABLE ... ALTER COLUMN TYPE</tt> when the table has a partial exclusion constraint (Tom Lane)</p>"
  ],
  [
    "<p>Fix incorrect printing of queries with duplicate join names (Philip Dubé)</p>",
    "<p>This oversight caused a dump/restore failure for views containing such queries.</p>"
  ],
  [
    "<p>Fix misoptimization of <tt class=\"LITERAL\">{1,1}</tt> quantifiers in regular expressions (Tom Lane)</p>",
    "<p>Such quantifiers were treated as no-ops and optimized away; but the documentation specifies that they impose greediness, or non-greediness in the case of the non-greedy variant <tt class=\"LITERAL\">{1,1}?</tt>, on the subexpression they're attached to, and this did not happen. The misbehavior occurred only if the subexpression contained capturing parentheses or a back-reference.</p>"
  ],
  [
    "<p>Fix race condition in check to see whether a pre-existing shared memory segment is still in use by a conflicting postmaster (Tom Lane)</p>"
  ],
  [
    "<p>Avoid attempting to do database accesses for parameter checking in processes that are not connected to a specific database (Vignesh C, Andres Freund)</p>",
    "<p>This error could result in failures like <span class=\"QUOTE\">\"cannot read pg_class without having selected a database\"</span>.</p>"
  ],
  [
    "<p>Improve <span class=\"APPLICATION\">initdb</span>'s handling of multiple equivalent names for the system time zone (Tom Lane, Andrew Gierth)</p>",
    "<p>Make <span class=\"APPLICATION\">initdb</span> examine the <tt class=\"FILENAME\">/etc/localtime</tt> symbolic link, if that exists, to break ties between equivalent names for the system time zone. This makes <span class=\"APPLICATION\">initdb</span> more likely to select the time zone name that the user would expect when multiple identical time zones exist. It will not change the behavior if <tt class=\"FILENAME\">/etc/localtime</tt> is not a symlink to a zone data file, nor if the time zone is determined from the <tt class=\"ENVAR\">TZ</tt> environment variable.</p>",
    "<p>Separately, prefer <tt class=\"LITERAL\">UTC</tt> over other spellings of that time zone, when neither <tt class=\"ENVAR\">TZ</tt> nor <tt class=\"FILENAME\">/etc/localtime</tt> provide a hint. This fixes an annoyance introduced by <span class=\"APPLICATION\">tzdata</span> 2019a's change to make the <tt class=\"LITERAL\">UCT</tt> and <tt class=\"LITERAL\">UTC</tt> zone names equivalent: <span class=\"APPLICATION\">initdb</span> was then preferring <tt class=\"LITERAL\">UCT</tt>, which almost nobody wants.</p>"
  ],
  [
    "<p>Fix misleading error reports from <span class=\"APPLICATION\">reindexdb</span> (Julien Rouhaud)</p>"
  ],
  [
    "<p>In <tt class=\"FILENAME\">contrib/postgres_fdw</tt>, account for possible data modifications by local <tt class=\"LITERAL\">BEFORE ROW UPDATE</tt> triggers (Shohei Mochizuki)</p>",
    "<p>If a trigger modified a column that was otherwise not changed by the <tt class=\"COMMAND\">UPDATE</tt>, the new value was not transmitted to the remote server.</p>"
  ],
  [
    "<p>On Windows, avoid failure when the database encoding is set to SQL_ASCII and we attempt to log a non-ASCII string (Noah Misch)</p>",
    "<p>The code had been assuming that such strings must be in UTF-8, and would throw an error if they didn't appear to be validly encoded. Now, just transmit the untranslated bytes to the log.</p>"
  ],
  [
    "<p>Make <span class=\"APPLICATION\">PL/pgSQL</span>'s header files C++-safe (George Tarasov)</p>"
  ]
]