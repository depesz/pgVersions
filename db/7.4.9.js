[
  [
    "<p>Fix error that allowed <code class=\"command\">VACUUM</code> to remove <code class=\"literal\">ctid</code> chains too soon, and add more\n          checking in code that follows <code class=\"literal\">ctid</code> links</p>",
    "<p>This fixes a long-standing problem that could cause\n          crashes in very rare circumstances.</p>"
  ],
  [
    "<p>Fix <code class=\"type\">CHAR()</code> to properly pad\n          spaces to the specified length when using a multiple-byte\n          character set (Yoshiyuki Asaba)</p>",
    "<p>In prior releases, the padding of <code class=\"type\">CHAR()</code> was incorrect because it only padded\n          to the specified number of bytes without considering how\n          many characters were stored.</p>"
  ],
  [
    "<p>Fix the sense of the test for read-only transaction in\n          <code class=\"command\">COPY</code></p>",
    "<p>The code formerly prohibited <code class=\"command\">COPY TO</code>, where it should prohibit\n          <code class=\"command\">COPY FROM</code>.</p>"
  ],
  [
    "<p>Fix planning problem with outer-join ON clauses that\n          reference only the inner-side relation</p>"
  ],
  [
    "<p>Further fixes for <code class=\"literal\">x FULL JOIN y\n          ON true</code> corner cases</p>"
  ],
  [
    "<p>Make <code class=\"function\">array_in</code> and\n          <code class=\"function\">array_recv</code> more paranoid\n          about validating their OID parameter</p>"
  ],
  [
    "<p>Fix missing rows in queries like <code class=\"literal\">UPDATE a=... WHERE a...</code> with GiST index\n          on column <code class=\"literal\">a</code></p>"
  ],
  [
    "<p>Improve robustness of datetime parsing</p>"
  ],
  [
    "<p>Improve checking for partially-written WAL pages</p>"
  ],
  [
    "<p>Improve robustness of signal handling when SSL is\n          enabled</p>"
  ],
  [
    "<p>Don't try to open more than <code class=\"literal\">max_files_per_process</code> files during\n          postmaster startup</p>"
  ],
  [
    "<p>Various memory leakage fixes</p>"
  ],
  [
    "<p>Various portability improvements</p>"
  ],
  [
    "<p>Fix PL/pgSQL to handle <code class=\"literal\">var :=\n          var</code> correctly when the variable is of\n          pass-by-reference type</p>"
  ],
  [
    "<p>Update <code class=\"filename\">contrib/tsearch2</code>\n          to use current Snowball code</p>"
  ]
]