[
  [
    "<p>Fix race condition in transaction log management</p>",
    "<p>There was a narrow window in which an I/O operation\n          could be initiated for the wrong page, leading to an\n          Assert failure or data corruption.</p>"
  ],
  [
    "<p><code class=\"filename\">/contrib/ltree</code> fixes\n          (Teodor)</p>"
  ],
  [
    "<p>Fix longstanding planning error for outer joins</p>",
    "<p>This bug sometimes caused a bogus error <span class=\"quote\">&#x201C;<span class=\"quote\">RIGHT JOIN is only supported\n          with merge-joinable join conditions</span>&#x201D;</span>.</p>"
  ],
  [
    "<p>Prevent core dump in <span class=\"application\">pg_autovacuum</span> when a table has been\n          dropped</p>"
  ]
]