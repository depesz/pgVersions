[
  [
    "<p>Do not let extension scripts replace objects not already belonging to the extension (Tom Lane)</p>",
    "<p>This change prevents extension scripts from doing <code class=\"command\">CREATE OR REPLACE</code> if there is an existing object that does not belong to the extension. It also prevents <code class=\"command\">CREATE IF NOT EXISTS</code> in the same situation. This prevents a form of trojan-horse attack in which a hostile database user could become the owner of an extension object and then modify it to compromise future uses of the object by other users. As a side benefit, it also reduces the risk of accidentally replacing objects one did not mean to.</p>",
    "<p>The <span class=\"productname\">PostgreSQL</span> Project thanks Sven Klemm for reporting this problem. (CVE-2022-2625)</p>"
  ],
  [
    "<p>Fix replay of <code class=\"command\">CREATE DATABASE</code> WAL records on standby servers (Kyotaro Horiguchi, Asim R Praveen, Paul Guo)</p>",
    "<p>Standby servers may encounter missing tablespace directories when replaying database-creation WAL records. Prior to this patch, a standby would fail to recover in such a case; however, such directories could be legitimately missing. Create the tablespace (as a plain directory), then check that it has been dropped again once replay reaches a consistent state.</p>"
  ],
  [
    "<p>Support <span class=\"quote\">“<span class=\"quote\">in place</span>”</span> tablespaces (Thomas Munro, Michael Paquier, Álvaro Herrera)</p>",
    "<p>Normally a Postgres tablespace is a symbolic link to a directory on some other filesystem. This change allows it to just be a plain directory. While this has no use for separating tables onto different filesystems, it is a convenient setup for testing. Moreover, it is necessary to support the <code class=\"command\">CREATE DATABASE</code> replay fix, which transiently creates a missing tablespace as an <span class=\"quote\">“<span class=\"quote\">in place</span>”</span> tablespace.</p>"
  ],
  [
    "<p>Fix permissions checks in <code class=\"command\">CREATE INDEX</code> (Nathan Bossart, Noah Misch)</p>",
    "<p>The fix for CVE-2022-1552 caused <code class=\"command\">CREATE INDEX</code> to apply the table owner's permissions while performing lookups of operator classes and other objects, where formerly the calling user's permissions were used. This broke dump/restore scenarios, because <span class=\"application\">pg_dump</span> issues <code class=\"command\">CREATE INDEX</code> before re-granting permissions.</p>"
  ],
  [
    "<p>In extended query protocol, force an immediate commit after <code class=\"command\">CREATE DATABASE</code> and other commands that can't run in a transaction block (Tom Lane)</p>",
    "<p>If the client does not send a Sync message immediately after such a command, but instead sends another command, any failure in that command would lead to rolling back the preceding command, typically leaving inconsistent state on-disk (such as a missing or extra database directory). The mechanisms intended to prevent that situation turn out to work for multiple commands in a simple-Query message, but not for a series of extended-protocol messages. To prevent inconsistency without breaking use-cases that work today, force an implicit commit after such commands.</p>"
  ],
  [
    "<p>Fix race condition when checking transaction visibility (Simon Riggs)</p>",
    "<p><code class=\"function\">TransactionIdIsInProgress</code> could report <code class=\"literal\">false</code> before the subject transaction is considered visible, leading to various misbehaviors. The race condition window is normally very narrow, but use of synchronous replication makes it much wider, because the wait for a synchronous replica happens in that window.</p>"
  ],
  [
    "<p>Fix queries in which a <span class=\"quote\">“<span class=\"quote\">whole-row variable</span>”</span> references the result of a function that returns a domain over composite type (Tom Lane)</p>"
  ],
  [
    "<p>Fix <span class=\"quote\">“<span class=\"quote\">variable not found in subplan target list</span>”</span> planner error when pulling up a sub-<code class=\"literal\">SELECT</code> that's referenced in a <code class=\"literal\">GROUPING</code> function (Richard Guo)</p>"
  ],
  [
    "<p>Fix <code class=\"literal\">ALTER TABLE ... ENABLE/DISABLE TRIGGER</code> to handle recursion correctly for triggers on partitioned tables (Álvaro Herrera, Amit Langote)</p>",
    "<p>In certain cases, a <span class=\"quote\">“<span class=\"quote\">trigger does not exist</span>”</span> failure would occur because the command would try to adjust the trigger on a child partition that doesn't have it.</p>"
  ],
  [
    "<p>Prevent <code class=\"function\">pg_stat_get_subscription()</code> from possibly returning an extra row containing garbage values (Kuntal Ghosh)</p>"
  ],
  [
    "<p>Ensure that <code class=\"function\">pg_stop_backup()</code> cleans up session state properly (Fujii Masao)</p>",
    "<p>This omission could lead to assertion failures or crashes later in the session.</p>"
  ],
  [
    "<p>Fix join alias matching in <code class=\"literal\">FOR [KEY] UPDATE/SHARE</code> clauses (Dean Rasheed)</p>",
    "<p>In corner cases, a misleading error could be reported.</p>"
  ],
  [
    "<p>Avoid crashing if too many column aliases are attached to an <code class=\"literal\">XMLTABLE</code> or <code class=\"literal\">JSON_TABLE</code> construct (Álvaro Herrera)</p>"
  ],
  [
    "<p>Reject <code class=\"literal\">ROW()</code> expressions and functions in <code class=\"literal\">FROM</code> that have too many columns (Tom Lane)</p>",
    "<p>Cases with more than about 1600 columns are unsupported, and have always failed at execution. However, it emerges that some earlier code could be driven to assertion failures or crashes by queries with more than 32K columns. Add a parse-time check to prevent that.</p>"
  ],
  [
    "<p>When decompiling a view or rule, show a <code class=\"command\">SELECT</code> output column's <code class=\"literal\">AS \"?column?\"</code> alias clause if it could be referenced elsewhere (Tom Lane)</p>",
    "<p>Previously, this auto-generated alias was always hidden; but there are corner cases where doing so results in a non-restorable view or rule definition.</p>"
  ],
  [
    "<p>Fix dumping of a view using a function in <code class=\"literal\">FROM</code> that returns a composite type, when column(s) of the composite type have been dropped since the view was made (Tom Lane)</p>",
    "<p>This oversight could lead to dump/reload or <span class=\"application\">pg_upgrade</span> failures, as the dumped view would have too many column aliases for the function.</p>"
  ],
  [
    "<p>Report implicitly-created operator families to event triggers (Masahiko Sawada)</p>",
    "<p>If <code class=\"command\">CREATE OPERATOR CLASS</code> results in the implicit creation of an operator family, that object was not reported to event triggers that should capture such events.</p>"
  ],
  [
    "<p>Fix control file updates made when a restartpoint is running during promotion of a standby server (Kyotaro Horiguchi)</p>",
    "<p>Previously, when the restartpoint completed it could incorrectly update the last-checkpoint fields of the control file, potentially leading to PANIC and failure to restart if the server crashes before the next normal checkpoint completes.</p>"
  ],
  [
    "<p>Prevent triggering of standby's <code class=\"varname\">wal_receiver_timeout</code> during logical replication of large transactions (Wang Wei, Amit Kapila)</p>",
    "<p>If a large transaction on the primary server sends no data to the standby (perhaps because no table it changes is published), it was possible for the standby to timeout. Fix that by ensuring we send keepalive messages periodically in such situations.</p>"
  ],
  [
    "<p>Disallow nested backup operations in logical replication walsenders (Fujii Masao)</p>"
  ],
  [
    "<p>Fix memory leak in logical replication subscribers (Hou Zhijie)</p>"
  ],
  [
    "<p>Prevent open-file leak when reading an invalid timezone abbreviation file (Kyotaro Horiguchi)</p>",
    "<p>Such cases could result in harmless warning messages.</p>"
  ],
  [
    "<p>Allow custom server parameters to have short descriptions that are NULL (Steve Chavez)</p>",
    "<p>Previously, although extensions could choose to create such settings, some code paths would crash while processing them.</p>"
  ],
  [
    "<p>Fix WAL consistency checking logic to correctly handle <code class=\"literal\">BRIN_EVACUATE_PAGE</code> flags (Haiyang Wang)</p>"
  ],
  [
    "<p>Fix erroneous assertion checks in shared hashtable management (Thomas Munro)</p>"
  ],
  [
    "<p>Arrange to clean up after commit-time errors within <code class=\"function\">SPI_commit()</code>, rather than expecting callers to do that (Peter Eisentraut, Tom Lane)</p>",
    "<p>Proper cleanup is complicated and requires use of low-level facilities, so it's not surprising that no known caller got it right. This led to misbehaviors when a PL procedure issued <code class=\"command\">COMMIT</code> but a failure occurred (such as a deferred constraint check). To improve matters, redefine <code class=\"function\">SPI_commit()</code> as starting a new transaction, so that it becomes equivalent to <code class=\"function\">SPI_commit_and_chain()</code> except that you get default transaction characteristics instead of preserving the prior transaction's characteristics. To make this somewhat transparent API-wise, redefine <code class=\"function\">SPI_start_transaction()</code> as a no-op. All known callers of <code class=\"function\">SPI_commit()</code> immediately call <code class=\"function\">SPI_start_transaction()</code>, so they will not notice any change. Similar remarks apply to <code class=\"function\">SPI_rollback()</code>.</p>",
    "<p>Also fix PL/Python, which omitted any handling of such errors at all, resulting in jumping out of the Python interpreter. This is reported to crash Python 3.11. Older Python releases leak some memory but seem okay with it otherwise.</p>"
  ],
  [
    "<p>Remove misguided SSL key file ownership check in <span class=\"application\">libpq</span> (Tom Lane)</p>",
    "<p>In the previous minor releases, we copied the server's permission checking rules for SSL private key files into libpq. But we should not have also copied the server's file-ownership check. While that works in normal use-cases, it can result in an unexpected failure for clients running as root, and perhaps in other cases.</p>"
  ],
  [
    "<p>Ensure <span class=\"application\">ecpg</span> reports server connection loss sanely (Tom Lane)</p>",
    "<p>Misprocessing of a libpq-generated error result, such as a report of lost connection, would lead to printing <span class=\"quote\">“<span class=\"quote\">(null)</span>”</span> instead of a useful error message; or in older releases it would lead to a crash.</p>"
  ],
  [
    "<p>Avoid core dump in <span class=\"application\">ecpglib</span> with unexpected orders of operations (Tom Lane)</p>",
    "<p>Certain operations such as <code class=\"command\">EXEC SQL PREPARE</code> would crash (rather than reporting an error as expected) if called before establishing any database connection.</p>"
  ],
  [
    "<p>In <span class=\"application\">ecpglib</span>, avoid redundant <code class=\"function\">newlocale()</code> calls (Noah Misch)</p>",
    "<p>Allocate a C locale object once per process when first connecting, rather than creating and freeing locale objects once per query. This mitigates a libc memory leak on AIX, and may offer some performance benefit everywhere.</p>"
  ],
  [
    "<p>In <span class=\"application\">psql</span>'s <code class=\"command\">\\watch</code> command, echo a newline after cancellation with control-C (Pavel Stehule)</p>",
    "<p>This prevents libedit (and possibly also libreadline) from becoming confused about which column the cursor is in.</p>"
  ],
  [
    "<p>Fix <code class=\"filename\">contrib/pg_stat_statements</code> to avoid problems with very large query-text files on 32-bit platforms (Tom Lane)</p>"
  ],
  [
    "<p>Ensure that <code class=\"filename\">contrib/postgres_fdw</code> sends constants of <code class=\"type\">regconfig</code> and other <code class=\"type\">reg*</code> types with proper schema qualification (Tom Lane)</p>"
  ],
  [
    "<p>Block signals while allocating dynamic shared memory on Linux (Thomas Munro)</p>",
    "<p>This avoids problems when a signal interrupts <code class=\"function\">posix_fallocate()</code>.</p>"
  ],
  [
    "<p>Detect unexpected <code class=\"literal\">EEXIST</code> error from <code class=\"function\">shm_open()</code> (Thomas Munro)</p>",
    "<p>This avoids a possible crash on Solaris.</p>"
  ],
  [
    "<p>Adjust PL/Perl test case so it will work under Perl 5.36 (Dagfinn Ilmari Mannsåker)</p>"
  ],
  [
    "<p>Avoid incorrectly using an out-of-date <span class=\"application\">libldap_r</span> library when multiple <span class=\"productname\">OpenLDAP</span> installations are present while building <span class=\"productname\">PostgreSQL</span> (Tom Lane)</p>"
  ]
]