[
  [
    "<p>Fix <code class=\"command\">CREATE INDEX CONCURRENTLY</code> to wait for concurrent prepared transactions (Andrey Borodin)</p>",
    "<p>At the point where <code class=\"command\">CREATE INDEX CONCURRENTLY</code> waits for all concurrent transactions to complete so that it can see rows they inserted, it must also wait for all prepared transactions to complete, for the same reason. Its failure to do so meant that rows inserted by prepared transactions might be omitted from the new index, causing queries relying on the index to miss such rows. In installations that have enabled prepared transactions (<code class=\"varname\">max_prepared_transactions</code> &gt; 0), it's recommended to reindex any concurrently-built indexes in case this problem occurred when they were built.</p>"
  ],
  [
    "<p>Avoid incorrect results when <code class=\"literal\">WHERE CURRENT OF</code> is applied to a cursor whose plan contains a MergeAppend node (Tom Lane)</p>",
    "<p>This case is unsupported (in general, a cursor using <code class=\"literal\">ORDER BY</code> is not guaranteed to be simply updatable); but the code previously did not reject it, and could silently give false matches.</p>"
  ],
  [
    "<p>Fix crash when <code class=\"literal\">WHERE CURRENT OF</code> is applied to a cursor whose plan contains a custom scan node (David Geier)</p>"
  ],
  [
    "<p>Fix planner's handling of a placeholder that is computed at some join level and used only at that same level (Tom Lane)</p>",
    "<p>This oversight could lead to <span class=\"quote\">“<span class=\"quote\">failed to build any <em class=\"replaceable\"><code>N</code></em>-way joins</span>”</span> planner errors.</p>"
  ],
  [
    "<p>Be more careful about whether index AMs support mark/restore (Andrew Gierth)</p>",
    "<p>This prevents errors about missing support functions in rare edge cases.</p>"
  ],
  [
    "<p>Adjust settings to make it more difficult to run out of DSM slots during heavy usage of parallel queries (Thomas Munro)</p>"
  ],
  [
    "<p>Fix <code class=\"command\">ALTER DEFAULT PRIVILEGES</code> to handle duplicated arguments safely (Michael Paquier)</p>",
    "<p>Duplicate role or schema names within the same command could lead to <span class=\"quote\">“<span class=\"quote\">tuple already updated by self</span>”</span> errors or unique-constraint violations.</p>"
  ],
  [
    "<p>Flush ACL-related caches when <code class=\"structname\">pg_authid</code> changes (Noah Misch)</p>",
    "<p>This change ensures that permissions-related decisions will promptly reflect the results of <code class=\"command\">ALTER ROLE ... [NO] INHERIT</code>.</p>"
  ],
  [
    "<p>Prevent misprocessing of ambiguous <code class=\"command\">CREATE TABLE LIKE</code> clauses (Tom Lane)</p>",
    "<p>A <code class=\"literal\">LIKE</code> clause is re-examined after initial creation of the new table, to handle importation of indexes and such. It was possible for this re-examination to find a different table of the same name, causing unexpected behavior; one example is where the new table is a temporary table of the same name as the <code class=\"literal\">LIKE</code> target.</p>"
  ],
  [
    "<p>Rearrange order of operations in <code class=\"command\">CREATE TABLE LIKE</code> so that indexes are cloned before building foreign key constraints (Tom Lane)</p>",
    "<p>This fixes the case where a self-referential foreign key constraint declared in the outer <code class=\"command\">CREATE TABLE</code> depends on an index that's coming from the <code class=\"literal\">LIKE</code> clause.</p>"
  ],
  [
    "<p>Disallow <code class=\"command\">CREATE STATISTICS</code> on system catalogs (Tomas Vondra)</p>"
  ],
  [
    "<p>Disallow converting an inheritance child table to a view (Tom Lane)</p>"
  ],
  [
    "<p>Ensure that disk space allocated for a dropped relation is released promptly at commit (Thomas Munro)</p>",
    "<p>Previously, if the dropped relation spanned multiple 1GB segments, only the first segment was truncated immediately. Other segments were simply unlinked, which doesn't authorize the kernel to release the storage so long as any other backends still have the files open.</p>"
  ],
  [
    "<p>Fix handling of backslash-escaped multibyte characters in <code class=\"command\">COPY FROM</code> (Heikki Linnakangas)</p>",
    "<p>A backslash followed by a multibyte character was not handled correctly. In some client character encodings, this could lead to misinterpreting part of a multibyte character as a field separator or end-of-copy-data marker.</p>"
  ],
  [
    "<p>Avoid preallocating executor hash tables in <code class=\"command\">EXPLAIN</code> without <code class=\"literal\">ANALYZE</code> (Alexey Bashtanov)</p>"
  ],
  [
    "<p>Fix recently-introduced race conditions in <code class=\"command\">LISTEN</code>/<code class=\"command\">NOTIFY</code> queue handling (Tom Lane)</p>",
    "<p>A newly-listening backend could attempt to read SLRU pages that were in process of being truncated, possibly causing an error.</p>",
    "<p>The queue tail pointer could become set to a value that's not equal to the queue position of any backend, resulting in effective disabling of the queue truncation logic. Continued use of <code class=\"command\">NOTIFY</code> then led to queue-fill warnings, and eventually to inability to send any more notifies until the server is restarted.</p>"
  ],
  [
    "<p>Allow the <code class=\"type\">jsonb</code> concatenation operator to handle all combinations of JSON data types (Tom Lane)</p>",
    "<p>We can concatenate two JSON objects or two JSON arrays. Handle other cases by wrapping non-array inputs in one-element arrays, then performing an array concatenation. Previously, some combinations of inputs followed this rule but others arbitrarily threw an error.</p>"
  ],
  [
    "<p>Fix use of uninitialized value while parsing a <code class=\"literal\">*</code> quantifier in a BRE-mode regular expression (Tom Lane)</p>",
    "<p>This error could cause the quantifier to act non-greedy, that is behave like a <code class=\"literal\">*?</code> quantifier would do in full regular expressions.</p>"
  ],
  [
    "<p>Fix numeric <code class=\"function\">power()</code> for the case where the exponent is exactly <code class=\"literal\">INT_MIN</code> (-2147483648) (Dean Rasheed)</p>",
    "<p>Previously, a result with no significant digits was produced.</p>"
  ],
  [
    "<p>Prevent possible data loss from incorrect detection of the wraparound point of an SLRU log (Noah Misch)</p>",
    "<p>The wraparound point typically falls in the middle of a page, which must be rounded off to a page boundary, and that was not done correctly. No issue could arise unless an installation had gotten to within one page of SLRU overflow, which is unlikely in a properly-functioning system. If this did happen, it would manifest in later <span class=\"quote\">“<span class=\"quote\">apparent wraparound</span>”</span> or <span class=\"quote\">“<span class=\"quote\">could not access status of transaction</span>”</span> errors.</p>"
  ],
  [
    "<p>Fix memory leak in walsender processes while sending new snapshots for logical decoding (Amit Kapila)</p>"
  ],
  [
    "<p>Fix walsender to accept additional commands after terminating replication (Jeff Davis)</p>"
  ],
  [
    "<p>Ensure detection of deadlocks between hot standby backends and the startup (WAL-application) process (Fujii Masao)</p>",
    "<p>The startup process did not run the deadlock detection code, so that in situations where the startup process is last to join a circular wait situation, the deadlock might never be recognized.</p>"
  ],
  [
    "<p>Ensure that unserviced requests for background workers are cleaned up when the postmaster begins a <span class=\"quote\">“<span class=\"quote\">smart</span>”</span> or <span class=\"quote\">“<span class=\"quote\">fast</span>”</span> shutdown sequence (Tom Lane)</p>",
    "<p>Previously, there was a race condition whereby a child process that had requested a background worker just before shutdown could wait indefinitely, preventing shutdown from completing.</p>"
  ],
  [
    "<p>Fix portability problem in parsing of <code class=\"varname\">recovery_target_xid</code> values (Michael Paquier)</p>",
    "<p>The target XID is potentially 64 bits wide, but it was parsed with <code class=\"function\">strtoul()</code>, causing misbehavior on platforms where <code class=\"type\">long</code> is 32 bits (such as Windows).</p>"
  ],
  [
    "<p>Avoid assertion failure in <code class=\"function\">pg_get_functiondef()</code> when examining a function with a <code class=\"literal\">TRANSFORM</code> option (Tom Lane)</p>"
  ],
  [
    "<p>In <span class=\"application\">psql</span>, re-allow including a password in a <em class=\"replaceable\"><code>connection_string</code></em> argument of a <code class=\"command\">\\connect</code> command (Tom Lane)</p>",
    "<p>This used to work, but a recent bug fix caused the password to be ignored (resulting in prompting for a password).</p>"
  ],
  [
    "<p>Fix assorted bugs in <span class=\"application\">psql</span>'s <code class=\"command\">\\help</code> command (Kyotaro Horiguchi, Tom Lane)</p>",
    "<p><code class=\"command\">\\help</code> with two argument words failed to find a command description using only the first word, for example <code class=\"literal\">\\help reset all</code> should show the help for <code class=\"command\">RESET</code> but did not. Also, <code class=\"command\">\\help</code> often failed to invoke the pager when it should. It also leaked memory.</p>"
  ],
  [
    "<p>In <span class=\"application\">pg_dump</span>, ensure that the restore script runs <code class=\"command\">ALTER PUBLICATION ADD TABLE</code> commands as the owner of the publication (Tom Lane)</p>",
    "<p>Previously, these commands would be run by the role that started the restore script; which will usually work, but in corner cases that role might not have adequate permissions.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_dump</span> to handle <code class=\"literal\">WITH GRANT OPTION</code> in an extension's initial privileges (Noah Misch)</p>",
    "<p>If an extension's script creates an object and grants privileges on it with grant option, then later the user revokes such privileges, <span class=\"application\">pg_dump</span> would generate incorrect SQL for reproducing the situation. (Few if any extensions do this today.)</p>"
  ],
  [
    "<p>In <span class=\"application\">pg_rewind</span>, ensure that all WAL is accounted for when rewinding a standby server (Ian Barwick, Heikki Linnakangas)</p>"
  ],
  [
    "<p>Report the correct database name in connection failure error messages from some client programs (Álvaro Herrera)</p>",
    "<p>If the database name was defaulted rather than given on the command line, <span class=\"application\">pg_dumpall</span>, <span class=\"application\">pgbench</span>, <span class=\"application\">oid2name</span>, and <span class=\"application\">vacuumlo</span> would produce misleading error messages after a connection failure.</p>"
  ],
  [
    "<p>Fix memory leak in <code class=\"filename\">contrib/auto_explain</code> (Japin Li)</p>",
    "<p>Memory consumed while producing the <code class=\"literal\">EXPLAIN</code> output was not freed until the end of the current transaction (for a top-level statement) or the end of the surrounding statement (for a nested statement). This was particularly a problem with <code class=\"varname\">log_nested_statements</code> enabled.</p>"
  ],
  [
    "<p>In <code class=\"filename\">contrib/postgres_fdw</code>, avoid leaking open connections to remote servers when a user mapping or foreign server object is dropped (Bharath Rupireddy)</p>",
    "<p>Open connections that depend on a dropped user mapping or foreign server can no longer be referenced, but formerly they were kept around anyway for the duration of the local session.</p>"
  ],
  [
    "<p>In <code class=\"filename\">contrib/pgcrypto</code>, check for error returns from OpenSSL's EVP functions (Michael Paquier)</p>",
    "<p>We do not really expect errors here, but this change silences warnings from static analysis tools.</p>"
  ],
  [
    "<p>In <code class=\"filename\">contrib/pg_trgm</code>'s GiST index support, avoid crash in the rare case that picksplit is called on exactly two index items (Andrew Gierth, Alexander Korotkov)</p>"
  ],
  [
    "<p>Fix miscalculation of timeouts in <code class=\"filename\">contrib/pg_prewarm</code> and <code class=\"filename\">contrib/postgres_fdw</code> (Alexey Kondratov, Tom Lane)</p>",
    "<p>The main loop in <code class=\"filename\">contrib/pg_prewarm</code>'s autoprewarm parent process underestimated its desired sleep time by a factor of 1000, causing it to consume much more CPU than intended. When waiting for a result from a remote server, <code class=\"filename\">contrib/postgres_fdw</code> overestimated the desired timeout by a factor of 1000 (though this error had been mitigated by imposing a clamp to 60 seconds).</p>",
    "<p>Both of these errors stemmed from incorrectly converting seconds-and-microseconds to milliseconds. Introduce a new API <code class=\"function\">TimestampDifferenceMilliseconds()</code> to make it easier to get this right in the future.</p>"
  ],
  [
    "<p>Improve <span class=\"application\">configure</span>'s heuristics for selecting <code class=\"varname\">PG_SYSROOT</code> on macOS (Tom Lane)</p>",
    "<p>The new method is more likely to produce desirable results when Xcode is newer than the underlying operating system. Choosing a sysroot that does not match the OS version may result in nonfunctional executables.</p>"
  ],
  [
    "<p>While building on macOS, specify <code class=\"option\">-isysroot</code> in link steps as well as compile steps (James Hilliard)</p>",
    "<p>This likewise improves the results when Xcode is out of sync with the operating system.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2021a for DST law changes in Russia (Volgograd zone) and South Sudan, plus historical corrections for Australia, Bahamas, Belize, Bermuda, Ghana, Israel, Kenya, Nigeria, Palestine, Seychelles, and Vanuatu.</p>",
    "<p>Notably, the Australia/Currie zone has been corrected to the point where it is identical to Australia/Hobart.</p>"
  ]
]