[
  [
    "<p>Further restrict visibility of <code class=\"structname\">pg_user_mappings</code>.<code class=\"structfield\">umoptions</code>, to protect passwords\n          stored as user mapping options (Noah Misch)</p>",
    "<p>The fix for CVE-2017-7486 was incorrect: it allowed a\n          user to see the options in her own user mapping, even if\n          she did not have <code class=\"literal\">USAGE</code>\n          permission on the associated foreign server. Such options\n          might include a password that had been provided by the\n          server owner rather than the user herself. Since\n          <code class=\"structname\">information_schema.user_mapping_options</code>\n          does not show the options in such cases, <code class=\"structname\">pg_user_mappings</code> should not either.\n          (CVE-2017-7547)</p>",
    "<p>By itself, this patch will only fix the behavior in\n          newly initdb'd databases. If you wish to apply this\n          change in an existing database, you will need to do the\n          following:</p>",
    "<div class=\"procedure\">\n            <ol class=\"procedure\" type=\"1\">\n              <li class=\"step\">\n                <p>Restart the postmaster after adding <code class=\"literal\">allow_system_table_mods = true</code> to\n                <code class=\"filename\">postgresql.conf</code>. (In\n                versions supporting <code class=\"command\">ALTER\n                SYSTEM</code>, you can use that to make the\n                configuration change, but you'll still need a\n                restart.)</p>\n              </li>\n              <li class=\"step\">\n                <p>In <span class=\"emphasis\"><em>each</em></span>\n                database of the cluster, run the following commands\n                as superuser:</p>\n                <pre class=\"programlisting\">\n                SET search_path = pg_catalog;\nCREATE OR REPLACE VIEW pg_user_mappings AS\n    SELECT\n        U.oid       AS umid,\n        S.oid       AS srvid,\n        S.srvname   AS srvname,\n        U.umuser    AS umuser,\n        CASE WHEN U.umuser = 0 THEN\n            'public'\n        ELSE\n            A.rolname\n        END AS usename,\n        CASE WHEN (U.umuser &lt;&gt; 0 AND A.rolname = current_user\n                     AND (pg_has_role(S.srvowner, 'USAGE')\n                          OR has_server_privilege(S.oid, 'USAGE')))\n                    OR (U.umuser = 0 AND pg_has_role(S.srvowner, 'USAGE'))\n                    OR (SELECT rolsuper FROM pg_authid WHERE rolname = current_user)\n                    THEN U.umoptions\n                 ELSE NULL END AS umoptions\n    FROM pg_user_mapping U\n         LEFT JOIN pg_authid A ON (A.oid = U.umuser) JOIN\n        pg_foreign_server S ON (U.umserver = S.oid);</pre>\n              </li>\n              <li class=\"step\">\n                <p>Do not forget to include the <code class=\"literal\">template0</code> and <code class=\"literal\">template1</code> databases, or the\n                vulnerability will still exist in databases you\n                create later. To fix <code class=\"literal\">template0</code>, you'll need to\n                temporarily make it accept connections. In\n                <span class=\"productname\">PostgreSQL</span> 9.5 and\n                later, you can use</p>\n                <pre class=\"programlisting\">\n                ALTER DATABASE template0 WITH ALLOW_CONNECTIONS true;</pre>\n                <p>and then after fixing <code class=\"literal\">template0</code>, undo that with</p>\n                <pre class=\"programlisting\">\n                ALTER DATABASE template0 WITH ALLOW_CONNECTIONS false;</pre>\n                <p>In prior versions, instead use</p>\n                <pre class=\"programlisting\">\n                UPDATE pg_database SET datallowconn = true WHERE datname = 'template0';\nUPDATE pg_database SET datallowconn = false WHERE datname = 'template0';</pre>\n              </li>\n              <li class=\"step\">\n                <p>Finally, remove the <code class=\"literal\">allow_system_table_mods</code>\n                configuration setting, and again restart the\n                postmaster.</p>\n              </li>\n            </ol>\n          </div>"
  ],
  [
    "<p>Disallow empty passwords in all password-based\n          authentication methods (Heikki Linnakangas)</p>",
    "<p><span class=\"application\">libpq</span> ignores empty\n          password specifications, and does not transmit them to\n          the server. So, if a user's password has been set to the\n          empty string, it's impossible to log in with that\n          password via <span class=\"application\">psql</span> or\n          other <span class=\"application\">libpq</span>-based\n          clients. An administrator might therefore believe that\n          setting the password to empty is equivalent to disabling\n          password login. However, with a modified or\n          non-<span class=\"application\">libpq</span>-based client,\n          logging in could be possible, depending on which\n          authentication method is configured. In particular the\n          most common method, <code class=\"literal\">md5</code>,\n          accepted empty passwords. Change the server to reject\n          empty passwords in all cases. (CVE-2017-7546)</p>"
  ],
  [
    "<p>Fix concurrent locking of tuple update chains (&#xC1;lvaro\n          Herrera)</p>",
    "<p>If several sessions concurrently lock a tuple update\n          chain with nonconflicting lock modes using an old\n          snapshot, and they all succeed, it was possible for some\n          of them to nonetheless fail (and conclude there is no\n          live tuple version) due to a race condition. This had\n          consequences such as foreign-key checks failing to see a\n          tuple that definitely exists but is being updated\n          concurrently.</p>"
  ],
  [
    "<p>Fix potential data corruption when freezing a tuple\n          whose XMAX is a multixact with exactly one\n          still-interesting member (Teodor Sigaev)</p>"
  ],
  [
    "<p>On Windows, retry process creation if we fail to\n          reserve the address range for our shared memory in the\n          new process (Tom Lane, Amit Kapila)</p>",
    "<p>This is expected to fix infrequent\n          child-process-launch failures that are probably due to\n          interference from antivirus products.</p>"
  ],
  [
    "<p>Fix low-probability corruption of shared\n          predicate-lock hash table in Windows builds (Thomas\n          Munro, Tom Lane)</p>"
  ],
  [
    "<p>Avoid logging clean closure of an SSL connection as\n          though it were a connection reset (Michael Paquier)</p>"
  ],
  [
    "<p>Prevent sending SSL session tickets to clients (Tom\n          Lane)</p>",
    "<p>This fix prevents reconnection failures with\n          ticket-aware client-side SSL code.</p>"
  ],
  [
    "<p>Fix code for setting <a class=\"xref\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-connection.html#GUC-TCP-KEEPALIVES-IDLE\">tcp_keepalives_idle</a>\n          on Solaris (Tom Lane)</p>"
  ],
  [
    "<p>Fix statistics collector to honor inquiry messages\n          issued just after a postmaster shutdown and immediate\n          restart (Tom Lane)</p>",
    "<p>Statistics inquiries issued within half a second of\n          the previous postmaster shutdown were effectively\n          ignored.</p>"
  ],
  [
    "<p>Ensure that the statistics collector's receive buffer\n          size is at least 100KB (Tom Lane)</p>",
    "<p>This reduces the risk of dropped statistics data on\n          older platforms whose default receive buffer size is less\n          than that.</p>"
  ],
  [
    "<p>Fix possible creation of an invalid WAL segment when a\n          standby is promoted just after it processes an\n          <code class=\"literal\">XLOG_SWITCH</code> WAL record\n          (Andres Freund)</p>"
  ],
  [
    "<p>Fix <span class=\"systemitem\">SIGHUP</span> and\n          <span class=\"systemitem\">SIGUSR1</span> handling in\n          walsender processes (Petr Jelinek, Andres Freund)</p>"
  ],
  [
    "<p>Fix unnecessarily slow restarts of <span class=\"application\">walreceiver</span> processes due to race\n          condition in postmaster (Tom Lane)</p>"
  ],
  [
    "<p>Fix cases where an <code class=\"command\">INSERT</code>\n          or <code class=\"command\">UPDATE</code> assigns to more\n          than one element of a column that is of domain-over-array\n          type (Tom Lane)</p>"
  ],
  [
    "<p>Allow window functions to be used in sub-<code class=\"literal\">SELECT</code>s that are within the arguments of\n          an aggregate function (Tom Lane)</p>"
  ],
  [
    "<p>Move autogenerated array types out of the way during\n          <code class=\"command\">ALTER ... RENAME</code> (Vik\n          Fearing)</p>",
    "<p>Previously, we would rename a conflicting\n          autogenerated array type out of the way during\n          <code class=\"command\">CREATE</code>; this fix extends\n          that behavior to renaming operations.</p>"
  ],
  [
    "<p>Ensure that <code class=\"command\">ALTER USER ...\n          SET</code> accepts all the syntax variants that\n          <code class=\"command\">ALTER ROLE ... SET</code> does\n          (Peter Eisentraut)</p>"
  ],
  [
    "<p>Properly update dependency info when changing a\n          datatype I/O function's argument or return type from\n          <code class=\"type\">opaque</code> to the correct type\n          (Heikki Linnakangas)</p>",
    "<p><code class=\"command\">CREATE TYPE</code> updates I/O\n          functions declared in this long-obsolete style, but it\n          forgot to record a dependency on the type, allowing a\n          subsequent <code class=\"command\">DROP TYPE</code> to\n          leave broken function definitions behind.</p>"
  ],
  [
    "<p>Reduce memory usage when <code class=\"command\">ANALYZE</code> processes a <code class=\"type\">tsvector</code> column (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Fix unnecessary precision loss and sloppy rounding\n          when multiplying or dividing <code class=\"type\">money</code> values by integers or floats (Tom\n          Lane)</p>"
  ],
  [
    "<p>Tighten checks for whitespace in functions that parse\n          identifiers, such as <code class=\"function\">regprocedurein()</code> (Tom Lane)</p>",
    "<p>Depending on the prevailing locale, these functions\n          could misinterpret fragments of multibyte characters as\n          whitespace.</p>"
  ],
  [
    "<p>Use relevant <code class=\"literal\">#define</code>\n          symbols from Perl while compiling <span class=\"application\">PL/Perl</span> (Ashutosh Sharma, Tom\n          Lane)</p>",
    "<p>This avoids portability problems, typically\n          manifesting as a <span class=\"quote\">&#x201C;<span class=\"quote\">handshake</span>&#x201D;</span> mismatch during library\n          load, when working with recent Perl versions.</p>"
  ],
  [
    "<p>In <span class=\"application\">libpq</span>, reset\n          GSS/SASL and SSPI authentication state properly after a\n          failed connection attempt (Michael Paquier)</p>",
    "<p>Failure to do this meant that when falling back from\n          SSL to non-SSL connections, a GSS/SASL failure in the SSL\n          attempt would always cause the non-SSL attempt to fail.\n          SSPI did not fail, but it leaked memory.</p>"
  ],
  [
    "<p>In <span class=\"application\">psql</span>, fix failure\n          when <code class=\"command\">COPY FROM STDIN</code> is\n          ended with a keyboard EOF signal and then another\n          <code class=\"command\">COPY FROM STDIN</code> is attempted\n          (Thomas Munro)</p>",
    "<p>This misbehavior was observed on BSD-derived platforms\n          (including macOS), but not on most others.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_dump</span> and\n          <span class=\"application\">pg_restore</span> to emit\n          <code class=\"command\">REFRESH MATERIALIZED VIEW</code>\n          commands last (Tom Lane)</p>",
    "<p>This prevents errors during dump/restore when a\n          materialized view refers to tables owned by a different\n          user.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_dump</span> with the\n          <code class=\"option\">--clean</code> option to drop event\n          triggers as expected (Tom Lane)</p>",
    "<p>It also now correctly assigns ownership of event\n          triggers; before, they were restored as being owned by\n          the superuser running the restore script.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_dump</span> to not\n          emit invalid SQL for an empty operator class (Daniel\n          Gustafsson)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_dump</span> output to\n          stdout on Windows (Kuntal Ghosh)</p>",
    "<p>A compressed plain-text dump written to stdout would\n          contain corrupt data due to failure to put the file\n          descriptor into binary mode.</p>"
  ],
  [
    "<p>Fix <code class=\"function\">pg_get_ruledef()</code> to\n          print correct output for the <code class=\"literal\">ON\n          SELECT</code> rule of a view whose columns have been\n          renamed (Tom Lane)</p>",
    "<p>In some corner cases, <span class=\"application\">pg_dump</span> relies on <code class=\"function\">pg_get_ruledef()</code> to dump views, so that\n          this error could result in dump/reload failures.</p>"
  ],
  [
    "<p>Fix dumping of outer joins with empty constraints,\n          such as the result of a <code class=\"literal\">NATURAL\n          LEFT JOIN</code> with no common columns (Tom Lane)</p>"
  ],
  [
    "<p>Fix dumping of function expressions in the\n          <code class=\"literal\">FROM</code> clause in cases where\n          the expression does not deparse into something that looks\n          like a function call (Tom Lane)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_basebackup</span>\n          output to stdout on Windows (Haribabu Kommi)</p>",
    "<p>A backup written to stdout would contain corrupt data\n          due to failure to put the file descriptor into binary\n          mode.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_upgrade</span> to\n          ensure that the ending WAL record does not have <a class=\"xref\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-wal.html#GUC-WAL-LEVEL\">wal_level</a> =\n          <code class=\"literal\">minimum</code> (Bruce Momjian)</p>",
    "<p>This condition could prevent upgraded standby servers\n          from reconnecting.</p>"
  ],
  [
    "<p>In <code class=\"filename\">postgres_fdw</code>,\n          re-establish connections to remote servers after\n          <code class=\"command\">ALTER SERVER</code> or <code class=\"command\">ALTER USER MAPPING</code> commands (Kyotaro\n          Horiguchi)</p>",
    "<p>This ensures that option changes affecting connection\n          parameters will be applied promptly.</p>"
  ],
  [
    "<p>In <code class=\"filename\">postgres_fdw</code>, allow\n          cancellation of remote transaction control commands\n          (Robert Haas, Rafia Sabih)</p>",
    "<p>This change allows us to quickly escape a wait for an\n          unresponsive remote server in many more cases than\n          previously.</p>"
  ],
  [
    "<p>Always use <code class=\"option\">-fPIC</code>, not\n          <code class=\"option\">-fpic</code>, when building shared\n          libraries with gcc (Tom Lane)</p>",
    "<p>This supports larger extension libraries on platforms\n          where it makes a difference.</p>"
  ],
  [
    "<p>Fix unescaped-braces issue in our build scripts for\n          Microsoft MSVC, to avoid a warning or error from recent\n          Perl versions (Andrew Dunstan)</p>"
  ],
  [
    "<p>In MSVC builds, handle the case where the <span class=\"application\">openssl</span> library is not within a\n          <code class=\"filename\">VC</code> subdirectory (Andrew\n          Dunstan)</p>"
  ],
  [
    "<p>In MSVC builds, add proper include path for\n          <span class=\"application\">libxml2</span> header files\n          (Andrew Dunstan)</p>",
    "<p>This fixes a former need to move things around in\n          standard Windows installations of <span class=\"application\">libxml2</span>.</p>"
  ],
  [
    "<p>In MSVC builds, recognize a Tcl library that is named\n          <code class=\"filename\">tcl86.lib</code> (Noah Misch)</p>"
  ]
]