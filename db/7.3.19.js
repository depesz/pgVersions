[
  [
    "<p>Support explicit placement of the temporary-table\n          schema within <code class=\"varname\">search_path</code>,\n          and disable searching it for functions and operators\n          (Tom)</p>",
    "<p>This is needed to allow a security-definer function to\n          set a truly secure value of <code class=\"varname\">search_path</code>. Without it, an unprivileged\n          SQL user can use temporary objects to execute code with\n          the privileges of the security-definer function\n          (CVE-2007-2138). See <code class=\"command\">CREATE\n          FUNCTION</code> for more information.</p>"
  ],
  [
    "<p>Fix potential-data-corruption bug in how <code class=\"command\">VACUUM FULL</code> handles <code class=\"command\">UPDATE</code> chains (Tom, Pavan Deolasee)</p>"
  ]
]