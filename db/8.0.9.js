[
  [
    "<p>Fix crash when referencing <code class=\"literal\">NEW</code> row values in rule WHERE expressions\n          (Tom)</p>"
  ],
  [
    "<p>Fix core dump when an untyped literal is taken as\n          ANYARRAY</p>"
  ],
  [
    "<p>Fix mishandling of AFTER triggers when query contains\n          a SQL function returning multiple rows (Tom)</p>"
  ],
  [
    "<p>Fix <code class=\"command\">ALTER TABLE ... TYPE</code>\n          to recheck <code class=\"literal\">NOT NULL</code> for\n          <code class=\"literal\">USING</code> clause (Tom)</p>"
  ],
  [
    "<p>Fix <code class=\"function\">string_to_array()</code> to\n          handle overlapping matches for the separator string</p>",
    "<p>For example, <code class=\"literal\">string_to_array('123xx456xxx789',\n          'xx')</code>.</p>"
  ],
  [
    "<p>Fix corner cases in pattern matching for <span class=\"application\">psql</span>'s <code class=\"literal\">\\d</code> commands</p>"
  ],
  [
    "<p>Fix index-corrupting bugs in /contrib/ltree\n          (Teodor)</p>"
  ],
  [
    "<p>Numerous robustness fixes in <span class=\"application\">ecpg</span> (Joachim Wieland)</p>"
  ],
  [
    "<p>Fix backslash escaping in /contrib/dbmirror</p>"
  ],
  [
    "<p>Fix instability of statistics collection on Win32\n          (Tom, Andrew)</p>"
  ],
  [
    "<p>Fixes for <span class=\"systemitem\">AIX</span> and\n          <span class=\"productname\">Intel</span> compilers\n          (Tom)</p>"
  ]
]