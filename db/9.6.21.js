[
  [
    "<p>Fix <tt class=\"COMMAND\">CREATE INDEX CONCURRENTLY</tt> to wait for concurrent prepared transactions (Andrey Borodin)</p>",
    "<p>At the point where <tt class=\"COMMAND\">CREATE INDEX CONCURRENTLY</tt> waits for all concurrent transactions to complete so that it can see rows they inserted, it must also wait for all prepared transactions to complete, for the same reason. Its failure to do so meant that rows inserted by prepared transactions might be omitted from the new index, causing queries relying on the index to miss such rows. In installations that have enabled prepared transactions (<tt class=\"VARNAME\">max_prepared_transactions</tt> &gt; 0), it's recommended to reindex any concurrently-built indexes in case this problem occurred when they were built.</p>"
  ],
  [
    "<p>Avoid incorrect results when <tt class=\"LITERAL\">WHERE CURRENT OF</tt> is applied to a cursor whose plan contains a MergeAppend node (Tom Lane)</p>",
    "<p>This case is unsupported (in general, a cursor using <tt class=\"LITERAL\">ORDER BY</tt> is not guaranteed to be simply updatable); but the code previously did not reject it, and could silently give false matches.</p>"
  ],
  [
    "<p>Fix crash when <tt class=\"LITERAL\">WHERE CURRENT OF</tt> is applied to a cursor whose plan contains a custom scan node (David Geier)</p>"
  ],
  [
    "<p>Fix planner's handling of a placeholder that is computed at some join level and used only at that same level (Tom Lane)</p>",
    "<p>This oversight could lead to <span class=\"QUOTE\">\"failed to build any <tt class=\"REPLACEABLE c2\">N</tt>-way joins\"</span> planner errors.</p>"
  ],
  [
    "<p>Be more careful about whether index AMs support mark/restore (Andrew Gierth)</p>",
    "<p>This prevents errors about missing support functions in rare edge cases.</p>"
  ],
  [
    "<p>Fix <tt class=\"COMMAND\">ALTER DEFAULT PRIVILEGES</tt> to handle duplicated arguments safely (Michael Paquier)</p>",
    "<p>Duplicate role or schema names within the same command could lead to <span class=\"QUOTE\">\"tuple already updated by self\"</span> errors or unique-constraint violations.</p>"
  ],
  [
    "<p>Flush ACL-related caches when <tt class=\"STRUCTNAME\">pg_authid</tt> changes (Noah Misch)</p>",
    "<p>This change ensures that permissions-related decisions will promptly reflect the results of <tt class=\"COMMAND\">ALTER ROLE ... [NO] INHERIT</tt>.</p>"
  ],
  [
    "<p>Prevent misprocessing of ambiguous <tt class=\"COMMAND\">CREATE TABLE LIKE</tt> clauses (Tom Lane)</p>",
    "<p>A <tt class=\"LITERAL\">LIKE</tt> clause is re-examined after initial creation of the new table, to handle importation of indexes and such. It was possible for this re-examination to find a different table of the same name, causing unexpected behavior; one example is where the new table is a temporary table of the same name as the <tt class=\"LITERAL\">LIKE</tt> target.</p>"
  ],
  [
    "<p>Rearrange order of operations in <tt class=\"COMMAND\">CREATE TABLE LIKE</tt> so that indexes are cloned before building foreign key constraints (Tom Lane)</p>",
    "<p>This fixes the case where a self-referential foreign key constraint declared in the outer <tt class=\"COMMAND\">CREATE TABLE</tt> depends on an index that's coming from the <tt class=\"LITERAL\">LIKE</tt> clause.</p>"
  ],
  [
    "<p>Disallow converting an inheritance child table to a view (Tom Lane)</p>"
  ],
  [
    "<p>Ensure that disk space allocated for a dropped relation is released promptly at commit (Thomas Munro)</p>",
    "<p>Previously, if the dropped relation spanned multiple 1GB segments, only the first segment was truncated immediately. Other segments were simply unlinked, which doesn't authorize the kernel to release the storage so long as any other backends still have the files open.</p>"
  ],
  [
    "<p>Fix handling of backslash-escaped multibyte characters in <tt class=\"COMMAND\">COPY FROM</tt> (Heikki Linnakangas)</p>",
    "<p>A backslash followed by a multibyte character was not handled correctly. In some client character encodings, this could lead to misinterpreting part of a multibyte character as a field separator or end-of-copy-data marker.</p>"
  ],
  [
    "<p>Avoid preallocating executor hash tables in <tt class=\"COMMAND\">EXPLAIN</tt> without <tt class=\"LITERAL\">ANALYZE</tt> (Alexey Bashtanov)</p>"
  ],
  [
    "<p>Fix recently-introduced race conditions in <tt class=\"COMMAND\">LISTEN</tt>/<tt class=\"COMMAND\">NOTIFY</tt> queue handling (Tom Lane)</p>",
    "<p>A newly-listening backend could attempt to read SLRU pages that were in process of being truncated, possibly causing an error.</p>",
    "<p>The queue tail pointer could become set to a value that's not equal to the queue position of any backend, resulting in effective disabling of the queue truncation logic. Continued use of <tt class=\"COMMAND\">NOTIFY</tt> then led to queue-fill warnings, and eventually to inability to send any more notifies until the server is restarted.</p>"
  ],
  [
    "<p>Allow the <tt class=\"TYPE\">jsonb</tt> concatenation operator to handle all combinations of JSON data types (Tom Lane)</p>",
    "<p>We can concatenate two JSON objects or two JSON arrays. Handle other cases by wrapping non-array inputs in one-element arrays, then performing an array concatenation. Previously, some combinations of inputs followed this rule but others arbitrarily threw an error.</p>"
  ],
  [
    "<p>Fix use of uninitialized value while parsing a <tt class=\"LITERAL\">*</tt> quantifier in a BRE-mode regular expression (Tom Lane)</p>",
    "<p>This error could cause the quantifier to act non-greedy, that is behave like a <tt class=\"LITERAL\">*?</tt> quantifier would do in full regular expressions.</p>"
  ],
  [
    "<p>Fix numeric <code class=\"FUNCTION\">power()</code> for the case where the exponent is exactly <tt class=\"LITERAL\">INT_MIN</tt> (-2147483648) (Dean Rasheed)</p>",
    "<p>Previously, a result with no significant digits was produced.</p>"
  ],
  [
    "<p>Prevent possible data loss from incorrect detection of the wraparound point of an SLRU log (Noah Misch)</p>",
    "<p>The wraparound point typically falls in the middle of a page, which must be rounded off to a page boundary, and that was not done correctly. No issue could arise unless an installation had gotten to within one page of SLRU overflow, which is unlikely in a properly-functioning system. If this did happen, it would manifest in later <span class=\"QUOTE\">\"apparent wraparound\"</span> or <span class=\"QUOTE\">\"could not access status of transaction\"</span> errors.</p>"
  ],
  [
    "<p>Fix memory leak in walsender processes while sending new snapshots for logical decoding (Amit Kapila)</p>"
  ],
  [
    "<p>Fix walsender to accept additional commands after terminating replication (Jeff Davis)</p>"
  ],
  [
    "<p>Ensure detection of deadlocks between hot standby backends and the startup (WAL-application) process (Fujii Masao)</p>",
    "<p>The startup process did not run the deadlock detection code, so that in situations where the startup process is last to join a circular wait situation, the deadlock might never be recognized.</p>"
  ],
  [
    "<p>Fix portability problem in parsing of <tt class=\"VARNAME\">recovery_target_xid</tt> values (Michael Paquier)</p>",
    "<p>The target XID is potentially 64 bits wide, but it was parsed with <code class=\"FUNCTION\">strtoul()</code>, causing misbehavior on platforms where <tt class=\"TYPE\">long</tt> is 32 bits (such as Windows).</p>"
  ],
  [
    "<p>Avoid assertion failure in <code class=\"FUNCTION\">pg_get_functiondef()</code> when examining a function with a <tt class=\"LITERAL\">TRANSFORM</tt> option (Tom Lane)</p>"
  ],
  [
    "<p>In <span class=\"APPLICATION\">psql</span>, re-allow including a password in a <tt class=\"REPLACEABLE c2\">connection_string</tt> argument of a <tt class=\"COMMAND\">\\connect</tt> command (Tom Lane)</p>",
    "<p>This used to work, but a recent bug fix caused the password to be ignored (resulting in prompting for a password).</p>"
  ],
  [
    "<p>Fix assorted bugs in <span class=\"APPLICATION\">psql</span>'s <tt class=\"COMMAND\">\\help</tt> command (Kyotaro Horiguchi, Tom Lane)</p>",
    "<p><tt class=\"COMMAND\">\\help</tt> with two argument words failed to find a command description using only the first word, for example <tt class=\"LITERAL\">\\help reset all</tt> should show the help for <tt class=\"COMMAND\">RESET</tt> but did not. Also, <tt class=\"COMMAND\">\\help</tt> often failed to invoke the pager when it should. It also leaked memory.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_dump</span> to handle <tt class=\"LITERAL\">WITH GRANT OPTION</tt> in an extension's initial privileges (Noah Misch)</p>",
    "<p>If an extension's script creates an object and grants privileges on it with grant option, then later the user revokes such privileges, <span class=\"APPLICATION\">pg_dump</span> would generate incorrect SQL for reproducing the situation. (Few if any extensions do this today.)</p>"
  ],
  [
    "<p>In <span class=\"APPLICATION\">pg_rewind</span>, ensure that all WAL is accounted for when rewinding a standby server (Ian Barwick, Heikki Linnakangas)</p>"
  ],
  [
    "<p>Report the correct database name in connection failure error messages from some client programs (Álvaro Herrera)</p>",
    "<p>If the database name was defaulted rather than given on the command line, <span class=\"APPLICATION\">pg_dumpall</span>, <span class=\"APPLICATION\">pgbench</span>, <span class=\"APPLICATION\">oid2name</span>, and <span class=\"APPLICATION\">vacuumlo</span> would produce misleading error messages after a connection failure.</p>"
  ],
  [
    "<p>Fix memory leak in <tt class=\"FILENAME\">contrib/auto_explain</tt> (Japin Li)</p>",
    "<p>Memory consumed while producing the <tt class=\"LITERAL\">EXPLAIN</tt> output was not freed until the end of the current transaction (for a top-level statement) or the end of the surrounding statement (for a nested statement). This was particularly a problem with <tt class=\"VARNAME\">log_nested_statements</tt> enabled.</p>"
  ],
  [
    "<p>In <tt class=\"FILENAME\">contrib/postgres_fdw</tt>, avoid leaking open connections to remote servers when a user mapping or foreign server object is dropped (Bharath Rupireddy)</p>",
    "<p>Open connections that depend on a dropped user mapping or foreign server can no longer be referenced, but formerly they were kept around anyway for the duration of the local session.</p>"
  ],
  [
    "<p>In <tt class=\"FILENAME\">contrib/pgcrypto</tt>, check for error returns from OpenSSL's EVP functions (Michael Paquier)</p>",
    "<p>We do not really expect errors here, but this change silences warnings from static analysis tools.</p>"
  ],
  [
    "<p>In <tt class=\"FILENAME\">contrib/pg_trgm</tt>'s GiST index support, avoid crash in the rare case that picksplit is called on exactly two index items (Andrew Gierth, Alexander Korotkov)</p>"
  ],
  [
    "<p>Fix miscalculation of timeouts in <tt class=\"FILENAME\">contrib/pg_prewarm</tt> and <tt class=\"FILENAME\">contrib/postgres_fdw</tt> (Alexey Kondratov, Tom Lane)</p>",
    "<p>The main loop in <tt class=\"FILENAME\">contrib/pg_prewarm</tt>'s autoprewarm parent process underestimated its desired sleep time by a factor of 1000, causing it to consume much more CPU than intended. When waiting for a result from a remote server, <tt class=\"FILENAME\">contrib/postgres_fdw</tt> overestimated the desired timeout by a factor of 1000 (though this error had been mitigated by imposing a clamp to 60 seconds).</p>",
    "<p>Both of these errors stemmed from incorrectly converting seconds-and-microseconds to milliseconds. Introduce a new API <code class=\"FUNCTION\">TimestampDifferenceMilliseconds()</code> to make it easier to get this right in the future.</p>"
  ],
  [
    "<p>Improve <span class=\"APPLICATION\">configure</span>'s heuristics for selecting <tt class=\"VARNAME\">PG_SYSROOT</tt> on macOS (Tom Lane)</p>",
    "<p>The new method is more likely to produce desirable results when Xcode is newer than the underlying operating system. Choosing a sysroot that does not match the OS version may result in nonfunctional executables.</p>"
  ],
  [
    "<p>While building on macOS, specify <tt class=\"OPTION\">-isysroot</tt> in link steps as well as compile steps (James Hilliard)</p>",
    "<p>This likewise improves the results when Xcode is out of sync with the operating system.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"APPLICATION\">tzdata</span> release 2021a for DST law changes in Russia (Volgograd zone) and South Sudan, plus historical corrections for Australia, Bahamas, Belize, Bermuda, Ghana, Israel, Kenya, Nigeria, Palestine, Seychelles, and Vanuatu.</p>",
    "<p>Notably, the Australia/Currie zone has been corrected to the point where it is identical to Australia/Hobart.</p>"
  ]
]