[
  [
    "<p>Change encoding function signature to prevent\n          misuse</p>"
  ],
  [
    "<p>Repair ancient race condition that allowed a\n          transaction to be seen as committed for some purposes (eg\n          SELECT FOR UPDATE) slightly sooner than for other\n          purposes</p>",
    "<p>This is an extremely serious bug since it could lead\n          to apparent data inconsistencies being briefly visible to\n          applications.</p>"
  ],
  [
    "<p>Repair race condition between relation extension and\n          VACUUM</p>",
    "<p>This could theoretically have caused loss of a page's\n          worth of freshly-inserted data, although the scenario\n          seems of very low probability. There are no known cases\n          of it having caused more than an Assert failure.</p>"
  ],
  [
    "<p>Fix comparisons of <code class=\"type\">TIME WITH TIME\n          ZONE</code> values</p>",
    "<p>The comparison code was wrong in the case where the\n          <code class=\"literal\">--enable-integer-datetimes</code>\n          configuration switch had been used. NOTE: if you have an\n          index on a <code class=\"type\">TIME WITH TIME ZONE</code>\n          column, it will need to be <code class=\"command\">REINDEX</code>ed after installing this update,\n          because the fix corrects the sort order of column\n          values.</p>"
  ],
  [
    "<p>Fix <code class=\"function\">EXTRACT(EPOCH)</code> for\n          <code class=\"type\">TIME WITH TIME ZONE</code> values</p>"
  ],
  [
    "<p>Fix mis-display of negative fractional seconds in\n          <code class=\"type\">INTERVAL</code> values</p>",
    "<p>This error only occurred when the <code class=\"literal\">--enable-integer-datetimes</code> configuration\n          switch had been used.</p>"
  ],
  [
    "<p>Additional buffer overrun checks in plpgsql (Neil)</p>"
  ],
  [
    "<p>Fix pg_dump to dump trigger names containing\n          <code class=\"literal\">%</code> correctly (Neil)</p>"
  ],
  [
    "<p>Prevent <code class=\"function\">to_char(interval)</code> from dumping core for\n          month-related formats</p>"
  ],
  [
    "<p>Fix <code class=\"filename\">contrib/pgcrypto</code> for\n          newer OpenSSL builds (Marko Kreen)</p>"
  ],
  [
    "<p>Still more 64-bit fixes for <code class=\"filename\">contrib/intagg</code></p>"
  ],
  [
    "<p>Prevent incorrect optimization of functions returning\n          <code class=\"type\">RECORD</code></p>"
  ]
]