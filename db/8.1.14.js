[
  [
    "<p>Widen local lock counters from 32 to 64 bits (Tom)</p>",
    "<p>This responds to reports that the counters could\n          overflow in sufficiently long transactions, leading to\n          unexpected <span class=\"quote\">&#x201C;<span class=\"quote\">lock\n          is already held</span>&#x201D;</span> errors.</p>"
  ],
  [
    "<p>Fix possible duplicate output of tuples during a GiST\n          index scan (Teodor)</p>"
  ],
  [
    "<p>Add checks in executor startup to ensure that the\n          tuples produced by an <code class=\"command\">INSERT</code>\n          or <code class=\"command\">UPDATE</code> will match the\n          target table's current rowtype (Tom)</p>",
    "<p><code class=\"command\">ALTER COLUMN TYPE</code>,\n          followed by re-use of a previously cached plan, could\n          produce this type of situation. The check protects\n          against data corruption and/or crashes that could\n          ensue.</p>"
  ],
  [
    "<p>Fix <code class=\"literal\">AT TIME ZONE</code> to first\n          try to interpret its timezone argument as a timezone\n          abbreviation, and only try it as a full timezone name if\n          that fails, rather than the other way around as formerly\n          (Tom)</p>",
    "<p>The timestamp input functions have always resolved\n          ambiguous zone names in this order. Making <code class=\"literal\">AT TIME ZONE</code> do so as well improves\n          consistency, and fixes a compatibility bug introduced in\n          8.1: in ambiguous cases we now behave the same as 8.0 and\n          before did, since in the older versions <code class=\"literal\">AT TIME ZONE</code> accepted <span class=\"emphasis\"><em>only</em></span> abbreviations.</p>"
  ],
  [
    "<p>Fix datetime input functions to correctly detect\n          integer overflow when running on a 64-bit platform\n          (Tom)</p>"
  ],
  [
    "<p>Improve performance of writing very long log messages\n          to syslog (Tom)</p>"
  ],
  [
    "<p>Fix bug in backwards scanning of a cursor on a\n          <code class=\"literal\">SELECT DISTINCT ON</code> query\n          (Tom)</p>"
  ],
  [
    "<p>Fix planner bug with nested sub-select expressions\n          (Tom)</p>",
    "<p>If the outer sub-select has no direct dependency on\n          the parent query, but the inner one does, the outer value\n          might not get recalculated for new parent query rows.</p>"
  ],
  [
    "<p>Fix planner to estimate that <code class=\"literal\">GROUP BY</code> expressions yielding boolean\n          results always result in two groups, regardless of the\n          expressions' contents (Tom)</p>",
    "<p>This is very substantially more accurate than the\n          regular <code class=\"literal\">GROUP BY</code> estimate\n          for certain boolean tests like <em class=\"replaceable\"><code>col</code></em> <code class=\"literal\">IS NULL</code>.</p>"
  ],
  [
    "<p>Fix PL/pgSQL to not fail when a <code class=\"literal\">FOR</code> loop's target variable is a record\n          containing composite-type fields (Tom)</p>"
  ],
  [
    "<p>Fix PL/Tcl to behave correctly with Tcl 8.5, and to be\n          more careful about the encoding of data sent to or from\n          Tcl (Tom)</p>"
  ],
  [
    "<p>Fix PL/Python to work with Python 2.5</p>",
    "<p>This is a back-port of fixes made during the 8.2\n          development cycle.</p>"
  ],
  [
    "<p>Improve <span class=\"application\">pg_dump</span> and\n          <span class=\"application\">pg_restore</span>'s error\n          reporting after failure to send a SQL command (Tom)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_ctl</span> to\n          properly preserve postmaster command-line arguments\n          across a <code class=\"literal\">restart</code> (Bruce)</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2008f (for DST law\n          changes in Argentina, Bahamas, Brazil, Mauritius,\n          Morocco, Pakistan, Palestine, and Paraguay)</p>"
  ]
]