[
  [
    "<p>Fix potential crash in <code class=\"command\">SET\n          SESSION AUTHORIZATION</code> (CVE-2006-0553)</p>",
    "<p>An unprivileged user could crash the server process,\n          resulting in momentary denial of service to other users,\n          if the server has been compiled with Asserts enabled\n          (which is not the default). Thanks to Akio Ishida for\n          reporting this problem.</p>"
  ],
  [
    "<p>Fix bug with row visibility logic in self-inserted\n          rows (Tom)</p>",
    "<p>Under rare circumstances a row inserted by the current\n          command could be seen as already valid, when it should\n          not be. Repairs bug created in 7.3.11 release.</p>"
  ],
  [
    "<p>Fix race condition that could lead to <span class=\"quote\">&#x201C;<span class=\"quote\">file already\n          exists</span>&#x201D;</span> errors during pg_clog file creation\n          (Tom)</p>"
  ],
  [
    "<p>Fix to allow restoring dumps that have cross-schema\n          references to custom operators (Tom)</p>"
  ],
  [
    "<p>Portability fix for testing presence of <code class=\"function\">finite</code> and <code class=\"function\">isinf</code> during configure (Tom)</p>"
  ]
]