[
  [
    "<p>Prevent functions in indexes from executing with the\n          privileges of the user running <code class=\"command\">VACUUM</code>, <code class=\"command\">ANALYZE</code>, etc (Tom)</p>",
    "<p>Functions used in index expressions and partial-index\n          predicates are evaluated whenever a new table entry is\n          made. It has long been understood that this poses a risk\n          of trojan-horse code execution if one modifies a table\n          owned by an untrustworthy user. (Note that triggers,\n          defaults, check constraints, etc. pose the same type of\n          risk.) But functions in indexes pose extra danger because\n          they will be executed by routine maintenance operations\n          such as <code class=\"command\">VACUUM FULL</code>, which\n          are commonly performed automatically under a superuser\n          account. For example, a nefarious user can execute code\n          with superuser privileges by setting up a trojan-horse\n          index definition and waiting for the next routine vacuum.\n          The fix arranges for standard maintenance operations\n          (including <code class=\"command\">VACUUM</code>,\n          <code class=\"command\">ANALYZE</code>, <code class=\"command\">REINDEX</code>, and <code class=\"command\">CLUSTER</code>) to execute as the table owner\n          rather than the calling user, using the same\n          privilege-switching mechanism already used for\n          <code class=\"literal\">SECURITY DEFINER</code> functions.\n          To prevent bypassing this security measure, execution of\n          <code class=\"command\">SET SESSION AUTHORIZATION</code>\n          and <code class=\"command\">SET ROLE</code> is now\n          forbidden within a <code class=\"literal\">SECURITY\n          DEFINER</code> context. (CVE-2007-6600)</p>"
  ],
  [
    "<p>Require non-superusers who use <code class=\"filename\">/contrib/dblink</code> to use only password\n          authentication, as a security measure (Joe)</p>",
    "<p>The fix that appeared for this in 7.3.20 was\n          incomplete, as it plugged the hole for only some\n          <code class=\"filename\">dblink</code> functions.\n          (CVE-2007-6601, CVE-2007-3278)</p>"
  ],
  [
    "<p>Fix potential crash in <code class=\"function\">translate()</code> when using a multibyte\n          database encoding (Tom)</p>"
  ],
  [
    "<p>Make <code class=\"filename\">contrib/tablefunc</code>'s\n          <code class=\"function\">crosstab()</code> handle NULL\n          rowid as a category in its own right, rather than\n          crashing (Joe)</p>"
  ],
  [
    "<p>Require a specific version of <span class=\"productname\">Autoconf</span> to be used when\n          re-generating the <code class=\"command\">configure</code>\n          script (Peter)</p>",
    "<p>This affects developers and packagers only. The change\n          was made to prevent accidental use of untested\n          combinations of <span class=\"productname\">Autoconf</span>\n          and <span class=\"productname\">PostgreSQL</span> versions.\n          You can remove the version check if you really want to\n          use a different <span class=\"productname\">Autoconf</span>\n          version, but it's your responsibility whether the result\n          works or not.</p>"
  ]
]