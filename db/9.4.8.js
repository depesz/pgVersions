[
  [
    "<p>Clear the OpenSSL error queue before OpenSSL calls, rather than assuming it's clear already; and make sure we leave it clear afterwards (Peter Geoghegan, Dave Vitek, Peter Eisentraut)</p>",
    "<p>This change prevents problems when there are multiple connections using OpenSSL within a single process and not all the code involved follows the same rules for when to clear the error queue. Failures have been reported specifically when a client application uses SSL connections in <span class=\"APPLICATION\">libpq</span> concurrently with SSL connections using the PHP, Python, or Ruby wrappers for OpenSSL. It's possible for similar problems to arise within the server as well, if an extension module establishes an outgoing SSL connection.</p>"
  ],
  [
    "<p>Fix <span class=\"QUOTE\">\"failed to build any <tt class=\"REPLACEABLE c2\">N</tt>-way joins\"</span> planner error with a full join enclosed in the right-hand side of a left join (Tom Lane)</p>"
  ],
  [
    "<p>Fix incorrect handling of equivalence-class tests in multilevel nestloop plans (Tom Lane)</p>",
    "<p>Given a three-or-more-way equivalence class of variables, such as <tt class=\"LITERAL\">X.X = Y.Y = Z.Z</tt>, it was possible for the planner to omit some of the tests needed to enforce that all the variables are actually equal, leading to join rows being output that didn't satisfy the <tt class=\"LITERAL\">WHERE</tt> clauses. For various reasons, erroneous plans were seldom selected in practice, so that this bug has gone undetected for a long time.</p>"
  ],
  [
    "<p>Fix query-lifespan memory leak in GIN index scans (Julien Rouhaud)</p>"
  ],
  [
    "<p>Fix query-lifespan memory leak and potential index corruption hazard in GIN index insertion (Tom Lane)</p>",
    "<p>The memory leak would typically not amount to much in simple queries, but it could be very substantial during a large GIN index build with high <tt class=\"VARNAME\">maintenance_work_mem</tt>.</p>"
  ],
  [
    "<p>Fix possible misbehavior of <tt class=\"LITERAL\">TH</tt>, <tt class=\"LITERAL\">th</tt>, and <tt class=\"LITERAL\">Y,YYY</tt> format codes in <code class=\"FUNCTION\">to_timestamp()</code> (Tom Lane)</p>",
    "<p>These could advance off the end of the input string, causing subsequent format codes to read garbage.</p>"
  ],
  [
    "<p>Fix dumping of rules and views in which the <tt class=\"REPLACEABLE c2\">array</tt> argument of a <tt class=\"LITERAL\"><tt class=\"REPLACEABLE c2\">value</tt> <tt class=\"REPLACEABLE c2\">operator</tt> ANY (<tt class=\"REPLACEABLE c2\">array</tt>)</tt> construct is a sub-SELECT (Tom Lane)</p>"
  ],
  [
    "<p>Disallow newlines in <tt class=\"COMMAND\">ALTER SYSTEM</tt> parameter values (Tom Lane)</p>",
    "<p>The configuration-file parser doesn't support embedded newlines in string literals, so we mustn't allow them in values to be inserted by <tt class=\"COMMAND\">ALTER SYSTEM</tt>.</p>"
  ],
  [
    "<p>Fix <tt class=\"COMMAND\">ALTER TABLE ... REPLICA IDENTITY USING INDEX</tt> to work properly if an index on OID is selected (David Rowley)</p>"
  ],
  [
    "<p>Fix crash in logical decoding on alignment-picky platforms (Tom Lane, Andres Freund)</p>",
    "<p>The failure occurred only with a transaction large enough to spill to disk and a primary-key change within that transaction.</p>"
  ],
  [
    "<p>Avoid repeated requests for feedback from receiver while shutting down walsender (Nick Cleaton)</p>"
  ],
  [
    "<p>Make <span class=\"APPLICATION\">pg_regress</span> use a startup timeout from the <tt class=\"ENVAR\">PGCTLTIMEOUT</tt> environment variable, if that's set (Tom Lane)</p>",
    "<p>This is for consistency with a behavior recently added to <span class=\"APPLICATION\">pg_ctl</span>; it eases automated testing on slow machines.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_upgrade</span> to correctly restore extension membership for operator families containing only one operator class (Tom Lane)</p>",
    "<p>In such a case, the operator family was restored into the new database, but it was no longer marked as part of the extension. This had no immediate ill effects, but would cause later <span class=\"APPLICATION\">pg_dump</span> runs to emit output that would cause (harmless) errors on restore.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_upgrade</span> to not fail when new-cluster TOAST rules differ from old (Tom Lane)</p>",
    "<p><span class=\"APPLICATION\">pg_upgrade</span> had special-case code to handle the situation where the new <span class=\"PRODUCTNAME\">PostgreSQL</span> version thinks that a table should have a TOAST table while the old version did not. That code was broken, so remove it, and instead do nothing in such cases; there seems no reason to believe that we can't get along fine without a TOAST table if that was okay according to the old version's rules.</p>"
  ],
  [
    "<p>Reduce the number of SysV semaphores used by a build configured with <tt class=\"OPTION\">--disable-spinlocks</tt> (Tom Lane)</p>"
  ],
  [
    "<p>Rename internal function <code class=\"FUNCTION\">strtoi()</code> to <code class=\"FUNCTION\">strtoint()</code> to avoid conflict with a NetBSD library function (Thomas Munro)</p>"
  ],
  [
    "<p>Fix reporting of errors from <code class=\"FUNCTION\">bind()</code> and <code class=\"FUNCTION\">listen()</code> system calls on Windows (Tom Lane)</p>"
  ],
  [
    "<p>Reduce verbosity of compiler output when building with Microsoft Visual Studio (Christian Ullrich)</p>"
  ],
  [
    "<p>Fix <code class=\"FUNCTION\">putenv()</code> to work properly with Visual Studio 2013 (Michael Paquier)</p>"
  ],
  [
    "<p>Avoid possibly-unsafe use of Windows' <code class=\"FUNCTION\">FormatMessage()</code> function (Christian Ullrich)</p>",
    "<p>Use the <tt class=\"LITERAL\">FORMAT_MESSAGE_IGNORE_INSERTS</tt> flag where appropriate. No live bug is known to exist here, but it seems like a good idea to be careful.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"APPLICATION\">tzdata</span> release 2016d for DST law changes in Russia and Venezuela. There are new zone names <tt class=\"LITERAL\">Europe/Kirov</tt> and <tt class=\"LITERAL\">Asia/Tomsk</tt> to reflect the fact that these regions now have different time zone histories from adjacent regions.</p>"
  ]
]