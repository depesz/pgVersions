[
  [
    "<p>Preserve the <tt class=\"STRUCTFIELD\">indisclustered</tt> setting of indexes rewritten by <tt class=\"COMMAND\">ALTER TABLE</tt> (Amit Langote, Justin Pryzby)</p>",
    "<p>Previously, <tt class=\"COMMAND\">ALTER TABLE</tt> lost track of which index had been used for <tt class=\"COMMAND\">CLUSTER</tt>.</p>"
  ],
  [
    "<p>Preserve the replica identity properties of indexes rewritten by <tt class=\"COMMAND\">ALTER TABLE</tt> (Quan Zongliang, Peter Eisentraut)</p>"
  ],
  [
    "<p>Lock objects sooner during <tt class=\"COMMAND\">DROP OWNED BY</tt> (Álvaro Herrera)</p>",
    "<p>This avoids failures in race-condition cases where another session is deleting some of the same objects.</p>"
  ],
  [
    "<p>Fix error-case processing for <tt class=\"COMMAND\">CREATE ROLE ... IN ROLE</tt> (Andrew Gierth)</p>",
    "<p>Some error cases would be reported as <span class=\"QUOTE\">\"unexpected node type\"</span> or the like, instead of the intended message.</p>"
  ],
  [
    "<p>Fix full text search to handle NOT above a phrase search correctly (Tom Lane)</p>",
    "<p>Queries such as <tt class=\"LITERAL\">!(foo&lt;-&gt;bar)</tt> failed to find matching rows when implemented as a GiST or GIN index search.</p>"
  ],
  [
    "<p>Fix full text search for cases where a phrase search includes an item with both prefix matching and a weight restriction (Tom Lane)</p>"
  ],
  [
    "<p>Fix <code class=\"FUNCTION\">ts_headline()</code> to make better headline selections when working with phrase queries (Tom Lane)</p>"
  ],
  [
    "<p>Fix bugs in <tt class=\"VARNAME\">gin_fuzzy_search_limit</tt> processing (Adé Heyward, Tom Lane)</p>",
    "<p>A small value of <tt class=\"VARNAME\">gin_fuzzy_search_limit</tt> could result in unexpected slowness due to unintentionally rescanning the same index page many times. Another code path failed to apply the intended filtering at all, possibly returning too many values.</p>"
  ],
  [
    "<p>Allow input of type <tt class=\"TYPE\">circle</tt> to accept the format <span class=\"QUOTE\">\"<tt class=\"LITERAL\">(<tt class=\"REPLACEABLE c2\">x</tt>,<tt class=\"REPLACEABLE c2\">y</tt>),<tt class=\"REPLACEABLE c2\">r</tt></tt>\"</span> as the documentation says it does (David Zhang)</p>"
  ],
  [
    "<p>Make the <code class=\"FUNCTION\">get_bit()</code> and <code class=\"FUNCTION\">set_bit()</code> functions cope with <tt class=\"TYPE\">bytea</tt> strings longer than 256MB (Movead Li)</p>",
    "<p>Since the bit number argument is only <tt class=\"TYPE\">int4</tt>, it's impossible to use these functions to access bits beyond the first 256MB of a long <tt class=\"TYPE\">bytea</tt>. We'll widen the argument to <tt class=\"TYPE\">int8</tt> in v13, but in the meantime, allow these functions to work on the initial substring of a long <tt class=\"TYPE\">bytea</tt>.</p>"
  ],
  [
    "<p>Avoid possibly leaking an open-file descriptor for a directory in <code class=\"FUNCTION\">pg_ls_dir()</code>, <code class=\"FUNCTION\">pg_timezone_names()</code>, <code class=\"FUNCTION\">pg_tablespace_databases()</code>, and allied functions (Justin Pryzby)</p>"
  ],
  [
    "<p>Fix polymorphic-function type resolution to correctly infer the actual type of an <tt class=\"TYPE\">anyarray</tt> output when given only an <tt class=\"TYPE\">anyrange</tt> input (Tom Lane)</p>"
  ],
  [
    "<p>Avoid unlikely crash when <tt class=\"COMMAND\">REINDEX</tt> is terminated by a session-shutdown signal (Tom Lane)</p>"
  ],
  [
    "<p>Prevent printout of possibly-incorrect hash join table statistics in <tt class=\"COMMAND\">EXPLAIN</tt> (Konstantin Knizhnik, Tom Lane, Thomas Munro)</p>"
  ],
  [
    "<p>Fix reporting of elapsed time for heap truncation steps in <tt class=\"COMMAND\">VACUUM VERBOSE</tt> (Tatsuhito Kasahara)</p>"
  ],
  [
    "<p>Avoid possibly showing <span class=\"QUOTE\">\"waiting\"</span> twice in a process's PS status (Masahiko Sawada)</p>"
  ],
  [
    "<p>Avoid premature recycling of WAL segments during crash recovery (Jehan-Guillaume de Rorthais)</p>",
    "<p>WAL segments that become ready to be archived during crash recovery were potentially recycled without being archived.</p>"
  ],
  [
    "<p>Avoid scanning irrelevant timelines during archive recovery (Kyotaro Horiguchi)</p>",
    "<p>This can eliminate many attempts to fetch non-existent WAL files from archive storage, which is helpful if archive access is slow.</p>"
  ],
  [
    "<p>Remove bogus <span class=\"QUOTE\">\"subtransaction logged without previous top-level txn record\"</span> error check in logical decoding (Arseny Sher, Amit Kapila)</p>",
    "<p>This condition is legitimately reachable in various scenarios, so remove the check.</p>"
  ],
  [
    "<p>Ensure that a replication slot's <tt class=\"LITERAL\">io_in_progress_lock</tt> is released in failure code paths (Pavan Deolasee)</p>",
    "<p>This could result in a walsender later becoming stuck waiting for the lock.</p>"
  ],
  [
    "<p>Fix race conditions in synchronous standby management (Tom Lane)</p>",
    "<p>During a change in the <tt class=\"VARNAME\">synchronous_standby_names</tt> setting, there was a window in which wrong decisions could be made about whether it is OK to release transactions that are waiting for synchronous commit. Another hazard for similarly wrong decisions existed if a walsender process exited and was immediately replaced by another.</p>"
  ],
  [
    "<p>Ensure <tt class=\"VARNAME\">nextXid</tt> can't go backwards on a standby server (Eka Palamadai)</p>",
    "<p>This race condition could allow incorrect hot standby feedback messages to be sent back to the primary server, potentially allowing <tt class=\"COMMAND\">VACUUM</tt> to run too soon on the primary.</p>"
  ],
  [
    "<p>Add missing SQLSTATE values to a few error reports (Sawada Masahiko)</p>"
  ],
  [
    "<p>Fix PL/pgSQL to reliably refuse to execute an event trigger function as a plain function (Tom Lane)</p>"
  ],
  [
    "<p>Fix memory leak in <span class=\"APPLICATION\">libpq</span> when using <tt class=\"LITERAL\">sslmode=verify-full</tt> (Roman Peshkurov)</p>",
    "<p>Certificate verification during connection startup could leak some memory. This would become an issue if a client process opened many database connections during its lifetime.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">ecpg</span> to treat an argument of just <span class=\"QUOTE\">\"<tt class=\"LITERAL\">-</tt>\"</span> as meaning <span class=\"QUOTE\">\"read from stdin\"</span> on all platforms (Tom Lane)</p>"
  ],
  [
    "<p>Add <span class=\"APPLICATION\">pg_dump</span> support for <tt class=\"COMMAND\">ALTER ... DEPENDS ON EXTENSION</tt> (Álvaro Herrera)</p>",
    "<p><span class=\"APPLICATION\">pg_dump</span> previously ignored dependencies added this way, causing them to be forgotten during dump/restore or <span class=\"APPLICATION\">pg_upgrade</span>.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_dump</span> to dump comments on RLS policy objects (Tom Lane)</p>"
  ],
  [
    "<p>In <span class=\"APPLICATION\">pg_dump</span>, postpone restore of event triggers till the end (Fabrízio de Royes Mello, Hamid Akhtar, Tom Lane)</p>",
    "<p>This minimizes the risk that an event trigger could interfere with the restoration of other objects.</p>"
  ],
  [
    "<p>Fix quoting of <tt class=\"OPTION\">--encoding</tt>, <tt class=\"OPTION\">--lc-ctype</tt> and <tt class=\"OPTION\">--lc-collate</tt> values in <span class=\"APPLICATION\">createdb</span> utility (Michael Paquier)</p>"
  ],
  [
    "<p><tt class=\"FILENAME\">contrib/lo</tt>'s <code class=\"FUNCTION\">lo_manage()</code> function crashed if called directly rather than as a trigger (Tom Lane)</p>"
  ],
  [
    "<p>In <tt class=\"FILENAME\">contrib/ltree</tt>, protect against overflow of <tt class=\"TYPE\">ltree</tt> and <tt class=\"TYPE\">lquery</tt> length fields (Nikita Glukhov)</p>"
  ],
  [
    "<p>Fix cache reference leak in <tt class=\"FILENAME\">contrib/sepgsql</tt> (Michael Luo)</p>"
  ],
  [
    "<p>Avoid failures when dealing with Unix-style locale names on Windows (Juan José Santamaría Flecha)</p>"
  ],
  [
    "<p>In MSVC builds, cope with spaces in the path name for Python (Victor Wagner)</p>"
  ],
  [
    "<p>In MSVC builds, fix detection of Visual Studio version to work with more language settings (Andrew Dunstan)</p>"
  ],
  [
    "<p>In MSVC builds, use <tt class=\"LITERAL\">-Wno-deprecated</tt> with bison versions newer than 3.0, as non-Windows builds already do (Andrew Dunstan)</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"APPLICATION\">tzdata</span> release 2020a for DST law changes in Morocco and the Canadian Yukon, plus historical corrections for Shanghai.</p>",
    "<p>The America/Godthab zone has been renamed to America/Nuuk to reflect current English usage; however, the old name remains available as a compatibility link.</p>",
    "<p>Also, update <span class=\"APPLICATION\">initdb</span>'s list of known Windows time zone names to include recent additions, improving the odds that it will correctly translate the system time zone setting on that platform.</p>"
  ]
]