[
  [
    "<p>Fix failure to reset <span class=\"APPLICATION\">libpq</span>'s state fully between connection attempts (Tom Lane)</p>",
    "<p>An unprivileged user of <tt class=\"FILENAME\">dblink</tt> or <tt class=\"FILENAME\">postgres_fdw</tt> could bypass the checks intended to prevent use of server-side credentials, such as a <tt class=\"FILENAME\">~/.pgpass</tt> file owned by the operating-system user running the server. Servers allowing peer authentication on local connections are particularly vulnerable. Other attacks such as SQL injection into a <tt class=\"FILENAME\">postgres_fdw</tt> session are also possible. Attacking <tt class=\"FILENAME\">postgres_fdw</tt> in this way requires the ability to create a foreign server object with selected connection parameters, but any user with access to <tt class=\"FILENAME\">dblink</tt> could exploit the problem. In general, an attacker with the ability to select the connection parameters for a <span class=\"APPLICATION\">libpq</span>-using application could cause mischief, though other plausible attack scenarios are harder to think of. Our thanks to Andrew Krasichkov for reporting this issue. (CVE-2018-10915)</p>"
  ],
  [
    "<p>Fix <tt class=\"LITERAL\">INSERT ... ON CONFLICT UPDATE</tt> through a view that isn't just <tt class=\"LITERAL\">SELECT * FROM ...</tt> (Dean Rasheed, Amit Langote)</p>",
    "<p>Erroneous expansion of an updatable view could lead to crashes or <span class=\"QUOTE\">\"attribute ... has the wrong type\"</span> errors, if the view's <tt class=\"LITERAL\">SELECT</tt> list doesn't match one-to-one with the underlying table's columns. Furthermore, this bug could be leveraged to allow updates of columns that an attacking user lacks <tt class=\"LITERAL\">UPDATE</tt> privilege for, if that user has <tt class=\"LITERAL\">INSERT</tt> and <tt class=\"LITERAL\">UPDATE</tt> privileges for some other column(s) of the table. Any user could also use it for disclosure of server memory. (CVE-2018-10925)</p>"
  ],
  [
    "<p>Ensure that updates to the <tt class=\"STRUCTFIELD\">relfrozenxid</tt> and <tt class=\"STRUCTFIELD\">relminmxid</tt> values for <span class=\"QUOTE\">\"nailed\"</span> system catalogs are processed in a timely fashion (Andres Freund)</p>",
    "<p>Overoptimistic caching rules could prevent these updates from being seen by other sessions, leading to spurious errors and/or data corruption. The problem was significantly worse for shared catalogs, such as <tt class=\"STRUCTNAME\">pg_authid</tt>, because the stale cache data could persist into new sessions as well as existing ones.</p>"
  ],
  [
    "<p>Fix case where a freshly-promoted standby crashes before having completed its first post-recovery checkpoint (Michael Paquier, Kyotaro Horiguchi, Pavan Deolasee, Álvaro Herrera)</p>",
    "<p>This led to a situation where the server did not think it had reached a consistent database state during subsequent WAL replay, preventing restart.</p>"
  ],
  [
    "<p>Avoid emitting a bogus WAL record when recycling an all-zero btree page (Amit Kapila)</p>",
    "<p>This mistake has been seen to cause assertion failures, and potentially it could result in unnecessary query cancellations on hot standby servers.</p>"
  ],
  [
    "<p>During WAL replay, guard against corrupted record lengths exceeding 1GB (Michael Paquier)</p>",
    "<p>Treat such a case as corrupt data. Previously, the code would try to allocate space and get a hard error, making recovery impossible.</p>"
  ],
  [
    "<p>When ending recovery, delay writing the timeline history file as long as possible (Heikki Linnakangas)</p>",
    "<p>This avoids some situations where a failure during recovery cleanup (such as a problem with a two-phase state file) led to inconsistent timeline state on-disk.</p>"
  ],
  [
    "<p>Improve performance of WAL replay for transactions that drop many relations (Fujii Masao)</p>",
    "<p>This change reduces the number of times that shared buffers are scanned, so that it is of most benefit when that setting is large.</p>"
  ],
  [
    "<p>Improve performance of lock releasing in standby server WAL replay (Thomas Munro)</p>"
  ],
  [
    "<p>Make logical WAL senders report streaming state correctly (Simon Riggs, Sawada Masahiko)</p>",
    "<p>The code previously mis-detected whether or not it had caught up with the upstream server.</p>"
  ],
  [
    "<p>Fix bugs in snapshot handling during logical decoding, allowing wrong decoding results in rare cases (Arseny Sher, Álvaro Herrera)</p>"
  ],
  [
    "<p>Ensure a table's cached index list is correctly rebuilt after an index creation fails partway through (Peter Geoghegan)</p>",
    "<p>Previously, the failed index's OID could remain in the list, causing problems later in the same session.</p>"
  ],
  [
    "<p>Fix mishandling of empty uncompressed posting list pages in GIN indexes (Sivasubramanian Ramasubramanian, Alexander Korotkov)</p>",
    "<p>This could result in an assertion failure after pg_upgrade of a pre-9.4 GIN index (9.4 and later will not create such pages).</p>"
  ],
  [
    "<p>Ensure that <tt class=\"COMMAND\">VACUUM</tt> will respond to signals within btree page deletion loops (Andres Freund)</p>",
    "<p>Corrupted btree indexes could result in an infinite loop here, and that previously wasn't interruptible without forcing a crash.</p>"
  ],
  [
    "<p>Fix misoptimization of equivalence classes involving composite-type columns (Tom Lane)</p>",
    "<p>This resulted in failure to recognize that an index on a composite column could provide the sort order needed for a mergejoin on that column.</p>"
  ],
  [
    "<p>Fix SQL-standard <tt class=\"LITERAL\">FETCH FIRST</tt> syntax to allow parameters (<tt class=\"LITERAL\">$<tt class=\"REPLACEABLE c2\">n</tt></tt>), as the standard expects (Andrew Gierth)</p>"
  ],
  [
    "<p>Fix failure to schema-qualify some object names in <code class=\"FUNCTION\">getObjectDescription</code> output (Kyotaro Horiguchi, Tom Lane)</p>",
    "<p>Names of collations, conversions, and text search objects were not schema-qualified when they should be.</p>"
  ],
  [
    "<p>Widen <tt class=\"COMMAND\">COPY FROM</tt>'s current-line-number counter from 32 to 64 bits (David Rowley)</p>",
    "<p>This avoids two problems with input exceeding 4G lines: <tt class=\"LITERAL\">COPY FROM WITH HEADER</tt> would drop a line every 4G lines, not only the first line, and error reports could show a wrong line number.</p>"
  ],
  [
    "<p>Add a string freeing function to <span class=\"APPLICATION\">ecpg</span>'s <tt class=\"FILENAME\">pgtypes</tt> library, so that cross-module memory management problems can be avoided on Windows (Takayuki Tsunakawa)</p>",
    "<p>On Windows, crashes can ensue if the <code class=\"FUNCTION\">free</code> call for a given chunk of memory is not made from the same DLL that <code class=\"FUNCTION\">malloc</code>'ed the memory. The <tt class=\"FILENAME\">pgtypes</tt> library sometimes returns strings that it expects the caller to free, making it impossible to follow this rule. Add a <code class=\"FUNCTION\">PGTYPESchar_free()</code> function that just wraps <code class=\"FUNCTION\">free</code>, allowing applications to follow this rule.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">ecpg</span>'s support for <tt class=\"TYPE\">long long</tt> variables on Windows, as well as other platforms that declare <code class=\"FUNCTION\">strtoll</code>/<code class=\"FUNCTION\">strtoull</code> nonstandardly or not at all (Dang Minh Huong, Tom Lane)</p>"
  ],
  [
    "<p>Fix misidentification of SQL statement type in PL/pgSQL, when a rule change causes a change in the semantics of a statement intra-session (Tom Lane)</p>",
    "<p>This error led to assertion failures, or in rare cases, failure to enforce the <tt class=\"LITERAL\">INTO STRICT</tt> option as expected.</p>"
  ],
  [
    "<p>Fix password prompting in client programs so that echo is properly disabled on Windows when <tt class=\"LITERAL\">stdin</tt> is not the terminal (Matthew Stickney)</p>"
  ],
  [
    "<p>Further fix mis-quoting of values for list-valued GUC variables in dumps (Tom Lane)</p>",
    "<p>The previous fix for quoting of <tt class=\"VARNAME\">search_path</tt> and other list-valued variables in <span class=\"APPLICATION\">pg_dump</span> output turned out to misbehave for empty-string list elements, and it risked truncation of long file paths.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_dump</span>'s failure to dump <tt class=\"LITERAL\">REPLICA IDENTITY</tt> properties for constraint indexes (Tom Lane)</p>",
    "<p>Manually created unique indexes were properly marked, but not those created by declaring <tt class=\"LITERAL\">UNIQUE</tt> or <tt class=\"LITERAL\">PRIMARY KEY</tt> constraints.</p>"
  ],
  [
    "<p>Make <span class=\"APPLICATION\">pg_upgrade</span> check that the old server was shut down cleanly (Bruce Momjian)</p>",
    "<p>The previous check could be fooled by an immediate-mode shutdown.</p>"
  ],
  [
    "<p>Fix <tt class=\"FILENAME\">contrib/hstore_plperl</tt> to look through Perl scalar references, and to not crash if it doesn't find a hash reference where it expects one (Tom Lane)</p>"
  ],
  [
    "<p>Fix crash in <tt class=\"FILENAME\">contrib/ltree</tt>'s <code class=\"FUNCTION\">lca()</code> function when the input array is empty (Pierre Ducroquet)</p>"
  ],
  [
    "<p>Fix various error-handling code paths in which an incorrect error code might be reported (Michael Paquier, Tom Lane, Magnus Hagander)</p>"
  ],
  [
    "<p>Rearrange makefiles to ensure that programs link to freshly-built libraries (such as <tt class=\"FILENAME\">libpq.so</tt>) rather than ones that might exist in the system library directories (Tom Lane)</p>",
    "<p>This avoids problems when building on platforms that supply old copies of <span class=\"PRODUCTNAME\">PostgreSQL</span> libraries.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"APPLICATION\">tzdata</span> release 2018e for DST law changes in North Korea, plus historical corrections for Czechoslovakia.</p>",
    "<p>This update includes a redefinition of <span class=\"QUOTE\">\"daylight savings\"</span> in Ireland, as well as for some past years in Namibia and Czechoslovakia. In those jurisdictions, legally standard time is observed in summer, and daylight savings time in winter, so that the daylight savings offset is one hour behind standard time not one hour ahead. This does not affect either the actual UTC offset or the timezone abbreviations in use; the only known effect is that the <tt class=\"STRUCTFIELD\">is_dst</tt> column in the <tt class=\"STRUCTNAME\">pg_timezone_names</tt> view will now be true in winter and false in summer in these cases.</p>"
  ]
]