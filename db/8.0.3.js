[
  [
    "<p>Change encoding function signature to prevent\n          misuse</p>"
  ],
  [
    "<p>Change <code class=\"filename\">contrib/tsearch2</code>\n          to avoid unsafe use of <code class=\"type\">INTERNAL</code>\n          function results</p>"
  ],
  [
    "<p>Guard against incorrect second parameter to\n          <code class=\"function\">record_out</code></p>"
  ],
  [
    "<p>Repair ancient race condition that allowed a\n          transaction to be seen as committed for some purposes (eg\n          SELECT FOR UPDATE) slightly sooner than for other\n          purposes</p>",
    "<p>This is an extremely serious bug since it could lead\n          to apparent data inconsistencies being briefly visible to\n          applications.</p>"
  ],
  [
    "<p>Repair race condition between relation extension and\n          VACUUM</p>",
    "<p>This could theoretically have caused loss of a page's\n          worth of freshly-inserted data, although the scenario\n          seems of very low probability. There are no known cases\n          of it having caused more than an Assert failure.</p>"
  ],
  [
    "<p>Fix comparisons of <code class=\"type\">TIME WITH TIME\n          ZONE</code> values</p>",
    "<p>The comparison code was wrong in the case where the\n          <code class=\"literal\">--enable-integer-datetimes</code>\n          configuration switch had been used. NOTE: if you have an\n          index on a <code class=\"type\">TIME WITH TIME ZONE</code>\n          column, it will need to be <code class=\"command\">REINDEX</code>ed after installing this update,\n          because the fix corrects the sort order of column\n          values.</p>"
  ],
  [
    "<p>Fix <code class=\"function\">EXTRACT(EPOCH)</code> for\n          <code class=\"type\">TIME WITH TIME ZONE</code> values</p>"
  ],
  [
    "<p>Fix mis-display of negative fractional seconds in\n          <code class=\"type\">INTERVAL</code> values</p>",
    "<p>This error only occurred when the <code class=\"literal\">--enable-integer-datetimes</code> configuration\n          switch had been used.</p>"
  ],
  [
    "<p>Fix pg_dump to dump trigger names containing\n          <code class=\"literal\">%</code> correctly (Neil)</p>"
  ],
  [
    "<p>Still more 64-bit fixes for <code class=\"filename\">contrib/intagg</code></p>"
  ],
  [
    "<p>Prevent incorrect optimization of functions returning\n          <code class=\"type\">RECORD</code></p>"
  ],
  [
    "<p>Prevent crash on <code class=\"literal\">COALESCE(NULL,NULL)</code></p>"
  ],
  [
    "<p>Fix Borland makefile for libpq</p>"
  ],
  [
    "<p>Fix <code class=\"filename\">contrib/btree_gist</code>\n          for <code class=\"type\">timetz</code> type (Teodor)</p>"
  ],
  [
    "<p>Make <code class=\"command\">pg_ctl</code> check the PID\n          found in <code class=\"filename\">postmaster.pid</code> to\n          see if it is still a live process</p>"
  ],
  [
    "<p>Fix <code class=\"command\">pg_dump</code>/<code class=\"command\">pg_restore</code> problems caused by addition\n          of dump timestamps</p>"
  ],
  [
    "<p>Fix interaction between materializing holdable cursors\n          and firing deferred triggers during transaction\n          commit</p>"
  ],
  [
    "<p>Fix memory leak in SQL functions returning\n          pass-by-reference data types</p>"
  ]
]