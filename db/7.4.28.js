[
  [
    "<p>Add new configuration parameter <code class=\"varname\">ssl_renegotiation_limit</code> to control how\n          often we do session key renegotiation for an SSL\n          connection (Magnus)</p>",
    "<p>This can be set to zero to disable renegotiation\n          completely, which may be required if a broken SSL library\n          is used. In particular, some vendors are shipping stopgap\n          patches for CVE-2009-3555 that cause renegotiation\n          attempts to fail.</p>"
  ],
  [
    "<p>Make <code class=\"function\">substring()</code> for\n          <code class=\"type\">bit</code> types treat any negative\n          length as meaning <span class=\"quote\">&#x201C;<span class=\"quote\">all the rest of the string</span>&#x201D;</span>\n          (Tom)</p>",
    "<p>The previous coding treated only -1 that way, and\n          would produce an invalid result value for other negative\n          values, possibly leading to a crash (CVE-2010-0442).</p>"
  ],
  [
    "<p>Fix some cases of pathologically slow regular\n          expression matching (Tom)</p>"
  ],
  [
    "<p>When reading <code class=\"filename\">pg_hba.conf</code>\n          and related files, do not treat <code class=\"literal\">@something</code> as a file inclusion request\n          if the <code class=\"literal\">@</code> appears inside\n          quote marks; also, never treat <code class=\"literal\">@</code> by itself as a file inclusion request\n          (Tom)</p>",
    "<p>This prevents erratic behavior if a role or database\n          name starts with <code class=\"literal\">@</code>. If you\n          need to include a file whose path name contains spaces,\n          you can still do so, but you must write <code class=\"literal\">@\"/path to/file\"</code> rather than putting the\n          quotes around the whole construct.</p>"
  ],
  [
    "<p>Prevent infinite loop on some platforms if a directory\n          is named as an inclusion target in <code class=\"filename\">pg_hba.conf</code> and related files (Tom)</p>"
  ],
  [
    "<p>Ensure PL/Tcl initializes the Tcl interpreter fully\n          (Tom)</p>",
    "<p>The only known symptom of this oversight is that the\n          Tcl <code class=\"literal\">clock</code> command misbehaves\n          if using Tcl 8.5 or later.</p>"
  ],
  [
    "<p>Prevent crash in <code class=\"filename\">contrib/dblink</code> when too many key\n          columns are specified to a <code class=\"function\">dblink_build_sql_*</code> function (Rushabh\n          Lathia, Joe Conway)</p>"
  ]
]