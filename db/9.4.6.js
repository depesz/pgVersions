[
  [
    "<p>Fix inconsistent hash calculations in <tt class=\"LITERAL\">jsonb_path_ops</tt> GIN indexes (Tom Lane)</p>",
    "<p>When processing <tt class=\"TYPE\">jsonb</tt> values that contain both scalars and sub-objects at the same nesting level, for example an array containing both scalars and sub-arrays, key hash values could be calculated differently than they would be for the same key in a different context. This could result in queries not finding entries that they should find. Fixing this means that existing indexes may now be inconsistent with the new hash calculation code. Users should <tt class=\"COMMAND\">REINDEX</tt> <tt class=\"LITERAL\">jsonb_path_ops</tt> GIN indexes after installing this update to make sure that all searches work as expected.</p>"
  ],
  [
    "<p>Fix infinite loops and buffer-overrun problems in regular expressions (Tom Lane)</p>",
    "<p>Very large character ranges in bracket expressions could cause infinite loops in some cases, and memory overwrites in other cases. (CVE-2016-0773)</p>"
  ],
  [
    "<p>Perform an immediate shutdown if the <tt class=\"FILENAME\">postmaster.pid</tt> file is removed (Tom Lane)</p>",
    "<p>The postmaster now checks every minute or so that <tt class=\"FILENAME\">postmaster.pid</tt> is still there and still contains its own PID. If not, it performs an immediate shutdown, as though it had received <span class=\"SYSTEMITEM\">SIGQUIT</span>. The main motivation for this change is to ensure that failed buildfarm runs will get cleaned up without manual intervention; but it also serves to limit the bad effects if a DBA forcibly removes <tt class=\"FILENAME\">postmaster.pid</tt> and then starts a new postmaster.</p>"
  ],
  [
    "<p>In <tt class=\"LITERAL\">SERIALIZABLE</tt> transaction isolation mode, serialization anomalies could be missed due to race conditions during insertions (Kevin Grittner, Thomas Munro)</p>"
  ],
  [
    "<p>Fix failure to emit appropriate WAL records when doing <tt class=\"LITERAL\">ALTER TABLE ... SET TABLESPACE</tt> for unlogged relations (Michael Paquier, Andres Freund)</p>",
    "<p>Even though the relation's data is unlogged, the move must be logged or the relation will be inaccessible after a standby is promoted to master.</p>"
  ],
  [
    "<p>Fix possible misinitialization of unlogged relations at the end of crash recovery (Andres Freund, Michael Paquier)</p>"
  ],
  [
    "<p>Ensure walsender slots are fully re-initialized when being re-used (Magnus Hagander)</p>"
  ],
  [
    "<p>Fix <tt class=\"COMMAND\">ALTER COLUMN TYPE</tt> to reconstruct inherited check constraints properly (Tom Lane)</p>"
  ],
  [
    "<p>Fix <tt class=\"COMMAND\">REASSIGN OWNED</tt> to change ownership of composite types properly (Álvaro Herrera)</p>"
  ],
  [
    "<p>Fix <tt class=\"COMMAND\">REASSIGN OWNED</tt> and <tt class=\"COMMAND\">ALTER OWNER</tt> to correctly update granted-permissions lists when changing owners of data types, foreign data wrappers, or foreign servers (Bruce Momjian, Álvaro Herrera)</p>"
  ],
  [
    "<p>Fix <tt class=\"COMMAND\">REASSIGN OWNED</tt> to ignore foreign user mappings, rather than fail (Álvaro Herrera)</p>"
  ],
  [
    "<p>Fix possible crash after doing query rewrite for an updatable view (Stephen Frost)</p>"
  ],
  [
    "<p>Fix planner's handling of <tt class=\"LITERAL\">LATERAL</tt> references (Tom Lane)</p>",
    "<p>This fixes some corner cases that led to <span class=\"QUOTE\">\"failed to build any N-way joins\"</span> or <span class=\"QUOTE\">\"could not devise a query plan\"</span> planner failures.</p>"
  ],
  [
    "<p>Add more defenses against bad planner cost estimates for GIN index scans when the index's internal statistics are very out-of-date (Tom Lane)</p>"
  ],
  [
    "<p>Make planner cope with hypothetical GIN indexes suggested by an index advisor plug-in (Julien Rouhaud)</p>"
  ],
  [
    "<p>Speed up generation of unique table aliases in <tt class=\"COMMAND\">EXPLAIN</tt> and rule dumping, and ensure that generated aliases do not exceed <tt class=\"LITERAL\">NAMEDATALEN</tt> (Tom Lane)</p>"
  ],
  [
    "<p>Fix dumping of whole-row Vars in <tt class=\"LITERAL\">ROW()</tt> and <tt class=\"LITERAL\">VALUES()</tt> lists (Tom Lane)</p>"
  ],
  [
    "<p>Translation of minus-infinity dates and timestamps to <tt class=\"TYPE\">json</tt> or <tt class=\"TYPE\">jsonb</tt> incorrectly rendered them as plus-infinity (Tom Lane)</p>"
  ],
  [
    "<p>Fix possible internal overflow in <tt class=\"TYPE\">numeric</tt> division (Dean Rasheed)</p>"
  ],
  [
    "<p>Fix enforcement of restrictions inside parentheses within regular expression lookahead constraints (Tom Lane)</p>",
    "<p>Lookahead constraints aren't allowed to contain backrefs, and parentheses within them are always considered non-capturing, according to the manual. However, the code failed to handle these cases properly inside a parenthesized subexpression, and would give unexpected results.</p>"
  ],
  [
    "<p>Conversion of regular expressions to indexscan bounds could produce incorrect bounds from regexps containing lookahead constraints (Tom Lane)</p>"
  ],
  [
    "<p>Fix regular-expression compiler to handle loops of constraint arcs (Tom Lane)</p>",
    "<p>The code added for CVE-2007-4772 was both incomplete, in that it didn't handle loops involving more than one state, and incorrect, in that it could cause assertion failures (though there seem to be no bad consequences of that in a non-assert build). Multi-state loops would cause the compiler to run until the query was canceled or it reached the too-many-states error condition.</p>"
  ],
  [
    "<p>Improve memory-usage accounting in regular-expression compiler (Tom Lane)</p>",
    "<p>This causes the code to emit <span class=\"QUOTE\">\"regular expression is too complex\"</span> errors in some cases that previously used unreasonable amounts of time and memory.</p>"
  ],
  [
    "<p>Improve performance of regular-expression compiler (Tom Lane)</p>"
  ],
  [
    "<p>Make <tt class=\"LITERAL\">%h</tt> and <tt class=\"LITERAL\">%r</tt> escapes in <tt class=\"VARNAME\">log_line_prefix</tt> work for messages emitted due to <tt class=\"VARNAME\">log_connections</tt> (Tom Lane)</p>",
    "<p>Previously, <tt class=\"LITERAL\">%h</tt>/<tt class=\"LITERAL\">%r</tt> started to work just after a new session had emitted the <span class=\"QUOTE\">\"connection received\"</span> log message; now they work for that message too.</p>"
  ],
  [
    "<p>On Windows, ensure the shared-memory mapping handle gets closed in child processes that don't need it (Tom Lane, Amit Kapila)</p>",
    "<p>This oversight resulted in failure to recover from crashes whenever <tt class=\"VARNAME\">logging_collector</tt> is turned on.</p>"
  ],
  [
    "<p>Fix possible failure to detect socket EOF in non-blocking mode on Windows (Tom Lane)</p>",
    "<p>It's not entirely clear whether this problem can happen in pre-9.5 branches, but if it did, the symptom would be that a walsender process would wait indefinitely rather than noticing a loss of connection.</p>"
  ],
  [
    "<p>Avoid leaking a token handle during SSPI authentication (Christian Ullrich)</p>"
  ],
  [
    "<p>In <span class=\"APPLICATION\">psql</span>, ensure that <span class=\"APPLICATION\">libreadline</span>'s idea of the screen size is updated when the terminal window size changes (Merlin Moncure)</p>",
    "<p>Previously, <span class=\"APPLICATION\">libreadline</span> did not notice if the window was resized during query output, leading to strange behavior during later input of multiline queries.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">psql</span>'s <tt class=\"LITERAL\">\\det</tt> command to interpret its pattern argument the same way as other <tt class=\"LITERAL\">\\d</tt> commands with potentially schema-qualified patterns do (Reece Hart)</p>"
  ],
  [
    "<p>Avoid possible crash in <span class=\"APPLICATION\">psql</span>'s <tt class=\"LITERAL\">\\c</tt> command when previous connection was via Unix socket and command specifies a new hostname and same username (Tom Lane)</p>"
  ],
  [
    "<p>In <tt class=\"LITERAL\">pg_ctl start -w</tt>, test child process status directly rather than relying on heuristics (Tom Lane, Michael Paquier)</p>",
    "<p>Previously, <span class=\"APPLICATION\">pg_ctl</span> relied on an assumption that the new postmaster would always create <tt class=\"FILENAME\">postmaster.pid</tt> within five seconds. But that can fail on heavily-loaded systems, causing <span class=\"APPLICATION\">pg_ctl</span> to report incorrectly that the postmaster failed to start.</p>",
    "<p>Except on Windows, this change also means that a <tt class=\"LITERAL\">pg_ctl start -w</tt> done immediately after another such command will now reliably fail, whereas previously it would report success if done within two seconds of the first command.</p>"
  ],
  [
    "<p>In <tt class=\"LITERAL\">pg_ctl start -w</tt>, don't attempt to use a wildcard listen address to connect to the postmaster (Kondo Yuta)</p>",
    "<p>On Windows, <span class=\"APPLICATION\">pg_ctl</span> would fail to detect postmaster startup if <tt class=\"VARNAME\">listen_addresses</tt> is set to <tt class=\"LITERAL\">0.0.0.0</tt> or <tt class=\"LITERAL\">::</tt>, because it would try to use that value verbatim as the address to connect to, which doesn't work. Instead assume that <tt class=\"LITERAL\">127.0.0.1</tt> or <tt class=\"LITERAL\">::1</tt>, respectively, is the right thing to use.</p>"
  ],
  [
    "<p>In <span class=\"APPLICATION\">pg_ctl</span> on Windows, check service status to decide where to send output, rather than checking if standard output is a terminal (Michael Paquier)</p>"
  ],
  [
    "<p>In <span class=\"APPLICATION\">pg_dump</span> and <span class=\"APPLICATION\">pg_basebackup</span>, adopt the GNU convention for handling tar-archive members exceeding 8GB (Tom Lane)</p>",
    "<p>The POSIX standard for <tt class=\"LITERAL\">tar</tt> file format does not allow archive member files to exceed 8GB, but most modern implementations of <span class=\"APPLICATION\">tar</span> support an extension that fixes that. Adopt this extension so that <span class=\"APPLICATION\">pg_dump</span> with <tt class=\"OPTION\">-Ft</tt> no longer fails on tables with more than 8GB of data, and so that <span class=\"APPLICATION\">pg_basebackup</span> can handle files larger than 8GB. In addition, fix some portability issues that could cause failures for members between 4GB and 8GB on some platforms. Potentially these problems could cause unrecoverable data loss due to unreadable backup files.</p>"
  ],
  [
    "<p>Fix assorted corner-case bugs in <span class=\"APPLICATION\">pg_dump</span>'s processing of extension member objects (Tom Lane)</p>"
  ],
  [
    "<p>Make <span class=\"APPLICATION\">pg_dump</span> mark a view's triggers as needing to be processed after its rule, to prevent possible failure during parallel <span class=\"APPLICATION\">pg_restore</span> (Tom Lane)</p>"
  ],
  [
    "<p>Ensure that relation option values are properly quoted in <span class=\"APPLICATION\">pg_dump</span> (Kouhei Sutou, Tom Lane)</p>",
    "<p>A reloption value that isn't a simple identifier or number could lead to dump/reload failures due to syntax errors in CREATE statements issued by <span class=\"APPLICATION\">pg_dump</span>. This is not an issue with any reloption currently supported by core <span class=\"PRODUCTNAME\">PostgreSQL</span>, but extensions could allow reloptions that cause the problem.</p>"
  ],
  [
    "<p>Avoid repeated password prompts during parallel <span class=\"APPLICATION\">pg_dump</span> (Zeus Kronion)</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_upgrade</span>'s file-copying code to handle errors properly on Windows (Bruce Momjian)</p>"
  ],
  [
    "<p>Install guards in <span class=\"APPLICATION\">pgbench</span> against corner-case overflow conditions during evaluation of script-specified division or modulo operators (Fabien Coelho, Michael Paquier)</p>"
  ],
  [
    "<p>Fix failure to localize messages emitted by <span class=\"APPLICATION\">pg_receivexlog</span> and <span class=\"APPLICATION\">pg_recvlogical</span> (Ioseph Kim)</p>"
  ],
  [
    "<p>Avoid dump/reload problems when using both <span class=\"APPLICATION\">plpython2</span> and <span class=\"APPLICATION\">plpython3</span> (Tom Lane)</p>",
    "<p>In principle, both versions of <span class=\"APPLICATION\">PL/Python</span> can be used in the same database, though not in the same session (because the two versions of <span class=\"APPLICATION\">libpython</span> cannot safely be used concurrently). However, <span class=\"APPLICATION\">pg_restore</span> and <span class=\"APPLICATION\">pg_upgrade</span> both do things that can fall foul of the same-session restriction. Work around that by changing the timing of the check.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">PL/Python</span> regression tests to pass with Python 3.5 (Peter Eisentraut)</p>"
  ],
  [
    "<p>Fix premature clearing of <span class=\"APPLICATION\">libpq</span>'s input buffer when socket EOF is seen (Tom Lane)</p>",
    "<p>This mistake caused <span class=\"APPLICATION\">libpq</span> to sometimes not report the backend's final error message before reporting <span class=\"QUOTE\">\"server closed the connection unexpectedly\"</span>.</p>"
  ],
  [
    "<p>Prevent certain <span class=\"APPLICATION\">PL/Java</span> parameters from being set by non-superusers (Noah Misch)</p>",
    "<p>This change mitigates a <span class=\"APPLICATION\">PL/Java</span> security bug (CVE-2016-0766), which was fixed in <span class=\"APPLICATION\">PL/Java</span> by marking these parameters as superuser-only. To fix the security hazard for sites that update <span class=\"PRODUCTNAME\">PostgreSQL</span> more frequently than <span class=\"APPLICATION\">PL/Java</span>, make the core code aware of them also.</p>"
  ],
  [
    "<p>Improve <span class=\"APPLICATION\">libpq</span>'s handling of out-of-memory situations (Michael Paquier, Amit Kapila, Heikki Linnakangas)</p>"
  ],
  [
    "<p>Fix order of arguments in <span class=\"APPLICATION\">ecpg</span>-generated <tt class=\"LITERAL\">typedef</tt> statements (Michael Meskes)</p>"
  ],
  [
    "<p>Use <tt class=\"LITERAL\">%g</tt> not <tt class=\"LITERAL\">%f</tt> format in <span class=\"APPLICATION\">ecpg</span>'s <code class=\"FUNCTION\">PGTYPESnumeric_from_double()</code> (Tom Lane)</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">ecpg</span>-supplied header files to not contain comments continued from a preprocessor directive line onto the next line (Michael Meskes)</p>",
    "<p>Such a comment is rejected by <span class=\"APPLICATION\">ecpg</span>. It's not yet clear whether <span class=\"APPLICATION\">ecpg</span> itself should be changed.</p>"
  ],
  [
    "<p>Fix <code class=\"FUNCTION\">hstore_to_json_loose()</code>'s test for whether an <tt class=\"TYPE\">hstore</tt> value can be converted to a JSON number (Tom Lane)</p>",
    "<p>Previously this function could be fooled by non-alphanumeric trailing characters, leading to emitting syntactically-invalid JSON.</p>"
  ],
  [
    "<p>Ensure that <tt class=\"FILENAME\">contrib/pgcrypto</tt>'s <code class=\"FUNCTION\">crypt()</code> function can be interrupted by query cancel (Andreas Karlsson)</p>"
  ],
  [
    "<p>In <tt class=\"FILENAME\">contrib/postgres_fdw</tt>, fix bugs triggered by use of <tt class=\"LITERAL\">tableoid</tt> in data-modifying commands (Etsuro Fujita, Robert Haas)</p>"
  ],
  [
    "<p>Accept <span class=\"APPLICATION\">flex</span> versions later than 2.5.x (Tom Lane, Michael Paquier)</p>",
    "<p>Now that flex 2.6.0 has been released, the version checks in our build scripts needed to be adjusted.</p>"
  ],
  [
    "<p>Improve reproducibility of build output by ensuring filenames are given to the linker in a fixed order (Christoph Berg)</p>",
    "<p>This avoids possible bitwise differences in the produced executable files from one build to the next.</p>"
  ],
  [
    "<p>Install our <tt class=\"FILENAME\">missing</tt> script where PGXS builds can find it (Jim Nasby)</p>",
    "<p>This allows sane behavior in a PGXS build done on a machine where build tools such as <span class=\"APPLICATION\">bison</span> are missing.</p>"
  ],
  [
    "<p>Ensure that <tt class=\"FILENAME\">dynloader.h</tt> is included in the installed header files in MSVC builds (Bruce Momjian, Michael Paquier)</p>"
  ],
  [
    "<p>Add variant regression test expected-output file to match behavior of current <span class=\"APPLICATION\">libxml2</span> (Tom Lane)</p>",
    "<p>The fix for <span class=\"APPLICATION\">libxml2</span>'s CVE-2015-7499 causes it not to output error context reports in some cases where it used to do so. This seems to be a bug, but we'll probably have to live with it for some time, so work around it.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"APPLICATION\">tzdata</span> release 2016a for DST law changes in Cayman Islands, Metlakatla, and Trans-Baikal Territory (Zabaykalsky Krai), plus historical corrections for Pakistan.</p>"
  ]
]