[
  [
    "<p>Before exiting <span class=\"application\">walreceiver</span>, ensure all the received\n          WAL is fsync'd to disk (Heikki Linnakangas)</p>",
    "<p>Otherwise the standby server could replay some\n          un-synced WAL, conceivably leading to data corruption if\n          the system crashes just at that point.</p>"
  ],
  [
    "<p>Avoid excess fsync activity in <span class=\"application\">walreceiver</span> (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Make <code class=\"command\">ALTER TABLE</code>\n          revalidate uniqueness and exclusion constraints when\n          needed (Noah Misch)</p>",
    "<p>This was broken in 9.0 by a change that was intended\n          to suppress revalidation during <code class=\"command\">VACUUM FULL</code> and <code class=\"command\">CLUSTER</code>, but unintentionally affected\n          <code class=\"command\">ALTER TABLE</code> as well.</p>"
  ],
  [
    "<p>Fix EvalPlanQual for <code class=\"command\">UPDATE</code> of an inheritance tree in which\n          the tables are not all alike (Tom Lane)</p>",
    "<p>Any variation in the table row types (including\n          dropped columns present in only some child tables) would\n          confuse the EvalPlanQual code, leading to misbehavior or\n          even crashes. Since EvalPlanQual is only executed during\n          concurrent updates to the same row, the problem was only\n          seen intermittently.</p>"
  ],
  [
    "<p>Avoid failures when <code class=\"command\">EXPLAIN</code> tries to display a simple-form\n          <code class=\"literal\">CASE</code> expression (Tom\n          Lane)</p>",
    "<p>If the <code class=\"literal\">CASE</code>'s test\n          expression was a constant, the planner could simplify the\n          <code class=\"literal\">CASE</code> into a form that\n          confused the expression-display code, resulting in\n          <span class=\"quote\">&#x201C;<span class=\"quote\">unexpected CASE\n          WHEN clause</span>&#x201D;</span> errors.</p>"
  ],
  [
    "<p>Fix assignment to an array slice that is before the\n          existing range of subscripts (Tom Lane)</p>",
    "<p>If there was a gap between the newly added subscripts\n          and the first pre-existing subscript, the code\n          miscalculated how many entries needed to be copied from\n          the old array's null bitmap, potentially leading to data\n          corruption or crash.</p>"
  ],
  [
    "<p>Avoid unexpected conversion overflow in planner for\n          very distant date values (Tom Lane)</p>",
    "<p>The <code class=\"type\">date</code> type supports a\n          wider range of dates than can be represented by the\n          <code class=\"type\">timestamp</code> types, but the\n          planner assumed it could always convert a date to\n          timestamp with impunity.</p>"
  ],
  [
    "<p>Fix PL/Python crash when an array contains null\n          entries (Alex Hunsaker)</p>"
  ],
  [
    "<p>Remove <span class=\"application\">ecpg</span>'s fixed\n          length limit for constants defining an array dimension\n          (Michael Meskes)</p>"
  ],
  [
    "<p>Fix erroneous parsing of <code class=\"type\">tsquery</code> values containing <code class=\"literal\">... &amp; !(subexpression) | ...</code> (Tom\n          Lane)</p>",
    "<p>Queries containing this combination of operators were\n          not executed correctly. The same error existed in\n          <code class=\"filename\">contrib/intarray</code>'s\n          <code class=\"type\">query_int</code> type and <code class=\"filename\">contrib/ltree</code>'s <code class=\"type\">ltxtquery</code> type.</p>"
  ],
  [
    "<p>Fix buffer overrun in <code class=\"filename\">contrib/intarray</code>'s input function for\n          the <code class=\"type\">query_int</code> type (Apple)</p>",
    "<p>This bug is a security risk since the function's\n          return address could be overwritten. Thanks to Apple\n          Inc's security team for reporting this issue and\n          supplying the fix. (CVE-2010-4015)</p>"
  ],
  [
    "<p>Fix bug in <code class=\"filename\">contrib/seg</code>'s\n          GiST picksplit algorithm (Alexander Korotkov)</p>",
    "<p>This could result in considerable inefficiency, though\n          not actually incorrect answers, in a GiST index on a\n          <code class=\"type\">seg</code> column. If you have such an\n          index, consider <code class=\"command\">REINDEX</code>ing\n          it after installing this update. (This is identical to\n          the bug that was fixed in <code class=\"filename\">contrib/cube</code> in the previous\n          update.)</p>"
  ]
]