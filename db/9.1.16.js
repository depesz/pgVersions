[
  [
    "<p>Avoid possible crash when client disconnects just\n          before the authentication timeout expires (Benkocs\n          Norbert Attila)</p>",
    "<p>If the timeout interrupt fired partway through the\n          session shutdown sequence, SSL-related state would be\n          freed twice, typically causing a crash and hence denial\n          of service to other sessions. Experimentation shows that\n          an unauthenticated remote attacker could trigger the bug\n          somewhat consistently, hence treat as security issue.\n          (CVE-2015-3165)</p>"
  ],
  [
    "<p>Improve detection of system-call failures (Noah\n          Misch)</p>",
    "<p>Our replacement implementation of <code class=\"function\">snprintf()</code> failed to check for errors\n          reported by the underlying system library calls; the main\n          case that might be missed is out-of-memory situations. In\n          the worst case this might lead to information exposure,\n          due to our code assuming that a buffer had been\n          overwritten when it hadn't been. Also, there were a few\n          places in which security-relevant calls of other system\n          library functions did not check for failure.</p>",
    "<p>It remains possible that some calls of the\n          <code class=\"function\">*printf()</code> family of\n          functions are vulnerable to information disclosure if an\n          out-of-memory error occurs at just the wrong time. We\n          judge the risk to not be large, but will continue\n          analysis in this area. (CVE-2015-3166)</p>"
  ],
  [
    "<p>In <code class=\"filename\">contrib/pgcrypto</code>,\n          uniformly report decryption failures as <span class=\"quote\">&#x201C;<span class=\"quote\">Wrong key or corrupt\n          data</span>&#x201D;</span> (Noah Misch)</p>",
    "<p>Previously, some cases of decryption with an incorrect\n          key could report other error message texts. It has been\n          shown that such variance in error reports can aid\n          attackers in recovering keys from other systems. While\n          it's unknown whether <code class=\"filename\">pgcrypto</code>'s specific behaviors are\n          likewise exploitable, it seems better to avoid the risk\n          by using a one-size-fits-all message. (CVE-2015-3167)</p>"
  ],
  [
    "<p>Fix incorrect declaration of <code class=\"filename\">contrib/citext</code>'s <code class=\"function\">regexp_matches()</code> functions (Tom\n          Lane)</p>",
    "<p>These functions should return <code class=\"type\">setof\n          text[]</code>, like the core functions they are wrappers\n          for; but they were incorrectly declared as returning just\n          <code class=\"type\">text[]</code>. This mistake had two\n          results: first, if there was no match you got a scalar\n          null result, whereas what you should get is an empty set\n          (zero rows). Second, the <code class=\"literal\">g</code>\n          flag was effectively ignored, since you would get only\n          one result array even if there were multiple matches.</p>",
    "<p>While the latter behavior is clearly a bug, there\n          might be applications depending on the former behavior;\n          therefore the function declarations will not be changed\n          by default until <span class=\"productname\">PostgreSQL</span> 9.5. In pre-9.5 branches,\n          the old behavior exists in version 1.0 of the\n          <code class=\"literal\">citext</code> extension, while we\n          have provided corrected declarations in version 1.1\n          (which is <span class=\"emphasis\"><em>not</em></span>\n          installed by default). To adopt the fix in pre-9.5\n          branches, execute <code class=\"literal\">ALTER EXTENSION\n          citext UPDATE TO '1.1'</code> in each database in which\n          <code class=\"literal\">citext</code> is installed. (You\n          can also <span class=\"quote\">&#x201C;<span class=\"quote\">update</span>&#x201D;</span> back to 1.0 if you need to\n          undo that.) Be aware that either update direction will\n          require dropping and recreating any views or rules that\n          use <code class=\"filename\">citext</code>'s <code class=\"function\">regexp_matches()</code> functions.</p>"
  ],
  [
    "<p>Fix incorrect checking of deferred exclusion\n          constraints after a HOT update (Tom Lane)</p>",
    "<p>If a new row that potentially violates a deferred\n          exclusion constraint is HOT-updated (that is, no indexed\n          columns change and the row can be stored back onto the\n          same table page) later in the same transaction, the\n          exclusion constraint would be reported as violated when\n          the check finally occurred, even if the row(s) the new\n          row originally conflicted with had been deleted.</p>"
  ],
  [
    "<p>Prevent improper reordering of antijoins (NOT EXISTS\n          joins) versus other outer joins (Tom Lane)</p>",
    "<p>This oversight in the planner has been observed to\n          cause <span class=\"quote\">&#x201C;<span class=\"quote\">could not\n          find RelOptInfo for given relids</span>&#x201D;</span> errors,\n          but it seems possible that sometimes an incorrect query\n          plan might get past that consistency check and result in\n          silently-wrong query output.</p>"
  ],
  [
    "<p>Fix incorrect matching of subexpressions in outer-join\n          plan nodes (Tom Lane)</p>",
    "<p>Previously, if textually identical non-strict\n          subexpressions were used both above and below an outer\n          join, the planner might try to re-use the value computed\n          below the join, which would be incorrect because the\n          executor would force the value to NULL in case of an\n          unmatched outer row.</p>"
  ],
  [
    "<p>Fix GEQO planner to cope with failure of its join\n          order heuristic (Tom Lane)</p>",
    "<p>This oversight has been seen to lead to <span class=\"quote\">&#x201C;<span class=\"quote\">failed to join all relations\n          together</span>&#x201D;</span> errors in queries involving\n          <code class=\"literal\">LATERAL</code>, and that might\n          happen in other cases as well.</p>"
  ],
  [
    "<p>Fix possible deadlock at startup when <code class=\"literal\">max_prepared_transactions</code> is too small\n          (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Don't archive useless preallocated WAL files after a\n          timeline switch (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Avoid <span class=\"quote\">&#x201C;<span class=\"quote\">cannot\n          GetMultiXactIdMembers() during recovery</span>&#x201D;</span>\n          error (&#xC1;lvaro Herrera)</p>"
  ],
  [
    "<p>Recursively <code class=\"function\">fsync()</code> the\n          data directory after a crash (Abhijit Menon-Sen, Robert\n          Haas)</p>",
    "<p>This ensures consistency if another crash occurs\n          shortly later. (The second crash would have to be a\n          system-level crash, not just a database crash, for there\n          to be a problem.)</p>"
  ],
  [
    "<p>Fix autovacuum launcher's possible failure to shut\n          down, if an error occurs after it receives SIGTERM\n          (&#xC1;lvaro Herrera)</p>"
  ],
  [
    "<p>Cope with unexpected signals in <code class=\"function\">LockBufferForCleanup()</code> (Andres\n          Freund)</p>",
    "<p>This oversight could result in spurious errors about\n          <span class=\"quote\">&#x201C;<span class=\"quote\">multiple\n          backends attempting to wait for pincount\n          1</span>&#x201D;</span>.</p>"
  ],
  [
    "<p>Avoid waiting for WAL flush or synchronous replication\n          during commit of a transaction that was read-only so far\n          as the user is concerned (Andres Freund)</p>",
    "<p>Previously, a delay could occur at commit in\n          transactions that had written WAL due to HOT page\n          pruning, leading to undesirable effects such as sessions\n          getting stuck at startup if all synchronous replicas are\n          down. Sessions have also been observed to get stuck in\n          catchup interrupt processing when using synchronous\n          replication; this will fix that problem as well.</p>"
  ],
  [
    "<p>Fix crash when manipulating hash indexes on temporary\n          tables (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Fix possible failure during hash index bucket split,\n          if other processes are modifying the index concurrently\n          (Tom Lane)</p>"
  ],
  [
    "<p>Check for interrupts while analyzing index expressions\n          (Jeff Janes)</p>",
    "<p><code class=\"command\">ANALYZE</code> executes index\n          expressions many times; if there are slow functions in\n          such an expression, it's desirable to be able to cancel\n          the <code class=\"command\">ANALYZE</code> before that loop\n          finishes.</p>"
  ],
  [
    "<p>Ensure <code class=\"structfield\">tableoid</code> of a\n          foreign table is reported correctly when a <code class=\"literal\">READ COMMITTED</code> recheck occurs after\n          locking rows in <code class=\"command\">SELECT FOR\n          UPDATE</code>, <code class=\"command\">UPDATE</code>, or\n          <code class=\"command\">DELETE</code> (Etsuro Fujita)</p>"
  ],
  [
    "<p>Add the name of the target server to object\n          description strings for foreign-server user mappings\n          (&#xC1;lvaro Herrera)</p>"
  ],
  [
    "<p>Recommend setting <code class=\"literal\">include_realm</code> to 1 when using\n          Kerberos/GSSAPI/SSPI authentication (Stephen Frost)</p>",
    "<p>Without this, identically-named users from different\n          realms cannot be distinguished. For the moment this is\n          only a documentation change, but it will become the\n          default setting in <span class=\"productname\">PostgreSQL</span> 9.5.</p>"
  ],
  [
    "<p>Remove code for matching IPv4 <code class=\"filename\">pg_hba.conf</code> entries to IPv4-in-IPv6\n          addresses (Tom Lane)</p>",
    "<p>This hack was added in 2003 in response to a report\n          that some Linux kernels of the time would report IPv4\n          connections as having IPv4-in-IPv6 addresses. However,\n          the logic was accidentally broken in 9.0. The lack of any\n          field complaints since then shows that it's not needed\n          anymore. Now we have reports that the broken code causes\n          crashes on some systems, so let's just remove it rather\n          than fix it. (Had we chosen to fix it, that would make\n          for a subtle and potentially security-sensitive change in\n          the effective meaning of IPv4 <code class=\"filename\">pg_hba.conf</code> entries, which does not\n          seem like a good thing to do in minor releases.)</p>"
  ],
  [
    "<p>Report WAL flush, not insert, position in <code class=\"literal\">IDENTIFY_SYSTEM</code> replication command\n          (Heikki Linnakangas)</p>",
    "<p>This avoids a possible startup failure in <span class=\"application\">pg_receivexlog</span>.</p>"
  ],
  [
    "<p>While shutting down service on Windows, periodically\n          send status updates to the Service Control Manager to\n          prevent it from killing the service too soon; and ensure\n          that <span class=\"application\">pg_ctl</span> will wait\n          for shutdown (Krystian Bigaj)</p>"
  ],
  [
    "<p>Reduce risk of network deadlock when using\n          <span class=\"application\">libpq</span>'s non-blocking\n          mode (Heikki Linnakangas)</p>",
    "<p>When sending large volumes of data, it's important to\n          drain the input buffer every so often, in case the server\n          has sent enough response data to cause it to block on\n          output. (A typical scenario is that the server is sending\n          a stream of NOTICE messages during <code class=\"literal\">COPY FROM STDIN</code>.) This worked properly\n          in the normal blocking mode, but not so much in\n          non-blocking mode. We've modified <span class=\"application\">libpq</span> to opportunistically drain\n          input when it can, but a full defense against this\n          problem requires application cooperation: the application\n          should watch for socket read-ready as well as write-ready\n          conditions, and be sure to call <code class=\"function\">PQconsumeInput()</code> upon read-ready.</p>"
  ],
  [
    "<p>Fix array handling in <span class=\"application\">ecpg</span> (Michael Meskes)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">psql</span> to sanely\n          handle URIs and conninfo strings as the first parameter\n          to <code class=\"command\">\\connect</code> (David Fetter,\n          Andrew Dunstan, &#xC1;lvaro Herrera)</p>",
    "<p>This syntax has been accepted (but undocumented) for a\n          long time, but previously some parameters might be taken\n          from the old connection instead of the given string,\n          which was agreed to be undesirable.</p>"
  ],
  [
    "<p>Suppress incorrect complaints from <span class=\"application\">psql</span> on some platforms that it\n          failed to write <code class=\"filename\">~/.psql_history</code> at exit (Tom Lane)</p>",
    "<p>This misbehavior was caused by a workaround for a bug\n          in very old (pre-2006) versions of <span class=\"application\">libedit</span>. We fixed it by removing the\n          workaround, which will cause a similar failure to appear\n          for anyone still using such versions of <span class=\"application\">libedit</span>. Recommendation: upgrade\n          that library, or use <span class=\"application\">libreadline</span>.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_dump</span>'s rule\n          for deciding which casts are system-provided casts that\n          should not be dumped (Tom Lane)</p>"
  ],
  [
    "<p>In <span class=\"application\">pg_dump</span>, fix\n          failure to honor <code class=\"literal\">-Z</code>\n          compression level option together with <code class=\"literal\">-Fd</code> (Michael Paquier)</p>"
  ],
  [
    "<p>Make <span class=\"application\">pg_dump</span> consider\n          foreign key relationships between extension configuration\n          tables while choosing dump order (Gilles Darold, Michael\n          Paquier, Stephen Frost)</p>",
    "<p>This oversight could result in producing dumps that\n          fail to reload because foreign key constraints are\n          transiently violated.</p>"
  ],
  [
    "<p>Fix dumping of views that are just <code class=\"literal\">VALUES(...)</code> but have column aliases (Tom\n          Lane)</p>"
  ],
  [
    "<p>In <span class=\"application\">pg_upgrade</span>, force\n          timeline 1 in the new cluster (Bruce Momjian)</p>",
    "<p>This change prevents upgrade failures caused by bogus\n          complaints about missing WAL history files.</p>"
  ],
  [
    "<p>In <span class=\"application\">pg_upgrade</span>, check\n          for improperly non-connectable databases before\n          proceeding (Bruce Momjian)</p>"
  ],
  [
    "<p>In <span class=\"application\">pg_upgrade</span>, quote\n          directory paths properly in the generated <code class=\"literal\">delete_old_cluster</code> script (Bruce\n          Momjian)</p>"
  ],
  [
    "<p>In <span class=\"application\">pg_upgrade</span>,\n          preserve database-level freezing info properly (Bruce\n          Momjian)</p>",
    "<p>This oversight could cause missing-clog-file errors\n          for tables within the <code class=\"literal\">postgres</code> and <code class=\"literal\">template1</code> databases.</p>"
  ],
  [
    "<p>Run <span class=\"application\">pg_upgrade</span> and\n          <span class=\"application\">pg_resetxlog</span> with\n          restricted privileges on Windows, so that they don't fail\n          when run by an administrator (Muhammad Asif Naeem)</p>"
  ],
  [
    "<p>Improve handling of <code class=\"function\">readdir()</code> failures when scanning\n          directories in <span class=\"application\">initdb</span>\n          and <span class=\"application\">pg_basebackup</span> (Marco\n          Nenciarini)</p>"
  ],
  [
    "<p>Fix slow sorting algorithm in <code class=\"filename\">contrib/intarray</code> (Tom Lane)</p>"
  ],
  [
    "<p>Fix compile failure on Sparc V8 machines (Rob\n          Rowan)</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2015d for DST law\n          changes in Egypt, Mongolia, and Palestine, plus\n          historical changes in Canada and Chile. Also adopt\n          revised zone abbreviations for the America/Adak zone\n          (HST/HDT not HAST/HADT).</p>"
  ]
]