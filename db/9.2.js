[
  [
    "<p>Allow queries to retrieve data only from indexes,\n              avoiding heap access (Robert Haas, Ibrar Ahmed,\n              Heikki Linnakangas, Tom Lane)</p>",
    "<p>This feature is often called <em class=\"firstterm\">index-only scans</em>. Heap access can be\n              skipped for heap pages containing only tuples that\n              are visible to all sessions, as reported by the\n              visibility map; so the benefit applies mainly to\n              mostly-static data. The visibility map was made\n              crash-safe as a necessary part of implementing this\n              feature.</p>"
  ],
  [
    "<p>Add the <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/spgist.html\" title=\"Chapter&#xA0;63.&#xA0;SP-GiST Indexes\">SP-GiST</a>\n              (Space-Partitioned GiST) index access method (Teodor\n              Sigaev, Oleg Bartunov, Tom Lane)</p>",
    "<p>SP-GiST is comparable to GiST in flexibility, but\n              supports unbalanced partitioned search structures\n              rather than balanced trees. For suitable problems,\n              SP-GiST can be faster than GiST in both index build\n              time and search time.</p>"
  ],
  [
    "<p>Allow group commit to work effectively under heavy\n              load (Peter Geoghegan, Simon Riggs, Heikki\n              Linnakangas)</p>",
    "<p>Previously, batching of commits became ineffective\n              as the write workload increased, because of internal\n              lock contention.</p>"
  ],
  [
    "<p>Allow uncontended locks to be managed using a new\n              fast-path lock mechanism (Robert Haas)</p>"
  ],
  [
    "<p>Reduce overhead of creating virtual transaction ID\n              locks (Robert Haas)</p>"
  ],
  [
    "<p>Reduce the overhead of serializable isolation\n              level locks (Dan Ports)</p>"
  ],
  [
    "<p>Improve PowerPC and Itanium spinlock performance\n              (Manabu Ori, Robert Haas, Tom Lane)</p>"
  ],
  [
    "<p>Reduce overhead for shared invalidation cache\n              messages (Robert Haas)</p>"
  ],
  [
    "<p>Move the frequently accessed members of the\n              <code class=\"structname\">PGPROC</code> shared memory\n              array to a separate array (Pavan Deolasee, Heikki\n              Linnakangas, Robert Haas)</p>"
  ],
  [
    "<p>Improve <code class=\"command\">COPY</code>\n              performance by adding tuples to the heap in batches\n              (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Improve GiST index performance for geometric data\n              types by producing better trees with less memory\n              allocation overhead (Alexander Korotkov)</p>"
  ],
  [
    "<p>Improve GiST index build times (Alexander\n              Korotkov, Heikki Linnakangas)</p>"
  ],
  [
    "<p>Allow hint bits to be set sooner for temporary and\n              unlogged tables (Robert Haas)</p>"
  ],
  [
    "<p>Allow sorting to be performed by inlined,\n              non-<acronym class=\"acronym\">SQL</acronym>-callable\n              comparison functions (Peter Geoghegan, Robert Haas,\n              Tom Lane)</p>"
  ],
  [
    "<p>Make the number of CLOG buffers scale based on\n              <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-resource.html#GUC-SHARED-BUFFERS\"><code class=\"varname\">\n              shared_buffers</code></a> (Robert Haas, Simon Riggs,\n              Tom Lane)</p>"
  ],
  [
    "<p>Improve performance of buffer pool scans that\n              occur when tables or databases are dropped (Jeff\n              Janes, Simon Riggs)</p>"
  ],
  [
    "<p>Improve performance of checkpointer's\n              fsync-request queue when many tables are being\n              dropped or truncated (Tom Lane)</p>"
  ],
  [
    "<p>Pass the safe number of file descriptors to child\n              processes on Windows (Heikki Linnakangas)</p>",
    "<p>This allows Windows sessions to use more open file\n              descriptors than before.</p>"
  ],
  [
    "<p>Create a dedicated background process to perform\n              checkpoints (Simon Riggs)</p>",
    "<p>Formerly the background writer did both dirty-page\n              writing and checkpointing. Separating this into two\n              processes allows each goal to be accomplished more\n              predictably.</p>"
  ],
  [
    "<p>Improve asynchronous commit behavior by waking the\n              walwriter sooner (Simon Riggs)</p>",
    "<p>Previously, only <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-wal.html#GUC-WAL-WRITER-DELAY\"><code class=\"varname\">\n              wal_writer_delay</code></a> triggered <acronym class=\"acronym\">WAL</acronym> flushing to disk; now filling\n              a <acronym class=\"acronym\">WAL</acronym> buffer also\n              triggers <acronym class=\"acronym\">WAL</acronym>\n              writes.</p>"
  ],
  [
    "<p>Allow the bgwriter, walwriter, checkpointer,\n              statistics collector, log collector, and archiver\n              background processes to sleep more efficiently during\n              periods of inactivity (Peter Geoghegan, Tom Lane)</p>",
    "<p>This series of changes reduces the frequency of\n              process wake-ups when there is nothing to do,\n              dramatically reducing power consumption on idle\n              servers.</p>"
  ],
  [
    "<p>Allow the planner to generate custom plans for\n              specific parameter values even when using prepared\n              statements (Tom Lane)</p>",
    "<p>In the past, a prepared statement always had a\n              single <span class=\"quote\">&#x201C;<span class=\"quote\">generic</span>&#x201D;</span> plan that was used for\n              all parameter values, which was frequently much\n              inferior to the plans used for non-prepared\n              statements containing explicit constant values. Now,\n              the planner attempts to generate custom plans for\n              specific parameter values. A generic plan will only\n              be used after custom plans have repeatedly proven to\n              provide no benefit. This change should eliminate the\n              performance penalties formerly seen from use of\n              prepared statements (including non-dynamic statements\n              in PL/pgSQL).</p>"
  ],
  [
    "<p>Improve the planner's ability to use nested loops\n              with inner index scans (Tom Lane)</p>",
    "<p>The new <span class=\"quote\">&#x201C;<span class=\"quote\">parameterized path</span>&#x201D;</span> mechanism\n              allows inner index scans to use values from relations\n              that are more than one join level up from the scan.\n              This can greatly improve performance in situations\n              where semantic restrictions (such as outer joins)\n              limit the allowed join orderings.</p>"
  ],
  [
    "<p>Improve the planning <acronym class=\"acronym\">API</acronym> for foreign data wrappers\n              (Etsuro Fujita, Shigeru Hanada, Tom Lane)</p>",
    "<p>Wrappers can now provide multiple access\n              <span class=\"quote\">&#x201C;<span class=\"quote\">paths</span>&#x201D;</span> for their tables,\n              allowing more flexibility in join planning.</p>"
  ],
  [
    "<p>Recognize self-contradictory restriction clauses\n              for non-table relations (Tom Lane)</p>",
    "<p>This check is only performed when <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-query.html#GUC-CONSTRAINT-EXCLUSION\"><code class=\"varname\">\n              constraint_exclusion</code></a> is <code class=\"literal\">on</code>.</p>"
  ],
  [
    "<p>Allow <code class=\"literal\">indexed_col op\n              ANY(ARRAY[...])</code> conditions to be used in plain\n              index scans and index-only scans (Tom Lane)</p>",
    "<p>Formerly such conditions could only be used in\n              bitmap index scans.</p>"
  ],
  [
    "<p>Support <code class=\"function\">MIN</code>/<code class=\"function\">MAX</code> index optimizations on\n              <code class=\"type\">boolean</code> columns (Marti\n              Raudsepp)</p>"
  ],
  [
    "<p>Account for set-returning functions in\n              <code class=\"command\">SELECT</code> target lists when\n              setting row count estimates (Tom Lane)</p>"
  ],
  [
    "<p>Fix planner to handle indexes with duplicated\n              columns more reliably (Tom Lane)</p>"
  ],
  [
    "<p>Collect and use element-frequency statistics for\n              arrays (Alexander Korotkov, Tom Lane)</p>",
    "<p>This change improves selectivity estimation for\n              the array <code class=\"literal\">&lt;@</code>,\n              <code class=\"literal\">&amp;&amp;</code>, and\n              <code class=\"literal\">@&gt;</code> operators (array\n              containment and overlaps).</p>"
  ],
  [
    "<p>Allow statistics to be collected for foreign\n              tables (Etsuro Fujita)</p>"
  ],
  [
    "<p>Improve cost estimates for use of partial indexes\n              (Tom Lane)</p>"
  ],
  [
    "<p>Improve the planner's ability to use statistics\n              for columns referenced in subqueries (Tom Lane)</p>"
  ],
  [
    "<p>Improve statistical estimates for subqueries using\n              <code class=\"literal\">DISTINCT</code> (Tom Lane)</p>"
  ],
  [
    "<p>Do not treat role names and <code class=\"literal\">samerole</code> specified in <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/auth-pg-hba-conf.html\" title=\"20.1.&#xA0;The pg_hba.conf File\"><code class=\"filename\">pg_hba.conf</code></a> as automatically\n              including superusers (Andrew Dunstan)</p>",
    "<p>This makes it easier to use <code class=\"literal\">reject</code> lines with group roles.</p>"
  ],
  [
    "<p>Adjust <code class=\"filename\">pg_hba.conf</code>\n              processing to handle token parsing more consistently\n              (Brendan Jurd, &#xC1;lvaro Herrera)</p>"
  ],
  [
    "<p>Disallow empty <code class=\"filename\">pg_hba.conf</code> files (Tom Lane)</p>",
    "<p>This was done to more quickly detect\n              misconfiguration.</p>"
  ],
  [
    "<p>Make superuser privilege imply replication\n              privilege (Noah Misch)</p>",
    "<p>This avoids the need to explicitly assign such\n              privileges.</p>"
  ],
  [
    "<p>Attempt to log the current query string during a\n              backend crash (Marti Raudsepp)</p>"
  ],
  [
    "<p>Make logging of autovacuum I/O activity more\n              verbose (Greg Smith, Noah Misch)</p>",
    "<p>This logging is triggered by <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-autovacuum.html#GUC-LOG-AUTOVACUUM-MIN-DURATION\">\n              <code class=\"varname\">log_autovacuum_min_duration</code></a>.</p>"
  ],
  [
    "<p>Make <acronym class=\"acronym\">WAL</acronym> replay\n              report failures sooner (Fujii Masao)</p>",
    "<p>There were some cases where failures were only\n              reported once the server went into master mode.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-admin.html#FUNCTIONS-ADMIN-BACKUP\" title=\"9.26.3.&#xA0;Backup Control Functions\"><code class=\"function\">pg_xlog_location_diff()</code></a> to\n              simplify WAL location comparisons (Euler Taveira de\n              Oliveira)</p>",
    "<p>This is useful for computing replication lag.</p>"
  ],
  [
    "<p>Support configurable event log application names\n              on Windows (MauMau, Magnus Hagander)</p>",
    "<p>This allows different instances to use the event\n              log with different identifiers, by setting the\n              <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-logging.html#GUC-EVENT-SOURCE\"><code class=\"varname\">\n              event_source</code></a> server parameter, which is\n              similar to how <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-logging.html#GUC-SYSLOG-IDENT\"><code class=\"varname\">\n              syslog_ident</code></a> works.</p>"
  ],
  [
    "<p>Change <span class=\"quote\">&#x201C;<span class=\"quote\">unexpected EOF</span>&#x201D;</span> messages to\n              <code class=\"literal\">DEBUG1</code> level, except\n              when there is an open transaction (Magnus\n              Hagander)</p>",
    "<p>This change reduces log chatter caused by\n              applications that close database connections\n              ungracefully.</p>"
  ],
  [
    "<p>Track temporary file sizes and file counts in the\n              <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/monitoring-stats.html#PG-STAT-DATABASE-VIEW\" title=\"Table&#xA0;28.11.&#xA0;pg_stat_database View\"><code class=\"structname\">\n              pg_stat_database</code></a> system view (Tomas\n              Vondra)</p>"
  ],
  [
    "<p>Add a deadlock counter to the <code class=\"structname\">pg_stat_database</code> system view\n              (Magnus Hagander)</p>"
  ],
  [
    "<p>Add a server parameter <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-statistics.html#GUC-TRACK-IO-TIMING\"><code class=\"varname\">\n              track_io_timing</code></a> to track I/O timings (Ants\n              Aasma, Robert Haas)</p>"
  ],
  [
    "<p>Report checkpoint timing information in <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/monitoring-stats.html#PG-STAT-BGWRITER-VIEW\" title=\"Table&#xA0;28.10.&#xA0;pg_stat_bgwriter View\"><code class=\"structname\">\n              pg_stat_bgwriter</code></a> (Greg Smith, Peter\n              Geoghegan)</p>"
  ],
  [
    "<p>Silently ignore nonexistent schemas specified in\n              <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-client.html#GUC-SEARCH-PATH\"><code class=\"varname\">\n              search_path</code></a> (Tom Lane)</p>",
    "<p>This makes it more convenient to use generic path\n              settings, which might include some schemas that don't\n              exist in all databases.</p>"
  ],
  [
    "<p>Allow superusers to set <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-locks.html#GUC-DEADLOCK-TIMEOUT\"><code class=\"varname\">\n              deadlock_timeout</code></a> per-session, not just\n              per-cluster (Noah Misch)</p>",
    "<p>This allows <code class=\"varname\">deadlock_timeout</code> to be reduced for\n              transactions that are likely to be involved in a\n              deadlock, thus detecting the failure more quickly.\n              Alternatively, increasing the value can be used to\n              reduce the chances of a session being chosen for\n              cancellation due to a deadlock.</p>"
  ],
  [
    "<p>Add a server parameter <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-resource.html#GUC-TEMP-FILE-LIMIT\"><code class=\"varname\">\n              temp_file_limit</code></a> to constrain temporary\n              file space usage per session (Mark Kirkwood)</p>"
  ],
  [
    "<p>Allow a superuser to <code class=\"command\">SET</code> an extension's superuser-only\n              custom variable before loading the associated\n              extension (Tom Lane)</p>",
    "<p>The system now remembers whether a <code class=\"command\">SET</code> was performed by a superuser, so\n              that proper privilege checking can be done when the\n              extension is loaded.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/app-postmaster.html\" title=\"postmaster\">postmaster</a> <code class=\"option\">-C</code> option to query configuration\n              parameters (Bruce Momjian)</p>",
    "<p>This allows <span class=\"application\">pg_ctl</span> to better handle cases\n              where <code class=\"envar\">PGDATA</code> or\n              <code class=\"option\">-D</code> points to a\n              configuration-only directory.</p>"
  ],
  [
    "<p>Replace an empty locale name with the implied\n              value in <code class=\"command\">CREATE DATABASE</code>\n              (Tom Lane)</p>",
    "<p>This prevents cases where <code class=\"structname\">pg_database</code>.<code class=\"structfield\">datcollate</code> or <code class=\"structfield\">datctype</code> could be interpreted\n              differently after a server restart.</p>"
  ],
  [
    "<p>Allow multiple errors in <code class=\"filename\">postgresql.conf</code> to be reported,\n                rather than just the first one (Alexey Klyukin, Tom\n                Lane)</p>"
  ],
  [
    "<p>Allow a reload of <code class=\"filename\">postgresql.conf</code> to be processed\n                by all sessions, even if there are some settings\n                that are invalid for particular sessions (Alexey\n                Klyukin)</p>",
    "<p>Previously, such not-valid-within-session values\n                would cause all setting changes to be ignored by\n                that session.</p>"
  ],
  [
    "<p>Add an <code class=\"literal\">include_if_exists</code> facility for\n                configuration files (Greg Smith)</p>",
    "<p>This works the same as <code class=\"literal\">include</code>, except that an error is\n                not thrown if the file is missing.</p>"
  ],
  [
    "<p>Identify the server time zone during\n                <span class=\"application\">initdb</span>, and set\n                <code class=\"filename\">postgresql.conf</code>\n                entries <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-client.html#GUC-TIMEZONE\"><code class=\"varname\">\n                timezone</code></a> and <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-logging.html#GUC-LOG-TIMEZONE\"><code class=\"varname\">\n                log_timezone</code></a> accordingly (Tom Lane)</p>",
    "<p>This avoids expensive time zone probes during\n                server start.</p>"
  ],
  [
    "<p>Fix <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/view-pg-settings.html\" title=\"51.85.&#xA0;pg_settings\"><code class=\"structname\">pg_settings</code></a> to report\n                <code class=\"filename\">postgresql.conf</code> line\n                numbers on Windows (Tom Lane)</p>"
  ],
  [
    "<p>Allow streaming replication slaves to forward data\n            to other slaves (<a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/warm-standby.html#CASCADING-REPLICATION\" title=\"26.2.7.&#xA0;Cascading Replication\"><em class=\"firstterm\">cascading replication</em></a>) (Fujii\n            Masao)</p>",
    "<p>Previously, only the master server could supply\n            streaming replication log files to standby servers.</p>"
  ],
  [
    "<p>Add new <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-wal.html#GUC-SYNCHRONOUS-COMMIT\"><code class=\"varname\">\n            synchronous_commit</code></a> mode <code class=\"literal\">remote_write</code> (Fujii Masao, Simon\n            Riggs)</p>",
    "<p>This mode waits for the standby server to write\n            transaction data to its own operating system, but does\n            not wait for the data to be flushed to the standby's\n            disk.</p>"
  ],
  [
    "<p>Add a <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/app-pgreceivewal.html\" title=\"pg_receivewal\"><span class=\"application\">pg_receivexlog</span></a> tool to archive\n            WAL file changes as they are written, rather than\n            waiting for completed WAL files (Magnus Hagander)</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/app-pgbasebackup.html\" title=\"pg_basebackup\"><span class=\"application\">pg_basebackup</span></a> to make base\n            backups from standby servers (Jun Ishizuka, Fujii\n            Masao)</p>",
    "<p>This feature lets the work of making new base\n            backups be off-loaded from the primary server.</p>"
  ],
  [
    "<p>Allow streaming of WAL files while <span class=\"application\">pg_basebackup</span> is performing a\n            backup (Magnus Hagander)</p>",
    "<p>This allows passing of WAL files to the standby\n            before they are discarded on the primary.</p>"
  ],
  [
    "<p>Cancel the running query if the client gets\n            disconnected (Florian Pflug)</p>",
    "<p>If the backend detects loss of client connection\n            during a query, it will now cancel the query rather\n            than attempting to finish it.</p>"
  ],
  [
    "<p>Retain column names at run time for row expressions\n            (Andrew Dunstan, Tom Lane)</p>",
    "<p>This change allows better results when a row value\n            is converted to <code class=\"type\">hstore</code> or\n            <code class=\"type\">json</code> type: the fields of the\n            resulting value will now have the expected names.</p>"
  ],
  [
    "<p>Improve column labels used for sub-<code class=\"command\">SELECT</code> results (Marti Raudsepp)</p>",
    "<p>Previously, the generic label <code class=\"literal\">?column?</code> was used.</p>"
  ],
  [
    "<p>Improve heuristics for determining the types of\n            unknown values (Tom Lane)</p>",
    "<p>The longstanding rule that an unknown constant might\n            have the same type as the value on the other side of\n            the operator using it is now applied when considering\n            polymorphic operators, not only for simple operator\n            matches.</p>"
  ],
  [
    "<p>Warn about creating casts to or from domain types\n            (Robert Haas)</p>",
    "<p>Such casts have no effect.</p>"
  ],
  [
    "<p>When a row fails a <code class=\"literal\">CHECK</code> or <code class=\"literal\">NOT\n            NULL</code> constraint, show the row's contents as\n            error detail (Jan Kundr&#xE1;t)</p>",
    "<p>This should make it easier to identify which row is\n            problematic when an insert or update is processing many\n            rows.</p>"
  ],
  [
    "<p>Provide more reliable operation during concurrent\n            <acronym class=\"acronym\">DDL</acronym> (Robert Haas,\n            Noah Misch)</p>",
    "<p>This change adds locking that should eliminate\n            <span class=\"quote\">&#x201C;<span class=\"quote\">cache lookup\n            failed</span>&#x201D;</span> errors in many scenarios. Also,\n            it is no longer possible to add relations to a schema\n            that is being concurrently dropped, a scenario that\n            formerly led to inconsistent system catalog\n            contents.</p>"
  ],
  [
    "<p>Add <code class=\"literal\">CONCURRENTLY</code> option\n            to <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-dropindex.html\" title=\"DROP INDEX\"><code class=\"command\">DROP\n            INDEX</code></a> (Simon Riggs)</p>",
    "<p>This allows index removal without blocking other\n            sessions.</p>"
  ],
  [
    "<p>Allow foreign data wrappers to have per-column\n            options (Shigeru Hanada)</p>"
  ],
  [
    "<p>Improve pretty-printing of view definitions (Andrew\n            Dunstan)</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/ddl-constraints.html\" title=\"5.3.&#xA0;Constraints\"><code class=\"literal\">CHECK</code></a> constraints to be declared\n              <code class=\"literal\">NOT VALID</code> (&#xC1;lvaro\n              Herrera)</p>",
    "<p>Adding a <code class=\"literal\">NOT VALID</code>\n              constraint does not cause the table to be scanned to\n              verify that existing rows meet the constraint.\n              Subsequently, newly added or updated rows are\n              checked. Such constraints are ignored by the planner\n              when considering <code class=\"varname\">constraint_exclusion</code>, since it is\n              not certain that all rows meet the constraint.</p>",
    "<p>The new <code class=\"command\">ALTER TABLE\n              VALIDATE</code> command allows <code class=\"literal\">NOT VALID</code> constraints to be checked\n              for existing rows, after which they are converted\n              into ordinary constraints.</p>"
  ],
  [
    "<p>Allow <code class=\"literal\">CHECK</code>\n              constraints to be declared <code class=\"literal\">NO\n              INHERIT</code> (Nikhil Sontakke, Alex Hunsaker,\n              &#xC1;lvaro Herrera)</p>",
    "<p>This makes them enforceable only on the parent\n              table, not on child tables.</p>"
  ],
  [
    "<p>Add the ability to <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-altertable.html\" title=\"ALTER TABLE\">rename</a>\n              constraints (Peter Eisentraut)</p>"
  ],
  [
    "<p>Reduce need to rebuild tables and indexes for\n              certain <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-altertable.html\" title=\"ALTER TABLE\"><code class=\"command\">ALTER\n              TABLE</code></a> ... <code class=\"literal\">ALTER\n              COLUMN TYPE</code> operations (Noah Misch)</p>",
    "<p>Increasing the length limit for a <code class=\"type\">varchar</code> or <code class=\"type\">varbit</code> column, or removing the limit\n              altogether, no longer requires a table rewrite.\n              Similarly, increasing the allowable precision of a\n              <code class=\"type\">numeric</code> column, or changing\n              a column from constrained <code class=\"type\">numeric</code> to unconstrained <code class=\"type\">numeric</code>, no longer requires a table\n              rewrite. Table rewrites are also avoided in similar\n              cases involving the <code class=\"type\">interval</code>, <code class=\"type\">timestamp</code>, and <code class=\"type\">timestamptz</code> types.</p>"
  ],
  [
    "<p>Avoid having <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-altertable.html\" title=\"ALTER TABLE\"><code class=\"command\">ALTER\n              TABLE</code></a> revalidate foreign key constraints\n              in some cases where it is not necessary (Noah\n              Misch)</p>"
  ],
  [
    "<p>Add <code class=\"literal\">IF EXISTS</code> options\n              to some <code class=\"command\">ALTER</code> commands\n              (Pavel Stehule)</p>",
    "<p>For example, <code class=\"command\">ALTER FOREIGN\n              TABLE IF EXISTS foo RENAME TO bar</code>.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-alterforeigndatawrapper.html\" title=\"ALTER FOREIGN DATA WRAPPER\"><code class=\"command\">ALTER FOREIGN DATA WRAPPER</code></a> ...\n              <code class=\"literal\">RENAME</code> and <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-alterserver.html\" title=\"ALTER SERVER\"><code class=\"command\">ALTER\n              SERVER</code></a> ... <code class=\"literal\">RENAME</code> (Peter Eisentraut)</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-alterdomain.html\" title=\"ALTER DOMAIN\"><code class=\"command\">ALTER\n              DOMAIN</code></a> ... <code class=\"literal\">RENAME</code> (Peter Eisentraut)</p>",
    "<p>You could already rename domains using\n              <code class=\"command\">ALTER TYPE</code>.</p>"
  ],
  [
    "<p>Throw an error for <code class=\"command\">ALTER\n              DOMAIN</code> ... <code class=\"literal\">DROP\n              CONSTRAINT</code> on a nonexistent constraint (Peter\n              Eisentraut)</p>",
    "<p>An <code class=\"literal\">IF EXISTS</code> option\n              has been added to provide the previous behavior.</p>"
  ],
  [
    "<p>Allow <code class=\"command\">CREATE TABLE (LIKE\n              ...)</code> from foreign tables, views, and composite\n              types (Peter Eisentraut)</p>",
    "<p>For example, this allows a table to be created\n              whose schema matches a view.</p>"
  ],
  [
    "<p>Fix <code class=\"command\">CREATE TABLE (LIKE\n              ...)</code> to avoid index name conflicts when\n              copying index comments (Tom Lane)</p>"
  ],
  [
    "<p>Fix <code class=\"command\">CREATE TABLE</code> ...\n              <code class=\"literal\">AS EXECUTE</code> to handle\n              <code class=\"literal\">WITH NO DATA</code> and column\n              name specifications (Tom Lane)</p>"
  ],
  [
    "<p>Add a <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-createview.html\" title=\"CREATE VIEW\"><code class=\"literal\">security_barrier</code></a> option for\n              views (KaiGai Kohei, Robert Haas)</p>",
    "<p>This option prevents optimizations that might\n              allow view-protected data to be exposed to users, for\n              example pushing a clause involving an insecure\n              function into the <code class=\"literal\">WHERE</code>\n              clause of the view. Such views can be expected to\n              perform more poorly than ordinary views.</p>"
  ],
  [
    "<p>Add a new <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-createfunction.html\" title=\"CREATE FUNCTION\"><code class=\"literal\">LEAKPROOF</code></a> function attribute to\n              mark functions that can safely be pushed down into\n              <code class=\"literal\">security_barrier</code> views\n              (KaiGai Kohei)</p>"
  ],
  [
    "<p>Add support for privileges on data types (Peter\n              Eisentraut)</p>",
    "<p>This adds support for the <acronym class=\"acronym\">SQL</acronym>-conforming <code class=\"literal\">USAGE</code> privilege on types and\n              domains. The intent is to be able to restrict which\n              users can create dependencies on types, since such\n              dependencies limit the owner's ability to alter the\n              type.</p>"
  ],
  [
    "<p>Check for <code class=\"command\">INSERT</code>\n              privileges in <code class=\"command\">SELECT\n              INTO</code> / <code class=\"command\">CREATE TABLE\n              AS</code> (KaiGai Kohei)</p>",
    "<p>Because the object is being created by\n              <code class=\"command\">SELECT INTO</code> or\n              <code class=\"command\">CREATE TABLE AS</code>, the\n              creator would ordinarily have insert permissions; but\n              there are corner cases where this is not true, such\n              as when <code class=\"literal\">ALTER DEFAULT\n              PRIVILEGES</code> has removed such permissions.</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-vacuum.html\" title=\"VACUUM\"><code class=\"command\">VACUUM</code></a> to\n            more easily skip pages that cannot be locked (Simon\n            Riggs, Robert Haas)</p>",
    "<p>This change should greatly reduce the incidence of\n            <code class=\"command\">VACUUM</code> getting\n            <span class=\"quote\">&#x201C;<span class=\"quote\">stuck</span>&#x201D;</span> waiting for other\n            sessions.</p>"
  ],
  [
    "<p>Make <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-explain.html\" title=\"EXPLAIN\"><code class=\"command\">EXPLAIN</code></a>\n            <code class=\"literal\">(BUFFERS)</code> count blocks\n            dirtied and written (Robert Haas)</p>"
  ],
  [
    "<p>Make <code class=\"command\">EXPLAIN ANALYZE</code>\n            report the number of rows rejected by filter steps\n            (Marko Tiikkaja)</p>"
  ],
  [
    "<p>Allow <code class=\"command\">EXPLAIN ANALYZE</code>\n            to avoid timing overhead when time values are not\n            wanted (Tomas Vondra)</p>",
    "<p>This is accomplished by setting the new <code class=\"literal\">TIMING</code> option to <code class=\"literal\">FALSE</code>.</p>"
  ],
  [
    "<p>Add support for <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/rangetypes.html\" title=\"8.17.&#xA0;Range Types\">range\n            data types</a> (Jeff Davis, Tom Lane, Alexander\n            Korotkov)</p>",
    "<p>A range data type stores a lower and upper bound\n            belonging to its base data type. It supports operations\n            like contains, overlaps, and intersection.</p>"
  ],
  [
    "<p>Add a <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/datatype-json.html\" title=\"8.14.&#xA0;JSON Types\"><code class=\"type\">JSON</code></a> data type (Robert Haas)</p>",
    "<p>This type stores <acronym class=\"acronym\">JSON</acronym> (JavaScript Object Notation)\n            data with proper validation.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-json.html\" title=\"9.15.&#xA0;JSON Functions and Operators\"><code class=\"function\">\n            array_to_json()</code></a> and <code class=\"function\">row_to_json()</code> (Andrew Dunstan)</p>"
  ],
  [
    "<p>Add a <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/datatype-numeric.html#DATATYPE-SERIAL\" title=\"8.1.4.&#xA0;Serial Types\"><code class=\"type\">SMALLSERIAL</code></a> data type (Mike\n            Pultz)</p>",
    "<p>This is like <code class=\"type\">SERIAL</code>,\n            except it stores the sequence in a two-byte integer\n            column (<code class=\"type\">int2</code>).</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-createdomain.html\" title=\"CREATE DOMAIN\">domains</a> to be declared\n            <code class=\"literal\">NOT VALID</code> (&#xC1;lvaro\n            Herrera)</p>",
    "<p>This option can be set at domain creation time, or\n            via <code class=\"command\">ALTER DOMAIN</code> ...\n            <code class=\"literal\">ADD CONSTRAINT</code> ...\n            <code class=\"literal\">NOT VALID</code>. <code class=\"command\">ALTER DOMAIN</code> ... <code class=\"literal\">VALIDATE CONSTRAINT</code> fully validates\n            the constraint.</p>"
  ],
  [
    "<p>Support more locale-specific formatting options for\n            the <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/datatype-money.html\" title=\"8.2.&#xA0;Monetary Types\"><code class=\"type\">money</code></a> data type (Tom Lane)</p>",
    "<p>Specifically, honor all the POSIX options for\n            ordering of the value, sign, and currency symbol in\n            monetary output. Also, make sure that the thousands\n            separator is only inserted to the left of the decimal\n            point, as required by POSIX.</p>"
  ],
  [
    "<p>Add bitwise <span class=\"quote\">&#x201C;<span class=\"quote\">and</span>&#x201D;</span>, <span class=\"quote\">&#x201C;<span class=\"quote\">or</span>&#x201D;</span>, and\n            <span class=\"quote\">&#x201C;<span class=\"quote\">not</span>&#x201D;</span> operators for the\n            <code class=\"type\">macaddr</code> data type (Brendan\n            Jurd)</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-xml.html#FUNCTIONS-XML-PROCESSING\" title=\"9.14.3.&#xA0;Processing XML\"><code class=\"function\">xpath()</code></a> to return a\n            single-element <acronym class=\"acronym\">XML</acronym>\n            array when supplied a scalar value (Florian Pflug)</p>",
    "<p>Previously, it returned an empty array. This change\n            will also cause <code class=\"function\">xpath_exists()</code> to return true, not\n            false, for such expressions.</p>"
  ],
  [
    "<p>Improve <acronym class=\"acronym\">XML</acronym> error\n            handling to be more robust (Florian Pflug)</p>"
  ],
  [
    "<p>Allow non-superusers to use <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-admin.html#FUNCTIONS-ADMIN-SIGNAL\" title=\"9.26.2.&#xA0;Server Signaling Functions\"><code class=\"function\">pg_cancel_backend()</code></a> and <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-admin.html#FUNCTIONS-ADMIN-SIGNAL\" title=\"9.26.2.&#xA0;Server Signaling Functions\"><code class=\"function\">pg_terminate_backend()</code></a> on other\n            sessions belonging to the same user (Magnus Hagander,\n            Josh Kupershmidt, Dan Farina)</p>",
    "<p>Previously only superusers were allowed to use these\n            functions.</p>"
  ],
  [
    "<p>Allow importing and exporting of transaction\n            snapshots (Joachim Wieland, Tom Lane)</p>",
    "<p>This allows multiple transactions to share identical\n            views of the database state. Snapshots are exported via\n            <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-admin.html#FUNCTIONS-SNAPSHOT-SYNCHRONIZATION\" title=\"9.26.5.&#xA0;Snapshot Synchronization Functions\"><code class=\"function\">\n            pg_export_snapshot()</code></a> and imported via\n            <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-set-transaction.html\" title=\"SET TRANSACTION\"><code class=\"command\">SET TRANSACTION\n            SNAPSHOT</code></a>. Only snapshots from\n            currently-running transactions can be imported.</p>"
  ],
  [
    "<p>Support <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-info.html#FUNCTIONS-INFO-CATALOG-TABLE\" title=\"Table&#xA0;9.63.&#xA0;System Catalog Information Functions\">\n            <code class=\"literal\">COLLATION FOR</code></a> on\n            expressions (Peter Eisentraut)</p>",
    "<p>This returns a string representing the collation of\n            the expression.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-info.html#FUNCTIONS-INFO-SCHEMA-TABLE\" title=\"Table&#xA0;9.62.&#xA0;Schema Visibility Inquiry Functions\">\n            <code class=\"function\">pg_opfamily_is_visible()</code></a> (Josh\n            Kupershmidt)</p>"
  ],
  [
    "<p>Add a <code class=\"type\">numeric</code> variant of\n            <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-admin.html#FUNCTIONS-ADMIN-DBSIZE\" title=\"Table&#xA0;9.84.&#xA0;Database Object Size Functions\"><code class=\"function\">\n            pg_size_pretty()</code></a> for use with <code class=\"function\">pg_xlog_location_diff()</code> (Fujii\n            Masao)</p>"
  ],
  [
    "<p>Add a <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-info.html#FUNCTIONS-INFO-SESSION-TABLE\" title=\"Table&#xA0;9.60.&#xA0;Session Information Functions\"><code class=\"function\">\n            pg_trigger_depth()</code></a> function (Kevin\n            Grittner)</p>",
    "<p>This reports the current trigger call depth.</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-aggregate.html#FUNCTIONS-AGGREGATE-TABLE\" title=\"Table&#xA0;9.52.&#xA0;General-Purpose Aggregate Functions\">\n            <code class=\"function\">string_agg()</code></a> to\n            process <code class=\"type\">bytea</code> values (Pavel\n            Stehule)</p>"
  ],
  [
    "<p>Fix regular expressions in which a back-reference\n            occurs within a larger quantified subexpression (Tom\n            Lane)</p>",
    "<p>For example, <code class=\"literal\">^(\\w+)(\n            \\1)+$</code>. Previous releases did not check that the\n            back-reference actually matched the first\n            occurrence.</p>"
  ],
  [
    "<p>Add information schema views <code class=\"structname\">role_udt_grants</code>, <code class=\"structname\">udt_privileges</code>, and <code class=\"structname\">user_defined_types</code> (Peter\n            Eisentraut)</p>"
  ],
  [
    "<p>Add composite-type attributes to the information\n            schema <code class=\"structname\">element_types</code>\n            view (Peter Eisentraut)</p>"
  ],
  [
    "<p>Implement <code class=\"structfield\">interval_type</code> columns in the\n            information schema (Peter Eisentraut)</p>",
    "<p>Formerly these columns read as nulls.</p>"
  ],
  [
    "<p>Implement collation-related columns in the\n            information schema <code class=\"structname\">attributes</code>, <code class=\"structname\">columns</code>, <code class=\"structname\">domains</code>, and <code class=\"structname\">element_types</code> views (Peter\n            Eisentraut)</p>"
  ],
  [
    "<p>Implement the <code class=\"structfield\">with_hierarchy</code> column in the\n            information schema <code class=\"structname\">table_privileges</code> view (Peter\n            Eisentraut)</p>"
  ],
  [
    "<p>Add display of sequence <code class=\"literal\">USAGE</code> privileges to information schema\n            (Peter Eisentraut)</p>"
  ],
  [
    "<p>Make the information schema show default privileges\n            (Peter Eisentraut)</p>",
    "<p>Previously, non-empty default permissions were not\n            represented in the views.</p>"
  ],
  [
    "<p>Allow the PL/pgSQL <code class=\"command\">OPEN</code> cursor command to supply\n              parameters by name (Yeb Havinga)</p>"
  ],
  [
    "<p>Add a <code class=\"command\">GET STACKED\n              DIAGNOSTICS</code> PL/pgSQL command to retrieve\n              exception info (Pavel Stehule)</p>"
  ],
  [
    "<p>Speed up PL/pgSQL array assignment by caching type\n              information (Pavel Stehule)</p>"
  ],
  [
    "<p>Improve performance and memory consumption for\n              long chains of <code class=\"literal\">ELSIF</code>\n              clauses (Tom Lane)</p>"
  ],
  [
    "<p>Output the function signature, not just the name,\n              in PL/pgSQL error messages (Pavel Stehule)</p>"
  ],
  [
    "<p>Add PL/Python <acronym class=\"acronym\">SPI</acronym> cursor support (Jan\n              Urbanski)</p>",
    "<p>This allows PL/Python to read partial result\n              sets.</p>"
  ],
  [
    "<p>Add result metadata functions to PL/Python (Peter\n              Eisentraut)</p>",
    "<p>Specifically, this adds result object functions\n              <code class=\"literal\">.colnames</code>, <code class=\"literal\">.coltypes</code>, and <code class=\"literal\">.coltypmods</code>.</p>"
  ],
  [
    "<p>Remove support for Python 2.2 (Peter\n              Eisentraut)</p>"
  ],
  [
    "<p>Allow <acronym class=\"acronym\">SQL</acronym>-language functions to\n              reference parameters by name (Matthew Draper)</p>",
    "<p>To use this, simply name the function arguments\n              and then reference the argument names in the\n              <acronym class=\"acronym\">SQL</acronym> function\n              body.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/app-initdb.html\" title=\"initdb\"><span class=\"application\">initdb</span></a>\n            options <code class=\"option\">--auth-local</code> and\n            <code class=\"option\">--auth-host</code> (Peter\n            Eisentraut)</p>",
    "<p>This allows separate control of <code class=\"literal\">local</code> and <code class=\"literal\">host</code> <code class=\"filename\">pg_hba.conf</code> authentication settings.\n            <code class=\"option\">--auth</code> still controls\n            both.</p>"
  ],
  [
    "<p>Add <code class=\"option\">--replication</code>/<code class=\"option\">--no-replication</code> flags to <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/app-createuser.html\" title=\"createuser\"><span class=\"application\">createuser</span></a> to control\n            replication permission (Fujii Masao)</p>"
  ],
  [
    "<p>Add the <code class=\"option\">--if-exists</code>\n            option to <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/app-dropdb.html\" title=\"dropdb\"><span class=\"application\">dropdb</span></a>\n            and <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/app-dropuser.html\" title=\"dropuser\"><span class=\"application\">dropuser</span></a> (Josh\n            Kupershmidt)</p>"
  ],
  [
    "<p>Give command-line tools the ability to specify the\n            name of the database to connect to, and fall back to\n            <code class=\"literal\">template1</code> if a\n            <code class=\"literal\">postgres</code> database\n            connection fails (Robert Haas)</p>"
  ],
  [
    "<p>Add a display mode to auto-expand output based on\n              the display width (Peter Eisentraut)</p>",
    "<p>This adds the <code class=\"literal\">auto</code>\n              option to the <code class=\"command\">\\x</code>\n              command, which switches to the expanded mode when the\n              normal output would be wider than the screen.</p>"
  ],
  [
    "<p>Allow inclusion of a script file that is named\n              relative to the directory of the file from which it\n              was invoked (Gurjeet Singh)</p>",
    "<p>This is done with a new command <code class=\"command\">\\ir</code>.</p>"
  ],
  [
    "<p>Add support for non-<acronym class=\"acronym\">ASCII</acronym> characters in <span class=\"application\">psql</span> variable names (Tom\n              Lane)</p>"
  ],
  [
    "<p>Add support for major-version-specific\n              <code class=\"filename\">.psqlrc</code> files (Bruce\n              Momjian)</p>",
    "<p><span class=\"application\">psql</span> already\n              supported minor-version-specific <code class=\"filename\">.psqlrc</code> files.</p>"
  ],
  [
    "<p>Provide environment variable overrides for\n              <span class=\"application\">psql</span> history and\n              startup file locations (Andrew Dunstan)</p>",
    "<p><code class=\"envar\">PSQL_HISTORY</code> and\n              <code class=\"envar\">PSQLRC</code> now determine these\n              file names if set.</p>"
  ],
  [
    "<p>Add a <code class=\"command\">\\setenv</code> command\n              to modify the environment variables passed to child\n              processes (Andrew Dunstan)</p>"
  ],
  [
    "<p>Name <span class=\"application\">psql</span>'s\n              temporary editor files with a <code class=\"filename\">.sql</code> extension (Peter\n              Eisentraut)</p>",
    "<p>This allows extension-sensitive editors to select\n              the right mode.</p>"
  ],
  [
    "<p>Allow <span class=\"application\">psql</span> to use\n              zero-byte field and record separators (Peter\n              Eisentraut)</p>",
    "<p>Various shell tools use zero-byte (NUL)\n              separators, e.g. <span class=\"application\">find</span>.</p>"
  ],
  [
    "<p>Make the <code class=\"command\">\\timing</code>\n              option report times for failed queries (Magnus\n              Hagander)</p>",
    "<p>Previously times were reported only for successful\n              queries.</p>"
  ],
  [
    "<p>Unify and tighten <span class=\"application\">psql</span>'s treatment of <code class=\"command\">\\copy</code> and SQL <code class=\"command\">COPY</code> (Noah Misch)</p>",
    "<p>This fix makes failure behavior more predictable\n              and honors <code class=\"command\">\\set\n              ON_ERROR_ROLLBACK</code>.</p>"
  ],
  [
    "<p>Make <code class=\"command\">\\d</code> on a sequence\n              show the table/column name owning it (Magnus\n              Hagander)</p>"
  ],
  [
    "<p>Show statistics target for columns in <code class=\"command\">\\d+</code> (Magnus Hagander)</p>"
  ],
  [
    "<p>Show role password expiration dates in\n              <code class=\"command\">\\du</code> (Fabr&#xED;zio de Royes\n              Mello)</p>"
  ],
  [
    "<p>Display comments for casts, conversions, domains,\n              and languages (Josh Kupershmidt)</p>",
    "<p>These are included in the output of <code class=\"command\">\\dC+</code>, <code class=\"command\">\\dc+</code>, <code class=\"command\">\\dD+</code>, and <code class=\"command\">\\dL</code> respectively.</p>"
  ],
  [
    "<p>Display comments for <acronym class=\"acronym\">SQL</acronym>/<acronym class=\"acronym\">MED</acronym> objects (Josh\n              Kupershmidt)</p>",
    "<p>These are included in the output of <code class=\"command\">\\des+</code>, <code class=\"command\">\\det+</code>, and <code class=\"command\">\\dew+</code> for foreign servers, foreign\n              tables, and foreign data wrappers respectively.</p>"
  ],
  [
    "<p>Change <code class=\"command\">\\dd</code> to display\n              comments only for object types without their own\n              backslash command (Josh Kupershmidt)</p>"
  ],
  [
    "<p>In <span class=\"application\">psql</span> tab\n              completion, complete <acronym class=\"acronym\">SQL</acronym> keywords in either upper or\n              lower case according to the new <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/app-psql.html#APP-PSQL-VARIABLES\" title=\"Variables\"><code class=\"literal\">COMP_KEYWORD_CASE</code></a> setting (Peter\n              Eisentraut)</p>"
  ],
  [
    "<p>Add tab completion support for <code class=\"command\">EXECUTE</code> (Andreas Karlsson)</p>"
  ],
  [
    "<p>Allow tab completion of role references in\n              <code class=\"command\">GRANT</code>/<code class=\"command\">REVOKE</code> (Peter Eisentraut)</p>"
  ],
  [
    "<p>Allow tab completion of file names to supply\n              quotes, when necessary (Noah Misch)</p>"
  ],
  [
    "<p>Change tab completion support for <code class=\"command\">TABLE</code> to also include views (Magnus\n              Hagander)</p>"
  ],
  [
    "<p>Add an <code class=\"option\">--exclude-table-data</code> option to\n              <span class=\"application\">pg_dump</span> (Andrew\n              Dunstan)</p>",
    "<p>This allows dumping of a table's definition but\n              not its data, on a per-table basis.</p>"
  ],
  [
    "<p>Add a <code class=\"option\">--section</code> option\n              to <span class=\"application\">pg_dump</span> and\n              <span class=\"application\">pg_restore</span> (Andrew\n              Dunstan)</p>",
    "<p>Valid values are <code class=\"literal\">pre-data</code>, <code class=\"literal\">data</code>, and <code class=\"literal\">post-data</code>. The option can be given\n              more than once to select two or more sections.</p>"
  ],
  [
    "<p>Make <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/app-pg-dumpall.html\" title=\"pg_dumpall\"><span class=\"application\">pg_dumpall</span></a> dump all roles\n              first, then all configuration settings on roles (Phil\n              Sorber)</p>",
    "<p>This allows a role's configuration settings to\n              mention other roles without generating an error.</p>"
  ],
  [
    "<p>Allow <span class=\"application\">pg_dumpall</span>\n              to avoid errors if the <code class=\"literal\">postgres</code> database is missing in the\n              new cluster (Robert Haas)</p>"
  ],
  [
    "<p>Dump foreign server user mappings in user name\n              order (Peter Eisentraut)</p>",
    "<p>This helps produce deterministic dump files.</p>"
  ],
  [
    "<p>Dump operators in a predictable order (Peter\n              Eisentraut)</p>"
  ],
  [
    "<p>Tighten rules for when extension configuration\n              tables are dumped by <span class=\"application\">pg_dump</span> (Tom Lane)</p>"
  ],
  [
    "<p>Make <span class=\"application\">pg_dump</span> emit\n              more useful dependency information (Tom Lane)</p>",
    "<p>The dependency links included in archive-format\n              dumps were formerly of very limited use, because they\n              frequently referenced objects that appeared nowhere\n              in the dump. Now they represent actual dependencies\n              (possibly indirect) among the dumped objects.</p>"
  ],
  [
    "<p>Improve <span class=\"application\">pg_dump</span>'s\n              performance when dumping many database objects (Tom\n              Lane)</p>"
  ],
  [
    "<p>Allow <span class=\"application\">libpq</span>\n            connection strings to have the format of a <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/libpq-connect.html#LIBPQ-CONNSTRING\" title=\"33.1.1.&#xA0;Connection Strings\"><acronym class=\"acronym\">URI</acronym></a> (Alexander Shulgin)</p>",
    "<p>The syntax begins with <code class=\"literal\">postgres://</code>. This can allow\n            applications to avoid implementing their own parser for\n            URIs representing database connections.</p>"
  ],
  [
    "<p>Add a <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/libpq-connect.html#LIBPQ-CONNECT-SSLCOMPRESSION\">connection\n            option</a> to disable <acronym class=\"acronym\">SSL</acronym> compression (Laurenz Albe)</p>",
    "<p>This can be used to remove the overhead of\n            <acronym class=\"acronym\">SSL</acronym> compression on\n            fast networks.</p>"
  ],
  [
    "<p>Add a <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/libpq-single-row-mode.html\" title=\"33.5.&#xA0;Retrieving Query Results Row-By-Row\">single-row\n            processing mode</a> for better handling of large result\n            sets (Kyotaro Horiguchi, Marko Kreen)</p>",
    "<p>Previously, <span class=\"application\">libpq</span>\n            always collected the entire query result in memory\n            before passing it back to the application.</p>"
  ],
  [
    "<p>Add <code class=\"literal\">const</code> qualifiers to\n            the declarations of the functions <code class=\"function\">PQconnectdbParams</code>, <code class=\"function\">PQconnectStartParams</code>, and\n            <code class=\"function\">PQpingParams</code> (Lionel Elie\n            Mamane)</p>"
  ],
  [
    "<p>Allow the <code class=\"filename\">.pgpass</code> file\n            to include escaped characters in the password field\n            (Robert Haas)</p>"
  ],
  [
    "<p>Make library functions use <code class=\"function\">abort()</code> instead of <code class=\"function\">exit()</code> when it is necessary to\n            terminate the process (Peter Eisentraut)</p>",
    "<p>This choice does not interfere with the normal exit\n            codes used by the program, and generates a signal that\n            can be caught by the caller.</p>"
  ],
  [
    "<p>Remove dead ports (Peter Eisentraut)</p>",
    "<p>The following platforms are no longer supported:\n            dgux, nextstep, sunos4, svr4, ultrix4, univel,\n            bsdi.</p>"
  ],
  [
    "<p>Add support for building with <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/install-windows.html\" title=\"Chapter&#xA0;17.&#xA0;Installation from Source Code on Windows\">\n            MS Visual Studio 2010</a> (Brar Piening)</p>"
  ],
  [
    "<p>Enable compiling with the MinGW-w64 32-bit compiler\n            (Lars Kanis)</p>"
  ],
  [
    "<p>Install <code class=\"filename\">plpgsql.h</code> into\n            <code class=\"filename\">include/server</code> during\n            installation (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Improve the latch facility to include detection of\n            postmaster death (Peter Geoghegan, Heikki Linnakangas,\n            Tom Lane)</p>",
    "<p>This eliminates one of the main reasons that\n            background processes formerly had to wake up to poll\n            for events.</p>"
  ],
  [
    "<p>Use C flexible array members, where supported (Peter\n            Eisentraut)</p>"
  ],
  [
    "<p>Improve the concurrent transaction regression tests\n            (<span class=\"application\">isolationtester</span>)\n            (Noah Misch)</p>"
  ],
  [
    "<p>Modify <span class=\"application\">thread_test</span>\n            to create its test files in the current directory,\n            rather than <code class=\"filename\">/tmp</code> (Bruce\n            Momjian)</p>"
  ],
  [
    "<p>Improve flex and bison warning and error reporting\n            (Tom Lane)</p>"
  ],
  [
    "<p>Add memory barrier support (Robert Haas)</p>",
    "<p>This is currently unused.</p>"
  ],
  [
    "<p>Modify pgindent to use a typedef file (Bruce\n            Momjian)</p>"
  ],
  [
    "<p>Add a hook for processing messages due to be sent to\n            the server log (Martin Pihlak)</p>"
  ],
  [
    "<p>Add object access hooks for <code class=\"command\">DROP</code> commands (KaiGai Kohei)</p>"
  ],
  [
    "<p>Centralize <code class=\"command\">DROP</code>\n            handling for some object types (KaiGai Kohei)</p>"
  ],
  [
    "<p>Add a <span class=\"application\">pg_upgrade</span>\n            test suite (Peter Eisentraut)</p>"
  ],
  [
    "<p>Sync regular expression code with <acronym class=\"acronym\">TCL</acronym> 8.5.11 and improve internal\n            processing (Tom Lane)</p>"
  ],
  [
    "<p>Move <acronym class=\"acronym\">CRC</acronym> tables\n            to libpgport, and provide them in a separate include\n            file (Daniel Farina)</p>"
  ],
  [
    "<p>Add options to <span class=\"application\">git_changelog</span> for use in major\n            release note creation (Bruce Momjian)</p>"
  ],
  [
    "<p>Support Linux's <code class=\"filename\">/proc/self/oom_score_adj</code> API (Tom\n            Lane)</p>"
  ],
  [
    "<p>Improve efficiency of <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/dblink.html\" title=\"F.11.&#xA0;dblink\">dblink</a> by\n            using libpq's new single-row processing mode (Kyotaro\n            Horiguchi, Marko Kreen)</p>",
    "<p>This improvement does not apply to <code class=\"function\">dblink_send_query()</code>/<code class=\"function\">dblink_get_result()</code>.</p>"
  ],
  [
    "<p>Support <code class=\"literal\">force_not_null</code>\n            option in <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/file-fdw.html\" title=\"F.15.&#xA0;file_fdw\">file_fdw</a> (Shigeru Hanada)</p>"
  ],
  [
    "<p>Implement dry-run mode for <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/pgarchivecleanup.html\" title=\"pg_archivecleanup\"><span class=\"application\">pg_archivecleanup</span></a> (Gabriele\n            Bartolini)</p>",
    "<p>This only outputs the names of files to be\n            deleted.</p>"
  ],
  [
    "<p>Add new <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/pgbench.html\" title=\"pgbench\">pgbench</a> switches <code class=\"option\">--unlogged-tables</code>, <code class=\"option\">--tablespace</code>, and <code class=\"option\">--index-tablespace</code> (Robert Haas)</p>"
  ],
  [
    "<p>Change <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/pgtestfsync.html\" title=\"pg_test_fsync\"><span class=\"application\">pg_test_fsync</span></a> to test for a\n            fixed amount of time, rather than a fixed number of\n            cycles (Bruce Momjian)</p>",
    "<p>The <code class=\"option\">-o</code>/cycles option was\n            removed, and <code class=\"option\">-s</code>/seconds\n            added.</p>"
  ],
  [
    "<p>Add a <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/pgtesttiming.html\" title=\"pg_test_timing\"><span class=\"application\">pg_test_timing</span></a> utility to\n            measure clock monotonicity and timing overhead (Ants\n            Aasma, Greg Smith)</p>"
  ],
  [
    "<p>Add a <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/tcn.html\" title=\"F.40.&#xA0;tcn\">tcn</a> (triggered change\n            notification) module to generate <code class=\"command\">NOTIFY</code> events on table changes (Kevin\n            Grittner)</p>"
  ],
  [
    "<p>Adjust <span class=\"application\">pg_upgrade</span>\n              environment variables (Bruce Momjian)</p>",
    "<p>Rename data, bin, and port environment variables\n              to begin with <code class=\"literal\">PG</code>, and\n              support <code class=\"envar\">PGPORTOLD</code>/<code class=\"envar\">PGPORTNEW</code>, to replace <code class=\"envar\">PGPORT</code>.</p>"
  ],
  [
    "<p>Overhaul <span class=\"application\">pg_upgrade</span> logging and failure\n              reporting (Bruce Momjian)</p>",
    "<p>Create four append-only log files, and delete them\n              on success. Add <code class=\"option\">-r</code>/<code class=\"option\">--retain</code> option to unconditionally\n              retain these files. Also remove <span class=\"application\">pg_upgrade</span> options <code class=\"option\">-g</code>/<code class=\"option\">-G</code>/<code class=\"option\">-l</code>\n              options as unnecessary, and tighten log file\n              permissions.</p>"
  ],
  [
    "<p>Make <span class=\"application\">pg_upgrade</span>\n              create a script to incrementally generate more\n              accurate optimizer statistics (Bruce Momjian)</p>",
    "<p>This reduces the time needed to generate minimal\n              cluster statistics after an upgrade.</p>"
  ],
  [
    "<p>Allow <span class=\"application\">pg_upgrade</span>\n              to upgrade an old cluster that does not have a\n              <code class=\"literal\">postgres</code> database (Bruce\n              Momjian)</p>"
  ],
  [
    "<p>Allow <span class=\"application\">pg_upgrade</span>\n              to handle cases where some old or new databases are\n              missing, as long as they are empty (Bruce\n              Momjian)</p>"
  ],
  [
    "<p>Allow <span class=\"application\">pg_upgrade</span>\n              to handle configuration-only directory installations\n              (Bruce Momjian)</p>"
  ],
  [
    "<p>In <span class=\"application\">pg_upgrade</span>,\n              add <code class=\"option\">-o</code>/<code class=\"option\">-O</code> options to pass parameters to the\n              servers (Bruce Momjian)</p>",
    "<p>This is useful for configuration-only directory\n              installs.</p>"
  ],
  [
    "<p>Change <span class=\"application\">pg_upgrade</span>\n              to use port 50432 by default (Bruce Momjian)</p>",
    "<p>This helps avoid unintended client connections\n              during the upgrade.</p>"
  ],
  [
    "<p>Reduce cluster locking in <span class=\"application\">pg_upgrade</span> (Bruce Momjian)</p>",
    "<p>Specifically, only lock the old cluster if link\n              mode is used, and do it right after the schema is\n              restored.</p>"
  ],
  [
    "<p>Allow <span class=\"application\">pg_stat_statements</span> to aggregate\n              similar queries via SQL text normalization (Peter\n              Geoghegan, Tom Lane)</p>",
    "<p>Users with applications that use non-parameterized\n              SQL will now be able to monitor query performance\n              without detailed log analysis.</p>"
  ],
  [
    "<p>Add dirtied and written block counts and\n              read/write times to <span class=\"application\">pg_stat_statements</span> (Robert Haas,\n              Ants Aasma)</p>"
  ],
  [
    "<p>Prevent <span class=\"application\">pg_stat_statements</span> from\n              double-counting <code class=\"command\">PREPARE</code>\n              and <code class=\"command\">EXECUTE</code> commands\n              (Tom Lane)</p>"
  ],
  [
    "<p>Support <code class=\"literal\">SECURITY\n              LABEL</code> on global objects (KaiGai Kohei, Robert\n              Haas)</p>",
    "<p>Specifically, add security labels to databases,\n              tablespaces, and roles.</p>"
  ],
  [
    "<p>Allow sepgsql to honor database labels (KaiGai\n              Kohei)</p>"
  ],
  [
    "<p>Perform sepgsql permission checks during the\n              creation of various objects (KaiGai Kohei)</p>"
  ],
  [
    "<p>Add <code class=\"function\">sepgsql_setcon()</code>\n              and related functions to control the sepgsql security\n              domain (KaiGai Kohei)</p>"
  ],
  [
    "<p>Add a user space access cache to sepgsql to\n              improve performance (KaiGai Kohei)</p>"
  ],
  [
    "<p>Add a rule to optionally build HTML documentation\n            using the stylesheet from the website (Magnus\n            Hagander)</p>",
    "<p>Use <code class=\"command\">gmake STYLE=website\n            draft</code>.</p>"
  ],
  [
    "<p>Improve <code class=\"command\">EXPLAIN</code>\n            documentation (Tom Lane)</p>"
  ],
  [
    "<p>Document that user/database names are preserved with\n            double-quoting by command-line tools like <span class=\"application\">vacuumdb</span> (Bruce Momjian)</p>"
  ],
  [
    "<p>Document the actual string returned by the client\n            for MD5 authentication (Cyan Ogilvie)</p>"
  ],
  [
    "<p>Deprecate use of <code class=\"literal\">GLOBAL</code>\n            and <code class=\"literal\">LOCAL</code> in <code class=\"command\">CREATE TEMP TABLE</code> (Noah Misch)</p>",
    "<p><span class=\"productname\">PostgreSQL</span> has long\n            treated these keyword as no-ops, and continues to do\n            so; but in future they might mean what the SQL standard\n            says they mean, so applications should avoid using\n            them.</p>"
  ]
]