[
  [
    "<p>Prevent row-level security policies from being bypassed via selectivity estimators (Dean Rasheed)</p>",
    "<p>Some of the planner's selectivity estimators apply user-defined operators to values found in <code class=\"structname\">pg_statistic</code> (e.g., most-common values). A leaky operator therefore can disclose some of the entries in a data column, even if the calling user lacks permission to read that column. In CVE-2017-7484 we added restrictions to forestall that, but we failed to consider the effects of row-level security. A user who has SQL permission to read a column, but who is forbidden to see certain rows due to RLS policy, might still learn something about those rows' contents via a leaky operator. This patch further tightens the rules, allowing leaky operators to be applied to statistics data only when there is no relevant RLS policy. (CVE-2019-10130)</p>"
  ],
  [
    "<p>Avoid catalog corruption when a temporary table with <code class=\"literal\">ON COMMIT DROP</code> and an identity column is created in a single-statement transaction (Peter Eisentraut)</p>",
    "<p>This hazard was overlooked because the case is not actually useful, since the temporary table would be dropped immediately after creation.</p>"
  ],
  [
    "<p>Avoid crash when an EPQ recheck is performed for a partitioned query result relation (Amit Langote)</p>",
    "<p>This occurs when using <code class=\"literal\">READ COMMITTED</code> isolation level and another session has concurrently updated some of the target row(s).</p>"
  ],
  [
    "<p>Fix behavior for an <code class=\"command\">UPDATE</code> or <code class=\"command\">DELETE</code> on an inheritance tree or partitioned table in which every table can be excluded (Amit Langote, Tom Lane)</p>",
    "<p>In such cases, the query did not report the correct set of output columns when a <code class=\"literal\">RETURNING</code> clause was present, and if there were any statement-level triggers that should be fired, it didn't fire them.</p>"
  ],
  [
    "<p>Avoid throwing incorrect errors for updates of temporary tables and unlogged tables when a <code class=\"literal\">FOR ALL TABLES</code> publication exists (Peter Eisentraut)</p>",
    "<p>Such tables should be ignored for publication purposes, but some parts of the code failed to do so.</p>"
  ],
  [
    "<p>Fix handling of explicit <code class=\"literal\">DEFAULT</code> items in an <code class=\"command\">INSERT ... VALUES</code> command with multiple <code class=\"literal\">VALUES</code> rows, if the target relation is an updatable view (Amit Langote, Dean Rasheed)</p>",
    "<p>When the updatable view has no default for the column but its underlying table has one, a single-row <code class=\"command\">INSERT ... VALUES</code> will use the underlying table's default. In the multi-row case, however, NULL was always used. Correct it to act like the single-row case.</p>"
  ],
  [
    "<p>Fix <code class=\"command\">CREATE VIEW</code> to allow zero-column views (Ashutosh Sharma)</p>",
    "<p>We should allow this for consistency with allowing zero-column tables. Since a table can be converted to a view, zero-column views could be created even with the restriction in place, leading to dump/reload failures.</p>"
  ],
  [
    "<p>Add missing support for <code class=\"command\">CREATE TABLE IF NOT EXISTS ... AS EXECUTE ...</code> (Andreas Karlsson)</p>",
    "<p>The combination of <code class=\"literal\">IF NOT EXISTS</code> and <code class=\"literal\">EXECUTE</code> should work, but the grammar omitted it.</p>"
  ],
  [
    "<p>Ensure that sub-<code class=\"command\">SELECT</code>s appearing in row-level-security policy expressions are executed with the correct user's permissions (Dean Rasheed)</p>",
    "<p>Previously, if the table having the RLS policy was accessed via a view, such checks might be executed as the user calling the view, not as the view owner as they should be.</p>"
  ],
  [
    "<p>Accept XML documents as valid values of type <code class=\"type\">xml</code> when <code class=\"varname\">xmloption</code> is set to <code class=\"literal\">content</code>, as required by SQL:2006 and later (Chapman Flack)</p>",
    "<p>Previously <span class=\"productname\">PostgreSQL</span> followed the SQL:2003 definition, which doesn't allow this. But that creates a serious problem for dump/restore: there is no setting of <code class=\"varname\">xmloption</code> that will accept all valid XML data. Hence, switch to the 2006 definition.</p>",
    "<p><span class=\"application\">pg_dump</span> is also modified to emit <code class=\"literal\">SET xmloption = content</code> while restoring data, ensuring that dump/restore works even if the prevailing setting is <code class=\"literal\">document</code>.</p>"
  ],
  [
    "<p>Improve server's startup-time checks for whether a pre-existing shared memory segment is still in use (Noah Misch)</p>",
    "<p>The postmaster is now more likely to detect that there are still active processes from a previous postmaster incarnation, even if the <code class=\"filename\">postmaster.pid</code> file has been removed.</p>"
  ],
  [
    "<p>Avoid counting parallel workers' transactions as separate transactions (Haribabu Kommi)</p>"
  ],
  [
    "<p>Fix incompatibility of GIN-index WAL records (Alexander Korotkov)</p>",
    "<p>A fix applied in February's minor releases was not sufficiently careful about backwards compatibility, leading to problems if a standby server of that vintage reads GIN page-deletion WAL records generated by a primary server of a previous minor release.</p>"
  ],
  [
    "<p>Fix possible crash while executing a <code class=\"command\">SHOW</code> command in a replication connection (Michael Paquier)</p>"
  ],
  [
    "<p>Avoid memory leak when a partition's relation cache entry is rebuilt (Amit Langote, Tom Lane)</p>"
  ],
  [
    "<p>Tolerate <code class=\"literal\">EINVAL</code> and <code class=\"literal\">ENOSYS</code> error results, where appropriate, for <code class=\"function\">fsync</code> and <code class=\"function\">sync_file_range</code> calls (Thomas Munro, James Sewell)</p>",
    "<p>The previous change to panic on file synchronization failures turns out to have been excessively paranoid for certain cases where a failure is predictable and essentially means <span class=\"quote\">“<span class=\"quote\">operation not supported</span>”</span>.</p>"
  ],
  [
    "<p>Report correct relation name in autovacuum's <code class=\"structname\">pg_stat_activity</code> display during BRIN summarize operations (Álvaro Herrera)</p>"
  ],
  [
    "<p>Fix <span class=\"quote\">“<span class=\"quote\">failed to build any <em class=\"replaceable\"><code>N</code></em>-way joins</span>”</span> planner failures with lateral references leading out of <code class=\"literal\">FULL</code> outer joins (Tom Lane)</p>"
  ],
  [
    "<p>Fix misplanning of queries in which a set-returning function is applied to a relation that is provably empty (Tom Lane, Julien Rouhaud)</p>",
    "<p>In v10, this oversight only led to slightly inefficient plans, but in v11 it could cause <span class=\"quote\">“<span class=\"quote\">set-valued function called in context that cannot accept a set</span>”</span> errors.</p>"
  ],
  [
    "<p>Check the appropriate user's permissions when enforcing rules about letting a leaky operator see <code class=\"structname\">pg_statistic</code> data (Dean Rasheed)</p>",
    "<p>When an underlying table is being accessed via a view, consider the privileges of the view owner while deciding whether leaky operators may be applied to the table's statistics data, rather than the privileges of the user making the query. This makes the planner's rules about what data is visible match up with the executor's, avoiding unnecessarily-poor plans.</p>"
  ],
  [
    "<p>Speed up planning when there are many equality conditions and many potentially-relevant foreign key constraints (David Rowley)</p>"
  ],
  [
    "<p>Avoid O(N^2) performance issue when rolling back a transaction that created many tables (Tomas Vondra)</p>"
  ],
  [
    "<p>Fix corner-case server crashes in dynamic shared memory allocation (Thomas Munro, Robert Haas)</p>"
  ],
  [
    "<p>Fix race conditions in management of dynamic shared memory (Thomas Munro)</p>",
    "<p>These could lead to <span class=\"quote\">“<span class=\"quote\">dsa_area could not attach to segment</span>”</span> or <span class=\"quote\">“<span class=\"quote\">cannot unpin a segment that is not pinned</span>”</span> errors.</p>"
  ],
  [
    "<p>Fix race condition in which a hot-standby postmaster could fail to shut down after receiving a smart-shutdown request (Tom Lane)</p>"
  ],
  [
    "<p>Fix possible crash when <code class=\"function\">pg_identify_object_as_address()</code> is given invalid input (Álvaro Herrera)</p>"
  ],
  [
    "<p>Fix possible <span class=\"quote\">“<span class=\"quote\">could not access status of transaction</span>”</span> failures in <code class=\"function\">txid_status()</code> (Thomas Munro)</p>"
  ],
  [
    "<p>Tighten validation of encoded SCRAM-SHA-256 and MD5 passwords (Jonathan Katz)</p>",
    "<p>A password string that had the right initial characters could be mistaken for one that is correctly hashed into SCRAM-SHA-256 or MD5 format. The password would be accepted but would be unusable later.</p>"
  ],
  [
    "<p>Fix handling of <code class=\"varname\">lc_time</code> settings that imply an encoding different from the database's encoding (Juan José Santamaría Flecha, Tom Lane)</p>",
    "<p>Localized month or day names that include non-ASCII characters previously caused unexpected errors or wrong output in such locales.</p>"
  ],
  [
    "<p>Fix incorrect <code class=\"varname\">operator_precedence_warning</code> checks involving unary minus operators (Rikard Falkeborn)</p>"
  ],
  [
    "<p>Disallow <code class=\"literal\">NaN</code> as a value for floating-point server parameters (Tom Lane)</p>"
  ],
  [
    "<p>Rearrange <code class=\"command\">REINDEX</code> processing to avoid assertion failures when reindexing individual indexes of <code class=\"structname\">pg_class</code> (Andres Freund, Tom Lane)</p>"
  ],
  [
    "<p>Fix planner assertion failure for parameterized dummy paths (Tom Lane)</p>"
  ],
  [
    "<p>Insert correct test function in the result of <code class=\"function\">SnapBuildInitialSnapshot()</code> (Antonin Houska)</p>",
    "<p>No core code cares about this, but some extensions do.</p>"
  ],
  [
    "<p>Fix intermittent <span class=\"quote\">“<span class=\"quote\">could not reattach to shared memory</span>”</span> session startup failures on Windows (Noah Misch)</p>",
    "<p>A previously unrecognized source of these failures is creation of thread stacks for a process's default thread pool. Arrange for such stacks to be allocated in a different memory region.</p>"
  ],
  [
    "<p>Fix error detection in directory scanning on Windows (Konstantin Knizhnik)</p>",
    "<p>Errors, such as lack of permissions to read the directory, were not detected or reported correctly; instead the code silently acted as though the directory were empty.</p>"
  ],
  [
    "<p>Fix grammar problems in <span class=\"application\">ecpg</span> (Tom Lane)</p>",
    "<p>A missing semicolon led to mistranslation of <code class=\"literal\">SET <em class=\"replaceable\"><code>variable</code></em> = DEFAULT</code> (but not <code class=\"literal\">SET <em class=\"replaceable\"><code>variable</code></em> TO DEFAULT</code>) in <span class=\"application\">ecpg</span> programs, producing syntactically invalid output that the server would reject. Additionally, in a <code class=\"command\">DROP TYPE</code> or <code class=\"command\">DROP DOMAIN</code> command that listed multiple type names, only the first type name was actually processed.</p>"
  ],
  [
    "<p>Sync <span class=\"application\">ecpg</span>'s syntax for <code class=\"command\">CREATE TABLE AS</code> with the server's (Daisuke Higuchi)</p>"
  ],
  [
    "<p>Fix possible buffer overruns in <span class=\"application\">ecpg</span>'s processing of include filenames (Liu Huailing, Fei Wu)</p>"
  ],
  [
    "<p>Avoid crash in <code class=\"filename\">contrib/postgres_fdw</code> when a query using remote grouping or aggregation has a <code class=\"literal\">SELECT</code>-list item that is an uncorrelated sub-select, outer reference, or parameter symbol (Tom Lane)</p>"
  ],
  [
    "<p>Avoid crash in <code class=\"filename\">contrib/vacuumlo</code> if an <code class=\"function\">lo_unlink()</code> call failed (Tom Lane)</p>"
  ],
  [
    "<p>Sync our copy of the timezone library with IANA tzcode release 2019a (Tom Lane)</p>",
    "<p>This corrects a small bug in <span class=\"application\">zic</span> that caused it to output an incorrect year-2440 transition in the <code class=\"literal\">Africa/Casablanca</code> zone, and adds support for <span class=\"application\">zic</span>'s new <code class=\"option\">-r</code> option.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2019a for DST law changes in Palestine and Metlakatla, plus historical corrections for Israel.</p>",
    "<p><code class=\"literal\">Etc/UCT</code> is now a backward-compatibility link to <code class=\"literal\">Etc/UTC</code>, instead of being a separate zone that generates the abbreviation <code class=\"literal\">UCT</code>, which nowadays is typically a typo. <span class=\"productname\">PostgreSQL</span> will still accept <code class=\"literal\">UCT</code> as an input zone abbreviation, but it won't output it.</p>"
  ]
]