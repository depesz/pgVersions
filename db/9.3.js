[
  [
    "<p>Prevent non-key-field row updates from blocking\n              foreign key checks (&#xC1;lvaro Herrera, Noah Misch,\n              Andres Freund, Alexander Shulgin, Marti Raudsepp,\n              Alexander Shulgin)</p>",
    "<p>This change improves concurrency and reduces the\n              probability of deadlocks when updating tables\n              involved in a foreign-key constraint. <code class=\"command\">UPDATE</code>s that do not change any\n              columns referenced in a foreign key now take the new\n              <code class=\"literal\">NO KEY UPDATE</code> lock mode\n              on the row, while foreign key checks use the new\n              <code class=\"literal\">KEY SHARE</code> lock mode,\n              which does not conflict with <code class=\"literal\">NO\n              KEY UPDATE</code>. So there is no blocking unless a\n              foreign-key column is changed.</p>"
  ],
  [
    "<p>Add configuration variable <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-client.html#GUC-LOCK-TIMEOUT\"><code class=\"varname\">\n              lock_timeout</code></a> to allow limiting how long a\n              session will wait to acquire any one lock (Zolt&#xE1;n\n              B&#xF6;sz&#xF6;rm&#xE9;nyi)</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/rangetypes.html#RANGETYPES-INDEXING\" title=\"8.17.9.&#xA0;Indexing\"><acronym class=\"acronym\">SP-GiST</acronym></a> support for range\n              data types (Alexander Korotkov)</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/gist.html\" title=\"Chapter&#xA0;62.&#xA0;GiST Indexes\"><acronym class=\"acronym\">GiST</acronym></a> indexes to be unlogged\n              (Jeevan Chalke)</p>"
  ],
  [
    "<p>Improve performance of <acronym class=\"acronym\">GiST</acronym> index insertion by\n              randomizing the choice of which page to descend to\n              when there are multiple equally good alternatives\n              (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Improve concurrency of hash index operations\n              (Robert Haas)</p>"
  ],
  [
    "<p>Collect and use histograms of upper and lower\n              bounds, as well as range lengths, for <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/rangetypes.html\" title=\"8.17.&#xA0;Range Types\">range types</a> (Alexander\n              Korotkov)</p>"
  ],
  [
    "<p>Improve optimizer's cost estimation for index\n              access (Tom Lane)</p>"
  ],
  [
    "<p>Improve optimizer's hash table size estimate for\n              doing <code class=\"literal\">DISTINCT</code> via hash\n              aggregation (Tom Lane)</p>"
  ],
  [
    "<p>Suppress no-op Result and Limit plan nodes\n              (Kyotaro Horiguchi, Amit Kapila, Tom Lane)</p>"
  ],
  [
    "<p>Reduce optimizer overhead by not keeping plans on\n              the basis of cheap startup cost when the optimizer\n              only cares about total cost overall (Tom Lane)</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-copy.html\" title=\"COPY\"><code class=\"command\">COPY FREEZE</code></a>\n              option to avoid the overhead of marking tuples as\n              frozen later (Simon Riggs, Jeff Davis)</p>"
  ],
  [
    "<p>Improve performance of <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/datatype-numeric.html\" title=\"8.1.&#xA0;Numeric Types\"><code class=\"type\">NUMERIC</code></a> calculations (Kyotaro\n              Horiguchi)</p>"
  ],
  [
    "<p>Improve synchronization of sessions waiting for\n              <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-wal.html#GUC-COMMIT-DELAY\"><code class=\"varname\">\n              commit_delay</code></a> (Peter Geoghegan)</p>",
    "<p>This greatly improves the usefulness of\n              <code class=\"varname\">commit_delay</code>.</p>"
  ],
  [
    "<p>Improve performance of the <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-createtable.html\" title=\"CREATE TABLE\"><code class=\"command\">CREATE TEMPORARY\n              TABLE ... ON COMMIT DELETE ROWS</code></a> option by\n              not truncating such temporary tables in transactions\n              that haven't touched any temporary tables (Heikki\n              Linnakangas)</p>"
  ],
  [
    "<p>Make vacuum recheck visibility after it has\n              removed expired tuples (Pavan Deolasee)</p>",
    "<p>This increases the chance of a page being marked\n              as all-visible.</p>"
  ],
  [
    "<p>Add per-resource-owner lock caches (Jeff\n              Janes)</p>",
    "<p>This speeds up lock bookkeeping at statement\n              completion in multi-statement transactions that hold\n              many locks; it is particularly useful for\n              <span class=\"application\">pg_dump</span>.</p>"
  ],
  [
    "<p>Avoid scanning the entire relation cache at commit\n              of a transaction that creates a new relation (Jeff\n              Janes)</p>",
    "<p>This speeds up sessions that create many tables in\n              successive small transactions, such as a <span class=\"application\">pg_restore</span> run.</p>"
  ],
  [
    "<p>Improve performance of transactions that drop many\n              relations (Tomas Vondra)</p>"
  ],
  [
    "<p>Add optional ability to <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/app-initdb.html#APP-INITDB-DATA-CHECKSUMS\">checksum</a>\n              data pages and report corruption (Simon Riggs, Jeff\n              Davis, Greg Smith, Ants Aasma)</p>",
    "<p>The checksum option can be set during <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/app-initdb.html\" title=\"initdb\">initdb</a>.</p>"
  ],
  [
    "<p>Split the <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/monitoring-stats.html\" title=\"28.2.&#xA0;The Statistics Collector\">statistics\n              collector's</a> data file into separate global and\n              per-database files (Tomas Vondra)</p>",
    "<p>This reduces the I/O required for statistics\n              tracking.</p>"
  ],
  [
    "<p>Fix the statistics collector to operate properly\n              in cases where the system clock goes backwards (Tom\n              Lane)</p>",
    "<p>Previously, statistics collection would stop until\n              the time again reached the latest time previously\n              recorded.</p>"
  ],
  [
    "<p>Emit an informative message to postmaster standard\n              error when we are about to stop logging there (Tom\n              Lane)</p>",
    "<p>This should help reduce user confusion about where\n              to look for log output in common configurations that\n              log to standard error only during postmaster\n              startup.</p>"
  ],
  [
    "<p>When an authentication failure occurs, log the\n              relevant <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/auth-pg-hba-conf.html\" title=\"20.1.&#xA0;The pg_hba.conf File\"><code class=\"filename\">pg_hba.conf</code></a> line, to ease\n              debugging of unintended failures (Magnus\n              Hagander)</p>"
  ],
  [
    "<p>Improve <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/auth-methods.html#AUTH-LDAP\" title=\"20.3.7.&#xA0;LDAP Authentication\"><acronym class=\"acronym\">LDAP</acronym></a> error reporting and\n              documentation (Peter Eisentraut)</p>"
  ],
  [
    "<p>Add support for specifying <acronym class=\"acronym\">LDAP</acronym> authentication parameters in\n              <acronym class=\"acronym\">URL</acronym> format, per\n              RFC 4516 (Peter Eisentraut)</p>"
  ],
  [
    "<p>Change the <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-connection.html#GUC-SSL-CIPHERS\"><code class=\"varname\">\n              ssl_ciphers</code></a> parameter to start with\n              <code class=\"literal\">DEFAULT</code>, rather than\n              <code class=\"literal\">ALL</code>, then remove\n              insecure ciphers (Magnus Hagander)</p>",
    "<p>This should yield a more appropriate SSL cipher\n              set.</p>"
  ],
  [
    "<p>Parse and load <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/auth-username-maps.html\" title=\"20.2.&#xA0;User Name Maps\"><code class=\"filename\">pg_ident.conf</code></a> once, not during\n              each connection (Amit Kapila)</p>",
    "<p>This is similar to how <code class=\"filename\">pg_hba.conf</code> is processed.</p>"
  ],
  [
    "<p>Greatly reduce System V <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/kernel-resources.html#SYSVIPC\" title=\"18.4.1.&#xA0;Shared Memory and Semaphores\">shared\n              memory</a> requirements (Robert Haas)</p>",
    "<p>On Unix-like systems, <code class=\"function\">mmap()</code> is now used for most of\n              <span class=\"productname\">PostgreSQL</span>'s shared\n              memory. For most users, this will eliminate any need\n              to adjust kernel parameters for shared memory.</p>"
  ],
  [
    "<p>Allow the postmaster to listen on multiple\n              Unix-domain sockets (Honza Hor&#xE1;k)</p>",
    "<p>The configuration parameter <code class=\"varname\">unix_socket_directory</code> is replaced by\n              <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-connection.html#GUC-UNIX-SOCKET-DIRECTORIES\">\n              <code class=\"varname\">unix_socket_directories</code></a>, which\n              accepts a list of directories.</p>"
  ],
  [
    "<p>Allow a directory of configuration files to be\n              processed (Magnus Hagander, Greg Smith, Selena\n              Deckelmann)</p>",
    "<p>Such a directory is specified with <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/config-setting.html#CONFIG-INCLUDES\" title=\"19.1.5.&#xA0;Managing Configuration File Contents\"><code class=\"varname\">\n              include_dir</code></a> in the server configuration\n              file.</p>"
  ],
  [
    "<p>Increase the maximum <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/app-initdb.html\" title=\"initdb\">initdb</a>-configured value for <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-resource.html#GUC-SHARED-BUFFERS\"><code class=\"varname\">\n              shared_buffers</code></a> to 128MB (Robert Haas)</p>",
    "<p>This is the maximum value that initdb will attempt\n              to set in <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/config-setting.html#CONFIG-SETTING-CONFIGURATION-FILE\" title=\"19.1.2.&#xA0;Parameter Interaction via the Configuration File\">\n              <code class=\"filename\">postgresql.conf</code></a>;\n              the previous maximum was 32MB.</p>"
  ],
  [
    "<p>Remove the <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-file-locations.html#GUC-EXTERNAL-PID-FILE\">\n              external <acronym class=\"acronym\">PID</acronym>\n              file</a>, if any, on postmaster exit (Peter\n              Eisentraut)</p>"
  ],
  [
    "<p>Allow a streaming replication standby to <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/protocol-replication.html\" title=\"52.4.&#xA0;Streaming Replication Protocol\">follow a\n            timeline switch</a> (Heikki Linnakangas)</p>",
    "<p>This allows streaming standby servers to receive WAL\n            data from a slave newly promoted to master status.\n            Previously, other standbys would require a resync to\n            begin following the new master.</p>"
  ],
  [
    "<p>Add <acronym class=\"acronym\">SQL</acronym> functions\n            <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-admin.html#FUNCTIONS-ADMIN-BACKUP\" title=\"9.26.3.&#xA0;Backup Control Functions\"><code class=\"function\">pg_is_in_backup()</code></a> and <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-admin.html#FUNCTIONS-ADMIN-BACKUP\" title=\"9.26.3.&#xA0;Backup Control Functions\"><code class=\"function\">pg_backup_start_time()</code></a> (Gilles\n            Darold)</p>",
    "<p>These functions report the status of base\n            backups.</p>"
  ],
  [
    "<p>Improve performance of streaming log shipping with\n            <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-wal.html#GUC-SYNCHRONOUS-COMMIT\"><code class=\"varname\">\n            synchronous_commit</code></a> disabled (Andres\n            Freund)</p>"
  ],
  [
    "<p>Allow much faster promotion of a streaming standby\n            to primary (Simon Riggs, Kyotaro Horiguchi)</p>"
  ],
  [
    "<p>Add the last checkpoint's redo location to <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/app-pgcontroldata.html\" title=\"pg_controldata\"><span class=\"application\">pg_controldata</span></a>'s output (Fujii\n            Masao)</p>",
    "<p>This information is useful for determining which\n            <acronym class=\"acronym\">WAL</acronym> files are needed\n            for restore.</p>"
  ],
  [
    "<p>Allow tools like <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/app-pgreceivewal.html\" title=\"pg_receivewal\"><span class=\"application\">pg_receivexlog</span></a> to run on\n            computers with different architectures (Heikki\n            Linnakangas)</p>",
    "<p>WAL files can still only be replayed on servers with\n            the same architecture as the primary; but they can now\n            be transmitted to and stored on machines of any\n            architecture, since the streaming replication protocol\n            is now machine-independent.</p>"
  ],
  [
    "<p>Make <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/app-pgbasebackup.html\" title=\"pg_basebackup\"><span class=\"application\">pg_basebackup</span></a> <code class=\"option\">--write-recovery-conf</code> output a minimal\n            <code class=\"filename\">recovery.conf</code> file\n            (Zolt&#xE1;n B&#xF6;sz&#xF6;rm&#xE9;nyi, Magnus Hagander)</p>",
    "<p>This simplifies setting up a standby server.</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/app-pgreceivewal.html\" title=\"pg_receivewal\"><span class=\"application\">pg_receivexlog</span></a> and <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/app-pgbasebackup.html\" title=\"pg_basebackup\"><span class=\"application\">pg_basebackup</span></a> <code class=\"option\">--xlog-method</code> to handle streaming\n            timeline switches (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-replication.html#GUC-WAL-RECEIVER-TIMEOUT\">\n            <code class=\"varname\">wal_receiver_timeout</code></a>\n            parameter to control the <acronym class=\"acronym\">WAL</acronym> receiver's timeout (Amit\n            Kapila)</p>",
    "<p>This allows more rapid detection of connection\n            failure.</p>"
  ],
  [
    "<p>Change the <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/wal.html\" title=\"Chapter&#xA0;30.&#xA0;Reliability and the Write-Ahead Log\">\n            <acronym class=\"acronym\">WAL</acronym></a> record\n            format to allow splitting the record header across\n            pages (Heikki Linnakangas)</p>",
    "<p>The new format is slightly more compact, and is more\n            efficient to write.</p>"
  ],
  [
    "<p>Implement <acronym class=\"acronym\">SQL</acronym>-standard <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/queries-table-expressions.html#QUERIES-LATERAL\" title=\"7.2.1.5.&#xA0;LATERAL Subqueries\"><code class=\"literal\">LATERAL</code></a> option for <code class=\"literal\">FROM</code>-clause subqueries and function\n            calls (Tom Lane)</p>",
    "<p>This feature allows subqueries and functions in\n            <code class=\"literal\">FROM</code> to reference columns\n            from other tables in the <code class=\"literal\">FROM</code> clause. The <code class=\"literal\">LATERAL</code> keyword is optional for\n            functions.</p>"
  ],
  [
    "<p>Add support for piping <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-copy.html\" title=\"COPY\"><code class=\"command\">COPY</code></a> and <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/app-psql.html\" title=\"psql\"><span class=\"application\">psql</span></a> <code class=\"command\">\\copy</code> data to/from an external program\n            (Etsuro Fujita)</p>"
  ],
  [
    "<p>Allow a multirow <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-values.html\" title=\"VALUES\"><code class=\"literal\">VALUES</code></a> clause in a rule to\n            reference <code class=\"literal\">OLD</code>/<code class=\"literal\">NEW</code> (Tom Lane)</p>"
  ],
  [
    "<p>Add support for <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/event-triggers.html\" title=\"Chapter&#xA0;39.&#xA0;Event Triggers\">event\n            triggers</a> (Dimitri Fontaine, Robert Haas, &#xC1;lvaro\n            Herrera)</p>",
    "<p>This allows server-side functions written in\n            event-enabled languages to be called when DDL commands\n            are run.</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-createforeigndatawrapper.html\" title=\"CREATE FOREIGN DATA WRAPPER\">foreign data wrappers</a>\n            to support writes (inserts/updates/deletes) on foreign\n            tables (KaiGai Kohei)</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-createschema.html\" title=\"CREATE SCHEMA\"><code class=\"command\">CREATE\n            SCHEMA ... IF NOT EXISTS</code></a> clause (Fabr&#xED;zio de\n            Royes Mello)</p>"
  ],
  [
    "<p>Make <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-reassign-owned.html\" title=\"REASSIGN OWNED\"><code class=\"command\">REASSIGN\n            OWNED</code></a> also change ownership of shared\n            objects (&#xC1;lvaro Herrera)</p>"
  ],
  [
    "<p>Make <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-createaggregate.html\" title=\"CREATE AGGREGATE\"><code class=\"command\">CREATE\n            AGGREGATE</code></a> complain if the given initial\n            value string is not valid input for the transition\n            datatype (Tom Lane)</p>"
  ],
  [
    "<p>Suppress <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-createtable.html\" title=\"CREATE TABLE\"><code class=\"command\">CREATE\n            TABLE</code></a>'s messages about implicit index and\n            sequence creation (Robert Haas)</p>",
    "<p>These messages now appear at <code class=\"literal\">DEBUG1</code> verbosity, so that they will\n            not be shown by default.</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-droptable.html\" title=\"DROP TABLE\"><code class=\"command\">DROP TABLE IF\n            EXISTS</code></a> to succeed when a non-existent schema\n            is specified in the table name (Bruce Momjian)</p>",
    "<p>Previously, it threw an error if the schema did not\n            exist.</p>"
  ],
  [
    "<p>Provide clients with <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/libpq-exec.html#LIBPQ-PQRESULTERRORFIELD\">constraint\n            violation details</a> as separate fields (Pavel\n            Stehule)</p>",
    "<p>This allows clients to retrieve table, column, data\n            type, or constraint name error details. Previously such\n            information had to be extracted from error strings.\n            Client library support is required to access these\n            fields.</p>"
  ],
  [
    "<p>Support <code class=\"literal\">IF NOT EXISTS</code>\n              option in <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-altertype.html\" title=\"ALTER TYPE\"><code class=\"command\">ALTER TYPE\n              ... ADD VALUE</code></a> (Andrew Dunstan)</p>",
    "<p>This is useful for conditionally adding values to\n              enumerated types.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-alterrole.html\" title=\"ALTER ROLE\"><code class=\"command\">ALTER ROLE\n              ALL SET</code></a> to establish settings for all\n              users (Peter Eisentraut)</p>",
    "<p>This allows settings to apply to all users in all\n              databases. <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-alterdatabase.html\" title=\"ALTER DATABASE\"><code class=\"command\">ALTER DATABASE\n              SET</code></a> already allowed addition of settings\n              for all users in a single database. <code class=\"filename\">postgresql.conf</code> has a similar\n              effect.</p>"
  ],
  [
    "<p>Add support for <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-alterrule.html\" title=\"ALTER RULE\"><code class=\"command\">ALTER RULE ... RENAME</code></a> (Ali\n              Dar)</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-creatematerializedview.html\" title=\"CREATE MATERIALIZED VIEW\">materialized views</a>\n              (Kevin Grittner)</p>",
    "<p>Unlike ordinary views, where the base tables are\n              read on every access, materialized views create\n              physical tables at creation or refresh time. Access\n              to the materialized view then reads from its physical\n              table. There is not yet any facility for\n              incrementally refreshing materialized views or\n              auto-accessing them via base table access.</p>"
  ],
  [
    "<p>Make simple views <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-createview.html#SQL-CREATEVIEW-UPDATABLE-VIEWS\" title=\"Updatable Views\">auto-updatable</a> (Dean\n              Rasheed)</p>",
    "<p>Simple views that reference some or all columns\n              from a single base table are now updatable by\n              default. More complex views can be made updatable\n              using <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-createtrigger.html\" title=\"CREATE TRIGGER\"><code class=\"literal\">INSTEAD\n              OF</code></a> triggers or <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-createrule.html\" title=\"CREATE RULE\"><code class=\"literal\">INSTEAD</code></a>\n              rules.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-createview.html\" title=\"CREATE VIEW\"><code class=\"command\">CREATE\n              RECURSIVE VIEW</code></a> syntax (Peter\n              Eisentraut)</p>",
    "<p>Internally this is translated into <code class=\"command\">CREATE VIEW ... WITH RECURSIVE\n              ...</code>.</p>"
  ],
  [
    "<p>Improve view/rule printing code to handle cases\n              where referenced tables are renamed, or columns are\n              renamed, added, or dropped (Tom Lane)</p>",
    "<p>Table and column renamings can produce cases\n              where, if we merely substitute the new name into the\n              original text of a rule or view, the result is\n              ambiguous. This change fixes the rule-dumping code to\n              insert manufactured table and column aliases when\n              needed to preserve the original semantics.</p>"
  ],
  [
    "<p>Increase the maximum size of <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/largeobjects.html\" title=\"Chapter&#xA0;34.&#xA0;Large Objects\">large objects</a>\n            from 2GB to 4TB (Nozomi Anzai, Yugo Nagata)</p>",
    "<p>This change includes adding 64-bit-capable large\n            object access functions, both in the server and in\n            libpq.</p>"
  ],
  [
    "<p>Allow text <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/datatype-datetime.html#DATATYPE-TIMEZONES\" title=\"8.5.3.&#xA0;Time Zones\">timezone designations</a>,\n            e.g. <span class=\"quote\">&#x201C;<span class=\"quote\">America/Chicago</span>&#x201D;</span>, in the\n            <span class=\"quote\">&#x201C;<span class=\"quote\">T</span>&#x201D;</span> field of <acronym class=\"acronym\">ISO</acronym>-format <code class=\"type\">timestamptz</code> input (Bruce Momjian)</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-json.html\" title=\"9.15.&#xA0;JSON Functions and Operators\">operators\n              and functions</a> to extract elements from\n              <code class=\"type\">JSON</code> values (Andrew\n              Dunstan)</p>"
  ],
  [
    "<p>Allow <code class=\"type\">JSON</code> values to be\n              <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-json.html\" title=\"9.15.&#xA0;JSON Functions and Operators\">converted\n              into records</a> (Andrew Dunstan)</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-json.html\" title=\"9.15.&#xA0;JSON Functions and Operators\">functions</a>\n              to convert scalars, records, and <code class=\"type\">hstore</code> values to <code class=\"type\">JSON</code> (Andrew Dunstan)</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-array.html#ARRAY-FUNCTIONS-TABLE\" title=\"Table&#xA0;9.49.&#xA0;Array Functions\"><code class=\"function\">array_remove()</code></a> and <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-array.html#ARRAY-FUNCTIONS-TABLE\" title=\"Table&#xA0;9.49.&#xA0;Array Functions\"><code class=\"function\">array_replace()</code></a> functions (Marco\n            Nenciarini, Gabriele Bartolini)</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-string.html#FUNCTIONS-STRING-OTHER\" title=\"Table&#xA0;9.9.&#xA0;Other String Functions\"><code class=\"function\">\n            concat()</code></a> and <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-string.html#FUNCTIONS-STRING-FORMAT\" title=\"9.4.1.&#xA0;format\"><code class=\"function\">format()</code></a> to properly expand\n            <code class=\"literal\">VARIADIC</code>-labeled arguments\n            (Pavel Stehule)</p>"
  ],
  [
    "<p>Improve <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-string.html#FUNCTIONS-STRING-FORMAT\" title=\"9.4.1.&#xA0;format\"><code class=\"function\">format()</code></a> to provide field width\n            and left/right alignment options (Pavel Stehule)</p>"
  ],
  [
    "<p>Make <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-formatting.html#FUNCTIONS-FORMATTING-TABLE\" title=\"Table&#xA0;9.23.&#xA0;Formatting Functions\"><code class=\"function\">\n            to_char()</code></a>, <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-formatting.html#FUNCTIONS-FORMATTING-TABLE\" title=\"Table&#xA0;9.23.&#xA0;Formatting Functions\"><code class=\"function\">\n            to_date()</code></a>, and <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-formatting.html#FUNCTIONS-FORMATTING-TABLE\" title=\"Table&#xA0;9.23.&#xA0;Formatting Functions\"><code class=\"function\">\n            to_timestamp()</code></a> handle negative (BC) century\n            values properly (Bruce Momjian)</p>",
    "<p>Previously the behavior was either wrong or\n            inconsistent with positive/<acronym class=\"acronym\">AD</acronym> handling, e.g. with the format\n            mask <span class=\"quote\">&#x201C;<span class=\"quote\">IYYY-IW-DY</span>&#x201D;</span>.</p>"
  ],
  [
    "<p>Make <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-formatting.html#FUNCTIONS-FORMATTING-TABLE\" title=\"Table&#xA0;9.23.&#xA0;Formatting Functions\"><code class=\"function\">\n            to_date()</code></a> and <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-formatting.html#FUNCTIONS-FORMATTING-TABLE\" title=\"Table&#xA0;9.23.&#xA0;Formatting Functions\"><code class=\"function\">\n            to_timestamp()</code></a> return proper results when\n            mixing <acronym class=\"acronym\">ISO</acronym> and\n            Gregorian week/day designations (Bruce Momjian)</p>"
  ],
  [
    "<p>Cause <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-info.html#FUNCTIONS-INFO-CATALOG-TABLE\" title=\"Table&#xA0;9.63.&#xA0;System Catalog Information Functions\">\n            <code class=\"function\">pg_get_viewdef()</code></a> to\n            start a new line by default after each <code class=\"literal\">SELECT</code> target list entry and\n            <code class=\"literal\">FROM</code> entry (Marko\n            Tiikkaja)</p>",
    "<p>This reduces line length in view printing, for\n            instance in <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/app-pgdump.html\" title=\"pg_dump\"><span class=\"application\">pg_dump</span></a> output.</p>"
  ],
  [
    "<p>Fix <code class=\"function\">map_sql_value_to_xml_value()</code> to print\n            values of domain types the same way their base type\n            would be printed (Pavel Stehule)</p>",
    "<p>There are special formatting rules for certain\n            built-in types such as <code class=\"type\">boolean</code>; these rules now also apply to\n            domains over these types.</p>"
  ],
  [
    "<p>Allow PL/pgSQL to use <code class=\"literal\">RETURN</code> with a composite-type\n              expression (Asif Rehman)</p>",
    "<p>Previously, in a function returning a composite\n              type, <code class=\"literal\">RETURN</code> could only\n              reference a variable of that type.</p>"
  ],
  [
    "<p>Allow PL/pgSQL to access <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/plpgsql-control-structures.html#PLPGSQL-EXCEPTION-DIAGNOSTICS\" title=\"42.6.6.1.&#xA0;Obtaining Information About an Error\">\n              constraint violation details</a> as separate fields\n              (Pavel Stehule)</p>"
  ],
  [
    "<p>Allow PL/pgSQL to access the number of rows\n              processed by <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-copy.html\" title=\"COPY\"><code class=\"command\">COPY</code></a>\n              (Pavel Stehule)</p>",
    "<p>A <code class=\"command\">COPY</code> executed in a\n              PL/pgSQL function now updates the value retrieved by\n              <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/plpgsql-statements.html#PLPGSQL-STATEMENTS-DIAGNOSTICS\" title=\"42.5.5.&#xA0;Obtaining the Result Status\"><code class=\"command\">\n              GET DIAGNOSTICS x = ROW_COUNT</code></a>.</p>"
  ],
  [
    "<p>Allow unreserved keywords to be used as\n              identifiers everywhere in PL/pgSQL (Tom Lane)</p>",
    "<p>In certain places in the PL/pgSQL grammar,\n              keywords had to be quoted to be used as identifiers,\n              even if they were nominally unreserved.</p>"
  ],
  [
    "<p>Add PL/Python result object string handler (Peter\n              Eisentraut)</p>",
    "<p>This allows <code class=\"literal\">plpy.debug(rv)</code> to output something\n              reasonable.</p>"
  ],
  [
    "<p>Make PL/Python convert OID values to a proper\n              Python numeric type (Peter Eisentraut)</p>"
  ],
  [
    "<p>Handle <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/spi.html\" title=\"Chapter&#xA0;46.&#xA0;Server Programming Interface\"><acronym class=\"acronym\">\n              SPI</acronym></a> errors raised explicitly (with\n              PL/Python's <code class=\"literal\">RAISE</code>) the\n              same as internal <acronym class=\"acronym\">SPI</acronym> errors (Oskari Saarenmaa and\n              Jan Urbanski)</p>"
  ],
  [
    "<p>Prevent leakage of <acronym class=\"acronym\">SPI</acronym> tuple tables during\n            subtransaction abort (Tom Lane)</p>",
    "<p>At the end of any failed subtransaction, the core\n            SPI code now releases any SPI tuple tables that were\n            created during that subtransaction. This avoids the\n            need for SPI-using code to keep track of such tuple\n            tables and release them manually in error-recovery\n            code. Failure to do so caused a number of\n            transaction-lifespan memory leakage issues in PL/pgSQL\n            and perhaps other SPI clients. <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/spi-spi-freetupletable.html\" title=\"SPI_freetuptable\"><code class=\"function\">SPI_freetuptable()</code></a> now protects\n            itself against multiple freeing requests, so any\n            existing code that did take care to clean up shouldn't\n            be broken by this change.</p>"
  ],
  [
    "<p>Allow <acronym class=\"acronym\">SPI</acronym>\n            functions to access the number of rows processed by\n            <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-copy.html\" title=\"COPY\"><code class=\"command\">COPY</code></a> (Pavel\n            Stehule)</p>"
  ],
  [
    "<p>Add command-line utility <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/app-pg-isready.html\" title=\"pg_isready\"><span class=\"application\">pg_isready</span></a> to check if the\n            server is ready to accept connections (Phil Sorber)</p>"
  ],
  [
    "<p>Support multiple <code class=\"option\">--table</code>\n            arguments for <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/app-pgrestore.html\" title=\"pg_restore\"><span class=\"application\">pg_restore</span></a>, <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/app-clusterdb.html\" title=\"clusterdb\"><span class=\"application\">clusterdb</span></a>,\n            <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/app-reindexdb.html\" title=\"reindexdb\"><span class=\"application\">reindexdb</span></a>, and <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/app-vacuumdb.html\" title=\"vacuumdb\"><span class=\"application\">vacuumdb</span></a> (Josh\n            Kupershmidt)</p>",
    "<p>This is similar to the way <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/app-pgdump.html\" title=\"pg_dump\"><span class=\"application\">pg_dump</span></a>'s <code class=\"option\">--table</code> option works.</p>"
  ],
  [
    "<p>Add <code class=\"option\">--dbname</code> option to\n            <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/app-pg-dumpall.html\" title=\"pg_dumpall\"><span class=\"application\">pg_dumpall</span></a>, <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/app-pgbasebackup.html\" title=\"pg_basebackup\"><span class=\"application\">pg_basebackup</span></a>, and <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/app-pgreceivewal.html\" title=\"pg_receivewal\"><span class=\"application\">pg_receivexlog</span></a> to allow\n            specifying a connection string (Amit Kapila)</p>"
  ],
  [
    "<p>Add libpq function <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/libpq-connect.html#LIBPQ-PQCONNINFO\"><code class=\"function\">PQconninfo()</code></a> to return connection\n            information (Zolt&#xE1;n B&#xF6;sz&#xF6;rm&#xE9;nyi, Magnus Hagander)</p>"
  ],
  [
    "<p>Adjust function cost settings so <span class=\"application\">psql</span> tab completion and pattern\n              searching are more efficient (Tom Lane)</p>"
  ],
  [
    "<p>Improve <span class=\"application\">psql</span>'s\n              tab completion coverage (Jeff Janes, Dean Rasheed,\n              Peter Eisentraut, Magnus Hagander)</p>"
  ],
  [
    "<p>Allow the <span class=\"application\">psql</span>\n              <code class=\"option\">--single-transaction</code> mode\n              to work when reading from standard input (Fabien\n              Coelho, Robert Haas)</p>",
    "<p>Previously this option only worked when reading\n              from a file.</p>"
  ],
  [
    "<p>Remove <span class=\"application\">psql</span>\n              warning when connecting to an older server (Peter\n              Eisentraut)</p>",
    "<p>A warning is still issued when connecting to a\n              server of a newer major version than <span class=\"application\">psql</span>'s.</p>"
  ],
  [
    "<p>Add <span class=\"application\">psql</span>\n                command <code class=\"command\">\\watch</code> to\n                repeatedly execute a SQL command (Will\n                Leinweber)</p>"
  ],
  [
    "<p>Add <span class=\"application\">psql</span>\n                command <code class=\"command\">\\gset</code> to store\n                query results in <span class=\"application\">psql</span> variables (Pavel\n                Stehule)</p>"
  ],
  [
    "<p>Add <acronym class=\"acronym\">SSL</acronym>\n                information to <span class=\"application\">psql</span>'s <code class=\"command\">\\conninfo</code> command (Alastair\n                Turner)</p>"
  ],
  [
    "<p>Add <span class=\"quote\">&#x201C;<span class=\"quote\">Security</span>&#x201D;</span> column to\n                <span class=\"application\">psql</span>'s\n                <code class=\"command\">\\df+</code> output (Jon\n                Erdman)</p>"
  ],
  [
    "<p>Allow <span class=\"application\">psql</span>\n                command <code class=\"command\">\\l</code> to accept a\n                database name pattern (Peter Eisentraut)</p>"
  ],
  [
    "<p>In <span class=\"application\">psql</span>, do not\n                allow <code class=\"command\">\\connect</code> to use\n                defaults if there is no active connection (Bruce\n                Momjian)</p>",
    "<p>This might be the case if the server had\n                crashed.</p>"
  ],
  [
    "<p>Properly reset state after failure of a SQL\n                command executed with <span class=\"application\">psql</span>'s <code class=\"literal\">\\g</code> <em class=\"replaceable\"><code>file</code></em> (Tom Lane)</p>",
    "<p>Previously, the output from subsequent SQL\n                commands would unexpectedly continue to go to the\n                same file.</p>"
  ],
  [
    "<p>Add a <code class=\"literal\">latex-longtable</code> output format to\n                <span class=\"application\">psql</span> (Bruce\n                Momjian)</p>",
    "<p>This format allows tables to span multiple\n                pages.</p>"
  ],
  [
    "<p>Add a <code class=\"literal\">border=3</code>\n                output mode to the <span class=\"application\">psql</span> <code class=\"literal\">latex</code> format (Bruce Momjian)</p>"
  ],
  [
    "<p>In <span class=\"application\">psql</span>'s\n                tuples-only and expanded output modes, no longer\n                emit <span class=\"quote\">&#x201C;<span class=\"quote\">(No\n                rows)</span>&#x201D;</span> for zero rows (Peter\n                Eisentraut)</p>"
  ],
  [
    "<p>In <span class=\"application\">psql</span>'s\n                unaligned, expanded output mode, no longer print an\n                empty line for zero rows (Peter Eisentraut)</p>"
  ],
  [
    "<p>Add <span class=\"application\">pg_dump</span>\n              <code class=\"option\">--jobs</code> option to dump\n              tables in parallel (Joachim Wieland)</p>"
  ],
  [
    "<p>Make <span class=\"application\">pg_dump</span>\n              output functions in a more predictable order (Joel\n              Jacobson)</p>"
  ],
  [
    "<p>Fix tar files emitted by <span class=\"application\">pg_dump</span> to be <acronym class=\"acronym\">POSIX</acronym> conformant (Brian Weaver,\n              Tom Lane)</p>"
  ],
  [
    "<p>Add <code class=\"option\">--dbname</code> option to\n              <span class=\"application\">pg_dump</span>, for\n              consistency with other client commands (Heikki\n              Linnakangas)</p>",
    "<p>The database name could already be supplied last\n              without a flag.</p>"
  ],
  [
    "<p>Make initdb fsync the newly created data directory\n              (Jeff Davis)</p>",
    "<p>This insures data integrity in event of a system\n              crash shortly after initdb. This can be disabled by\n              using <code class=\"option\">--nosync</code>.</p>"
  ],
  [
    "<p>Add initdb <code class=\"option\">--sync-only</code>\n              option to sync the data directory to durable storage\n              (Bruce Momjian)</p>",
    "<p>This is used by <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/pgupgrade.html\" title=\"pg_upgrade\"><span class=\"application\">pg_upgrade</span></a>.</p>"
  ],
  [
    "<p>Make initdb issue a warning about placing the data\n              directory at the top of a file system mount point\n              (Bruce Momjian)</p>"
  ],
  [
    "<p>Add infrastructure to allow plug-in <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/bgworker.html\" title=\"Chapter&#xA0;47.&#xA0;Background Worker Processes\">background\n            worker processes</a> (&#xC1;lvaro Herrera)</p>"
  ],
  [
    "<p>Create a centralized timeout <acronym class=\"acronym\">API</acronym> (Zolt&#xE1;n B&#xF6;sz&#xF6;rm&#xE9;nyi)</p>"
  ],
  [
    "<p>Create libpgcommon and move <code class=\"function\">pg_malloc()</code> and other functions there\n            (&#xC1;lvaro Herrera, Andres Freund)</p>",
    "<p>This allows libpgport to be used solely for\n            portability-related code.</p>"
  ],
  [
    "<p>Add support for list links embedded in larger\n            structs (Andres Freund)</p>"
  ],
  [
    "<p>Use <code class=\"literal\">SA_RESTART</code> for all\n            signals, including <code class=\"literal\">SIGALRM</code>\n            (Tom Lane)</p>"
  ],
  [
    "<p>Ensure that the correct text domain is used when\n            translating <code class=\"function\">errcontext()</code>\n            messages (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Standardize naming of client-side memory allocation\n            functions (Tom Lane)</p>"
  ],
  [
    "<p>Provide support for <span class=\"quote\">&#x201C;<span class=\"quote\">static\n            assertions</span>&#x201D;</span> that will fail at compile\n            time if some compile-time-constant condition is not met\n            (Andres Freund, Tom Lane)</p>"
  ],
  [
    "<p>Support <code class=\"function\">Assert()</code> in\n            client-side code (Andrew Dunstan)</p>"
  ],
  [
    "<p>Add decoration to inform the C compiler that some\n            <code class=\"function\">ereport()</code> and\n            <code class=\"function\">elog()</code> calls do not\n            return (Peter Eisentraut, Andres Freund, Tom Lane,\n            Heikki Linnakangas)</p>"
  ],
  [
    "<p>Allow options to be passed to the regression test\n            output comparison utility via <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/regress-evaluation.html\" title=\"32.2.&#xA0;Test Evaluation\"><code class=\"envar\">PG_REGRESS_DIFF_OPTS</code></a> (Peter\n            Eisentraut)</p>"
  ],
  [
    "<p>Add isolation tests for <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-createindex.html\" title=\"CREATE INDEX\"><code class=\"command\">CREATE INDEX\n            CONCURRENTLY</code></a> (Abhijit Menon-Sen)</p>"
  ],
  [
    "<p>Remove typedefs for <code class=\"type\">int2</code>/<code class=\"type\">int4</code> as\n            they are better represented as <code class=\"type\">int16</code>/<code class=\"type\">int32</code>\n            (Peter Eisentraut)</p>"
  ],
  [
    "<p>Fix <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/install-procedure.html#INSTALL\" title=\"Installing the Files\">install-strip</a> on Mac\n            <span class=\"productname\">OS X</span> (Peter\n            Eisentraut)</p>"
  ],
  [
    "<p>Remove <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/install-procedure.html#CONFIGURE\" title=\"Configuration\">configure</a> flag <code class=\"option\">--disable-shared</code>, as it is no longer\n            supported (Bruce Momjian)</p>"
  ],
  [
    "<p>Rewrite pgindent in <span class=\"application\">Perl</span> (Andrew Dunstan)</p>"
  ],
  [
    "<p>Provide Emacs macro to set Perl formatting to match\n            <span class=\"productname\">PostgreSQL</span>'s perltidy\n            settings (Peter Eisentraut)</p>"
  ],
  [
    "<p>Run tool to check the keyword list whenever the\n            backend grammar is changed (Tom Lane)</p>"
  ],
  [
    "<p>Change the way <code class=\"literal\">UESCAPE</code>\n            is lexed, to significantly reduce the size of the lexer\n            tables (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Centralize <span class=\"application\">flex</span> and\n            <span class=\"application\">bison</span> <span class=\"application\">make</span> rules (Peter Eisentraut)</p>",
    "<p>This is useful for <span class=\"application\">pgxs</span> authors.</p>"
  ],
  [
    "<p>Change many internal backend functions to return\n            object <code class=\"type\">OID</code>s rather than void\n            (Dimitri Fontaine)</p>",
    "<p>This is useful for event triggers.</p>"
  ],
  [
    "<p>Invent pre-commit/pre-prepare/pre-subcommit events\n            for transaction callbacks (Tom Lane)</p>",
    "<p>Loadable modules that use transaction callbacks\n            might need modification to handle these new event\n            types.</p>"
  ],
  [
    "<p>Add function <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-info.html#FUNCTIONS-INFO-CATALOG-TABLE\" title=\"Table&#xA0;9.63.&#xA0;System Catalog Information Functions\">\n            <code class=\"function\">pg_identify_object()</code></a>\n            to produce a machine-readable description of a database\n            object (&#xC1;lvaro Herrera)</p>"
  ],
  [
    "<p>Add post-<code class=\"command\">ALTER</code>-object\n            server hooks (KaiGai Kohei)</p>"
  ],
  [
    "<p>Implement a generic binary heap and use it for\n            Merge-Append operations (Abhijit Menon-Sen)</p>"
  ],
  [
    "<p>Provide a tool to help detect timezone abbreviation\n            changes when updating the <code class=\"filename\">src/timezone/data</code> files (Tom\n            Lane)</p>"
  ],
  [
    "<p>Add <span class=\"application\">pkg-config</span>\n            support for <span class=\"application\">libpq</span> and\n            <span class=\"application\">ecpg</span> libraries (Peter\n            Eisentraut)</p>"
  ],
  [
    "<p>Remove <code class=\"filename\">src/tools/backend</code>, now that the\n            content is on the <span class=\"productname\">PostgreSQL</span> wiki (Bruce\n            Momjian)</p>"
  ],
  [
    "<p>Split out <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/wal.html\" title=\"Chapter&#xA0;30.&#xA0;Reliability and the Write-Ahead Log\">\n            <acronym class=\"acronym\">WAL</acronym></a> reading as\n            an independent facility (Heikki Linnakangas, Andres\n            Freund)</p>"
  ],
  [
    "<p>Use a 64-bit integer to represent <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/wal.html\" title=\"Chapter&#xA0;30.&#xA0;Reliability and the Write-Ahead Log\">\n            <acronym class=\"acronym\">WAL</acronym></a> positions\n            (<code class=\"structname\">XLogRecPtr</code>) instead of\n            two 32-bit integers (Heikki Linnakangas)</p>",
    "<p>Generally, tools that need to read the\n            <acronym class=\"acronym\">WAL</acronym> format will need\n            to be adjusted.</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/plpython.html\" title=\"Chapter&#xA0;45.&#xA0;PL/Python - Python Procedural Language\">\n            PL/Python</a> to support platform-specific include\n            directories (Peter Eisentraut)</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/plpython.html\" title=\"Chapter&#xA0;45.&#xA0;PL/Python - Python Procedural Language\">\n            PL/Python</a> on <span class=\"productname\">OS X</span>\n            to build against custom versions of <span class=\"application\">Python</span> (Peter Eisentraut)</p>"
  ],
  [
    "<p>Add a <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/postgres-fdw.html\" title=\"F.34.&#xA0;postgres_fdw\"><span class=\"productname\">Postgres</span> foreign data wrapper</a>\n            contrib module to allow access to other <span class=\"productname\">Postgres</span> servers (Shigeru\n            Hanada)</p>",
    "<p>This foreign data wrapper supports writes.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/pgwaldump.html\" title=\"pg_waldump\"><span class=\"application\">pg_xlogdump</span></a> contrib program\n            (Andres Freund)</p>"
  ],
  [
    "<p>Add support for indexing of regular-expression\n            searches in <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/pgtrgm.html\" title=\"F.32.&#xA0;pg_trgm\"><span class=\"productname\">pg_trgm</span></a> (Alexander\n            Korotkov)</p>"
  ],
  [
    "<p>Improve <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/pgtrgm.html\" title=\"F.32.&#xA0;pg_trgm\"><span class=\"productname\">pg_trgm</span></a>'s handling of\n            multibyte characters (Tom Lane)</p>",
    "<p>On a platform that does not have the wcstombs() or\n            towlower() library functions, this could result in an\n            incompatible change in the contents of <span class=\"productname\">pg_trgm</span> indexes for non-ASCII\n            data. In such cases, <code class=\"command\">REINDEX</code> those indexes to ensure\n            correct search results.</p>"
  ],
  [
    "<p>Add a <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/pgstattuple.html\" title=\"F.31.&#xA0;pgstattuple\">pgstattuple</a> function to\n            report the size of the pending-insertions list of a\n            <acronym class=\"acronym\">GIN</acronym> index (Fujii\n            Masao)</p>"
  ],
  [
    "<p>Make <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/oid2name.html\" title=\"oid2name\"><span class=\"application\">oid2name</span></a>, <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/pgbench.html\" title=\"pgbench\"><span class=\"application\">pgbench</span></a>, and <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/vacuumlo.html\" title=\"vacuumlo\"><span class=\"application\">vacuumlo</span></a> set <code class=\"varname\">fallback_application_name</code> (Amit\n            Kapila)</p>"
  ],
  [
    "<p>Improve output of <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/pgtesttiming.html\" title=\"pg_test_timing\"><span class=\"application\">pg_test_timing</span></a> (Bruce\n            Momjian)</p>"
  ],
  [
    "<p>Improve output of <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/pgtestfsync.html\" title=\"pg_test_fsync\"><span class=\"application\">pg_test_fsync</span></a> (Peter\n            Geoghegan)</p>"
  ],
  [
    "<p>Create a dedicated foreign data wrapper, with its\n            own option validator function, for <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/dblink.html\" title=\"F.11.&#xA0;dblink\">dblink</a>\n            (Shigeru Hanada)</p>",
    "<p>When using this FDW to define the target of a\n            <span class=\"application\">dblink</span> connection,\n            instead of using a hard-wired list of connection\n            options, the underlying <span class=\"application\">libpq</span> library is consulted to see\n            what connection options it supports.</p>"
  ],
  [
    "<p>Allow <span class=\"application\">pg_upgrade</span>\n              to do dumps and restores in parallel (Bruce Momjian,\n              Andrew Dunstan)</p>",
    "<p>This allows parallel schema dump/restore of\n              databases, as well as parallel copy/link of data\n              files per tablespace. Use the <code class=\"option\">--jobs</code> option to specify the level of\n              parallelism.</p>"
  ],
  [
    "<p>Make <span class=\"application\">pg_upgrade</span>\n              create Unix-domain sockets in the current directory\n              (Bruce Momjian, Tom Lane)</p>",
    "<p>This reduces the possibility that someone will\n              accidentally connect during the upgrade.</p>"
  ],
  [
    "<p>Make <span class=\"application\">pg_upgrade</span>\n              <code class=\"option\">--check</code> mode properly\n              detect the location of non-default socket directories\n              (Bruce Momjian, Tom Lane)</p>"
  ],
  [
    "<p>Improve performance of <span class=\"application\">pg_upgrade</span> for databases with\n              many tables (Bruce Momjian)</p>"
  ],
  [
    "<p>Improve <span class=\"application\">pg_upgrade</span>'s logs by showing\n              executed commands (&#xC1;lvaro Herrera)</p>"
  ],
  [
    "<p>Improve <span class=\"application\">pg_upgrade</span>'s status display\n              during copy/link (Bruce Momjian)</p>"
  ],
  [
    "<p>Add <code class=\"option\">--foreign-keys</code>\n              option to <span class=\"application\">pgbench</span>\n              (Jeff Janes)</p>",
    "<p>This adds foreign key constraints to the standard\n              tables created by <span class=\"application\">pgbench</span>, for use in foreign key\n              performance testing.</p>"
  ],
  [
    "<p>Allow <span class=\"application\">pgbench</span> to\n              aggregate performance statistics and produce output\n              every <code class=\"option\">--aggregate-interval</code> seconds (Tomas\n              Vondra)</p>"
  ],
  [
    "<p>Add <span class=\"application\">pgbench</span>\n              <code class=\"option\">--sampling-rate</code> option to\n              control the percentage of transactions logged (Tomas\n              Vondra)</p>"
  ],
  [
    "<p>Reduce and improve the status message output of\n              <span class=\"application\">pgbench</span>'s\n              initialization mode (Robert Haas, Peter\n              Eisentraut)</p>"
  ],
  [
    "<p>Add <span class=\"application\">pgbench</span>\n              <code class=\"option\">-q</code> mode to print one\n              output line every five seconds (Tomas Vondra)</p>"
  ],
  [
    "<p>Output <span class=\"application\">pgbench</span>\n              elapsed and estimated remaining time during\n              initialization (Tomas Vondra)</p>"
  ],
  [
    "<p>Allow <span class=\"application\">pgbench</span> to\n              use much larger scale factors, by changing relevant\n              columns from <code class=\"type\">integer</code> to\n              <code class=\"type\">bigint</code> when the requested\n              scale factor exceeds 20000 (Greg Smith)</p>"
  ],
  [
    "<p>Allow <span class=\"productname\">EPUB</span>-format\n            documentation to be created (Peter Eisentraut)</p>"
  ],
  [
    "<p>Update <span class=\"productname\">FreeBSD</span>\n            kernel configuration documentation (Brad Davis)</p>"
  ],
  [
    "<p>Improve <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/tutorial-window.html\" title=\"3.5.&#xA0;Window Functions\"><code class=\"literal\">WINDOW</code> function</a> documentation\n            (Bruce Momjian, Florian Pflug)</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/docguide-toolsets.html\" title=\"J.2.&#xA0;Tool Sets\">instructions</a> for\n            setting up the documentation tool chain on <span class=\"productname\">macOS</span> (Peter Eisentraut)</p>"
  ],
  [
    "<p>Improve <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-wal.html#GUC-COMMIT-DELAY\"><code class=\"varname\">commit_delay</code></a> documentation (Peter\n            Geoghegan)</p>"
  ]
]