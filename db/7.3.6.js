[
  [
    "<p>Revert erroneous changes in rule permissions\n          checking</p>",
    "<p>A patch applied in 7.3.3 to fix a corner case in rule\n          permissions checks turns out to have disabled\n          rule-related permissions checks in many not-so-corner\n          cases. This would for example allow users to insert into\n          views they weren't supposed to have permission to insert\n          into. We have therefore reverted the 7.3.3 patch. The\n          original bug will be fixed in 8.0.</p>"
  ],
  [
    "<p>Repair incorrect order of operations in\n          GetNewTransactionId()</p>",
    "<p>This bug could result in failure under\n          out-of-disk-space conditions, including inability to\n          restart even after disk space is freed.</p>"
  ],
  [
    "<p>Ensure configure selects -fno-strict-aliasing even\n          when an external value for CFLAGS is supplied</p>",
    "<p>On some platforms, building with -fstrict-aliasing\n          causes bugs.</p>"
  ],
  [
    "<p>Make pg_restore handle 64-bit off_t correctly</p>",
    "<p>This bug prevented proper restoration from archive\n          files exceeding 4 GB.</p>"
  ],
  [
    "<p>Make contrib/dblink not assume that local and remote\n          type OIDs match (Joe)</p>"
  ],
  [
    "<p>Quote connectby()'s start_with argument properly\n          (Joe)</p>"
  ],
  [
    "<p>Don't crash when a rowtype argument to a plpgsql\n          function is NULL</p>"
  ],
  [
    "<p>Avoid generating invalid character encoding sequences\n          in corner cases when planning LIKE operations</p>"
  ],
  [
    "<p>Ensure text_position() cannot scan past end of source\n          string in multibyte cases (Korea PostgreSQL Users'\n          Group)</p>"
  ],
  [
    "<p>Fix index optimization and selectivity estimates for\n          LIKE operations on bytea columns (Joe)</p>"
  ]
]