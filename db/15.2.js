[
  [
    "<p><span class=\"application\">libpq</span> can leak memory contents after GSSAPI transport encryption initiation fails (Jacob Champion)</p>",
    "<p>A modified server, or an unauthenticated man-in-the-middle, can send a not-zero-terminated error message during setup of GSSAPI (Kerberos) transport encryption. <span class=\"application\">libpq</span> will then copy that string, as well as following bytes in application memory up to the next zero byte, to its error report. Depending on what the calling application does with the error report, this could result in disclosure of application memory contents. There is also a small probability of a crash due to reading beyond the end of memory. Fix by properly zero-terminating the server message. (CVE-2022-41862)</p>"
  ],
  [
    "<p>Fix calculation of which <code class=\"literal\">GENERATED</code> columns need to be updated in child tables during an <code class=\"command\">UPDATE</code> on a partitioned table or inheritance tree (Amit Langote, Tom Lane)</p>",
    "<p>This fixes failure to update <code class=\"literal\">GENERATED</code> columns that do not exist in the parent table, or that have different dependencies than are in the parent column's generation expression.</p>"
  ],
  [
    "<p>Fix possible failure of <code class=\"command\">MERGE</code> to compute <code class=\"literal\">GENERATED</code> columns (Dean Rasheed)</p>",
    "<p>When the first row-level action of the <code class=\"command\">MERGE</code> was an <code class=\"literal\">UPDATE</code>, any subsequent <code class=\"literal\">INSERT</code> actions would fail to compute <code class=\"literal\">GENERATED</code> columns that were deemed unnecessary to compute for the <code class=\"literal\">UPDATE</code> action (due to not depending on any of the <code class=\"literal\">UPDATE</code> target columns).</p>"
  ],
  [
    "<p>Fix <code class=\"command\">MERGE</code>'s check for unreachable <code class=\"literal\">WHEN</code> clauses (Dean Rasheed)</p>",
    "<p>A <code class=\"literal\">WHEN</code> clause following an unconditional <code class=\"literal\">WHEN</code> clause should be rejected as unreachable, but this case was not always detected.</p>"
  ],
  [
    "<p>Fix <code class=\"command\">MERGE</code>'s rule-detection test (Dean Rasheed)</p>",
    "<p><code class=\"command\">MERGE</code> is not supported on tables with rules; but it also failed on tables that once had rules but no longer do.</p>"
  ],
  [
    "<p>In <code class=\"command\">MERGE</code>, don't count a <code class=\"literal\">DO NOTHING</code> action as a processed tuple (Álvaro Herrera)</p>",
    "<p>This makes the code's behavior match the documentation.</p>"
  ],
  [
    "<p>Allow a <code class=\"literal\">WITH RECURSIVE ... CYCLE</code> CTE to access its output column (Tom Lane)</p>",
    "<p>A reference to the <code class=\"literal\">SET</code> column from within the CTE would fail with <span class=\"quote\">“<span class=\"quote\">cache lookup failed for type 0</span>”</span>.</p>"
  ],
  [
    "<p>Fix handling of pending inserts when doing a bulk insertion to a foreign table (Etsuro Fujita)</p>",
    "<p>In some cases pending insertions were not flushed to the FDW soon enough, leading to logical inconsistencies, for example <code class=\"literal\">BEFORE ROW</code> triggers not seeing rows they should be able to see.</p>"
  ],
  [
    "<p>Allow <code class=\"literal\">REPLICA IDENTITY</code> to be set on an index that's not (yet) valid (Tom Lane)</p>",
    "<p>When <span class=\"application\">pg_dump</span> dumps a partitioned index that's marked <code class=\"literal\">REPLICA IDENTITY</code>, it generates a command sequence that applies <code class=\"literal\">REPLICA IDENTITY</code> before the partitioned index has been marked valid, causing restore to fail. There seems no very good reason to prohibit doing it in that order, so allow it. The marking will have no effect anyway until the index becomes valid.</p>"
  ],
  [
    "<p>Fix handling of <code class=\"literal\">DEFAULT</code> markers in rules that perform an <code class=\"command\">INSERT</code> from a multi-row <code class=\"literal\">VALUES</code> list (Dean Rasheed)</p>",
    "<p>In some cases a <code class=\"literal\">DEFAULT</code> marker would not get replaced with the proper default-value expression, leading to an <span class=\"quote\">“<span class=\"quote\">unrecognized node type</span>”</span> error.</p>"
  ],
  [
    "<p>Reject uses of undefined variables in <code class=\"type\">jsonpath</code> existence checks (Alexander Korotkov, David G. Johnston)</p>",
    "<p>While <code class=\"type\">jsonpath</code> match operators threw an error for an undefined variable in the path pattern, the existence operators silently treated it as a match.</p>"
  ],
  [
    "<p>Fix <code class=\"type\">jsonb</code> subscripting to cope with toasted subscript values (Tom Lane, David G. Johnston)</p>",
    "<p>Using a text value fetched directly from a table as a <code class=\"type\">jsonb</code> subscript was likely to fail. Fetches would usually not find any matching element. Assignments could store the value with a garbage key, although keys long enough to cause that problem are probably rare in the field.</p>"
  ],
  [
    "<p>Fix edge-case data corruption in parallel hash joins (Dmitry Astapov)</p>",
    "<p>If the final chunk of a large tuple being written out to a temporary file was exactly 32760 bytes, it would be corrupted due to a fencepost bug. The query would typically fail later with corrupted-data symptoms.</p>"
  ],
  [
    "<p>Honor non-default settings of <code class=\"varname\">checkpoint_completion_target</code> (Bharath Rupireddy)</p>",
    "<p>Internal state was not updated after a change in <code class=\"varname\">checkpoint_completion_target</code>, possibly resulting in performing checkpoint I/O faster or slower than desired, especially if that setting was changed on-the-fly.</p>"
  ],
  [
    "<p>Log the correct ending timestamp in <code class=\"varname\">recovery_target_xid</code> mode (Tom Lane)</p>",
    "<p>When ending recovery based on the <code class=\"varname\">recovery_target_xid</code> setting with <code class=\"varname\">recovery_target_inclusive</code> = <code class=\"literal\">off</code>, we printed an incorrect timestamp (always 2000-01-01) in the <span class=\"quote\">“<span class=\"quote\">recovery stopping before ... transaction</span>”</span> log message.</p>"
  ],
  [
    "<p>Improve error reporting for some buffered file read failures (Peter Eisentraut)</p>",
    "<p>Correctly report a short read, giving the numbers of bytes desired and actually read, instead of reporting an irrelevant error code. Most places got this right already, but some recently-written replication logic did not.</p>"
  ],
  [
    "<p>Remove arbitrary limit on number of elements in <code class=\"type\">int2vector</code> and <code class=\"type\">oidvector</code> (Tom Lane)</p>",
    "<p>The input functions for these types previously rejected more than 100 elements. With the introduction of the logical replication column list feature, it's necessary to accept <code class=\"type\">int2vector</code>s having up to 1600 columns, otherwise long column lists cause logical-replication failures.</p>"
  ],
  [
    "<p>In extended query protocol, avoid an immediate commit after <code class=\"command\">ANALYZE</code> if we're running a pipeline (Tom Lane)</p>",
    "<p>If there's not been an explicit <code class=\"command\">BEGIN TRANSACTION</code>, <code class=\"command\">ANALYZE</code> would take it on itself to commit, which should not happen within a pipelined series of commands.</p>"
  ],
  [
    "<p>Reject cancel request packets having the wrong length (Andrey Borodin)</p>",
    "<p>The server would process a cancel request even if its length word was too small. This led to reading beyond the end of the allocated buffer. In theory that could cause a segfault, but it seems quite unlikely to happen in practice, since the buffer would have to be very close to the end of memory. The more likely outcome was a bogus log message about wrong backend PID or cancel code. Complain about the wrong length, instead.</p>"
  ],
  [
    "<p>Fix planner preprocessing oversights for window function run-condition expressions (Richard Guo, David Rowley)</p>",
    "<p>This could lead to planner errors such as <span class=\"quote\">“<span class=\"quote\">WindowFunc not found in subplan target lists</span>”</span>.</p>"
  ],
  [
    "<p>Fix possible dangling-pointer access during execution of window function run-condition expressions (David Rowley)</p>",
    "<p>In practice, because the run-condition optimization is only applied to certain window functions that happen to all return <code class=\"type\">int8</code>, this only manifested as a problem on 32-bit builds.</p>"
  ],
  [
    "<p>Add recursion and looping defenses in subquery pullup (Tom Lane)</p>",
    "<p>A contrived query can result in deep recursion and unreasonable amounts of time spent trying to flatten subqueries. A proper fix for that seems unduly invasive for a back-patch, but we can at least add stack depth checks and an interrupt check to allow the query to be cancelled.</p>"
  ],
  [
    "<p>Fix planner issues when combining Memoize nodes with partitionwise joins or parameterized nestloops (Richard Guo)</p>",
    "<p>These errors could lead to not using Memoize in contexts where it would be useful, or possibly to wrong query plans.</p>"
  ],
  [
    "<p>Fix partitionwise-join code to tolerate failure to produce a plan for each partition (Tom Lane)</p>",
    "<p>This could result in <span class=\"quote\">“<span class=\"quote\">could not devise a query plan for the given query</span>”</span> errors.</p>"
  ],
  [
    "<p>Limit the amount of cleanup work done by <code class=\"function\">get_actual_variable_range</code> (Simon Riggs)</p>",
    "<p>Planner runs occurring just after deletion of a large number of tuples appearing at the end of an index could expend significant amounts of work setting the <span class=\"quote\">“<span class=\"quote\">killed</span>”</span> bits for those index entries. Limit the amount of work done in any one query by giving up on this process after examining 100 heap pages. All the cleanup will still happen eventually, but without so large a performance hiccup.</p>"
  ],
  [
    "<p>Prevent the statistics machinery from getting confused when a relation's relkind changes (Andres Freund)</p>",
    "<p>Converting a table to a view could lead to crashes or assertion failures.</p>"
  ],
  [
    "<p>Fix under-parenthesized display of <code class=\"literal\">AT TIME ZONE</code> constructs (Tom Lane)</p>",
    "<p>This could result in dump/restore failures for rules or views in which an argument of <code class=\"literal\">AT TIME ZONE</code> is itself an expression.</p>"
  ],
  [
    "<p>Prevent clobbering of cached parsetrees for utility statements in SQL functions (Tom Lane, Daniel Gustafsson)</p>",
    "<p>If a SQL-language function executes the same utility command more than once within a single calling query, it could crash or report strange errors such as <span class=\"quote\">“<span class=\"quote\">unrecognized node type</span>”</span>.</p>"
  ],
  [
    "<p>Ensure that execution of full-text-search queries can be cancelled while they are performing phrase matches (Tom Lane)</p>"
  ],
  [
    "<p>Fix memory leak in hashing strings with nondeterministic collations (Jeff Davis)</p>"
  ],
  [
    "<p>Fix deadlock between <code class=\"command\">DROP DATABASE</code> and logical replication worker process (Hou Zhijie)</p>",
    "<p>This was caused by an ill-advised choice to block interrupts while creating a logical replication slot in the worker. In version 15 that could lead to an undetected deadlock. In version 14, no deadlock has been observed, but it's still a bad idea to block interrupts while waiting for network I/O.</p>"
  ],
  [
    "<p>Clean up the <span class=\"application\">libpq</span> connection object after a failed replication connection attempt (Andres Freund)</p>",
    "<p>The previous coding leaked the connection object. In background code paths that's pretty harmless because the calling process will give up and exit. But in commands such as <code class=\"command\">CREATE SUBSCRIPTION</code>, such a failure resulted in a small session-lifespan memory leak.</p>"
  ],
  [
    "<p>In hot-standby servers, reduce processing effort for tracking XIDs known to be active on the primary (Simon Riggs, Michail Nikolaev)</p>",
    "<p>Insufficiently-aggressive cleanup of the KnownAssignedXids array could lead to poor performance, particularly when <code class=\"varname\">max_connections</code> is set to a large value on the standby.</p>"
  ],
  [
    "<p>Ignore invalidated logical-replication slots while determining oldest catalog xmin (Sirisha Chamarthi)</p>",
    "<p>A replication slot could prevent cleanup of dead tuples in the system catalogs even after it becomes invalidated due to exceeding <code class=\"varname\">max_slot_wal_keep_size</code>. Thus, failure of a replication consumer could lead to indefinitely-large catalog bloat.</p>"
  ],
  [
    "<p>In logical decoding, notify the remote node when a transaction is detected to have crashed (Hou Zhijie)</p>",
    "<p>After a server restart, we'll re-stream the changes for transactions occurring shortly before the restart. Some of these transactions probably never completed; when we realize that one didn't we throw away the relevant decoding state locally, but we neglected to tell the subscriber about it. That led to the subscriber keeping useless streaming files until it's next restarted.</p>"
  ],
  [
    "<p>Fix uninitialized-memory usage in logical decoding (Masahiko Sawada)</p>",
    "<p>In certain cases, resumption of logical decoding could try to re-use XID data that had already been freed, leading to unpredictable behavior.</p>"
  ],
  [
    "<p>Acquire spinlock while updating shared state during logical decoding context creation (Masahiko Sawada)</p>",
    "<p>We neglected to acquire the appropriate lock while updating data about two-phase transactions, potentially allowing other processes to see inconsistent data.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pgoutput</span> replication plug-in to not send columns not listed in a table's replication column list (Hou Zhijie)</p>",
    "<p><code class=\"literal\">UPDATE</code> and <code class=\"literal\">DELETE</code> events did not pay attention to the configured column list, thus sending more data than expected. This did not cause a problem when the receiver is our built-in logical replication code, but it might confuse other receivers, and in any case it wasted network bandwidth.</p>"
  ],
  [
    "<p>Avoid rare <span class=\"quote\">“<span class=\"quote\">failed to acquire cleanup lock</span>”</span> panic during WAL replay of hash-index page split operations (Robert Haas)</p>"
  ],
  [
    "<p>Advance a heap page's LSN when setting its all-visible bit during WAL replay (Jeff Davis)</p>",
    "<p>Failure to do this left the page possibly different on standby servers than the primary, and violated some other expectations about when the LSN changes. This seems only a theoretical hazard so far as <span class=\"productname\">PostgreSQL</span> itself is concerned, but it could upset third-party tools.</p>"
  ],
  [
    "<p>Fix <code class=\"function\">int64_div_fast_to_numeric()</code> to work for a wider range of inputs (Dean Rasheed)</p>",
    "<p>This function misbehaved with some values of its second argument. No such usages exist in core <span class=\"productname\">PostgreSQL</span>, but it's clearly a hazard for external modules, so repair.</p>"
  ],
  [
    "<p>Fix latent buffer-overrun problem in <code class=\"literal\">WaitEventSet</code> logic (Thomas Munro)</p>",
    "<p>The <code class=\"function\">epoll</code>-based and <code class=\"function\">kqueue</code>-based implementations could ask the kernel for too many events if the size of their internal buffer was different from the size of the caller's output buffer. That case is not known to occur in released <span class=\"productname\">PostgreSQL</span> versions, but this error is a hazard for external modules and future bug fixes.</p>"
  ],
  [
    "<p>Avoid nominally-undefined behavior when accessing shared memory in 32-bit builds (Andres Freund)</p>",
    "<p>clang's undefined-behavior sanitizer complained about use of a pointer that was less aligned than it should be. It's very unlikely that this would cause a problem in non-debug builds, but it's worth fixing for testing purposes.</p>"
  ],
  [
    "<p>Fix assertion failure in BRIN minmax-multi opclasses (Tomas Vondra)</p>",
    "<p>The assertion was overly strict, so this mistake was harmless in non-assert builds.</p>"
  ],
  [
    "<p>Remove faulty assertion in useless-RESULT-RTE optimization logic (Tom Lane)</p>"
  ],
  [
    "<p>Fix copy-and-paste errors in cache-lookup-failure messages for ACL checks (Justin Pryzby)</p>",
    "<p>In principle these errors should never be reached. But if they are, some of them reported the wrong type of object.</p>"
  ],
  [
    "<p>Fix possible corruption of very large tablespace map files in <span class=\"application\">pg_basebackup</span> (Antonin Houska)</p>"
  ],
  [
    "<p>Avoid harmless warning from <span class=\"application\">pg_dump</span> in <code class=\"option\">--if-exists</code> mode (Tom Lane)</p>",
    "<p>If the <code class=\"literal\">public</code> schema has a non-default owner then use of <span class=\"application\">pg_dump</span>'s <code class=\"option\">--if-exists</code> option resulted in a warning message <span class=\"quote\">“<span class=\"quote\">warning: could not find where to insert IF EXISTS in statement \"-- *not* dropping schema, since initdb creates it\"</span>”</span>. The dump output was okay, though.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">psql</span>'s <code class=\"literal\">\\sf</code> and <code class=\"literal\">\\ef</code> commands to handle SQL-language functions that have <acronym class=\"acronym\">SQL</acronym>-standard function bodies (Tom Lane)</p>",
    "<p>These commands misidentified the start of the function body when it used new-style syntax.</p>"
  ],
  [
    "<p>Fix tab completion of <code class=\"command\">ALTER FUNCTION/PROCEDURE/ROUTINE</code> ... <code class=\"command\">SET SCHEMA</code> (Dean Rasheed)</p>"
  ],
  [
    "<p>Update <code class=\"filename\">contrib/pageinspect</code> to mark its disk-accessing functions as <code class=\"literal\">PARALLEL RESTRICTED</code> (Tom Lane)</p>",
    "<p>This avoids possible failure if one of these functions is used to examine a temporary table, since a session's temporary tables are not accessible from parallel workers.</p>"
  ],
  [
    "<p>Fix <code class=\"filename\">contrib/seg</code> to not crash or print garbage if an input number has more than 127 digits (Tom Lane)</p>"
  ],
  [
    "<p>Fix build on Microsoft Visual Studio 2013 (Tom Lane)</p>",
    "<p>A previous patch supposed that all platforms of interest have <code class=\"function\">snprintf()</code>, but MSVC 2013 isn't quite there yet. Revert to using <code class=\"function\">sprintf()</code> on that platform.</p>"
  ],
  [
    "<p>Fix compile failure in building PL/Perl with MSVC when using Strawberry Perl (Andrew Dunstan)</p>"
  ],
  [
    "<p>Fix mismatch of PL/Perl built with MSVC versus a Perl library built with gcc (Andrew Dunstan)</p>",
    "<p>Such combinations could previously fail with <span class=\"quote\">“<span class=\"quote\">loadable library and perl binaries are mismatched</span>”</span> errors.</p>"
  ],
  [
    "<p>Suppress compiler warnings from Perl's header files (Andres Freund)</p>",
    "<p>Our preferred compiler options provoke warnings about constructs appearing in recent versions of Perl's header files. When using <span class=\"application\">gcc</span>, we can suppress these warnings with a pragma.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_waldump</span> to build on compilers that don't discard unused static-inline functions (Tom Lane)</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2022g for DST law changes in Greenland and Mexico, plus historical corrections for northern Canada, Colombia, and Singapore.</p>",
    "<p>Notably, a new timezone America/Ciudad_Juarez has been split off from America/Ojinaga.</p>"
  ]
]