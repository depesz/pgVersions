[
  [
    "<p>Make <code class=\"function\">pg_options_to_table</code>\n          return NULL for an option with no value (Tom Lane)</p>",
    "<p>Previously such cases would result in a server\n          crash.</p>"
  ],
  [
    "<p>Fix memory leak at end of a GiST index scan (Tom\n          Lane)</p>",
    "<p>Commands that perform many separate GiST index scans,\n          such as verification of a new GiST-based exclusion\n          constraint on a table already containing many rows, could\n          transiently require large amounts of memory due to this\n          leak.</p>"
  ],
  [
    "<p>Fix explicit reference to <code class=\"literal\">pg_temp</code> schema in <code class=\"command\">CREATE TEMPORARY TABLE</code> (Robert Haas)</p>",
    "<p>This used to be allowed, but failed in 9.1.0.</p>"
  ]
]