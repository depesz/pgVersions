[
  [
    "<p>Force zero_damaged_pages to be on during recovery from\n          WAL</p>"
  ],
  [
    "<p>Prevent some obscure cases of <span class=\"quote\">&#x201C;<span class=\"quote\">variable not in subplan\n          target lists</span>&#x201D;</span></p>"
  ],
  [
    "<p>Force stats processes to detach from shared memory,\n          ensuring cleaner shutdown</p>"
  ],
  [
    "<p>Make PQescapeBytea and byteaout consistent with each\n          other (Joe)</p>"
  ],
  [
    "<p>Added missing SPI_finish() calls to dblink's\n          get_tuple_of_interest() (Joe)</p>"
  ],
  [
    "<p>Fix for possible foreign key violation when rule\n          rewrites INSERT (Jan)</p>"
  ],
  [
    "<p>Support qualified type names in PL/Tcl's spi_prepare\n          command (Jan)</p>"
  ],
  [
    "<p>Make pg_dump handle a procedural language handler\n          located in pg_catalog</p>"
  ],
  [
    "<p>Make pg_dump handle cases where a custom opclass is in\n          another schema</p>"
  ],
  [
    "<p>Make pg_dump dump binary-compatible casts correctly\n          (Jan)</p>"
  ],
  [
    "<p>Fix insertion of expressions containing subqueries\n          into rule bodies</p>"
  ],
  [
    "<p>Fix incorrect argument processing in clusterdb script\n          (Anand Ranganathan)</p>"
  ],
  [
    "<p>Fix problems with dropped columns in plpython\n          triggers</p>"
  ],
  [
    "<p>Repair problems with to_char() reading past end of its\n          input string (Karel)</p>"
  ],
  [
    "<p>Fix GB18030 mapping errors (Tatsuo)</p>"
  ],
  [
    "<p>Fix several problems with SSL error handling and\n          asynchronous SSL I/O</p>"
  ],
  [
    "<p>Remove ability to bind a list of values to a single\n          parameter in JDBC (prevents possible SQL-injection\n          attacks)</p>"
  ],
  [
    "<p>Fix some errors in HAVE_INT64_TIMESTAMP code paths</p>"
  ],
  [
    "<p>Fix corner case for btree search in parallel with\n          first root page split</p>"
  ]
]