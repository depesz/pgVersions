[
  [
    "<p>Prevent access to external files/URLs via XML entity\n          references (Noah Misch, Tom Lane)</p>",
    "<p><code class=\"function\">xml_parse()</code> would\n          attempt to fetch external files or URLs as needed to\n          resolve DTD and entity references in an XML value, thus\n          allowing unprivileged database users to attempt to fetch\n          data with the privileges of the database server. While\n          the external data wouldn't get returned directly to the\n          user, portions of it could be exposed in error messages\n          if the data didn't parse as valid XML; and in any case\n          the mere ability to check existence of a file might be\n          useful to an attacker. (CVE-2012-3489)</p>"
  ],
  [
    "<p>Prevent access to external files/URLs via <code class=\"filename\">contrib/xml2</code>'s <code class=\"function\">xslt_process()</code> (Peter Eisentraut)</p>",
    "<p><span class=\"application\">libxslt</span> offers the\n          ability to read and write both files and URLs through\n          stylesheet commands, thus allowing unprivileged database\n          users to both read and write data with the privileges of\n          the database server. Disable that through proper use of\n          <span class=\"application\">libxslt</span>'s security\n          options. (CVE-2012-3488)</p>",
    "<p>Also, remove <code class=\"function\">xslt_process()</code>'s ability to fetch\n          documents and stylesheets from external files/URLs. While\n          this was a documented <span class=\"quote\">&#x201C;<span class=\"quote\">feature</span>&#x201D;</span>, it was long regarded as a\n          bad idea. The fix for CVE-2012-3489 broke that\n          capability, and rather than expend effort on trying to\n          fix it, we're just going to summarily remove it.</p>"
  ],
  [
    "<p>Prevent too-early recycling of btree index pages (Noah\n          Misch)</p>",
    "<p>When we allowed read-only transactions to skip\n          assigning XIDs, we introduced the possibility that a\n          deleted btree page could be recycled while a read-only\n          transaction was still in flight to it. This would result\n          in incorrect index search results. The probability of\n          such an error occurring in the field seems very low\n          because of the timing requirements, but nonetheless it\n          should be fixed.</p>"
  ],
  [
    "<p>Fix crash-safety bug with newly-created-or-reset\n          sequences (Tom Lane)</p>",
    "<p>If <code class=\"command\">ALTER SEQUENCE</code> was\n          executed on a freshly created or reset sequence, and then\n          precisely one <code class=\"function\">nextval()</code>\n          call was made on it, and then the server crashed, WAL\n          replay would restore the sequence to a state in which it\n          appeared that no <code class=\"function\">nextval()</code>\n          had been done, thus allowing the first sequence value to\n          be returned again by the next <code class=\"function\">nextval()</code> call. In particular this\n          could manifest for <code class=\"type\">serial</code>\n          columns, since creation of a serial column's sequence\n          includes an <code class=\"command\">ALTER SEQUENCE OWNED\n          BY</code> step.</p>"
  ],
  [
    "<p>Fix race condition in <code class=\"literal\">enum</code>-type value comparisons (Robert\n          Haas, Tom Lane)</p>",
    "<p>Comparisons could fail when encountering an enum value\n          added since the current query started.</p>"
  ],
  [
    "<p>Fix <code class=\"function\">txid_current()</code> to\n          report the correct epoch when not in hot standby (Heikki\n          Linnakangas)</p>",
    "<p>This fixes a regression introduced in the previous\n          minor release.</p>"
  ],
  [
    "<p>Prevent selection of unsuitable replication\n          connections as the synchronous standby (Fujii Masao)</p>",
    "<p>The master might improperly choose pseudo-servers such\n          as <span class=\"application\">pg_receivexlog</span> or\n          <span class=\"application\">pg_basebackup</span> as the\n          synchronous standby, and then wait indefinitely for\n          them.</p>"
  ],
  [
    "<p>Fix bug in startup of Hot Standby when a master\n          transaction has many subtransactions (Andres Freund)</p>",
    "<p>This mistake led to failures reported as <span class=\"quote\">&#x201C;<span class=\"quote\">out-of-order XID insertion\n          in KnownAssignedXids</span>&#x201D;</span>.</p>"
  ],
  [
    "<p>Ensure the <code class=\"filename\">backup_label</code>\n          file is fsync'd after <code class=\"function\">pg_start_backup()</code> (Dave Kerr)</p>"
  ],
  [
    "<p>Fix timeout handling in walsender processes (Tom\n          Lane)</p>",
    "<p>WAL sender background processes neglected to establish\n          a <span class=\"systemitem\">SIGALRM</span> handler,\n          meaning they would wait forever in some corner cases\n          where a timeout ought to happen.</p>"
  ],
  [
    "<p>Wake walsenders after each background flush by\n          walwriter (Andres Freund, Simon Riggs)</p>",
    "<p>This greatly reduces replication delay when the\n          workload contains only asynchronously-committed\n          transactions.</p>"
  ],
  [
    "<p>Fix <code class=\"literal\">LISTEN</code>/<code class=\"literal\">NOTIFY</code> to cope better with I/O problems,\n          such as out of disk space (Tom Lane)</p>",
    "<p>After a write failure, all subsequent attempts to send\n          more <code class=\"literal\">NOTIFY</code> messages would\n          fail with messages like <span class=\"quote\">&#x201C;<span class=\"quote\">Could not read from file \"pg_notify/<em class=\"replaceable\"><code>nnnn</code></em>\" at offset\n          <em class=\"replaceable\"><code>nnnnn</code></em>:\n          Success</span>&#x201D;</span>.</p>"
  ],
  [
    "<p>Only allow autovacuum to be auto-canceled by a\n          directly blocked process (Tom Lane)</p>",
    "<p>The original coding could allow inconsistent behavior\n          in some cases; in particular, an autovacuum could get\n          canceled after less than <code class=\"literal\">deadlock_timeout</code> grace period.</p>"
  ],
  [
    "<p>Improve logging of autovacuum cancels (Robert\n          Haas)</p>"
  ],
  [
    "<p>Fix log collector so that <code class=\"literal\">log_truncate_on_rotation</code> works during\n          the very first log rotation after server start (Tom\n          Lane)</p>"
  ],
  [
    "<p>Fix <code class=\"literal\">WITH</code> attached to a\n          nested set operation (<code class=\"literal\">UNION</code>/<code class=\"literal\">INTERSECT</code>/<code class=\"literal\">EXCEPT</code>) (Tom Lane)</p>"
  ],
  [
    "<p>Ensure that a whole-row reference to a subquery\n          doesn't include any extra <code class=\"literal\">GROUP\n          BY</code> or <code class=\"literal\">ORDER BY</code>\n          columns (Tom Lane)</p>"
  ],
  [
    "<p>Fix dependencies generated during <code class=\"literal\">ALTER TABLE ... ADD CONSTRAINT USING\n          INDEX</code> (Tom Lane)</p>",
    "<p>This command left behind a redundant <code class=\"structname\">pg_depend</code> entry for the index, which\n          could confuse later operations, notably <code class=\"literal\">ALTER TABLE ... ALTER COLUMN TYPE</code> on one\n          of the indexed columns.</p>"
  ],
  [
    "<p>Fix <code class=\"command\">REASSIGN OWNED</code> to\n          work on extensions (Alvaro Herrera)</p>"
  ],
  [
    "<p>Disallow copying whole-row references in <code class=\"literal\">CHECK</code> constraints and index definitions\n          during <code class=\"command\">CREATE TABLE</code> (Tom\n          Lane)</p>",
    "<p>This situation can arise in <code class=\"command\">CREATE TABLE</code> with <code class=\"literal\">LIKE</code> or <code class=\"literal\">INHERITS</code>. The copied whole-row variable\n          was incorrectly labeled with the row type of the original\n          table not the new one. Rejecting the case seems\n          reasonable for <code class=\"literal\">LIKE</code>, since\n          the row types might well diverge later. For <code class=\"literal\">INHERITS</code> we should ideally allow it,\n          with an implicit coercion to the parent table's row type;\n          but that will require more work than seems safe to\n          back-patch.</p>"
  ],
  [
    "<p>Fix memory leak in <code class=\"literal\">ARRAY(SELECT\n          ...)</code> subqueries (Heikki Linnakangas, Tom Lane)</p>"
  ],
  [
    "<p>Fix planner to pass correct collation to operator\n          selectivity estimators (Tom Lane)</p>",
    "<p>This was not previously required by any core\n          selectivity estimation function, but third-party code\n          might need it.</p>"
  ],
  [
    "<p>Fix extraction of common prefixes from regular\n          expressions (Tom Lane)</p>",
    "<p>The code could get confused by quantified\n          parenthesized subexpressions, such as <code class=\"literal\">^(foo)?bar</code>. This would lead to incorrect\n          index optimization of searches for such patterns.</p>"
  ],
  [
    "<p>Fix bugs with parsing signed <em class=\"replaceable\"><code>hh</code></em><code class=\"literal\">:</code><em class=\"replaceable\"><code>mm</code></em> and <em class=\"replaceable\"><code>hh</code></em><code class=\"literal\">:</code><em class=\"replaceable\"><code>mm</code></em><code class=\"literal\">:</code><em class=\"replaceable\"><code>ss</code></em> fields in <code class=\"type\">interval</code> constants (Amit Kapila, Tom\n          Lane)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_dump</span> to better\n          handle views containing partial <code class=\"literal\">GROUP BY</code> lists (Tom Lane)</p>",
    "<p>A view that lists only a primary key column in\n          <code class=\"literal\">GROUP BY</code>, but uses other\n          table columns as if they were grouped, gets marked as\n          depending on the primary key. Improper handling of such\n          primary key dependencies in <span class=\"application\">pg_dump</span> resulted in poorly-ordered\n          dumps, which at best would be inefficient to restore and\n          at worst could result in outright failure of a parallel\n          <span class=\"application\">pg_restore</span> run.</p>"
  ],
  [
    "<p>In PL/Perl, avoid setting UTF8 flag when in SQL_ASCII\n          encoding (Alex Hunsaker, Kyotaro Horiguchi, Alvaro\n          Herrera)</p>"
  ],
  [
    "<p>Use Postgres' encoding conversion functions, not\n          Python's, when converting a Python Unicode string to the\n          server encoding in PL/Python (Jan Urbanski)</p>",
    "<p>This avoids some corner-case problems, notably that\n          Python doesn't support all the encodings Postgres does. A\n          notable functional change is that if the server encoding\n          is SQL_ASCII, you will get the UTF-8 representation of\n          the string; formerly, any non-ASCII characters in the\n          string would result in an error.</p>"
  ],
  [
    "<p>Fix mapping of PostgreSQL encodings to Python\n          encodings in PL/Python (Jan Urbanski)</p>"
  ],
  [
    "<p>Report errors properly in <code class=\"filename\">contrib/xml2</code>'s <code class=\"function\">xslt_process()</code> (Tom Lane)</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2012e for DST law\n          changes in Morocco and Tokelau</p>"
  ]
]