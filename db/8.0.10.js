[
  [
    "<p>Improve handling of <code class=\"function\">getaddrinfo()</code> on AIX (Tom)</p>",
    "<p>This fixes a problem with starting the statistics\n          collector, among other things.</p>"
  ],
  [
    "<p>Fix <span class=\"quote\">&#x201C;<span class=\"quote\">failed to\n          re-find parent key</span>&#x201D;</span> errors in <code class=\"command\">VACUUM</code> (Tom)</p>"
  ],
  [
    "<p>Fix race condition for truncation of a large relation\n          across a gigabyte boundary by <code class=\"command\">VACUUM</code> (Tom)</p>"
  ],
  [
    "<p>Fix bugs affecting multi-gigabyte hash indexes\n          (Tom)</p>"
  ],
  [
    "<p>Fix possible deadlock in Windows signal handling\n          (Teodor)</p>"
  ],
  [
    "<p>Fix error when constructing an <code class=\"literal\">ARRAY[]</code> made up of multiple empty\n          elements (Tom)</p>"
  ],
  [
    "<p>Fix ecpg memory leak during connection (Michael)</p>"
  ],
  [
    "<p><code class=\"function\">to_number()</code> and\n          <code class=\"function\">to_char(numeric)</code> are now\n          <code class=\"literal\">STABLE</code>, not <code class=\"literal\">IMMUTABLE</code>, for new <span class=\"application\">initdb</span> installs (Tom)</p>",
    "<p>This is because <code class=\"varname\">lc_numeric</code> can potentially change the\n          output of these functions.</p>"
  ],
  [
    "<p>Improve index usage of regular expressions that use\n          parentheses (Tom)</p>",
    "<p>This improves <span class=\"application\">psql</span>\n          <code class=\"literal\">\\d</code> performance also.</p>"
  ],
  [
    "<p>Update timezone database</p>",
    "<p>This affects Australian and Canadian daylight-savings\n          rules in particular.</p>"
  ]
]