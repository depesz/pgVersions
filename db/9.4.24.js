[
  [
    "<p>Require schema qualification to cast to a temporary type when using functional cast syntax (Noah Misch)</p>",
    "<p>We have long required invocations of temporary functions to explicitly specify the temporary schema, that is <tt class=\"LITERAL\">pg_temp.<tt class=\"REPLACEABLE c2\">func_name</tt>(<tt class=\"REPLACEABLE c2\">args</tt>)</tt>. Require this as well for casting to temporary types using functional notation, for example <tt class=\"LITERAL\">pg_temp.<tt class=\"REPLACEABLE c2\">type_name</tt>(<tt class=\"REPLACEABLE c2\">arg</tt>)</tt>. Otherwise it's possible to capture a function call using a temporary object, allowing privilege escalation in much the same ways that we blocked in CVE-2007-2138. (CVE-2019-10208)</p>"
  ],
  [
    "<p>Fix failure of <tt class=\"COMMAND\">ALTER TABLE ... ALTER COLUMN TYPE</tt> when altering multiple columns' types in one command (Tom Lane)</p>",
    "<p>This fixes a regression introduced in the most recent minor releases: indexes using the altered columns were not processed correctly, leading to strange failures during <tt class=\"COMMAND\">ALTER TABLE</tt>.</p>"
  ],
  [
    "<p>Fix mishandling of multi-column foreign keys when rebuilding a foreign key constraint (Tom Lane)</p>",
    "<p><tt class=\"COMMAND\">ALTER TABLE</tt> could make an incorrect decision about whether revalidation of a foreign key is necessary, if not all columns of the key are of the same type. It seems likely that the error would always have been in the conservative direction, that is revalidating unnecessarily.</p>"
  ],
  [
    "<p>Prevent incorrect canonicalization of date ranges with <tt class=\"LITERAL\">infinity</tt> endpoints (Laurenz Albe)</p>",
    "<p>It's incorrect to try to convert an open range to a closed one or vice versa by incrementing or decrementing the endpoint value, if the endpoint is infinite; so leave the range alone in such cases.</p>"
  ],
  [
    "<p>Fix loss of fractional digits when converting very large <tt class=\"TYPE\">money</tt> values to <tt class=\"TYPE\">numeric</tt> (Tom Lane)</p>"
  ],
  [
    "<p>Fix spinlock assembly code for MIPS CPUs so that it works on MIPS r6 (YunQiang Su)</p>"
  ],
  [
    "<p>Make <span class=\"APPLICATION\">libpq</span> ignore carriage return (<tt class=\"LITERAL\">\\r</tt>) in connection service files (Tom Lane, Michael Paquier)</p>",
    "<p>In some corner cases, service files containing Windows-style newlines could be mis-parsed, resulting in connection failures.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_dump</span> to ensure that custom operator classes are dumped in the right order (Tom Lane)</p>",
    "<p>If a user-defined opclass is the subtype opclass of a user-defined range type, related objects were dumped in the wrong order, producing an unrestorable dump. (The underlying failure to handle opclass dependencies might manifest in other cases too, but this is the only known case.)</p>"
  ],
  [
    "<p>Fix <tt class=\"FILENAME\">contrib/passwordcheck</tt> to coexist with other users of <tt class=\"VARNAME\">check_password_hook</tt> (Michael Paquier)</p>"
  ],
  [
    "<p>Fix <tt class=\"FILENAME\">contrib/sepgsql</tt> tests to work under recent SELinux releases (Mike Palmiotto)</p>"
  ],
  [
    "<p>Reduce <span class=\"SYSTEMITEM\">stderr</span> output from <span class=\"APPLICATION\">pg_upgrade</span>'s test script (Tom Lane)</p>"
  ],
  [
    "<p>Support building Postgres with Microsoft Visual Studio 2019 (Haribabu Kommi)</p>"
  ],
  [
    "<p>In Visual Studio builds, honor <tt class=\"LITERAL\">WindowsSDKVersion</tt> environment variable, if that's set (Peifeng Qiu)</p>",
    "<p>This fixes build failures in some configurations.</p>"
  ],
  [
    "<p>Support OpenSSL 1.1.0 and newer in Visual Studio builds (Juan José Santamaría Flecha, Michael Paquier)</p>"
  ],
  [
    "<p>Avoid choosing <tt class=\"LITERAL\">localtime</tt> or <tt class=\"LITERAL\">posixrules</tt> as <tt class=\"VARNAME\">TimeZone</tt> during <span class=\"APPLICATION\">initdb</span> (Tom Lane)</p>",
    "<p>In some cases <span class=\"APPLICATION\">initdb</span> would choose one of these artificial zone names over the <span class=\"QUOTE\">\"real\"</span> zone name. Prefer any other match to the C library's timezone behavior over these two.</p>"
  ],
  [
    "<p>Adjust <tt class=\"STRUCTNAME\">pg_timezone_names</tt> view to show the <tt class=\"LITERAL\">Factory</tt> time zone if and only if it has a short abbreviation (Tom Lane)</p>",
    "<p>Historically, IANA set up this artificial zone with an <span class=\"QUOTE\">\"abbreviation\"</span> like <tt class=\"LITERAL\">Local time zone must be set--see zic manual page</tt>. Modern versions of the tzdb database show <tt class=\"LITERAL\">-00</tt> instead, but some platforms alter the data to show one or another of the historical phrases. Show this zone only if it uses the modern abbreviation.</p>"
  ],
  [
    "<p>Sync our copy of the timezone library with IANA tzcode release 2019b (Tom Lane)</p>",
    "<p>This adds support for <span class=\"APPLICATION\">zic</span>'s new <tt class=\"OPTION\">-b slim</tt> option to reduce the size of the installed zone files. We are not currently using that, but may enable it in future.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"APPLICATION\">tzdata</span> release 2019b for DST law changes in Brazil, plus historical corrections for Hong Kong, Italy, and Palestine.</p>"
  ]
]