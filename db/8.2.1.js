[
  [
    "<p>Fix crash with <code class=\"literal\">SELECT</code> ...\n          <code class=\"literal\">LIMIT ALL</code> (also <code class=\"literal\">LIMIT NULL</code>) (Tom)</p>"
  ],
  [
    "<p><code class=\"filename\">Several\n          /contrib/tsearch2</code> fixes (Teodor)</p>"
  ],
  [
    "<p>On Windows, make log messages coming from the\n          operating system use <acronym class=\"acronym\">ASCII</acronym> encoding (Hiroshi Saito)</p>",
    "<p>This fixes a conversion problem when there is a\n          mismatch between the encoding of the operating system and\n          database server.</p>"
  ],
  [
    "<p>Fix Windows linking of <span class=\"application\">pg_dump</span> using <code class=\"filename\">win32.mak</code> (Hiroshi Saito)</p>"
  ],
  [
    "<p>Fix planner mistakes for outer join queries (Tom)</p>"
  ],
  [
    "<p>Fix several problems in queries involving sub-SELECTs\n          (Tom)</p>"
  ],
  [
    "<p>Fix potential crash in SPI during subtransaction abort\n          (Tom)</p>",
    "<p>This affects all PL functions since they all use\n          SPI.</p>"
  ],
  [
    "<p>Improve build speed of <acronym class=\"acronym\">PDF</acronym> documentation (Peter)</p>"
  ],
  [
    "<p>Re-add <acronym class=\"acronym\">JST</acronym> (Japan)\n          timezone abbreviation (Tom)</p>"
  ],
  [
    "<p>Improve optimization decisions related to index scans\n          (Tom)</p>"
  ],
  [
    "<p>Have <span class=\"application\">psql</span> print\n          multi-byte combining characters as before, rather than\n          output as <code class=\"literal\">\\u</code> (Tom)</p>"
  ],
  [
    "<p>Improve index usage of regular expressions that use\n          parentheses (Tom)</p>",
    "<p>This improves <span class=\"application\">psql</span>\n          <code class=\"literal\">\\d</code> performance also.</p>"
  ],
  [
    "<p>Make <span class=\"application\">pg_dumpall</span>\n          assume that databases have public <code class=\"literal\">CONNECT</code> privilege, when dumping from a\n          pre-8.2 server (Tom)</p>",
    "<p>This preserves the previous behavior that anyone can\n          connect to a database if allowed by <code class=\"filename\">pg_hba.conf</code>.</p>"
  ]
]