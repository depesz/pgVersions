[
  [
    "<p>Change the server to reject invalidly-encoded\n          multibyte characters in all cases (Tatsuo, Tom)</p>",
    "<p>While <span class=\"productname\">PostgreSQL</span> has\n          been moving in this direction for some time, the checks\n          are now applied uniformly to all encodings and all\n          textual input, and are now always errors not merely\n          warnings. This change defends against SQL-injection\n          attacks of the type described in CVE-2006-2313.</p>"
  ],
  [
    "<p>Reject unsafe uses of <code class=\"literal\">\\'</code>\n          in string literals</p>",
    "<p>As a server-side defense against SQL-injection attacks\n          of the type described in CVE-2006-2314, the server now\n          only accepts <code class=\"literal\">''</code> and not\n          <code class=\"literal\">\\'</code> as a representation of\n          ASCII single quote in SQL string literals. By default,\n          <code class=\"literal\">\\'</code> is rejected only when\n          <code class=\"varname\">client_encoding</code> is set to a\n          client-only encoding (SJIS, BIG5, GBK, GB18030, or UHC),\n          which is the scenario in which SQL injection is possible.\n          A new configuration parameter <code class=\"varname\">backslash_quote</code> is available to adjust\n          this behavior when needed. Note that full security\n          against CVE-2006-2314 might require client-side changes;\n          the purpose of <code class=\"varname\">backslash_quote</code> is in part to make it\n          obvious that insecure clients are insecure.</p>"
  ],
  [
    "<p>Modify <span class=\"application\">libpq</span>'s\n          string-escaping routines to be aware of encoding\n          considerations and <code class=\"varname\">standard_conforming_strings</code></p>",
    "<p>This fixes <span class=\"application\">libpq</span>-using applications for the\n          security issues described in CVE-2006-2313 and\n          CVE-2006-2314, and also future-proofs them against the\n          planned changeover to SQL-standard string literal syntax.\n          Applications that use multiple <span class=\"productname\">PostgreSQL</span> connections concurrently\n          should migrate to <code class=\"function\">PQescapeStringConn()</code> and <code class=\"function\">PQescapeByteaConn()</code> to ensure that\n          escaping is done correctly for the settings in use in\n          each database connection. Applications that do string\n          escaping <span class=\"quote\">&#x201C;<span class=\"quote\">by\n          hand</span>&#x201D;</span> should be modified to rely on library\n          routines instead.</p>"
  ],
  [
    "<p>Fix weak key selection in pgcrypto (Marko Kreen)</p>",
    "<p>Errors in fortuna PRNG reseeding logic could cause a\n          predictable session key to be selected by <code class=\"function\">pgp_sym_encrypt()</code> in some cases. This\n          only affects non-OpenSSL-using builds.</p>"
  ],
  [
    "<p>Fix some incorrect encoding conversion functions</p>",
    "<p><code class=\"function\">win1251_to_iso</code>,\n          <code class=\"function\">win866_to_iso</code>, <code class=\"function\">euc_tw_to_big5</code>, <code class=\"function\">euc_tw_to_mic</code>, <code class=\"function\">mic_to_euc_tw</code> were all broken to\n          varying extents.</p>"
  ],
  [
    "<p>Clean up stray remaining uses of <code class=\"literal\">\\'</code> in strings (Bruce, Jan)</p>"
  ],
  [
    "<p>Make autovacuum visible in <code class=\"structname\">pg_stat_activity</code> (Alvaro)</p>"
  ],
  [
    "<p>Disable <code class=\"literal\">full_page_writes</code>\n          (Tom)</p>",
    "<p>In certain cases, having <code class=\"literal\">full_page_writes</code> off would cause crash\n          recovery to fail. A proper fix will appear in 8.2; for\n          now it's just disabled.</p>"
  ],
  [
    "<p>Various planner fixes, particularly for bitmap index\n          scans and MIN/MAX optimization (Tom)</p>"
  ],
  [
    "<p>Fix incorrect optimization in merge join (Tom)</p>",
    "<p>Outer joins could sometimes emit multiple copies of\n          unmatched rows.</p>"
  ],
  [
    "<p>Fix crash from using and modifying a plpgsql function\n          in the same transaction</p>"
  ],
  [
    "<p>Fix WAL replay for case where a B-Tree index has been\n          truncated</p>"
  ],
  [
    "<p>Fix <code class=\"literal\">SIMILAR TO</code> for\n          patterns involving <code class=\"literal\">|</code>\n          (Tom)</p>"
  ],
  [
    "<p>Fix <code class=\"command\">SELECT INTO</code> and\n          <code class=\"command\">CREATE TABLE AS</code> to create\n          tables in the default tablespace, not the base directory\n          (Kris Jurka)</p>"
  ],
  [
    "<p>Fix server to use custom DH SSL parameters correctly\n          (Michael Fuhr)</p>"
  ],
  [
    "<p>Improve qsort performance (Dann Corbit)</p>",
    "<p>Currently this code is only used on Solaris.</p>"
  ],
  [
    "<p>Fix for OS/X Bonjour on x86 systems (Ashley Clark)</p>"
  ],
  [
    "<p>Fix various minor memory leaks</p>"
  ],
  [
    "<p>Fix problem with password prompting on some Win32\n          systems (Robert Kinberg)</p>"
  ],
  [
    "<p>Improve <span class=\"application\">pg_dump</span>'s\n          handling of default values for domains</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_dumpall</span> to\n          handle identically-named users and groups reasonably\n          (only possible when dumping from a pre-8.1 server)\n          (Tom)</p>",
    "<p>The user and group will be merged into a single role\n          with <code class=\"literal\">LOGIN</code> permission.\n          Formerly the merged role wouldn't have <code class=\"literal\">LOGIN</code> permission, making it unusable as\n          a user.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_restore</span>\n          <code class=\"literal\">-n</code> to work as documented\n          (Tom)</p>"
  ]
]