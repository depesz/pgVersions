[
  [
    "<p>Restore functionality of <code class=\"command\">ALTER {ROLE|DATABASE} SET role</code> (Tom Lane, Noah Misch) <a class=\"ulink\" href=\"https://postgr.es/c/c46333865\" target=\"_top\">§</a></p>",
    "<p>The fix for CVE-2024-10978 accidentally caused settings for <code class=\"varname\">role</code> to not be applied if they come from non-interactive sources, including previous <code class=\"command\">ALTER {ROLE|DATABASE}</code> commands and the <code class=\"varname\">PGOPTIONS</code> environment variable.</p>"
  ]
]