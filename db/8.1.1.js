[
  [
    "<p>Fix incorrect optimizations of outer-join conditions\n          (Tom)</p>"
  ],
  [
    "<p>Fix problems with wrong reported column names in cases\n          involving sub-selects flattened by the optimizer\n          (Tom)</p>"
  ],
  [
    "<p>Fix update failures in scenarios involving CHECK\n          constraints, toasted columns, <span class=\"emphasis\"><em>and</em></span> indexes (Tom)</p>"
  ],
  [
    "<p>Fix bgwriter problems after recovering from errors\n          (Tom)</p>",
    "<p>The background writer was found to leak buffer pins\n          after write errors. While not fatal in itself, this might\n          lead to mysterious blockages of later VACUUM\n          commands.</p>"
  ],
  [
    "<p>Prevent failure if client sends Bind protocol message\n          when current transaction is already aborted</p>"
  ],
  [
    "<p><code class=\"filename\">/contrib/tsearch2</code> and\n          <code class=\"filename\">/contrib/ltree</code> fixes\n          (Teodor)</p>"
  ],
  [
    "<p>Fix problems with translated error messages in\n          languages that require word reordering, such as Turkish;\n          also problems with unexpected truncation of output\n          strings and wrong display of the smallest possible bigint\n          value (Andrew, Tom)</p>",
    "<p>These problems only appeared on platforms that were\n          using our <code class=\"filename\">port/snprintf.c</code>\n          code, which includes BSD variants if <code class=\"literal\">--enable-nls</code> was given, and perhaps\n          others. In addition, a different form of the\n          translated-error-message problem could appear on Windows\n          depending on which version of <code class=\"filename\">libintl</code> was used.</p>"
  ],
  [
    "<p>Re-allow <code class=\"literal\">AM</code>/<code class=\"literal\">PM</code>, <code class=\"literal\">HH</code>,\n          <code class=\"literal\">HH12</code>, and <code class=\"literal\">D</code> format specifiers for <code class=\"function\">to_char(time)</code> and <code class=\"function\">to_char(interval)</code>. (<code class=\"function\">to_char(interval)</code> should probably use\n          <code class=\"literal\">HH24</code>.) (Bruce)</p>"
  ],
  [
    "<p>AIX, HPUX, and MSVC compile fixes (Tom, Hiroshi\n          Saito)</p>"
  ],
  [
    "<p>Optimizer improvements (Tom)</p>"
  ],
  [
    "<p>Retry file reads and writes after Windows\n          NO_SYSTEM_RESOURCES error (Qingqing Zhou)</p>"
  ],
  [
    "<p>Prevent <span class=\"application\">autovacuum</span>\n          from crashing during ANALYZE of expression index\n          (Alvaro)</p>"
  ],
  [
    "<p>Fix problems with ON COMMIT DELETE ROWS temp\n          tables</p>"
  ],
  [
    "<p>Fix problems when a trigger alters the output of a\n          SELECT DISTINCT query</p>"
  ],
  [
    "<p>Add 8.1.0 release note item on how to migrate invalid\n          <code class=\"literal\">UTF-8</code> byte sequences (Paul\n          Lindner)</p>"
  ]
]