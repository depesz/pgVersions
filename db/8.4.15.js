[
  [
    "<p>Fix multiple bugs associated with <code class=\"command\">CREATE INDEX CONCURRENTLY</code> (Andres\n          Freund, Tom Lane)</p>",
    "<p>Fix <code class=\"command\">CREATE INDEX\n          CONCURRENTLY</code> to use in-place updates when changing\n          the state of an index's <code class=\"structname\">pg_index</code> row. This prevents race\n          conditions that could cause concurrent sessions to miss\n          updating the target index, thus resulting in corrupt\n          concurrently-created indexes.</p>",
    "<p>Also, fix various other operations to ensure that they\n          ignore invalid indexes resulting from a failed\n          <code class=\"command\">CREATE INDEX CONCURRENTLY</code>\n          command. The most important of these is <code class=\"command\">VACUUM</code>, because an auto-vacuum could\n          easily be launched on the table before corrective action\n          can be taken to fix or remove the invalid index.</p>"
  ],
  [
    "<p>Avoid corruption of internal hash tables when out of\n          memory (Hitoshi Harada)</p>"
  ],
  [
    "<p>Fix planning of non-strict equivalence clauses above\n          outer joins (Tom Lane)</p>",
    "<p>The planner could derive incorrect constraints from a\n          clause equating a non-strict construct to something else,\n          for example <code class=\"literal\">WHERE COALESCE(foo, 0)\n          = 0</code> when <code class=\"literal\">foo</code> is\n          coming from the nullable side of an outer join.</p>"
  ],
  [
    "<p>Improve planner's ability to prove exclusion\n          constraints from equivalence classes (Tom Lane)</p>"
  ],
  [
    "<p>Fix partial-row matching in hashed subplans to handle\n          cross-type cases correctly (Tom Lane)</p>",
    "<p>This affects multicolumn <code class=\"literal\">NOT\n          IN</code> subplans, such as <code class=\"literal\">WHERE\n          (a, b) NOT IN (SELECT x, y FROM ...)</code> when for\n          instance <code class=\"literal\">b</code> and <code class=\"literal\">y</code> are <code class=\"type\">int4</code> and\n          <code class=\"type\">int8</code> respectively. This mistake\n          led to wrong answers or crashes depending on the specific\n          datatypes involved.</p>"
  ],
  [
    "<p>Acquire buffer lock when re-fetching the old tuple for\n          an <code class=\"literal\">AFTER ROW UPDATE/DELETE</code>\n          trigger (Andres Freund)</p>",
    "<p>In very unusual circumstances, this oversight could\n          result in passing incorrect data to the precheck logic\n          for a foreign-key enforcement trigger. That could result\n          in a crash, or in an incorrect decision about whether to\n          fire the trigger.</p>"
  ],
  [
    "<p>Fix <code class=\"command\">ALTER COLUMN TYPE</code> to\n          handle inherited check constraints properly (Pavan\n          Deolasee)</p>",
    "<p>This worked correctly in pre-8.4 releases, and now\n          works correctly in 8.4 and later.</p>"
  ],
  [
    "<p>Fix <code class=\"command\">REASSIGN OWNED</code> to\n          handle grants on tablespaces (&#xC1;lvaro Herrera)</p>"
  ],
  [
    "<p>Ignore incorrect <code class=\"structname\">pg_attribute</code> entries for system\n          columns for views (Tom Lane)</p>",
    "<p>Views do not have any system columns. However, we\n          forgot to remove such entries when converting a table to\n          a view. That's fixed properly for 9.3 and later, but in\n          previous branches we need to defend against existing\n          mis-converted views.</p>"
  ],
  [
    "<p>Fix rule printing to dump <code class=\"literal\">INSERT\n          INTO <em class=\"replaceable\"><code>table</code></em>\n          DEFAULT VALUES</code> correctly (Tom Lane)</p>"
  ],
  [
    "<p>Guard against stack overflow when there are too many\n          <code class=\"literal\">UNION</code>/<code class=\"literal\">INTERSECT</code>/<code class=\"literal\">EXCEPT</code> clauses in a query (Tom Lane)</p>"
  ],
  [
    "<p>Prevent platform-dependent failures when dividing the\n          minimum possible integer value by -1 (Xi Wang, Tom\n          Lane)</p>"
  ],
  [
    "<p>Fix possible access past end of string in date parsing\n          (Hitoshi Harada)</p>"
  ],
  [
    "<p>Produce an understandable error message if the length\n          of the path name for a Unix-domain socket exceeds the\n          platform-specific limit (Tom Lane, Andrew Dunstan)</p>",
    "<p>Formerly, this would result in something quite\n          unhelpful, such as <span class=\"quote\">&#x201C;<span class=\"quote\">Non-recoverable failure in name\n          resolution</span>&#x201D;</span>.</p>"
  ],
  [
    "<p>Fix memory leaks when sending composite column values\n          to the client (Tom Lane)</p>"
  ],
  [
    "<p>Make <span class=\"application\">pg_ctl</span> more\n          robust about reading the <code class=\"filename\">postmaster.pid</code> file (Heikki\n          Linnakangas)</p>",
    "<p>Fix race conditions and possible file descriptor\n          leakage.</p>"
  ],
  [
    "<p>Fix possible crash in <span class=\"application\">psql</span> if incorrectly-encoded data is\n          presented and the <code class=\"varname\">client_encoding</code> setting is a client-only\n          encoding, such as SJIS (Jiang Guiqing)</p>"
  ],
  [
    "<p>Fix bugs in the <code class=\"filename\">restore.sql</code> script emitted by\n          <span class=\"application\">pg_dump</span> in <code class=\"literal\">tar</code> output format (Tom Lane)</p>",
    "<p>The script would fail outright on tables whose names\n          include upper-case characters. Also, make the script\n          capable of restoring data in <code class=\"option\">--inserts</code> mode as well as the regular\n          COPY mode.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_restore</span> to\n          accept POSIX-conformant <code class=\"literal\">tar</code>\n          files (Brian Weaver, Tom Lane)</p>",
    "<p>The original coding of <span class=\"application\">pg_dump</span>'s <code class=\"literal\">tar</code> output mode produced files that are\n          not fully conformant with the POSIX standard. This has\n          been corrected for version 9.3. This patch updates\n          previous branches so that they will accept both the\n          incorrect and the corrected formats, in hopes of avoiding\n          compatibility problems when 9.3 comes out.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_resetxlog</span> to\n          locate <code class=\"filename\">postmaster.pid</code>\n          correctly when given a relative path to the data\n          directory (Tom Lane)</p>",
    "<p>This mistake could lead to <span class=\"application\">pg_resetxlog</span> not noticing that there\n          is an active postmaster using the data directory.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">libpq</span>'s\n          <code class=\"function\">lo_import()</code> and\n          <code class=\"function\">lo_export()</code> functions to\n          report file I/O errors properly (Tom Lane)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">ecpg</span>'s processing\n          of nested structure pointer variables (Muhammad\n          Usama)</p>"
  ],
  [
    "<p>Make <code class=\"filename\">contrib/pageinspect</code>'s btree page\n          inspection functions take buffer locks while examining\n          pages (Tom Lane)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pgxs</span> support for\n          building loadable modules on AIX (Tom Lane)</p>",
    "<p>Building modules outside the original source tree\n          didn't work on AIX.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2012j for DST law\n          changes in Cuba, Israel, Jordan, Libya, Palestine,\n          Western Samoa, and portions of Brazil.</p>"
  ]
]