[
  [
    "<p>Prevent error recursion crashes when encoding\n          conversion fails (Tom)</p>",
    "<p>This change extends fixes made in the last two minor\n          releases for related failure scenarios. The previous\n          fixes were narrowly tailored for the original problem\n          reports, but we have now recognized that <span class=\"emphasis\"><em>any</em></span> error thrown by an\n          encoding conversion function could potentially lead to\n          infinite recursion while trying to report the error. The\n          solution therefore is to disable translation and encoding\n          conversion and report the plain-ASCII form of any error\n          message, if we find we have gotten into a recursive error\n          reporting situation. (CVE-2009-0922)</p>"
  ],
  [
    "<p>Disallow <code class=\"command\">CREATE\n          CONVERSION</code> with the wrong encodings for the\n          specified conversion function (Heikki)</p>",
    "<p>This prevents one possible scenario for encoding\n          conversion failure. The previous change is a backstop to\n          guard against other kinds of failures in the same\n          area.</p>"
  ],
  [
    "<p>Fix core dump when <code class=\"function\">to_char()</code> is given format codes that\n          are inappropriate for the type of the data argument\n          (Tom)</p>"
  ],
  [
    "<p>Add <code class=\"literal\">MUST</code> (Mauritius\n          Island Summer Time) to the default list of known timezone\n          abbreviations (Xavier Bugaud)</p>"
  ]
]