[
  [
    "<p>Improve behavior of <span class=\"application\">libpq</span>'s quoting functions (Andres Freund, Tom Lane) <a class=\"ulink\" href=\"https://postgr.es/c/985908df1\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/c08309584\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/f864a4cdf\" target=\"_top\">§</a></p>",
    "<p>The changes made for CVE-2025-1094 had one serious oversight: <code class=\"function\">PQescapeLiteral()</code> and <code class=\"function\">PQescapeIdentifier()</code> failed to honor their string length parameter, instead always reading to the input string's trailing null. This resulted in including unwanted text in the output, if the caller intended to truncate the string via the length parameter. With very bad luck it could cause a crash due to reading off the end of memory.</p>",
    "<p>In addition, modify all these quoting functions so that when invalid encoding is detected, an invalid sequence is substituted for just the first byte of the presumed character, not all of it. This reduces the risk of problems if a calling application performs additional processing on the quoted string.</p>"
  ],
  [
    "<p>Fix crash involving triggers on partitioned tables that make use of transition tables (Kyotaro Horiguchi) <a class=\"ulink\" href=\"https://postgr.es/c/8e58f8024\" target=\"_top\">§</a></p>",
    "<p>If there are both <code class=\"literal\">AFTER UPDATE</code> and <code class=\"literal\">AFTER DELETE</code> triggers, the need for transition tables was determined incorrectly, leading to a crash during cross-partition updates.</p>"
  ]
]