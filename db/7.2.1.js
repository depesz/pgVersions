[
  [
    "<p>Ensure that sequence counters do not go backwards\n          after a crash (Tom)</p>"
  ],
  [
    "<p>Fix pgaccess kanji-conversion key binding (Tatsuo)</p>"
  ],
  [
    "<p>Optimizer improvements (Tom)</p>"
  ],
  [
    "<p>Cash I/O improvements (Tom)</p>"
  ],
  [
    "<p>New Russian FAQ</p>"
  ],
  [
    "<p>Compile fix for missing AuthBlockSig (Heiko)</p>"
  ],
  [
    "<p>Additional time zones and time zone fixes (Thomas)</p>"
  ],
  [
    "<p>Allow psql \\connect to handle mixed case database and\n          user names (Tom)</p>"
  ],
  [
    "<p>Return proper OID on command completion even with ON\n          INSERT rules (Tom)</p>"
  ],
  [
    "<p>Allow COPY FROM to use 8-bit DELIMITERS (Tatsuo)</p>"
  ],
  [
    "<p>Fix bug in extract/date_part for\n          milliseconds/microseconds (Tatsuo)</p>"
  ],
  [
    "<p>Improve handling of multiple UNIONs with different\n          lengths (Tom)</p>"
  ],
  [
    "<p>contrib/btree_gist improvements (Teodor Sigaev)</p>"
  ],
  [
    "<p>contrib/tsearch dictionary improvements, see\n          README.tsearch for an additional installation step\n          (Thomas T. Thai, Teodor Sigaev)</p>"
  ],
  [
    "<p>Fix for array subscripts handling (Tom)</p>"
  ],
  [
    "<p>Allow EXECUTE of \"CREATE TABLE AS ... SELECT\" in\n          PL/pgSQL (Tom)</p>"
  ]
]