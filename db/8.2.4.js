[
  [
    "<p>Support explicit placement of the temporary-table\n          schema within <code class=\"varname\">search_path</code>,\n          and disable searching it for functions and operators\n          (Tom)</p>",
    "<p>This is needed to allow a security-definer function to\n          set a truly secure value of <code class=\"varname\">search_path</code>. Without it, an unprivileged\n          SQL user can use temporary objects to execute code with\n          the privileges of the security-definer function\n          (CVE-2007-2138). See <code class=\"command\">CREATE\n          FUNCTION</code> for more information.</p>"
  ],
  [
    "<p>Fix <code class=\"varname\">shared_preload_libraries</code> for Windows by\n          forcing reload in each backend (Korry Douglas)</p>"
  ],
  [
    "<p>Fix <code class=\"function\">to_char()</code> so it\n          properly upper/lower cases localized day or month names\n          (Pavel Stehule)</p>"
  ],
  [
    "<p><code class=\"filename\">/contrib/tsearch2</code> crash\n          fixes (Teodor)</p>"
  ],
  [
    "<p>Require <code class=\"command\">COMMIT PREPARED</code>\n          to be executed in the same database as the transaction\n          was prepared in (Heikki)</p>"
  ],
  [
    "<p>Allow <code class=\"command\">pg_dump</code> to do\n          binary backups larger than two gigabytes on Windows\n          (Magnus)</p>"
  ],
  [
    "<p>New traditional (Taiwan) Chinese <acronym class=\"acronym\">FAQ</acronym> (Zhou Daojing)</p>"
  ],
  [
    "<p>Prevent the statistics collector from writing to disk\n          too frequently (Tom)</p>"
  ],
  [
    "<p>Fix potential-data-corruption bug in how <code class=\"command\">VACUUM FULL</code> handles <code class=\"command\">UPDATE</code> chains (Tom, Pavan Deolasee)</p>"
  ],
  [
    "<p>Fix bug in domains that use array types (Tom)</p>"
  ],
  [
    "<p>Fix <code class=\"command\">pg_dump</code> so it can\n          dump a serial column's sequence using <code class=\"option\">-t</code> when not also dumping the owning table\n          (Tom)</p>"
  ],
  [
    "<p>Planner fixes, including improving outer join and\n          bitmap scan selection logic (Tom)</p>"
  ],
  [
    "<p>Fix possible wrong answers or crash when a PL/pgSQL\n          function tries to <code class=\"literal\">RETURN</code>\n          from within an <code class=\"literal\">EXCEPTION</code>\n          block (Tom)</p>"
  ],
  [
    "<p>Fix PANIC during enlargement of a hash index (Tom)</p>"
  ],
  [
    "<p>Fix POSIX-style timezone specs to follow new USA DST\n          rules (Tom)</p>"
  ]
]