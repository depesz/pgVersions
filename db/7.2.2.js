[
  [
    "<p>Allow EXECUTE of \"CREATE TABLE AS ... SELECT\" in\n          PL/pgSQL (Tom)</p>"
  ],
  [
    "<p>Fix for compressed transaction log id wraparound\n          (Tom)</p>"
  ],
  [
    "<p>Fix PQescapeBytea/PQunescapeBytea so that they handle\n          bytes &gt; 0x7f (Tatsuo)</p>"
  ],
  [
    "<p>Fix for psql and <span class=\"application\">pg_dump</span> crashing when invoked with\n          non-existent long options (Tatsuo)</p>"
  ],
  [
    "<p>Fix crash when invoking geometric operators (Tom)</p>"
  ],
  [
    "<p>Allow OPEN cursor(args) (Tom)</p>"
  ],
  [
    "<p>Fix for rtree_gist index build (Teodor)</p>"
  ],
  [
    "<p>Fix for dumping user-defined aggregates (Tom)</p>"
  ],
  [
    "<p>contrib/intarray fixes (Oleg)</p>"
  ],
  [
    "<p>Fix for complex UNION/EXCEPT/INTERSECT queries using\n          parens (Tom)</p>"
  ],
  [
    "<p>Fix to pg_convert (Tatsuo)</p>"
  ],
  [
    "<p>Fix for crash with long DATA strings (Thomas,\n          Neil)</p>"
  ],
  [
    "<p>Fix for repeat(), lpad(), rpad() and long strings\n          (Neil)</p>"
  ]
]