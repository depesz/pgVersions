[
  [
    "<p>Fix some additional cases of VACUUM \"No one parent\n          tuple was found\" error</p>"
  ],
  [
    "<p>Prevent VACUUM from being called inside a function\n          (Bruce)</p>"
  ],
  [
    "<p>Ensure pg_clog updates are sync'd to disk before\n          marking checkpoint complete</p>"
  ],
  [
    "<p>Avoid integer overflow during large hash joins</p>"
  ],
  [
    "<p>Make GROUP commands work when pg_group.grolist is\n          large enough to be toasted</p>"
  ],
  [
    "<p>Fix errors in datetime tables; some timezone names\n          weren't being recognized</p>"
  ],
  [
    "<p>Fix integer overflows in circle_poly(), path_encode(),\n          path_add() (Neil)</p>"
  ],
  [
    "<p>Repair long-standing logic errors in lseg_eq(),\n          lseg_ne(), lseg_center()</p>"
  ]
]