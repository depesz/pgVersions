[
  [
    "<p>Fix persistence marking of shared buffers during WAL\n          replay (Jeff Davis)</p>",
    "<p>This mistake can result in buffers not being written\n          out during checkpoints, resulting in data corruption if\n          the server later crashes without ever having written\n          those buffers. Corruption can occur on any server\n          following crash recovery, but it is significantly more\n          likely to occur on standby slave servers since those\n          perform much more WAL replay. There is a low probability\n          of corruption of btree and GIN indexes. There is a much\n          higher probability of corruption of table <span class=\"quote\">&#x201C;<span class=\"quote\">visibility\n          maps</span>&#x201D;</span>, which might lead to wrong answers\n          from index-only scans. Table data proper cannot be\n          corrupted by this bug.</p>",
    "<p>While no index corruption due to this bug is known to\n          have occurred in the field, as a precautionary measure it\n          is recommended that production installations <code class=\"command\">REINDEX</code> all btree and GIN indexes at a\n          convenient time after upgrading to 9.2.1.</p>",
    "<p>Also, it is recommended to perform a <code class=\"command\">VACUUM</code> of all tables while having\n          <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-client.html#GUC-VACUUM-FREEZE-TABLE-AGE\"><code class=\"varname\">\n          vacuum_freeze_table_age</code></a> set to zero. This will\n          fix any incorrect visibility map data. <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-resource.html#GUC-VACUUM-COST-DELAY\"><code class=\"varname\">\n          vacuum_cost_delay</code></a> can be adjusted to reduce\n          the performance impact of vacuuming, while causing it to\n          take longer to finish.</p>"
  ],
  [
    "<p>Fix possible incorrect sorting of output from queries\n          involving <code class=\"literal\">WHERE <em class=\"replaceable\"><code>indexed_column</code></em> IN\n          (<em class=\"replaceable\"><code>list_of_values</code></em>)</code>\n          (Tom Lane)</p>"
  ],
  [
    "<p>Fix planner failure for queries involving <code class=\"literal\">GROUP BY</code> expressions along with window\n          functions and aggregates (Tom Lane)</p>"
  ],
  [
    "<p>Fix planner's assignment of executor parameters (Tom\n          Lane)</p>",
    "<p>This error could result in wrong answers from queries\n          that scan the same <code class=\"literal\">WITH</code>\n          subquery multiple times.</p>"
  ],
  [
    "<p>Improve planner's handling of join conditions in index\n          scans (Tom Lane)</p>"
  ],
  [
    "<p>Improve selectivity estimation for text search queries\n          involving prefixes, i.e. <em class=\"replaceable\"><code>word</code></em><code class=\"literal\">:*</code> patterns (Tom Lane)</p>"
  ],
  [
    "<p>Fix delayed recognition of permissions changes (Tom\n          Lane)</p>",
    "<p>A command that needed no locks other than ones its\n          transaction already had might fail to notice a concurrent\n          <code class=\"command\">GRANT</code> or <code class=\"command\">REVOKE</code> that committed since the start of\n          its transaction.</p>"
  ],
  [
    "<p>Fix <code class=\"command\">ANALYZE</code> to not fail\n          when a column is a domain over an array type (Tom\n          Lane)</p>"
  ],
  [
    "<p>Prevent PL/Perl from crashing if a recursive PL/Perl\n          function is redefined while being executed (Tom Lane)</p>"
  ],
  [
    "<p>Work around possible misoptimization in PL/Perl (Tom\n          Lane)</p>",
    "<p>Some Linux distributions contain an incorrect version\n          of <code class=\"filename\">pthread.h</code> that results\n          in incorrect compiled code in PL/Perl, leading to crashes\n          if a PL/Perl function calls another one that throws an\n          error.</p>"
  ],
  [
    "<p>Remove unnecessary dependency on <span class=\"application\">pg_config</span> from <span class=\"application\">pg_upgrade</span> (Peter Eisentraut)</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2012f for DST law\n          changes in Fiji</p>"
  ]
]