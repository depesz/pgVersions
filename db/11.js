[
  [
    "<p>Make <a class=\"link\" href=\"https://www.postgresql.org/docs/11/app-pgdump.html\" title=\"pg_dump\"><span class=\"application\">pg_dump</span></a> dump the properties of a database, not just its contents (Haribabu Kommi)</p>",
    "<p>Previously, attributes of the database itself, such as database-level <code class=\"command\">GRANT</code>/<code class=\"command\">REVOKE</code> permissions and <code class=\"command\">ALTER DATABASE SET</code> variable settings, were only dumped by <a class=\"link\" href=\"https://www.postgresql.org/docs/11/app-pg-dumpall.html\" title=\"pg_dumpall\"><span class=\"application\">pg_dumpall</span></a>. Now <code class=\"command\">pg_dump --create</code> and <code class=\"command\">pg_restore --create</code> will restore these database properties in addition to the objects within the database. <code class=\"command\">pg_dumpall -g</code> now only dumps role- and tablespace-related attributes. <span class=\"application\">pg_dumpall</span>'s complete output (without <code class=\"option\">-g</code>) is unchanged.</p>",
    "<p><span class=\"application\">pg_dump</span> and <span class=\"application\">pg_restore</span>, without <code class=\"option\">--create</code>, no longer dump/restore database-level comments and security labels; those are now treated as properties of the database.</p>",
    "<p><span class=\"application\">pg_dumpall</span>'s output script will now always create databases with their original locale and encoding, and hence will fail if the locale or encoding name is unknown to the destination system. Previously, <code class=\"command\">CREATE DATABASE</code> would be emitted without these specifications if the database locale and encoding matched the old cluster's defaults.</p>",
    "<p><code class=\"command\">pg_dumpall --clean</code> now restores the original locale and encoding settings of the <code class=\"literal\">postgres</code> and <code class=\"literal\">template1</code> databases, as well as those of user-created databases.</p>"
  ],
  [
    "<p>Consider syntactic form when disambiguating function versus column references (Tom Lane)</p>",
    "<p>When <em class=\"replaceable\"><code>x</code></em> is a table name or composite column, <span class=\"productname\">PostgreSQL</span> has traditionally considered the syntactic forms <code class=\"literal\"><em class=\"replaceable\"><code>f</code></em>(<em class=\"replaceable\"><code>x</code></em>)</code> and <code class=\"literal\"><em class=\"replaceable\"><code>x</code></em>.<em class=\"replaceable\"><code>f</code></em></code> to be equivalent, allowing tricks such as writing a function and then using it as though it were a computed-on-demand column. However, if both interpretations are feasible, the column interpretation was always chosen, leading to surprising results if the user intended the function interpretation. Now, if there is ambiguity, the interpretation that matches the syntactic form is chosen.</p>"
  ],
  [
    "<p>Fully enforce uniqueness of table and domain constraint names (Tom Lane)</p>",
    "<p><span class=\"productname\">PostgreSQL</span> expects the names of a table's constraints to be distinct, and likewise for the names of a domain's constraints. However, there was not rigid enforcement of this, and previously there were corner cases where duplicate names could be created.</p>"
  ],
  [
    "<p>Make <code class=\"function\">power(numeric, numeric)</code> and <code class=\"function\">power(float8, float8)</code> handle <code class=\"literal\">NaN</code> inputs according to the POSIX standard (Tom Lane, Dang Minh Huong)</p>",
    "<p>POSIX says that <code class=\"literal\">NaN ^ 0 = 1</code> and <code class=\"literal\">1 ^ NaN = 1</code>, but all other cases with <code class=\"literal\">NaN</code> input(s) should return <code class=\"literal\">NaN</code>. <code class=\"function\">power(numeric, numeric)</code> just returned <code class=\"literal\">NaN</code> in all such cases; now it honors the two exceptions. <code class=\"function\">power(float8, float8)</code> followed the standard if the C library does; but on some old Unix platforms the library doesn't, and there were also problems on some versions of Windows.</p>"
  ],
  [
    "<p>Prevent <a class=\"link\" href=\"https://www.postgresql.org/docs/11/functions-formatting.html#FUNCTIONS-FORMATTING-NUMERIC-TABLE\" title=\"Table 9.26. Template Patterns for Numeric Formatting\"><code class=\"function\">to_number()</code></a> from consuming characters when the template separator does not match (Oliver Ford)</p>",
    "<p>Specifically, <code class=\"command\">SELECT to_number('1234', '9,999')</code> used to return <code class=\"literal\">134</code>. It will now return <code class=\"literal\">1234</code>. <code class=\"literal\">L</code> and <code class=\"literal\">TH</code> now only consume characters that are not digits, positive/negative signs, decimal points, or commas.</p>"
  ],
  [
    "<p>Fix <a class=\"link\" href=\"https://www.postgresql.org/docs/11/functions-formatting.html\" title=\"9.8. Data Type Formatting Functions\"><code class=\"function\">to_date()</code></a>, <code class=\"function\">to_number()</code>, and <code class=\"function\">to_timestamp()</code> to skip a character for each template character (Tom Lane)</p>",
    "<p>Previously, they skipped one <span class=\"emphasis\"><em>byte</em></span> for each byte of template character, resulting in strange behavior if either string contained multibyte characters.</p>"
  ],
  [
    "<p>Adjust the handling of backslashes inside double-quotes in template strings for <code class=\"function\">to_char()</code>, <code class=\"function\">to_number()</code>, and <code class=\"function\">to_timestamp()</code>.</p>",
    "<p>Such a backslash now escapes the character after it, particularly a double-quote or another backslash.</p>"
  ],
  [
    "<p>Correctly handle relative path expressions in <code class=\"function\">xmltable()</code>, <code class=\"function\">xpath()</code>, and other XML-handling functions (Markus Winand)</p>",
    "<p>Per the SQL standard, relative paths start from the document node of the XML input document, not the root node as these functions previously did.</p>"
  ],
  [
    "<p>In the <a class=\"link\" href=\"https://www.postgresql.org/docs/11/protocol-overview.html#PROTOCOL-QUERY-CONCEPTS\" title=\"53.1.2. Extended Query Overview\">extended query protocol</a>, make <a class=\"link\" href=\"https://www.postgresql.org/docs/11/runtime-config-client.html#GUC-STATEMENT-TIMEOUT\"><code class=\"varname\">statement_timeout</code></a> apply to each Execute message separately, not to all commands before Sync (Tatsuo Ishii, Andres Freund)</p>"
  ],
  [
    "<p>Remove the <code class=\"structfield\">relhaspkey</code> column from system catalog <code class=\"structname\">pg_class</code> (Peter Eisentraut)</p>",
    "<p>Applications needing to check for a primary key should consult <code class=\"structname\">pg_index</code>.</p>"
  ],
  [
    "<p>Replace system catalog <code class=\"structname\">pg_proc</code>'s <code class=\"structfield\">proisagg</code> and <code class=\"structfield\">proiswindow</code> columns with <code class=\"structfield\">prokind</code> (Peter Eisentraut)</p>",
    "<p>This new column more clearly distinguishes functions, procedures, aggregates, and window functions.</p>"
  ],
  [
    "<p>Correct information schema column <a class=\"link\" href=\"https://www.postgresql.org/docs/11/infoschema-tables.html\" title=\"37.52. tables\"><code class=\"structname\">tables</code>.<code class=\"structfield\">table_type</code></a> to return <code class=\"literal\">FOREIGN</code> instead of <code class=\"literal\">FOREIGN TABLE</code> (Peter Eisentraut)</p>",
    "<p>This new output matches the SQL standard.</p>"
  ],
  [
    "<p>Change the ps process display labels for background workers to match the <a class=\"link\" href=\"https://www.postgresql.org/docs/11/monitoring-stats.html#PG-STAT-ACTIVITY-VIEW\" title=\"Table 28.3. pg_stat_activity View\"><code class=\"structname\">pg_stat_activity</code>.<code class=\"structfield\">backend_type</code></a> labels (Peter Eisentraut)</p>"
  ],
  [
    "<p>Cause large object permission checks to happen during large object open, <a class=\"link\" href=\"https://www.postgresql.org/docs/11/lo-interfaces.html#LO-OPEN\" title=\"35.3.4. Opening an Existing Large Object\"><code class=\"function\">lo_open()</code></a>, not when a read or write is attempted (Tom Lane, Michael Paquier)</p>",
    "<p>If write access is requested and not available, an error will now be thrown even if the large object is never written to.</p>"
  ],
  [
    "<p>Prevent non-superusers from reindexing shared catalogs (Michael Paquier, Robert Haas)</p>",
    "<p>Previously, database owners were also allowed to do this, but now it is considered outside the bounds of their privileges.</p>"
  ],
  [
    "<p>Remove deprecated <a class=\"link\" href=\"https://www.postgresql.org/docs/11/adminpack.html\" title=\"F.1. adminpack\"><code class=\"filename\">adminpack</code></a> functions <code class=\"function\">pg_file_read()</code>, <code class=\"function\">pg_file_length()</code>, and <code class=\"function\">pg_logfile_rotate()</code> (Stephen Frost)</p>",
    "<p>Equivalent functionality is now present in the core backend. Existing <code class=\"filename\">adminpack</code> installs will continue to have access to these functions until they are updated via <code class=\"command\">ALTER EXTENSION ... UPDATE</code>.</p>"
  ],
  [
    "<p>Honor the capitalization of double-quoted command options (Daniel Gustafsson)</p>",
    "<p>Previously, option names in certain SQL commands were forcibly lower-cased even if entered with double quotes; thus for example <code class=\"literal\">\"FillFactor\"</code> would be accepted as an index storage option, though properly its name is lower-case. Such cases will now generate an error.</p>"
  ],
  [
    "<p>Remove server parameter <code class=\"varname\">replacement_sort_tuples</code> (Peter Geoghegan)</p>",
    "<p>Replacement sorts were determined to be no longer useful.</p>"
  ],
  [
    "<p>Remove <code class=\"literal\">WITH</code> clause in <a class=\"link\" href=\"https://www.postgresql.org/docs/11/sql-createfunction.html\" title=\"CREATE FUNCTION\"><code class=\"command\">CREATE FUNCTION</code></a> (Michael Paquier)</p>",
    "<p><span class=\"productname\">PostgreSQL</span> has long supported a more standard-compliant syntax for this capability.</p>"
  ],
  [
    "<p>In PL/pgSQL trigger functions, the <code class=\"varname\">OLD</code> and <code class=\"varname\">NEW</code> variables now read as NULL when not assigned (Tom Lane)</p>",
    "<p>Previously, references to these variables could be parsed but not executed.</p>"
  ],
  [
    "<p>Allow the creation of partitions based on hashing a key column (Amul Sul)</p>"
  ],
  [
    "<p>Support indexes on partitioned tables (Álvaro Herrera, Amit Langote)</p>",
    "<p>An <span class=\"quote\">“<span class=\"quote\">index</span>”</span> on a partitioned table is not a physical index across the whole partitioned table, but rather a template for automatically creating similar indexes on each partition of the table.</p>",
    "<p>If the partition key is part of the index's column set, a partitioned index may be declared <code class=\"literal\">UNIQUE</code>. It will represent a valid uniqueness constraint across the whole partitioned table, even though each physical index only enforces uniqueness within its own partition.</p>",
    "<p>The new command <a class=\"link\" href=\"https://www.postgresql.org/docs/11/sql-alterindex.html\" title=\"ALTER INDEX\"><code class=\"command\">ALTER INDEX ATTACH PARTITION</code></a> causes an existing index on a partition to be associated with a matching index template for its partitioned table. This provides flexibility in setting up a new partitioned index for an existing partitioned table.</p>"
  ],
  [
    "<p>Allow foreign keys on partitioned tables (Álvaro Herrera)</p>"
  ],
  [
    "<p>Allow <code class=\"literal\">FOR EACH ROW</code> triggers on partitioned tables (Álvaro Herrera)</p>",
    "<p>Creation of a trigger on a partitioned table automatically creates triggers on all existing and future partitions. This also allows deferred unique constraints on partitioned tables.</p>"
  ],
  [
    "<p>Allow partitioned tables to have a default partition (Jeevan Ladhe, Beena Emerson, Ashutosh Bapat, Rahila Syed, Robert Haas)</p>",
    "<p>The default partition will store rows that don't match any of the other defined partitions, and is searched accordingly.</p>"
  ],
  [
    "<p><code class=\"command\">UPDATE</code> statements that change a partition key column now cause affected rows to be moved to the appropriate partitions (Amit Khandekar)</p>"
  ],
  [
    "<p>Allow <code class=\"command\">INSERT</code>, <code class=\"command\">UPDATE</code>, and <code class=\"command\">COPY</code> on partitioned tables to properly route rows to foreign partitions (Etsuro Fujita, Amit Langote)</p>",
    "<p>This is supported by <code class=\"filename\">postgres_fdw</code> foreign tables. Since the <code class=\"function\">ExecForeignInsert</code> callback function is called for this in a different way than it used to be, foreign data wrappers must be modified to cope with this change.</p>"
  ],
  [
    "<p>Allow faster partition elimination during query processing (Amit Langote, David Rowley, Dilip Kumar)</p>",
    "<p>This speeds access to partitioned tables with many partitions.</p>"
  ],
  [
    "<p>Allow partition elimination during query execution (David Rowley, Beena Emerson)</p>",
    "<p>Previously, partition elimination only happened at planning time, meaning many joins and prepared queries could not use partition elimination.</p>"
  ],
  [
    "<p>In an equality join between partitioned tables, allow matching partitions to be joined directly (Ashutosh Bapat)</p>",
    "<p>This feature is disabled by default but can be enabled by changing <a class=\"link\" href=\"https://www.postgresql.org/docs/11/runtime-config-query.html#GUC-ENABLE-PARTITIONWISE-JOIN\"><code class=\"varname\">enable_partitionwise_join</code></a>.</p>"
  ],
  [
    "<p>Allow aggregate functions on partitioned tables to be evaluated separately for each partition, subsequently merging the results (Jeevan Chalke, Ashutosh Bapat, Robert Haas)</p>",
    "<p>This feature is disabled by default but can be enabled by changing <a class=\"link\" href=\"https://www.postgresql.org/docs/11/runtime-config-query.html#GUC-ENABLE-PARTITIONWISE-AGGREGATE\"><code class=\"varname\">enable_partitionwise_aggregate</code></a>.</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/11/postgres-fdw.html\" title=\"F.33. postgres_fdw\"><code class=\"filename\">postgres_fdw</code></a> to push down aggregates to foreign tables that are partitions (Jeevan Chalke)</p>"
  ],
  [
    "<p>Allow parallel building of a btree index (Peter Geoghegan, Rushabh Lathia, Heikki Linnakangas)</p>"
  ],
  [
    "<p>Allow hash joins to be performed in parallel using a shared hash table (Thomas Munro)</p>"
  ],
  [
    "<p>Allow <code class=\"literal\">UNION</code> to run each <code class=\"command\">SELECT</code> in parallel if the individual <code class=\"command\">SELECT</code>s cannot be parallelized (Amit Khandekar, Robert Haas, Amul Sul)</p>"
  ],
  [
    "<p>Allow partition scans to more efficiently use parallel workers (Amit Khandekar, Robert Haas, Amul Sul)</p>"
  ],
  [
    "<p>Allow <code class=\"literal\">LIMIT</code> to be passed to parallel workers (Robert Haas, Tom Lane)</p>",
    "<p>This allows workers to reduce returned results and use targeted index scans.</p>"
  ],
  [
    "<p>Allow single-evaluation queries, e.g. <code class=\"literal\">WHERE</code> clause aggregate queries, and functions in the target list to be parallelized (Amit Kapila, Robert Haas)</p>"
  ],
  [
    "<p>Add server parameter <a class=\"link\" href=\"https://www.postgresql.org/docs/11/runtime-config-query.html#GUC-PARALLEL-LEADER-PARTICIPATION\"><code class=\"varname\">parallel_leader_participation</code></a> to control whether the leader also executes subplans (Thomas Munro)</p>",
    "<p>The default is enabled, meaning the leader will execute subplans.</p>"
  ],
  [
    "<p>Allow parallelization of commands <code class=\"command\">CREATE TABLE ... AS</code>, <code class=\"command\">SELECT INTO</code>, and <code class=\"command\">CREATE MATERIALIZED VIEW</code> (Haribabu Kommi)</p>"
  ],
  [
    "<p>Improve performance of sequential scans with many parallel workers (David Rowley)</p>"
  ],
  [
    "<p>Add reporting of parallel workers' sort activity in <code class=\"command\">EXPLAIN</code> (Robert Haas, Tom Lane)</p>"
  ],
  [
    "<p>Allow B-tree indexes to include columns that are not part of the search key or unique constraint, but are available to be read by index-only scans (Anastasia Lubennikova, Alexander Korotkov, Teodor Sigaev)</p>",
    "<p>This is enabled by the new <code class=\"literal\">INCLUDE</code> clause of <a class=\"link\" href=\"https://www.postgresql.org/docs/11/sql-createindex.html\" title=\"CREATE INDEX\"><code class=\"command\">CREATE INDEX</code></a>. It facilitates building <span class=\"quote\">“<span class=\"quote\">covering indexes</span>”</span> that optimize specific types of queries. Columns can be included even if their data types don't have B-tree support.</p>"
  ],
  [
    "<p>Improve performance of monotonically increasing index additions (Pavan Deolasee, Peter Geoghegan)</p>"
  ],
  [
    "<p>Improve performance of hash index scans (Ashutosh Sharma)</p>"
  ],
  [
    "<p>Add predicate locking for hash, GiST and GIN indexes (Shubham Barai)</p>",
    "<p>This reduces the likelihood of serialization conflicts in serializable-mode transactions.</p>"
  ],
  [
    "<p>Add prefix-match operator <code class=\"type\">text</code> <code class=\"literal\">^@</code> <code class=\"type\">text</code>, which is supported by SP-GiST (Ildus Kurbangaliev)</p>",
    "<p>This is similar to using <em class=\"replaceable\"><code>var</code></em> <code class=\"literal\">LIKE 'word%'</code> with a btree index, but it is more efficient.</p>"
  ],
  [
    "<p>Allow polygons to be indexed with SP-GiST (Nikita Glukhov, Alexander Korotkov)</p>"
  ],
  [
    "<p>Allow SP-GiST to use lossy representation of leaf keys (Teodor Sigaev, Heikki Linnakangas, Alexander Korotkov, Nikita Glukhov)</p>"
  ],
  [
    "<p>Improve selection of the most common values for statistics (Jeff Janes, Dean Rasheed)</p>",
    "<p>Previously, the most common values (<acronym class=\"acronym\">MCV</acronym>s) were identified based on their frequency compared to all column values. Now, <acronym class=\"acronym\">MCV</acronym>s are chosen based on their frequency compared to the non-<acronym class=\"acronym\">MCV</acronym> values. This improves the robustness of the algorithm for both uniform and non-uniform distributions.</p>"
  ],
  [
    "<p>Improve selectivity estimates for <code class=\"literal\">&gt;=</code> and <code class=\"literal\">&lt;=</code> (Tom Lane)</p>",
    "<p>Previously, such cases used the same selectivity estimates as <code class=\"literal\">&gt;</code> and <code class=\"literal\">&lt;</code>, respectively, unless the comparison constants are <acronym class=\"acronym\">MCV</acronym>s. This change is particularly helpful for queries involving <code class=\"literal\">BETWEEN</code> with small ranges.</p>"
  ],
  [
    "<p>Reduce <em class=\"replaceable\"><code>var</code></em> <code class=\"literal\">=</code> <em class=\"replaceable\"><code>var</code></em> to <em class=\"replaceable\"><code>var</code></em> <code class=\"literal\">IS NOT NULL</code> where equivalent (Tom Lane)</p>",
    "<p>This leads to better selectivity estimates.</p>"
  ],
  [
    "<p>Improve optimizer's row count estimates for <code class=\"literal\">EXISTS</code> and <code class=\"literal\">NOT EXISTS</code> queries (Tom Lane)</p>"
  ],
  [
    "<p>Make the optimizer account for evaluation costs and selectivity of <code class=\"literal\">HAVING</code> clauses (Tom Lane)</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/11/jit.html\" title=\"Chapter 32. Just-in-Time Compilation (JIT)\">Just-in-Time</a> (<acronym class=\"acronym\">JIT</acronym>) compilation of some parts of query plans to improve execution speed (Andres Freund)</p>",
    "<p>This feature requires <span class=\"application\">LLVM</span> to be available. It is not currently enabled by default, even in builds that support it.</p>"
  ],
  [
    "<p>Allow bitmap scans to perform index-only scans when possible (Alexander Kuzmenkov)</p>"
  ],
  [
    "<p>Update the free space map during <code class=\"command\">VACUUM</code> (Claudio Freire)</p>",
    "<p>This allows free space to be reused more quickly.</p>"
  ],
  [
    "<p>Allow <code class=\"command\">VACUUM</code> to avoid unnecessary index scans (Masahiko Sawada, Alexander Korotkov)</p>"
  ],
  [
    "<p>Improve performance of committing multiple concurrent transactions (Amit Kapila)</p>"
  ],
  [
    "<p>Reduce memory usage for queries using set-returning functions in their target lists (Andres Freund)</p>"
  ],
  [
    "<p>Improve the speed of aggregate computations (Andres Freund)</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/11/postgres-fdw.html\" title=\"F.33. postgres_fdw\"><code class=\"filename\">postgres_fdw</code></a> to push <code class=\"command\">UPDATE</code>s and <code class=\"command\">DELETE</code>s using joins to foreign servers (Etsuro Fujita)</p>",
    "<p>Previously, only non-join <code class=\"command\">UPDATE</code>s and <code class=\"command\">DELETE</code>s were pushed.</p>"
  ],
  [
    "<p>Add support for <em class=\"firstterm\">large pages</em> on Windows (Takayuki Tsunakawa, Thomas Munro)</p>",
    "<p>This is controlled by the <a class=\"link\" href=\"https://www.postgresql.org/docs/11/runtime-config-resource.html#GUC-HUGE-PAGES\">huge_pages</a> configuration parameter.</p>"
  ],
  [
    "<p>Show memory usage in output from <a class=\"link\" href=\"https://www.postgresql.org/docs/11/runtime-config-statistics.html#RUNTIME-CONFIG-STATISTICS-MONITOR\" title=\"19.9.2. Statistics Monitoring\"><code class=\"varname\">log_statement_stats</code></a>, <code class=\"varname\">log_parser_stats</code>, <code class=\"varname\">log_planner_stats</code>, and <code class=\"varname\">log_executor_stats</code> (Justin Pryzby, Peter Eisentraut)</p>"
  ],
  [
    "<p>Add column <a class=\"link\" href=\"https://www.postgresql.org/docs/11/monitoring-stats.html#PG-STAT-ACTIVITY-VIEW\" title=\"Table 28.3. pg_stat_activity View\"><code class=\"structname\">pg_stat_activity</code>.<code class=\"structfield\">backend_type</code></a> to show the type of a background worker (Peter Eisentraut)</p>",
    "<p>The type is also visible in <span class=\"application\">ps</span> output.</p>"
  ],
  [
    "<p>Make <a class=\"link\" href=\"https://www.postgresql.org/docs/11/runtime-config-autovacuum.html#GUC-LOG-AUTOVACUUM-MIN-DURATION\"><code class=\"varname\">log_autovacuum_min_duration</code></a> log skipped tables that are concurrently being dropped (Nathan Bossart)</p>"
  ],
  [
    "<p>Add <code class=\"literal\">information_schema</code> columns related to table constraints and triggers (Peter Eisentraut)</p>",
    "<p>Specifically, <code class=\"structname\">triggers</code>.<code class=\"structfield\">action_order</code>, <code class=\"structname\">triggers</code>.<code class=\"structfield\">action_reference_old_table</code>, and <code class=\"structname\">triggers</code>.<code class=\"structfield\">action_reference_new_table</code> are now populated, where before they were always null. Also, <code class=\"structname\">table_constraints</code>.<code class=\"structfield\">enforced</code> now exists but is not yet usefully populated.</p>"
  ],
  [
    "<p>Allow the server to specify more complex <a class=\"link\" href=\"https://www.postgresql.org/docs/11/auth-ldap.html\" title=\"20.10. LDAP Authentication\"><acronym class=\"acronym\">LDAP</acronym></a> specifications in search+bind mode (Thomas Munro)</p>",
    "<p>Specifically, <code class=\"literal\">ldapsearchfilter</code> allows pattern matching using combinations of <acronym class=\"acronym\">LDAP</acronym> attributes.</p>"
  ],
  [
    "<p>Allow <acronym class=\"acronym\">LDAP</acronym> authentication to use encrypted <acronym class=\"acronym\">LDAP</acronym> (Thomas Munro)</p>",
    "<p>We already supported <acronym class=\"acronym\">LDAP</acronym> over <acronym class=\"acronym\">TLS</acronym> by using <code class=\"literal\">ldaptls=1</code>. This new <acronym class=\"acronym\">TLS</acronym> <acronym class=\"acronym\">LDAP</acronym> method for encrypted <acronym class=\"acronym\">LDAP</acronym> is enabled with <code class=\"literal\">ldapscheme=ldaps</code> or <code class=\"literal\">ldapurl=ldaps://</code>.</p>"
  ],
  [
    "<p>Improve logging of <acronym class=\"acronym\">LDAP</acronym> errors (Thomas Munro)</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/11/default-roles.html#DEFAULT-ROLES-TABLE\" title=\"Table 21.1. Default Roles\">default roles</a> that enable file system access (Stephen Frost)</p>",
    "<p>Specifically, the new roles are: <code class=\"literal\">pg_read_server_files</code>, <code class=\"literal\">pg_write_server_files</code>, and <code class=\"literal\">pg_execute_server_program</code>. These roles now also control who can use server-side <code class=\"command\">COPY</code> and the <a class=\"link\" href=\"https://www.postgresql.org/docs/11/file-fdw.html\" title=\"F.14. file_fdw\"><code class=\"filename\">file_fdw</code></a> extension. Previously, only superusers could use these functions, and that is still the default behavior.</p>"
  ],
  [
    "<p>Allow access to file system functions to be controlled by <code class=\"command\">GRANT</code>/<code class=\"command\">REVOKE</code> permissions, rather than superuser checks (Stephen Frost)</p>",
    "<p>Specifically, these functions were modified: <a class=\"link\" href=\"https://www.postgresql.org/docs/11/functions-admin.html#FUNCTIONS-ADMIN-GENFILE-TABLE\" title=\"Table 9.88. Generic File Access Functions\"><code class=\"function\">pg_ls_dir()</code></a>, <code class=\"function\">pg_read_file()</code>, <code class=\"function\">pg_read_binary_file()</code>, <code class=\"function\">pg_stat_file()</code>.</p>"
  ],
  [
    "<p>Use <code class=\"command\">GRANT</code>/<code class=\"command\">REVOKE</code> to control access to <a class=\"link\" href=\"https://www.postgresql.org/docs/11/lo-interfaces.html#LO-IMPORT\" title=\"35.3.2. Importing a Large Object\"><code class=\"function\">lo_import()</code></a> and <code class=\"function\">lo_export()</code> (Michael Paquier, Tom Lane)</p>",
    "<p>Previously, only superusers were granted access to these functions.</p>",
    "<p>The compile-time option <code class=\"literal\">ALLOW_DANGEROUS_LO_FUNCTIONS</code> has been removed.</p>"
  ],
  [
    "<p>Use view owner not session owner when preventing non-password access to <a class=\"link\" href=\"https://www.postgresql.org/docs/11/postgres-fdw.html\" title=\"F.33. postgres_fdw\"><code class=\"filename\">postgres_fdw</code></a> tables (Robert Haas)</p>",
    "<p><span class=\"productname\">PostgreSQL</span> only allows superusers to access <code class=\"filename\">postgres_fdw</code> tables without passwords, e.g. via <code class=\"literal\">peer</code>. Previously, the session owner had to be a superuser to allow such access; now the view owner is checked instead.</p>"
  ],
  [
    "<p>Fix invalid locking permission check in <code class=\"command\">SELECT FOR UPDATE</code> on views (Tom Lane)</p>"
  ],
  [
    "<p>Add server setting <a class=\"link\" href=\"https://www.postgresql.org/docs/11/runtime-config-connection.html#GUC-SSL-PASSPHRASE-COMMAND\"><code class=\"varname\">ssl_passphrase_command</code></a> to allow supplying of the passphrase for <acronym class=\"acronym\">SSL</acronym> key files (Peter Eisentraut)</p>",
    "<p>Also add <a class=\"link\" href=\"https://www.postgresql.org/docs/11/runtime-config-connection.html#GUC-SSL-PASSPHRASE-COMMAND-SUPPORTS-RELOAD\"><code class=\"varname\">ssl_passphrase_command_supports_reload</code></a> to specify whether the <acronym class=\"acronym\">SSL</acronym> configuration should be reloaded and <code class=\"varname\">ssl_passphrase_command</code> called during a server configuration reload.</p>"
  ],
  [
    "<p>Add storage parameter <a class=\"link\" href=\"https://www.postgresql.org/docs/11/sql-createtable.html#SQL-CREATETABLE-STORAGE-PARAMETERS\" title=\"Storage Parameters\"><code class=\"varname\">toast_tuple_target</code></a> to control the minimum tuple length before <acronym class=\"acronym\">TOAST</acronym> storage will be considered (Simon Riggs)</p>",
    "<p>The default <acronym class=\"acronym\">TOAST</acronym> threshold has not been changed.</p>"
  ],
  [
    "<p>Allow server options related to memory and file sizes to be specified in units of bytes (Beena Emerson)</p>",
    "<p>The new unit suffix is <span class=\"quote\">“<span class=\"quote\">B</span>”</span>. This is in addition to the existing units <span class=\"quote\">“<span class=\"quote\">kB</span>”</span>, <span class=\"quote\">“<span class=\"quote\">MB</span>”</span>, <span class=\"quote\">“<span class=\"quote\">GB</span>”</span> and <span class=\"quote\">“<span class=\"quote\">TB</span>”</span>.</p>"
  ],
  [
    "<p>Allow the <acronym class=\"acronym\">WAL</acronym> file size to be set during <span class=\"application\">initdb</span> (Beena Emerson)</p>",
    "<p>Previously, the 16MB default could only be changed at compile time.</p>"
  ],
  [
    "<p>Retain <acronym class=\"acronym\">WAL</acronym> data for only a single checkpoint (Simon Riggs)</p>",
    "<p>Previously, <acronym class=\"acronym\">WAL</acronym> was retained for two checkpoints.</p>"
  ],
  [
    "<p>Fill the unused portion of force-switched <acronym class=\"acronym\">WAL</acronym> segment files with zeros for improved compressibility (Chapman Flack)</p>"
  ],
  [
    "<p>Replicate <code class=\"command\">TRUNCATE</code> activity when using logical replication (Simon Riggs, Marco Nenciarini, Peter Eisentraut)</p>"
  ],
  [
    "<p>Pass prepared transaction information to logical replication subscribers (Nikhil Sontakke, Stas Kelvich)</p>"
  ],
  [
    "<p>Exclude unlogged tables, temporary tables, and <code class=\"filename\">pg_internal.init</code> files from streaming base backups (David Steele)</p>",
    "<p>There is no need to copy such files.</p>"
  ],
  [
    "<p>Allow checksums of heap pages to be verified during streaming base backup (Michael Banck)</p>"
  ],
  [
    "<p>Allow replication slots to be advanced programmatically, rather than be consumed by subscribers (Petr Jelinek)</p>",
    "<p>This allows efficient advancement of replication slots when the contents do not need to be consumed. This is performed by <code class=\"function\">pg_replication_slot_advance()</code>.</p>"
  ],
  [
    "<p>Add timeline information to the <a class=\"link\" href=\"https://www.postgresql.org/docs/11/continuous-archiving.html#BACKUP-LOWLEVEL-BASE-BACKUP\" title=\"25.3.3. Making a Base Backup Using the Low Level API\"><code class=\"filename\">backup_label</code></a> file (Michael Paquier)</p>",
    "<p>Also add a check that the <acronym class=\"acronym\">WAL</acronym> timeline matches the <code class=\"filename\">backup_label</code> file's timeline.</p>"
  ],
  [
    "<p>Add host and port connection information to the <code class=\"structname\">pg_stat_wal_receiver</code> system view (Haribabu Kommi)</p>"
  ],
  [
    "<p>Allow <code class=\"command\">ALTER TABLE</code> to add a column with a non-null default without doing a table rewrite (Andrew Dunstan, Serge Rielau)</p>",
    "<p>This is enabled when the default value is a constant.</p>"
  ],
  [
    "<p>Allow views to be locked by locking the underlying tables (Yugo Nagata)</p>"
  ],
  [
    "<p>Allow <code class=\"command\">ALTER INDEX</code> to set statistics-gathering targets for expression indexes (Alexander Korotkov, Adrien Nayrat)</p>",
    "<p>In <span class=\"application\">psql</span>, <code class=\"literal\">\\d+</code> now shows the statistics target for indexes.</p>"
  ],
  [
    "<p>Allow multiple tables to be specified in one <code class=\"command\">VACUUM</code> or <code class=\"command\">ANALYZE</code> command (Nathan Bossart)</p>",
    "<p>Also, if any table mentioned in <code class=\"command\">VACUUM</code> uses a column list, then the <code class=\"command\">ANALYZE</code> keyword must be supplied; previously, <code class=\"command\">ANALYZE</code> was implied in such cases.</p>"
  ],
  [
    "<p>Add parenthesized options syntax to <code class=\"command\">ANALYZE</code> (Nathan Bossart)</p>",
    "<p>This is similar to the syntax supported by <code class=\"command\">VACUUM</code>.</p>"
  ],
  [
    "<p>Add <code class=\"command\">CREATE AGGREGATE</code> option to specify the behavior of the aggregate's finalization function (Tom Lane)</p>",
    "<p>This is helpful for allowing user-defined aggregate functions to be optimized and to work as window functions.</p>"
  ],
  [
    "<p>Allow the creation of arrays of domains (Tom Lane)</p>",
    "<p>This also allows <code class=\"function\">array_agg()</code> to be used on domains.</p>"
  ],
  [
    "<p>Support domains over composite types (Tom Lane)</p>",
    "<p>Also allow PL/Perl, PL/Python, and PL/Tcl to handle composite-domain function arguments and results. Also improve PL/Python domain handling.</p>"
  ],
  [
    "<p>Add casts from <code class=\"type\">JSONB</code> scalars to numeric and boolean data types (Anastasia Lubennikova)</p>"
  ],
  [
    "<p>Add all <a class=\"link\" href=\"https://www.postgresql.org/docs/11/sql-select.html#SQL-WINDOW\" title=\"WINDOW Clause\">window function</a> framing options specified by SQL:2011 (Oliver Ford, Tom Lane)</p>",
    "<p>Specifically, allow <code class=\"literal\">RANGE</code> mode to use <code class=\"literal\">PRECEDING</code> and <code class=\"literal\">FOLLOWING</code> to select rows having grouping values within plus or minus the specified offset. Add <code class=\"literal\">GROUPS</code> mode to include plus or minus the number of peer groups. Frame exclusion syntax was also added.</p>"
  ],
  [
    "<p>Add <acronym class=\"acronym\">SHA-2</acronym> family of hash functions (Peter Eisentraut)</p>",
    "<p>Specifically, <a class=\"link\" href=\"https://www.postgresql.org/docs/11/functions-binarystring.html#FUNCTIONS-BINARYSTRING-OTHER\" title=\"Table 9.12. Other Binary String Functions\"><code class=\"function\">sha224()</code></a>, <code class=\"function\">sha256()</code>, <code class=\"function\">sha384()</code>, <code class=\"function\">sha512()</code> were added.</p>"
  ],
  [
    "<p>Add support for 64-bit non-cryptographic hash functions (Robert Haas, Amul Sul)</p>"
  ],
  [
    "<p>Allow <code class=\"function\">to_char()</code> and <code class=\"function\">to_timestamp()</code> to specify the time zone's offset from <acronym class=\"acronym\">UTC</acronym> in hours and minutes (Nikita Glukhov, Andrew Dunstan)</p>",
    "<p>This is done with format specifications <a class=\"link\" href=\"https://www.postgresql.org/docs/11/functions-formatting.html#FUNCTIONS-FORMATTING-DATETIME-TABLE\" title=\"Table 9.24. Template Patterns for Date/Time Formatting\"><code class=\"literal\">TZH</code></a> and <code class=\"literal\">TZM</code>.</p>"
  ],
  [
    "<p>Add text search function <a class=\"link\" href=\"https://www.postgresql.org/docs/11/functions-textsearch.html#TEXTSEARCH-FUNCTIONS-TABLE\" title=\"Table 9.41. Text Search Functions\"><code class=\"function\">websearch_to_tsquery()</code></a> that supports a query syntax similar to that used by web search engines (Victor Drobny, Dmitry Ivanov)</p>"
  ],
  [
    "<p>Add functions <a class=\"link\" href=\"https://www.postgresql.org/docs/11/functions-textsearch.html#TEXTSEARCH-FUNCTIONS-TABLE\" title=\"Table 9.41. Text Search Functions\"><code class=\"function\">json(b)_to_tsvector()</code></a> to create a text search query for matching <code class=\"type\">JSON</code>/<code class=\"type\">JSONB</code> values (Dmitry Dolgov)</p>"
  ],
  [
    "<p>Add SQL-level procedures, which can start and commit their own transactions (Peter Eisentraut)</p>",
    "<p>They are created with the new <a class=\"link\" href=\"https://www.postgresql.org/docs/11/sql-createprocedure.html\" title=\"CREATE PROCEDURE\"><code class=\"command\">CREATE PROCEDURE</code></a> command and invoked via <a class=\"link\" href=\"https://www.postgresql.org/docs/11/sql-call.html\" title=\"CALL\"><code class=\"command\">CALL</code></a>.</p>",
    "<p>The new <code class=\"command\">ALTER</code>/<code class=\"command\">DROP ROUTINE</code> commands allow altering/dropping of all routine-like objects, including procedures, functions, and aggregates.</p>",
    "<p>Also, writing <code class=\"literal\">FUNCTION</code> is now preferred over writing <code class=\"literal\">PROCEDURE</code> in <code class=\"command\">CREATE OPERATOR</code> and <code class=\"command\">CREATE TRIGGER</code>, because the referenced object must be a function not a procedure. However, the old syntax is still accepted for compatibility.</p>"
  ],
  [
    "<p>Add transaction control to PL/pgSQL, PL/Perl, PL/Python, PL/Tcl, and <acronym class=\"acronym\">SPI</acronym> server-side languages (Peter Eisentraut)</p>",
    "<p>Transaction control is only available within top-transaction-level procedures and nested <code class=\"command\">DO</code> and <code class=\"command\">CALL</code> blocks that only contain other <code class=\"command\">DO</code> and <code class=\"command\">CALL</code> blocks.</p>"
  ],
  [
    "<p>Add the ability to define PL/pgSQL composite-type variables as not null, constant, or with initial values (Tom Lane)</p>"
  ],
  [
    "<p>Allow PL/pgSQL to handle changes to composite types (e.g. record, row) that happen between the first and later function executions in the same session (Tom Lane)</p>",
    "<p>Previously, such circumstances generated errors.</p>"
  ],
  [
    "<p>Add extension <code class=\"filename\">jsonb_plpython</code> to transform <code class=\"type\">JSONB</code> to/from PL/Python types (Anthony Bykov)</p>"
  ],
  [
    "<p>Add extension <code class=\"filename\">jsonb_plperl</code> to transform <code class=\"type\">JSONB</code> to/from PL/Perl types (Anthony Bykov)</p>"
  ],
  [
    "<p>Change libpq to disable compression by default (Peter Eisentraut)</p>",
    "<p>Compression is already disabled in modern OpenSSL versions, so that the libpq setting had no effect with such libraries.</p>"
  ],
  [
    "<p>Add <code class=\"literal\">DO CONTINUE</code> option to <span class=\"application\">ecpg</span>'s <code class=\"literal\">WHENEVER</code> statement (Vinayak Pokale)</p>",
    "<p>This generates a C <code class=\"command\">continue</code> statement, causing a return to the top of the contained loop when the specified condition occurs.</p>"
  ],
  [
    "<p>Add an <span class=\"application\">ecpg</span> mode to enable Oracle Pro*C-style handling of char arrays.</p>",
    "<p>This mode is enabled with <code class=\"option\">-C</code>.</p>"
  ],
  [
    "<p>Add <span class=\"application\">psql</span> command <code class=\"literal\">\\gdesc</code> to display the names and types of the columns in a query result (Pavel Stehule)</p>"
  ],
  [
    "<p>Add <span class=\"application\">psql</span> variables to report query activity and errors (Fabien Coelho)</p>",
    "<p>Specifically, the new variables are <code class=\"literal\">ERROR</code>, <code class=\"literal\">SQLSTATE</code>, <code class=\"literal\">ROW_COUNT</code>, <code class=\"literal\">LAST_ERROR_MESSAGE</code>, and <code class=\"literal\">LAST_ERROR_SQLSTATE</code>.</p>"
  ],
  [
    "<p>Allow <span class=\"application\">psql</span> to test for the existence of a variable (Fabien Coelho)</p>",
    "<p>Specifically, the syntax <code class=\"literal\">:{?variable_name}</code> allows a variable's existence to be tested in an <code class=\"literal\">\\if</code> statement.</p>"
  ],
  [
    "<p>Allow environment variable <code class=\"envar\">PSQL_PAGER</code> to control <span class=\"application\">psql</span>'s pager (Pavel Stehule)</p>",
    "<p>This allows <span class=\"application\">psql</span>'s default pager to be specified as a separate environment variable from the pager for other applications. <code class=\"envar\">PAGER</code> is still honored if <code class=\"envar\">PSQL_PAGER</code> is not set.</p>"
  ],
  [
    "<p>Make psql's <code class=\"literal\">\\d+</code> command always show the table's partitioning information (Amit Langote, Ashutosh Bapat)</p>",
    "<p>Previously, partition information would not be displayed for a partitioned table if it had no partitions. Also indicate which partitions are themselves partitioned.</p>"
  ],
  [
    "<p>Ensure that <span class=\"application\">psql</span> reports the proper user name when prompting for a password (Tom Lane)</p>",
    "<p>Previously, combinations of <code class=\"option\">-U</code> and a user name embedded in a <acronym class=\"acronym\">URI</acronym> caused incorrect reporting. Also suppress the user name before the password prompt when <code class=\"option\">--password</code> is specified.</p>"
  ],
  [
    "<p>Allow <code class=\"command\">quit</code> and <code class=\"command\">exit</code> to exit <span class=\"application\">psql</span> when given with no prior input (Bruce Momjian)</p>",
    "<p>Also print hints about how to exit when <code class=\"command\">quit</code> and <code class=\"command\">exit</code> are used alone on a line while the input buffer is not empty. Add a similar hint for <code class=\"command\">help</code>.</p>"
  ],
  [
    "<p>Make <span class=\"application\">psql</span> hint at using control-D when <code class=\"command\">\\q</code> is entered alone on a line but ignored (Bruce Momjian)</p>",
    "<p>For example, <code class=\"command\">\\q</code> does not exit when supplied in character strings.</p>"
  ],
  [
    "<p>Improve tab completion for <code class=\"command\">ALTER INDEX RESET</code>/<code class=\"command\">SET</code> (Masahiko Sawada)</p>"
  ],
  [
    "<p>Add infrastructure to allow <span class=\"application\">psql</span> to adapt its tab completion queries based on the server version (Tom Lane)</p>",
    "<p>Previously, tab completion queries could fail against older servers.</p>"
  ],
  [
    "<p>Add <span class=\"application\">pgbench</span> expression support for NULLs, booleans, and some functions and operators (Fabien Coelho)</p>"
  ],
  [
    "<p>Add <code class=\"literal\">\\if</code> conditional support to <span class=\"application\">pgbench</span> (Fabien Coelho)</p>"
  ],
  [
    "<p>Allow the use of non-<acronym class=\"acronym\">ASCII</acronym> characters in <span class=\"application\">pgbench</span> variable names (Fabien Coelho)</p>"
  ],
  [
    "<p>Add <span class=\"application\">pgbench</span> option <code class=\"option\">--init-steps</code> to control the initialization steps performed (Masahiko Sawada)</p>"
  ],
  [
    "<p>Add an approximately Zipfian-distributed random generator to <span class=\"application\">pgbench</span> (Alik Khilazhev)</p>"
  ],
  [
    "<p>Allow the random seed to be set in <span class=\"application\">pgbench</span> (Fabien Coelho)</p>"
  ],
  [
    "<p>Allow <span class=\"application\">pgbench</span> to do exponentiation with <code class=\"function\">pow()</code> and <code class=\"function\">power()</code> (Raúl Marín Rodríguez)</p>"
  ],
  [
    "<p>Add hashing functions to <span class=\"application\">pgbench</span> (Ildar Musin)</p>"
  ],
  [
    "<p>Make <span class=\"application\">pgbench</span> statistics more accurate when using <code class=\"option\">--latency-limit</code> and <code class=\"option\">--rate</code> (Fabien Coelho)</p>"
  ],
  [
    "<p>Add an option to <a class=\"link\" href=\"https://www.postgresql.org/docs/11/app-pgbasebackup.html\" title=\"pg_basebackup\"><span class=\"application\">pg_basebackup</span></a> that creates a named replication slot (Michael Banck)</p>",
    "<p>The option <code class=\"option\">--create-slot</code> creates the named replication slot (<code class=\"option\">--slot</code>) when the <acronym class=\"acronym\">WAL</acronym> streaming method (<code class=\"option\">--wal-method=stream</code>) is used.</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/11/app-initdb.html\" title=\"initdb\"><span class=\"application\">initdb</span></a> to set group read access to the data directory (David Steele)</p>",
    "<p>This is accomplished with the new initdb option <code class=\"option\">--allow-group-access</code>. Administrators can also set group permissions on the empty data directory before running initdb. Server variable <a class=\"link\" href=\"https://www.postgresql.org/docs/11/runtime-config-file-locations.html#GUC-DATA-DIRECTORY\"><code class=\"varname\">data_directory_mode</code></a> allows reading of data directory group permissions.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/11/pgverifychecksums.html\" title=\"pg_verify_checksums\"><span class=\"application\">pg_verify_checksums</span></a> tool to verify database checksums while offline (Magnus Hagander)</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/11/app-pgresetwal.html\" title=\"pg_resetwal\"><span class=\"application\">pg_resetwal</span></a> to change the <acronym class=\"acronym\">WAL</acronym> segment size via <code class=\"option\">--wal-segsize</code> (Nathan Bossart)</p>"
  ],
  [
    "<p>Add long options to <span class=\"application\">pg_resetwal</span> and <span class=\"application\">pg_controldata</span> (Nathan Bossart, Peter Eisentraut)</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/11/app-pgreceivewal.html\" title=\"pg_receivewal\"><span class=\"application\">pg_receivewal</span></a> option <code class=\"option\">--no-sync</code> to prevent synchronous <acronym class=\"acronym\">WAL</acronym> writes, for testing (Michael Paquier)</p>"
  ],
  [
    "<p>Add <span class=\"application\">pg_receivewal</span> option <code class=\"option\">--endpos</code> to specify when <acronym class=\"acronym\">WAL</acronym> receiving should stop (Michael Paquier)</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/11/app-pg-ctl.html\" title=\"pg_ctl\"><span class=\"application\">pg_ctl</span></a> to send the <code class=\"literal\">SIGKILL</code> signal to processes (Andres Freund)</p>",
    "<p>This was previously unsupported due to concerns over possible misuse.</p>"
  ],
  [
    "<p>Reduce the number of files copied by <a class=\"link\" href=\"https://www.postgresql.org/docs/11/app-pgrewind.html\" title=\"pg_rewind\"><span class=\"application\">pg_rewind</span></a> (Michael Paquier)</p>"
  ],
  [
    "<p>Prevent <span class=\"application\">pg_rewind</span> from running as <code class=\"literal\">root</code> (Michael Paquier)</p>"
  ],
  [
    "<p>Add <span class=\"application\">pg_dumpall</span> option <code class=\"option\">--encoding</code> to control output encoding (Michael Paquier)</p>",
    "<p><span class=\"application\">pg_dump</span> already had this option.</p>"
  ],
  [
    "<p>Add <span class=\"application\">pg_dump</span> option <code class=\"option\">--load-via-partition-root</code> to force loading of data into the partition's root table, rather than the original partition (Rushabh Lathia)</p>",
    "<p>This is useful if the system to be loaded to has different collation definitions or endianness, possibly requiring rows to be stored in different partitions than previously.</p>"
  ],
  [
    "<p>Add an option to suppress dumping and restoring database object comments (Robins Tharakan)</p>",
    "<p>The new <span class=\"application\">pg_dump</span>, <span class=\"application\">pg_dumpall</span>, and <span class=\"application\">pg_restore</span> option is <code class=\"option\">--no-comments</code>.</p>"
  ],
  [
    "<p>Add <span class=\"application\">PGXS</span> support for installing include files (Andrew Gierth)</p>",
    "<p>This supports creating extension modules that depend on other modules. Formerly there was no easy way for the dependent module to find the referenced one's include files. Several existing <code class=\"filename\">contrib</code> modules that define data types have been adjusted to install relevant files. Also, PL/Perl and PL/Python now install their include files, to support creation of transform modules for those languages.</p>"
  ],
  [
    "<p>Install <code class=\"filename\">errcodes.txt</code> to allow extensions to access the list of error codes known to <span class=\"productname\">PostgreSQL</span> (Thomas Munro)</p>"
  ],
  [
    "<p>Convert documentation to DocBook <acronym class=\"acronym\">XML</acronym> (Peter Eisentraut, Alexander Lakhin, Jürgen Purtz)</p>",
    "<p>The file names still use an <code class=\"filename\">sgml</code> extension for compatibility with back branches.</p>"
  ],
  [
    "<p>Use <code class=\"filename\">stdbool.h</code> to define type <code class=\"type\">bool</code> on platforms where it's suitable, which is most (Peter Eisentraut)</p>",
    "<p>This eliminates a coding hazard for extension modules that need to include <code class=\"filename\">stdbool.h</code>.</p>"
  ],
  [
    "<p>Overhaul the way that initial system catalog contents are defined (John Naylor)</p>",
    "<p>The initial data is now represented in Perl data structures, making it much easier to manipulate mechanically.</p>"
  ],
  [
    "<p>Prevent extensions from creating custom server parameters that take a quoted list of values (Tom Lane)</p>",
    "<p>This cannot be supported at present because knowledge of the parameter's property would be required even before the extension is loaded.</p>"
  ],
  [
    "<p>Add ability to use channel binding when using <a class=\"link\" href=\"https://www.postgresql.org/docs/11/auth-password.html\" title=\"20.5. Password Authentication\"><acronym class=\"acronym\">SCRAM</acronym></a> authentication (Michael Paquier)</p>",
    "<p>Channel binding is intended to prevent man-in-the-middle attacks, but <acronym class=\"acronym\">SCRAM</acronym> cannot prevent them unless it can be forced to be active. Unfortunately, there is no way to do that in libpq. Support for it is expected in future versions of libpq and in interfaces not built using libpq, e.g. JDBC.</p>"
  ],
  [
    "<p>Allow background workers to attach to databases that normally disallow connections (Magnus Hagander)</p>"
  ],
  [
    "<p>Add support for hardware <acronym class=\"acronym\">CRC</acronym> calculations on <span class=\"productname\">ARMv8</span> (Yuqi Gu, Heikki Linnakangas, Thomas Munro)</p>"
  ],
  [
    "<p>Speed up lookups of built-in functions by OID (Andres Freund)</p>",
    "<p>The previous binary search has been replaced by a lookup array.</p>"
  ],
  [
    "<p>Speed up construction of query results (Andres Freund)</p>"
  ],
  [
    "<p>Improve speed of access to system caches (Andres Freund)</p>"
  ],
  [
    "<p>Add a generational memory allocator which is optimized for serial allocation/deallocation (Tomas Vondra)</p>",
    "<p>This reduces memory usage for logical decoding.</p>"
  ],
  [
    "<p>Make the computation of <code class=\"structname\">pg_class</code>.<code class=\"structfield\">reltuples</code> by <code class=\"command\">VACUUM</code> consistent with its computation by <code class=\"command\">ANALYZE</code> (Tomas Vondra)</p>"
  ],
  [
    "<p>Update to use <span class=\"application\">perltidy</span> version <code class=\"literal\">20170521</code> (Tom Lane, Peter Eisentraut)</p>"
  ],
  [
    "<p>Allow extension <a class=\"link\" href=\"https://www.postgresql.org/docs/11/pgprewarm.html\" title=\"F.27. pg_prewarm\"><code class=\"filename\">pg_prewarm</code></a> to restore the previous shared buffer contents on startup (Mithun Cy, Robert Haas)</p>",
    "<p>This is accomplished by having <code class=\"filename\">pg_prewarm</code> store the shared buffers' relation and block number data to disk occasionally during server operation, and at shutdown.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/11/pgtrgm.html\" title=\"F.31. pg_trgm\"><code class=\"filename\">pg_trgm</code></a> function <code class=\"function\">strict_word_similarity()</code> to compute the similarity of whole words (Alexander Korotkov)</p>",
    "<p>The function <code class=\"function\">word_similarity()</code> already existed for this purpose, but it was designed to find similar parts of words, while <code class=\"function\">strict_word_similarity()</code> computes the similarity to whole words.</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/11/btree-gin.html\" title=\"F.6. btree_gin\"><code class=\"filename\">btree_gin</code></a> to index <code class=\"type\">bool</code>, <code class=\"type\">bpchar</code>, <code class=\"type\">name</code> and <code class=\"type\">uuid</code> data types (Matheus Oliveira)</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/11/cube.html\" title=\"F.9. cube\"><code class=\"filename\">cube</code></a> and <a class=\"link\" href=\"https://www.postgresql.org/docs/11/seg.html\" title=\"F.34. seg\"><code class=\"filename\">seg</code></a> extensions to perform index-only scans using GiST indexes (Andrey Borodin)</p>"
  ],
  [
    "<p>Allow retrieval of negative cube coordinates using the <code class=\"literal\">~&gt;</code> operator (Alexander Korotkov)</p>",
    "<p>This is useful for KNN-GiST searches when looking for coordinates in descending order.</p>"
  ],
  [
    "<p>Add Vietnamese letter handling to the <a class=\"link\" href=\"https://www.postgresql.org/docs/11/unaccent.html\" title=\"F.43. unaccent\"><code class=\"filename\">unaccent</code></a> extension (Dang Minh Huong, Michael Paquier)</p>"
  ],
  [
    "<p>Enhance <a class=\"link\" href=\"https://www.postgresql.org/docs/11/amcheck.html\" title=\"F.2. amcheck\"><code class=\"filename\">amcheck</code></a> to check that each heap tuple has an index entry (Peter Geoghegan)</p>"
  ],
  [
    "<p>Have <a class=\"link\" href=\"https://www.postgresql.org/docs/11/adminpack.html\" title=\"F.1. adminpack\"><code class=\"filename\">adminpack</code></a> use the new default file system access roles (Stephen Frost)</p>",
    "<p>Previously, only superusers could call <code class=\"filename\">adminpack</code> functions; now role permissions are checked.</p>"
  ],
  [
    "<p>Widen <code class=\"structname\">pg_stat_statement</code>'s query ID to 64 bits (Robert Haas)</p>",
    "<p>This greatly reduces the chance of query ID hash collisions. The query ID can now potentially display as a negative value.</p>"
  ],
  [
    "<p>Remove the <code class=\"filename\">contrib/start-scripts/osx</code> scripts since they are no longer recommended (use <code class=\"filename\">contrib/start-scripts/macos</code> instead) (Tom Lane)</p>"
  ],
  [
    "<p>Remove the <code class=\"filename\">chkpass</code> extension (Peter Eisentraut)</p>",
    "<p>This extension is no longer considered to be a usable security tool or example of how to write an extension.</p>"
  ]
]