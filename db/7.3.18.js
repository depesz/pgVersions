[
  [
    "<p>Remove security vulnerability that allowed connected\n          users to read backend memory (Tom)</p>",
    "<p>The vulnerability involves changing the data type of a\n          table column used in a SQL function (CVE-2007-0555). This\n          error can easily be exploited to cause a backend crash,\n          and in principle might be used to read database content\n          that the user should not be able to access.</p>"
  ],
  [
    "<p>Fix rare bug wherein btree index page splits could\n          fail due to choosing an infeasible split point (Heikki\n          Linnakangas)</p>"
  ],
  [
    "<p>Tighten security of multi-byte character processing\n          for UTF8 sequences over three bytes long (Tom)</p>"
  ]
]