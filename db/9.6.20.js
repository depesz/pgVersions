[
  [
    "<p>Block <tt class=\"COMMAND\">DECLARE CURSOR ... WITH HOLD</tt> and firing of deferred triggers within index expressions and materialized view queries (Noah Misch)</p>",
    "<p>This is essentially a leak in the <span class=\"QUOTE\">\"security restricted operation\"</span> sandbox mechanism. An attacker having permission to create non-temporary SQL objects could parlay this leak to execute arbitrary SQL code as a superuser.</p>",
    "<p>The <span class=\"PRODUCTNAME\">PostgreSQL</span> Project thanks Etienne Stalmans for reporting this problem. (CVE-2020-25695)</p>"
  ],
  [
    "<p>Fix usage of complex connection-string parameters in <span class=\"APPLICATION\">pg_dump</span>, <span class=\"APPLICATION\">pg_restore</span>, <span class=\"APPLICATION\">clusterdb</span>, <span class=\"APPLICATION\">reindexdb</span>, and <span class=\"APPLICATION\">vacuumdb</span> (Tom Lane)</p>",
    "<p>The <tt class=\"OPTION\">-d</tt> parameter of <span class=\"APPLICATION\">pg_dump</span> and <span class=\"APPLICATION\">pg_restore</span>, or the <tt class=\"OPTION\">--maintenance-db</tt> parameter of the other programs mentioned, can be a <span class=\"QUOTE\">\"connection string\"</span> containing multiple connection parameters rather than just a database name. In cases where these programs need to initiate additional connections, such as parallel processing or processing of multiple databases, the connection string was forgotten and just the basic connection parameters (database name, host, port, and username) were used for the additional connections. This could lead to connection failures if the connection string included any other essential information, such as non-default SSL or GSS parameters. Worse, the connection might succeed but not be encrypted as intended, or be vulnerable to man-in-the-middle attacks that the intended connection parameters would have prevented. (CVE-2020-25694)</p>"
  ],
  [
    "<p>When <span class=\"APPLICATION\">psql</span>'s <tt class=\"COMMAND\">\\connect</tt> command re-uses connection parameters, ensure that all non-overridden parameters from a previous connection string are re-used (Tom Lane)</p>",
    "<p>This avoids cases where reconnection might fail due to omission of relevant parameters, such as non-default SSL or GSS options. Worse, the reconnection might succeed but not be encrypted as intended, or be vulnerable to man-in-the-middle attacks that the intended connection parameters would have prevented. This is largely the same problem as just cited for <span class=\"APPLICATION\">pg_dump</span> et al, although <span class=\"APPLICATION\">psql</span>'s behavior is more complex since the user may intentionally override some connection parameters. (CVE-2020-25694)</p>"
  ],
  [
    "<p>Prevent <span class=\"APPLICATION\">psql</span>'s <tt class=\"COMMAND\">\\gset</tt> command from modifying specially-treated variables (Noah Misch)</p>",
    "<p><tt class=\"COMMAND\">\\gset</tt> without a prefix would overwrite whatever variables the server told it to. Thus, a compromised server could set specially-treated variables such as <tt class=\"VARNAME\">PROMPT1</tt>, giving the ability to execute arbitrary shell code in the user's session.</p>",
    "<p>The <span class=\"PRODUCTNAME\">PostgreSQL</span> Project thanks Nick Cleaton for reporting this problem. (CVE-2020-25696)</p>"
  ],
  [
    "<p>Prevent possible data loss from concurrent truncations of SLRU logs (Noah Misch)</p>",
    "<p>This rare problem would manifest in later <span class=\"QUOTE\">\"apparent wraparound\"</span> or <span class=\"QUOTE\">\"could not access status of transaction\"</span> errors.</p>"
  ],
  [
    "<p>Ensure that SLRU directories are properly fsync'd during checkpoints (Thomas Munro)</p>",
    "<p>This prevents possible data loss in a subsequent operating system crash.</p>"
  ],
  [
    "<p>Fix <tt class=\"COMMAND\">ALTER ROLE</tt> for users with the <tt class=\"LITERAL\">BYPASSRLS</tt> attribute (Tom Lane, Stephen Frost)</p>",
    "<p>The <tt class=\"LITERAL\">BYPASSRLS</tt> attribute is only allowed to be changed by superusers, but other <tt class=\"COMMAND\">ALTER ROLE</tt> operations, such as password changes, should be allowed with only ordinary permission checks. The previous coding erroneously restricted all changes on such a role to superusers.</p>"
  ],
  [
    "<p>Fix handling of expressions in <tt class=\"COMMAND\">CREATE TABLE LIKE</tt> with inheritance (Tom Lane)</p>",
    "<p>If a <tt class=\"COMMAND\">CREATE TABLE</tt> command uses both <tt class=\"LITERAL\">LIKE</tt> and traditional inheritance, column references in <tt class=\"LITERAL\">CHECK</tt> constraints and expression indexes that came from a <tt class=\"LITERAL\">LIKE</tt> parent table tended to get mis-numbered, resulting in wrong answers and/or bizarre error messages. The same could happen in <tt class=\"LITERAL\">GENERATED</tt> expressions, in branches that have that feature.</p>"
  ],
  [
    "<p>Fix off-by-one conversion of negative years to BC dates in <code class=\"FUNCTION\">to_date()</code> and <code class=\"FUNCTION\">to_timestamp()</code> (Dar Alathar-Yemen, Tom Lane)</p>",
    "<p>Also, arrange for the combination of a negative year and an explicit <span class=\"QUOTE\">\"BC\"</span> marker to cancel out and produce AD.</p>"
  ],
  [
    "<p>Ensure that standby servers will archive WAL timeline history files when <tt class=\"VARNAME\">archive_mode</tt> is set to <tt class=\"LITERAL\">always</tt> (Grigory Smolkin, Fujii Masao)</p>",
    "<p>This oversight could lead to failure of subsequent PITR recovery attempts.</p>"
  ],
  [
    "<p>During <span class=\"QUOTE\">\"smart\"</span> shutdown, don't terminate background processes until all client (foreground) sessions are done (Tom Lane)</p>",
    "<p>The previous behavior broke parallel query processing, since the postmaster would terminate parallel workers and refuse to launch any new ones. It also caused autovacuum to cease functioning, which could have dire long-term effects if the surviving client sessions make a lot of data changes.</p>"
  ],
  [
    "<p>Avoid recursive consumption of stack space while processing signals in the postmaster (Tom Lane)</p>",
    "<p>Heavy use of parallel processing has been observed to cause postmaster crashes due to too many concurrent signals requesting creation of a parallel worker process.</p>"
  ],
  [
    "<p>Avoid running <span class=\"SYSTEMITEM\">atexit</span> handlers when exiting due to SIGQUIT (Kyotaro Horiguchi, Tom Lane)</p>",
    "<p>Most server processes followed this practice already, but the archiver process was overlooked. Backends that were still waiting for a client startup packet got it wrong, too.</p>"
  ],
  [
    "<p>Avoid misoptimization of subquery qualifications that reference apparently-constant grouping columns (Tom Lane)</p>",
    "<p>A <span class=\"QUOTE\">\"constant\"</span> subquery output column isn't really constant if it is a grouping column that appears in only some of the grouping sets.</p>"
  ],
  [
    "<p>Avoid failure when SQL function inlining changes the shape of a potentially-hashable subplan comparison expression (Tom Lane)</p>"
  ],
  [
    "<p>While building or re-building an index, tolerate the appearance of new HOT chains due to concurrent updates (Anastasia Lubennikova, Álvaro Herrera)</p>",
    "<p>This oversight could lead to <span class=\"QUOTE\">\"failed to find parent tuple for heap-only tuple\"</span> errors.</p>"
  ],
  [
    "<p>Ensure that data is detoasted before being inserted into a BRIN index (Tomas Vondra)</p>",
    "<p>Index entries are not supposed to contain out-of-line TOAST pointers, but BRIN didn't get that memo. This could lead to errors like <span class=\"QUOTE\">\"missing chunk number 0 for toast value NNN\"</span>. (If you are faced with such an error from an existing index, <tt class=\"COMMAND\">REINDEX</tt> should be enough to fix it.)</p>"
  ],
  [
    "<p>Handle concurrent desummarization correctly during BRIN index scans (Alexander Lakhin, Álvaro Herrera)</p>",
    "<p>Previously, if a page range was desummarized at just the wrong time, an index scan might falsely raise an error indicating index corruption.</p>"
  ],
  [
    "<p>Fix rare <span class=\"QUOTE\">\"lost saved point in index\"</span> errors in scans of multicolumn GIN indexes (Tom Lane)</p>"
  ],
  [
    "<p>Fix use-after-free hazard when an event trigger monitors an <tt class=\"COMMAND\">ALTER TABLE</tt> operation (Jehan-Guillaume de Rorthais)</p>"
  ],
  [
    "<p>Fix incorrect error message about inconsistent moving-aggregate data types (Jeff Janes)</p>"
  ],
  [
    "<p>Avoid lockup when a parallel worker reports a very long error message (Vignesh C)</p>"
  ],
  [
    "<p>Avoid unnecessary failure when transferring very large payloads through shared memory queues (Markus Wanner)</p>"
  ],
  [
    "<p>Fix relation cache memory leaks with RLS policies (Tom Lane)</p>"
  ],
  [
    "<p>Fix small memory leak when SIGHUP processing decides that a new GUC variable value cannot be applied without a restart (Tom Lane)</p>"
  ],
  [
    "<p>Make <span class=\"APPLICATION\">libpq</span> support arbitrary-length lines in <tt class=\"FILENAME\">.pgpass</tt> files (Tom Lane)</p>",
    "<p>This is mostly useful to allow using very long security tokens as passwords.</p>"
  ],
  [
    "<p>In <span class=\"APPLICATION\">libpq</span> for Windows, call <code class=\"FUNCTION\">WSAStartup()</code> once per process and <code class=\"FUNCTION\">WSACleanup()</code> not at all (Tom Lane, Alexander Lakhin)</p>",
    "<p>Previously, <span class=\"APPLICATION\">libpq</span> invoked <code class=\"FUNCTION\">WSAStartup()</code> at connection start and <code class=\"FUNCTION\">WSACleanup()</code> at connection cleanup. However, it appears that calling <code class=\"FUNCTION\">WSACleanup()</code> can interfere with other program operations; notably, we have observed rare failures to emit expected output to stdout. There appear to be no ill effects from omitting the call, so do that. (This also eliminates a performance issue from repeated DLL loads and unloads when a program performs a series of database connections.)</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">ecpg</span> library's per-thread initialization logic for Windows (Tom Lane, Alexander Lakhin)</p>",
    "<p>Multi-threaded <span class=\"APPLICATION\">ecpg</span> applications could suffer rare misbehavior due to incorrect locking.</p>"
  ],
  [
    "<p>On Windows, make <span class=\"APPLICATION\">psql</span> read the output of a backtick command in text mode, not binary mode (Tom Lane)</p>",
    "<p>This ensures proper handling of newlines.</p>"
  ],
  [
    "<p>Ensure that <span class=\"APPLICATION\">pg_dump</span> collects per-column information about extension configuration tables (Fabrízio de Royes Mello, Tom Lane)</p>",
    "<p>Failure to do this led to crashes when specifying <tt class=\"OPTION\">--inserts</tt>, or underspecified (though usually correct) <tt class=\"COMMAND\">COPY</tt> commands when using <tt class=\"COMMAND\">COPY</tt> to reload the tables' data.</p>"
  ],
  [
    "<p>Make <span class=\"APPLICATION\">pg_upgrade</span> check for pre-existence of tablespace directories in the target cluster (Bruce Momjian)</p>"
  ],
  [
    "<p>Fix potential memory leak in <tt class=\"FILENAME\">contrib/pgcrypto</tt> (Michael Paquier)</p>"
  ],
  [
    "<p>Add check for an unlikely failure case in <tt class=\"FILENAME\">contrib/pgcrypto</tt> (Daniel Gustafsson)</p>"
  ],
  [
    "<p>Use <tt class=\"LITERAL\">return</tt> not <tt class=\"LITERAL\">exit()</tt> in <span class=\"APPLICATION\">configure</span>'s test programs (Peter Eisentraut)</p>",
    "<p>This avoids failures with pickier compilers.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"APPLICATION\">tzdata</span> release 2020d for DST law changes in Fiji, Morocco, Palestine, the Canadian Yukon, Macquarie Island, and Casey Station (Antarctica); plus historical corrections for France, Hungary, Monaco, and Palestine.</p>"
  ],
  [
    "<p>Sync our copy of the timezone library with IANA tzcode release 2020d (Tom Lane)</p>",
    "<p>This absorbs upstream's change of <span class=\"APPLICATION\">zic</span>'s default output option from <span class=\"QUOTE\">\"fat\"</span> to <span class=\"QUOTE\">\"slim\"</span>. That's just cosmetic for our purposes, as we continue to select the <span class=\"QUOTE\">\"fat\"</span> mode in pre-v13 branches. This change also ensures that <code class=\"FUNCTION\">strftime()</code> does not change <tt class=\"VARNAME\">errno</tt> unless it fails.</p>"
  ]
]