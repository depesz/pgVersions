[
  [
    "<p>Add new configuration parameter <code class=\"varname\">ssl_renegotiation_limit</code> to control how\n          often we do session key renegotiation for an SSL\n          connection (Magnus)</p>",
    "<p>This can be set to zero to disable renegotiation\n          completely, which may be required if a broken SSL library\n          is used. In particular, some vendors are shipping stopgap\n          patches for CVE-2009-3555 that cause renegotiation\n          attempts to fail.</p>"
  ],
  [
    "<p>Fix possible deadlock during backend startup (Tom)</p>"
  ],
  [
    "<p>Fix possible crashes due to not handling errors during\n          relcache reload cleanly (Tom)</p>"
  ],
  [
    "<p>Fix possible crash due to use of dangling pointer to a\n          cached plan (Tatsuo)</p>"
  ],
  [
    "<p>Fix possible crash due to overenthusiastic\n          invalidation of cached plan for <code class=\"command\">ROLLBACK</code> (Tom)</p>"
  ],
  [
    "<p>Fix possible crashes when trying to recover from a\n          failure in subtransaction start (Tom)</p>"
  ],
  [
    "<p>Fix server memory leak associated with use of\n          savepoints and a client encoding different from server's\n          encoding (Tom)</p>"
  ],
  [
    "<p>Fix incorrect WAL data emitted during end-of-recovery\n          cleanup of a GIST index page split (Yoichi Hirai)</p>",
    "<p>This would result in index corruption, or even more\n          likely an error during WAL replay, if we were unlucky\n          enough to crash during end-of-recovery cleanup after\n          having completed an incomplete GIST insertion.</p>"
  ],
  [
    "<p>Fix bug in WAL redo cleanup method for GIN indexes\n          (Heikki)</p>"
  ],
  [
    "<p>Fix incorrect comparison of scan key in GIN index\n          search (Teodor)</p>"
  ],
  [
    "<p>Make <code class=\"function\">substring()</code> for\n          <code class=\"type\">bit</code> types treat any negative\n          length as meaning <span class=\"quote\">&#x201C;<span class=\"quote\">all the rest of the string</span>&#x201D;</span>\n          (Tom)</p>",
    "<p>The previous coding treated only -1 that way, and\n          would produce an invalid result value for other negative\n          values, possibly leading to a crash (CVE-2010-0442).</p>"
  ],
  [
    "<p>Fix integer-to-bit-string conversions to handle the\n          first fractional byte correctly when the output bit width\n          is wider than the given integer by something other than a\n          multiple of 8 bits (Tom)</p>"
  ],
  [
    "<p>Fix some cases of pathologically slow regular\n          expression matching (Tom)</p>"
  ],
  [
    "<p>Fix bug occurring when trying to inline a SQL function\n          that returns a set of a composite type that contains\n          dropped columns (Tom)</p>"
  ],
  [
    "<p>Fix bug with trying to update a field of an element of\n          a composite-type array column (Tom)</p>"
  ],
  [
    "<p>Avoid failure when <code class=\"command\">EXPLAIN</code> has to print a FieldStore or\n          assignment ArrayRef expression (Tom)</p>",
    "<p>These cases can arise now that <code class=\"command\">EXPLAIN VERBOSE</code> tries to print plan node\n          target lists.</p>"
  ],
  [
    "<p>Avoid an unnecessary coercion failure in some cases\n          where an undecorated literal string appears in a subquery\n          within <code class=\"command\">UNION</code>/<code class=\"command\">INTERSECT</code>/<code class=\"command\">EXCEPT</code> (Tom)</p>",
    "<p>This fixes a regression for some cases that worked\n          before 8.4.</p>"
  ],
  [
    "<p>Avoid undesirable rowtype compatibility check failures\n          in some cases where a whole-row Var has a rowtype that\n          contains dropped columns (Tom)</p>"
  ],
  [
    "<p>Fix the <code class=\"literal\">STOP WAL LOCATION</code>\n          entry in backup history files to report the next WAL\n          segment's name when the end location is exactly at a\n          segment boundary (Itagaki Takahiro)</p>"
  ],
  [
    "<p>Always pass the catalog ID to an option validator\n          function specified in <code class=\"command\">CREATE\n          FOREIGN DATA WRAPPER</code> (Martin Pihlak)</p>"
  ],
  [
    "<p>Fix some more cases of temporary-file leakage\n          (Heikki)</p>",
    "<p>This corrects a problem introduced in the previous\n          minor release. One case that failed is when a plpgsql\n          function returning set is called within another\n          function's exception handler.</p>"
  ],
  [
    "<p>Add support for doing <code class=\"literal\">FULL JOIN\n          ON FALSE</code> (Tom)</p>",
    "<p>This prevents a regression from pre-8.4 releases for\n          some queries that can now be simplified to a\n          constant-false join condition.</p>"
  ],
  [
    "<p>Improve constraint exclusion processing of\n          boolean-variable cases, in particular make it possible to\n          exclude a partition that has a <span class=\"quote\">&#x201C;<span class=\"quote\">bool_column =\n          false</span>&#x201D;</span> constraint (Tom)</p>"
  ],
  [
    "<p>Prevent treating an <code class=\"literal\">INOUT</code>\n          cast as representing binary compatibility (Heikki)</p>"
  ],
  [
    "<p>Include column name in the message when warning about\n          inability to grant or revoke column-level privileges\n          (Stephen Frost)</p>",
    "<p>This is more useful than before and helps to prevent\n          confusion when a <code class=\"command\">REVOKE</code>\n          generates multiple messages, which formerly appeared to\n          be duplicates.</p>"
  ],
  [
    "<p>When reading <code class=\"filename\">pg_hba.conf</code>\n          and related files, do not treat <code class=\"literal\">@something</code> as a file inclusion request\n          if the <code class=\"literal\">@</code> appears inside\n          quote marks; also, never treat <code class=\"literal\">@</code> by itself as a file inclusion request\n          (Tom)</p>",
    "<p>This prevents erratic behavior if a role or database\n          name starts with <code class=\"literal\">@</code>. If you\n          need to include a file whose path name contains spaces,\n          you can still do so, but you must write <code class=\"literal\">@\"/path to/file\"</code> rather than putting the\n          quotes around the whole construct.</p>"
  ],
  [
    "<p>Prevent infinite loop on some platforms if a directory\n          is named as an inclusion target in <code class=\"filename\">pg_hba.conf</code> and related files (Tom)</p>"
  ],
  [
    "<p>Fix possible infinite loop if <code class=\"function\">SSL_read</code> or <code class=\"function\">SSL_write</code> fails without setting\n          <code class=\"varname\">errno</code> (Tom)</p>",
    "<p>This is reportedly possible with some Windows versions\n          of <span class=\"application\">openssl</span>.</p>"
  ],
  [
    "<p>Disallow <acronym class=\"acronym\">GSSAPI</acronym>\n          authentication on local connections, since it requires a\n          hostname to function correctly (Magnus)</p>"
  ],
  [
    "<p>Protect <span class=\"application\">ecpg</span> against\n          applications freeing strings unexpectedly (Michael)</p>"
  ],
  [
    "<p>Make <span class=\"application\">ecpg</span> report the\n          proper SQLSTATE if the connection disappears\n          (Michael)</p>"
  ],
  [
    "<p>Fix translation of cell contents in <span class=\"application\">psql</span> <code class=\"literal\">\\d</code>\n          output (Heikki)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">psql</span>'s\n          <code class=\"literal\">numericlocale</code> option to not\n          format strings it shouldn't in latex and troff output\n          formats (Heikki)</p>"
  ],
  [
    "<p>Fix a small per-query memory leak in <span class=\"application\">psql</span> (Tom)</p>"
  ],
  [
    "<p>Make <span class=\"application\">psql</span> return the\n          correct exit status (3) when <code class=\"literal\">ON_ERROR_STOP</code> and <code class=\"literal\">--single-transaction</code> are both specified\n          and an error occurs during the implied <code class=\"command\">COMMIT</code> (Bruce)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_dump</span>'s output\n          of permissions for foreign servers (Heikki)</p>"
  ],
  [
    "<p>Fix possible crash in parallel <span class=\"application\">pg_restore</span> due to out-of-range\n          dependency IDs (Tom)</p>"
  ],
  [
    "<p>Fix plpgsql failure in one case where a composite\n          column is set to NULL (Tom)</p>"
  ],
  [
    "<p>Fix possible failure when calling PL/Perl functions\n          from PL/PerlU or vice versa (Tim Bunce)</p>"
  ],
  [
    "<p>Add <code class=\"literal\">volatile</code> markings in\n          PL/Python to avoid possible compiler-specific misbehavior\n          (Zdenek Kotala)</p>"
  ],
  [
    "<p>Ensure PL/Tcl initializes the Tcl interpreter fully\n          (Tom)</p>",
    "<p>The only known symptom of this oversight is that the\n          Tcl <code class=\"literal\">clock</code> command misbehaves\n          if using Tcl 8.5 or later.</p>"
  ],
  [
    "<p>Prevent <code class=\"function\">ExecutorEnd</code> from\n          being run on portals created within a failed transaction\n          or subtransaction (Tom)</p>",
    "<p>This is known to cause issues when using <code class=\"filename\">contrib/auto_explain</code>.</p>"
  ],
  [
    "<p>Prevent crash in <code class=\"filename\">contrib/dblink</code> when too many key\n          columns are specified to a <code class=\"function\">dblink_build_sql_*</code> function (Rushabh\n          Lathia, Joe Conway)</p>"
  ],
  [
    "<p>Allow zero-dimensional arrays in <code class=\"filename\">contrib/ltree</code> operations (Tom)</p>",
    "<p>This case was formerly rejected as an error, but it's\n          more convenient to treat it the same as a zero-element\n          array. In particular this avoids unnecessary failures\n          when an <code class=\"type\">ltree</code> operation is\n          applied to the result of <code class=\"literal\">ARRAY(SELECT ...)</code> and the sub-select\n          returns no rows.</p>"
  ],
  [
    "<p>Fix assorted crashes in <code class=\"filename\">contrib/xml2</code> caused by sloppy memory\n          management (Tom)</p>"
  ],
  [
    "<p>Make building of <code class=\"filename\">contrib/xml2</code> more robust on Windows\n          (Andrew)</p>"
  ],
  [
    "<p>Fix race condition in Windows signal handling (Radu\n          Ilie)</p>",
    "<p>One known symptom of this bug is that rows in\n          <code class=\"structname\">pg_listener</code> could be\n          dropped under heavy load.</p>"
  ],
  [
    "<p>Make the configure script report failure if the C\n          compiler does not provide a working 64-bit integer\n          datatype (Tom)</p>",
    "<p>This case has been broken for some time, and no longer\n          seems worth supporting, so just reject it at configure\n          time instead.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2010e for DST law\n          changes in Bangladesh, Chile, Fiji, Mexico, Paraguay,\n          Samoa.</p>"
  ]
]