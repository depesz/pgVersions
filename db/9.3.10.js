[
  [
    "<p>Guard against stack overflows in <code class=\"type\">json</code> parsing (Oskari Saarenmaa)</p>",
    "<p>If an application constructs PostgreSQL <code class=\"type\">json</code> or <code class=\"type\">jsonb</code>\n          values from arbitrary user input, the application's users\n          can reliably crash the PostgreSQL server, causing\n          momentary denial of service. (CVE-2015-5289)</p>"
  ],
  [
    "<p>Fix <code class=\"filename\">contrib/pgcrypto</code> to\n          detect and report too-short <code class=\"function\">crypt()</code> salts (Josh Kupershmidt)</p>",
    "<p>Certain invalid salt arguments crashed the server or\n          disclosed a few bytes of server memory. We have not ruled\n          out the viability of attacks that arrange for presence of\n          confidential information in the disclosed bytes, but they\n          seem unlikely. (CVE-2015-5288)</p>"
  ],
  [
    "<p>Fix subtransaction cleanup after a portal (cursor)\n          belonging to an outer subtransaction fails (Tom Lane,\n          Michael Paquier)</p>",
    "<p>A function executed in an outer-subtransaction cursor\n          could cause an assertion failure or crash by referencing\n          a relation created within an inner subtransaction.</p>"
  ],
  [
    "<p>Ensure all relations referred to by an updatable view\n          are properly locked during an update statement (Dean\n          Rasheed)</p>"
  ],
  [
    "<p>Fix insertion of relations into the relation cache\n          <span class=\"quote\">&#x201C;<span class=\"quote\">init\n          file</span>&#x201D;</span> (Tom Lane)</p>",
    "<p>An oversight in a patch in the most recent minor\n          releases caused <code class=\"structname\">pg_trigger_tgrelid_tgname_index</code> to be\n          omitted from the init file. Subsequent sessions detected\n          this, then deemed the init file to be broken and silently\n          ignored it, resulting in a significant degradation in\n          session startup time. In addition to fixing the bug,\n          install some guards so that any similar future mistake\n          will be more obvious.</p>"
  ],
  [
    "<p>Avoid O(N^2) behavior when inserting many tuples into\n          a SPI query result (Neil Conway)</p>"
  ],
  [
    "<p>Improve <code class=\"command\">LISTEN</code> startup\n          time when there are many unread notifications (Matt\n          Newell)</p>"
  ],
  [
    "<p>Fix performance problem when a session alters large\n          numbers of foreign key constraints (Jan Wieck, Tom\n          Lane)</p>",
    "<p>This was seen primarily when restoring <span class=\"application\">pg_dump</span> output for databases with\n          many thousands of tables.</p>"
  ],
  [
    "<p>Disable SSL renegotiation by default (Michael Paquier,\n          Andres Freund)</p>",
    "<p>While use of SSL renegotiation is a good idea in\n          theory, we have seen too many bugs in practice, both in\n          the underlying OpenSSL library and in our usage of it.\n          Renegotiation will be removed entirely in 9.5 and later.\n          In the older branches, just change the default value of\n          <code class=\"varname\">ssl_renegotiation_limit</code> to\n          zero (disabled).</p>"
  ],
  [
    "<p>Lower the minimum values of the <code class=\"literal\">*_freeze_max_age</code> parameters (Andres\n          Freund)</p>",
    "<p>This is mainly to make tests of related behavior less\n          time-consuming, but it may also be of value for\n          installations with limited disk space.</p>"
  ],
  [
    "<p>Limit the maximum value of <code class=\"varname\">wal_buffers</code> to 2GB to avoid server\n          crashes (Josh Berkus)</p>"
  ],
  [
    "<p>Avoid logging complaints when a parameter that can\n          only be set at server start appears multiple times in\n          <code class=\"filename\">postgresql.conf</code>, and fix\n          counting of line numbers after an <code class=\"literal\">include_dir</code> directive (Tom Lane)</p>"
  ],
  [
    "<p>Fix rare internal overflow in multiplication of\n          <code class=\"type\">numeric</code> values (Dean\n          Rasheed)</p>"
  ],
  [
    "<p>Guard against hard-to-reach stack overflows involving\n          record types, range types, <code class=\"type\">json</code>, <code class=\"type\">jsonb</code>,\n          <code class=\"type\">tsquery</code>, <code class=\"type\">ltxtquery</code> and <code class=\"type\">query_int</code> (Noah Misch)</p>"
  ],
  [
    "<p>Fix handling of <code class=\"literal\">DOW</code> and\n          <code class=\"literal\">DOY</code> in datetime input (Greg\n          Stark)</p>",
    "<p>These tokens aren't meant to be used in datetime\n          values, but previously they resulted in opaque internal\n          error messages rather than <span class=\"quote\">&#x201C;<span class=\"quote\">invalid input\n          syntax</span>&#x201D;</span>.</p>"
  ],
  [
    "<p>Add more query-cancel checks to regular expression\n          matching (Tom Lane)</p>"
  ],
  [
    "<p>Add recursion depth protections to regular expression,\n          <code class=\"literal\">SIMILAR TO</code>, and <code class=\"literal\">LIKE</code> matching (Tom Lane)</p>",
    "<p>Suitable search patterns and a low stack depth limit\n          could lead to stack-overrun crashes.</p>"
  ],
  [
    "<p>Fix potential infinite loop in regular expression\n          execution (Tom Lane)</p>",
    "<p>A search pattern that can apparently match a\n          zero-length string, but actually doesn't match because of\n          a back reference, could lead to an infinite loop.</p>"
  ],
  [
    "<p>In regular expression execution, correctly record\n          match data for capturing parentheses within a quantifier\n          even when the match is zero-length (Tom Lane)</p>"
  ],
  [
    "<p>Fix low-memory failures in regular expression\n          compilation (Andreas Seltenreich)</p>"
  ],
  [
    "<p>Fix low-probability memory leak during regular\n          expression execution (Tom Lane)</p>"
  ],
  [
    "<p>Fix rare low-memory failure in lock cleanup during\n          transaction abort (Tom Lane)</p>"
  ],
  [
    "<p>Fix <span class=\"quote\">&#x201C;<span class=\"quote\">unexpected out-of-memory situation during\n          sort</span>&#x201D;</span> errors when using tuplestores with\n          small <code class=\"varname\">work_mem</code> settings (Tom\n          Lane)</p>"
  ],
  [
    "<p>Fix very-low-probability stack overrun in <code class=\"function\">qsort</code> (Tom Lane)</p>"
  ],
  [
    "<p>Fix <span class=\"quote\">&#x201C;<span class=\"quote\">invalid\n          memory alloc request size</span>&#x201D;</span> failure in hash\n          joins with large <code class=\"varname\">work_mem</code>\n          settings (Tomas Vondra, Tom Lane)</p>"
  ],
  [
    "<p>Fix assorted planner bugs (Tom Lane)</p>",
    "<p>These mistakes could lead to incorrect query plans\n          that would give wrong answers, or to assertion failures\n          in assert-enabled builds, or to odd planner errors such\n          as <span class=\"quote\">&#x201C;<span class=\"quote\">could not\n          devise a query plan for the given query</span>&#x201D;</span>,\n          <span class=\"quote\">&#x201C;<span class=\"quote\">could not find\n          pathkey item to sort</span>&#x201D;</span>, <span class=\"quote\">&#x201C;<span class=\"quote\">plan should not reference\n          subplan's variable</span>&#x201D;</span>, or <span class=\"quote\">&#x201C;<span class=\"quote\">failed to assign all\n          NestLoopParams to plan nodes</span>&#x201D;</span>. Thanks are\n          due to Andreas Seltenreich and Piotr Stefaniak for fuzz\n          testing that exposed these problems.</p>"
  ],
  [
    "<p>Improve planner's performance for <code class=\"command\">UPDATE</code>/<code class=\"command\">DELETE</code> on large inheritance sets (Tom\n          Lane, Dean Rasheed)</p>"
  ],
  [
    "<p>Ensure standby promotion trigger files are removed at\n          postmaster startup (Michael Paquier, Fujii Masao)</p>",
    "<p>This prevents unwanted promotion from occurring if\n          these files appear in a database backup that is used to\n          initialize a new standby server.</p>"
  ],
  [
    "<p>During postmaster shutdown, ensure that per-socket\n          lock files are removed and listen sockets are closed\n          before we remove the <code class=\"filename\">postmaster.pid</code> file (Tom Lane)</p>",
    "<p>This avoids race-condition failures if an external\n          script attempts to start a new postmaster as soon as\n          <code class=\"literal\">pg_ctl stop</code> returns.</p>"
  ],
  [
    "<p>Fix postmaster's handling of a startup-process crash\n          during crash recovery (Tom Lane)</p>",
    "<p>If, during a crash recovery cycle, the startup process\n          crashes without having restored database consistency,\n          we'd try to launch a new startup process, which typically\n          would just crash again, leading to an infinite loop.</p>"
  ],
  [
    "<p>Make emergency autovacuuming for multixact wraparound\n          more robust (Andres Freund)</p>"
  ],
  [
    "<p>Do not print a <code class=\"literal\">WARNING</code>\n          when an autovacuum worker is already gone when we attempt\n          to signal it, and reduce log verbosity for such signals\n          (Tom Lane)</p>"
  ],
  [
    "<p>Prevent autovacuum launcher from sleeping unduly long\n          if the server clock is moved backwards a large amount\n          (&#xC1;lvaro Herrera)</p>"
  ],
  [
    "<p>Ensure that cleanup of a GIN index's\n          pending-insertions list is interruptable by cancel\n          requests (Jeff Janes)</p>"
  ],
  [
    "<p>Allow all-zeroes pages in GIN indexes to be reused\n          (Heikki Linnakangas)</p>",
    "<p>Such a page might be left behind after a crash.</p>"
  ],
  [
    "<p>Fix handling of all-zeroes pages in SP-GiST indexes\n          (Heikki Linnakangas)</p>",
    "<p><code class=\"command\">VACUUM</code> attempted to\n          recycle such pages, but did so in a way that wasn't\n          crash-safe.</p>"
  ],
  [
    "<p>Fix off-by-one error that led to otherwise-harmless\n          warnings about <span class=\"quote\">&#x201C;<span class=\"quote\">apparent wraparound</span>&#x201D;</span> in\n          subtrans/multixact truncation (Thomas Munro)</p>"
  ],
  [
    "<p>Fix misreporting of <code class=\"command\">CONTINUE</code> and <code class=\"command\">MOVE</code> statement types in <span class=\"application\">PL/pgSQL</span>'s error context messages\n          (Pavel Stehule, Tom Lane)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">PL/Perl</span> to handle\n          non-<acronym class=\"acronym\">ASCII</acronym> error\n          message texts correctly (Alex Hunsaker)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">PL/Python</span> crash\n          when returning the string representation of a\n          <code class=\"type\">record</code> result (Tom Lane)</p>"
  ],
  [
    "<p>Fix some places in <span class=\"application\">PL/Tcl</span> that neglected to check for\n          failure of <code class=\"function\">malloc()</code> calls\n          (Michael Paquier, &#xC1;lvaro Herrera)</p>"
  ],
  [
    "<p>In <code class=\"filename\">contrib/isn</code>, fix\n          output of ISBN-13 numbers that begin with 979 (Fabien\n          Coelho)</p>",
    "<p>EANs beginning with 979 (but not 9790) are considered\n          ISBNs, but they must be printed in the new 13-digit\n          format, not the 10-digit format.</p>"
  ],
  [
    "<p>Improve <code class=\"filename\">contrib/postgres_fdw</code>'s handling of\n          collation-related decisions (Tom Lane)</p>",
    "<p>The main user-visible effect is expected to be that\n          comparisons involving <code class=\"type\">varchar</code>\n          columns will be sent to the remote server for execution\n          in more cases than before.</p>"
  ],
  [
    "<p>Improve <span class=\"application\">libpq</span>'s\n          handling of out-of-memory conditions (Michael Paquier,\n          Heikki Linnakangas)</p>"
  ],
  [
    "<p>Fix memory leaks and missing out-of-memory checks in\n          <span class=\"application\">ecpg</span> (Michael\n          Paquier)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">psql</span>'s code for\n          locale-aware formatting of numeric output (Tom Lane)</p>",
    "<p>The formatting code invoked by <code class=\"literal\">\\pset numericlocale on</code> did the wrong\n          thing for some uncommon cases such as numbers with an\n          exponent but no decimal point. It could also mangle\n          already-localized output from the <code class=\"type\">money</code> data type.</p>"
  ],
  [
    "<p>Prevent crash in <span class=\"application\">psql</span>'s <code class=\"command\">\\c</code> command when there is no current\n          connection (Noah Misch)</p>"
  ],
  [
    "<p>Make <span class=\"application\">pg_dump</span> handle\n          inherited <code class=\"literal\">NOT VALID</code> check\n          constraints correctly (Tom Lane)</p>"
  ],
  [
    "<p>Fix selection of default <span class=\"application\">zlib</span> compression level in\n          <span class=\"application\">pg_dump</span>'s directory\n          output format (Andrew Dunstan)</p>"
  ],
  [
    "<p>Ensure that temporary files created during a\n          <span class=\"application\">pg_dump</span> run with\n          <acronym class=\"acronym\">tar</acronym>-format output are\n          not world-readable (Michael Paquier)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_dump</span> and\n          <span class=\"application\">pg_upgrade</span> to support\n          cases where the <code class=\"literal\">postgres</code> or\n          <code class=\"literal\">template1</code> database is in a\n          non-default tablespace (Marti Raudsepp, Bruce\n          Momjian)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_dump</span> to handle\n          object privileges sanely when dumping from a server too\n          old to have a particular privilege type (Tom Lane)</p>",
    "<p>When dumping data types from pre-9.2 servers, and when\n          dumping functions or procedural languages from pre-7.3\n          servers, <span class=\"application\">pg_dump</span> would\n          produce <code class=\"command\">GRANT</code>/<code class=\"command\">REVOKE</code> commands that revoked the owner's\n          grantable privileges and instead granted all privileges\n          to <code class=\"literal\">PUBLIC</code>. Since the\n          privileges involved are just <code class=\"literal\">USAGE</code> and <code class=\"literal\">EXECUTE</code>, this isn't a security problem,\n          but it's certainly a surprising representation of the\n          older systems' behavior. Fix it to leave the default\n          privilege state alone in these cases.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_dump</span> to dump\n          shell types (Tom Lane)</p>",
    "<p>Shell types (that is, not-yet-fully-defined types)\n          aren't useful for much, but nonetheless <span class=\"application\">pg_dump</span> should dump them.</p>"
  ],
  [
    "<p>Fix assorted minor memory leaks in <span class=\"application\">pg_dump</span> and other client-side\n          programs (Michael Paquier)</p>"
  ],
  [
    "<p>Fix spinlock assembly code for PPC hardware to be\n          compatible with <acronym class=\"acronym\">AIX</acronym>'s\n          native assembler (Tom Lane)</p>",
    "<p>Building with <span class=\"application\">gcc</span>\n          didn't work if <span class=\"application\">gcc</span> had\n          been configured to use the native assembler, which is\n          becoming more common.</p>"
  ],
  [
    "<p>On <acronym class=\"acronym\">AIX</acronym>, test the\n          <code class=\"literal\">-qlonglong</code> compiler option\n          rather than just assuming it's safe to use (Noah\n          Misch)</p>"
  ],
  [
    "<p>On <acronym class=\"acronym\">AIX</acronym>, use\n          <code class=\"literal\">-Wl,-brtllib</code> link option to\n          allow symbols to be resolved at runtime (Noah Misch)</p>",
    "<p>Perl relies on this ability in 5.8.0 and later.</p>"
  ],
  [
    "<p>Avoid use of inline functions when compiling with\n          32-bit <span class=\"application\">xlc</span>, due to\n          compiler bugs (Noah Misch)</p>"
  ],
  [
    "<p>Use <code class=\"filename\">librt</code> for\n          <code class=\"function\">sched_yield()</code> when\n          necessary, which it is on some Solaris versions (Oskari\n          Saarenmaa)</p>"
  ],
  [
    "<p>Fix Windows <code class=\"filename\">install.bat</code>\n          script to handle target directory names that contain\n          spaces (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Make the numeric form of the <span class=\"productname\">PostgreSQL</span> version number (e.g.,\n          <code class=\"literal\">90405</code>) readily available to\n          extension Makefiles, as a variable named <code class=\"varname\">VERSION_NUM</code> (Michael Paquier)</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2015g for DST law\n          changes in Cayman Islands, Fiji, Moldova, Morocco,\n          Norfolk Island, North Korea, Turkey, and Uruguay. There\n          is a new zone name <code class=\"literal\">America/Fort_Nelson</code> for the Canadian\n          Northern Rockies.</p>"
  ]
]