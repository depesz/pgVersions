[
  [
    "<p>Fix bugs in <code class=\"literal\">information_schema.referential_constraints</code>\n          view (Tom Lane)</p>",
    "<p>This view was being insufficiently careful about\n          matching the foreign-key constraint to the depended-on\n          primary or unique key constraint. That could result in\n          failure to show a foreign key constraint at all, or\n          showing it multiple times, or claiming that it depends on\n          a different constraint than the one it really does.</p>",
    "<p>Since the view definition is installed by <span class=\"application\">initdb</span>, merely upgrading will not\n          fix the problem. If you need to fix this in an existing\n          installation, you can (as a superuser) drop the\n          <code class=\"literal\">information_schema</code> schema\n          then re-create it by sourcing <code class=\"filename\"><em class=\"replaceable\"><code>SHAREDIR</code></em>/information_schema.sql</code>.\n          (Run <code class=\"literal\">pg_config --sharedir</code> if\n          you're uncertain where <em class=\"replaceable\"><code>SHAREDIR</code></em> is.) This must\n          be repeated in each database to be fixed.</p>"
  ],
  [
    "<p>Fix TOAST-related data corruption during <code class=\"literal\">CREATE TABLE dest AS SELECT * FROM src</code>\n          or <code class=\"literal\">INSERT INTO dest SELECT * FROM\n          src</code> (Tom Lane)</p>",
    "<p>If a table has been modified by <code class=\"command\">ALTER TABLE ADD COLUMN</code>, attempts to copy\n          its data verbatim to another table could produce corrupt\n          results in certain corner cases. The problem can only\n          manifest in this precise form in 8.4 and later, but we\n          patched earlier versions as well in case there are other\n          code paths that could trigger the same bug.</p>"
  ],
  [
    "<p>Fix race condition during toast table access from\n          stale syscache entries (Tom Lane)</p>",
    "<p>The typical symptom was transient errors like\n          <span class=\"quote\">&#x201C;<span class=\"quote\">missing chunk\n          number 0 for toast value NNNNN in\n          pg_toast_2619</span>&#x201D;</span>, where the cited toast table\n          would always belong to a system catalog.</p>"
  ],
  [
    "<p>Make <code class=\"function\">DatumGetInetP()</code>\n          unpack inet datums that have a 1-byte header, and add a\n          new macro, <code class=\"function\">DatumGetInetPP()</code>, that does not (Heikki\n          Linnakangas)</p>",
    "<p>This change affects no core code, but might prevent\n          crashes in add-on code that expects <code class=\"function\">DatumGetInetP()</code> to produce an unpacked\n          datum as per usual convention.</p>"
  ],
  [
    "<p>Improve locale support in <code class=\"type\">money</code> type's input and output (Tom\n          Lane)</p>",
    "<p>Aside from not supporting all standard <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-client.html#GUC-LC-MONETARY\"><code class=\"varname\">lc_monetary</code></a> formatting options, the\n          input and output functions were inconsistent, meaning\n          there were locales in which dumped <code class=\"type\">money</code> values could not be re-read.</p>"
  ],
  [
    "<p>Don't let <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-compatible.html#GUC-TRANSFORM-NULL-EQUALS\">\n          <code class=\"varname\">transform_null_equals</code></a>\n          affect <code class=\"literal\">CASE foo WHEN NULL\n          ...</code> constructs (Heikki Linnakangas)</p>",
    "<p><code class=\"varname\">transform_null_equals</code> is\n          only supposed to affect <code class=\"literal\">foo =\n          NULL</code> expressions written directly by the user, not\n          equality checks generated internally by this form of\n          <code class=\"literal\">CASE</code>.</p>"
  ],
  [
    "<p>Change foreign-key trigger creation order to better\n          support self-referential foreign keys (Tom Lane)</p>",
    "<p>For a cascading foreign key that references its own\n          table, a row update will fire both the <code class=\"literal\">ON UPDATE</code> trigger and the <code class=\"literal\">CHECK</code> trigger as one event. The\n          <code class=\"literal\">ON UPDATE</code> trigger must\n          execute first, else the <code class=\"literal\">CHECK</code> will check a non-final state of\n          the row and possibly throw an inappropriate error.\n          However, the firing order of these triggers is determined\n          by their names, which generally sort in creation order\n          since the triggers have auto-generated names following\n          the convention <span class=\"quote\">&#x201C;<span class=\"quote\">RI_ConstraintTrigger_NNNN</span>&#x201D;</span>. A\n          proper fix would require modifying that convention, which\n          we will do in 9.2, but it seems risky to change it in\n          existing releases. So this patch just changes the\n          creation order of the triggers. Users encountering this\n          type of error should drop and re-create the foreign key\n          constraint to get its triggers into the right order.</p>"
  ],
  [
    "<p>Avoid floating-point underflow while tracking buffer\n          allocation rate (Greg Matthews)</p>",
    "<p>While harmless in itself, on certain platforms this\n          would result in annoying kernel log messages.</p>"
  ],
  [
    "<p>Preserve blank lines within commands in <span class=\"application\">psql</span>'s command history (Robert\n          Haas)</p>",
    "<p>The former behavior could cause problems if an empty\n          line was removed from within a string literal, for\n          example.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_dump</span> to dump\n          user-defined casts between auto-generated types, such as\n          table rowtypes (Tom Lane)</p>"
  ],
  [
    "<p>Use the preferred version of <span class=\"application\">xsubpp</span> to build PL/Perl, not\n          necessarily the operating system's main copy (David\n          Wheeler and Alex Hunsaker)</p>"
  ],
  [
    "<p>Fix incorrect coding in <code class=\"filename\">contrib/dict_int</code> and <code class=\"filename\">contrib/dict_xsyn</code> (Tom Lane)</p>",
    "<p>Some functions incorrectly assumed that memory\n          returned by <code class=\"function\">palloc()</code> is\n          guaranteed zeroed.</p>"
  ],
  [
    "<p>Honor query cancel interrupts promptly in <code class=\"function\">pgstatindex()</code> (Robert Haas)</p>"
  ],
  [
    "<p>Ensure VPATH builds properly install all server header\n          files (Peter Eisentraut)</p>"
  ],
  [
    "<p>Shorten file names reported in verbose error messages\n          (Peter Eisentraut)</p>",
    "<p>Regular builds have always reported just the name of\n          the C file containing the error message call, but VPATH\n          builds formerly reported an absolute path name.</p>"
  ],
  [
    "<p>Fix interpretation of Windows timezone names for\n          Central America (Tom Lane)</p>",
    "<p>Map <span class=\"quote\">&#x201C;<span class=\"quote\">Central\n          America Standard Time</span>&#x201D;</span> to <code class=\"literal\">CST6</code>, not <code class=\"literal\">CST6CDT</code>, because DST is generally not\n          observed anywhere in Central America.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2011n for DST law\n          changes in Brazil, Cuba, Fiji, Palestine, Russia, and\n          Samoa; also historical corrections for Alaska and British\n          East Africa.</p>"
  ]
]