[
  [
    "<p>Allow <code class=\"literal\">REPLICA IDENTITY</code> to be set on an index that's not (yet) valid (Tom Lane)</p>",
    "<p>When <span class=\"application\">pg_dump</span> dumps a partitioned index that's marked <code class=\"literal\">REPLICA IDENTITY</code>, it generates a command sequence that applies <code class=\"literal\">REPLICA IDENTITY</code> before the partitioned index has been marked valid, causing restore to fail. There seems no very good reason to prohibit doing it in that order, so allow it. The marking will have no effect anyway until the index becomes valid.</p>"
  ],
  [
    "<p>Fix handling of <code class=\"literal\">DEFAULT</code> markers in rules that perform an <code class=\"command\">INSERT</code> from a multi-row <code class=\"literal\">VALUES</code> list (Dean Rasheed)</p>",
    "<p>In some cases a <code class=\"literal\">DEFAULT</code> marker would not get replaced with the proper default-value expression, leading to an <span class=\"quote\">“<span class=\"quote\">unrecognized node type</span>”</span> error.</p>"
  ],
  [
    "<p>Fix edge-case data corruption in parallel hash joins (Dmitry Astapov)</p>",
    "<p>If the final chunk of a large tuple being written out to a temporary file was exactly 32760 bytes, it would be corrupted due to a fencepost bug. The query would typically fail later with corrupted-data symptoms.</p>"
  ],
  [
    "<p>Honor non-default settings of <code class=\"varname\">checkpoint_completion_target</code> (Bharath Rupireddy)</p>",
    "<p>Internal state was not updated after a change in <code class=\"varname\">checkpoint_completion_target</code>, possibly resulting in performing checkpoint I/O faster or slower than desired, especially if that setting was changed on-the-fly.</p>"
  ],
  [
    "<p>Log the correct ending timestamp in <code class=\"varname\">recovery_target_xid</code> mode (Tom Lane)</p>",
    "<p>When ending recovery based on the <code class=\"varname\">recovery_target_xid</code> setting with <code class=\"varname\">recovery_target_inclusive</code> = <code class=\"literal\">off</code>, we printed an incorrect timestamp (always 2000-01-01) in the <span class=\"quote\">“<span class=\"quote\">recovery stopping before ... transaction</span>”</span> log message.</p>"
  ],
  [
    "<p>In extended query protocol, avoid an immediate commit after <code class=\"command\">ANALYZE</code> if we're running a pipeline (Tom Lane)</p>",
    "<p>If there's not been an explicit <code class=\"command\">BEGIN TRANSACTION</code>, <code class=\"command\">ANALYZE</code> would take it on itself to commit, which should not happen within a pipelined series of commands.</p>"
  ],
  [
    "<p>Reject cancel request packets having the wrong length (Andrey Borodin)</p>",
    "<p>The server would process a cancel request even if its length word was too small. This led to reading beyond the end of the allocated buffer. In theory that could cause a segfault, but it seems quite unlikely to happen in practice, since the buffer would have to be very close to the end of memory. The more likely outcome was a bogus log message about wrong backend PID or cancel code. Complain about the wrong length, instead.</p>"
  ],
  [
    "<p>Add recursion and looping defenses in subquery pullup (Tom Lane)</p>",
    "<p>A contrived query can result in deep recursion and unreasonable amounts of time spent trying to flatten subqueries. A proper fix for that seems unduly invasive for a back-patch, but we can at least add stack depth checks and an interrupt check to allow the query to be cancelled.</p>"
  ],
  [
    "<p>Fix partitionwise-join code to tolerate failure to produce a plan for each partition (Tom Lane)</p>",
    "<p>This could result in <span class=\"quote\">“<span class=\"quote\">could not devise a query plan for the given query</span>”</span> errors.</p>"
  ],
  [
    "<p>Limit the amount of cleanup work done by <code class=\"function\">get_actual_variable_range</code> (Simon Riggs)</p>",
    "<p>Planner runs occurring just after deletion of a large number of tuples appearing at the end of an index could expend significant amounts of work setting the <span class=\"quote\">“<span class=\"quote\">killed</span>”</span> bits for those index entries. Limit the amount of work done in any one query by giving up on this process after examining 100 heap pages. All the cleanup will still happen eventually, but without so large a performance hiccup.</p>"
  ],
  [
    "<p>Ensure that execution of full-text-search queries can be cancelled while they are performing phrase matches (Tom Lane)</p>"
  ],
  [
    "<p>Clean up the <span class=\"application\">libpq</span> connection object after a failed replication connection attempt (Andres Freund)</p>",
    "<p>The previous coding leaked the connection object. In background code paths that's pretty harmless because the calling process will give up and exit. But in commands such as <code class=\"command\">CREATE SUBSCRIPTION</code>, such a failure resulted in a small session-lifespan memory leak.</p>"
  ],
  [
    "<p>In hot-standby servers, reduce processing effort for tracking XIDs known to be active on the primary (Simon Riggs, Michail Nikolaev)</p>",
    "<p>Insufficiently-aggressive cleanup of the KnownAssignedXids array could lead to poor performance, particularly when <code class=\"varname\">max_connections</code> is set to a large value on the standby.</p>"
  ],
  [
    "<p>Fix uninitialized-memory usage in logical decoding (Masahiko Sawada)</p>",
    "<p>In certain cases, resumption of logical decoding could try to re-use XID data that had already been freed, leading to unpredictable behavior.</p>"
  ],
  [
    "<p>Avoid rare <span class=\"quote\">“<span class=\"quote\">failed to acquire cleanup lock</span>”</span> panic during WAL replay of hash-index page split operations (Robert Haas)</p>"
  ],
  [
    "<p>Advance a heap page's LSN when setting its all-visible bit during WAL replay (Jeff Davis)</p>",
    "<p>Failure to do this left the page possibly different on standby servers than the primary, and violated some other expectations about when the LSN changes. This seems only a theoretical hazard so far as <span class=\"productname\">PostgreSQL</span> itself is concerned, but it could upset third-party tools.</p>"
  ],
  [
    "<p>Prevent unsafe usage of a relation cache entry's <code class=\"structfield\">rd_smgr</code> pointer (Amul Sul)</p>",
    "<p>Remove various assumptions that <code class=\"structfield\">rd_smgr</code> would stay valid over a series of operations, by wrapping all uses of it in a function that will recompute it if needed. This prevents bugs occurring when an unexpected cache flush occurs partway through such a series.</p>"
  ],
  [
    "<p>Fix latent buffer-overrun problem in <code class=\"literal\">WaitEventSet</code> logic (Thomas Munro)</p>",
    "<p>The <code class=\"function\">epoll</code>-based and <code class=\"function\">kqueue</code>-based implementations could ask the kernel for too many events if the size of their internal buffer was different from the size of the caller's output buffer. That case is not known to occur in released <span class=\"productname\">PostgreSQL</span> versions, but this error is a hazard for external modules and future bug fixes.</p>"
  ],
  [
    "<p>Avoid nominally-undefined behavior when accessing shared memory in 32-bit builds (Andres Freund)</p>",
    "<p>clang's undefined-behavior sanitizer complained about use of a pointer that was less aligned than it should be. It's very unlikely that this would cause a problem in non-debug builds, but it's worth fixing for testing purposes.</p>"
  ],
  [
    "<p>Fix copy-and-paste errors in cache-lookup-failure messages for ACL checks (Justin Pryzby)</p>",
    "<p>In principle these errors should never be reached. But if they are, some of them reported the wrong type of object.</p>"
  ],
  [
    "<p>In <span class=\"application\">pg_dump</span>, avoid calling unsafe server functions before we have locks on the tables to be examined (Tom Lane, Gilles Darold)</p>",
    "<p><span class=\"application\">pg_dump</span> uses certain server functions that can fail if examining a table that gets dropped concurrently. Avoid this type of failure by ensuring that we obtain access share lock before inquiring too deeply into a table's properties, and that we don't apply such functions to tables we don't intend to dump at all.</p>"
  ],
  [
    "<p>Fix tab completion of <code class=\"command\">ALTER FUNCTION/PROCEDURE/ROUTINE</code> ... <code class=\"command\">SET SCHEMA</code> (Dean Rasheed)</p>"
  ],
  [
    "<p>Fix <code class=\"filename\">contrib/seg</code> to not crash or print garbage if an input number has more than 127 digits (Tom Lane)</p>"
  ],
  [
    "<p>In <code class=\"filename\">contrib/sepgsql</code>, avoid deprecation warnings with recent <span class=\"application\">libselinux</span> (Michael Paquier)</p>"
  ],
  [
    "<p>Fix compile failure in building PL/Perl with MSVC when using Strawberry Perl (Andrew Dunstan)</p>"
  ],
  [
    "<p>Fix mismatch of PL/Perl built with MSVC versus a Perl library built with gcc (Andrew Dunstan)</p>",
    "<p>Such combinations could previously fail with <span class=\"quote\">“<span class=\"quote\">loadable library and perl binaries are mismatched</span>”</span> errors.</p>"
  ],
  [
    "<p>Suppress compiler warnings from Perl's header files (Andres Freund)</p>",
    "<p>Our preferred compiler options provoke warnings about constructs appearing in recent versions of Perl's header files. When using <span class=\"application\">gcc</span>, we can suppress these warnings with a pragma.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_waldump</span> to build on compilers that don't discard unused static-inline functions (Tom Lane)</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2022g for DST law changes in Greenland and Mexico, plus historical corrections for northern Canada, Colombia, and Singapore.</p>",
    "<p>Notably, a new timezone America/Ciudad_Juarez has been split off from America/Ojinaga.</p>"
  ]
]