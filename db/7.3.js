[
  [
    "<p>Add pg_locks view to show locks (Neil)</p>"
  ],
  [
    "<p>Security fixes for password negotiation memory\n            allocation (Neil)</p>"
  ],
  [
    "<p>Remove support for version 0 FE/BE protocol\n            (<span class=\"productname\">PostgreSQL</span> 6.2 and\n            earlier) (Tom)</p>"
  ],
  [
    "<p>Reserve the last few backend slots for superusers,\n            add parameter superuser_reserved_connections to control\n            this (Nigel J. Andrews)</p>"
  ],
  [
    "<p>Improve startup by calling localtime() only once\n            (Tom)</p>"
  ],
  [
    "<p>Cache system catalog information in flat files for\n            faster startup (Tom)</p>"
  ],
  [
    "<p>Improve caching of index information (Tom)</p>"
  ],
  [
    "<p>Optimizer improvements (Tom, Fernando Nasser)</p>"
  ],
  [
    "<p>Catalog caches now store failed lookups (Tom)</p>"
  ],
  [
    "<p>Hash function improvements (Neil)</p>"
  ],
  [
    "<p>Improve performance of query tokenization and\n            network handling (Peter)</p>"
  ],
  [
    "<p>Speed improvement for large object restore (Mario\n            Weilguni)</p>"
  ],
  [
    "<p>Mark expired index entries on first lookup, saving\n            later heap fetches (Tom)</p>"
  ],
  [
    "<p>Avoid excessive NULL bitmap padding (Manfred\n            Koizar)</p>"
  ],
  [
    "<p>Add BSD-licensed qsort() for Solaris, for\n            performance (Bruce)</p>"
  ],
  [
    "<p>Reduce per-row overhead by four bytes (Manfred\n            Koizar)</p>"
  ],
  [
    "<p>Fix GEQO optimizer bug (Neil Conway)</p>"
  ],
  [
    "<p>Make WITHOUT OID actually save four bytes per row\n            (Manfred Koizar)</p>"
  ],
  [
    "<p>Add default_statistics_target variable to specify\n            ANALYZE buckets (Neil)</p>"
  ],
  [
    "<p>Use local buffer cache for temporary tables so no\n            WAL overhead (Tom)</p>"
  ],
  [
    "<p>Improve free space map performance on large tables\n            (Stephen Marshall, Tom)</p>"
  ],
  [
    "<p>Improved WAL write concurrency (Tom)</p>"
  ],
  [
    "<p>Add privileges on functions and procedural languages\n            (Peter)</p>"
  ],
  [
    "<p>Add OWNER to CREATE DATABASE so superusers can\n            create databases on behalf of unprivileged users (Gavin\n            Sherry, Tom)</p>"
  ],
  [
    "<p>Add new object privilege bits EXECUTE and USAGE\n            (Tom)</p>"
  ],
  [
    "<p>Add SET SESSION AUTHORIZATION DEFAULT and RESET\n            SESSION AUTHORIZATION (Tom)</p>"
  ],
  [
    "<p>Allow functions to be executed with the privilege of\n            the function owner (Peter)</p>"
  ],
  [
    "<p>Server log messages now tagged with LOG, not DEBUG\n            (Bruce)</p>"
  ],
  [
    "<p>Add user column to pg_hba.conf (Bruce)</p>"
  ],
  [
    "<p>Have log_connections output two lines in log file\n            (Tom)</p>"
  ],
  [
    "<p>Remove debug_level from postgresql.conf, now\n            server_min_messages (Bruce)</p>"
  ],
  [
    "<p>New ALTER DATABASE/USER ... SET command for\n            per-user/database initialization (Peter)</p>"
  ],
  [
    "<p>New parameters server_min_messages and\n            client_min_messages to control which messages are sent\n            to the server logs or client applications (Bruce)</p>"
  ],
  [
    "<p>Allow pg_hba.conf to specify lists of\n            users/databases separated by commas, group names\n            prepended with +, and file names prepended with @\n            (Bruce)</p>"
  ],
  [
    "<p>Remove secondary password file capability and\n            pg_password utility (Bruce)</p>"
  ],
  [
    "<p>Add variable db_user_namespace for database-local\n            user names (Bruce)</p>"
  ],
  [
    "<p>SSL improvements (Bear Giles)</p>"
  ],
  [
    "<p>Make encryption of stored passwords the default\n            (Bruce)</p>"
  ],
  [
    "<p>Allow statistics collector to be reset by calling\n            pg_stat_reset() (Christopher)</p>"
  ],
  [
    "<p>Add log_duration parameter (Bruce)</p>"
  ],
  [
    "<p>Rename debug_print_query to log_statement\n            (Bruce)</p>"
  ],
  [
    "<p>Rename show_query_stats to show_statement_stats\n            (Bruce)</p>"
  ],
  [
    "<p>Add param log_min_error_statement to print commands\n            to logs on error (Gavin)</p>"
  ],
  [
    "<p>Make cursors insensitive, meaning their contents do\n            not change (Tom)</p>"
  ],
  [
    "<p>Disable LIMIT #,# syntax; now only LIMIT # OFFSET #\n            supported (Bruce)</p>"
  ],
  [
    "<p>Increase identifier length to 63 (Neil, Bruce)</p>"
  ],
  [
    "<p>UNION fixes for merging &gt;= 3 columns of different\n            lengths (Tom)</p>"
  ],
  [
    "<p>Add DEFAULT key word to INSERT, e.g., INSERT ...\n            (..., DEFAULT, ...) (Rod)</p>"
  ],
  [
    "<p>Allow views to have default values using ALTER\n            COLUMN ... SET DEFAULT (Neil)</p>"
  ],
  [
    "<p>Fail on INSERTs with column lists that don't supply\n            all column values, e.g., INSERT INTO tab (col1, col2)\n            VALUES ('val1'); (Rod)</p>"
  ],
  [
    "<p>Fix for join aliases (Tom)</p>"
  ],
  [
    "<p>Fix for FULL OUTER JOINs (Tom)</p>"
  ],
  [
    "<p>Improve reporting of invalid identifier and location\n            (Tom, Gavin)</p>"
  ],
  [
    "<p>Fix OPEN cursor(args) (Tom)</p>"
  ],
  [
    "<p>Allow 'ctid' to be used in a view and\n            currtid(viewname) (Hiroshi)</p>"
  ],
  [
    "<p>Fix for CREATE TABLE AS with UNION (Tom)</p>"
  ],
  [
    "<p>SQL99 syntax improvements (Thomas)</p>"
  ],
  [
    "<p>Add statement_timeout variable to cancel queries\n            (Bruce)</p>"
  ],
  [
    "<p>Allow prepared queries with PREPARE/EXECUTE\n            (Neil)</p>"
  ],
  [
    "<p>Allow FOR UPDATE to appear after LIMIT/OFFSET\n            (Bruce)</p>"
  ],
  [
    "<p>Add variable autocommit (Tom, David Van Wie)</p>"
  ],
  [
    "<p>Make equals signs optional in CREATE DATABASE (Gavin\n            Sherry)</p>"
  ],
  [
    "<p>Make ALTER TABLE OWNER change index ownership too\n            (Neil)</p>"
  ],
  [
    "<p>New ALTER TABLE tabname ALTER COLUMN colname SET\n            STORAGE controls TOAST storage, compression (John\n            Gray)</p>"
  ],
  [
    "<p>Add schema support, CREATE/DROP SCHEMA (Tom)</p>"
  ],
  [
    "<p>Create schema for temporary tables (Tom)</p>"
  ],
  [
    "<p>Add variable search_path for schema search (Tom)</p>"
  ],
  [
    "<p>Add ALTER TABLE SET/DROP NOT NULL (Christopher)</p>"
  ],
  [
    "<p>New CREATE FUNCTION volatility levels (Tom)</p>"
  ],
  [
    "<p>Make rule names unique only per table (Tom)</p>"
  ],
  [
    "<p>Add 'ON tablename' clause to DROP RULE and COMMENT\n            ON RULE (Tom)</p>"
  ],
  [
    "<p>Add ALTER TRIGGER RENAME (Joe)</p>"
  ],
  [
    "<p>New current_schema() and current_schemas() inquiry\n            functions (Tom)</p>"
  ],
  [
    "<p>Allow functions to return multiple rows (table\n            functions) (Joe)</p>"
  ],
  [
    "<p>Make WITH optional in CREATE DATABASE, for\n            consistency (Bruce)</p>"
  ],
  [
    "<p>Add object dependency tracking (Rod, Tom)</p>"
  ],
  [
    "<p>Add RESTRICT/CASCADE to DROP commands (Rod)</p>"
  ],
  [
    "<p>Add ALTER TABLE DROP for non-CHECK CONSTRAINT\n            (Rod)</p>"
  ],
  [
    "<p>Autodestroy sequence on DROP of table with SERIAL\n            (Rod)</p>"
  ],
  [
    "<p>Prevent column dropping if column is used by foreign\n            key (Rod)</p>"
  ],
  [
    "<p>Automatically drop constraints/functions when object\n            is dropped (Rod)</p>"
  ],
  [
    "<p>Add CREATE/DROP OPERATOR CLASS (Bill Studenmund,\n            Tom)</p>"
  ],
  [
    "<p>Add ALTER TABLE DROP COLUMN (Christopher, Tom,\n            Hiroshi)</p>"
  ],
  [
    "<p>Prevent inherited columns from being removed or\n            renamed (Alvaro Herrera)</p>"
  ],
  [
    "<p>Fix foreign key constraints to not error on\n            intermediate database states (Stephan)</p>"
  ],
  [
    "<p>Propagate column or table renaming to foreign key\n            constraints</p>"
  ],
  [
    "<p>Add CREATE OR REPLACE VIEW (Gavin, Neil, Tom)</p>"
  ],
  [
    "<p>Add CREATE OR REPLACE RULE (Gavin, Neil, Tom)</p>"
  ],
  [
    "<p>Have rules execute alphabetically, returning more\n            predictable values (Tom)</p>"
  ],
  [
    "<p>Triggers are now fired in alphabetical order\n            (Tom)</p>"
  ],
  [
    "<p>Add /contrib/adddepend to handle pre-7.3 object\n            dependencies (Rod)</p>"
  ],
  [
    "<p>Allow better casting when inserting/updating values\n            (Tom)</p>"
  ],
  [
    "<p>Have COPY TO output embedded carriage returns and\n            newlines as \\r and \\n (Tom)</p>"
  ],
  [
    "<p>Allow DELIMITER in COPY FROM to be 8-bit clean\n            (Tatsuo)</p>"
  ],
  [
    "<p>Make <span class=\"application\">pg_dump</span> use\n            ALTER TABLE ADD PRIMARY KEY, for performance (Neil)</p>"
  ],
  [
    "<p>Disable brackets in multistatement rules (Bruce)</p>"
  ],
  [
    "<p>Disable VACUUM from being called inside a function\n            (Bruce)</p>"
  ],
  [
    "<p>Allow dropdb and other scripts to use identifiers\n            with spaces (Bruce)</p>"
  ],
  [
    "<p>Restrict database comment changes to the current\n            database</p>"
  ],
  [
    "<p>Allow comments on operators, independent of the\n            underlying function (Rod)</p>"
  ],
  [
    "<p>Rollback SET commands in aborted transactions\n            (Tom)</p>"
  ],
  [
    "<p>EXPLAIN now outputs as a query (Tom)</p>"
  ],
  [
    "<p>Display condition expressions and sort keys in\n            EXPLAIN (Tom)</p>"
  ],
  [
    "<p>Add 'SET LOCAL var = value' to set configuration\n            variables for a single transaction (Tom)</p>"
  ],
  [
    "<p>Allow ANALYZE to run in a transaction (Bruce)</p>"
  ],
  [
    "<p>Improve COPY syntax using new WITH clauses, keep\n            backward compatibility (Bruce)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_dump</span> to\n            consistently output tags in non-ASCII dumps (Bruce)</p>"
  ],
  [
    "<p>Make foreign key constraints clearer in dump file\n            (Rod)</p>"
  ],
  [
    "<p>Add COMMENT ON CONSTRAINT (Rod)</p>"
  ],
  [
    "<p>Allow COPY TO/FROM to specify column names (Brent\n            Verner)</p>"
  ],
  [
    "<p>Dump UNIQUE and PRIMARY KEY constraints as ALTER\n            TABLE (Rod)</p>"
  ],
  [
    "<p>Have SHOW output a query result (Joe)</p>"
  ],
  [
    "<p>Generate failure on short COPY lines rather than pad\n            NULLs (Neil)</p>"
  ],
  [
    "<p>Fix CLUSTER to preserve all table attributes (Alvaro\n            Herrera)</p>"
  ],
  [
    "<p>New pg_settings table to view/modify GUC settings\n            (Joe)</p>"
  ],
  [
    "<p>Add smart quoting, portability improvements to\n            <span class=\"application\">pg_dump</span> output\n            (Peter)</p>"
  ],
  [
    "<p>Dump serial columns out as SERIAL (Tom)</p>"
  ],
  [
    "<p>Enable large file support, &gt;2G for <span class=\"application\">pg_dump</span> (Peter, Philip Warner,\n            Bruce)</p>"
  ],
  [
    "<p>Disallow TRUNCATE on tables that are involved in\n            referential constraints (Rod)</p>"
  ],
  [
    "<p>Have TRUNCATE also auto-truncate the toast table of\n            the relation (Tom)</p>"
  ],
  [
    "<p>Add clusterdb utility that will auto-cluster an\n            entire database based on previous CLUSTER operations\n            (Alvaro Herrera)</p>"
  ],
  [
    "<p>Overhaul pg_dumpall (Peter)</p>"
  ],
  [
    "<p>Allow REINDEX of TOAST tables (Tom)</p>"
  ],
  [
    "<p>Implemented START TRANSACTION, per SQL99 (Neil)</p>"
  ],
  [
    "<p>Fix rare index corruption when a page split affects\n            bulk delete (Tom)</p>"
  ],
  [
    "<p>Fix ALTER TABLE ... ADD COLUMN for inheritance\n            (Alvaro Herrera)</p>"
  ],
  [
    "<p>Fix factorial(0) to return 1 (Bruce)</p>"
  ],
  [
    "<p>Date/time/timezone improvements (Thomas)</p>"
  ],
  [
    "<p>Fix for array slice extraction (Tom)</p>"
  ],
  [
    "<p>Fix extract/date_part to report proper microseconds\n            for timestamp (Tatsuo)</p>"
  ],
  [
    "<p>Allow text_substr() and bytea_substr() to read TOAST\n            values more efficiently (John Gray)</p>"
  ],
  [
    "<p>Add domain support (Rod)</p>"
  ],
  [
    "<p>Make WITHOUT TIME ZONE the default for TIMESTAMP and\n            TIME data types (Thomas)</p>"
  ],
  [
    "<p>Allow alternate storage scheme of 64-bit integers\n            for date/time types using --enable-integer-datetimes in\n            configure (Thomas)</p>"
  ],
  [
    "<p>Make timezone(timestamptz) return timestamp rather\n            than a string (Thomas)</p>"
  ],
  [
    "<p>Allow fractional seconds in date/time types for\n            dates prior to 1BC (Thomas)</p>"
  ],
  [
    "<p>Limit timestamp data types to 6 decimal places of\n            precision (Thomas)</p>"
  ],
  [
    "<p>Change timezone conversion functions from timetz()\n            to timezone() (Thomas)</p>"
  ],
  [
    "<p>Add configuration variables datestyle and timezone\n            (Tom)</p>"
  ],
  [
    "<p>Add OVERLAY(), which allows substitution of a\n            substring in a string (Thomas)</p>"
  ],
  [
    "<p>Add SIMILAR TO (Thomas, Tom)</p>"
  ],
  [
    "<p>Add regular expression SUBSTRING(string FROM pat FOR\n            escape) (Thomas)</p>"
  ],
  [
    "<p>Add LOCALTIME and LOCALTIMESTAMP functions\n            (Thomas)</p>"
  ],
  [
    "<p>Add named composite types using CREATE TYPE typename\n            AS (column) (Joe)</p>"
  ],
  [
    "<p>Allow composite type definition in the table alias\n            clause (Joe)</p>"
  ],
  [
    "<p>Add new API to simplify creation of C language table\n            functions (Joe)</p>"
  ],
  [
    "<p>Remove ODBC-compatible empty parentheses from calls\n            to SQL99 functions for which these parentheses do not\n            match the standard (Thomas)</p>"
  ],
  [
    "<p>Allow macaddr data type to accept 12 hex digits with\n            no separators (Mike Wyer)</p>"
  ],
  [
    "<p>Add CREATE/DROP CAST (Peter)</p>"
  ],
  [
    "<p>Add IS DISTINCT FROM operator (Thomas)</p>"
  ],
  [
    "<p>Add SQL99 TREAT() function, synonym for CAST()\n            (Thomas)</p>"
  ],
  [
    "<p>Add pg_backend_pid() to output backend pid\n            (Bruce)</p>"
  ],
  [
    "<p>Add IS OF / IS NOT OF type predicate (Thomas)</p>"
  ],
  [
    "<p>Allow bit string constants without fully-specified\n            length (Thomas)</p>"
  ],
  [
    "<p>Allow conversion between 8-byte integers and bit\n            strings (Thomas)</p>"
  ],
  [
    "<p>Implement hex literal conversion to bit string\n            literal (Thomas)</p>"
  ],
  [
    "<p>Allow table functions to appear in the FROM clause\n            (Joe)</p>"
  ],
  [
    "<p>Increase maximum number of function parameters to 32\n            (Bruce)</p>"
  ],
  [
    "<p>No longer automatically create index for SERIAL\n            column (Tom)</p>"
  ],
  [
    "<p>Add current_database() (Rod)</p>"
  ],
  [
    "<p>Fix cash_words() to not overflow buffer (Tom)</p>"
  ],
  [
    "<p>Add functions replace(), split_part(), to_hex()\n            (Joe)</p>"
  ],
  [
    "<p>Fix LIKE for bytea as a right-hand argument\n            (Joe)</p>"
  ],
  [
    "<p>Prevent crashes caused by SELECT cash_out(2)\n            (Tom)</p>"
  ],
  [
    "<p>Fix to_char(1,'FM999.99') to return a period\n            (Karel)</p>"
  ],
  [
    "<p>Fix trigger/type/language functions returning OPAQUE\n            to return proper type (Tom)</p>"
  ],
  [
    "<p>Add additional encodings: Korean (JOHAB), Thai\n            (WIN874), Vietnamese (TCVN), Arabic (WIN1256),\n            Simplified Chinese (GBK), Korean (UHC) (Eiji\n            Tokuya)</p>"
  ],
  [
    "<p>Enable locale support by default (Peter)</p>"
  ],
  [
    "<p>Add locale variables (Peter)</p>"
  ],
  [
    "<p>Escape byes &gt;= 0x7f for multibyte in\n            PQescapeBytea/PQunescapeBytea (Tatsuo)</p>"
  ],
  [
    "<p>Add locale awareness to regular expression character\n            classes</p>"
  ],
  [
    "<p>Enable multibyte support by default (Tatsuo)</p>"
  ],
  [
    "<p>Add GB18030 multibyte support (Bill Huang)</p>"
  ],
  [
    "<p>Add CREATE/DROP CONVERSION, allowing loadable\n            encodings (Tatsuo, Kaori)</p>"
  ],
  [
    "<p>Add pg_conversion table (Tatsuo)</p>"
  ],
  [
    "<p>Add SQL99 CONVERT() function (Tatsuo)</p>"
  ],
  [
    "<p>pg_dumpall, pg_controldata, and pg_resetxlog now\n            national-language aware (Peter)</p>"
  ],
  [
    "<p>New and updated translations</p>"
  ],
  [
    "<p>Allow recursive SQL function (Peter)</p>"
  ],
  [
    "<p>Change PL/Tcl build to use configured compiler and\n            Makefile.shlib (Peter)</p>"
  ],
  [
    "<p>Overhaul the PL/pgSQL FOUND variable to be more\n            Oracle-compatible (Neil, Tom)</p>"
  ],
  [
    "<p>Allow PL/pgSQL to handle quoted identifiers\n            (Tom)</p>"
  ],
  [
    "<p>Allow set-returning PL/pgSQL functions (Neil)</p>"
  ],
  [
    "<p>Make PL/pgSQL schema-aware (Joe)</p>"
  ],
  [
    "<p>Remove some memory leaks (Nigel J. Andrews, Tom)</p>"
  ],
  [
    "<p>Don't lowercase psql \\connect database name for\n            7.2.0 compatibility (Tom)</p>"
  ],
  [
    "<p>Add psql \\timing to time user queries (Greg Sabino\n            Mullane)</p>"
  ],
  [
    "<p>Have psql \\d show index information (Greg Sabino\n            Mullane)</p>"
  ],
  [
    "<p>New psql \\dD shows domains (Jonathan Eisler)</p>"
  ],
  [
    "<p>Allow psql to show rules on views (Paul ?)</p>"
  ],
  [
    "<p>Fix for psql variable substitution (Tom)</p>"
  ],
  [
    "<p>Allow psql \\d to show temporary table structure\n            (Tom)</p>"
  ],
  [
    "<p>Allow psql \\d to show foreign keys (Rod)</p>"
  ],
  [
    "<p>Fix \\? to honor \\pset pager (Bruce)</p>"
  ],
  [
    "<p>Have psql reports its version number on startup\n            (Tom)</p>"
  ],
  [
    "<p>Allow \\copy to specify column names (Tom)</p>"
  ],
  [
    "<p>Add ~/.pgpass to store host/user password\n            combinations (Alvaro Herrera)</p>"
  ],
  [
    "<p>Add PQunescapeBytea() function to libpq (Patrick\n            Welche)</p>"
  ],
  [
    "<p>Fix for sending large queries over non-blocking\n            connections (Bernhard Herzog)</p>"
  ],
  [
    "<p>Fix for libpq using timers on Win9X (David Ford)</p>"
  ],
  [
    "<p>Allow libpq notify to handle servers with\n            different-length identifiers (Tom)</p>"
  ],
  [
    "<p>Add libpq PQescapeString() and PQescapeBytea() to\n            Windows (Bruce)</p>"
  ],
  [
    "<p>Fix for SSL with non-blocking connections (Jack\n            Bates)</p>"
  ],
  [
    "<p>Add libpq connection timeout parameter (Denis A\n            Ustimenko)</p>"
  ],
  [
    "<p>Allow JDBC to compile with JDK 1.4 (Dave)</p>"
  ],
  [
    "<p>Add JDBC 3 support (Barry)</p>"
  ],
  [
    "<p>Allows JDBC to set loglevel by adding ?loglevel=X to\n            the connection URL (Barry)</p>"
  ],
  [
    "<p>Add Driver.info() message that prints out the\n            version number (Barry)</p>"
  ],
  [
    "<p>Add updateable result sets (Raghu Nidagal, Dave)</p>"
  ],
  [
    "<p>Add support for callable statements (Paul Bethe)</p>"
  ],
  [
    "<p>Add query cancel capability</p>"
  ],
  [
    "<p>Add refresh row (Dave)</p>"
  ],
  [
    "<p>Fix MD5 encryption handling for multibyte servers\n            (Jun Kawai)</p>"
  ],
  [
    "<p>Add support for prepared statements (Barry)</p>"
  ],
  [
    "<p>Fixed ECPG bug concerning octal numbers in single\n            quotes (Michael)</p>"
  ],
  [
    "<p>Move src/interfaces/libpgeasy to\n            http://gborg.postgresql.org (Marc, Bruce)</p>"
  ],
  [
    "<p>Improve Python interface (Elliot Lee, Andrew\n            Johnson, Greg Copeland)</p>"
  ],
  [
    "<p>Add libpgtcl connection close event (Gerhard\n            Hintermayer)</p>"
  ],
  [
    "<p>Move src/interfaces/libpq++ to\n            http://gborg.postgresql.org (Marc, Bruce)</p>"
  ],
  [
    "<p>Move src/interfaces/odbc to\n            http://gborg.postgresql.org (Marc)</p>"
  ],
  [
    "<p>Move src/interfaces/libpgeasy to\n            http://gborg.postgresql.org (Marc, Bruce)</p>"
  ],
  [
    "<p>Move src/interfaces/perl5 to\n            http://gborg.postgresql.org (Marc, Bruce)</p>"
  ],
  [
    "<p>Remove src/bin/pgaccess from main tree, now at\n            http://www.pgaccess.org (Bruce)</p>"
  ],
  [
    "<p>Add pg_on_connection_loss command to libpgtcl\n            (Gerhard Hintermayer, Tom)</p>"
  ],
  [
    "<p>Fix for parallel make (Peter)</p>"
  ],
  [
    "<p>AIX fixes for linking Tcl (Andreas Zeugswetter)</p>"
  ],
  [
    "<p>Allow PL/Perl to build under Cygwin (Jason\n            Tishler)</p>"
  ],
  [
    "<p>Improve MIPS compiles (Peter, Oliver Elphick)</p>"
  ],
  [
    "<p>Require Autoconf version 2.53 (Peter)</p>"
  ],
  [
    "<p>Require readline and zlib by default in configure\n            (Peter)</p>"
  ],
  [
    "<p>Allow Solaris to use Intimate Shared Memory (ISM),\n            for performance (Scott Brunza, P.J. Josh Rovero)</p>"
  ],
  [
    "<p>Always enable syslog in compile, remove\n            --enable-syslog option (Tatsuo)</p>"
  ],
  [
    "<p>Always enable multibyte in compile, remove\n            --enable-multibyte option (Tatsuo)</p>"
  ],
  [
    "<p>Always enable locale in compile, remove\n            --enable-locale option (Peter)</p>"
  ],
  [
    "<p>Fix for Win9x DLL creation (Magnus Naeslund)</p>"
  ],
  [
    "<p>Fix for link() usage by WAL code on Windows, BeOS\n            (Jason Tishler)</p>"
  ],
  [
    "<p>Add sys/types.h to c.h, remove from main files\n            (Peter, Bruce)</p>"
  ],
  [
    "<p>Fix AIX hang on SMP machines (Tomoyuki Niijima)</p>"
  ],
  [
    "<p>AIX SMP hang fix (Tomoyuki Niijima)</p>"
  ],
  [
    "<p>Fix pre-1970 date handling on newer glibc libraries\n            (Tom)</p>"
  ],
  [
    "<p>Fix PowerPC SMP locking (Tom)</p>"
  ],
  [
    "<p>Prevent gcc -ffast-math from being used (Peter,\n            Tom)</p>"
  ],
  [
    "<p>Bison &gt;= 1.50 now required for developer\n            builds</p>"
  ],
  [
    "<p>Kerberos 5 support now builds with Heimdal\n            (Peter)</p>"
  ],
  [
    "<p>Add appendix in the User's Guide which lists SQL\n            features (Thomas)</p>"
  ],
  [
    "<p>Improve loadable module linking to use RTLD_NOW\n            (Tom)</p>"
  ],
  [
    "<p>New error levels WARNING, INFO, LOG, DEBUG[1-5]\n            (Bruce)</p>"
  ],
  [
    "<p>New src/port directory holds replaced libc functions\n            (Peter, Bruce)</p>"
  ],
  [
    "<p>New pg_namespace system catalog for schemas\n            (Tom)</p>"
  ],
  [
    "<p>Add pg_class.relnamespace for schemas (Tom)</p>"
  ],
  [
    "<p>Add pg_type.typnamespace for schemas (Tom)</p>"
  ],
  [
    "<p>Add pg_proc.pronamespace for schemas (Tom)</p>"
  ],
  [
    "<p>Restructure aggregates to have pg_proc entries\n            (Tom)</p>"
  ],
  [
    "<p>System relations now have their own namespace, pg_*\n            test not required (Fernando Nasser)</p>"
  ],
  [
    "<p>Rename TOAST index names to be *_index rather than\n            *_idx (Neil)</p>"
  ],
  [
    "<p>Add namespaces for operators, opclasses (Tom)</p>"
  ],
  [
    "<p>Add additional checks to server control file\n            (Thomas)</p>"
  ],
  [
    "<p>New Polish FAQ (Marcin Mazurek)</p>"
  ],
  [
    "<p>Add Posix semaphore support (Tom)</p>"
  ],
  [
    "<p>Document need for reindex (Bruce)</p>"
  ],
  [
    "<p>Rename some internal identifiers to simplify Windows\n            compile (Jan, Katherine Ward)</p>"
  ],
  [
    "<p>Add documentation on computing disk space\n            (Bruce)</p>"
  ],
  [
    "<p>Remove KSQO from GUC (Bruce)</p>"
  ],
  [
    "<p>Fix memory leak in rtree (Kenneth Been)</p>"
  ],
  [
    "<p>Modify a few error messages for consistency\n            (Bruce)</p>"
  ],
  [
    "<p>Remove unused system table columns (Peter)</p>"
  ],
  [
    "<p>Make system columns NOT NULL where appropriate\n            (Tom)</p>"
  ],
  [
    "<p>Clean up use of sprintf in favor of snprintf()\n            (Neil, Jukka Holappa)</p>"
  ],
  [
    "<p>Remove OPAQUE and create specific subtypes (Tom)</p>"
  ],
  [
    "<p>Cleanups in array internal handling (Joe, Tom)</p>"
  ],
  [
    "<p>Disallow pg_atoi('') (Bruce)</p>"
  ],
  [
    "<p>Remove parameter wal_files because WAL files are now\n            recycled (Bruce)</p>"
  ],
  [
    "<p>Add version numbers to heap pages (Tom)</p>"
  ],
  [
    "<p>Allow inet arrays in /contrib/array (Neil)</p>"
  ],
  [
    "<p>GiST fixes (Teodor Sigaev, Neil)</p>"
  ],
  [
    "<p>Upgrade /contrib/mysql</p>"
  ],
  [
    "<p>Add /contrib/dbsize which shows table sizes without\n            vacuum (Peter)</p>"
  ],
  [
    "<p>Add /contrib/intagg, integer aggregator routines\n            (mlw)</p>"
  ],
  [
    "<p>Improve /contrib/oid2name (Neil, Bruce)</p>"
  ],
  [
    "<p>Improve /contrib/tsearch (Oleg, Teodor Sigaev)</p>"
  ],
  [
    "<p>Cleanups of /contrib/rserver (Alexey V. Borzov)</p>"
  ],
  [
    "<p>Update /contrib/oracle conversion utility (Gilles\n            Darold)</p>"
  ],
  [
    "<p>Update /contrib/dblink (Joe)</p>"
  ],
  [
    "<p>Improve options supported by /contrib/vacuumlo\n            (Mario Weilguni)</p>"
  ],
  [
    "<p>Improvements to /contrib/intarray (Oleg, Teodor\n            Sigaev, Andrey Oktyabrski)</p>"
  ],
  [
    "<p>Add /contrib/reindexdb utility (Shaun Thomas)</p>"
  ],
  [
    "<p>Add indexing to /contrib/isbn_issn (Dan Weston)</p>"
  ],
  [
    "<p>Add /contrib/dbmirror (Steven Singer)</p>"
  ],
  [
    "<p>Improve /contrib/pgbench (Neil)</p>"
  ],
  [
    "<p>Add /contrib/tablefunc table function examples\n            (Joe)</p>"
  ],
  [
    "<p>Add /contrib/ltree data type for tree structures\n            (Teodor Sigaev, Oleg Bartunov)</p>"
  ],
  [
    "<p>Move /contrib/pg_controldata, pg_resetxlog into main\n            tree (Bruce)</p>"
  ],
  [
    "<p>Fixes to /contrib/cube (Bruno Wolff)</p>"
  ],
  [
    "<p>Improve /contrib/fulltextindex (Christopher)</p>"
  ]
]