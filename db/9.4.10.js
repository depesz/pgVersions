[
  [
    "<p>Fix WAL-logging of truncation of relation free space maps and visibility maps (Pavan Deolasee, Heikki Linnakangas)</p>",
    "<p>It was possible for these files to not be correctly restored during crash recovery, or to be written incorrectly on a standby server. Bogus entries in a free space map could lead to attempts to access pages that have been truncated away from the relation itself, typically producing errors like <span class=\"QUOTE\">\"could not read block <tt class=\"REPLACEABLE c2\">XXX</tt>: read only 0 of 8192 bytes\"</span>. Checksum failures in the visibility map are also possible, if checksumming is enabled.</p>",
    "<p>Procedures for determining whether there is a problem and repairing it if so are discussed at <a href=\"https://wiki.postgresql.org/wiki/Free_Space_Map_Problems\" target=\"_top\">https://wiki.postgresql.org/wiki/Free_Space_Map_Problems</a>.</p>"
  ],
  [
    "<p>Fix incorrect creation of GIN index WAL records on big-endian machines (Tom Lane)</p>",
    "<p>The typical symptom was <span class=\"QUOTE\">\"unexpected GIN leaf action\"</span> errors during WAL replay.</p>"
  ],
  [
    "<p>Fix <tt class=\"COMMAND\">SELECT FOR UPDATE/SHARE</tt> to correctly lock tuples that have been updated by a subsequently-aborted transaction (Álvaro Herrera)</p>",
    "<p>In 9.5 and later, the <tt class=\"COMMAND\">SELECT</tt> would sometimes fail to return such tuples at all. A failure has not been proven to occur in earlier releases, but might be possible with concurrent updates.</p>"
  ],
  [
    "<p>Fix EvalPlanQual rechecks involving CTE scans (Tom Lane)</p>",
    "<p>The recheck would always see the CTE as returning no rows, typically leading to failure to update rows that were recently updated.</p>"
  ],
  [
    "<p>Fix improper repetition of previous results from hashed aggregation in a subquery (Andrew Gierth)</p>",
    "<p>The test to see if we can reuse a previously-computed hash table of the aggregate state values neglected the possibility of an outer query reference appearing in an aggregate argument expression. A change in the value of such a reference should lead to recalculating the hash table, but did not.</p>"
  ],
  [
    "<p>Fix query-lifespan memory leak in a bulk <tt class=\"COMMAND\">UPDATE</tt> on a table with a <tt class=\"LITERAL\">PRIMARY KEY</tt> or <tt class=\"LITERAL\">REPLICA IDENTITY</tt> index (Tom Lane)</p>"
  ],
  [
    "<p>Fix <tt class=\"COMMAND\">EXPLAIN</tt> to emit valid XML when <a href=\"https://www.postgresql.org/docs/9.4/runtime-config-statistics.html#GUC-TRACK-IO-TIMING\">track_io_timing</a> is on (Markus Winand)</p>",
    "<p>Previously the XML output-format option produced syntactically invalid tags such as <tt class=\"LITERAL\">&lt;I/O-Read-Time&gt;</tt>. That is now rendered as <tt class=\"LITERAL\">&lt;I-O-Read-Time&gt;</tt>.</p>"
  ],
  [
    "<p>Suppress printing of zeroes for unmeasured times in <tt class=\"COMMAND\">EXPLAIN</tt> (Maksim Milyutin)</p>",
    "<p>Certain option combinations resulted in printing zero values for times that actually aren't ever measured in that combination. Our general policy in <tt class=\"COMMAND\">EXPLAIN</tt> is not to print such fields at all, so do that consistently in all cases.</p>"
  ],
  [
    "<p>Fix timeout length when <tt class=\"COMMAND\">VACUUM</tt> is waiting for exclusive table lock so that it can truncate the table (Simon Riggs)</p>",
    "<p>The timeout was meant to be 50 milliseconds, but it was actually only 50 microseconds, causing <tt class=\"COMMAND\">VACUUM</tt> to give up on truncation much more easily than intended. Set it to the intended value.</p>"
  ],
  [
    "<p>Fix bugs in merging inherited <tt class=\"LITERAL\">CHECK</tt> constraints while creating or altering a table (Tom Lane, Amit Langote)</p>",
    "<p>Allow identical <tt class=\"LITERAL\">CHECK</tt> constraints to be added to a parent and child table in either order. Prevent merging of a valid constraint from the parent table with a <tt class=\"LITERAL\">NOT VALID</tt> constraint on the child. Likewise, prevent merging of a <tt class=\"LITERAL\">NO INHERIT</tt> child constraint with an inherited constraint.</p>"
  ],
  [
    "<p>Remove artificial restrictions on the values accepted by <code class=\"FUNCTION\">numeric_in()</code> and <code class=\"FUNCTION\">numeric_recv()</code> (Tom Lane)</p>",
    "<p>We allow numeric values up to the limit of the storage format (more than <tt class=\"LITERAL\">1e100000</tt>), so it seems fairly pointless that <code class=\"FUNCTION\">numeric_in()</code> rejected scientific-notation exponents above 1000. Likewise, it was silly for <code class=\"FUNCTION\">numeric_recv()</code> to reject more than 1000 digits in an input value.</p>"
  ],
  [
    "<p>Avoid very-low-probability data corruption due to testing tuple visibility without holding buffer lock (Thomas Munro, Peter Geoghegan, Tom Lane)</p>"
  ],
  [
    "<p>Fix logical WAL decoding to work properly when a subtransaction's WAL output is large enough to spill to disk (Andres Freund)</p>"
  ],
  [
    "<p>Fix buffer overread in logical WAL decoding (Tom Lane)</p>",
    "<p>Logical decoding of a tuple update record read 23 bytes too many, which was usually harmless but with very bad luck could result in a crash.</p>"
  ],
  [
    "<p>Fix file descriptor leakage when truncating a temporary relation of more than 1GB (Andres Freund)</p>"
  ],
  [
    "<p>Disallow starting a standalone backend with <tt class=\"LITERAL\">standby_mode</tt> turned on (Michael Paquier)</p>",
    "<p>This can't do anything useful, since there will be no WAL receiver process to fetch more WAL data; and it could result in misbehavior in code that wasn't designed with this situation in mind.</p>"
  ],
  [
    "<p>Properly initialize replication slot state when recycling a previously-used slot (Michael Paquier)</p>",
    "<p>This failure to reset all of the fields of the slot could prevent <tt class=\"COMMAND\">VACUUM</tt> from removing dead tuples.</p>"
  ],
  [
    "<p>Round shared-memory allocation request to a multiple of the actual huge page size when attempting to use huge pages on Linux (Tom Lane)</p>",
    "<p>This avoids possible failures during <code class=\"FUNCTION\">munmap()</code> on systems with atypical default huge page sizes. Except in crash-recovery cases, there were no ill effects other than a log message.</p>"
  ],
  [
    "<p>Use a more random value for the dynamic shared memory control segment's ID (Robert Haas, Tom Lane)</p>",
    "<p>Previously, the same value would be chosen every time, because it was derived from <code class=\"FUNCTION\">random()</code> but <code class=\"FUNCTION\">srandom()</code> had not yet been called. While relatively harmless, this was not the intended behavior.</p>"
  ],
  [
    "<p>On Windows, retry creation of the dynamic shared memory control segment after an access-denied error (Kyotaro Horiguchi, Amit Kapila)</p>",
    "<p>Windows sometimes returns <tt class=\"LITERAL\">ERROR_ACCESS_DENIED</tt> rather than <tt class=\"LITERAL\">ERROR_ALREADY_EXISTS</tt> when there is an existing segment. This led to postmaster startup failure due to believing that the former was an unrecoverable error.</p>"
  ],
  [
    "<p>Don't try to share SSL contexts across multiple connections in <span class=\"APPLICATION\">libpq</span> (Heikki Linnakangas)</p>",
    "<p>This led to assorted corner-case bugs, particularly when trying to use different SSL parameters for different connections.</p>"
  ],
  [
    "<p>Avoid corner-case memory leak in <span class=\"APPLICATION\">libpq</span> (Tom Lane)</p>",
    "<p>The reported problem involved leaking an error report during <code class=\"FUNCTION\">PQreset()</code>, but there might be related cases.</p>"
  ],
  [
    "<p>Make <span class=\"APPLICATION\">ecpg</span>'s <tt class=\"OPTION\">--help</tt> and <tt class=\"OPTION\">--version</tt> options work consistently with our other executables (Haribabu Kommi)</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pgbench</span>'s calculation of average latency (Fabien Coelho)</p>",
    "<p>The calculation was incorrect when there were <tt class=\"LITERAL\">\\sleep</tt> commands in the script, or when the test duration was specified in number of transactions rather than total time.</p>"
  ],
  [
    "<p>In <span class=\"APPLICATION\">pg_dump</span>, never dump range constructor functions (Tom Lane)</p>",
    "<p>This oversight led to <span class=\"APPLICATION\">pg_upgrade</span> failures with extensions containing range types, due to duplicate creation of the constructor functions.</p>"
  ],
  [
    "<p>In <span class=\"APPLICATION\">pg_xlogdump</span>, retry opening new WAL segments when using <tt class=\"OPTION\">--follow</tt> option (Magnus Hagander)</p>",
    "<p>This allows for a possible delay in the server's creation of the next segment.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_xlogdump</span> to cope with a WAL file that begins with a continuation record spanning more than one page (Pavan Deolasee)</p>"
  ],
  [
    "<p>Fix <tt class=\"FILENAME\">contrib/pg_buffercache</tt> to work when <tt class=\"VARNAME\">shared_buffers</tt> exceeds 256GB (KaiGai Kohei)</p>"
  ],
  [
    "<p>Fix <tt class=\"FILENAME\">contrib/intarray/bench/bench.pl</tt> to print the results of the <tt class=\"COMMAND\">EXPLAIN</tt> it does when given the <tt class=\"OPTION\">-e</tt> option (Daniel Gustafsson)</p>"
  ],
  [
    "<p>Install TAP test infrastructure so that it's available for extension testing (Craig Ringer)</p>",
    "<p>When <span class=\"PRODUCTNAME\">PostgreSQL</span> has been configured with <tt class=\"OPTION\">--enable-tap-tests</tt>, <span class=\"QUOTE\">\"make install\"</span> will now install the Perl support files for TAP testing where PGXS can find them. This allows non-core extensions to use <tt class=\"LITERAL\">$(prove_check)</tt> without extra tests.</p>"
  ],
  [
    "<p>In MSVC builds, include <span class=\"APPLICATION\">pg_recvlogical</span> in a client-only installation (MauMau)</p>"
  ],
  [
    "<p>Update Windows time zone mapping to recognize some time zone names added in recent Windows versions (Michael Paquier)</p>"
  ],
  [
    "<p>Prevent failure of obsolete dynamic time zone abbreviations (Tom Lane)</p>",
    "<p>If a dynamic time zone abbreviation does not match any entry in the referenced time zone, treat it as equivalent to the time zone name. This avoids unexpected failures when IANA removes abbreviations from their time zone database, as they did in <span class=\"APPLICATION\">tzdata</span> release 2016f and seem likely to do again in the future. The consequences were not limited to not recognizing the individual abbreviation; any mismatch caused the <tt class=\"STRUCTNAME\">pg_timezone_abbrevs</tt> view to fail altogether.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"APPLICATION\">tzdata</span> release 2016h for DST law changes in Palestine and Turkey, plus historical corrections for Turkey and some regions of Russia. Switch to numeric abbreviations for some time zones in Antarctica, the former Soviet Union, and Sri Lanka.</p>",
    "<p>The IANA time zone database previously provided textual abbreviations for all time zones, sometimes making up abbreviations that have little or no currency among the local population. They are in process of reversing that policy in favor of using numeric UTC offsets in zones where there is no evidence of real-world use of an English abbreviation. At least for the time being, <span class=\"PRODUCTNAME\">PostgreSQL</span> will continue to accept such removed abbreviations for timestamp input. But they will not be shown in the <tt class=\"STRUCTNAME\">pg_timezone_names</tt> view nor used for output.</p>",
    "<p>In this update, <tt class=\"LITERAL\">AMT</tt> is no longer shown as being in use to mean Armenia Time. Therefore, we have changed the <tt class=\"LITERAL\">Default</tt> abbreviation set to interpret it as Amazon Time, thus UTC-4 not UTC+4.</p>"
  ]
]