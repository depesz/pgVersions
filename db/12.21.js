[
  [
    "<p>Ensure cached plans are marked as dependent on the calling role when RLS applies to a non-top-level table reference (Nathan Bossart) <a class=\"ulink\" href=\"https://postgr.es/c/448525e8a\" target=\"_top\">§</a></p>",
    "<p>If a CTE, subquery, sublink, security invoker view, or coercion projection in a query references a table with row-level security policies, we neglected to mark the resulting plan as potentially dependent on which role is executing it. This could lead to later query executions in the same session using the wrong plan, and then returning or hiding rows that should have been hidden or returned instead.</p>",
    "<p>The <span class=\"productname\">PostgreSQL</span> Project thanks Wolfgang Walther for reporting this problem. (CVE-2024-10976)</p>"
  ],
  [
    "<p>Make <span class=\"application\">libpq</span> discard error messages received during SSL or GSS protocol negotiation (Jacob Champion) <a class=\"ulink\" href=\"https://postgr.es/c/2a951ef0a\" target=\"_top\">§</a></p>",
    "<p>An error message received before encryption negotiation is completed might have been injected by a man-in-the-middle, rather than being real server output. Reporting it opens the door to various security hazards; for example, the message might spoof a query result that a careless user could mistake for correct output. The best answer seems to be to discard such data and rely only on <span class=\"application\">libpq</span>'s own report of the connection failure.</p>",
    "<p>The <span class=\"productname\">PostgreSQL</span> Project thanks Jacob Champion for reporting this problem. (CVE-2024-10977)</p>"
  ],
  [
    "<p>Fix unintended interactions between <code class=\"command\">SET SESSION AUTHORIZATION</code> and <code class=\"command\">SET ROLE</code> (Tom Lane) <a class=\"ulink\" href=\"https://postgr.es/c/4c9d96f74\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/0edad8654\" target=\"_top\">§</a></p>",
    "<p>The SQL standard mandates that <code class=\"command\">SET SESSION AUTHORIZATION</code> have a side-effect of doing <code class=\"command\">SET ROLE NONE</code>. Our implementation of that was flawed, creating more interaction between the two settings than intended. Notably, rolling back a transaction that had done <code class=\"command\">SET SESSION AUTHORIZATION</code> would revert <code class=\"literal\">ROLE</code> to <code class=\"literal\">NONE</code> even if that had not been the previous state, so that the effective user ID might now be different from what it had been before the transaction. Transiently setting <code class=\"varname\">session_authorization</code> in a function <code class=\"literal\">SET</code> clause had a similar effect. A related bug was that if a parallel worker inspected <code class=\"literal\">current_setting('role')</code>, it saw <code class=\"literal\">none</code> even when it should see something else.</p>",
    "<p>The <span class=\"productname\">PostgreSQL</span> Project thanks Tom Lane for reporting this problem. (CVE-2024-10978)</p>"
  ],
  [
    "<p>Prevent trusted PL/Perl code from changing environment variables (Andrew Dunstan, Noah Misch) <a class=\"ulink\" href=\"https://postgr.es/c/2ab12d860\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/b1e58defb\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/1e457468f\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/9fc1c3a02\" target=\"_top\">§</a></p>",
    "<p>The ability to manipulate process environment variables such as <code class=\"literal\">PATH</code> gives an attacker opportunities to execute arbitrary code. Therefore, <span class=\"quote\">“<span class=\"quote\">trusted</span>”</span> PLs must not offer the ability to do that. To fix <code class=\"literal\">plperl</code>, replace <code class=\"varname\">%ENV</code> with a tied hash that rejects any modification attempt with a warning. Untrusted <code class=\"literal\">plperlu</code> retains the ability to change the environment.</p>",
    "<p>The <span class=\"productname\">PostgreSQL</span> Project thanks Coby Abrams for reporting this problem. (CVE-2024-10979)</p>"
  ],
  [
    "<p>Disallow <code class=\"command\">ALTER TABLE ATTACH PARTITION</code> if the table to be attached has a foreign key referencing the partitioned table (Álvaro Herrera) <a class=\"ulink\" href=\"https://postgr.es/c/17ed92e1f\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/cae459d11\" target=\"_top\">§</a></p>",
    "<p>This arrangement is not supported, and other ways of creating it already fail.</p>"
  ],
  [
    "<p>Don't use partitionwise joins or grouping if the query's collation for the key column doesn't match the partition key's collation (Jian He, Webbo Han) <a class=\"ulink\" href=\"https://postgr.es/c/9c4757491\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/46d9be5ef\" target=\"_top\">§</a></p>",
    "<p>Such plans could produce incorrect results.</p>"
  ],
  [
    "<p>Allow cancellation of the second stage of index build for large hash indexes (Pavel Borisov) <a class=\"ulink\" href=\"https://postgr.es/c/813ade548\" target=\"_top\">§</a></p>"
  ],
  [
    "<p>Fix assertion failure or confusing error message for <code class=\"literal\">COPY (<em class=\"replaceable\"><code>query</code></em>) TO ...</code>, when the <em class=\"replaceable\"><code>query</code></em> is rewritten by a <code class=\"literal\">DO INSTEAD NOTIFY</code> rule (Tender Wang, Tom Lane) <a class=\"ulink\" href=\"https://postgr.es/c/6a57a457c\" target=\"_top\">§</a></p>"
  ],
  [
    "<p>Fix detection of skewed data during parallel hash join (Thomas Munro) <a class=\"ulink\" href=\"https://postgr.es/c/4fa80a6d7\" target=\"_top\">§</a></p>",
    "<p>After repartitioning the inner side of a hash join because one partition has accumulated too many tuples, we check to see if all the partition's tuples went into the same child partition, which suggests that they all have the same hash value and further repartitioning cannot improve matters. This check malfunctioned in some cases, allowing repeated futile repartitioning which would eventually end in a resource-exhaustion error.</p>"
  ],
  [
    "<p>Fix race condition in committing a serializable transaction (Heikki Linnakangas) <a class=\"ulink\" href=\"https://postgr.es/c/e2ec3afeb\" target=\"_top\">§</a></p>",
    "<p>Mis-processing of a recently committed transaction could lead to an assertion failure or a <span class=\"quote\">“<span class=\"quote\">could not access status of transaction</span>”</span> error.</p>"
  ],
  [
    "<p>Fix race condition in <code class=\"command\">COMMIT PREPARED</code> that resulted in orphaned 2PC files (wuchengwen) <a class=\"ulink\" href=\"https://postgr.es/c/34d751ba7\" target=\"_top\">§</a></p>",
    "<p>A concurrent <code class=\"command\">PREPARE TRANSACTION</code> could cause <code class=\"command\">COMMIT PREPARED</code> to not remove the on-disk two-phase state file for the completed transaction. There was no immediate ill effect, but a subsequent crash-and-recovery could fail with <span class=\"quote\">“<span class=\"quote\">could not access status of transaction</span>”</span>, requiring manual removal of the orphaned file to restore service.</p>"
  ],
  [
    "<p>Avoid invalid memory accesses after skipping an invalid toast index during <code class=\"command\">VACUUM FULL</code> (Tender Wang) <a class=\"ulink\" href=\"https://postgr.es/c/2f33e68a5\" target=\"_top\">§</a></p>",
    "<p>A list tracking yet-to-be-rebuilt indexes was not properly updated in this code path, risking assertion failures or crashes later on.</p>"
  ],
  [
    "<p>Fix ways in which an <span class=\"quote\">“<span class=\"quote\">in place</span>”</span> catalog update could be lost (Noah Misch) <a class=\"ulink\" href=\"https://postgr.es/c/7354b680a\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/cafcc3ad0\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/5a33a39a8\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/a0c0078b1\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/c2139db11\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/d729f1ea5\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/766809db3\" target=\"_top\">§</a></p>",
    "<p>Normal row updates write a new version of the row to preserve rollback-ability of the transaction. However, certain system catalog updates are intentionally non-transactional and are done with an in-place update of the row. These patches fix race conditions that could cause the effects of an in-place update to be lost. As an example, it was possible to forget having set <code class=\"structname\">pg_class</code>.<code class=\"structfield\">relhasindex</code> to true, preventing updates of the new index and thus causing index corruption.</p>"
  ],
  [
    "<p>Reset catalog caches at end of recovery (Noah Misch) <a class=\"ulink\" href=\"https://postgr.es/c/da9950456\" target=\"_top\">§</a></p>",
    "<p>This prevents scenarios wherein an in-place catalog update could be lost due to using stale data from a catalog cache.</p>"
  ],
  [
    "<p>Avoid using parallel query while holding off interrupts (Francesco Degrassi, Noah Misch, Tom Lane) <a class=\"ulink\" href=\"https://postgr.es/c/507b72bd9\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/6e39ca6e7\" target=\"_top\">§</a></p>",
    "<p>This situation cannot arise normally, but it can be reached with test scenarios such as using a SQL-language function as B-tree support (which would be far too slow for production usage). If it did occur it would result in an indefinite wait.</p>"
  ],
  [
    "<p>Guard against stack overflow in <span class=\"application\">libxml2</span> with too-deeply-nested XML input (Tom Lane, with hat tip to Nick Wellnhofer) <a class=\"ulink\" href=\"https://postgr.es/c/0206795d2\" target=\"_top\">§</a></p>",
    "<p>Use <code class=\"function\">xmlXPathCtxtCompile()</code> rather than <code class=\"function\">xmlXPathCompile()</code>, because the latter fails to protect itself against recursion-to-stack-overflow in <span class=\"application\">libxml2</span> releases before 2.13.4.</p>"
  ],
  [
    "<p>Fix <span class=\"quote\">“<span class=\"quote\">failed to find plan for subquery/CTE</span>”</span> errors in <code class=\"command\">EXPLAIN</code> (Richard Guo, Tom Lane) <a class=\"ulink\" href=\"https://postgr.es/c/7408772de\" target=\"_top\">§</a></p>",
    "<p>This case arose while trying to print references to fields of a RECORD-type output of a subquery when the subquery has been optimized out of the plan altogether (which is possible at least in the case that it has a constant-false <code class=\"literal\">WHERE</code> condition). Nothing remains in the plan to identify the original field names, so fall back to printing <code class=\"literal\">f<em class=\"replaceable\"><code>N</code></em></code> for the <em class=\"replaceable\"><code>N</code></em>'th record column. (That's actually the right thing anyway, if the record output arose from a <code class=\"literal\">ROW()</code> constructor.)</p>"
  ],
  [
    "<p>Disallow a <code class=\"literal\">USING</code> clause when altering the type of a generated column (Peter Eisentraut) <a class=\"ulink\" href=\"https://postgr.es/c/1c57ae795\" target=\"_top\">§</a></p>",
    "<p>A generated column already has an expression specifying the column contents, so including <code class=\"literal\">USING</code> doesn't make sense.</p>"
  ],
  [
    "<p>Ignore not-yet-defined Portals in the <code class=\"structname\">pg_cursors</code> view (Tom Lane) <a class=\"ulink\" href=\"https://postgr.es/c/5c17f5a63\" target=\"_top\">§</a></p>",
    "<p>It is possible for user-defined code that inspects this view to be called while a new cursor is being set up, and if that happens a null pointer dereference would ensue. Avoid the problem by defining the view to exclude incompletely-set-up cursors.</p>"
  ],
  [
    "<p>Reduce memory consumption of logical decoding (Masahiko Sawada) <a class=\"ulink\" href=\"https://postgr.es/c/53fa68b3b\" target=\"_top\">§</a></p>",
    "<p>Use a smaller default block size to store tuple data received during logical replication. This reduces memory wastage, which has been reported to be severe while processing long-running transactions, even leading to out-of-memory failures.</p>"
  ],
  [
    "<p>Re-disable sending of stateless (TLSv1.2) session tickets (Daniel Gustafsson) <a class=\"ulink\" href=\"https://postgr.es/c/cd98a142c\" target=\"_top\">§</a></p>",
    "<p>A previous change to prevent sending of stateful (TLSv1.3) session tickets accidentally re-enabled sending of stateless ones. Thus, while we intended to prevent clients from thinking that TLS session resumption is supported, some still did.</p>"
  ],
  [
    "<p>Avoid <span class=\"quote\">“<span class=\"quote\">wrong tuple length</span>”</span> failure when dropping a database with many ACL (permission) entries (Ayush Tiwari) <a class=\"ulink\" href=\"https://postgr.es/c/f18d3e47f\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/9d42627bc\" target=\"_top\">§</a></p>"
  ],
  [
    "<p>Allow adjusting the <code class=\"varname\">session_authorization</code> and <code class=\"varname\">role</code> settings in parallel workers (Tom Lane) <a class=\"ulink\" href=\"https://postgr.es/c/adc28d01e\" target=\"_top\">§</a></p>",
    "<p>Our code intends to allow modifiable server settings to be set by function <code class=\"literal\">SET</code> clauses, but not otherwise within a parallel worker. <code class=\"literal\">SET</code> clauses failed for these two settings, though.</p>"
  ],
  [
    "<p>Fix behavior of stable functions called from a <code class=\"command\">CALL</code> statement's argument list, when the <code class=\"command\">CALL</code> is within a PL/pgSQL <code class=\"literal\">EXCEPTION</code> block (Tom Lane) <a class=\"ulink\" href=\"https://postgr.es/c/cf1443d67\" target=\"_top\">§</a></p>",
    "<p>As with a similar fix in our previous quarterly releases, this case allowed such functions to be passed the wrong snapshot, causing them to see stale values of rows modified since the start of the outer transaction.</p>"
  ],
  [
    "<p>Parse <span class=\"application\">libpq</span>'s <code class=\"literal\">keepalives</code> connection option in the same way as other integer-valued options (Yuto Sasaki) <a class=\"ulink\" href=\"https://postgr.es/c/47d8a15de\" target=\"_top\">§</a></p>",
    "<p>The coding used here rejected trailing whitespace in the option value, unlike other cases. This turns out to be problematic in <span class=\"application\">ecpg</span>'s usage, for example.</p>"
  ],
  [
    "<p>In <span class=\"application\">ecpglib</span>, fix out-of-bounds read when parsing incorrect datetime input (Bruce Momjian, Pavel Nekrasov) <a class=\"ulink\" href=\"https://postgr.es/c/9ecfd8a48\" target=\"_top\">§</a></p>",
    "<p>It was possible to try to read the location just before the start of a constant array. Real-world consequences seem minimal, though.</p>"
  ],
  [
    "<p>Include the source timeline history in <span class=\"application\">pg_rewind</span>'s debug output (Heikki Linnakangas) <a class=\"ulink\" href=\"https://postgr.es/c/8b86e289f\" target=\"_top\">§</a></p>",
    "<p>This was the intention to begin with, but a coding error caused the source history to always print as empty.</p>"
  ],
  [
    "<p>Avoid trying to reindex temporary tables and indexes in <span class=\"application\">vacuumdb</span> and in parallel <span class=\"application\">reindexdb</span> (VaibhaveS, Michael Paquier, Fujii Masao, Nathan Bossart) <a class=\"ulink\" href=\"https://postgr.es/c/ef57a7135\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/5e0431c32\" target=\"_top\">§</a></p>",
    "<p>Reindexing other sessions' temporary tables cannot work, but the check to skip them was missing in some code paths, leading to unwanted failures.</p>"
  ],
  [
    "<p>Allow inspection of sequence relations in relevant functions of <code class=\"filename\">contrib/pageinspect</code> and <code class=\"filename\">contrib/pgstattuple</code> (Nathan Bossart, Ayush Vatsa) <a class=\"ulink\" href=\"https://postgr.es/c/dd5670fa5\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/e0277d90a\" target=\"_top\">§</a></p>",
    "<p>This had been allowed in the past, but it got broken during the introduction of non-default access methods for tables.</p>"
  ],
  [
    "<p>Fix incorrect LLVM-generated code on ARM64 platforms (Thomas Munro, Anthonin Bonnefoy) <a class=\"ulink\" href=\"https://postgr.es/c/50c1453a3\" target=\"_top\">§</a></p>",
    "<p>When using JIT compilation on ARM platforms, the generated code could not support relocation distances exceeding 32 bits, allowing unlucky placement of generated code to cause server crashes on large-memory systems.</p>"
  ],
  [
    "<p>Fix a few places that assumed that process start time (represented as a <code class=\"type\">time_t</code>) will fit into a <code class=\"type\">long</code> value (Max Johnson, Nathan Bossart) <a class=\"ulink\" href=\"https://postgr.es/c/c91d0af0a\" target=\"_top\">§</a></p>",
    "<p>On platforms where <code class=\"type\">long</code> is 32 bits (notably Windows), this coding would fail after Y2038. Most of the failures appear only cosmetic, but notably <code class=\"literal\">pg_ctl start</code> would hang.</p>"
  ],
  [
    "<p>Prevent <span class=\"quote\">“<span class=\"quote\">nothing provides perl(PostgreSQL::Test::Utils)</span>”</span> failures while building RPM packages of <span class=\"productname\">PostgreSQL</span> (Noah Misch) <a class=\"ulink\" href=\"https://postgr.es/c/646b16bca\" target=\"_top\">§</a></p>"
  ],
  [
    "<p>Fix building with Strawberry Perl on Windows (Andrew Dunstan) <a class=\"ulink\" href=\"https://postgr.es/c/d94e3b33e\" target=\"_top\">§</a></p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2024b (Tom Lane) <a class=\"ulink\" href=\"https://postgr.es/c/8f1759c9b\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/205813da4\" target=\"_top\">§</a></p>",
    "<p>This <span class=\"application\">tzdata</span> release changes the old System-V-compatibility zone names to duplicate the corresponding geographic zones; for example <code class=\"literal\">PST8PDT</code> is now an alias for <code class=\"literal\">America/Los_Angeles</code>. The main visible consequence is that for timestamps before the introduction of standardized time zones, the zone is considered to represent local mean solar time for the named location. For example, in <code class=\"literal\">PST8PDT</code>, <code class=\"type\">timestamptz</code> input such as <code class=\"literal\">1801-01-01 00:00</code> would previously have been rendered as <code class=\"literal\">1801-01-01 00:00:00-08</code>, but now it is rendered as <code class=\"literal\">1801-01-01 00:00:00-07:52:58</code>.</p>",
    "<p>Also, historical corrections for Mexico, Mongolia, and Portugal. Notably, <code class=\"literal\">Asia/Choibalsan</code> is now an alias for <code class=\"literal\">Asia/Ulaanbaatar</code> rather than being a separate zone, mainly because the differences between those zones were found to be based on untrustworthy data.</p>"
  ]
]