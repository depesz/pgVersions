[
  [
    "<p>Repair possible failure to update hint bits on\n          disk</p>",
    "<p>Under rare circumstances this oversight could lead to\n          <span class=\"quote\">&#x201C;<span class=\"quote\">could not access\n          transaction status</span>&#x201D;</span> failures, which\n          qualifies it as a potential-data-loss bug.</p>"
  ],
  [
    "<p>Ensure that hashed outer join does not miss tuples</p>",
    "<p>Very large left joins using a hash join plan could\n          fail to output unmatched left-side rows given just the\n          right data distribution.</p>"
  ],
  [
    "<p>Disallow running pg_ctl as root</p>",
    "<p>This is to guard against any possible security\n          issues.</p>"
  ],
  [
    "<p>Avoid using temp files in /tmp in\n          make_oidjoins_check</p>",
    "<p>This has been reported as a security issue, though\n          it's hardly worthy of concern since there is no reason\n          for non-developers to use this script anyway.</p>"
  ]
]