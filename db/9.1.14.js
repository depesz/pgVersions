[
  [
    "<p>Correctly initialize padding bytes in <code class=\"filename\">contrib/btree_gist</code> indexes on\n          <code class=\"type\">bit</code> columns (Heikki\n          Linnakangas)</p>",
    "<p>This error could result in incorrect query results due\n          to values that should compare equal not being seen as\n          equal. Users with GiST indexes on <code class=\"type\">bit</code> or <code class=\"type\">bit\n          varying</code> columns should <code class=\"command\">REINDEX</code> those indexes after installing\n          this update.</p>"
  ],
  [
    "<p>Protect against torn pages when deleting GIN list\n          pages (Heikki Linnakangas)</p>",
    "<p>This fix prevents possible index corruption if a\n          system crash occurs while the page update is being\n          written to disk.</p>"
  ],
  [
    "<p>Don't clear the right-link of a GiST index page while\n          replaying updates from WAL (Heikki Linnakangas)</p>",
    "<p>This error could lead to transiently wrong answers\n          from GiST index scans performed in Hot Standby.</p>"
  ],
  [
    "<p>Fix feedback status when <a class=\"xref\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-replication.html#GUC-HOT-STANDBY-FEEDBACK\">\n          hot_standby_feedback</a> is turned off on-the-fly (Simon\n          Riggs)</p>"
  ],
  [
    "<p>Fix possibly-incorrect cache invalidation during\n          nested calls to <code class=\"function\">ReceiveSharedInvalidMessages</code> (Andres\n          Freund)</p>"
  ],
  [
    "<p>Fix <span class=\"quote\">&#x201C;<span class=\"quote\">could not\n          find pathkey item to sort</span>&#x201D;</span> planner failures\n          with <code class=\"literal\">UNION ALL</code> over\n          subqueries reading from tables with inheritance children\n          (Tom Lane)</p>"
  ],
  [
    "<p>Don't assume a subquery's output is unique if there's\n          a set-returning function in its targetlist (David\n          Rowley)</p>",
    "<p>This oversight could lead to misoptimization of\n          constructs like <code class=\"literal\">WHERE x IN (SELECT\n          y, generate_series(1,10) FROM t GROUP BY y)</code>.</p>"
  ],
  [
    "<p>Fix failure to detoast fields in composite elements of\n          structured types (Tom Lane)</p>",
    "<p>This corrects cases where TOAST pointers could be\n          copied into other tables without being dereferenced. If\n          the original data is later deleted, it would lead to\n          errors like <span class=\"quote\">&#x201C;<span class=\"quote\">missing chunk number 0 for toast value\n          ...</span>&#x201D;</span> when the now-dangling pointer is\n          used.</p>"
  ],
  [
    "<p>Fix <span class=\"quote\">&#x201C;<span class=\"quote\">record\n          type has not been registered</span>&#x201D;</span> failures with\n          whole-row references to the output of Append plan nodes\n          (Tom Lane)</p>"
  ],
  [
    "<p>Fix possible crash when invoking a user-defined\n          function while rewinding a cursor (Tom Lane)</p>"
  ],
  [
    "<p>Fix query-lifespan memory leak while evaluating the\n          arguments for a function in <code class=\"literal\">FROM</code> (Tom Lane)</p>"
  ],
  [
    "<p>Fix session-lifespan memory leaks in\n          regular-expression processing (Tom Lane, Arthur O'Dwyer,\n          Greg Stark)</p>"
  ],
  [
    "<p>Fix data encoding error in <code class=\"filename\">hungarian.stop</code> (Tom Lane)</p>"
  ],
  [
    "<p>Prevent foreign tables from being created with OIDS\n          when <a class=\"xref\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-compatible.html#GUC-DEFAULT-WITH-OIDS\">default_with_oids</a>\n          is true (Etsuro Fujita)</p>"
  ],
  [
    "<p>Fix liveness checks for rows that were inserted in the\n          current transaction and then deleted by a now-rolled-back\n          subtransaction (Andres Freund)</p>",
    "<p>This could cause problems (at least spurious warnings,\n          and at worst an infinite loop) if <code class=\"command\">CREATE INDEX</code> or <code class=\"command\">CLUSTER</code> were done later in the same\n          transaction.</p>"
  ],
  [
    "<p>Clear <code class=\"structname\">pg_stat_activity</code>.<code class=\"structfield\">xact_start</code> during <code class=\"command\">PREPARE TRANSACTION</code> (Andres Freund)</p>",
    "<p>After the <code class=\"command\">PREPARE</code>, the\n          originating session is no longer in a transaction, so it\n          should not continue to display a transaction start\n          time.</p>"
  ],
  [
    "<p>Fix <code class=\"command\">REASSIGN OWNED</code> to not\n          fail for text search objects (&#xC1;lvaro Herrera)</p>"
  ],
  [
    "<p>Block signals during postmaster startup (Tom Lane)</p>",
    "<p>This ensures that the postmaster will properly clean\n          up after itself if, for example, it receives <span class=\"systemitem\">SIGINT</span> while still starting up.</p>"
  ],
  [
    "<p>Fix client host name lookup when processing\n          <code class=\"filename\">pg_hba.conf</code> entries that\n          specify host names instead of IP addresses (Tom Lane)</p>",
    "<p>Ensure that reverse-DNS lookup failures are reported,\n          instead of just silently not matching such entries. Also\n          ensure that we make only one reverse-DNS lookup attempt\n          per connection, not one per host name entry, which is\n          what previously happened if the lookup attempts\n          failed.</p>"
  ],
  [
    "<p>Secure Unix-domain sockets of temporary postmasters\n          started during <code class=\"literal\">make check</code>\n          (Noah Misch)</p>",
    "<p>Any local user able to access the socket file could\n          connect as the server's bootstrap superuser, then proceed\n          to execute arbitrary code as the operating-system user\n          running the test, as we previously noted in\n          CVE-2014-0067. This change defends against that risk by\n          placing the server's socket in a temporary, mode 0700\n          subdirectory of <code class=\"filename\">/tmp</code>. The\n          hazard remains however on platforms where Unix sockets\n          are not supported, notably Windows, because then the\n          temporary postmaster must accept local TCP\n          connections.</p>",
    "<p>A useful side effect of this change is to simplify\n          <code class=\"literal\">make check</code> testing in builds\n          that override <code class=\"literal\">DEFAULT_PGSOCKET_DIR</code>. Popular\n          non-default values like <code class=\"filename\">/var/run/postgresql</code> are often not\n          writable by the build user, requiring workarounds that\n          will no longer be necessary.</p>"
  ],
  [
    "<p>Fix tablespace creation WAL replay to work on Windows\n          (MauMau)</p>"
  ],
  [
    "<p>Fix detection of socket creation failures on Windows\n          (Bruce Momjian)</p>"
  ],
  [
    "<p>On Windows, allow new sessions to absorb values of\n          PGC_BACKEND parameters (such as <a class=\"xref\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-logging.html#GUC-LOG-CONNECTIONS\">log_connections</a>)\n          from the configuration file (Amit Kapila)</p>",
    "<p>Previously, if such a parameter were changed in the\n          file post-startup, the change would have no effect.</p>"
  ],
  [
    "<p>Properly quote executable path names on Windows\n          (Nikhil Deshpande)</p>",
    "<p>This oversight could cause <span class=\"application\">initdb</span> and <span class=\"application\">pg_upgrade</span> to fail on Windows, if\n          the installation path contained both spaces and\n          <code class=\"literal\">@</code> signs.</p>"
  ],
  [
    "<p>Fix linking of <span class=\"application\">libpython</span> on macOS (Tom Lane)</p>",
    "<p>The method we previously used can fail with the Python\n          library supplied by Xcode 5.0 and later.</p>"
  ],
  [
    "<p>Avoid buffer bloat in <span class=\"application\">libpq</span> when the server consistently\n          sends data faster than the client can absorb it\n          (Shin-ichi Morita, Tom Lane)</p>",
    "<p><span class=\"application\">libpq</span> could be\n          coerced into enlarging its input buffer until it runs out\n          of memory (which would be reported misleadingly as\n          <span class=\"quote\">&#x201C;<span class=\"quote\">lost\n          synchronization with server</span>&#x201D;</span>). Under\n          ordinary circumstances it's quite far-fetched that data\n          could be continuously transmitted more quickly than the\n          <code class=\"function\">recv()</code> loop can absorb it,\n          but this has been observed when the client is\n          artificially slowed by scheduler constraints.</p>"
  ],
  [
    "<p>Ensure that LDAP lookup attempts in <span class=\"application\">libpq</span> time out as intended (Laurenz\n          Albe)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">ecpg</span> to do the\n          right thing when an array of <code class=\"type\">char\n          *</code> is the target for a FETCH statement returning\n          more than one row, as well as some other array-handling\n          fixes (Ashutosh Bapat)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_restore</span>'s\n          processing of old-style large object comments (Tom\n          Lane)</p>",
    "<p>A direct-to-database restore from an archive file\n          generated by a pre-9.0 version of <span class=\"application\">pg_dump</span> would usually fail if the\n          archive contained more than a few comments for large\n          objects.</p>"
  ],
  [
    "<p>In <code class=\"filename\">contrib/pgcrypto</code>\n          functions, ensure sensitive information is cleared from\n          stack variables before returning (Marko Kreen)</p>"
  ],
  [
    "<p>In <code class=\"filename\">contrib/uuid-ossp</code>,\n          cache the state of the OSSP UUID library across calls\n          (Tom Lane)</p>",
    "<p>This improves the efficiency of UUID generation and\n          reduces the amount of entropy drawn from <code class=\"filename\">/dev/urandom</code>, on platforms that have\n          that.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2014e for DST law\n          changes in Crimea, Egypt, and Morocco.</p>"
  ]
]