[
  [
    "<p>Restore creation of OID column in CREATE TABLE AS /\n          SELECT INTO</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_dump</span> core dump\n          when dumping views having comments</p>"
  ],
  [
    "<p>Dump DEFERRABLE/INITIALLY DEFERRED constraints\n          properly</p>"
  ],
  [
    "<p>Fix UPDATE when child table's column numbering differs\n          from parent</p>"
  ],
  [
    "<p>Increase default value of max_fsm_relations</p>"
  ],
  [
    "<p>Fix problem when fetching backwards in a cursor for a\n          single-row query</p>"
  ],
  [
    "<p>Make backward fetch work properly with cursor on\n          SELECT DISTINCT query</p>"
  ],
  [
    "<p>Fix problems with loading <span class=\"application\">pg_dump</span> files containing contrib/lo\n          usage</p>"
  ],
  [
    "<p>Fix problem with all-numeric user names</p>"
  ],
  [
    "<p>Fix possible memory leak and core dump during\n          disconnect in libpgtcl</p>"
  ],
  [
    "<p>Make plpython's spi_execute command handle nulls\n          properly (Andrew Bosma)</p>"
  ],
  [
    "<p>Adjust plpython error reporting so that its regression\n          test passes again</p>"
  ],
  [
    "<p>Work with bison 1.875</p>"
  ],
  [
    "<p>Handle mixed-case names properly in plpgsql's %type\n          (Neil)</p>"
  ],
  [
    "<p>Fix core dump in pltcl when executing a query\n          rewritten by a rule</p>"
  ],
  [
    "<p>Repair array subscript overruns (per report from\n          Yichen Xie)</p>"
  ],
  [
    "<p>Reduce MAX_TIME_PRECISION from 13 to 10 in\n          floating-point case</p>"
  ],
  [
    "<p>Correctly case-fold variable names in per-database and\n          per-user settings</p>"
  ],
  [
    "<p>Fix coredump in plpgsql's RETURN NEXT when SELECT into\n          record returns no rows</p>"
  ],
  [
    "<p>Fix outdated use of pg_type.typprtlen in python client\n          interface</p>"
  ],
  [
    "<p>Correctly handle fractional seconds in timestamps in\n          JDBC driver</p>"
  ],
  [
    "<p>Improve performance of getImportedKeys() in JDBC</p>"
  ],
  [
    "<p>Make shared-library symlinks work standardly on HPUX\n          (Giles)</p>"
  ],
  [
    "<p>Repair inconsistent rounding behavior for timestamp,\n          time, interval</p>"
  ],
  [
    "<p>SSL negotiation fixes (Nathan Mueller)</p>"
  ],
  [
    "<p>Make libpq's ~/.pgpass feature work when connecting\n          with PQconnectDB</p>"
  ],
  [
    "<p>Update my2pg, ora2pg</p>"
  ],
  [
    "<p>Translation updates</p>"
  ],
  [
    "<p>Add casts between types lo and oid in contrib/lo</p>"
  ],
  [
    "<p>fastpath code now checks for privilege to call\n          function</p>"
  ]
]