[
  [
    "<p>Protect against indirect security threats caused by\n          index functions changing session-local state (Gurjeet\n          Singh, Tom)</p>",
    "<p>This change prevents allegedly-immutable index\n          functions from possibly subverting a superuser's session\n          (CVE-2009-4136).</p>"
  ],
  [
    "<p>Reject SSL certificates containing an embedded null\n          byte in the common name (CN) field (Magnus)</p>",
    "<p>This prevents unintended matching of a certificate to\n          a server or client name during SSL validation\n          (CVE-2009-4034).</p>"
  ],
  [
    "<p>Fix possible crash during backend-startup-time cache\n          initialization (Tom)</p>"
  ],
  [
    "<p>Prevent signals from interrupting <code class=\"literal\">VACUUM</code> at unsafe times (Alvaro)</p>",
    "<p>This fix prevents a PANIC if a <code class=\"literal\">VACUUM FULL</code> is canceled after it's\n          already committed its tuple movements, as well as\n          transient errors if a plain <code class=\"literal\">VACUUM</code> is interrupted after having\n          truncated the table.</p>"
  ],
  [
    "<p>Fix possible crash due to integer overflow in hash\n          table size calculation (Tom)</p>",
    "<p>This could occur with extremely large planner\n          estimates for the size of a hashjoin's result.</p>"
  ],
  [
    "<p>Fix very rare crash in <code class=\"type\">inet</code>/<code class=\"type\">cidr</code>\n          comparisons (Chris Mikkelson)</p>"
  ],
  [
    "<p>Ensure that shared tuple-level locks held by prepared\n          transactions are not ignored (Heikki)</p>"
  ],
  [
    "<p>Fix premature drop of temporary files used for a\n          cursor that is accessed within a subtransaction\n          (Heikki)</p>"
  ],
  [
    "<p>Fix PAM password processing to be more robust\n          (Tom)</p>",
    "<p>The previous code is known to fail with the\n          combination of the Linux <code class=\"literal\">pam_krb5</code> PAM module with Microsoft\n          Active Directory as the domain controller. It might have\n          problems elsewhere too, since it was making unjustified\n          assumptions about what arguments the PAM stack would pass\n          to it.</p>"
  ],
  [
    "<p>Fix processing of ownership dependencies during\n          <code class=\"literal\">CREATE OR REPLACE FUNCTION</code>\n          (Tom)</p>"
  ],
  [
    "<p>Ensure that Perl arrays are properly converted to\n          <span class=\"productname\">PostgreSQL</span> arrays when\n          returned by a set-returning PL/Perl function (Andrew\n          Dunstan, Abhijit Menon-Sen)</p>",
    "<p>This worked correctly already for non-set-returning\n          functions.</p>"
  ],
  [
    "<p>Fix rare crash in exception processing in PL/Python\n          (Peter)</p>"
  ],
  [
    "<p>Ensure <span class=\"application\">psql</span>'s flex\n          module is compiled with the correct system header\n          definitions (Tom)</p>",
    "<p>This fixes build failures on platforms where\n          <code class=\"literal\">--enable-largefile</code> causes\n          incompatible changes in the generated code.</p>"
  ],
  [
    "<p>Make the postmaster ignore any <code class=\"literal\">application_name</code> parameter in connection\n          request packets, to improve compatibility with future\n          libpq versions (Tom)</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2009s for DST law\n          changes in Antarctica, Argentina, Bangladesh, Fiji,\n          Novokuznetsk, Pakistan, Palestine, Samoa, Syria; also\n          historical corrections for Hong Kong.</p>"
  ]
]