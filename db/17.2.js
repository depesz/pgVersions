[
  [
    "<p>Repair ABI break for extensions that work with struct <code class=\"structname\">ResultRelInfo</code> (Tom Lane) <a class=\"ulink\" href=\"https://postgr.es/c/6bfacd368\" target=\"_top\">§</a></p>",
    "<p>Last week's minor releases unintentionally broke binary compatibility with <span class=\"application\">timescaledb</span> and several other extensions. Restore the affected structure to its previous size, so that such extensions need not be rebuilt.</p>"
  ],
  [
    "<p>Restore functionality of <code class=\"command\">ALTER {ROLE|DATABASE} SET role</code> (Tom Lane, Noah Misch) <a class=\"ulink\" href=\"https://postgr.es/c/1c05004a8\" target=\"_top\">§</a></p>",
    "<p>The fix for CVE-2024-10978 accidentally caused settings for <code class=\"varname\">role</code> to not be applied if they come from non-interactive sources, including previous <code class=\"command\">ALTER {ROLE|DATABASE}</code> commands and the <code class=\"varname\">PGOPTIONS</code> environment variable.</p>"
  ],
  [
    "<p>Fix cases where a logical replication slot's <code class=\"structfield\">restart_lsn</code> could go backwards (Masahiko Sawada) <a class=\"ulink\" href=\"https://postgr.es/c/568e78a65\" target=\"_top\">§</a></p>",
    "<p>Previously, restarting logical replication could sometimes cause the slot's restart point to be recomputed as an older value than had previously been advertised in <code class=\"structname\">pg_replication_slots</code>. This is bad, since for example WAL files might have been removed on the basis of the later <code class=\"structfield\">restart_lsn</code> value, in which case replication would fail to restart.</p>"
  ],
  [
    "<p>Avoid deleting still-needed WAL files during <span class=\"application\">pg_rewind</span> (Polina Bungina, Alexander Kukushkin) <a class=\"ulink\" href=\"https://postgr.es/c/cb844d66b\" target=\"_top\">§</a></p>",
    "<p>Previously, in unlucky cases, it was possible for <span class=\"application\">pg_rewind</span> to remove important WAL files from the rewound demoted primary. In particular this happens if those files have been marked for archival (i.e., their <code class=\"filename\">.ready</code> files were created) but not yet archived. Then the newly promoted node no longer has such files because of them having been recycled, but likely they are needed for recovery in the demoted node. If <span class=\"application\">pg_rewind</span> removes them, recovery is not possible anymore.</p>"
  ],
  [
    "<p>Fix race conditions associated with dropping shared statistics entries (Kyotaro Horiguchi, Michael Paquier) <a class=\"ulink\" href=\"https://postgr.es/c/1d6a03ea4\" target=\"_top\">§</a></p>",
    "<p>These bugs could lead to loss of statistics data, assertion failures, or <span class=\"quote\">“<span class=\"quote\">can only drop stats once</span>”</span> errors.</p>"
  ],
  [
    "<p>Count index scans in <code class=\"filename\">contrib/bloom</code> indexes in the statistics views, such as the <code class=\"structname\">pg_stat_user_indexes</code>.<code class=\"structfield\">idx_scan</code> counter (Masahiro Ikeda) <a class=\"ulink\" href=\"https://postgr.es/c/7af6d1306\" target=\"_top\">§</a></p>"
  ],
  [
    "<p>Fix crash when checking to see if an index's opclass options have changed (Alexander Korotkov) <a class=\"ulink\" href=\"https://postgr.es/c/a6fa869cf\" target=\"_top\">§</a></p>",
    "<p>Some forms of <code class=\"command\">ALTER TABLE</code> would fail if the table has an index with non-default operator class options.</p>"
  ],
  [
    "<p>Avoid assertion failure caused by disconnected NFA sub-graphs in regular expression parsing (Tom Lane) <a class=\"ulink\" href=\"https://postgr.es/c/5f28e6ba7\" target=\"_top\">§</a></p>",
    "<p>This bug does not appear to have any visible consequences in non-assert builds.</p>"
  ]
]