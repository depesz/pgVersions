[
  [
    "<p>Fix buffer overruns in <code class=\"function\">to_char()</code> (Bruce Momjian)</p>",
    "<p>When <code class=\"function\">to_char()</code> processes\n          a numeric formatting template calling for a large number\n          of digits, <span class=\"productname\">PostgreSQL</span>\n          would read past the end of a buffer. When processing a\n          crafted timestamp formatting template, <span class=\"productname\">PostgreSQL</span> would write past the end\n          of a buffer. Either case could crash the server. We have\n          not ruled out the possibility of attacks that lead to\n          privilege escalation, though they seem unlikely.\n          (CVE-2015-0241)</p>"
  ],
  [
    "<p>Fix buffer overrun in replacement <code class=\"function\">*printf()</code> functions (Tom Lane)</p>",
    "<p><span class=\"productname\">PostgreSQL</span> includes a\n          replacement implementation of <code class=\"function\">printf</code> and related functions. This code\n          will overrun a stack buffer when formatting a floating\n          point number (conversion specifiers <code class=\"literal\">e</code>, <code class=\"literal\">E</code>,\n          <code class=\"literal\">f</code>, <code class=\"literal\">F</code>, <code class=\"literal\">g</code> or\n          <code class=\"literal\">G</code>) with requested precision\n          greater than about 500. This will crash the server, and\n          we have not ruled out the possibility of attacks that\n          lead to privilege escalation. A database user can trigger\n          such a buffer overrun through the <code class=\"function\">to_char()</code> SQL function. While that is\n          the only affected core <span class=\"productname\">PostgreSQL</span> functionality, extension\n          modules that use printf-family functions may be at risk\n          as well.</p>",
    "<p>This issue primarily affects <span class=\"productname\">PostgreSQL</span> on Windows. <span class=\"productname\">PostgreSQL</span> uses the system\n          implementation of these functions where adequate, which\n          it is on other modern platforms. (CVE-2015-0242)</p>"
  ],
  [
    "<p>Fix buffer overruns in <code class=\"filename\">contrib/pgcrypto</code> (Marko Tiikkaja, Noah\n          Misch)</p>",
    "<p>Errors in memory size tracking within the <code class=\"filename\">pgcrypto</code> module permitted stack buffer\n          overruns and improper dependence on the contents of\n          uninitialized memory. The buffer overrun cases can crash\n          the server, and we have not ruled out the possibility of\n          attacks that lead to privilege escalation.\n          (CVE-2015-0243)</p>"
  ],
  [
    "<p>Fix possible loss of frontend/backend protocol\n          synchronization after an error (Heikki Linnakangas)</p>",
    "<p>If any error occurred while the server was in the\n          middle of reading a protocol message from the client, it\n          could lose synchronization and incorrectly try to\n          interpret part of the message's data as a new protocol\n          message. An attacker able to submit crafted binary data\n          within a command parameter might succeed in injecting his\n          own SQL commands this way. Statement timeout and query\n          cancellation are the most likely sources of errors\n          triggering this scenario. Particularly vulnerable are\n          applications that use a timeout and also submit arbitrary\n          user-crafted data as binary query parameters. Disabling\n          statement timeout will reduce, but not eliminate, the\n          risk of exploit. Our thanks to Emil Lenngren for\n          reporting this issue. (CVE-2015-0244)</p>"
  ],
  [
    "<p>Fix information leak via constraint-violation error\n          messages (Stephen Frost)</p>",
    "<p>Some server error messages show the values of columns\n          that violate a constraint, such as a unique constraint.\n          If the user does not have <code class=\"literal\">SELECT</code> privilege on all columns of the\n          table, this could mean exposing values that the user\n          should not be able to see. Adjust the code so that values\n          are displayed only when they came from the SQL command or\n          could be selected by the user. (CVE-2014-8161)</p>"
  ],
  [
    "<p>Lock down regression testing's temporary installations\n          on Windows (Noah Misch)</p>",
    "<p>Use SSPI authentication to allow connections only from\n          the OS user who launched the test suite. This closes on\n          Windows the same vulnerability previously closed on other\n          platforms, namely that other users might be able to\n          connect to the test postmaster. (CVE-2014-0067)</p>"
  ],
  [
    "<p>Avoid possible data corruption if <code class=\"command\">ALTER DATABASE SET TABLESPACE</code> is used to\n          move a database to a new tablespace and then shortly\n          later move it back to its original tablespace (Tom\n          Lane)</p>"
  ],
  [
    "<p>Avoid corrupting tables when <code class=\"command\">ANALYZE</code> inside a transaction is rolled\n          back (Andres Freund, Tom Lane, Michael Paquier)</p>",
    "<p>If the failing transaction had earlier removed the\n          last index, rule, or trigger from the table, the table\n          would be left in a corrupted state with the relevant\n          <code class=\"structname\">pg_class</code> flags not set\n          though they should be.</p>"
  ],
  [
    "<p>Fix use-of-already-freed-memory problem in\n          EvalPlanQual processing (Tom Lane)</p>",
    "<p>In <code class=\"literal\">READ COMMITTED</code> mode,\n          queries that lock or update recently-updated rows could\n          crash as a result of this bug.</p>"
  ],
  [
    "<p>Fix planning of <code class=\"command\">SELECT FOR\n          UPDATE</code> when using a partial index on a child table\n          (Kyotaro Horiguchi)</p>",
    "<p>In <code class=\"literal\">READ COMMITTED</code> mode,\n          <code class=\"command\">SELECT FOR UPDATE</code> must also\n          recheck the partial index's <code class=\"literal\">WHERE</code> condition when rechecking a\n          recently-updated row to see if it still satisfies the\n          query's <code class=\"literal\">WHERE</code> condition.\n          This requirement was missed if the index belonged to an\n          inheritance child table, so that it was possible to\n          incorrectly return rows that no longer satisfy the query\n          condition.</p>"
  ],
  [
    "<p>Fix corner case wherein <code class=\"command\">SELECT\n          FOR UPDATE</code> could return a row twice, and possibly\n          miss returning other rows (Tom Lane)</p>",
    "<p>In <code class=\"literal\">READ COMMITTED</code> mode, a\n          <code class=\"command\">SELECT FOR UPDATE</code> that is\n          scanning an inheritance tree could incorrectly return a\n          row from a prior child table instead of the one it should\n          return from a later child table.</p>"
  ],
  [
    "<p>Reject duplicate column names in the\n          referenced-columns list of a <code class=\"literal\">FOREIGN KEY</code> declaration (David\n          Rowley)</p>",
    "<p>This restriction is per SQL standard. Previously we\n          did not reject the case explicitly, but later on the code\n          would fail with bizarre-looking errors.</p>"
  ],
  [
    "<p>Fix bugs in raising a <code class=\"type\">numeric</code> value to a large integral power\n          (Tom Lane)</p>",
    "<p>The previous code could get a wrong answer, or consume\n          excessive amounts of time and memory before realizing\n          that the answer must overflow.</p>"
  ],
  [
    "<p>In <code class=\"function\">numeric_recv()</code>,\n          truncate away any fractional digits that would be hidden\n          according to the value's <code class=\"literal\">dscale</code> field (Tom Lane)</p>",
    "<p>A <code class=\"type\">numeric</code> value's display\n          scale (<code class=\"literal\">dscale</code>) should never\n          be less than the number of nonzero fractional digits; but\n          apparently there's at least one broken client application\n          that transmits binary <code class=\"type\">numeric</code>\n          values in which that's true. This leads to strange\n          behavior since the extra digits are taken into account by\n          arithmetic operations even though they aren't printed.\n          The least risky fix seems to be to truncate away such\n          <span class=\"quote\">&#x201C;<span class=\"quote\">hidden</span>&#x201D;</span> digits on receipt, so that\n          the value is indeed what it prints as.</p>"
  ],
  [
    "<p>Reject out-of-range numeric timezone specifications\n          (Tom Lane)</p>",
    "<p>Simple numeric timezone specifications exceeding +/-\n          168 hours (one week) would be accepted, but could then\n          cause null-pointer dereference crashes in certain\n          operations. There's no use-case for such large UTC\n          offsets, so reject them.</p>"
  ],
  [
    "<p>Fix bugs in <code class=\"type\">tsquery</code>\n          <code class=\"literal\">@&gt;</code> <code class=\"type\">tsquery</code> operator (Heikki Linnakangas)</p>",
    "<p>Two different terms would be considered to match if\n          they had the same CRC. Also, if the second operand had\n          more terms than the first, it would be assumed not to be\n          contained in the first; which is wrong since it might\n          contain duplicate terms.</p>"
  ],
  [
    "<p>Improve ispell dictionary's defenses against bad affix\n          files (Tom Lane)</p>"
  ],
  [
    "<p>Allow more than 64K phrases in a thesaurus dictionary\n          (David Boutin)</p>",
    "<p>The previous coding could crash on an oversize\n          dictionary, so this was deemed a back-patchable bug fix\n          rather than a feature addition.</p>"
  ],
  [
    "<p>Fix namespace handling in <code class=\"function\">xpath()</code> (Ali Akbar)</p>",
    "<p>Previously, the <code class=\"type\">xml</code> value\n          resulting from an <code class=\"function\">xpath()</code>\n          call would not have namespace declarations if the\n          namespace declarations were attached to an ancestor\n          element in the input <code class=\"type\">xml</code> value,\n          rather than to the specific element being returned.\n          Propagate the ancestral declaration so that the result is\n          correct when considered in isolation.</p>"
  ],
  [
    "<p>Fix planner problems with nested append relations,\n          such as inherited tables within <code class=\"literal\">UNION ALL</code> subqueries (Tom Lane)</p>"
  ],
  [
    "<p>Fail cleanly when a GiST index tuple doesn't fit on a\n          page, rather than going into infinite recursion (Andrew\n          Gierth)</p>"
  ],
  [
    "<p>Exempt tables that have per-table <code class=\"varname\">cost_limit</code> and/or <code class=\"varname\">cost_delay</code> settings from autovacuum's\n          global cost balancing rules (&#xC1;lvaro Herrera)</p>",
    "<p>The previous behavior resulted in basically ignoring\n          these per-table settings, which was unintended. Now, a\n          table having such settings will be vacuumed using those\n          settings, independently of what is going on in other\n          autovacuum workers. This may result in heavier total I/O\n          load than before, so such settings should be re-examined\n          for sanity.</p>"
  ],
  [
    "<p>Avoid wholesale autovacuuming when autovacuum is\n          nominally off (Tom Lane)</p>",
    "<p>Even when autovacuum is nominally off, we will still\n          launch autovacuum worker processes to vacuum tables that\n          are at risk of XID wraparound. However, such a worker\n          process then proceeded to vacuum all tables in the target\n          database, if they met the usual thresholds for\n          autovacuuming. This is at best pretty unexpected; at\n          worst it delays response to the wraparound threat. Fix it\n          so that if autovacuum is turned off, workers <span class=\"emphasis\"><em>only</em></span> do anti-wraparound\n          vacuums and not any other work.</p>"
  ],
  [
    "<p>Fix race condition between hot standby queries and\n          replaying a full-page image (Heikki Linnakangas)</p>",
    "<p>This mistake could result in transient errors in\n          queries being executed in hot standby.</p>"
  ],
  [
    "<p>Fix several cases where recovery logic improperly\n          ignored WAL records for <code class=\"literal\">COMMIT/ABORT PREPARED</code> (Heikki\n          Linnakangas)</p>",
    "<p>The most notable oversight was that <code class=\"varname\">recovery_target_xid</code> could not be used to\n          stop at a two-phase commit.</p>"
  ],
  [
    "<p>Avoid creating unnecessary <code class=\"filename\">.ready</code> marker files for timeline\n          history files (Fujii Masao)</p>"
  ],
  [
    "<p>Fix possible null pointer dereference when an empty\n          prepared statement is used and the <code class=\"varname\">log_statement</code> setting is <code class=\"literal\">mod</code> or <code class=\"literal\">ddl</code>\n          (Fujii Masao)</p>"
  ],
  [
    "<p>Change <span class=\"quote\">&#x201C;<span class=\"quote\">pgstat\n          wait timeout</span>&#x201D;</span> warning message to be LOG\n          level, and rephrase it to be more understandable (Tom\n          Lane)</p>",
    "<p>This message was originally thought to be essentially\n          a can't-happen case, but it occurs often enough on our\n          slower buildfarm members to be a nuisance. Reduce it to\n          LOG level, and expend a bit more effort on the wording:\n          it now reads <span class=\"quote\">&#x201C;<span class=\"quote\">using stale statistics instead of current ones\n          because stats collector is not\n          responding</span>&#x201D;</span>.</p>"
  ],
  [
    "<p>Fix SPARC spinlock implementation to ensure\n          correctness if the CPU is being run in a non-TSO\n          coherency mode, as some non-Solaris kernels do (Andres\n          Freund)</p>"
  ],
  [
    "<p>Warn if macOS's <code class=\"function\">setlocale()</code> starts an unwanted extra\n          thread inside the postmaster (Noah Misch)</p>"
  ],
  [
    "<p>Fix processing of repeated <code class=\"literal\">dbname</code> parameters in <code class=\"function\">PQconnectdbParams()</code> (Alex Shulgin)</p>",
    "<p>Unexpected behavior ensued if the first occurrence of\n          <code class=\"literal\">dbname</code> contained a\n          connection string or URI to be expanded.</p>"
  ],
  [
    "<p>Ensure that <span class=\"application\">libpq</span>\n          reports a suitable error message on unexpected socket EOF\n          (Marko Tiikkaja, Tom Lane)</p>",
    "<p>Depending on kernel behavior, <span class=\"application\">libpq</span> might return an empty error\n          string rather than something useful when the server\n          unexpectedly closed the socket.</p>"
  ],
  [
    "<p>Clear any old error message during <code class=\"function\">PQreset()</code> (Heikki Linnakangas)</p>",
    "<p>If <code class=\"function\">PQreset()</code> is called\n          repeatedly, and the connection cannot be re-established,\n          error messages from the failed connection attempts kept\n          accumulating in the <code class=\"structname\">PGconn</code>'s error string.</p>"
  ],
  [
    "<p>Properly handle out-of-memory conditions while parsing\n          connection options in <span class=\"application\">libpq</span> (Alex Shulgin, Heikki\n          Linnakangas)</p>"
  ],
  [
    "<p>Fix array overrun in <span class=\"application\">ecpg</span>'s version of <code class=\"function\">ParseDateTime()</code> (Michael Paquier)</p>"
  ],
  [
    "<p>In <span class=\"application\">initdb</span>, give a\n          clearer error message if a password file is specified but\n          is empty (Mats Erik Andersson)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">psql</span>'s\n          <code class=\"command\">\\s</code> command to work nicely\n          with libedit, and add pager support (Stepan Rutz, Tom\n          Lane)</p>",
    "<p>When using libedit rather than readline, <code class=\"command\">\\s</code> printed the command history in a\n          fairly unreadable encoded format, and on recent libedit\n          versions might fail altogether. Fix that by printing the\n          history ourselves rather than having the library do it. A\n          pleasant side-effect is that the pager is used if\n          appropriate.</p>",
    "<p>This patch also fixes a bug that caused newline\n          encoding to be applied inconsistently when saving the\n          command history with libedit. Multiline history entries\n          written by older <span class=\"application\">psql</span>\n          versions will be read cleanly with this patch, but\n          perhaps not vice versa, depending on the exact libedit\n          versions involved.</p>"
  ],
  [
    "<p>Improve consistency of parsing of <span class=\"application\">psql</span>'s special variables (Tom\n          Lane)</p>",
    "<p>Allow variant spellings of <code class=\"literal\">on</code> and <code class=\"literal\">off</code>\n          (such as <code class=\"literal\">1</code>/<code class=\"literal\">0</code>) for <code class=\"literal\">ECHO_HIDDEN</code> and <code class=\"literal\">ON_ERROR_ROLLBACK</code>. Report a warning for\n          unrecognized values for <code class=\"literal\">COMP_KEYWORD_CASE</code>, <code class=\"literal\">ECHO</code>, <code class=\"literal\">ECHO_HIDDEN</code>, <code class=\"literal\">HISTCONTROL</code>, <code class=\"literal\">ON_ERROR_ROLLBACK</code>, and <code class=\"literal\">VERBOSITY</code>. Recognize all values for all\n          these variables case-insensitively; previously there was\n          a mishmash of case-sensitive and case-insensitive\n          behaviors.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">psql</span>'s\n          expanded-mode display to work consistently when using\n          <code class=\"literal\">border</code> = 3 and <code class=\"literal\">linestyle</code> = <code class=\"literal\">ascii</code> or <code class=\"literal\">unicode</code> (Stephen Frost)</p>"
  ],
  [
    "<p>Fix possible deadlock during parallel restore of a\n          schema-only dump (Robert Haas, Tom Lane)</p>"
  ],
  [
    "<p>Fix core dump in <code class=\"literal\">pg_dump\n          --binary-upgrade</code> on zero-column composite type\n          (Rushabh Lathia)</p>"
  ],
  [
    "<p>Fix block number checking in <code class=\"filename\">contrib/pageinspect</code>'s <code class=\"function\">get_raw_page()</code> (Tom Lane)</p>",
    "<p>The incorrect checking logic could prevent access to\n          some pages in non-main relation forks.</p>"
  ],
  [
    "<p>Fix <code class=\"filename\">contrib/pgcrypto</code>'s\n          <code class=\"function\">pgp_sym_decrypt()</code> to not\n          fail on messages whose length is 6 less than a power of 2\n          (Marko Tiikkaja)</p>"
  ],
  [
    "<p>Handle unexpected query results, especially NULLs,\n          safely in <code class=\"filename\">contrib/tablefunc</code>'s <code class=\"function\">connectby()</code> (Michael Paquier)</p>",
    "<p><code class=\"function\">connectby()</code> previously\n          crashed if it encountered a NULL key value. It now prints\n          that row but doesn't recurse further.</p>"
  ],
  [
    "<p>Avoid a possible crash in <code class=\"filename\">contrib/xml2</code>'s <code class=\"function\">xslt_process()</code> (Mark Simonetti)</p>",
    "<p><span class=\"application\">libxslt</span> seems to have\n          an undocumented dependency on the order in which\n          resources are freed; reorder our calls to avoid a\n          crash.</p>"
  ],
  [
    "<p>Numerous cleanups of warnings from Coverity static\n          code analyzer (Andres Freund, Tatsuo Ishii, Marko Kreen,\n          Tom Lane, Michael Paquier)</p>",
    "<p>These changes are mostly cosmetic but in some cases\n          fix corner-case bugs, for example a crash rather than a\n          proper error report after an out-of-memory failure. None\n          are believed to represent security issues.</p>"
  ],
  [
    "<p>Detect incompatible OpenLDAP versions during build\n          (Noah Misch)</p>",
    "<p>With OpenLDAP versions 2.4.24 through 2.4.31,\n          inclusive, <span class=\"productname\">PostgreSQL</span>\n          backends can crash at exit. Raise a warning during\n          <span class=\"application\">configure</span> based on the\n          compile-time OpenLDAP version number, and test the\n          crashing scenario in the <code class=\"filename\">contrib/dblink</code> regression test.</p>"
  ],
  [
    "<p>In non-MSVC Windows builds, ensure <code class=\"filename\">libpq.dll</code> is installed with execute\n          permissions (Noah Misch)</p>"
  ],
  [
    "<p>Make <span class=\"application\">pg_regress</span>\n          remove any temporary installation it created upon\n          successful exit (Tom Lane)</p>",
    "<p>This results in a very substantial reduction in disk\n          space usage during <code class=\"literal\">make\n          check-world</code>, since that sequence involves creation\n          of numerous temporary installations.</p>"
  ],
  [
    "<p>Support time zone abbreviations that change UTC offset\n          from time to time (Tom Lane)</p>",
    "<p>Previously, <span class=\"productname\">PostgreSQL</span> assumed that the UTC\n          offset associated with a time zone abbreviation (such as\n          <code class=\"literal\">EST</code>) never changes in the\n          usage of any particular locale. However this assumption\n          fails in the real world, so introduce the ability for a\n          zone abbreviation to represent a UTC offset that\n          sometimes changes. Update the zone abbreviation\n          definition files to make use of this feature in timezone\n          locales that have changed the UTC offset of their\n          abbreviations since 1970 (according to the IANA timezone\n          database). In such timezones, <span class=\"productname\">PostgreSQL</span> will now associate the\n          correct UTC offset with the abbreviation depending on the\n          given date.</p>"
  ],
  [
    "<p>Update time zone abbreviations lists (Tom Lane)</p>",
    "<p>Add CST (China Standard Time) to our lists. Remove\n          references to ADT as <span class=\"quote\">&#x201C;<span class=\"quote\">Arabia Daylight Time</span>&#x201D;</span>, an\n          abbreviation that's been out of use since 2007;\n          therefore, claiming there is a conflict with <span class=\"quote\">&#x201C;<span class=\"quote\">Atlantic Daylight\n          Time</span>&#x201D;</span> doesn't seem especially helpful. Fix\n          entirely incorrect GMT offsets for CKT (Cook Islands),\n          FJT, and FJST (Fiji); we didn't even have them on the\n          proper side of the date line.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2015a.</p>",
    "<p>The IANA timezone database has adopted abbreviations\n          of the form <code class=\"literal\">A<em class=\"replaceable\"><code>x</code></em>ST</code>/<code class=\"literal\">A<em class=\"replaceable\"><code>x</code></em>DT</code> for all\n          Australian time zones, reflecting what they believe to be\n          current majority practice Down Under. These names do not\n          conflict with usage elsewhere (other than ACST for Acre\n          Summer Time, which has been in disuse since 1994).\n          Accordingly, adopt these names into our <span class=\"quote\">&#x201C;<span class=\"quote\">Default</span>&#x201D;</span>\n          timezone abbreviation set. The <span class=\"quote\">&#x201C;<span class=\"quote\">Australia</span>&#x201D;</span>\n          abbreviation set now contains only CST, EAST, EST, SAST,\n          SAT, and WST, all of which are thought to be mostly\n          historical usage. Note that SAST has also been changed to\n          be South Africa Standard Time in the <span class=\"quote\">&#x201C;<span class=\"quote\">Default</span>&#x201D;</span>\n          abbreviation set.</p>",
    "<p>Also, add zone abbreviations SRET (Asia/Srednekolymsk)\n          and XJT (Asia/Urumqi), and use WSST/WSDT for western\n          Samoa. Also, there were DST law changes in Chile, Mexico,\n          the Turks &amp; Caicos Islands (America/Grand_Turk), and\n          Fiji. There is a new zone Pacific/Bougainville for\n          portions of Papua New Guinea. Also, numerous corrections\n          for historical (pre-1970) time zone data.</p>"
  ]
]