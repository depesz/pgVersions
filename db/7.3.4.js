[
  [
    "<p>Repair breakage in timestamp-to-date conversion for\n          dates before 2000</p>"
  ],
  [
    "<p>Prevent rare possibility of server startup failure\n          (Tom)</p>"
  ],
  [
    "<p>Fix bugs in interval-to-time conversion (Tom)</p>"
  ],
  [
    "<p>Add constraint names in a few places in pg_dump\n          (Rod)</p>"
  ],
  [
    "<p>Improve performance of functions with many parameters\n          (Tom)</p>"
  ],
  [
    "<p>Fix to_ascii() buffer overruns (Tom)</p>"
  ],
  [
    "<p>Prevent restore of database comments from throwing an\n          error (Tom)</p>"
  ],
  [
    "<p>Work around buggy strxfrm() present in some Solaris\n          releases (Tom)</p>"
  ],
  [
    "<p>Properly escape jdbc setObject() strings to improve\n          security (Barry)</p>"
  ]
]