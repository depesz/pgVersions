[
  [
    "<p>Ensure proper quoting of transition table names when <span class=\"application\">pg_dump</span> emits <code class=\"command\">CREATE TRIGGER ... REFERENCING</code> commands (Tom Lane)</p>",
    "<p>This oversight could be exploited by an unprivileged user to gain superuser privileges during the next dump/reload or <span class=\"application\">pg_upgrade</span> run. (CVE-2018-16850)</p>"
  ],
  [
    "<p>Apply the tablespace specified for a partitioned index when creating a child index (Álvaro Herrera)</p>",
    "<p>Previously, child indexes were always created in the default tablespace.</p>"
  ],
  [
    "<p>Fix NULL handling in parallel hashed multi-batch left joins (Andrew Gierth, Thomas Munro)</p>",
    "<p>Outer-relation rows with null values of the hash key were omitted from the join result.</p>"
  ],
  [
    "<p>Fix incorrect processing of an array-type coercion expression appearing within a <code class=\"literal\">CASE</code> clause that has a constant test expression (Tom Lane)</p>"
  ],
  [
    "<p>Fix incorrect expansion of tuples lacking recently-added columns (Andrew Dunstan, Amit Langote)</p>",
    "<p>This is known to lead to crashes in triggers on tables with recently-added columns, and could have other symptoms as well.</p>"
  ],
  [
    "<p>Fix bugs with named or defaulted arguments in <code class=\"command\">CALL</code> argument lists (Tom Lane, Pavel Stehule)</p>"
  ],
  [
    "<p>Fix strictness check for strict aggregates with <code class=\"literal\">ORDER BY</code> columns (Andrew Gierth, Andres Freund)</p>",
    "<p>The strictness logic incorrectly ignored rows for which the <code class=\"literal\">ORDER BY</code> value(s) were null.</p>"
  ],
  [
    "<p>Disable <code class=\"varname\">recheck_on_update</code> optimization (Tom Lane)</p>",
    "<p>This new-in-v11 feature turns out not to have been ready for prime time. Disable it until something can be done about it.</p>"
  ],
  [
    "<p>Prevent creation of a partition in a trigger attached to its parent table (Amit Langote)</p>",
    "<p>Ideally we'd allow that, but for the moment it has to be blocked to avoid crashes.</p>"
  ],
  [
    "<p>Fix problems with applying <code class=\"literal\">ON COMMIT DELETE ROWS</code> to a partitioned temporary table (Amit Langote)</p>"
  ],
  [
    "<p>Fix character-class checks to not fail on Windows for Unicode characters above U+FFFF (Tom Lane, Kenji Uno)</p>",
    "<p>This bug affected full-text-search operations, as well as <code class=\"filename\">contrib/ltree</code> and <code class=\"filename\">contrib/pg_trgm</code>.</p>"
  ],
  [
    "<p>Ensure that the server will process already-received <code class=\"literal\">NOTIFY</code> and <code class=\"literal\">SIGTERM</code> interrupts before waiting for client input (Jeff Janes, Tom Lane)</p>"
  ],
  [
    "<p>Fix memory leak in repeated SP-GiST index scans (Tom Lane)</p>",
    "<p>This is only known to amount to anything significant in cases where an exclusion constraint using SP-GiST receives many new index entries in a single command.</p>"
  ],
  [
    "<p>Prevent starting the server with <code class=\"varname\">wal_level</code> set to too low a value to support an existing replication slot (Andres Freund)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">psql</span>, as well as documentation examples, to call <code class=\"function\">PQconsumeInput()</code> before each <code class=\"function\">PQnotifies()</code> call (Tom Lane)</p>",
    "<p>This fixes cases in which <span class=\"application\">psql</span> would not report receipt of a <code class=\"literal\">NOTIFY</code> message until after the next command.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_verify_checksums</span>'s determination of which files to check the checksums of (Michael Paquier)</p>",
    "<p>In some cases it complained about files that are not expected to have checksums.</p>"
  ],
  [
    "<p>In <code class=\"filename\">contrib/pg_stat_statements</code>, disallow the <code class=\"literal\">pg_read_all_stats</code> role from executing <code class=\"function\">pg_stat_statements_reset()</code> (Haribabu Kommi)</p>",
    "<p><code class=\"literal\">pg_read_all_stats</code> is only meant to grant permission to read statistics, not to change them, so this grant was incorrect.</p>",
    "<p>To cause this change to take effect, run <code class=\"literal\">ALTER EXTENSION pg_stat_statements UPDATE</code> in each database where <code class=\"filename\">pg_stat_statements</code> has been installed. (A database freshly created in 11.0 should not need this, but a database upgraded from a previous release probably still contains the old version of <code class=\"filename\">pg_stat_statements</code>. The <code class=\"literal\">UPDATE</code> command is harmless if the module was already updated.)</p>"
  ],
  [
    "<p>Rename red-black tree support functions to use <code class=\"literal\">rbt</code> prefix not <code class=\"literal\">rb</code> prefix (Tom Lane)</p>",
    "<p>This avoids name collisions with Ruby functions, which broke PL/Ruby. It's hoped that there are no other affected extensions.</p>"
  ],
  [
    "<p>Fix build problems on macOS 10.14 (Mojave) (Tom Lane)</p>",
    "<p>Adjust <span class=\"application\">configure</span> to add an <code class=\"option\">-isysroot</code> switch to <code class=\"varname\">CPPFLAGS</code>; without this, PL/Perl and PL/Tcl fail to configure or build on macOS 10.14. The specific sysroot used can be overridden at configure time or build time by setting the <code class=\"varname\">PG_SYSROOT</code> variable in the arguments of <span class=\"application\">configure</span> or <span class=\"application\">make</span>.</p>",
    "<p>It is now recommended that Perl-related extensions write <code class=\"literal\">$(perl_includespec)</code> rather than <code class=\"literal\">-I$(perl_archlibexp)/CORE</code> in their compiler flags. The latter continues to work on most platforms, but not recent macOS.</p>",
    "<p>Also, it should no longer be necessary to specify <code class=\"option\">--with-tclconfig</code> manually to get PL/Tcl to build on recent macOS releases.</p>"
  ],
  [
    "<p>Fix MSVC build and regression-test scripts to work on recent Perl versions (Andrew Dunstan)</p>",
    "<p>Perl no longer includes the current directory in its search path by default; work around that.</p>"
  ],
  [
    "<p>On Windows, allow the regression tests to be run by an Administrator account (Andrew Dunstan)</p>",
    "<p>To do this safely, <span class=\"application\">pg_regress</span> now gives up any such privileges at startup.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2018g for DST law changes in Chile, Fiji, Morocco, and Russia (Volgograd), plus historical corrections for China, Hawaii, Japan, Macau, and North Korea.</p>"
  ]
]