[
  [
    "<p>Fix character string comparison for Windows locales\n          that consider different character combinations as equal\n          (Tom)</p>",
    "<p>This fix applies only on Windows and only when using\n          UTF-8 database encoding. The same fix was made for all\n          other cases over two years ago, but Windows with UTF-8\n          uses a separate code path that was not updated. If you\n          are using a locale that considers some non-identical\n          strings as equal, you may need to <code class=\"command\">REINDEX</code> to fix existing indexes on\n          textual columns.</p>"
  ],
  [
    "<p>Repair corner-case bugs in <code class=\"command\">VACUUM FULL</code> (Tom)</p>",
    "<p>A potential deadlock between concurrent <code class=\"command\">VACUUM FULL</code> operations on different\n          system catalogs was introduced in 8.2. This has now been\n          corrected. 8.3 made this worse because the deadlock could\n          occur within a critical code section, making it a PANIC\n          rather than just ERROR condition.</p>",
    "<p>Also, a <code class=\"command\">VACUUM FULL</code> that\n          failed partway through vacuuming a system catalog could\n          result in cache corruption in concurrent database\n          sessions.</p>",
    "<p>Another <code class=\"command\">VACUUM FULL</code> bug\n          introduced in 8.3 could result in a crash or\n          out-of-memory report when dealing with pages containing\n          no live tuples.</p>"
  ],
  [
    "<p>Fix misbehavior of foreign key checks involving\n          <code class=\"type\">character</code> or <code class=\"type\">bit</code> columns (Tom)</p>",
    "<p>If the referencing column were of a different but\n          compatible type (for instance <code class=\"type\">varchar</code>), the constraint was enforced\n          incorrectly.</p>"
  ],
  [
    "<p>Avoid needless deadlock failures in no-op foreign-key\n          checks (Stephan Szabo, Tom)</p>"
  ],
  [
    "<p>Fix possible core dump when re-planning a prepared\n          query (Tom)</p>",
    "<p>This bug affected only protocol-level prepare\n          operations, not SQL <code class=\"command\">PREPARE</code>,\n          and so tended to be seen only with JDBC, DBI, and other\n          client-side drivers that use prepared statements\n          heavily.</p>"
  ],
  [
    "<p>Fix possible failure when re-planning a query that\n          calls an SPI-using function (Tom)</p>"
  ],
  [
    "<p>Fix failure in row-wise comparisons involving columns\n          of different datatypes (Tom)</p>"
  ],
  [
    "<p>Fix longstanding <code class=\"command\">LISTEN</code>/<code class=\"command\">NOTIFY</code> race condition (Tom)</p>",
    "<p>In rare cases a session that had just executed a\n          <code class=\"command\">LISTEN</code> might not get a\n          notification, even though one would be expected because\n          the concurrent transaction executing <code class=\"command\">NOTIFY</code> was observed to commit later.</p>",
    "<p>A side effect of the fix is that a transaction that\n          has executed a not-yet-committed <code class=\"command\">LISTEN</code> command will not see any row in\n          <code class=\"structname\">pg_listener</code> for the\n          <code class=\"command\">LISTEN</code>, should it choose to\n          look; formerly it would have. This behavior was never\n          documented one way or the other, but it is possible that\n          some applications depend on the old behavior.</p>"
  ],
  [
    "<p>Disallow <code class=\"command\">LISTEN</code> and\n          <code class=\"command\">UNLISTEN</code> within a prepared\n          transaction (Tom)</p>",
    "<p>This was formerly allowed but trying to do it had\n          various unpleasant consequences, notably that the\n          originating backend could not exit as long as an\n          <code class=\"command\">UNLISTEN</code> remained\n          uncommitted.</p>"
  ],
  [
    "<p>Disallow dropping a temporary table within a prepared\n          transaction (Heikki)</p>",
    "<p>This was correctly disallowed by 8.1, but the check\n          was inadvertently broken in 8.2 and 8.3.</p>"
  ],
  [
    "<p>Fix rare crash when an error occurs during a query\n          using a hash index (Heikki)</p>"
  ],
  [
    "<p>Fix incorrect comparison of <code class=\"type\">tsquery</code> values (Teodor)</p>"
  ],
  [
    "<p>Fix incorrect behavior of <code class=\"literal\">LIKE</code> with non-ASCII characters in\n          single-byte encodings (Rolf Jentsch)</p>"
  ],
  [
    "<p>Disable <code class=\"function\">xmlvalidate</code>\n          (Tom)</p>",
    "<p>This function should have been removed before 8.3\n          release, but was inadvertently left in the source code.\n          It poses a small security risk since unprivileged users\n          could use it to read the first few characters of any file\n          accessible to the server.</p>"
  ],
  [
    "<p>Fix memory leaks in certain usages of set-returning\n          functions (Neil)</p>"
  ],
  [
    "<p>Make <code class=\"function\">encode(<em class=\"replaceable\"><code>bytea</code></em>, 'escape')</code>\n          convert all high-bit-set byte values into <code class=\"literal\">\\</code><em class=\"replaceable\"><code>nnn</code></em> octal escape\n          sequences (Tom)</p>",
    "<p>This is necessary to avoid encoding problems when the\n          database encoding is multi-byte. This change could pose\n          compatibility issues for applications that are expecting\n          specific results from <code class=\"function\">encode</code>.</p>"
  ],
  [
    "<p>Fix input of datetime values for February 29 in years\n          BC (Tom)</p>",
    "<p>The former coding was mistaken about which years were\n          leap years.</p>"
  ],
  [
    "<p>Fix <span class=\"quote\">&#x201C;<span class=\"quote\">unrecognized node type</span>&#x201D;</span> error in\n          some variants of <code class=\"command\">ALTER OWNER</code>\n          (Tom)</p>"
  ],
  [
    "<p>Avoid tablespace permissions errors in <code class=\"command\">CREATE TABLE LIKE INCLUDING INDEXES</code>\n          (Tom)</p>"
  ],
  [
    "<p>Ensure <code class=\"structname\">pg_stat_activity</code>.<code class=\"structfield\">waiting</code> flag is cleared when a lock\n          wait is aborted (Tom)</p>"
  ],
  [
    "<p>Fix handling of process permissions on Windows Vista\n          (Dave, Magnus)</p>",
    "<p>In particular, this fix allows starting the server as\n          the Administrator user.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2008a (in particular,\n          recent Chile changes); adjust timezone abbreviation\n          <code class=\"literal\">VET</code> (Venezuela) to mean\n          UTC-4:30, not UTC-4:00 (Tom)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">ecpg</span> problems\n          with arrays (Michael)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_ctl</span> to\n          correctly extract the postmaster's port number from\n          command-line options (Itagaki Takahiro, Tom)</p>",
    "<p>Previously, <code class=\"literal\">pg_ctl start\n          -w</code> could try to contact the postmaster on the\n          wrong port, leading to bogus reports of startup\n          failure.</p>"
  ],
  [
    "<p>Use <code class=\"option\">-fwrapv</code> to defend\n          against possible misoptimization in recent <span class=\"application\">gcc</span> versions (Tom)</p>",
    "<p>This is known to be necessary when building\n          <span class=\"productname\">PostgreSQL</span> with\n          <span class=\"application\">gcc</span> 4.3 or later.</p>"
  ],
  [
    "<p>Enable building <code class=\"filename\">contrib/uuid-ossp</code> with MSVC (Hiroshi\n          Saito)</p>"
  ]
]