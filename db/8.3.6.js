[
  [
    "<p>Make <code class=\"command\">DISCARD ALL</code> release\n          advisory locks, in addition to everything it already did\n          (Tom)</p>",
    "<p>This was decided to be the most appropriate behavior.\n          This could affect existing applications, however.</p>"
  ],
  [
    "<p>Fix whole-index GiST scans to work correctly\n          (Teodor)</p>",
    "<p>This error could cause rows to be lost if a table is\n          clustered on a GiST index.</p>"
  ],
  [
    "<p>Fix crash of <code class=\"literal\">xmlconcat(NULL)</code> (Peter)</p>"
  ],
  [
    "<p>Fix possible crash in <code class=\"literal\">ispell</code> dictionary if high-bit-set\n          characters are used as flags (Teodor)</p>",
    "<p>This is known to be done by one widely available\n          Norwegian dictionary, and the same condition may exist in\n          others.</p>"
  ],
  [
    "<p>Fix misordering of <span class=\"application\">pg_dump</span> output for composite types\n          (Tom)</p>",
    "<p>The most likely problem was for user-defined operator\n          classes to be dumped after indexes or views that needed\n          them.</p>"
  ],
  [
    "<p>Improve handling of URLs in <code class=\"function\">headline()</code> function (Teodor)</p>"
  ],
  [
    "<p>Improve handling of overlength headlines in\n          <code class=\"function\">headline()</code> function\n          (Teodor)</p>"
  ],
  [
    "<p>Prevent possible Assert failure or misconversion if an\n          encoding conversion is created with the wrong conversion\n          function for the specified pair of encodings (Tom,\n          Heikki)</p>"
  ],
  [
    "<p>Fix possible Assert failure if a statement executed in\n          PL/pgSQL is rewritten into another kind of statement, for\n          example if an <code class=\"command\">INSERT</code> is\n          rewritten into an <code class=\"command\">UPDATE</code>\n          (Heikki)</p>"
  ],
  [
    "<p>Ensure that a snapshot is available to datatype input\n          functions (Tom)</p>",
    "<p>This primarily affects domains that are declared with\n          <code class=\"literal\">CHECK</code> constraints involving\n          user-defined stable or immutable functions. Such\n          functions typically fail if no snapshot has been set.</p>"
  ],
  [
    "<p>Make it safer for SPI-using functions to be used\n          within datatype I/O; in particular, to be used in domain\n          check constraints (Tom)</p>"
  ],
  [
    "<p>Avoid unnecessary locking of small tables in\n          <code class=\"command\">VACUUM</code> (Heikki)</p>"
  ],
  [
    "<p>Fix a problem that sometimes kept <code class=\"command\">ALTER TABLE ENABLE/DISABLE RULE</code> from\n          being recognized by active sessions (Tom)</p>"
  ],
  [
    "<p>Fix a problem that made <code class=\"literal\">UPDATE\n          RETURNING tableoid</code> return zero instead of the\n          correct OID (Tom)</p>"
  ],
  [
    "<p>Allow functions declared as taking <code class=\"type\">ANYARRAY</code> to work on the <code class=\"structname\">pg_statistic</code> columns of that type\n          (Tom)</p>",
    "<p>This used to work, but was unintentionally broken in\n          8.3.</p>"
  ],
  [
    "<p>Fix planner misestimation of selectivity when\n          transitive equality is applied to an outer-join clause\n          (Tom)</p>",
    "<p>This could result in bad plans for queries like\n          <code class=\"literal\">... from a left join b on a.a1 =\n          b.b1 where a.a1 = 42 ...</code></p>"
  ],
  [
    "<p>Improve optimizer's handling of long <code class=\"literal\">IN</code> lists (Tom)</p>",
    "<p>This change avoids wasting large amounts of time on\n          such lists when constraint exclusion is enabled.</p>"
  ],
  [
    "<p>Prevent synchronous scan during GIN index build\n          (Tom)</p>",
    "<p>Because GIN is optimized for inserting tuples in\n          increasing TID order, choosing to use a synchronous scan\n          could slow the build by a factor of three or more.</p>"
  ],
  [
    "<p>Ensure that the contents of a holdable cursor don't\n          depend on the contents of TOAST tables (Tom)</p>",
    "<p>Previously, large field values in a cursor result\n          might be represented as TOAST pointers, which would fail\n          if the referenced table got dropped before the cursor is\n          read, or if the large value is deleted and then vacuumed\n          away. This cannot happen with an ordinary cursor, but it\n          could with a cursor that is held past its creating\n          transaction.</p>"
  ],
  [
    "<p>Fix memory leak when a set-returning function is\n          terminated without reading its whole result (Tom)</p>"
  ],
  [
    "<p>Fix encoding conversion problems in XML functions when\n          the database encoding isn't UTF-8 (Tom)</p>"
  ],
  [
    "<p>Fix <code class=\"filename\">contrib/dblink</code>'s\n          <code class=\"function\">dblink_get_result(text,bool)</code> function\n          (Joe)</p>"
  ],
  [
    "<p>Fix possible garbage output from <code class=\"filename\">contrib/sslinfo</code> functions (Tom)</p>"
  ],
  [
    "<p>Fix incorrect behavior of <code class=\"filename\">contrib/tsearch2</code> compatibility trigger\n          when it's fired more than once in a command (Teodor)</p>"
  ],
  [
    "<p>Fix possible mis-signaling in autovacuum (Heikki)</p>"
  ],
  [
    "<p>Support running as a service on Windows 7 beta (Dave\n          and Magnus)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">ecpg</span>'s handling\n          of varchar structs (Michael)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">configure</span> script\n          to properly report failure when unable to obtain linkage\n          information for PL/Perl (Andrew)</p>"
  ],
  [
    "<p>Make all documentation reference <code class=\"literal\">pgsql-bugs</code> and/or <code class=\"literal\">pgsql-hackers</code> as appropriate, instead of\n          the now-decommissioned <code class=\"literal\">pgsql-ports</code> and <code class=\"literal\">pgsql-patches</code> mailing lists (Tom)</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2009a (for Kathmandu\n          and historical DST corrections in Switzerland, Cuba)</p>"
  ]
]