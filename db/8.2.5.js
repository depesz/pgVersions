[
  [
    "<p>Prevent index corruption when a transaction inserts\n          rows and then aborts close to the end of a concurrent\n          <code class=\"command\">VACUUM</code> on the same table\n          (Tom)</p>"
  ],
  [
    "<p>Fix <code class=\"literal\">ALTER DOMAIN ADD\n          CONSTRAINT</code> for cases involving domains over\n          domains (Tom)</p>"
  ],
  [
    "<p>Make <code class=\"command\">CREATE DOMAIN ... DEFAULT\n          NULL</code> work properly (Tom)</p>"
  ],
  [
    "<p>Fix some planner problems with outer joins, notably\n          poor size estimation for <code class=\"literal\">t1 LEFT\n          JOIN t2 WHERE t2.col IS NULL</code> (Tom)</p>"
  ],
  [
    "<p>Allow the <code class=\"type\">interval</code> data type\n          to accept input consisting only of milliseconds or\n          microseconds (Neil)</p>"
  ],
  [
    "<p>Allow timezone name to appear before the year in\n          <code class=\"type\">timestamp</code> input (Tom)</p>"
  ],
  [
    "<p>Fixes for <acronym class=\"acronym\">GIN</acronym>\n          indexes used by <code class=\"filename\">/contrib/tsearch2</code> (Teodor)</p>"
  ],
  [
    "<p>Speed up rtree index insertion (Teodor)</p>"
  ],
  [
    "<p>Fix excessive logging of <acronym class=\"acronym\">SSL</acronym> error messages (Tom)</p>"
  ],
  [
    "<p>Fix logging so that log messages are never interleaved\n          when using the syslogger process (Andrew)</p>"
  ],
  [
    "<p>Fix crash when <code class=\"varname\">log_min_error_statement</code> logging runs out\n          of memory (Tom)</p>"
  ],
  [
    "<p>Fix incorrect handling of some foreign-key corner\n          cases (Tom)</p>"
  ],
  [
    "<p>Fix <code class=\"function\">stddev_pop(numeric)</code>\n          and <code class=\"function\">var_pop(numeric)</code>\n          (Tom)</p>"
  ],
  [
    "<p>Prevent <code class=\"command\">REINDEX</code> and\n          <code class=\"command\">CLUSTER</code> from failing due to\n          attempting to process temporary tables of other sessions\n          (Alvaro)</p>"
  ],
  [
    "<p>Update the time zone database rules, particularly New\n          Zealand's upcoming changes (Tom)</p>"
  ],
  [
    "<p>Windows socket and semaphore improvements (Magnus)</p>"
  ],
  [
    "<p>Make <code class=\"command\">pg_ctl -w</code> work\n          properly in Windows service mode (Dave Page)</p>"
  ],
  [
    "<p>Fix memory allocation bug when using <span class=\"application\">MIT Kerberos</span> on Windows (Magnus)</p>"
  ],
  [
    "<p>Suppress timezone name (<code class=\"literal\">%Z</code>) in log timestamps on Windows because\n          of possible encoding mismatches (Tom)</p>"
  ],
  [
    "<p>Require non-superusers who use <code class=\"filename\">/contrib/dblink</code> to use only password\n          authentication, as a security measure (Joe)</p>"
  ],
  [
    "<p>Restrict <code class=\"filename\">/contrib/pgstattuple</code> functions to\n          superusers, for security reasons (Tom)</p>"
  ],
  [
    "<p>Do not let <code class=\"filename\">/contrib/intarray</code> try to make its GIN\n          opclass the default (this caused problems at\n          dump/restore) (Tom)</p>"
  ]
]