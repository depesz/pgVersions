[
  [
    "<p>Repair sometimes-incorrect computation of StartUpID\n          after a crash</p>"
  ],
  [
    "<p>Avoid slowness with lots of deferred triggers in one\n          transaction (Stephan)</p>"
  ],
  [
    "<p>Don't lock referenced row when <code class=\"command\">UPDATE</code> doesn't change foreign key's\n          value (Jan)</p>"
  ],
  [
    "<p>Use <code class=\"command\">-fPIC</code> not\n          <code class=\"command\">-fpic</code> on Sparc (Tom\n          Callaway)</p>"
  ],
  [
    "<p>Repair lack of schema-awareness in\n          contrib/reindexdb</p>"
  ],
  [
    "<p>Fix contrib/intarray error for zero-element result\n          array (Teodor)</p>"
  ],
  [
    "<p>Ensure createuser script will exit on control-C\n          (Oliver)</p>"
  ],
  [
    "<p>Fix errors when the type of a dropped column has\n          itself been dropped</p>"
  ],
  [
    "<p><code class=\"command\">CHECKPOINT</code> does not cause\n          database panic on failure in noncritical steps</p>"
  ],
  [
    "<p>Accept 60 in seconds fields of timestamp, time,\n          interval input values</p>"
  ],
  [
    "<p>Issue notice, not error, if <code class=\"type\">TIMESTAMP</code>, <code class=\"type\">TIME</code>,\n          or <code class=\"type\">INTERVAL</code> precision too\n          large</p>"
  ],
  [
    "<p>Fix <code class=\"function\">abstime-to-time</code> cast\n          function (fix is not applied unless you <span class=\"application\">initdb</span>)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_proc</span> entry for\n          <code class=\"type\">timestampt_izone</code> (fix is not\n          applied unless you <span class=\"application\">initdb</span>)</p>"
  ],
  [
    "<p>Make <code class=\"function\">EXTRACT(EPOCH FROM\n          timestamp without time zone)</code> treat input as local\n          time</p>"
  ],
  [
    "<p><code class=\"command\">'now'::timestamptz</code> gave\n          wrong answer if timezone changed earlier in\n          transaction</p>"
  ],
  [
    "<p><code class=\"envar\">HAVE_INT64_TIMESTAMP</code> code\n          for time with timezone overwrote its input</p>"
  ],
  [
    "<p>Accept <code class=\"command\">GLOBAL\n          TEMP/TEMPORARY</code> as a synonym for <code class=\"command\">TEMPORARY</code></p>"
  ],
  [
    "<p>Avoid improper schema-privilege-check failure in\n          foreign-key triggers</p>"
  ],
  [
    "<p>Fix bugs in foreign-key triggers for <code class=\"command\">SET DEFAULT</code> action</p>"
  ],
  [
    "<p>Fix incorrect time-qual check in row fetch for\n          <code class=\"command\">UPDATE</code> and <code class=\"command\">DELETE</code> triggers</p>"
  ],
  [
    "<p>Foreign-key clauses were parsed but ignored in\n          <code class=\"command\">ALTER TABLE ADD COLUMN</code></p>"
  ],
  [
    "<p>Fix createlang script breakage for case where handler\n          function already exists</p>"
  ],
  [
    "<p>Fix misbehavior on zero-column tables in <span class=\"application\">pg_dump</span>, COPY, ANALYZE, other\n          places</p>"
  ],
  [
    "<p>Fix misbehavior of <code class=\"function\">func_error()</code> on type names containing\n          '%'</p>"
  ],
  [
    "<p>Fix misbehavior of <code class=\"function\">replace()</code> on strings containing '%'</p>"
  ],
  [
    "<p>Regular-expression patterns containing certain\n          multibyte characters failed</p>"
  ],
  [
    "<p>Account correctly for <code class=\"command\">NULL</code>s in more cases in join size\n          estimation</p>"
  ],
  [
    "<p>Avoid conflict with system definition of <code class=\"function\">isblank()</code> function or macro</p>"
  ],
  [
    "<p>Fix failure to convert large code point values in\n          EUC_TW conversions (Tatsuo)</p>"
  ],
  [
    "<p>Fix error recovery for <code class=\"function\">SSL_read</code>/<code class=\"function\">SSL_write</code> calls</p>"
  ],
  [
    "<p>Don't do early constant-folding of type coercion\n          expressions</p>"
  ],
  [
    "<p>Validate page header fields immediately after reading\n          in any page</p>"
  ],
  [
    "<p>Repair incorrect check for ungrouped variables in\n          unnamed joins</p>"
  ],
  [
    "<p>Fix buffer overrun in <code class=\"function\">to_ascii</code> (Guido Notari)</p>"
  ],
  [
    "<p>contrib/ltree fixes (Teodor)</p>"
  ],
  [
    "<p>Fix core dump in deadlock detection on machines where\n          char is unsigned</p>"
  ],
  [
    "<p>Avoid running out of buffers in many-way indexscan\n          (bug introduced in 7.3)</p>"
  ],
  [
    "<p>Fix planner's selectivity estimation functions to\n          handle domains properly</p>"
  ],
  [
    "<p>Fix <span class=\"application\">dbmirror</span>\n          memory-allocation bug (Steven Singer)</p>"
  ],
  [
    "<p>Prevent infinite loop in <code class=\"function\">ln(numeric)</code> due to roundoff error</p>"
  ],
  [
    "<p><code class=\"command\">GROUP BY</code> got confused if\n          there were multiple equal GROUP BY items</p>"
  ],
  [
    "<p>Fix bad plan when inherited <code class=\"command\">UPDATE</code>/<code class=\"command\">DELETE</code> references another inherited\n          table</p>"
  ],
  [
    "<p>Prevent clustering on incomplete (partial or\n          non-NULL-storing) indexes</p>"
  ],
  [
    "<p>Service shutdown request at proper time if it arrives\n          while still starting up</p>"
  ],
  [
    "<p>Fix left-links in temporary indexes (could make\n          backwards scans miss entries)</p>"
  ],
  [
    "<p>Fix incorrect handling of client_encoding setting in\n          postgresql.conf (Tatsuo)</p>"
  ],
  [
    "<p>Fix failure to respond to <code class=\"command\">pg_ctl\n          stop -m fast</code> after Async_NotifyHandler runs</p>"
  ],
  [
    "<p>Fix SPI for case where rule contains multiple\n          statements of the same type</p>"
  ],
  [
    "<p>Fix problem with checking for wrong type of access\n          privilege in rule query</p>"
  ],
  [
    "<p>Fix problem with <code class=\"command\">EXCEPT</code>\n          in <code class=\"command\">CREATE RULE</code></p>"
  ],
  [
    "<p>Prevent problem with dropping temp tables having\n          serial columns</p>"
  ],
  [
    "<p>Fix replace_vars_with_subplan_refs failure in complex\n          views</p>"
  ],
  [
    "<p>Fix regexp slowness in single-byte encodings\n          (Tatsuo)</p>"
  ],
  [
    "<p>Allow qualified type names in <code class=\"command\">CREATE CAST</code> and <code class=\"command\">DROP CAST</code></p>"
  ],
  [
    "<p>Accept <code class=\"function\">SETOF type[]</code>,\n          which formerly had to be written <code class=\"function\">SETOF _type</code></p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_dump</span> core dump\n          in some cases with procedural languages</p>"
  ],
  [
    "<p>Force ISO datestyle in <span class=\"application\">pg_dump</span> output, for portability\n          (Oliver)</p>"
  ],
  [
    "<p><span class=\"application\">pg_dump</span> failed to\n          handle error return from <code class=\"function\">lo_read</code> (Oleg Drokin)</p>"
  ],
  [
    "<p><span class=\"application\">pg_dumpall</span> failed\n          with groups having no members (Nick Eskelinen)</p>"
  ],
  [
    "<p><span class=\"application\">pg_dumpall</span> failed to\n          recognize --globals-only switch</p>"
  ],
  [
    "<p>pg_restore failed to restore blobs if -X\n          disable-triggers is specified</p>"
  ],
  [
    "<p>Repair intrafunction memory leak in plpgsql</p>"
  ],
  [
    "<p>pltcl's <code class=\"command\">elog</code> command\n          dumped core if given wrong parameters (Ian Harding)</p>"
  ],
  [
    "<p>plpython used wrong value of <code class=\"envar\">atttypmod</code> (Brad McLean)</p>"
  ],
  [
    "<p>Fix improper quoting of boolean values in Python\n          interface (D'Arcy)</p>"
  ],
  [
    "<p>Added <code class=\"function\">addDataType()</code>\n          method to PGConnection interface for JDBC</p>"
  ],
  [
    "<p>Fixed various problems with updateable ResultSets for\n          JDBC (Shawn Green)</p>"
  ],
  [
    "<p>Fixed various problems with DatabaseMetaData for JDBC\n          (Kris Jurka, Peter Royal)</p>"
  ],
  [
    "<p>Fixed problem with parsing table ACLs in JDBC</p>"
  ],
  [
    "<p>Better error message for character set conversion\n          problems in JDBC</p>"
  ]
]