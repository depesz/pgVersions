[
  [
    "<p>Fix sample server-start scripts to become <code class=\"literal\">$PGUSER</code> before opening <code class=\"literal\">$PGLOG</code> (Noah Misch)</p>",
    "<p>Previously, the postmaster log file was opened while\n          still running as root. The database owner could therefore\n          mount an attack against another system user by making\n          <code class=\"literal\">$PGLOG</code> be a symbolic link to\n          some other file, which would then become corrupted by\n          appending log messages.</p>",
    "<p>By default, these scripts are not installed anywhere.\n          Users who have made use of them will need to manually\n          recopy them, or apply the same changes to their modified\n          versions. If the existing <code class=\"literal\">$PGLOG</code> file is root-owned, it will need\n          to be removed or renamed out of the way before restarting\n          the server with the corrected script.\n          (CVE-2017-12172)</p>"
  ],
  [
    "<p>Properly reject attempts to convert infinite float\n          values to type <code class=\"type\">numeric</code> (Tom\n          Lane, KaiGai Kohei)</p>",
    "<p>Previously the behavior was platform-dependent.</p>"
  ],
  [
    "<p>Fix corner-case crashes when columns have been added\n          to the end of a view (Tom Lane)</p>"
  ],
  [
    "<p>Record proper dependencies when a view or rule\n          contains <code class=\"structname\">FieldSelect</code> or\n          <code class=\"structname\">FieldStore</code> expression\n          nodes (Tom Lane)</p>",
    "<p>Lack of these dependencies could allow a column or\n          data type <code class=\"command\">DROP</code> to go through\n          when it ought to fail, thereby causing later uses of the\n          view or rule to get errors. This patch does not do\n          anything to protect existing views/rules, only ones\n          created in the future.</p>"
  ],
  [
    "<p>Correctly detect hashability of range data types (Tom\n          Lane)</p>",
    "<p>The planner mistakenly assumed that any range type\n          could be hashed for use in hash joins or hash\n          aggregation, but actually it must check whether the\n          range's subtype has hash support. This does not affect\n          any of the built-in range types, since they're all\n          hashable anyway.</p>"
  ],
  [
    "<p>Fix low-probability loss of <code class=\"command\">NOTIFY</code> messages due to XID wraparound\n          (Marko Tiikkaja, Tom Lane)</p>",
    "<p>If a session executed no queries, but merely listened\n          for notifications, for more than 2 billion transactions,\n          it started to miss some notifications from\n          concurrently-committing transactions.</p>"
  ],
  [
    "<p>Prevent low-probability crash in processing of nested\n          trigger firings (Tom Lane)</p>"
  ],
  [
    "<p>Correctly restore the umask setting when file creation\n          fails in <code class=\"command\">COPY</code> or\n          <code class=\"function\">lo_export()</code> (Peter\n          Eisentraut)</p>"
  ],
  [
    "<p>Give a better error message for duplicate column names\n          in <code class=\"command\">ANALYZE</code> (Nathan\n          Bossart)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">libpq</span> to not\n          require user's home directory to exist (Tom Lane)</p>",
    "<p>In v10, failure to find the home directory while\n          trying to read <code class=\"filename\">~/.pgpass</code>\n          was treated as a hard error, but it should just cause\n          that file to not be found. Both v10 and previous release\n          branches made the same mistake when reading <code class=\"filename\">~/.pg_service.conf</code>, though this was\n          less obvious since that file is not sought unless a\n          service name is specified.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">libpq</span> to guard\n          against integer overflow in the row count of a\n          <code class=\"structname\">PGresult</code> (Michael\n          Paquier)</p>"
  ],
  [
    "<p>Sync our copy of the timezone library with IANA\n          release tzcode2017c (Tom Lane)</p>",
    "<p>This fixes various issues; the only one likely to be\n          user-visible is that the default DST rules for a\n          POSIX-style zone name, if no <code class=\"filename\">posixrules</code> file exists in the timezone\n          data directory, now match current US law rather than what\n          it was a dozen years ago.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2017c for DST law\n          changes in Fiji, Namibia, Northern Cyprus, Sudan, Tonga,\n          and Turks &amp; Caicos Islands, plus historical\n          corrections for Alaska, Apia, Burma, Calcutta, Detroit,\n          Ireland, Namibia, and Pago Pago.</p>"
  ]
]