[
  [
    "<p>Fix multiple bugs associated with <code class=\"command\">CREATE/DROP INDEX CONCURRENTLY</code> (Andres\n          Freund, Tom Lane, Simon Riggs, Pavan Deolasee)</p>",
    "<p>An error introduced while adding <code class=\"command\">DROP INDEX CONCURRENTLY</code> allowed\n          incorrect indexing decisions to be made during the\n          initial phase of <code class=\"command\">CREATE INDEX\n          CONCURRENTLY</code>; so that indexes built by that\n          command could be corrupt. It is recommended that indexes\n          built in 9.2.X with <code class=\"command\">CREATE INDEX\n          CONCURRENTLY</code> be rebuilt after applying this\n          update.</p>",
    "<p>In addition, fix <code class=\"command\">CREATE/DROP\n          INDEX CONCURRENTLY</code> to use in-place updates when\n          changing the state of an index's <code class=\"structname\">pg_index</code> row. This prevents race\n          conditions that could cause concurrent sessions to miss\n          updating the target index, thus again resulting in\n          corrupt concurrently-created indexes.</p>",
    "<p>Also, fix various other operations to ensure that they\n          ignore invalid indexes resulting from a failed\n          <code class=\"command\">CREATE INDEX CONCURRENTLY</code>\n          command. The most important of these is <code class=\"command\">VACUUM</code>, because an auto-vacuum could\n          easily be launched on the table before corrective action\n          can be taken to fix or remove the invalid index.</p>",
    "<p>Also fix <code class=\"command\">DROP INDEX\n          CONCURRENTLY</code> to not disable insertions into the\n          target index until all queries using it are done.</p>",
    "<p>Also fix misbehavior if <code class=\"command\">DROP\n          INDEX CONCURRENTLY</code> is canceled: the previous\n          coding could leave an un-droppable index behind.</p>"
  ],
  [
    "<p>Correct predicate locking for <code class=\"command\">DROP INDEX CONCURRENTLY</code> (Kevin\n          Grittner)</p>",
    "<p>Previously, SSI predicate locks were processed at the\n          wrong time, possibly leading to incorrect behavior of\n          serializable transactions executing in parallel with the\n          <code class=\"command\">DROP</code>.</p>"
  ],
  [
    "<p>Fix buffer locking during WAL replay (Tom Lane)</p>",
    "<p>The WAL replay code was insufficiently careful about\n          locking buffers when replaying WAL records that affect\n          more than one page. This could result in hot standby\n          queries transiently seeing inconsistent states, resulting\n          in wrong answers or unexpected failures.</p>"
  ],
  [
    "<p>Fix an error in WAL generation logic for GIN indexes\n          (Tom Lane)</p>",
    "<p>This could result in index corruption, if a torn-page\n          failure occurred.</p>"
  ],
  [
    "<p>Fix an error in WAL replay logic for SP-GiST indexes\n          (Tom Lane)</p>",
    "<p>This could result in index corruption after a crash,\n          or on a standby server.</p>"
  ],
  [
    "<p>Fix incorrect detection of end-of-base-backup location\n          during WAL recovery (Heikki Linnakangas)</p>",
    "<p>This mistake allowed hot standby mode to start up\n          before the database reaches a consistent state.</p>"
  ],
  [
    "<p>Properly remove startup process's virtual XID lock\n          when promoting a hot standby server to normal running\n          (Simon Riggs)</p>",
    "<p>This oversight could prevent subsequent execution of\n          certain operations such as <code class=\"command\">CREATE\n          INDEX CONCURRENTLY</code>.</p>"
  ],
  [
    "<p>Avoid bogus <span class=\"quote\">&#x201C;<span class=\"quote\">out-of-sequence timeline ID</span>&#x201D;</span> errors\n          in standby mode (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Prevent the postmaster from launching new child\n          processes after it's received a shutdown signal (Tom\n          Lane)</p>",
    "<p>This mistake could result in shutdown taking longer\n          than it should, or even never completing at all without\n          additional user action.</p>"
  ],
  [
    "<p>Fix the syslogger process to not fail when\n          <code class=\"varname\">log_rotation_age</code> exceeds\n          2^31 milliseconds (about 25 days) (Tom Lane)</p>"
  ],
  [
    "<p>Fix <code class=\"function\">WaitLatch()</code> to\n          return promptly when the requested timeout expires (Jeff\n          Janes, Tom Lane)</p>",
    "<p>With the previous coding, a steady stream of\n          non-wait-terminating interrupts could delay return from\n          <code class=\"function\">WaitLatch()</code> indefinitely.\n          This has been shown to be a problem for the autovacuum\n          launcher process, and might cause trouble elsewhere as\n          well.</p>"
  ],
  [
    "<p>Avoid corruption of internal hash tables when out of\n          memory (Hitoshi Harada)</p>"
  ],
  [
    "<p>Prevent file descriptors for dropped tables from being\n          held open past transaction end (Tom Lane)</p>",
    "<p>This should reduce problems with long-since-dropped\n          tables continuing to occupy disk space.</p>"
  ],
  [
    "<p>Prevent database-wide crash and restart when a new\n          child process is unable to create a pipe for its latch\n          (Tom Lane)</p>",
    "<p>Although the new process must fail, there is no good\n          reason to force a database-wide restart, so avoid that.\n          This improves robustness when the kernel is nearly out of\n          file descriptors.</p>"
  ],
  [
    "<p>Avoid planner crash with joins to unflattened\n          subqueries (Tom Lane)</p>"
  ],
  [
    "<p>Fix planning of non-strict equivalence clauses above\n          outer joins (Tom Lane)</p>",
    "<p>The planner could derive incorrect constraints from a\n          clause equating a non-strict construct to something else,\n          for example <code class=\"literal\">WHERE COALESCE(foo, 0)\n          = 0</code> when <code class=\"literal\">foo</code> is\n          coming from the nullable side of an outer join. 9.2\n          showed this type of error in more cases than previous\n          releases, but the basic bug has been there for a long\n          time.</p>"
  ],
  [
    "<p>Fix <code class=\"command\">SELECT DISTINCT</code> with\n          index-optimized <code class=\"function\">MIN</code>/<code class=\"function\">MAX</code>\n          on an inheritance tree (Tom Lane)</p>",
    "<p>The planner would fail with <span class=\"quote\">&#x201C;<span class=\"quote\">failed to re-find\n          MinMaxAggInfo record</span>&#x201D;</span> given this\n          combination of factors.</p>"
  ],
  [
    "<p>Make sure the planner sees implicit and explicit casts\n          as equivalent for all purposes, except in the minority of\n          cases where there's actually a semantic difference (Tom\n          Lane)</p>"
  ],
  [
    "<p>Include join clauses when considering whether partial\n          indexes can be used for a query (Tom Lane)</p>",
    "<p>A strict join clause can be sufficient to establish an\n          <em class=\"replaceable\"><code>x</code></em> <code class=\"literal\">IS NOT NULL</code> predicate, for example. This\n          fixes a planner regression in 9.2, since previous\n          versions could make comparable deductions.</p>"
  ],
  [
    "<p>Limit growth of planning time when there are many\n          indexable join clauses for the same index (Tom Lane)</p>"
  ],
  [
    "<p>Improve planner's ability to prove exclusion\n          constraints from equivalence classes (Tom Lane)</p>"
  ],
  [
    "<p>Fix partial-row matching in hashed subplans to handle\n          cross-type cases correctly (Tom Lane)</p>",
    "<p>This affects multicolumn <code class=\"literal\">NOT\n          IN</code> subplans, such as <code class=\"literal\">WHERE\n          (a, b) NOT IN (SELECT x, y FROM ...)</code> when for\n          instance <code class=\"literal\">b</code> and <code class=\"literal\">y</code> are <code class=\"type\">int4</code> and\n          <code class=\"type\">int8</code> respectively. This mistake\n          led to wrong answers or crashes depending on the specific\n          datatypes involved.</p>"
  ],
  [
    "<p>Fix btree mark/restore functions to handle array keys\n          (Tom Lane)</p>",
    "<p>This oversight could result in wrong answers from\n          merge joins whose inner side is an index scan using an\n          <code class=\"literal\"><em class=\"replaceable\"><code>indexed_column</code></em> =\n          ANY(<em class=\"replaceable\"><code>array</code></em>)</code>\n          condition.</p>"
  ],
  [
    "<p>Revert patch for taking fewer snapshots (Tom Lane)</p>",
    "<p>The 9.2 change to reduce the number of snapshots taken\n          during query execution led to some anomalous behaviors\n          not seen in previous releases, because execution would\n          proceed with a snapshot acquired before locking the\n          tables used by the query. Thus, for example, a query\n          would not be guaranteed to see updates committed by a\n          preceding transaction even if that transaction had\n          exclusive lock. We'll probably revisit this in future\n          releases, but meanwhile put it back the way it was before\n          9.2.</p>"
  ],
  [
    "<p>Acquire buffer lock when re-fetching the old tuple for\n          an <code class=\"literal\">AFTER ROW UPDATE/DELETE</code>\n          trigger (Andres Freund)</p>",
    "<p>In very unusual circumstances, this oversight could\n          result in passing incorrect data to a trigger\n          <code class=\"literal\">WHEN</code> condition, or to the\n          precheck logic for a foreign-key enforcement trigger.\n          That could result in a crash, or in an incorrect decision\n          about whether to fire the trigger.</p>"
  ],
  [
    "<p>Fix <code class=\"command\">ALTER COLUMN TYPE</code> to\n          handle inherited check constraints properly (Pavan\n          Deolasee)</p>",
    "<p>This worked correctly in pre-8.4 releases, and now\n          works correctly in 8.4 and later.</p>"
  ],
  [
    "<p>Fix <code class=\"command\">ALTER EXTENSION SET\n          SCHEMA</code>'s failure to move some subsidiary objects\n          into the new schema (&#xC1;lvaro Herrera, Dimitri\n          Fontaine)</p>"
  ],
  [
    "<p>Handle <code class=\"command\">CREATE TABLE AS\n          EXECUTE</code> correctly in extended query protocol (Tom\n          Lane)</p>"
  ],
  [
    "<p>Don't modify the input parse tree in <code class=\"command\">DROP RULE IF NOT EXISTS</code> and <code class=\"command\">DROP TRIGGER IF NOT EXISTS</code> (Tom\n          Lane)</p>",
    "<p>This mistake would cause errors if a cached statement\n          of one of these types was re-executed.</p>"
  ],
  [
    "<p>Fix <code class=\"command\">REASSIGN OWNED</code> to\n          handle grants on tablespaces (&#xC1;lvaro Herrera)</p>"
  ],
  [
    "<p>Ignore incorrect <code class=\"structname\">pg_attribute</code> entries for system\n          columns for views (Tom Lane)</p>",
    "<p>Views do not have any system columns. However, we\n          forgot to remove such entries when converting a table to\n          a view. That's fixed properly for 9.3 and later, but in\n          previous branches we need to defend against existing\n          mis-converted views.</p>"
  ],
  [
    "<p>Fix rule printing to dump <code class=\"literal\">INSERT\n          INTO <em class=\"replaceable\"><code>table</code></em>\n          DEFAULT VALUES</code> correctly (Tom Lane)</p>"
  ],
  [
    "<p>Guard against stack overflow when there are too many\n          <code class=\"literal\">UNION</code>/<code class=\"literal\">INTERSECT</code>/<code class=\"literal\">EXCEPT</code> clauses in a query (Tom Lane)</p>"
  ],
  [
    "<p>Prevent platform-dependent failures when dividing the\n          minimum possible integer value by -1 (Xi Wang, Tom\n          Lane)</p>"
  ],
  [
    "<p>Fix possible access past end of string in date parsing\n          (Hitoshi Harada)</p>"
  ],
  [
    "<p>Fix failure to advance XID epoch if XID wraparound\n          happens during a checkpoint and <code class=\"varname\">wal_level</code> is <code class=\"literal\">hot_standby</code> (Tom Lane, Andres\n          Freund)</p>",
    "<p>While this mistake had no particular impact on\n          <span class=\"productname\">PostgreSQL</span> itself, it\n          was bad for applications that rely on <code class=\"function\">txid_current()</code> and related functions:\n          the TXID value would appear to go backwards.</p>"
  ],
  [
    "<p>Fix <code class=\"function\">pg_terminate_backend()</code> and <code class=\"function\">pg_cancel_backend()</code> to not throw error\n          for a non-existent target process (Josh Kupershmidt)</p>",
    "<p>This case already worked as intended when called by a\n          superuser, but not so much when called by ordinary\n          users.</p>"
  ],
  [
    "<p>Fix display of <code class=\"structname\">pg_stat_replication</code>.<code class=\"structfield\">sync_state</code> at a page boundary\n          (Kyotaro Horiguchi)</p>"
  ],
  [
    "<p>Produce an understandable error message if the length\n          of the path name for a Unix-domain socket exceeds the\n          platform-specific limit (Tom Lane, Andrew Dunstan)</p>",
    "<p>Formerly, this would result in something quite\n          unhelpful, such as <span class=\"quote\">&#x201C;<span class=\"quote\">Non-recoverable failure in name\n          resolution</span>&#x201D;</span>.</p>"
  ],
  [
    "<p>Fix memory leaks when sending composite column values\n          to the client (Tom Lane)</p>"
  ],
  [
    "<p>Save some cycles by not searching for subtransaction\n          locks at commit (Simon Riggs)</p>",
    "<p>In a transaction holding many exclusive locks, this\n          useless activity could be quite costly.</p>"
  ],
  [
    "<p>Make <span class=\"application\">pg_ctl</span> more\n          robust about reading the <code class=\"filename\">postmaster.pid</code> file (Heikki\n          Linnakangas)</p>",
    "<p>This fixes race conditions and possible file\n          descriptor leakage.</p>"
  ],
  [
    "<p>Fix possible crash in <span class=\"application\">psql</span> if incorrectly-encoded data is\n          presented and the <code class=\"varname\">client_encoding</code> setting is a client-only\n          encoding, such as SJIS (Jiang Guiqing)</p>"
  ],
  [
    "<p>Make <span class=\"application\">pg_dump</span> dump\n          <code class=\"literal\">SEQUENCE SET</code> items in the\n          data not pre-data section of the archive (Tom Lane)</p>",
    "<p>This fixes an undesirable inconsistency between the\n          meanings of <code class=\"option\">--data-only</code> and\n          <code class=\"option\">--section=data</code>, and also\n          fixes dumping of sequences that are marked as extension\n          configuration tables.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_dump</span>'s\n          handling of <code class=\"command\">DROP DATABASE</code>\n          commands in <code class=\"option\">--clean</code> mode\n          (Guillaume Lelarge)</p>",
    "<p>Beginning in 9.2.0, <code class=\"literal\">pg_dump\n          --clean</code> would issue a <code class=\"command\">DROP\n          DATABASE</code> command, which was either useless or\n          dangerous depending on the usage scenario. It no longer\n          does that. This change also fixes the combination of\n          <code class=\"option\">--clean</code> and <code class=\"option\">--create</code> to work sensibly, i.e., emit\n          <code class=\"command\">DROP DATABASE</code> then\n          <code class=\"command\">CREATE DATABASE</code> before\n          reconnecting to the target database.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_dump</span> for views\n          with circular dependencies and no relation options (Tom\n          Lane)</p>",
    "<p>The previous fix to dump relation options when a view\n          is involved in a circular dependency didn't work right\n          for the case that the view has no options; it emitted\n          <code class=\"literal\">ALTER VIEW foo SET ()</code> which\n          is invalid syntax.</p>"
  ],
  [
    "<p>Fix bugs in the <code class=\"filename\">restore.sql</code> script emitted by\n          <span class=\"application\">pg_dump</span> in <code class=\"literal\">tar</code> output format (Tom Lane)</p>",
    "<p>The script would fail outright on tables whose names\n          include upper-case characters. Also, make the script\n          capable of restoring data in <code class=\"option\">--inserts</code> mode as well as the regular\n          COPY mode.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_restore</span> to\n          accept POSIX-conformant <code class=\"literal\">tar</code>\n          files (Brian Weaver, Tom Lane)</p>",
    "<p>The original coding of <span class=\"application\">pg_dump</span>'s <code class=\"literal\">tar</code> output mode produced files that are\n          not fully conformant with the POSIX standard. This has\n          been corrected for version 9.3. This patch updates\n          previous branches so that they will accept both the\n          incorrect and the corrected formats, in hopes of avoiding\n          compatibility problems when 9.3 comes out.</p>"
  ],
  [
    "<p>Fix <code class=\"literal\">tar</code> files emitted by\n          <span class=\"application\">pg_basebackup</span> to be\n          POSIX conformant (Brian Weaver, Tom Lane)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_resetxlog</span> to\n          locate <code class=\"filename\">postmaster.pid</code>\n          correctly when given a relative path to the data\n          directory (Tom Lane)</p>",
    "<p>This mistake could lead to <span class=\"application\">pg_resetxlog</span> not noticing that there\n          is an active postmaster using the data directory.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">libpq</span>'s\n          <code class=\"function\">lo_import()</code> and\n          <code class=\"function\">lo_export()</code> functions to\n          report file I/O errors properly (Tom Lane)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">ecpg</span>'s processing\n          of nested structure pointer variables (Muhammad\n          Usama)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">ecpg</span>'s\n          <code class=\"function\">ecpg_get_data</code> function to\n          handle arrays properly (Michael Meskes)</p>"
  ],
  [
    "<p>Prevent <span class=\"application\">pg_upgrade</span>\n          from trying to process TOAST tables for system catalogs\n          (Bruce Momjian)</p>",
    "<p>This fixes an error seen when the <code class=\"literal\">information_schema</code> has been dropped and\n          recreated. Other failures were also possible.</p>"
  ],
  [
    "<p>Improve <span class=\"application\">pg_upgrade</span>\n          performance by setting <code class=\"varname\">synchronous_commit</code> to <code class=\"literal\">off</code> in the new cluster (Bruce\n          Momjian)</p>"
  ],
  [
    "<p>Make <code class=\"filename\">contrib/pageinspect</code>'s btree page\n          inspection functions take buffer locks while examining\n          pages (Tom Lane)</p>"
  ],
  [
    "<p>Work around unportable behavior of <code class=\"literal\">malloc(0)</code> and <code class=\"literal\">realloc(NULL, 0)</code> (Tom Lane)</p>",
    "<p>On platforms where these calls return <code class=\"literal\">NULL</code>, some code mistakenly thought that\n          meant out-of-memory. This is known to have broken\n          <span class=\"application\">pg_dump</span> for databases\n          containing no user-defined aggregates. There might be\n          other cases as well.</p>"
  ],
  [
    "<p>Ensure that <code class=\"literal\">make install</code>\n          for an extension creates the <code class=\"filename\">extension</code> installation directory\n          (C&#xE9;dric Villemain)</p>",
    "<p>Previously, this step was missed if <code class=\"varname\">MODULEDIR</code> was set in the extension's\n          Makefile.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pgxs</span> support for\n          building loadable modules on AIX (Tom Lane)</p>",
    "<p>Building modules outside the original source tree\n          didn't work on AIX.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2012j for DST law\n          changes in Cuba, Israel, Jordan, Libya, Palestine,\n          Western Samoa, and portions of Brazil.</p>"
  ]
]