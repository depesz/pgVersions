[
  [
    "<p>By default, panic instead of retrying after <code class=\"FUNCTION\">fsync()</code> failure, to avoid possible data corruption (Craig Ringer, Thomas Munro)</p>",
    "<p>Some popular operating systems discard kernel data buffers when unable to write them out, reporting this as <code class=\"FUNCTION\">fsync()</code> failure. If we reissue the <code class=\"FUNCTION\">fsync()</code> request it will succeed, but in fact the data has been lost, so continuing risks database corruption. By raising a panic condition instead, we can replay from WAL, which may contain the only remaining copy of the data in such a situation. While this is surely ugly and inefficient, there are few alternatives, and fortunately the case happens very rarely.</p>",
    "<p>A new server parameter <a href=\"https://www.postgresql.org/docs/9.5/runtime-config-error-handling.html#GUC-DATA-SYNC-RETRY\">data_sync_retry</a> has been added to control this; if you are certain that your kernel does not discard dirty data buffers in such scenarios, you can set <tt class=\"VARNAME\">data_sync_retry</tt> to <tt class=\"LITERAL\">on</tt> to restore the old behavior.</p>"
  ],
  [
    "<p>Include each major release branch's release notes in the documentation for only that branch, rather than that branch and all later ones (Tom Lane)</p>",
    "<p>The duplication induced by the previous policy was getting out of hand. Our plan is to provide a full archive of release notes on the project's web site, but not duplicate it within each release.</p>"
  ],
  [
    "<p>Avoid possible deadlock when acquiring multiple buffer locks (Nishant Fnu)</p>"
  ],
  [
    "<p>Avoid deadlock between hot-standby queries and replay of GIN index page deletion (Alexander Korotkov)</p>"
  ],
  [
    "<p>Fix possible crashes in logical replication when index expressions or predicates are in use (Peter Eisentraut)</p>"
  ],
  [
    "<p>Avoid useless and expensive logical decoding of TOAST data during a table rewrite (Tomas Vondra)</p>"
  ],
  [
    "<p>Fix logic for stopping a subset of WAL senders when synchronous replication is enabled (Paul Guo, Michael Paquier)</p>"
  ],
  [
    "<p>Avoid possibly writing an incorrect replica identity field in a tuple deletion WAL record (Stas Kelvich)</p>"
  ],
  [
    "<p>Make the archiver prioritize WAL history files over WAL data files while choosing which file to archive next (David Steele)</p>"
  ],
  [
    "<p>Fix possible crash in <tt class=\"COMMAND\">UPDATE</tt> with a multiple <tt class=\"LITERAL\">SET</tt> clause using a sub-<tt class=\"LITERAL\">SELECT</tt> as source (Tom Lane)</p>"
  ],
  [
    "<p>Avoid crash if <span class=\"APPLICATION\">libxml2</span> returns a null error message (Sergio Conde Gómez)</p>"
  ],
  [
    "<p>Fix spurious grouping-related parser errors caused by inconsistent handling of collation assignment (Andrew Gierth)</p>",
    "<p>In some cases, expressions that should be considered to match were not seen as matching, if they included operations on collatable data types.</p>"
  ],
  [
    "<p>Check whether the comparison function underlying <code class=\"FUNCTION\">LEAST()</code> or <code class=\"FUNCTION\">GREATEST()</code> is leakproof, rather than just assuming it is (Tom Lane)</p>",
    "<p>Actual information leaks from btree comparison functions are typically hard to provoke, but in principle they could happen.</p>"
  ],
  [
    "<p>Fix incorrect planning of queries in which a lateral reference must be evaluated at a foreign table scan (Tom Lane)</p>"
  ],
  [
    "<p>Fix corner-case underestimation of the cost of a merge join (Tom Lane)</p>",
    "<p>The planner could prefer a merge join when the outer key range is much smaller than the inner key range, even if there are so many duplicate keys on the inner side that this is a poor choice.</p>"
  ],
  [
    "<p>Avoid O(N^2) planning time growth when a query contains many thousand indexable clauses (Tom Lane)</p>"
  ],
  [
    "<p>Improve <tt class=\"COMMAND\">ANALYZE</tt>'s handling of concurrently-updated rows (Jeff Janes, Tom Lane)</p>",
    "<p>Previously, rows deleted by an in-progress transaction were omitted from <tt class=\"COMMAND\">ANALYZE</tt>'s sample, but this has been found to lead to more inconsistency than including them would do. In effect, the sample now corresponds to an MVCC snapshot as of <tt class=\"COMMAND\">ANALYZE</tt>'s start time.</p>"
  ],
  [
    "<p>Make <tt class=\"COMMAND\">TRUNCATE</tt> ignore inheritance child tables that are temporary tables of other sessions (Amit Langote, Michael Paquier)</p>",
    "<p>This brings <tt class=\"COMMAND\">TRUNCATE</tt> into line with the behavior of other commands. Previously, such cases usually ended in failure.</p>"
  ],
  [
    "<p>Fix <tt class=\"COMMAND\">TRUNCATE</tt> to update the statistics counters for the right table (Tom Lane)</p>",
    "<p>If the truncated table had a TOAST table, that table's counters were reset instead.</p>"
  ],
  [
    "<p>Allow <tt class=\"COMMAND\">UNLISTEN</tt> in hot-standby mode (Shay Rojansky)</p>",
    "<p>This is necessarily a no-op, because <tt class=\"COMMAND\">LISTEN</tt> isn't allowed in hot-standby mode; but allowing the dummy operation simplifies session-state-reset logic in clients.</p>"
  ],
  [
    "<p>Fix missing role dependencies in some schema and data type permissions lists (Tom Lane)</p>",
    "<p>In some cases it was possible to drop a role to which permissions had been granted. This caused no immediate problem, but a subsequent dump/reload or upgrade would fail, with symptoms involving attempts to grant privileges to all-numeric role names.</p>"
  ],
  [
    "<p>Ensure relation caches are updated properly after renaming constraints (Amit Langote)</p>"
  ],
  [
    "<p>Make autovacuum more aggressive about removing leftover temporary tables, and also remove leftover temporary tables during <tt class=\"COMMAND\">DISCARD TEMP</tt> (Álvaro Herrera)</p>",
    "<p>This helps ensure that remnants from a crashed session are cleaned up more promptly.</p>"
  ],
  [
    "<p>Prevent empty GIN index pages from being reclaimed too quickly, causing failures of concurrent searches (Andrey Borodin, Alexander Korotkov)</p>"
  ],
  [
    "<p>Fix edge-case failures in float-to-integer coercions (Andrew Gierth, Tom Lane)</p>",
    "<p>Values very slightly above the maximum valid integer value might not be rejected, and then would overflow, producing the minimum valid integer instead. Also, values that should round to the minimum or maximum integer value might be incorrectly rejected.</p>"
  ],
  [
    "<p>Disallow setting <tt class=\"VARNAME\">client_min_messages</tt> higher than <tt class=\"LITERAL\">ERROR</tt> (Jonah Harris, Tom Lane)</p>",
    "<p>Previously, it was possible to set this variable to <tt class=\"LITERAL\">FATAL</tt> or <tt class=\"LITERAL\">PANIC</tt>, which had the effect of suppressing transmission of ordinary error messages to the client. However, that's contrary to guarantees that are given in the <span class=\"PRODUCTNAME\">PostgreSQL</span> wire protocol specification, and it caused some clients to become very confused. In released branches, fix this by silently treating such settings as meaning <tt class=\"LITERAL\">ERROR</tt> instead. Version 12 and later will reject those alternatives altogether.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">ecpglib</span> to use <code class=\"FUNCTION\">uselocale()</code> or <code class=\"FUNCTION\">_configthreadlocale()</code> in preference to <code class=\"FUNCTION\">setlocale()</code> (Michael Meskes, Tom Lane)</p>",
    "<p>Since <code class=\"FUNCTION\">setlocale()</code> is not thread-local, and might not even be thread-safe, the previous coding caused problems in multi-threaded <span class=\"APPLICATION\">ecpg</span> applications.</p>"
  ],
  [
    "<p>Fix incorrect results for numeric data passed through an <span class=\"APPLICATION\">ecpg</span> <acronym class=\"ACRONYM\">SQLDA</acronym> (SQL Descriptor Area) (Daisuke Higuchi)</p>",
    "<p>Values with leading zeroes were not copied correctly.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">psql</span>'s <tt class=\"COMMAND\">\\g</tt> <tt class=\"REPLACEABLE c2\">target</tt> meta-command to work with <tt class=\"COMMAND\">COPY TO STDOUT</tt> (Daniel Vérité)</p>",
    "<p>Previously, the <tt class=\"REPLACEABLE c2\">target</tt> option was ignored, so that the copy data always went to the current query output target.</p>"
  ],
  [
    "<p>Make <span class=\"APPLICATION\">psql</span>'s LaTeX output formats render special characters properly (Tom Lane)</p>",
    "<p>Backslash and some other ASCII punctuation characters were not rendered correctly, leading to document syntax errors or wrong characters in the output.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_dump</span>'s handling of materialized views with indirect dependencies on primary keys (Tom Lane)</p>",
    "<p>This led to mis-labeling of such views' dump archive entries, causing harmless warnings about <span class=\"QUOTE\">\"archive items not in correct section order\"</span>; less harmlessly, selective-restore options depending on those labels, such as <tt class=\"OPTION\">--section</tt>, might misbehave.</p>"
  ],
  [
    "<p>Avoid null-pointer-dereference crash on some platforms when <span class=\"APPLICATION\">pg_dump</span> or <span class=\"APPLICATION\">pg_restore</span> tries to report an error (Tom Lane)</p>"
  ],
  [
    "<p>Fix <tt class=\"FILENAME\">contrib/hstore</tt> to calculate correct hash values for empty <tt class=\"TYPE\">hstore</tt> values that were created in version 8.4 or before (Andrew Gierth)</p>",
    "<p>The previous coding did not give the same result as for an empty <tt class=\"TYPE\">hstore</tt> value created by a newer version, thus potentially causing wrong results in hash joins or hash aggregation. It is advisable to reindex any hash indexes built on <tt class=\"TYPE\">hstore</tt> columns, if the table might contain data that was originally stored as far back as 8.4 and was never dumped/reloaded since then.</p>"
  ],
  [
    "<p>Avoid crashes and excessive runtime with large inputs to <tt class=\"FILENAME\">contrib/intarray</tt>'s <tt class=\"LITERAL\">gist__int_ops</tt> index support (Andrew Gierth)</p>"
  ],
  [
    "<p>Adjust <span class=\"APPLICATION\">configure</span>'s selection of threading-related compiler flags and libraries to match what later <span class=\"PRODUCTNAME\">PostgreSQL</span> releases do (Tom Lane)</p>",
    "<p>The coding previously used in the 9.4 and 9.5 branches fails outright on some newer platforms, so sync it with what 9.6 and later have been doing.</p>"
  ],
  [
    "<p>Support new Makefile variables <tt class=\"LITERAL\">PG_CFLAGS</tt>, <tt class=\"LITERAL\">PG_CXXFLAGS</tt>, and <tt class=\"LITERAL\">PG_LDFLAGS</tt> in <span class=\"APPLICATION\">pgxs</span> builds (Christoph Berg)</p>",
    "<p>This simplifies customization of extension build processes.</p>"
  ],
  [
    "<p>Fix Perl-coded build scripts to not assume <span class=\"QUOTE\">\"<tt class=\"LITERAL\">.</tt>\"</span> is in the search path, since recent Perl versions don't include that (Andrew Dunstan)</p>"
  ],
  [
    "<p>Fix server command-line option parsing problems on OpenBSD (Tom Lane)</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"APPLICATION\">tzdata</span> release 2018i for DST law changes in Kazakhstan, Metlakatla, and Sao Tome and Principe. Kazakhstan's Qyzylorda zone is split in two, creating a new zone Asia/Qostanay, as some areas did not change UTC offset. Historical corrections for Hong Kong and numerous Pacific islands.</p>"
  ]
]