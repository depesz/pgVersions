[
  [
    "<p>Prevent possible loss of committed transactions during\n          crash</p>",
    "<p>Due to insufficient interlocking between transaction\n          commit and checkpointing, it was possible for\n          transactions committed just before the most recent\n          checkpoint to be lost, in whole or in part, following a\n          database crash and restart. This is a serious bug that\n          has existed since <span class=\"productname\">PostgreSQL</span> 7.1.</p>"
  ],
  [
    "<p>Remove asymmetrical word processing in tsearch\n          (Teodor)</p>"
  ],
  [
    "<p>Properly schema-qualify function names when\n          pg_dump'ing a CAST</p>"
  ]
]