[
  [
    "<p>Prevent index corruption when a transaction inserts\n          rows and then aborts close to the end of a concurrent\n          <code class=\"command\">VACUUM</code> on the same table\n          (Tom)</p>"
  ],
  [
    "<p>Make <code class=\"command\">CREATE DOMAIN ... DEFAULT\n          NULL</code> work properly (Tom)</p>"
  ],
  [
    "<p>Fix excessive logging of <acronym class=\"acronym\">SSL</acronym> error messages (Tom)</p>"
  ],
  [
    "<p>Fix logging so that log messages are never interleaved\n          when using the syslogger process (Andrew)</p>"
  ],
  [
    "<p>Fix crash when <code class=\"varname\">log_min_error_statement</code> logging runs out\n          of memory (Tom)</p>"
  ],
  [
    "<p>Fix incorrect handling of some foreign-key corner\n          cases (Tom)</p>"
  ],
  [
    "<p>Prevent <code class=\"command\">CLUSTER</code> from\n          failing due to attempting to process temporary tables of\n          other sessions (Alvaro)</p>"
  ],
  [
    "<p>Update the time zone database rules, particularly New\n          Zealand's upcoming changes (Tom)</p>"
  ],
  [
    "<p>Windows socket improvements (Magnus)</p>"
  ],
  [
    "<p>Suppress timezone name (<code class=\"literal\">%Z</code>) in log timestamps on Windows because\n          of possible encoding mismatches (Tom)</p>"
  ],
  [
    "<p>Require non-superusers who use <code class=\"filename\">/contrib/dblink</code> to use only password\n          authentication, as a security measure (Joe)</p>"
  ]
]