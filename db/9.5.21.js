[
  [
    "<p>Avoid failure in logical decoding when a large transaction must be spilled into many separate temporary files (Amit Khandekar)</p>"
  ],
  [
    "<p>Fix failure in logical replication publisher after a database crash and restart (Vignesh C)</p>"
  ],
  [
    "<p>Avoid memory leak when there are no free dynamic shared memory slots (Thomas Munro)</p>"
  ],
  [
    "<p>Ignore the <tt class=\"LITERAL\">CONCURRENTLY</tt> option when performing an index creation, drop, or rebuild on a temporary table (Michael Paquier, Heikki Linnakangas, Andres Freund)</p>",
    "<p>This avoids strange failures if the temporary table has an <tt class=\"LITERAL\">ON COMMIT</tt> action. There is no benefit in using <tt class=\"LITERAL\">CONCURRENTLY</tt> for a temporary table anyway, since other sessions cannot access the table, making the extra processing pointless.</p>"
  ],
  [
    "<p>Fix possible failure when resetting expression indexes on temporary tables that are marked <tt class=\"LITERAL\">ON COMMIT DELETE ROWS</tt> (Tom Lane)</p>"
  ],
  [
    "<p>Fix possible crash in BRIN index operations with <tt class=\"TYPE\">box</tt>, <tt class=\"TYPE\">range</tt> and <tt class=\"TYPE\">inet</tt> data types (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Fix handling of deleted pages in GIN indexes (Alexander Korotkov)</p>",
    "<p>Avoid possible deadlocks, incorrect updates of a deleted page's state, and failure to traverse through a recently-deleted page.</p>"
  ],
  [
    "<p>Fix possible crash with a SubPlan (sub-<tt class=\"LITERAL\">SELECT</tt>) within a multi-row <tt class=\"LITERAL\">VALUES</tt> list (Tom Lane)</p>"
  ],
  [
    "<p>Fix unlikely crash with pass-by-reference aggregate transition states (Andres Freund, Teodor Sigaev)</p>"
  ],
  [
    "<p>Improve error reporting in <code class=\"FUNCTION\">to_date()</code> and <code class=\"FUNCTION\">to_timestamp()</code> (Tom Lane, Álvaro Herrera)</p>",
    "<p>Reports about incorrect month or day names in input strings could truncate the input in the middle of a multi-byte character, leading to an improperly encoded error message that could cause follow-on failures. Truncate at the next whitespace instead.</p>"
  ],
  [
    "<p>Fix off-by-one result for <tt class=\"LITERAL\">EXTRACT(ISOYEAR FROM <tt class=\"REPLACEABLE c2\">timestamp</tt>)</tt> for BC dates (Tom Lane)</p>"
  ],
  [
    "<p>Avoid stack overflow in <tt class=\"LITERAL\">information_schema</tt> views when a self-referential view exists in the system catalogs (Tom Lane)</p>",
    "<p>A self-referential view can't work; it will always result in infinite recursion. We handled that situation correctly when trying to execute the view, but not when inquiring whether it is automatically updatable.</p>"
  ],
  [
    "<p>Improve performance of hash joins with very large inner relations (Thomas Munro)</p>"
  ],
  [
    "<p>Fix edge-case crashes and misestimations in selectivity calculations for the <tt class=\"LITERAL\">&lt;@</tt> and <tt class=\"LITERAL\">@&gt;</tt> range operators (Michael Paquier, Andrey Borodin, Tom Lane)</p>"
  ],
  [
    "<p>Improve error reporting for attempts to use automatic updating of views with conditional <tt class=\"LITERAL\">INSTEAD</tt> rules (Dean Rasheed)</p>",
    "<p>This has never been supported, but previously the error was thrown only at execution time, so that it could be masked by planner errors.</p>"
  ],
  [
    "<p>Prevent a composite type from being included in itself indirectly via a range type (Tom Lane, Julien Rouhaud)</p>"
  ],
  [
    "<p>Fix error reporting for index expressions of prohibited types (Amit Langote)</p>"
  ],
  [
    "<p>Fix dumping of views that contain only a <tt class=\"LITERAL\">VALUES</tt> list to handle cases where a view output column has been renamed (Tom Lane)</p>"
  ],
  [
    "<p>Allow <span class=\"APPLICATION\">libpq</span> to parse all GSS-related connection parameters even when the GSSAPI code hasn't been compiled in (Tom Lane)</p>",
    "<p>This makes the behavior similar to our SSL support, where it was long ago deemed to be a good idea to always accept all the related parameters, even if some are ignored or restricted due to lack of the feature in a particular build.</p>"
  ],
  [
    "<p>Fix incorrect handling of <tt class=\"LITERAL\">%b</tt> and <tt class=\"LITERAL\">%B</tt> format codes in <span class=\"APPLICATION\">ecpg</span>'s <code class=\"FUNCTION\">PGTYPEStimestamp_fmt_asc()</code> function (Tomas Vondra)</p>",
    "<p>Due to an off-by-one error, these codes would print the wrong month name, or possibly crash.</p>"
  ],
  [
    "<p>Fix parallel <span class=\"APPLICATION\">pg_dump</span>/<span class=\"APPLICATION\">pg_restore</span> to more gracefully handle failure to create worker processes (Tom Lane)</p>"
  ],
  [
    "<p>Prevent possible crash or lockup when attempting to terminate a parallel <span class=\"APPLICATION\">pg_dump</span>/<span class=\"APPLICATION\">pg_restore</span> run via a signal (Tom Lane)</p>"
  ],
  [
    "<p>In <span class=\"APPLICATION\">pg_upgrade</span>, look inside arrays and ranges while searching for non-upgradable data types in tables (Tom Lane)</p>"
  ],
  [
    "<p>Apply more thorough syntax checking to <span class=\"APPLICATION\">createuser</span>'s <tt class=\"OPTION\">--connection-limit</tt> option (Álvaro Herrera)</p>"
  ],
  [
    "<p>In <tt class=\"FILENAME\">contrib/dict_int</tt>, reject <tt class=\"VARNAME\">maxlen</tt> settings less than one (Tomas Vondra)</p>",
    "<p>This prevents a possible crash with silly settings for that parameter.</p>"
  ],
  [
    "<p>Disallow NULL category values in <tt class=\"FILENAME\">contrib/tablefunc</tt>'s <code class=\"FUNCTION\">crosstab()</code> function (Joe Conway)</p>",
    "<p>This case never worked usefully, and it would crash on some platforms.</p>"
  ],
  [
    "<p>Mark some timeout and statistics-tracking GUC variables as <tt class=\"LITERAL\">PGDLLIMPORT</tt>, to allow extensions to access them on Windows (Pascal Legrand)</p>",
    "<p>This applies to <tt class=\"LITERAL\">idle_in_transaction_session_timeout</tt>, <tt class=\"LITERAL\">lock_timeout</tt>, <tt class=\"LITERAL\">statement_timeout</tt>, <tt class=\"LITERAL\">track_activities</tt>, <tt class=\"LITERAL\">track_counts</tt>, and <tt class=\"LITERAL\">track_functions</tt>.</p>"
  ],
  [
    "<p>Fix race condition that led to delayed delivery of interprocess signals on Windows (Amit Kapila)</p>",
    "<p>This caused visible timing oddities in <tt class=\"COMMAND\">NOTIFY</tt>, and perhaps other misbehavior.</p>"
  ],
  [
    "<p>On Windows, retry a few times after an <tt class=\"LITERAL\">ERROR_ACCESS_DENIED</tt> file access failure (Alexander Lakhin, Tom Lane)</p>",
    "<p>This helps cope with cases where a file open attempt fails because the targeted file is flagged for deletion but not yet actually gone. <span class=\"APPLICATION\">pg_ctl</span>, for example, frequently failed with such an error when probing to see if the postmaster had shut down yet.</p>"
  ]
]