[
  [
    "<p>User-defined objects that reference certain built-in array functions along with their argument types must be recreated (Tom Lane)</p>",
    "<p>Specifically, <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-array.html\" title=\"9.19. Array Functions and Operators\"><code class=\"function\">array_append()</code></a>, <code class=\"function\">array_prepend()</code>, <code class=\"function\">array_cat()</code>, <code class=\"function\">array_position()</code>, <code class=\"function\">array_positions()</code>, <code class=\"function\">array_remove()</code>, <code class=\"function\">array_replace()</code>, and <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-math.html\" title=\"9.3. Mathematical Functions and Operators\"><code class=\"function\">width_bucket()</code></a> used to take <code class=\"type\">anyarray</code> arguments but now take <code class=\"type\">anycompatiblearray</code>. Therefore, user-defined objects like aggregates and operators that reference those array function signatures must be dropped before upgrading, and recreated once the upgrade completes.</p>"
  ],
  [
    "<p>Remove deprecated containment operators <code class=\"literal\">@</code> and <code class=\"literal\">~</code> for built-in <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-geometry.html\" title=\"9.11. Geometric Functions and Operators\">geometric data types</a> and contrib modules <a class=\"xref\" href=\"https://www.postgresql.org/docs/14/cube.html\" title=\"F.9. cube\">cube</a>, <a class=\"xref\" href=\"https://www.postgresql.org/docs/14/hstore.html\" title=\"F.16. hstore\">hstore</a>, <a class=\"xref\" href=\"https://www.postgresql.org/docs/14/intarray.html\" title=\"F.18. intarray\">intarray</a>, and <a class=\"xref\" href=\"https://www.postgresql.org/docs/14/seg.html\" title=\"F.36. seg\">seg</a> (Justin Pryzby)</p>",
    "<p>The more consistently named <code class=\"literal\">&lt;@</code> and <code class=\"literal\">@&gt;</code> have been recommended for many years.</p>"
  ],
  [
    "<p>Fix <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-textsearch.html\" title=\"9.13. Text Search Functions and Operators\"><code class=\"function\">to_tsquery()</code></a> and <code class=\"function\">websearch_to_tsquery()</code> to properly parse query text containing discarded tokens (Alexander Korotkov)</p>",
    "<p>Certain discarded tokens, like underscore, caused the output of these functions to produce incorrect tsquery output, e.g., both <code class=\"literal\">websearch_to_tsquery('\"pg_class pg\"')</code> and <code class=\"literal\">to_tsquery('pg_class &lt;-&gt; pg')</code> used to output <code class=\"literal\">( 'pg' &amp; 'class' ) &lt;-&gt; 'pg'</code>, but now both output <code class=\"literal\">'pg' &lt;-&gt; 'class' &lt;-&gt; 'pg'</code>.</p>"
  ],
  [
    "<p>Fix <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-textsearch.html\" title=\"9.13. Text Search Functions and Operators\"><code class=\"function\">websearch_to_tsquery()</code></a> to properly parse multiple adjacent discarded tokens in quotes (Alexander Korotkov)</p>",
    "<p>Previously, quoted text that contained multiple adjacent discarded tokens was treated as multiple tokens, causing incorrect tsquery output, e.g., <code class=\"literal\">websearch_to_tsquery('\"aaa: bbb\"')</code> used to output <code class=\"literal\">'aaa' &lt;2&gt; 'bbb'</code>, but now outputs <code class=\"literal\">'aaa' &lt;-&gt; 'bbb'</code>.</p>"
  ],
  [
    "<p>Change <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-datetime.html\" title=\"9.9. Date/Time Functions and Operators\"><code class=\"function\">EXTRACT()</code></a> to return type <code class=\"type\">numeric</code> instead of <code class=\"type\">float8</code> (Peter Eisentraut)</p>",
    "<p>This avoids loss-of-precision issues in some usages. The old behavior can still be obtained by using the old underlying function <code class=\"function\">date_part()</code>.</p>",
    "<p>Also, <code class=\"function\">EXTRACT(date)</code> now throws an error for units that are not part of the <code class=\"type\">date</code> data type.</p>"
  ],
  [
    "<p>Change <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-aggregate.html\" title=\"9.21. Aggregate Functions\"><code class=\"function\">var_samp()</code></a> and <code class=\"function\">stddev_samp()</code> with numeric parameters to return NULL when the input is a single NaN value (Tom Lane)</p>",
    "<p>Previously <code class=\"literal\">NaN</code> was returned.</p>"
  ],
  [
    "<p>Return false for <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-info.html\" title=\"9.26. System Information Functions and Operators\"><code class=\"function\">has_column_privilege()</code></a> checks on non-existent or dropped columns when using attribute numbers (Joe Conway)</p>",
    "<p>Previously such attribute numbers returned an invalid-column error.</p>"
  ],
  [
    "<p>Fix handling of infinite <a class=\"link\" href=\"https://www.postgresql.org/docs/14/sql-expressions.html#SYNTAX-WINDOW-FUNCTIONS\" title=\"4.2.8. Window Function Calls\">window function</a> ranges (Tom Lane)</p>",
    "<p>Previously window frame clauses like <code class=\"literal\">'inf' PRECEDING AND 'inf' FOLLOWING</code> returned incorrect results.</p>"
  ],
  [
    "<p>Remove factorial operators <code class=\"literal\">!</code> and <code class=\"literal\">!!</code>, as well as function <code class=\"function\">numeric_fac()</code> (Mark Dilger)</p>",
    "<p>The <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-math.html\" title=\"9.3. Mathematical Functions and Operators\"><code class=\"function\">factorial()</code></a> function is still supported.</p>"
  ],
  [
    "<p>Disallow <code class=\"function\">factorial()</code> of negative numbers (Peter Eisentraut)</p>",
    "<p>Previously such cases returned 1.</p>"
  ],
  [
    "<p>Remove support for <a class=\"link\" href=\"https://www.postgresql.org/docs/14/sql-createoperator.html\" title=\"CREATE OPERATOR\">postfix</a> (right-unary) operators (Mark Dilger)</p>",
    "<p><span class=\"application\">pg_dump</span> and <span class=\"application\">pg_upgrade</span> will warn if postfix operators are being dumped.</p>"
  ],
  [
    "<p>Allow <code class=\"literal\">\\D</code> and <code class=\"literal\">\\W</code> shorthands to match newlines in <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-matching.html#FUNCTIONS-POSIX-REGEXP\" title=\"9.7.3. POSIX Regular Expressions\">regular expression</a> newline-sensitive mode (Tom Lane)</p>",
    "<p>Previously they did not match newlines in this mode, but that disagrees with the behavior of other common regular expression engines. <code class=\"literal\">[^[:digit:]]</code> or <code class=\"literal\">[^[:word:]]</code> can be used to get the old behavior.</p>"
  ],
  [
    "<p>Disregard constraints when matching regular expression <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-matching.html#POSIX-ESCAPE-SEQUENCES\" title=\"9.7.3.3. Regular Expression Escapes\">back-references</a> (Tom Lane)</p>",
    "<p>For example, in <code class=\"literal\">(^\\d+).*\\1</code>, the <code class=\"literal\">^</code> constraint should be applied at the start of the string, but not when matching <code class=\"literal\">\\1</code>.</p>"
  ],
  [
    "<p>Disallow <code class=\"literal\">\\w</code> as a range start or end in regular expression character classes (Tom Lane)</p>",
    "<p>This previously was allowed but produced unexpected results.</p>"
  ],
  [
    "<p>Require <a class=\"link\" href=\"https://www.postgresql.org/docs/14/runtime-config-custom.html\" title=\"20.16. Customized Options\">custom server parameter</a> names to use only characters that are valid in unquoted <acronym class=\"acronym\">SQL</acronym> identifiers (Tom Lane)</p>"
  ],
  [
    "<p>Change the default of the <a class=\"xref\" href=\"https://www.postgresql.org/docs/14/runtime-config-connection.html#GUC-PASSWORD-ENCRYPTION\">password_encryption</a> server parameter to <code class=\"literal\">scram-sha-256</code> (Peter Eisentraut)</p>",
    "<p>Previously it was <code class=\"literal\">md5</code>. All new passwords will be stored as SHA256 unless this server setting is changed or the password is specified in MD5 format. Also, the legacy (and undocumented) Boolean-like values which were previously synonyms for <code class=\"literal\">md5</code> are no longer accepted.</p>"
  ],
  [
    "<p>Remove server parameter <code class=\"varname\">vacuum_cleanup_index_scale_factor</code> (Peter Geoghegan)</p>",
    "<p>This setting was ignored starting in <span class=\"productname\">PostgreSQL</span> version 13.3.</p>"
  ],
  [
    "<p>Remove server parameter <code class=\"varname\">operator_precedence_warning</code> (Tom Lane)</p>",
    "<p>This setting was used for warning applications about <span class=\"productname\">PostgreSQL</span> 9.5 changes.</p>"
  ],
  [
    "<p>Overhaul the specification of <code class=\"literal\">clientcert</code> in <a class=\"link\" href=\"https://www.postgresql.org/docs/14/auth-pg-hba-conf.html\" title=\"21.1. The pg_hba.conf File\"><code class=\"filename\">pg_hba.conf</code></a> (Kyotaro Horiguchi)</p>",
    "<p>Values <code class=\"literal\">1</code>/<code class=\"literal\">0</code>/<code class=\"literal\">no-verify</code> are no longer supported; only the strings <code class=\"literal\">verify-ca</code> and <code class=\"literal\">verify-full</code> can be used. Also, disallow <code class=\"literal\">verify-ca</code> if cert authentication is enabled since cert requires <code class=\"literal\">verify-full</code> checking.</p>"
  ],
  [
    "<p>Remove support for <a class=\"link\" href=\"https://www.postgresql.org/docs/14/runtime-config-connection.html#RUNTIME-CONFIG-CONNECTION-SSL\" title=\"20.3.3. SSL\"><acronym class=\"acronym\">SSL</acronym></a> compression (Daniel Gustafsson, Michael Paquier)</p>",
    "<p>This was already disabled by default in previous <span class=\"productname\">PostgreSQL</span> releases, and most modern OpenSSL and <acronym class=\"acronym\">TLS</acronym> versions no longer support it.</p>"
  ],
  [
    "<p>Remove server and <a class=\"link\" href=\"https://www.postgresql.org/docs/14/libpq.html\" title=\"Chapter 34. libpq — C Library\">libpq</a> support for the version 2 <a class=\"link\" href=\"https://www.postgresql.org/docs/14/protocol.html\" title=\"Chapter 53. Frontend/Backend Protocol\">wire protocol</a> (Heikki Linnakangas)</p>",
    "<p>This was last used as the default in <span class=\"productname\">PostgreSQL</span> 7.3 (released in 2002).</p>"
  ],
  [
    "<p>Disallow single-quoting of the language name in the <a class=\"link\" href=\"https://www.postgresql.org/docs/14/sql-createlanguage.html\" title=\"CREATE LANGUAGE\"><code class=\"command\">CREATE/DROP LANGUAGE</code></a> command (Peter Eisentraut)</p>"
  ],
  [
    "<p>Remove the <a class=\"link\" href=\"https://www.postgresql.org/docs/14/xfunc-sql.html#XFUNC-SQL-COMPOSITE-FUNCTIONS\" title=\"38.5.3. SQL Functions on Composite Types\">composite types</a> that were formerly created for sequences and toast tables (Tom Lane)</p>"
  ],
  [
    "<p>Process doubled quote marks in <a class=\"link\" href=\"https://www.postgresql.org/docs/14/ecpg.html\" title=\"Chapter 36. ECPG — Embedded SQL in C\">ecpg</a> <acronym class=\"acronym\">SQL</acronym> command strings correctly (Tom Lane)</p>",
    "<p>Previously <code class=\"literal\">'abc''def'</code> was passed to the server as <code class=\"literal\">'abc'def'</code>, and <code class=\"literal\">\"abc\"\"def\"</code> was passed as <code class=\"literal\">\"abc\"def\"</code>, causing syntax errors.</p>"
  ],
  [
    "<p>Prevent the containment operators (<code class=\"literal\">&lt;@</code> and <code class=\"literal\">@&gt;</code>) for <a class=\"xref\" href=\"https://www.postgresql.org/docs/14/intarray.html\" title=\"F.18. intarray\">intarray</a> from using GiST indexes (Tom Lane)</p>",
    "<p>Previously a full GiST index scan was required, so just avoid that and scan the heap, which is faster. Indexes created for this purpose should be removed.</p>"
  ],
  [
    "<p>Remove contrib program <span class=\"application\">pg_standby</span> (Justin Pryzby)</p>"
  ],
  [
    "<p>Prevent <a class=\"xref\" href=\"https://www.postgresql.org/docs/14/tablefunc.html\" title=\"F.40. tablefunc\">tablefunc</a>'s function <code class=\"function\">normal_rand()</code> from accepting negative values (Ashutosh Bapat)</p>",
    "<p>Negative values produced undesirable results.</p>"
  ],
  [
    "<p>Add predefined roles <a class=\"link\" href=\"https://www.postgresql.org/docs/14/predefined-roles.html\" title=\"22.5. Predefined Roles\"><code class=\"literal\">pg_read_all_data</code></a> and <code class=\"literal\">pg_write_all_data</code> (Stephen Frost)</p>",
    "<p>These non-login roles can be used to give read or write permission to all tables, views, and sequences.</p>"
  ],
  [
    "<p>Add predefined role <a class=\"link\" href=\"https://www.postgresql.org/docs/14/predefined-roles.html\" title=\"22.5. Predefined Roles\"><code class=\"literal\">pg_database_owner</code></a> that contains only the current database's owner (Noah Misch)</p>",
    "<p>This is especially useful in template databases.</p>"
  ],
  [
    "<p>Remove temporary files after backend crashes (Euler Taveira)</p>",
    "<p>Previously, such files were retained for debugging purposes. If necessary, deletion can be disabled with the new server parameter <a class=\"xref\" href=\"https://www.postgresql.org/docs/14/runtime-config-developer.html#GUC-REMOVE-TEMP-FILES-AFTER-CRASH\">remove_temp_files_after_crash</a>.</p>"
  ],
  [
    "<p>Allow long-running queries to be canceled if the client disconnects (Sergey Cherkashin, Thomas Munro)</p>",
    "<p>The server parameter <a class=\"xref\" href=\"https://www.postgresql.org/docs/14/runtime-config-connection.html#GUC-CLIENT-CONNECTION-CHECK-INTERVAL\">client_connection_check_interval</a> allows control over whether loss of connection is checked for intra-query. (This is supported on Linux and a few other operating systems.)</p>"
  ],
  [
    "<p>Add an optional timeout parameter to <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-admin.html#FUNCTIONS-ADMIN-SIGNAL\" title=\"9.27.2. Server Signaling Functions\"><code class=\"function\">pg_terminate_backend()</code></a></p>"
  ],
  [
    "<p>Allow wide tuples to be always added to almost-empty heap pages (John Naylor, Floris van Nee)</p>",
    "<p>Previously tuples whose insertion would have exceeded the page's <a class=\"link\" href=\"https://www.postgresql.org/docs/14/sql-createtable.html\" title=\"CREATE TABLE\">fill factor</a> were instead added to new pages.</p>"
  ],
  [
    "<p>Add Server Name Indication (<acronym class=\"acronym\">SNI</acronym>) in <acronym class=\"acronym\">SSL</acronym> connection packets (Peter Eisentraut)</p>",
    "<p>This can be disabled by turning off client connection option <a class=\"link\" href=\"https://www.postgresql.org/docs/14/libpq-connect.html#LIBPQ-PARAMKEYWORDS\" title=\"34.1.2. Parameter Key Words\"><code class=\"literal\">sslsni</code></a>.</p>"
  ],
  [
    "<p>Allow vacuum to skip index vacuuming when the number of removable index entries is insignificant (Masahiko Sawada, Peter Geoghegan)</p>",
    "<p>The vacuum parameter <a class=\"link\" href=\"https://www.postgresql.org/docs/14/sql-vacuum.html\" title=\"VACUUM\"><code class=\"literal\">INDEX_CLEANUP</code></a> has a new default of <code class=\"literal\">auto</code> that enables this optimization.</p>"
  ],
  [
    "<p>Allow vacuum to more eagerly add deleted btree pages to the free space map (Peter Geoghegan)</p>",
    "<p>Previously vacuum could only add pages to the free space map that were marked as deleted by previous vacuums.</p>"
  ],
  [
    "<p>Allow vacuum to reclaim space used by unused trailing heap line pointers (Matthias van de Meent, Peter Geoghegan)</p>"
  ],
  [
    "<p>Allow vacuum to be more aggressive in removing dead rows during minimal-locking index operations (Álvaro Herrera)</p>",
    "<p>Specifically, <code class=\"command\">CREATE INDEX CONCURRENTLY</code> and <code class=\"command\">REINDEX CONCURRENTLY</code> no longer limit the dead row removal of other relations.</p>"
  ],
  [
    "<p>Speed up vacuuming of databases with many relations (Tatsuhito Kasahara)</p>"
  ],
  [
    "<p>Reduce the default value of <a class=\"xref\" href=\"https://www.postgresql.org/docs/14/runtime-config-resource.html#GUC-VACUUM-COST-PAGE-MISS\">vacuum_cost_page_miss</a> to better reflect current hardware capabilities (Peter Geoghegan)</p>"
  ],
  [
    "<p>Add ability to skip vacuuming of <acronym class=\"acronym\">TOAST</acronym> tables (Nathan Bossart)</p>",
    "<p><a class=\"link\" href=\"https://www.postgresql.org/docs/14/sql-vacuum.html\" title=\"VACUUM\"><code class=\"command\">VACUUM</code></a> now has a <code class=\"literal\">PROCESS_TOAST</code> option which can be set to false to disable <acronym class=\"acronym\">TOAST</acronym> processing, and <a class=\"link\" href=\"https://www.postgresql.org/docs/14/app-vacuumdb.html\" title=\"vacuumdb\"><span class=\"application\">vacuumdb</span></a> has a <code class=\"option\">--no-process-toast</code> option.</p>"
  ],
  [
    "<p>Have <a class=\"link\" href=\"https://www.postgresql.org/docs/14/sql-copy.html\" title=\"COPY\"><code class=\"command\">COPY FREEZE</code></a> appropriately update page visibility bits (Anastasia Lubennikova, Pavan Deolasee, Jeff Janes)</p>"
  ],
  [
    "<p>Cause vacuum operations to be more aggressive if the table is near xid or multixact wraparound (Masahiko Sawada, Peter Geoghegan)</p>",
    "<p>This is controlled by <a class=\"xref\" href=\"https://www.postgresql.org/docs/14/runtime-config-client.html#GUC-VACUUM-FAILSAFE-AGE\">vacuum_failsafe_age</a> and <a class=\"xref\" href=\"https://www.postgresql.org/docs/14/runtime-config-client.html#GUC-MULTIXACT-FAILSAFE-AGE\">vacuum_multixact_failsafe_age</a>.</p>"
  ],
  [
    "<p>Increase warning time and hard limit before transaction id and multi-transaction wraparound (Noah Misch)</p>",
    "<p>This should reduce the possibility of failures that occur without having issued warnings about wraparound.</p>"
  ],
  [
    "<p>Add per-index information to <a class=\"link\" href=\"https://www.postgresql.org/docs/14/runtime-config-logging.html#GUC-LOG-AUTOVACUUM-MIN-DURATION\">autovacuum logging output</a> (Masahiko Sawada)</p>"
  ],
  [
    "<p>Improve the performance of updates and deletes on partitioned tables with many partitions (Amit Langote, Tom Lane)</p>",
    "<p>This change greatly reduces the planner's overhead for such cases, and also allows updates/deletes on partitioned tables to use execution-time partition pruning.</p>"
  ],
  [
    "<p>Allow partitions to be <a class=\"link\" href=\"https://www.postgresql.org/docs/14/sql-altertable.html\" title=\"ALTER TABLE\">detached</a> in a non-blocking manner (Álvaro Herrera)</p>",
    "<p>The syntax is <code class=\"command\">ALTER TABLE ... DETACH PARTITION ... CONCURRENTLY</code>, and <code class=\"command\">FINALIZE</code>.</p>"
  ],
  [
    "<p>Ignore <code class=\"literal\">COLLATE</code> clauses in partition boundary values (Tom Lane)</p>",
    "<p>Previously any such clause had to match the collation of the partition key; but it's more consistent to consider that it's automatically coerced to the collation of the partition key.</p>"
  ],
  [
    "<p>Allow btree index additions to <a class=\"link\" href=\"https://www.postgresql.org/docs/14/btree-implementation.html#BTREE-DELETION\" title=\"64.4.2. Bottom-up Index Deletion\">remove expired index entries</a> to prevent page splits (Peter Geoghegan)</p>",
    "<p>This is particularly helpful for reducing index bloat on tables whose indexed columns are frequently updated.</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/14/brin.html\" title=\"Chapter 68. BRIN Indexes\"><acronym class=\"acronym\">BRIN</acronym></a> indexes to record multiple min/max values per range (Tomas Vondra)</p>",
    "<p>This is useful if there are groups of values in each page range.</p>"
  ],
  [
    "<p>Allow <acronym class=\"acronym\">BRIN</acronym> indexes to use bloom filters (Tomas Vondra)</p>",
    "<p>This allows <acronym class=\"acronym\">BRIN</acronym> indexes to be used effectively with data that is not well-localized in the heap.</p>"
  ],
  [
    "<p>Allow some <a class=\"link\" href=\"https://www.postgresql.org/docs/14/gist.html\" title=\"Chapter 65. GiST Indexes\">GiST</a> indexes to be built by presorting the data (Andrey Borodin)</p>",
    "<p>Presorting happens automatically and allows for faster index creation and smaller indexes.</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/14/spgist.html\" title=\"Chapter 66. SP-GiST Indexes\">SP-GiST</a> indexes to contain <code class=\"literal\">INCLUDE</code>'d columns (Pavel Borisov)</p>"
  ],
  [
    "<p>Allow hash lookup for <code class=\"literal\">IN</code> clauses with many constants (James Coleman, David Rowley)</p>",
    "<p>Previously the code always sequentially scanned the list of values.</p>"
  ],
  [
    "<p>Increase the number of places <a class=\"link\" href=\"https://www.postgresql.org/docs/14/planner-stats.html#PLANNER-STATS-EXTENDED\" title=\"14.2.2. Extended Statistics\">extended statistics</a> can be used for <code class=\"literal\">OR</code> clause estimation (Tomas Vondra, Dean Rasheed)</p>"
  ],
  [
    "<p>Allow extended statistics on expressions (Tomas Vondra)</p>",
    "<p>This allows statistics on a group of expressions and columns, rather than only columns like previously. System view <a class=\"link\" href=\"https://www.postgresql.org/docs/14/view-pg-stats-ext-exprs.html\" title=\"52.91. pg_stats_ext_exprs\"><code class=\"structname\">pg_stats_ext_exprs</code></a> reports such statistics.</p>"
  ],
  [
    "<p>Allow efficient heap scanning of a range of <a class=\"link\" href=\"https://www.postgresql.org/docs/14/datatype-oid.html#DATATYPE-OID-TABLE\" title=\"Table 8.26. Object Identifier Types\"><code class=\"type\">TIDs</code></a> (Edmund Horner, David Rowley)</p>",
    "<p>Previously a sequential scan was required for non-equality <code class=\"type\">TID</code> specifications.</p>"
  ],
  [
    "<p>Fix <a class=\"link\" href=\"https://www.postgresql.org/docs/14/sql-explain.html\" title=\"EXPLAIN\"><code class=\"command\">EXPLAIN CREATE TABLE AS</code></a> and <code class=\"command\">EXPLAIN CREATE MATERIALIZED VIEW</code> to honor <code class=\"literal\">IF NOT EXISTS</code> (Bharath Rupireddy)</p>",
    "<p>Previously, if the object already existed, <code class=\"command\">EXPLAIN</code> would fail.</p>"
  ],
  [
    "<p>Improve the speed of computing <acronym class=\"acronym\">MVCC</acronym> <a class=\"link\" href=\"https://www.postgresql.org/docs/14/mvcc.html\" title=\"Chapter 13. Concurrency Control\">visibility snapshots</a> on systems with many CPUs and high session counts (Andres Freund)</p>",
    "<p>This also improves performance when there are many idle sessions.</p>"
  ],
  [
    "<p>Add executor method to memoize results from the inner side of a nested-loop join (David Rowley)</p>",
    "<p>This is useful if only a small percentage of rows is checked on the inner side. It can be disabled via server parameter <a class=\"xref\" href=\"https://www.postgresql.org/docs/14/runtime-config-query.html#GUC-ENABLE-MEMOIZE\">enable_memoize</a>.</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-window.html\" title=\"9.22. Window Functions\">window functions</a> to perform incremental sorts (David Rowley)</p>"
  ],
  [
    "<p>Improve the I/O performance of parallel sequential scans (Thomas Munro, David Rowley)</p>",
    "<p>This was done by allocating blocks in groups to <a class=\"link\" href=\"https://www.postgresql.org/docs/14/runtime-config-resource.html#GUC-MAX-PARALLEL-WORKERS\">parallel workers</a>.</p>"
  ],
  [
    "<p>Allow a query referencing multiple <a class=\"link\" href=\"https://www.postgresql.org/docs/14/sql-createforeigntable.html\" title=\"CREATE FOREIGN TABLE\">foreign tables</a> to perform foreign table scans in parallel (Robert Haas, Kyotaro Horiguchi, Thomas Munro, Etsuro Fujita)</p>",
    "<p><a class=\"link\" href=\"https://www.postgresql.org/docs/14/postgres-fdw.html\" title=\"F.35. postgres_fdw\"><span class=\"application\">postgres_fdw</span></a> supports this type of scan if <code class=\"literal\">async_capable</code> is set.</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/14/routine-vacuuming.html#VACUUM-FOR-STATISTICS\" title=\"25.1.3. Updating Planner Statistics\">analyze</a> to do page prefetching (Stephen Frost)</p>",
    "<p>This is controlled by <a class=\"xref\" href=\"https://www.postgresql.org/docs/14/runtime-config-resource.html#GUC-MAINTENANCE-IO-CONCURRENCY\">maintenance_io_concurrency</a>.</p>"
  ],
  [
    "<p>Improve performance of <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-matching.html#FUNCTIONS-POSIX-REGEXP\" title=\"9.7.3. POSIX Regular Expressions\">regular expression</a> searches (Tom Lane)</p>"
  ],
  [
    "<p>Dramatically improve Unicode normalization (John Naylor)</p>",
    "<p>This speeds <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-string.html\" title=\"9.4. String Functions and Operators\"><code class=\"function\">normalize()</code></a> and <code class=\"literal\">IS NORMALIZED</code>.</p>"
  ],
  [
    "<p>Add ability to use <a class=\"link\" href=\"https://www.postgresql.org/docs/14/sql-createtable.html\" title=\"CREATE TABLE\">LZ4 compression</a> on <acronym class=\"acronym\">TOAST</acronym> data (Dilip Kumar)</p>",
    "<p>This can be set at the column level, or set as a default via server parameter <a class=\"xref\" href=\"https://www.postgresql.org/docs/14/runtime-config-client.html#GUC-DEFAULT-TOAST-COMPRESSION\">default_toast_compression</a>. The server must be compiled with <a class=\"link\" href=\"https://www.postgresql.org/docs/14/install-procedure.html#CONFIGURE-OPTIONS-FEATURES\" title=\"17.4.1.2. PostgreSQL Features\"><code class=\"option\">--with-lz4</code></a> to support this feature. The default setting is still pglz.</p>"
  ],
  [
    "<p>If server parameter <a class=\"xref\" href=\"https://www.postgresql.org/docs/14/runtime-config-statistics.html#GUC-COMPUTE-QUERY-ID\">compute_query_id</a> is enabled, display the query id in <a class=\"link\" href=\"https://www.postgresql.org/docs/14/monitoring-stats.html#MONITORING-PG-STAT-ACTIVITY-VIEW\" title=\"28.2.3. pg_stat_activity\"><code class=\"structname\">pg_stat_activity</code></a>, <a class=\"link\" href=\"https://www.postgresql.org/docs/14/sql-explain.html\" title=\"EXPLAIN\"><code class=\"command\">EXPLAIN VERBOSE</code></a>, <a class=\"link\" href=\"https://www.postgresql.org/docs/14/runtime-config-logging.html\" title=\"20.8. Error Reporting and Logging\">csvlog</a>, and optionally in <a class=\"xref\" href=\"https://www.postgresql.org/docs/14/runtime-config-logging.html#GUC-LOG-LINE-PREFIX\">log_line_prefix</a> (Julien Rouhaud)</p>",
    "<p>A query id computed by an extension will also be displayed.</p>"
  ],
  [
    "<p>Improve logging of <a class=\"link\" href=\"https://www.postgresql.org/docs/14/routine-vacuuming.html#AUTOVACUUM\" title=\"25.1.6. The Autovacuum Daemon\">auto-vacuum</a> and auto-analyze (Stephen Frost, Jakub Wartak)</p>",
    "<p>This reports I/O timings for auto-vacuum and auto-analyze if <a class=\"xref\" href=\"https://www.postgresql.org/docs/14/runtime-config-statistics.html#GUC-TRACK-IO-TIMING\">track_io_timing</a> is enabled. Also, report buffer read and dirty rates for auto-analyze.</p>"
  ],
  [
    "<p>Add information about the original user name supplied by the client to the output of <a class=\"xref\" href=\"https://www.postgresql.org/docs/14/runtime-config-logging.html#GUC-LOG-CONNECTIONS\">log_connections</a> (Jacob Champion)</p>"
  ],
  [
    "<p>Add system view <a class=\"link\" href=\"https://www.postgresql.org/docs/14/progress-reporting.html#COPY-PROGRESS-REPORTING\" title=\"28.4.6. COPY Progress Reporting\"><code class=\"structname\">pg_stat_progress_copy</code></a> to report <code class=\"command\">COPY</code> progress (Josef Šimánek, Matthias van de Meent)</p>"
  ],
  [
    "<p>Add system view <a class=\"link\" href=\"https://www.postgresql.org/docs/14/monitoring-stats.html#MONITORING-PG-STAT-WAL-VIEW\" title=\"28.2.12. pg_stat_wal\"><code class=\"structname\">pg_stat_wal</code></a> to report <acronym class=\"acronym\">WAL</acronym> activity (Masahiro Ikeda)</p>"
  ],
  [
    "<p>Add system view <a class=\"link\" href=\"https://www.postgresql.org/docs/14/monitoring-stats.html#MONITORING-PG-STAT-REPLICATION-SLOTS-VIEW\" title=\"28.2.5. pg_stat_replication_slots\"><code class=\"structname\">pg_stat_replication_slots</code></a> to report replication slot activity (Sawada Masahiko, Amit Kapila, Vignesh C)</p>",
    "<p>The function <a class=\"link\" href=\"https://www.postgresql.org/docs/14/monitoring-stats.html#MONITORING-STATS-FUNCTIONS\" title=\"28.2.22. Statistics Functions\"><code class=\"function\">pg_stat_reset_replication_slot()</code></a> resets slot statistics.</p>"
  ],
  [
    "<p>Add system view <a class=\"link\" href=\"https://www.postgresql.org/docs/14/view-pg-backend-memory-contexts.html\" title=\"52.67. pg_backend_memory_contexts\"><code class=\"structname\">pg_backend_memory_contexts</code></a> to report session memory usage (Atsushi Torikoshi, Fujii Masao)</p>"
  ],
  [
    "<p>Add function <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-admin.html#FUNCTIONS-ADMIN-SIGNAL\" title=\"9.27.2. Server Signaling Functions\"><code class=\"function\">pg_log_backend_memory_contexts()</code></a> to output the memory contexts of arbitrary backends (Atsushi Torikoshi)</p>"
  ],
  [
    "<p>Add session statistics to the <a class=\"link\" href=\"https://www.postgresql.org/docs/14/monitoring-stats.html#MONITORING-PG-STAT-DATABASE-VIEW\" title=\"28.2.13. pg_stat_database\"><code class=\"structname\">pg_stat_database</code></a> system view (Laurenz Albe)</p>"
  ],
  [
    "<p>Add columns to <a class=\"link\" href=\"https://www.postgresql.org/docs/14/view-pg-prepared-statements.html\" title=\"52.77. pg_prepared_statements\"><code class=\"structname\">pg_prepared_statements</code></a> to report generic and custom plan counts (Atsushi Torikoshi, Kyotaro Horiguchi)</p>"
  ],
  [
    "<p>Add lock wait start time to <a class=\"link\" href=\"https://www.postgresql.org/docs/14/view-pg-locks.html\" title=\"52.74. pg_locks\"><code class=\"structname\">pg_locks</code></a> (Atsushi Torikoshi)</p>"
  ],
  [
    "<p>Make the archiver process visible in <code class=\"structname\">pg_stat_activity</code> (Kyotaro Horiguchi)</p>"
  ],
  [
    "<p>Add wait event <a class=\"link\" href=\"https://www.postgresql.org/docs/14/monitoring-stats.html#MONITORING-PG-STAT-ACTIVITY-VIEW\" title=\"28.2.3. pg_stat_activity\"><code class=\"literal\">WalReceiverExit</code></a> to report <acronym class=\"acronym\">WAL</acronym> receiver exit wait time (Fujii Masao)</p>"
  ],
  [
    "<p>Implement information schema view <a class=\"link\" href=\"https://www.postgresql.org/docs/14/infoschema-routine-column-usage.html\" title=\"37.40. routine_column_usage\"><code class=\"structname\">routine_column_usage</code></a> to track columns referenced by function and procedure default expressions (Peter Eisentraut)</p>"
  ],
  [
    "<p>Allow an SSL certificate's distinguished name (<acronym class=\"acronym\">DN</acronym>) to be matched for client certificate authentication (Andrew Dunstan)</p>",
    "<p>The new <a class=\"link\" href=\"https://www.postgresql.org/docs/14/auth-pg-hba-conf.html\" title=\"21.1. The pg_hba.conf File\"><code class=\"filename\">pg_hba.conf</code></a> option <code class=\"literal\">clientname=DN</code> allows comparison with certificate attributes beyond the <code class=\"literal\">CN</code> and can be combined with ident maps.</p>"
  ],
  [
    "<p>Allow <code class=\"filename\">pg_hba.conf</code> and <a class=\"link\" href=\"https://www.postgresql.org/docs/14/auth-username-maps.html\" title=\"21.2. User Name Maps\"><code class=\"filename\">pg_ident.conf</code></a> records to span multiple lines (Fabien Coelho)</p>",
    "<p>A backslash at the end of a line allows record contents to be continued on the next line.</p>"
  ],
  [
    "<p>Allow the specification of a certificate revocation list (<acronym class=\"acronym\">CRL</acronym>) directory (Kyotaro Horiguchi)</p>",
    "<p>This is controlled by server parameter <a class=\"xref\" href=\"https://www.postgresql.org/docs/14/runtime-config-connection.html#GUC-SSL-CRL-DIR\">ssl_crl_dir</a> and libpq connection option <a class=\"xref\" href=\"https://www.postgresql.org/docs/14/libpq-connect.html#LIBPQ-CONNECT-SSLCRLDIR\">sslcrldir</a>. Previously only single <acronym class=\"acronym\">CRL</acronym> files could be specified.</p>"
  ],
  [
    "<p>Allow passwords of an arbitrary length (Tom Lane, Nathan Bossart)</p>"
  ],
  [
    "<p>Add server parameter <a class=\"xref\" href=\"https://www.postgresql.org/docs/14/runtime-config-client.html#GUC-IDLE-SESSION-TIMEOUT\">idle_session_timeout</a> to close idle sessions (Li Japin)</p>",
    "<p>This is similar to <a class=\"xref\" href=\"https://www.postgresql.org/docs/14/runtime-config-client.html#GUC-IDLE-IN-TRANSACTION-SESSION-TIMEOUT\">idle_in_transaction_session_timeout</a>.</p>"
  ],
  [
    "<p>Change <a class=\"xref\" href=\"https://www.postgresql.org/docs/14/runtime-config-wal.html#GUC-CHECKPOINT-COMPLETION-TARGET\">checkpoint_completion_target</a> default to 0.9 (Stephen Frost)</p>",
    "<p>The previous default was 0.5.</p>"
  ],
  [
    "<p>Allow <code class=\"literal\">%P</code> in <a class=\"xref\" href=\"https://www.postgresql.org/docs/14/runtime-config-logging.html#GUC-LOG-LINE-PREFIX\">log_line_prefix</a> to report the parallel group leader's PID for a parallel worker (Justin Pryzby)</p>"
  ],
  [
    "<p>Allow <a class=\"xref\" href=\"https://www.postgresql.org/docs/14/runtime-config-connection.html#GUC-UNIX-SOCKET-DIRECTORIES\">unix_socket_directories</a> to specify paths as individual, comma-separated quoted strings (Ian Lawrence Barwick)</p>",
    "<p>Previously all the paths had to be in a single quoted string.</p>"
  ],
  [
    "<p>Allow startup allocation of dynamic shared memory (Thomas Munro)</p>",
    "<p>This is controlled by <a class=\"xref\" href=\"https://www.postgresql.org/docs/14/runtime-config-resource.html#GUC-MIN-DYNAMIC-SHARED-MEMORY\">min_dynamic_shared_memory</a>. This allows more use of huge pages.</p>"
  ],
  [
    "<p>Add server parameter <a class=\"xref\" href=\"https://www.postgresql.org/docs/14/runtime-config-resource.html#GUC-HUGE-PAGE-SIZE\">huge_page_size</a> to control the size of huge pages used on Linux (Odin Ugedal)</p>"
  ],
  [
    "<p>Allow standby servers to be rewound via <a class=\"link\" href=\"https://www.postgresql.org/docs/14/app-pgrewind.html\" title=\"pg_rewind\"><span class=\"application\">pg_rewind</span></a> (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Allow the <a class=\"xref\" href=\"https://www.postgresql.org/docs/14/runtime-config-wal.html#GUC-RESTORE-COMMAND\">restore_command</a> setting to be changed during a server reload (Sergei Kornilov)</p>",
    "<p>You can also set <code class=\"varname\">restore_command</code> to an empty string and reload to force recovery to only read from the <a class=\"link\" href=\"https://www.postgresql.org/docs/14/storage-file-layout.html\" title=\"70.1. Database File Layout\"><code class=\"filename\">pg_wal</code></a> directory.</p>"
  ],
  [
    "<p>Add server parameter <a class=\"xref\" href=\"https://www.postgresql.org/docs/14/runtime-config-logging.html#GUC-LOG-RECOVERY-CONFLICT-WAITS\">log_recovery_conflict_waits</a> to report long recovery conflict wait times (Bertrand Drouvot, Masahiko Sawada)</p>"
  ],
  [
    "<p>Pause recovery on a hot standby server if the primary changes its parameters in a way that prevents replay on the standby (Peter Eisentraut)</p>",
    "<p>Previously the standby would shut down immediately.</p>"
  ],
  [
    "<p>Add function <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-admin.html#FUNCTIONS-RECOVERY-CONTROL\" title=\"9.27.4. Recovery Control Functions\"><code class=\"function\">pg_get_wal_replay_pause_state()</code></a> to report the recovery state (Dilip Kumar)</p>",
    "<p>It gives more detailed information than <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-admin.html#FUNCTIONS-RECOVERY-CONTROL\" title=\"9.27.4. Recovery Control Functions\"><code class=\"function\">pg_is_wal_replay_paused()</code></a>, which still exists.</p>"
  ],
  [
    "<p>Add new read-only server parameter <a class=\"xref\" href=\"https://www.postgresql.org/docs/14/runtime-config-preset.html#GUC-IN-HOT-STANDBY\">in_hot_standby</a> (Haribabu Kommi, Greg Nancarrow, Tom Lane)</p>",
    "<p>This allows clients to easily detect whether they are connected to a hot standby server.</p>"
  ],
  [
    "<p>Speed truncation of small tables during recovery on clusters with a large number of shared buffers (Kirk Jamison)</p>"
  ],
  [
    "<p>Allow file system sync at the start of crash recovery on Linux (Thomas Munro)</p>",
    "<p>By default, <span class=\"productname\">PostgreSQL</span> opens and fsyncs each data file in the database cluster at the start of crash recovery. A new setting, <a class=\"xref\" href=\"https://www.postgresql.org/docs/14/runtime-config-error-handling.html#GUC-RECOVERY-INIT-SYNC-METHOD\">recovery_init_sync_method</a><code class=\"literal\">=syncfs</code>, instead syncs each filesystem used by the cluster. This allows for faster recovery on systems with many database files.</p>"
  ],
  [
    "<p>Add function <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-info.html\" title=\"9.26. System Information Functions and Operators\"><code class=\"function\">pg_xact_commit_timestamp_origin()</code></a> to return the commit timestamp and replication origin of the specified transaction (Movead Li)</p>"
  ],
  [
    "<p>Add the replication origin to the record returned by <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-info.html\" title=\"9.26. System Information Functions and Operators\"><code class=\"function\">pg_last_committed_xact()</code></a> (Movead Li)</p>"
  ],
  [
    "<p>Allow replication <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-admin.html#FUNCTIONS-REPLICATION\" title=\"9.27.6. Replication Management Functions\">origin functions</a> to be controlled using standard function permission controls (Martín Marqués)</p>",
    "<p>Previously these functions could only be executed by superusers, and this is still the default.</p>"
  ],
  [
    "<p>Allow logical replication to stream long in-progress transactions to subscribers (Dilip Kumar, Amit Kapila, Ajin Cherian, Tomas Vondra, Nikhil Sontakke, Stas Kelvich)</p>",
    "<p>Previously transactions that exceeded <a class=\"xref\" href=\"https://www.postgresql.org/docs/14/runtime-config-resource.html#GUC-LOGICAL-DECODING-WORK-MEM\">logical_decoding_work_mem</a> were written to disk until the transaction completed.</p>"
  ],
  [
    "<p>Enhance the logical replication <acronym class=\"acronym\">API</acronym> to allow streaming large in-progress transactions (Tomas Vondra, Dilip Kumar, Amit Kapila)</p>",
    "<p>The output functions begin with <a class=\"link\" href=\"https://www.postgresql.org/docs/14/logicaldecoding-output-plugin.html#LOGICALDECODING-OUTPUT-PLUGIN-STREAM-START\" title=\"49.6.4.14. Stream Start Callback\"><code class=\"literal\">stream</code></a>. <span class=\"application\">test_decoding</span> also supports these.</p>"
  ],
  [
    "<p>Allow multiple transactions during table sync in logical replication (Peter Smith, Amit Kapila, and Takamichi Osumi)</p>"
  ],
  [
    "<p>Immediately <acronym class=\"acronym\">WAL</acronym>-log subtransaction and top-level <code class=\"type\">XID</code> association (Tomas Vondra, Dilip Kumar, Amit Kapila)</p>",
    "<p>This is useful for logical decoding.</p>"
  ],
  [
    "<p>Enhance logical decoding APIs to handle two-phase commits (Ajin Cherian, Amit Kapila, Nikhil Sontakke, Stas Kelvich)</p>",
    "<p>This is controlled via <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-admin.html#FUNCTIONS-REPLICATION\" title=\"9.27.6. Replication Management Functions\"><code class=\"function\">pg_create_logical_replication_slot()</code></a>.</p>"
  ],
  [
    "<p>Generate <acronym class=\"acronym\">WAL</acronym> invalidation messages during command completion when using logical replication (Dilip Kumar, Tomas Vondra, Amit Kapila)</p>",
    "<p>When logical replication is disabled, <acronym class=\"acronym\">WAL</acronym> invalidation messages are generated at transaction completion. This allows logical streaming of in-progress transactions.</p>"
  ],
  [
    "<p>Allow logical decoding to more efficiently process cache invalidation messages (Dilip Kumar)</p>",
    "<p>This allows <a class=\"link\" href=\"https://www.postgresql.org/docs/14/logicaldecoding.html\" title=\"Chapter 49. Logical Decoding\">logical decoding</a> to work efficiently in presence of a large amount of <acronym class=\"acronym\">DDL</acronym>.</p>"
  ],
  [
    "<p>Allow control over whether logical decoding messages are sent to the replication stream (David Pirotte, Euler Taveira)</p>"
  ],
  [
    "<p>Allow logical replication subscriptions to use binary transfer mode (Dave Cramer)</p>",
    "<p>This is faster than text mode, but slightly less robust.</p>"
  ],
  [
    "<p>Allow logical decoding to be filtered by xid (Markus Wanner)</p>"
  ],
  [
    "<p>Reduce the number of keywords that can't be used as column labels without <code class=\"literal\">AS</code> (Mark Dilger)</p>",
    "<p>There are now 90% fewer restricted keywords.</p>"
  ],
  [
    "<p>Allow an alias to be specified for <code class=\"literal\">JOIN</code>'s <code class=\"literal\">USING</code> clause (Peter Eisentraut)</p>",
    "<p>The alias is created by writing <code class=\"literal\">AS</code> after the <code class=\"literal\">USING</code> clause. It can be used as a table qualification for the merged <code class=\"literal\">USING</code> columns.</p>"
  ],
  [
    "<p>Allow <code class=\"literal\">DISTINCT</code> to be added to <code class=\"literal\">GROUP BY</code> to remove duplicate <code class=\"literal\">GROUPING SET</code> combinations (Vik Fearing)</p>",
    "<p>For example, <code class=\"literal\">GROUP BY CUBE (a,b), CUBE (b,c)</code> will generate duplicate grouping combinations without <code class=\"literal\">DISTINCT</code>.</p>"
  ],
  [
    "<p>Properly handle <code class=\"literal\">DEFAULT</code> entries in multi-row <code class=\"literal\">VALUES</code> lists in <code class=\"command\">INSERT</code> (Dean Rasheed)</p>",
    "<p>Such cases used to throw an error.</p>"
  ],
  [
    "<p>Add <acronym class=\"acronym\">SQL</acronym>-standard <code class=\"literal\">SEARCH</code> and <code class=\"literal\">CYCLE</code> clauses for <a class=\"link\" href=\"https://www.postgresql.org/docs/14/queries-with.html\" title=\"7.8. WITH Queries (Common Table Expressions)\">common table expressions</a> (Peter Eisentraut)</p>",
    "<p>The same results could be accomplished using existing syntax, but much less conveniently.</p>"
  ],
  [
    "<p>Allow column names in the <code class=\"literal\">WHERE</code> clause of <code class=\"literal\">ON CONFLICT</code> to be table-qualified (Tom Lane)</p>",
    "<p>Only the target table can be referenced, however.</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/14/sql-refreshmaterializedview.html\" title=\"REFRESH MATERIALIZED VIEW\"><code class=\"command\">REFRESH MATERIALIZED VIEW</code></a> to use parallelism (Bharath Rupireddy)</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/14/sql-reindex.html\" title=\"REINDEX\"><code class=\"command\">REINDEX</code></a> to change the tablespace of the new index (Alexey Kondratov, Michael Paquier, Justin Pryzby)</p>",
    "<p>This is done by specifying a <code class=\"literal\">TABLESPACE</code> clause. A <code class=\"option\">--tablespace</code> option was also added to <a class=\"link\" href=\"https://www.postgresql.org/docs/14/app-reindexdb.html\" title=\"reindexdb\"><span class=\"application\">reindexdb</span></a> to control this.</p>"
  ],
  [
    "<p>Allow <code class=\"command\">REINDEX</code> to process all child tables or indexes of a partitioned relation (Justin Pryzby, Michael Paquier)</p>"
  ],
  [
    "<p>Allow index commands using <code class=\"command\">CONCURRENTLY</code> to avoid waiting for the completion of other operations using <code class=\"command\">CONCURRENTLY</code> (Álvaro Herrera)</p>"
  ],
  [
    "<p>Improve the performance of <a class=\"link\" href=\"https://www.postgresql.org/docs/14/sql-copy.html\" title=\"COPY\"><code class=\"command\">COPY FROM</code></a> in binary mode (Bharath Rupireddy, Amit Langote)</p>"
  ],
  [
    "<p>Preserve <acronym class=\"acronym\">SQL</acronym> standard syntax for SQL-defined functions in <a class=\"link\" href=\"https://www.postgresql.org/docs/14/sql-createview.html\" title=\"CREATE VIEW\">view definitions</a> (Tom Lane)</p>",
    "<p>Previously, calls to SQL-standard functions such as <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-datetime.html#FUNCTIONS-DATETIME-EXTRACT\" title=\"9.9.1. EXTRACT, date_part\"><code class=\"function\">EXTRACT()</code></a> were shown in plain function-call syntax. The original syntax is now preserved when displaying a view or rule.</p>"
  ],
  [
    "<p>Add the <acronym class=\"acronym\">SQL</acronym>-standard clause <code class=\"literal\">GRANTED BY</code> to <a class=\"link\" href=\"https://www.postgresql.org/docs/14/sql-grant.html\" title=\"GRANT\"><code class=\"command\">GRANT</code></a> and <a class=\"link\" href=\"https://www.postgresql.org/docs/14/sql-revoke.html\" title=\"REVOKE\"><code class=\"command\">REVOKE</code></a> (Peter Eisentraut)</p>"
  ],
  [
    "<p>Add <code class=\"literal\">OR REPLACE</code> option for <a class=\"link\" href=\"https://www.postgresql.org/docs/14/sql-createtrigger.html\" title=\"CREATE TRIGGER\"><code class=\"command\">CREATE TRIGGER</code></a> (Takamichi Osumi)</p>",
    "<p>This allows pre-existing triggers to be conditionally replaced.</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/14/sql-truncate.html\" title=\"TRUNCATE\"><code class=\"command\">TRUNCATE</code></a> to operate on foreign tables (Kazutaka Onishi, Kohei KaiGai)</p>",
    "<p>The <a class=\"link\" href=\"https://www.postgresql.org/docs/14/postgres-fdw.html\" title=\"F.35. postgres_fdw\"><span class=\"application\">postgres_fdw</span></a> module also now supports this.</p>"
  ],
  [
    "<p>Allow publications to be more easily added to and removed from a subscription (Japin Li)</p>",
    "<p>The new syntax is <a class=\"link\" href=\"https://www.postgresql.org/docs/14/sql-altersubscription.html\" title=\"ALTER SUBSCRIPTION\"><code class=\"command\">ALTER SUBSCRIPTION ... ADD/DROP PUBLICATION</code></a>. This avoids having to specify all publications to add/remove entries.</p>"
  ],
  [
    "<p>Add primary keys, unique constraints, and foreign keys to <a class=\"link\" href=\"https://www.postgresql.org/docs/14/catalogs.html\" title=\"Chapter 52. System Catalogs\">system catalogs</a> (Peter Eisentraut)</p>",
    "<p>These changes help <acronym class=\"acronym\">GUI</acronym> tools analyze the system catalogs. The existing unique indexes of catalogs now have associated <code class=\"literal\">UNIQUE</code> or <code class=\"literal\">PRIMARY KEY</code> constraints. Foreign key relationships are not actually stored or implemented as constraints, but can be obtained for display from the function <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-info.html#FUNCTIONS-INFO-CATALOG-TABLE\" title=\"Table 9.70. System Catalog Information Functions\">pg_get_catalog_foreign_keys()</a>.</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-info.html\" title=\"9.26. System Information Functions and Operators\"><code class=\"literal\">CURRENT_ROLE</code></a> every place <code class=\"literal\">CURRENT_USER</code> is accepted (Peter Eisentraut)</p>"
  ],
  [
    "<p>Allow extensions and built-in data types to implement <a class=\"link\" href=\"https://www.postgresql.org/docs/14/sql-altertype.html\" title=\"ALTER TYPE\">subscripting</a> (Dmitry Dolgov)</p>",
    "<p>Previously subscript handling was hard-coded into the server, so that subscripting could only be applied to array types. This change allows subscript notation to be used to extract or assign portions of a value of any type for which the concept makes sense.</p>"
  ],
  [
    "<p>Allow subscripting of <a class=\"link\" href=\"https://www.postgresql.org/docs/14/datatype-json.html\" title=\"8.14. JSON Types\"><code class=\"type\">JSONB</code></a> (Dmitry Dolgov)</p>",
    "<p><code class=\"type\">JSONB</code> subscripting can be used to extract and assign to portions of <code class=\"type\">JSONB</code> documents.</p>"
  ],
  [
    "<p>Add support for <a class=\"link\" href=\"https://www.postgresql.org/docs/14/rangetypes.html\" title=\"8.17. Range Types\">multirange data types</a> (Paul Jungwirth, Alexander Korotkov)</p>",
    "<p>These are like range data types, but they allow the specification of multiple, ordered, non-overlapping ranges. An associated multirange type is automatically created for every range type.</p>"
  ],
  [
    "<p>Add support for the <a class=\"link\" href=\"https://www.postgresql.org/docs/14/textsearch-dictionaries.html#TEXTSEARCH-SNOWBALL-DICTIONARY\" title=\"12.6.6. Snowball Dictionary\">stemming</a> of languages Armenian, Basque, Catalan, Hindi, Serbian, and Yiddish (Peter Eisentraut)</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/14/textsearch-intro.html#TEXTSEARCH-INTRO-CONFIGURATIONS\" title=\"12.1.3. Configurations\">tsearch data files</a> to have unlimited line lengths (Tom Lane)</p>",
    "<p>The previous limit was 4K bytes. Also remove function <code class=\"function\">t_readline()</code>.</p>"
  ],
  [
    "<p>Add support for <code class=\"literal\">Infinity</code> and <code class=\"literal\">-Infinity</code> values in the <a class=\"link\" href=\"https://www.postgresql.org/docs/14/datatype-numeric.html\" title=\"8.1. Numeric Types\">numeric data type</a> (Tom Lane)</p>",
    "<p>Floating-point data types already supported these.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-geometry.html\" title=\"9.11. Geometric Functions and Operators\">point operators</a> <code class=\"literal\">&lt;&lt;|</code> and <code class=\"literal\">|&gt;&gt;</code> representing strictly above/below tests (Emre Hasegeli)</p>",
    "<p>Previously these were called <code class=\"literal\">&gt;^</code> and <code class=\"literal\">&lt;^</code>, but that naming is inconsistent with other geometric data types. The old names remain available, but may someday be removed.</p>"
  ],
  [
    "<p>Add operators to add and subtract <a class=\"link\" href=\"https://www.postgresql.org/docs/14/datatype-pg-lsn.html\" title=\"8.20. pg_lsn Type\"><code class=\"type\">LSN</code></a> and numeric (byte) values (Fujii Masao)</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/14/protocol-overview.html#PROTOCOL-FORMAT-CODES\" title=\"53.1.3. Formats and Format Codes\">binary data transfer</a> to be more forgiving of array and record <code class=\"type\">OID</code> mismatches (Tom Lane)</p>"
  ],
  [
    "<p>Create composite array types for system catalogs (Wenjing Zeng)</p>",
    "<p>User-defined relations have long had composite types associated with them, and also array types over those composite types. System catalogs now do as well. This change also fixes an inconsistency that creating a user-defined table in single-user mode would fail to create a composite array type.</p>"
  ],
  [
    "<p>Allow <acronym class=\"acronym\">SQL</acronym>-language <a class=\"link\" href=\"https://www.postgresql.org/docs/14/sql-createfunction.html\" title=\"CREATE FUNCTION\">functions</a> and <a class=\"link\" href=\"https://www.postgresql.org/docs/14/sql-createprocedure.html\" title=\"CREATE PROCEDURE\">procedures</a> to use <acronym class=\"acronym\">SQL</acronym>-standard function bodies (Peter Eisentraut)</p>",
    "<p>Previously only string-literal function bodies were supported. When writing a function or procedure in SQL-standard syntax, the body is parsed immediately and stored as a parse tree. This allows better tracking of function dependencies, and can have security benefits.</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/14/sql-createprocedure.html\" title=\"CREATE PROCEDURE\">procedures</a> to have <code class=\"literal\">OUT</code> parameters (Peter Eisentraut)</p>"
  ],
  [
    "<p>Allow some array functions to operate on a mix of compatible data types (Tom Lane)</p>",
    "<p>The functions <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-array.html\" title=\"9.19. Array Functions and Operators\"><code class=\"function\">array_append()</code></a>, <code class=\"function\">array_prepend()</code>, <code class=\"function\">array_cat()</code>, <code class=\"function\">array_position()</code>, <code class=\"function\">array_positions()</code>, <code class=\"function\">array_remove()</code>, <code class=\"function\">array_replace()</code>, and <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-math.html\" title=\"9.3. Mathematical Functions and Operators\"><code class=\"function\">width_bucket()</code></a> now take <code class=\"type\">anycompatiblearray</code> instead of <code class=\"type\">anyarray</code> arguments. This makes them less fussy about exact matches of argument types.</p>"
  ],
  [
    "<p>Add <acronym class=\"acronym\">SQL</acronym>-standard <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-array.html\" title=\"9.19. Array Functions and Operators\"><code class=\"function\">trim_array()</code></a> function (Vik Fearing)</p>",
    "<p>This could already be done with array slices, but less easily.</p>"
  ],
  [
    "<p>Add <code class=\"type\">bytea</code> equivalents of <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-binarystring.html\" title=\"9.5. Binary String Functions and Operators\"><code class=\"function\">ltrim()</code></a> and <code class=\"function\">rtrim()</code> (Joel Jacobson)</p>"
  ],
  [
    "<p>Support negative indexes in <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-string.html\" title=\"9.4. String Functions and Operators\"><code class=\"function\">split_part()</code></a> (Nikhil Benesch)</p>",
    "<p>Negative values start from the last field and count backward.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-string.html\" title=\"9.4. String Functions and Operators\"><code class=\"function\">string_to_table()</code></a> function to split a string on delimiters (Pavel Stehule)</p>",
    "<p>This is similar to the <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-string.html\" title=\"9.4. String Functions and Operators\"><code class=\"function\">regexp_split_to_table()</code></a> function.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-string.html\" title=\"9.4. String Functions and Operators\"><code class=\"function\">unistr()</code></a> function to allow Unicode characters to be specified as backslash-hex escapes in strings (Pavel Stehule)</p>",
    "<p>This is similar to how Unicode can be specified in literal strings.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-aggregate.html\" title=\"9.21. Aggregate Functions\"><code class=\"function\">bit_xor()</code></a> XOR aggregate function (Alexey Bashtanov)</p>"
  ],
  [
    "<p>Add function <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-binarystring.html\" title=\"9.5. Binary String Functions and Operators\"><code class=\"function\">bit_count()</code></a> to return the number of bits set in a bit or byte string (David Fetter)</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-datetime.html#FUNCTIONS-DATETIME-BIN\" title=\"9.9.3. date_bin\"><code class=\"function\">date_bin()</code></a> function (John Naylor)</p>",
    "<p>This function <span class=\"quote\">“<span class=\"quote\">bins</span>”</span> input timestamps, grouping them into intervals of a uniform length aligned with a specified origin.</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-datetime.html\" title=\"9.9. Date/Time Functions and Operators\"><code class=\"function\">make_timestamp()</code></a>/<code class=\"function\">make_timestamptz()</code> to accept negative years (Peter Eisentraut)</p>",
    "<p>Negative values are interpreted as <code class=\"literal\">BC</code> years.</p>"
  ],
  [
    "<p>Add newer regular expression <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-string.html\" title=\"9.4. String Functions and Operators\"><code class=\"function\">substring()</code></a> syntax (Peter Eisentraut)</p>",
    "<p>The new SQL-standard syntax is <code class=\"literal\">SUBSTRING(text SIMILAR pattern ESCAPE escapechar)</code>. The previous standard syntax was <code class=\"literal\">SUBSTRING(text FROM pattern FOR escapechar)</code>, which is still accepted by <span class=\"productname\">PostgreSQL</span>.</p>"
  ],
  [
    "<p>Allow complemented character class escapes <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-matching.html#POSIX-ESCAPE-SEQUENCES\" title=\"9.7.3.3. Regular Expression Escapes\">\\D</a>, <code class=\"literal\">\\S</code>, and <code class=\"literal\">\\W</code> within regular expression brackets (Tom Lane)</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-matching.html#POSIX-BRACKET-EXPRESSIONS\" title=\"9.7.3.2. Bracket Expressions\"><code class=\"literal\">[[:word:]]</code></a> as a regular expression character class, equivalent to <code class=\"literal\">\\w</code> (Tom Lane)</p>"
  ],
  [
    "<p>Allow more flexible data types for default values of <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-window.html\" title=\"9.22. Window Functions\"><code class=\"function\">lead()</code></a> and <code class=\"function\">lag()</code> window functions (Vik Fearing)</p>"
  ],
  [
    "<p>Make non-zero <a class=\"link\" href=\"https://www.postgresql.org/docs/14/datatype-numeric.html#DATATYPE-FLOAT\" title=\"8.1.3. Floating-Point Types\">floating-point values</a> divided by infinity return zero (Kyotaro Horiguchi)</p>",
    "<p>Previously such operations produced underflow errors.</p>"
  ],
  [
    "<p>Make floating-point division of NaN by zero return NaN (Tom Lane)</p>",
    "<p>Previously this returned an error.</p>"
  ],
  [
    "<p>Cause <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-math.html\" title=\"9.3. Mathematical Functions and Operators\"><code class=\"function\">exp()</code></a> and <code class=\"function\">power()</code> for negative-infinity exponents to return zero (Tom Lane)</p>",
    "<p>Previously they often returned underflow errors.</p>"
  ],
  [
    "<p>Improve the accuracy of geometric computations involving infinity (Tom Lane)</p>"
  ],
  [
    "<p>Mark built-in type coercion functions as leakproof where possible (Tom Lane)</p>",
    "<p>This allows more use of functions that require type conversion in security-sensitive situations.</p>"
  ],
  [
    "<p>Change <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-info.html\" title=\"9.26. System Information Functions and Operators\"><code class=\"function\">pg_describe_object()</code></a>, <code class=\"function\">pg_identify_object()</code>, and <code class=\"function\">pg_identify_object_as_address()</code> to always report helpful error messages for non-existent objects (Michael Paquier)</p>"
  ],
  [
    "<p>Improve PL/pgSQL's <a class=\"link\" href=\"https://www.postgresql.org/docs/14/plpgsql-expressions.html\" title=\"43.4. Expressions\">expression</a> and <a class=\"link\" href=\"https://www.postgresql.org/docs/14/plpgsql-statements.html#PLPGSQL-STATEMENTS-ASSIGNMENT\" title=\"43.5.1. Assignment\">assignment</a> parsing (Tom Lane)</p>",
    "<p>This change allows assignment to array slices and nested record fields.</p>"
  ],
  [
    "<p>Allow plpgsql's <a class=\"link\" href=\"https://www.postgresql.org/docs/14/plpgsql-control-structures.html\" title=\"43.6. Control Structures\"><code class=\"literal\">RETURN QUERY</code></a> to execute its query using parallelism (Tom Lane)</p>"
  ],
  [
    "<p>Improve performance of repeated <a class=\"link\" href=\"https://www.postgresql.org/docs/14/plpgsql-transactions.html\" title=\"43.8. Transaction Management\">CALL</a>s within plpgsql procedures (Pavel Stehule, Tom Lane)</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/14/libpq-pipeline-mode.html#LIBPQ-PIPELINE-SENDING\" title=\"34.5.1.1. Issuing Queries\">pipeline</a> mode to libpq (Craig Ringer, Matthieu Garrigues, Álvaro Herrera)</p>",
    "<p>This allows multiple queries to be sent, only waiting for completion when a specific synchronization message is sent.</p>"
  ],
  [
    "<p>Enhance libpq's <a class=\"link\" href=\"https://www.postgresql.org/docs/14/libpq-connect.html#LIBPQ-PARAMKEYWORDS\" title=\"34.1.2. Parameter Key Words\"><code class=\"option\">target_session_attrs</code></a> parameter options (Haribabu Kommi, Greg Nancarrow, Vignesh C, Tom Lane)</p>",
    "<p>The new options are <code class=\"literal\">read-only</code>, <code class=\"literal\">primary</code>, <code class=\"literal\">standby</code>, and <code class=\"literal\">prefer-standby</code>.</p>"
  ],
  [
    "<p>Improve the output format of libpq's <a class=\"link\" href=\"https://www.postgresql.org/docs/14/libpq-control.html\" title=\"34.11. Control Functions\"><code class=\"function\">PQtrace()</code></a> (Aya Iwata, Álvaro Herrera)</p>"
  ],
  [
    "<p>Allow an ECPG SQL identifier to be linked to a specific connection (Hayato Kuroda)</p>",
    "<p>This is done via <a class=\"link\" href=\"https://www.postgresql.org/docs/14/ecpg-sql-declare-statement.html\" title=\"DECLARE STATEMENT\"><code class=\"literal\">DECLARE ... STATEMENT</code></a>.</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/14/app-vacuumdb.html\" title=\"vacuumdb\"><span class=\"application\">vacuumdb</span></a> to skip index cleanup and truncation (Nathan Bossart)</p>",
    "<p>The options are <code class=\"option\">--no-index-cleanup</code> and <code class=\"option\">--no-truncate</code>.</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/14/app-pgdump.html\" title=\"pg_dump\"><span class=\"application\">pg_dump</span></a> to dump only certain extensions (Guillaume Lelarge)</p>",
    "<p>This is controlled by option <code class=\"option\">--extension</code>.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/14/pgbench.html\" title=\"pgbench\"><span class=\"application\">pgbench</span></a> <code class=\"function\">permute()</code> function to randomly shuffle values (Fabien Coelho, Hironobu Suzuki, Dean Rasheed)</p>"
  ],
  [
    "<p>Include disconnection times in the reconnection overhead measured by <span class=\"application\">pgbench</span> with <code class=\"option\">-C</code> (Yugo Nagata)</p>"
  ],
  [
    "<p>Allow multiple verbose option specifications (<code class=\"option\">-v</code>) to increase the logging verbosity (Tom Lane)</p>",
    "<p>This behavior is supported by <a class=\"link\" href=\"https://www.postgresql.org/docs/14/app-pgdump.html\" title=\"pg_dump\"><span class=\"application\">pg_dump</span></a>, <a class=\"link\" href=\"https://www.postgresql.org/docs/14/app-pg-dumpall.html\" title=\"pg_dumpall\"><span class=\"application\">pg_dumpall</span></a>, and <a class=\"link\" href=\"https://www.postgresql.org/docs/14/app-pgrestore.html\" title=\"pg_restore\"><span class=\"application\">pg_restore</span></a>.</p>"
  ],
  [
    "<p>Allow <span class=\"application\">psql</span>'s <code class=\"literal\">\\df</code> and <code class=\"literal\">\\do</code> commands to specify function and operator argument types (Greg Sabino Mullane, Tom Lane)</p>",
    "<p>This helps reduce the number of matches printed for overloaded names.</p>"
  ],
  [
    "<p>Add an access method column to <span class=\"application\">psql</span>'s <code class=\"literal\">\\d[i|m|t]+</code> output (Georgios Kokolatos)</p>"
  ],
  [
    "<p>Allow <span class=\"application\">psql</span>'s <code class=\"literal\">\\dt</code> and <code class=\"literal\">\\di</code> to show <acronym class=\"acronym\">TOAST</acronym> tables and their indexes (Justin Pryzby)</p>"
  ],
  [
    "<p>Add <span class=\"application\">psql</span> command <code class=\"literal\">\\dX</code> to list extended statistics objects (Tatsuro Yamada)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">psql</span>'s <code class=\"literal\">\\dT</code> to understand array syntax and backend grammar aliases, like <code class=\"literal\">int</code> for <code class=\"literal\">integer</code> (Greg Sabino Mullane, Tom Lane)</p>"
  ],
  [
    "<p>When editing the previous query or a file with <span class=\"application\">psql</span>'s <code class=\"literal\">\\e</code>, or using <code class=\"literal\">\\ef</code> and <code class=\"literal\">\\ev</code>, ignore the results if the editor exits without saving (Laurenz Albe)</p>",
    "<p>Previously, such edits would load the previous query into the query buffer, and typically execute it immediately. This was deemed to be probably not what the user wants.</p>"
  ],
  [
    "<p>Improve tab completion (Vignesh C, Michael Paquier, Justin Pryzby, Georgios Kokolatos, Julien Rouhaud)</p>"
  ],
  [
    "<p>Add command-line utility <a class=\"link\" href=\"https://www.postgresql.org/docs/14/app-pgamcheck.html\" title=\"pg_amcheck\"><span class=\"application\">pg_amcheck</span></a> to simplify running <code class=\"filename\">contrib/amcheck</code> tests on many relations (Mark Dilger)</p>"
  ],
  [
    "<p>Add <code class=\"option\">--no-instructions</code> option to <a class=\"link\" href=\"https://www.postgresql.org/docs/14/app-initdb.html\" title=\"initdb\"><span class=\"application\">initdb</span></a> (Magnus Hagander)</p>",
    "<p>This suppresses the server startup instructions that are normally printed.</p>"
  ],
  [
    "<p>Stop <a class=\"link\" href=\"https://www.postgresql.org/docs/14/pgupgrade.html\" title=\"pg_upgrade\"><span class=\"application\">pg_upgrade</span></a> from creating <code class=\"filename\">analyze_new_cluster</code> script (Magnus Hagander)</p>",
    "<p>Instead, give comparable <a class=\"link\" href=\"https://www.postgresql.org/docs/14/app-vacuumdb.html\" title=\"vacuumdb\"><span class=\"application\">vacuumdb</span></a> instructions.</p>"
  ],
  [
    "<p>Remove support for the <a class=\"link\" href=\"https://www.postgresql.org/docs/14/app-postgres.html\" title=\"postgres\"><span class=\"application\">postmaster</span></a> <code class=\"option\">-o</code> option (Magnus Hagander)</p>",
    "<p>This option was unnecessary since all passed options could already be specified directly.</p>"
  ],
  [
    "<p>Rename \"Default Roles\" to <a class=\"link\" href=\"https://www.postgresql.org/docs/14/predefined-roles.html\" title=\"22.5. Predefined Roles\">\"Predefined Roles\"</a> (Bruce Momjian, Stephen Frost)</p>"
  ],
  [
    "<p>Add documentation for the <a class=\"link\" href=\"https://www.postgresql.org/docs/14/functions-math.html#FUNCTION-FACTORIAL\"><code class=\"function\">factorial()</code></a> function (Peter Eisentraut)</p>",
    "<p>With the removal of the ! operator in this release, <code class=\"function\">factorial()</code> is the only built-in way to compute a factorial.</p>"
  ],
  [
    "<p>Add configure option <a class=\"link\" href=\"https://www.postgresql.org/docs/14/install-procedure.html#CONFIGURE-OPTIONS-FEATURES\" title=\"17.4.1.2. PostgreSQL Features\"><code class=\"literal\">--with-ssl={openssl}</code></a> to allow future choice of the SSL library to use (Daniel Gustafsson, Michael Paquier)</p>",
    "<p>The spelling <code class=\"option\">--with-openssl</code> is kept for compatibility.</p>"
  ],
  [
    "<p>Add support for <a class=\"link\" href=\"https://www.postgresql.org/docs/14/runtime-config-connection.html#GUC-UNIX-SOCKET-DIRECTORIES\">abstract Unix-domain sockets</a> (Peter Eisentraut)</p>",
    "<p>This is currently supported on <span class=\"productname\">Linux</span> and <span class=\"productname\">Windows</span>.</p>"
  ],
  [
    "<p>Allow Windows to properly handle files larger than four gigabytes (Juan José Santamaría Flecha)</p>",
    "<p>For example this allows <a class=\"link\" href=\"https://www.postgresql.org/docs/14/sql-copy.html\" title=\"COPY\"><code class=\"command\">COPY,</code></a> <a class=\"link\" href=\"https://www.postgresql.org/docs/14/install-procedure.html#CONFIGURE-OPTIONS-MISC\" title=\"17.4.1.5. Miscellaneous\"><acronym class=\"acronym\">WAL</acronym></a> files, and relation segment files to be larger than four gigabytes.</p>"
  ],
  [
    "<p>Add server parameter <a class=\"xref\" href=\"https://www.postgresql.org/docs/14/runtime-config-developer.html#GUC-DEBUG-DISCARD-CACHES\">debug_discard_caches</a> to control cache flushing for test purposes (Craig Ringer)</p>",
    "<p>Previously this behavior could only be set at compile time. To invoke it during <span class=\"application\">initdb</span>, use the new option <code class=\"option\">--discard-caches</code>.</p>"
  ],
  [
    "<p>Various improvements in <span class=\"productname\">valgrind</span> error detection ability (Álvaro Herrera, Peter Geoghegan)</p>"
  ],
  [
    "<p>Add a test module for the regular expression package (Tom Lane)</p>"
  ],
  [
    "<p>Add support for <span class=\"productname\">LLVM</span> version 12 (Andres Freund)</p>"
  ],
  [
    "<p>Change SHA1, SHA2, and MD5 hash computations to use the <span class=\"productname\">OpenSSL</span> <acronym class=\"acronym\">EVP API</acronym> (Michael Paquier)</p>",
    "<p>This is more modern and supports <acronym class=\"acronym\">FIPS</acronym> mode.</p>"
  ],
  [
    "<p>Remove separate build-time control over the choice of random number generator (Daniel Gustafsson)</p>",
    "<p>This is now always determined by the choice of SSL library.</p>"
  ],
  [
    "<p>Add direct conversion routines between EUC_TW and Big5 encodings (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Add collation version support for <span class=\"productname\">FreeBSD</span> (Thomas Munro)</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/14/index-api.html\" title=\"62.1. Basic API Structure for Indexes\"><code class=\"structfield\">amadjustmembers</code></a> to the index access method <acronym class=\"acronym\">API</acronym> (Tom Lane)</p>",
    "<p>This allows an index access method to provide validity checking during creation of a new operator class or family.</p>"
  ],
  [
    "<p>Provide feature-test macros in <code class=\"filename\">libpq-fe.h</code> for recently-added <span class=\"application\">libpq</span> features (Tom Lane, Álvaro Herrera)</p>",
    "<p>Historically, applications have usually used compile-time checks of <code class=\"literal\">PG_VERSION_NUM</code> to test whether a feature is available. But that's normally the server version, which might not be a good guide to <span class=\"application\">libpq</span>'s version. <code class=\"filename\">libpq-fe.h</code> now offers <code class=\"literal\">#define</code> symbols denoting application-visible features added in v14; the intent is to keep adding symbols for such features in future versions.</p>"
  ],
  [
    "<p>Allow subscripting of <a class=\"link\" href=\"https://www.postgresql.org/docs/14/hstore.html\" title=\"F.16. hstore\"><span class=\"application\">hstore</span></a> values (Tom Lane, Dmitry Dolgov)</p>"
  ],
  [
    "<p>Allow GiST/GIN <a class=\"link\" href=\"https://www.postgresql.org/docs/14/pgtrgm.html\" title=\"F.33. pg_trgm\"><span class=\"application\">pg_trgm</span></a> indexes to do equality lookups (Julien Rouhaud)</p>",
    "<p>This is similar to <code class=\"literal\">LIKE</code> except no wildcards are honored.</p>"
  ],
  [
    "<p>Allow the <a class=\"link\" href=\"https://www.postgresql.org/docs/14/cube.html\" title=\"F.9. cube\"><span class=\"application\">cube</span></a> data type to be transferred in binary mode (KaiGai Kohei)</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/14/pgstattuple.html\" title=\"F.31. pgstattuple\"><code class=\"function\">pgstattuple_approx()</code></a> to report on <acronym class=\"acronym\">TOAST</acronym> tables (Peter Eisentraut)</p>"
  ],
  [
    "<p>Add contrib module <a class=\"link\" href=\"https://www.postgresql.org/docs/14/pgsurgery.html\" title=\"F.32. pg_surgery\"><span class=\"application\">pg_surgery</span></a> which allows changes to row visibility (Ashutosh Sharma)</p>",
    "<p>This is useful for correcting database corruption.</p>"
  ],
  [
    "<p>Add contrib module <a class=\"link\" href=\"https://www.postgresql.org/docs/14/oldsnapshot.html\" title=\"F.22. old_snapshot\"><span class=\"application\">old_snapshot</span></a> to report the <code class=\"type\">XID</code>/time mapping used by an active <a class=\"xref\" href=\"https://www.postgresql.org/docs/14/runtime-config-resource.html#GUC-OLD-SNAPSHOT-THRESHOLD\">old_snapshot_threshold</a> (Robert Haas)</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/14/amcheck.html\" title=\"F.2. amcheck\"><span class=\"application\">amcheck</span></a> to also check heap pages (Mark Dilger)</p>",
    "<p>Previously it only checked B-Tree index pages.</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/14/pageinspect.html\" title=\"F.23. pageinspect\"><span class=\"application\">pageinspect</span></a> to inspect GiST indexes (Andrey Borodin, Heikki Linnakangas)</p>"
  ],
  [
    "<p>Change <span class=\"application\">pageinspect</span> block numbers to be <a class=\"link\" href=\"https://www.postgresql.org/docs/14/datatype-numeric.html#DATATYPE-INT\" title=\"8.1.1. Integer Types\"><code class=\"type\">bigints</code></a> (Peter Eisentraut)</p>"
  ],
  [
    "<p>Mark <a class=\"link\" href=\"https://www.postgresql.org/docs/14/btree-gist.html\" title=\"F.7. btree_gist\"><span class=\"application\">btree_gist</span></a> functions as parallel safe (Steven Winfield)</p>"
  ],
  [
    "<p>Move query hash computation from <span class=\"application\">pg_stat_statements</span> to the core server (Julien Rouhaud)</p>",
    "<p>The new server parameter <a class=\"xref\" href=\"https://www.postgresql.org/docs/14/runtime-config-statistics.html#GUC-COMPUTE-QUERY-ID\">compute_query_id</a>'s default of <code class=\"literal\">auto</code> will automatically enable query id computation when this extension is loaded.</p>"
  ],
  [
    "<p>Cause <span class=\"application\">pg_stat_statements</span> to track top and nested statements separately (Julien Rohaud)</p>",
    "<p>Previously, when tracking all statements, identical top and nested statements were tracked as a single entry; but it seems more useful to separate such usages.</p>"
  ],
  [
    "<p>Add row counts for utility commands to <span class=\"application\">pg_stat_statements</span> (Fujii Masao, Katsuragi Yuta, Seino Yuki)</p>"
  ],
  [
    "<p>Add <code class=\"structname\">pg_stat_statements_info</code> system view to show <span class=\"application\">pg_stat_statements</span> activity (Katsuragi Yuta, Yuki Seino, Naoki Nakamichi)</p>"
  ],
  [
    "<p>Allow <span class=\"application\">postgres_fdw</span> to <code class=\"command\">INSERT</code> rows in bulk (Takayuki Tsunakawa, Tomas Vondra, Amit Langote)</p>"
  ],
  [
    "<p>Allow <span class=\"application\">postgres_fdw</span> to import table partitions if specified by <a class=\"link\" href=\"https://www.postgresql.org/docs/14/sql-importforeignschema.html\" title=\"IMPORT FOREIGN SCHEMA\"><code class=\"command\">IMPORT FOREIGN SCHEMA ... LIMIT TO</code></a> (Matthias van de Meent)</p>",
    "<p>By default, only the root of a partitioned table is imported.</p>"
  ],
  [
    "<p>Add <span class=\"application\">postgres_fdw</span> function <code class=\"function\">postgres_fdw_get_connections()</code> to report open foreign server connections (Bharath Rupireddy)</p>"
  ],
  [
    "<p>Allow control over whether foreign servers keep connections open after transaction completion (Bharath Rupireddy)</p>",
    "<p>This is controlled by <code class=\"varname\">keep_connections</code> and defaults to on.</p>"
  ],
  [
    "<p>Allow <span class=\"application\">postgres_fdw</span> to reestablish foreign server connections if necessary (Bharath Rupireddy)</p>",
    "<p>Previously foreign server restarts could cause foreign table access errors.</p>"
  ],
  [
    "<p>Add <span class=\"application\">postgres_fdw</span> functions to discard cached connections (Bharath Rupireddy)</p>"
  ]
]