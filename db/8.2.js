[
  [
    "<p>Allow the planner to reorder <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/queries-table-expressions.html#QUERIES-JOIN\" title=\"7.2.1.1.&#xA0;Joined Tables\">outer joins</a> in some\n            circumstances (Tom)</p>",
    "<p>In previous releases, outer joins would always be\n            evaluated in the order written in the query. This\n            change allows the query optimizer to consider\n            reordering outer joins, in cases where it can determine\n            that the join order can be changed without altering the\n            meaning of the query. This can make a considerable\n            performance difference for queries involving multiple\n            outer joins or mixed inner and outer joins.</p>"
  ],
  [
    "<p>Improve efficiency of <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-comparisons.html\" title=\"9.23.&#xA0;Row and Array Comparisons\"><code class=\"literal\">IN</code> (list-of-expressions)</a> clauses\n            (Tom)</p>"
  ],
  [
    "<p>Improve sorting speed and reduce memory usage\n            (Simon, Tom)</p>"
  ],
  [
    "<p>Improve subtransaction performance (Alvaro, Itagaki\n            Takahiro, Tom)</p>"
  ],
  [
    "<p>Add <code class=\"literal\">FILLFACTOR</code> to\n            <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-createtable.html\" title=\"CREATE TABLE\">table</a> and <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-createindex.html\" title=\"CREATE INDEX\">index</a>\n            creation (ITAGAKI Takahiro)</p>",
    "<p>This leaves extra free space in each table or index\n            page, allowing improved performance as the database\n            grows. This is particularly valuable to maintain\n            clustering.</p>"
  ],
  [
    "<p>Increase default values for <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-resource.html#GUC-SHARED-BUFFERS\"><code class=\"varname\">\n            shared_buffers</code></a> and <code class=\"varname\">max_fsm_pages</code> (Andrew)</p>"
  ],
  [
    "<p>Improve locking performance by breaking the lock\n            manager tables into sections (Tom)</p>",
    "<p>This allows locking to be more fine-grained,\n            reducing contention.</p>"
  ],
  [
    "<p>Reduce locking requirements of sequential scans\n            (Qingqing Zhou)</p>"
  ],
  [
    "<p>Reduce locking required for database creation and\n            destruction (Tom)</p>"
  ],
  [
    "<p>Improve the optimizer's selectivity estimates for\n            <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-matching.html#FUNCTIONS-LIKE\" title=\"9.7.1.&#xA0;LIKE\"><code class=\"literal\">LIKE</code></a>, <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-matching.html#FUNCTIONS-LIKE\" title=\"9.7.1.&#xA0;LIKE\"><code class=\"literal\">ILIKE</code></a>, and <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-matching.html#FUNCTIONS-POSIX-REGEXP\" title=\"9.7.3.&#xA0;POSIX Regular Expressions\">regular\n            expression</a> operations (Tom)</p>"
  ],
  [
    "<p>Improve planning of joins to <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/ddl-inherit.html\" title=\"5.9.&#xA0;Inheritance\">inherited tables</a> and\n            <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/queries-union.html\" title=\"7.4.&#xA0;Combining Queries\"><code class=\"literal\">UNION ALL</code></a> views (Tom)</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-query.html#GUC-CONSTRAINT-EXCLUSION\">constraint\n            exclusion</a> to be applied to <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/ddl-inherit.html\" title=\"5.9.&#xA0;Inheritance\">inherited</a> <code class=\"command\">UPDATE</code> and <code class=\"command\">DELETE</code> queries (Tom)</p>",
    "<p><code class=\"command\">SELECT</code> already honored\n            constraint exclusion.</p>"
  ],
  [
    "<p>Improve planning of constant <code class=\"literal\">WHERE</code> clauses, such as a condition\n            that depends only on variables inherited from an outer\n            query level (Tom)</p>"
  ],
  [
    "<p>Protocol-level unnamed prepared statements are\n            re-planned for each set of <code class=\"literal\">BIND</code> values (Tom)</p>",
    "<p>This improves performance because the exact\n            parameter values can be used in the plan.</p>"
  ],
  [
    "<p>Speed up vacuuming of B-Tree indexes (Heikki\n            Linnakangas, Tom)</p>"
  ],
  [
    "<p>Avoid extra scan of tables without indexes during\n            <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-vacuum.html\" title=\"VACUUM\"><code class=\"command\">VACUUM</code></a> (Greg\n            Stark)</p>"
  ],
  [
    "<p>Improve multicolumn <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/gist.html\" title=\"Chapter&#xA0;62.&#xA0;GiST Indexes\"><acronym class=\"acronym\">GiST</acronym></a> indexing (Oleg,\n            Teodor)</p>"
  ],
  [
    "<p>Remove dead index entries before B-Tree page split\n            (Junji Teramoto)</p>"
  ],
  [
    "<p>Allow a forced switch to a new transaction log file\n            (Simon, Tom)</p>",
    "<p>This is valuable for keeping warm standby slave\n            servers in sync with the master. Transaction log file\n            switching now also happens automatically during\n            <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-admin.html\" title=\"9.26.&#xA0;System Administration Functions\"><code class=\"function\">\n            pg_stop_backup()</code></a>. This ensures that all\n            transaction log files needed for recovery can be\n            archived immediately.</p>"
  ],
  [
    "<p>Add <acronym class=\"acronym\">WAL</acronym>\n            informational functions (Simon)</p>",
    "<p>Add functions for interrogating the current\n            transaction log insertion point and determining\n            <acronym class=\"acronym\">WAL</acronym> filenames from\n            the hex <acronym class=\"acronym\">WAL</acronym>\n            locations displayed by <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-admin.html\" title=\"9.26.&#xA0;System Administration Functions\"><code class=\"function\">\n            pg_stop_backup()</code></a> and related functions.</p>"
  ],
  [
    "<p>Improve recovery from a crash during <acronym class=\"acronym\">WAL</acronym> replay (Simon)</p>",
    "<p>The server now does periodic checkpoints during\n            <acronym class=\"acronym\">WAL</acronym> recovery, so if\n            there is a crash, future <acronym class=\"acronym\">WAL</acronym> recovery is shortened. This\n            also eliminates the need for warm standby servers to\n            replay the entire log since the base backup if they\n            crash.</p>"
  ],
  [
    "<p>Improve reliability of long-term <acronym class=\"acronym\">WAL</acronym> replay (Heikki, Simon, Tom)</p>",
    "<p>Formerly, trying to roll forward through more than 2\n            billion transactions would not work due to XID\n            wraparound. This meant warm standby servers had to be\n            reloaded from fresh base backups periodically.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-wal.html#GUC-ARCHIVE-TIMEOUT\"><code class=\"varname\">\n            archive_timeout</code></a> to force transaction log\n            file switches at a given interval (Simon)</p>",
    "<p>This enforces a maximum replication delay for warm\n            standby servers.</p>"
  ],
  [
    "<p>Add native <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/auth-methods.html#AUTH-LDAP\" title=\"20.3.7.&#xA0;LDAP Authentication\"><acronym class=\"acronym\">LDAP</acronym></a> authentication (Magnus\n            Hagander)</p>",
    "<p>This is particularly useful for platforms that do\n            not support <acronym class=\"acronym\">PAM</acronym>,\n            such as Windows.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-grant.html#SQL-GRANT-DESCRIPTION-OBJECTS\" title=\"GRANT on Database Objects\"><code class=\"literal\">GRANT\n            CONNECT ON DATABASE</code></a> (Gevik Babakhani)</p>",
    "<p>This gives SQL-level control over database access.\n            It works as an additional filter on top of the existing\n            <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/auth-pg-hba-conf.html\" title=\"20.1.&#xA0;The pg_hba.conf File\"><code class=\"filename\">pg_hba.conf</code></a> controls.</p>"
  ],
  [
    "<p>Add support for <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/ssl-tcp.html\" title=\"18.9.&#xA0;Secure TCP/IP Connections with SSL\"><acronym class=\"acronym\">\n            SSL</acronym> Certificate Revocation List</a>\n            (<acronym class=\"acronym\">CRL</acronym>) files (Libor\n            Hoho&#x161;)</p>",
    "<p>The server and <span class=\"application\">libpq</span> both recognize\n            <acronym class=\"acronym\">CRL</acronym> files now.</p>"
  ],
  [
    "<p><a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/gist.html\" title=\"Chapter&#xA0;62.&#xA0;GiST Indexes\"><acronym class=\"acronym\">GiST</acronym></a> indexes are now\n            clusterable (Teodor)</p>"
  ],
  [
    "<p>Remove routine autovacuum server log entries\n            (Bruce)</p>",
    "<p><a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/monitoring-stats.html#MONITORING-STATS-VIEWS-TABLE\" title=\"Table&#xA0;28.2.&#xA0;Collected Statistics Views\"><code class=\"literal\">\n            pg_stat_activity</code></a> now shows autovacuum\n            activity.</p>"
  ],
  [
    "<p>Track maximum XID age within individual tables,\n            instead of whole databases (Alvaro)</p>",
    "<p>This reduces the overhead involved in preventing\n            transaction ID wraparound, by avoiding unnecessary\n            VACUUMs.</p>"
  ],
  [
    "<p>Add last vacuum and analyze timestamp columns to the\n            stats collector (Larry Rosenman)</p>",
    "<p>These values now appear in the <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/monitoring-stats.html#MONITORING-STATS-VIEWS-TABLE\" title=\"Table&#xA0;28.2.&#xA0;Collected Statistics Views\"><code class=\"literal\">\n            pg_stat_*_tables</code></a> system views.</p>"
  ],
  [
    "<p>Improve performance of statistics monitoring,\n            especially <code class=\"varname\">stats_command_string</code> (Tom, Bruce)</p>",
    "<p>This release enables <code class=\"varname\">stats_command_string</code> by default, now\n            that its overhead is minimal. This means <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/monitoring-stats.html#MONITORING-STATS-VIEWS-TABLE\" title=\"Table&#xA0;28.2.&#xA0;Collected Statistics Views\"><code class=\"literal\">\n            pg_stat_activity</code></a> will now show all active\n            queries by default.</p>"
  ],
  [
    "<p>Add a <code class=\"literal\">waiting</code> column to\n            <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/monitoring-stats.html#MONITORING-STATS-VIEWS-TABLE\" title=\"Table&#xA0;28.2.&#xA0;Collected Statistics Views\"><code class=\"literal\">\n            pg_stat_activity</code></a> (Tom)</p>",
    "<p>This allows <code class=\"structname\">pg_stat_activity</code> to show all the\n            information included in the <span class=\"application\">ps</span> display.</p>"
  ],
  [
    "<p>Add configuration parameter <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-logging.html#GUC-UPDATE-PROCESS-TITLE\"><code class=\"varname\">\n            update_process_title</code></a> to control whether the\n            <span class=\"application\">ps</span> display is updated\n            for every command (Bruce)</p>",
    "<p>On platforms where it is expensive to update the\n            <span class=\"application\">ps</span> display, it might\n            be worthwhile to turn this off and rely solely on\n            <code class=\"structname\">pg_stat_activity</code> for\n            status information.</p>"
  ],
  [
    "<p>Allow units to be specified in configuration\n            settings (Peter)</p>",
    "<p>For example, you can now set <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-resource.html#GUC-SHARED-BUFFERS\"><code class=\"varname\">\n            shared_buffers</code></a> to <code class=\"literal\">32MB</code> rather than mentally converting\n            sizes.</p>"
  ],
  [
    "<p>Add support for <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/config-setting.html\" title=\"19.1.&#xA0;Setting Parameters\">include directives</a>\n            in <code class=\"filename\">postgresql.conf</code>\n            (Joachim Wieland)</p>"
  ],
  [
    "<p>Improve logging of protocol-level\n            prepare/bind/execute messages (Bruce, Tom)</p>",
    "<p>Such logging now shows statement names, bind\n            parameter values, and the text of the query being\n            executed. Also, the query text is properly included in\n            logged error messages when enabled by <code class=\"varname\">log_min_error_statement</code>.</p>"
  ],
  [
    "<p>Prevent <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-resource.html#GUC-MAX-STACK-DEPTH\"><code class=\"varname\">\n            max_stack_depth</code></a> from being set to unsafe\n            values</p>",
    "<p>On platforms where we can determine the actual\n            kernel stack depth limit (which is most), make sure\n            that the initial default value of <code class=\"varname\">max_stack_depth</code> is safe, and reject\n            attempts to set it to unsafely large values.</p>"
  ],
  [
    "<p>Enable highlighting of error location in query in\n            more cases (Tom)</p>",
    "<p>The server is now able to report a specific error\n            location for some semantic errors (such as unrecognized\n            column name), rather than just for basic syntax errors\n            as before.</p>"
  ],
  [
    "<p>Fix <span class=\"quote\">&#x201C;<span class=\"quote\">failed\n            to re-find parent key</span>&#x201D;</span> errors in\n            <code class=\"command\">VACUUM</code> (Tom)</p>"
  ],
  [
    "<p>Clean out <code class=\"filename\">pg_internal.init</code> cache files during\n            server restart (Simon)</p>",
    "<p>This avoids a hazard that the cache files might\n            contain stale data after PITR recovery.</p>"
  ],
  [
    "<p>Fix race condition for truncation of a large\n            relation across a gigabyte boundary by <code class=\"command\">VACUUM</code> (Tom)</p>"
  ],
  [
    "<p>Fix bug causing needless deadlock errors on\n            row-level locks (Tom)</p>"
  ],
  [
    "<p>Fix bugs affecting multi-gigabyte hash indexes\n            (Tom)</p>"
  ],
  [
    "<p>Each backend process is now its own process group\n            leader (Tom)</p>",
    "<p>This allows query cancel to abort subprocesses\n            invoked from a backend or archive/recovery process.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-insert.html\" title=\"INSERT\"><code class=\"command\">INSERT</code></a>/<a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-update.html\" title=\"UPDATE\"><code class=\"command\">UPDATE</code></a>/<a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-delete.html\" title=\"DELETE\"><code class=\"command\">DELETE</code></a> <code class=\"literal\">RETURNING</code> (Jonah Harris, Tom)</p>",
    "<p>This allows these commands to return values, such as\n            the computed serial key for a new row. In the\n            <code class=\"command\">UPDATE</code> case, values from\n            the updated version of the row are returned.</p>"
  ],
  [
    "<p>Add support for multiple-row <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/queries-values.html\" title=\"7.7.&#xA0;VALUES Lists\"><code class=\"literal\">VALUES</code></a> clauses, per SQL standard\n            (Joe, Tom)</p>",
    "<p>This allows <code class=\"command\">INSERT</code> to\n            insert multiple rows of constants, or queries to\n            generate result sets using constants. For example,\n            <code class=\"literal\">INSERT ... VALUES (...), (...),\n            ....</code>, and <code class=\"literal\">SELECT * FROM\n            (VALUES (...), (...), ....) AS alias(f1,\n            ...)</code>.</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-update.html\" title=\"UPDATE\"><code class=\"command\">UPDATE</code></a> and\n            <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-delete.html\" title=\"DELETE\"><code class=\"command\">DELETE</code></a> to use\n            an alias for the target table (Atsushi Ogawa)</p>",
    "<p>The SQL standard does not permit an alias in these\n            commands, but many database systems allow one anyway\n            for notational convenience.</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-update.html\" title=\"UPDATE\"><code class=\"command\">UPDATE</code></a> to set\n            multiple columns with a list of values (Susanne\n            Ebrecht)</p>",
    "<p>This is basically a short-hand for assigning the\n            columns and values in pairs. The syntax is <code class=\"literal\">UPDATE tab SET (<em class=\"replaceable\"><code>column</code></em>, ...) =\n            (<em class=\"replaceable\"><code>val</code></em>,\n            ...)</code>.</p>"
  ],
  [
    "<p>Make row comparisons work per standard (Tom)</p>",
    "<p>The forms &lt;, &lt;=, &gt;, &gt;= now compare rows\n            lexicographically, that is, compare the first elements,\n            if equal compare the second elements, and so on.\n            Formerly they expanded to an AND condition across all\n            the elements, which was neither standard nor very\n            useful.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-truncate.html\" title=\"TRUNCATE\"><code class=\"literal\">CASCADE</code></a>\n            option to <code class=\"command\">TRUNCATE</code>\n            (Joachim Wieland)</p>",
    "<p>This causes <code class=\"command\">TRUNCATE</code> to\n            automatically include all tables that reference the\n            specified table(s) via foreign keys. While convenient,\n            this is a dangerous tool &#x2014; use with caution!</p>"
  ],
  [
    "<p>Support <code class=\"literal\">FOR UPDATE</code> and\n            <code class=\"literal\">FOR SHARE</code> in the same\n            <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-insert.html\" title=\"INSERT\"><code class=\"literal\">SELECT</code></a>\n            command (Tom)</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-comparisons.html\" title=\"9.23.&#xA0;Row and Array Comparisons\"><code class=\"literal\">IS NOT DISTINCT FROM</code></a> (Pavel\n            Stehule)</p>",
    "<p>This operator is similar to equality (<code class=\"literal\">=</code>), but evaluates to true when both\n            left and right operands are <code class=\"literal\">NULL</code>, and to false when just one is,\n            rather than yielding <code class=\"literal\">NULL</code>\n            in these cases.</p>"
  ],
  [
    "<p>Improve the length output used by <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/queries-union.html\" title=\"7.4.&#xA0;Combining Queries\"><code class=\"literal\">UNION</code></a>/<code class=\"literal\">INTERSECT</code>/<code class=\"literal\">EXCEPT</code> (Tom)</p>",
    "<p>When all corresponding columns are of the same\n            defined length, that length is used for the result,\n            rather than a generic length.</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-matching.html#FUNCTIONS-LIKE\" title=\"9.7.1.&#xA0;LIKE\"><code class=\"literal\">ILIKE</code></a> to work for multi-byte\n            encodings (Tom)</p>",
    "<p>Internally, <code class=\"literal\">ILIKE</code> now\n            calls <code class=\"function\">lower()</code> and then\n            uses <code class=\"literal\">LIKE</code>. Locale-specific\n            regular expression patterns still do not work in these\n            encodings.</p>"
  ],
  [
    "<p>Enable <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-compatible.html#GUC-STANDARD-CONFORMING-STRINGS\">\n            <code class=\"varname\">standard_conforming_strings</code></a> to be\n            turned <code class=\"literal\">on</code> (Kevin\n            Grittner)</p>",
    "<p>This allows backslash escaping in strings to be\n            disabled, making <span class=\"productname\">PostgreSQL</span> more\n            standards-compliant. The default is <code class=\"literal\">off</code> for backwards compatibility, but\n            future releases will default this to <code class=\"literal\">on</code>.</p>"
  ],
  [
    "<p>Do not flatten subqueries that contain <code class=\"literal\">volatile</code> functions in their target\n            lists (Jaime Casanova)</p>",
    "<p>This prevents surprising behavior due to multiple\n            evaluation of a <code class=\"literal\">volatile</code>\n            function (such as <code class=\"function\">random()</code> or <code class=\"function\">nextval()</code>). It might cause\n            performance degradation in the presence of functions\n            that are unnecessarily marked as <code class=\"literal\">volatile</code>.</p>"
  ],
  [
    "<p>Add system views <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/view-pg-prepared-statements.html\" title=\"51.76.&#xA0;pg_prepared_statements\"><code class=\"literal\">pg_prepared_statements</code></a> and\n            <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/view-pg-cursors.html\" title=\"51.68.&#xA0;pg_cursors\"><code class=\"literal\">pg_cursors</code></a> to show prepared\n            statements and open cursors (Joachim Wieland, Neil)</p>",
    "<p>These are very useful in pooled connection\n            setups.</p>"
  ],
  [
    "<p>Support portal parameters in <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-explain.html\" title=\"EXPLAIN\"><code class=\"command\">EXPLAIN</code></a> and <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-execute.html\" title=\"EXECUTE\"><code class=\"command\">EXECUTE</code></a> (Tom)</p>",
    "<p>This allows, for example, <acronym class=\"acronym\">JDBC</acronym> <code class=\"literal\">?</code>\n            parameters to work in these commands.</p>"
  ],
  [
    "<p>If <acronym class=\"acronym\">SQL</acronym>-level\n            <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-prepare.html\" title=\"PREPARE\"><code class=\"command\">PREPARE</code></a>\n            parameters are unspecified, infer their types from the\n            content of the query (Neil)</p>",
    "<p>Protocol-level <code class=\"command\">PREPARE</code>\n            already did this.</p>"
  ],
  [
    "<p>Allow <code class=\"literal\">LIMIT</code> and\n            <code class=\"literal\">OFFSET</code> to exceed two\n            billion (Dhanaraj M)</p>"
  ],
  [
    "<p>Add <code class=\"literal\">TABLESPACE</code> clause\n            to <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-createtableas.html\" title=\"CREATE TABLE AS\"><code class=\"command\">CREATE TABLE\n            AS</code></a> (Neil)</p>",
    "<p>This allows a tablespace to be specified for the new\n            table.</p>"
  ],
  [
    "<p>Add <code class=\"literal\">ON COMMIT</code> clause to\n            <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-createtableas.html\" title=\"CREATE TABLE AS\"><code class=\"command\">CREATE TABLE\n            AS</code></a> (Neil)</p>",
    "<p>This allows temporary tables to be truncated or\n            dropped on transaction commit. The default behavior is\n            for the table to remain until the session ends.</p>"
  ],
  [
    "<p>Add <code class=\"literal\">INCLUDING\n            CONSTRAINTS</code> to <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-createtable.html\" title=\"CREATE TABLE\"><code class=\"command\">CREATE TABLE\n            LIKE</code></a> (Greg Stark)</p>",
    "<p>This allows easy copying of <code class=\"literal\">CHECK</code> constraints to a new table.</p>"
  ],
  [
    "<p>Allow the creation of placeholder (shell) <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-createtype.html\" title=\"CREATE TYPE\">types</a> (Martijn van Oosterhout)</p>",
    "<p>A shell type declaration creates a type name,\n            without specifying any of the details of the type.\n            Making a shell type is useful because it allows cleaner\n            declaration of the type's input/output functions, which\n            must exist before the type can be defined <span class=\"quote\">&#x201C;<span class=\"quote\">for real</span>&#x201D;</span>.\n            The syntax is <code class=\"command\">CREATE TYPE\n            <em class=\"replaceable\"><code>typename</code></em></code>.</p>"
  ],
  [
    "<p><a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-createaggregate.html\" title=\"CREATE AGGREGATE\">Aggregate functions</a> now\n            support multiple input parameters (Sergey Koposov,\n            Tom)</p>"
  ],
  [
    "<p>Add new aggregate creation <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-createaggregate.html\" title=\"CREATE AGGREGATE\">syntax</a> (Tom)</p>",
    "<p>The new syntax is <code class=\"command\">CREATE\n            AGGREGATE <em class=\"replaceable\"><code>aggname</code></em> (<em class=\"replaceable\"><code>input_type</code></em>) (<em class=\"replaceable\"><code>parameter_list</code></em>)</code>.\n            This more naturally supports the new multi-parameter\n            aggregate functionality. The previous syntax is still\n            supported.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-alterrole.html\" title=\"ALTER ROLE\"><code class=\"command\">ALTER ROLE PASSWORD\n            NULL</code></a> to remove a previously set role\n            password (Peter)</p>"
  ],
  [
    "<p>Add <code class=\"command\">DROP</code> object\n            <code class=\"literal\">IF EXISTS</code> for many object\n            types (Andrew)</p>",
    "<p>This allows <code class=\"command\">DROP</code>\n            operations on non-existent objects without generating\n            an error.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-drop-owned.html\" title=\"DROP OWNED\"><code class=\"literal\">DROP\n            OWNED</code></a> to drop all objects owned by a role\n            (Alvaro)</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-reassign-owned.html\" title=\"REASSIGN OWNED\"><code class=\"literal\">REASSIGN\n            OWNED</code></a> to reassign ownership of all objects\n            owned by a role (Alvaro)</p>",
    "<p>This, and <code class=\"literal\">DROP OWNED</code>\n            above, facilitate dropping roles.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-grant.html\" title=\"GRANT\"><code class=\"command\">GRANT ON\n            SEQUENCE</code></a> syntax (Bruce)</p>",
    "<p>This was added for setting sequence-specific\n            permissions. <code class=\"literal\">GRANT ON\n            TABLE</code> for sequences is still supported for\n            backward compatibility.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-grant.html\" title=\"GRANT\"><code class=\"literal\">USAGE</code></a>\n            permission for sequences that allows only <code class=\"function\">currval()</code> and <code class=\"function\">nextval()</code>, not <code class=\"function\">setval()</code> (Bruce)</p>",
    "<p><code class=\"literal\">USAGE</code> permission allows\n            more fine-grained control over sequence access.\n            Granting <code class=\"literal\">USAGE</code> allows\n            users to increment a sequence, but prevents them from\n            setting the sequence to an arbitrary value using\n            <code class=\"function\">setval()</code>.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-altertable.html\" title=\"ALTER TABLE\"><code class=\"literal\">ALTER TABLE [\n            NO ] INHERIT</code></a> (Greg Stark)</p>",
    "<p>This allows inheritance to be adjusted dynamically,\n            rather than just at table creation and destruction.\n            This is very valuable when using inheritance to\n            implement table partitioning.</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-comment.html\" title=\"COMMENT\">comments</a> on global objects to be stored\n            globally (Kris Jurka)</p>",
    "<p>Previously, comments attached to databases were\n            stored in individual databases, making them\n            ineffective, and there was no provision at all for\n            comments on roles or tablespaces. This change adds a\n            new shared catalog <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/catalog-pg-shdescription.html\" title=\"51.48.&#xA0;pg_shdescription\"><code class=\"structname\">pg_shdescription</code></a> and stores\n            comments on databases, roles, and tablespaces\n            therein.</p>"
  ],
  [
    "<p>Add option to allow indexes to be created without\n            blocking concurrent writes to the table (Greg Stark,\n            Tom)</p>",
    "<p>The new syntax is <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-createindex.html\" title=\"CREATE INDEX\"><code class=\"command\">CREATE INDEX\n            CONCURRENTLY</code></a>. The default behavior is still\n            to block table modification while an index is being\n            created.</p>"
  ],
  [
    "<p>Provide <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-admin.html#FUNCTIONS-ADVISORY-LOCKS\" title=\"9.26.10.&#xA0;Advisory Lock Functions\">advisory\n            locking</a> functionality (Abhijit Menon-Sen, Tom)</p>",
    "<p>This is a new locking API designed to replace what\n            used to be in /contrib/userlock. The userlock code is\n            now on pgfoundry.</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-copy.html\" title=\"COPY\"><code class=\"command\">COPY</code></a> to dump a\n            <code class=\"command\">SELECT</code> query (Zoltan\n            Boszormenyi, Karel Zak)</p>",
    "<p>This allows <code class=\"command\">COPY</code> to\n            dump arbitrary <acronym class=\"acronym\">SQL</acronym>\n            queries. The syntax is <code class=\"literal\">COPY\n            (SELECT ...) TO</code>.</p>"
  ],
  [
    "<p>Make the <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-copy.html\" title=\"COPY\"><code class=\"command\">COPY</code></a> command\n            return a command tag that includes the number of rows\n            copied (Volkan YAZICI)</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-vacuum.html\" title=\"VACUUM\"><code class=\"command\">VACUUM</code></a> to\n            expire rows without being affected by other concurrent\n            <code class=\"command\">VACUUM</code> operations (Hannu\n            Krossing, Alvaro, Tom)</p>"
  ],
  [
    "<p>Make <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/app-initdb.html\" title=\"initdb\"><span class=\"application\">initdb</span></a>\n            detect the operating system locale and set the default\n            <code class=\"varname\">DateStyle</code> accordingly\n            (Peter)</p>",
    "<p>This makes it more likely that the installed\n            <code class=\"filename\">postgresql.conf</code>\n            <code class=\"varname\">DateStyle</code> value will be as\n            desired.</p>"
  ],
  [
    "<p>Reduce number of progress messages displayed by\n            <span class=\"application\">initdb</span> (Tom)</p>"
  ],
  [
    "<p>Allow full timezone names in <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/datatype-datetime.html\" title=\"8.5.&#xA0;Date/Time Types\"><code class=\"type\">timestamp</code></a> input values (Joachim\n            Wieland)</p>",
    "<p>For example, <code class=\"literal\">'2006-05-24 21:11\n            America/New_York'::timestamptz</code>.</p>"
  ],
  [
    "<p>Support configurable timezone abbreviations (Joachim\n            Wieland)</p>",
    "<p>A desired set of timezone abbreviations can be\n            chosen via the configuration parameter <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-client.html#GUC-TIMEZONE-ABBREVIATIONS\">\n            <code class=\"varname\">timezone_abbreviations</code></a>.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/view-pg-timezone-abbrevs.html\" title=\"51.89.&#xA0;pg_timezone_abbrevs\"><code class=\"varname\">pg_timezone_abbrevs</code></a> and <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/view-pg-timezone-names.html\" title=\"51.90.&#xA0;pg_timezone_names\"><code class=\"varname\">pg_timezone_names</code></a> views to show\n            supported timezones (Magnus Hagander)</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-datetime.html#FUNCTIONS-DATETIME-TABLE\" title=\"Table&#xA0;9.30.&#xA0;Date/Time Functions\"><code class=\"function\">\n            clock_timestamp()</code></a>, <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-datetime.html#FUNCTIONS-DATETIME-TABLE\" title=\"Table&#xA0;9.30.&#xA0;Date/Time Functions\"><code class=\"function\">\n            statement_timestamp()</code></a>, and <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-datetime.html#FUNCTIONS-DATETIME-TABLE\" title=\"Table&#xA0;9.30.&#xA0;Date/Time Functions\"><code class=\"function\">\n            transaction_timestamp()</code></a> (Bruce)</p>",
    "<p><code class=\"function\">clock_timestamp()</code> is\n            the current wall-clock time, <code class=\"function\">statement_timestamp()</code> is the time the\n            current statement arrived at the server, and\n            <code class=\"function\">transaction_timestamp()</code>\n            is an alias for <code class=\"function\">now()</code>.</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-formatting.html\" title=\"9.8.&#xA0;Data Type Formatting Functions\"><code class=\"function\">to_char()</code></a> to print localized\n            month and day names (Euler Taveira de Oliveira)</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-formatting.html\" title=\"9.8.&#xA0;Data Type Formatting Functions\"><code class=\"function\">to_char(time)</code></a> and <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-formatting.html\" title=\"9.8.&#xA0;Data Type Formatting Functions\"><code class=\"function\">to_char(interval)</code></a> to output\n            <acronym class=\"acronym\">AM</acronym>/<acronym class=\"acronym\">PM</acronym> specifications (Bruce)</p>",
    "<p>Intervals and times are treated as 24-hour periods,\n            e.g. <code class=\"literal\">25 hours</code> is\n            considered <acronym class=\"acronym\">AM</acronym>.</p>"
  ],
  [
    "<p>Add new function <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-datetime.html#FUNCTIONS-DATETIME-TABLE\" title=\"Table&#xA0;9.30.&#xA0;Date/Time Functions\"><code class=\"function\">\n            justify_interval()</code></a> to adjust interval units\n            (Mark Dilger)</p>"
  ],
  [
    "<p>Allow timezone offsets up to 14:59 away from GMT</p>",
    "<p>Kiribati uses GMT+14, so we'd better accept\n            that.</p>"
  ],
  [
    "<p>Interval computation improvements (Michael\n            Glaesemann, Bruce)</p>"
  ],
  [
    "<p>Allow arrays to contain <code class=\"literal\">NULL</code> elements (Tom)</p>"
  ],
  [
    "<p>Allow assignment to array elements not contiguous\n            with the existing entries (Tom)</p>",
    "<p>The intervening array positions will be filled with\n            nulls. This is per SQL standard.</p>"
  ],
  [
    "<p>New built-in <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-array.html\" title=\"9.18.&#xA0;Array Functions and Operators\">operators</a>\n            for array-subset comparisons (<code class=\"literal\">@&gt;</code>, <code class=\"literal\">&lt;@</code>, <code class=\"literal\">&amp;&amp;</code>) (Teodor, Tom)</p>",
    "<p>These operators can be indexed for many data types\n            using <acronym class=\"acronym\">GiST</acronym> or\n            <acronym class=\"acronym\">GIN</acronym> indexes.</p>"
  ],
  [
    "<p>Add convenient arithmetic <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-net.html#CIDR-INET-OPERATORS-TABLE\" title=\"Table&#xA0;9.36.&#xA0;cidr and inet Operators\">operations</a>\n            on <code class=\"type\">INET</code>/<code class=\"type\">CIDR</code> values (Stephen R. van den Berg)</p>",
    "<p>The new operators are <code class=\"literal\">&amp;</code> (and), <code class=\"literal\">|</code> (or), <code class=\"literal\">~</code>\n            (not), <code class=\"type\">inet</code> <code class=\"literal\">+</code> <code class=\"type\">int8</code>,\n            <code class=\"type\">inet</code> <code class=\"literal\">-</code> <code class=\"type\">int8</code>, and\n            <code class=\"type\">inet</code> <code class=\"literal\">-</code> <code class=\"type\">inet</code>.</p>"
  ],
  [
    "<p>Add new <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-aggregate.html#FUNCTIONS-AGGREGATE-STATISTICS-TABLE\" title=\"Table&#xA0;9.53.&#xA0;Aggregate Functions for Statistics\">\n            aggregate functions</a> from SQL:2003 (Neil)</p>",
    "<p>The new functions are <code class=\"function\">var_pop()</code>, <code class=\"function\">var_samp()</code>, <code class=\"function\">stddev_pop()</code>, and <code class=\"function\">stddev_samp()</code>. <code class=\"function\">var_samp()</code> and <code class=\"function\">stddev_samp()</code> are merely renamings of\n            the existing aggregates <code class=\"function\">variance()</code> and <code class=\"function\">stddev()</code>. The latter names remain\n            available for backward compatibility.</p>"
  ],
  [
    "<p>Add SQL:2003 statistical <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-aggregate.html#FUNCTIONS-AGGREGATE-STATISTICS-TABLE\" title=\"Table&#xA0;9.53.&#xA0;Aggregate Functions for Statistics\">\n            aggregates</a> (Sergey Koposov)</p>",
    "<p>New functions: <code class=\"function\">regr_intercept()</code>, <code class=\"function\">regr_slope()</code>, <code class=\"function\">regr_r2()</code>, <code class=\"function\">corr()</code>, <code class=\"function\">covar_samp()</code>, <code class=\"function\">covar_pop()</code>, <code class=\"function\">regr_avgx()</code>, <code class=\"function\">regr_avgy()</code>, <code class=\"function\">regr_sxy()</code>, <code class=\"function\">regr_sxx()</code>, <code class=\"function\">regr_syy()</code>, <code class=\"function\">regr_count()</code>.</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-createdomain.html\" title=\"CREATE DOMAIN\">domains</a> to be based on other\n            domains (Tom)</p>"
  ],
  [
    "<p>Properly enforce domain <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/ddl-constraints.html\" title=\"5.3.&#xA0;Constraints\"><code class=\"literal\">CHECK</code></a> constraints everywhere\n            (Neil, Tom)</p>",
    "<p>For example, the result of a user-defined function\n            that is declared to return a domain type is now checked\n            against the domain's constraints. This closes a\n            significant hole in the domain implementation.</p>"
  ],
  [
    "<p>Fix problems with dumping renamed <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/datatype-numeric.html#DATATYPE-SERIAL\" title=\"8.1.4.&#xA0;Serial Types\"><code class=\"type\">SERIAL</code></a> columns (Tom)</p>",
    "<p>The fix is to dump a <code class=\"type\">SERIAL</code> column by explicitly specifying\n            its <code class=\"literal\">DEFAULT</code> and sequence\n            elements, and reconstructing the <code class=\"type\">SERIAL</code> column on reload using a new\n            <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-altersequence.html\" title=\"ALTER SEQUENCE\"><code class=\"command\">ALTER SEQUENCE\n            OWNED BY</code></a> command. This also allows dropping\n            a <code class=\"type\">SERIAL</code> column\n            specification.</p>"
  ],
  [
    "<p>Add a server-side sleep function <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-datetime.html#FUNCTIONS-DATETIME-DELAY\" title=\"9.9.5.&#xA0;Delaying Execution\"><code class=\"function\">pg_sleep()</code></a> (Joachim Wieland)</p>"
  ],
  [
    "<p>Add all comparison operators for the <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/datatype-oid.html\" title=\"8.18.&#xA0;Object Identifier Types\"><code class=\"type\">tid</code></a> (tuple id) data type (Mark\n            Kirkwood, Greg Stark, Tom)</p>"
  ],
  [
    "<p>Add <code class=\"literal\">TG_table_name</code> and\n            <code class=\"literal\">TG_table_schema</code> to trigger\n            parameters (Andrew)</p>",
    "<p><code class=\"literal\">TG_relname</code> is now\n            deprecated. Comparable changes have been made in the\n            trigger parameters for the other PLs as well.</p>"
  ],
  [
    "<p>Allow <code class=\"literal\">FOR</code> statements to\n            return values to scalars as well as records and row\n            types (Pavel Stehule)</p>"
  ],
  [
    "<p>Add a <code class=\"literal\">BY</code> clause to the\n            <code class=\"literal\">FOR</code> loop, to control the\n            iteration increment (Jaime Casanova)</p>"
  ],
  [
    "<p>Add <code class=\"literal\">STRICT</code> to <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/plpgsql-statements.html#PLPGSQL-STATEMENTS-SQL-ONEROW\" title=\"42.5.3.&#xA0;Executing a Query with a Single-row Result\">\n            <code class=\"command\">SELECT INTO</code></a> (Matt\n            Miller)</p>",
    "<p><code class=\"literal\">STRICT</code> mode throws an\n            exception if more or less than one row is returned by\n            the <code class=\"command\">SELECT</code>, for\n            <span class=\"productname\">Oracle PL/SQL</span>\n            compatibility.</p>"
  ],
  [
    "<p>Add <code class=\"literal\">table_name</code> and\n            <code class=\"literal\">table_schema</code> to trigger\n            parameters (Adam Sj&#xF8;gren)</p>"
  ],
  [
    "<p>Add prepared queries (Dmitry Karasik)</p>"
  ],
  [
    "<p>Make <code class=\"literal\">$_TD</code> trigger data\n            a global variable (Andrew)</p>",
    "<p>Previously, it was lexical, which caused unexpected\n            sharing violations.</p>"
  ],
  [
    "<p>Run PL/Perl and PL/PerlU in separate interpreters,\n            for security reasons (Andrew)</p>",
    "<p>In consequence, they can no longer share data nor\n            loaded modules. Also, if Perl has not been compiled\n            with the requisite flags to allow multiple\n            interpreters, only one of these languages can be used\n            in any given backend process.</p>"
  ],
  [
    "<p>Named parameters are passed as ordinary variables,\n            as well as in the <code class=\"literal\">args[]</code>\n            array (Sven Suursoho)</p>"
  ],
  [
    "<p>Add <code class=\"literal\">table_name</code> and\n            <code class=\"literal\">table_schema</code> to trigger\n            parameters (Andrew)</p>"
  ],
  [
    "<p>Allow returning of composite types and result sets\n            (Sven Suursoho)</p>"
  ],
  [
    "<p>Return result-set as <code class=\"literal\">list</code>, <code class=\"literal\">iterator</code>, or <code class=\"literal\">generator</code> (Sven Suursoho)</p>"
  ],
  [
    "<p>Allow functions to return <code class=\"literal\">void</code> (Neil)</p>"
  ],
  [
    "<p>Python 2.5 is now supported (Tom)</p>"
  ],
  [
    "<p>Add new command <code class=\"literal\">\\password</code> for changing role password\n            with client-side password encryption (Peter)</p>"
  ],
  [
    "<p>Allow <code class=\"literal\">\\c</code> to connect to\n            a new host and port number (David, Volkan YAZICI)</p>"
  ],
  [
    "<p>Add tablespace display to <code class=\"literal\">\\l+</code> (Philip Yarra)</p>"
  ],
  [
    "<p>Improve <code class=\"literal\">\\df</code> slash\n            command to include the argument names and modes\n            (<code class=\"literal\">OUT</code> or <code class=\"literal\">INOUT</code>) of the function (David\n            Fetter)</p>"
  ],
  [
    "<p>Support binary <code class=\"command\">COPY</code>\n            (Andreas Pflug)</p>"
  ],
  [
    "<p>Add option to run the entire session in a single\n            transaction (Simon)</p>",
    "<p>Use option <code class=\"literal\">-1</code> or\n            <code class=\"literal\">--single-transaction</code>.</p>"
  ],
  [
    "<p>Support for automatically retrieving <code class=\"command\">SELECT</code> results in batches using a\n            cursor (Chris Mair)</p>",
    "<p>This is enabled using <code class=\"command\">\\set\n            FETCH_COUNT <em class=\"replaceable\"><code>n</code></em></code>. This feature\n            allows large result sets to be retrieved in\n            <span class=\"application\">psql</span> without\n            attempting to buffer the entire result set in\n            memory.</p>"
  ],
  [
    "<p>Make multi-line values align in the proper column\n            (Martijn van Oosterhout)</p>",
    "<p>Field values containing newlines are now displayed\n            in a more readable fashion.</p>"
  ],
  [
    "<p>Save multi-line statements as a single entry, rather\n            than one line at a time (Sergey E. Koposov)</p>",
    "<p>This makes up-arrow recall of queries easier. (This\n            is not available on Windows, because that platform uses\n            the native command-line editing present in the\n            operating system.)</p>"
  ],
  [
    "<p>Make the line counter 64-bit so it can handle files\n            with more than two billion lines (David Fetter)</p>"
  ],
  [
    "<p>Report both the returned data and the command status\n            tag for <code class=\"command\">INSERT</code>/<code class=\"command\">UPDATE</code>/<code class=\"command\">DELETE\n            RETURNING</code> (Tom)</p>"
  ],
  [
    "<p>Allow complex selection of objects to be included or\n            excluded by <span class=\"application\">pg_dump</span>\n            (Greg Sabino Mullane)</p>",
    "<p><span class=\"application\">pg_dump</span> now\n            supports multiple <code class=\"literal\">-n</code>\n            (schema) and <code class=\"literal\">-t</code> (table)\n            options, and adds <code class=\"literal\">-N</code> and\n            <code class=\"literal\">-T</code> options to exclude\n            objects. Also, the arguments of these switches can now\n            be wild-card expressions rather than single object\n            names, for example <code class=\"literal\">-t\n            'foo*'</code>, and a schema can be part of a\n            <code class=\"literal\">-t</code> or <code class=\"literal\">-T</code> switch, for example <code class=\"literal\">-t schema1.table1</code>.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/app-pgrestore.html\" title=\"pg_restore\"><span class=\"application\">pg_restore</span></a> <code class=\"literal\">--no-data-for-failed-tables</code> option to\n            suppress loading data if table creation failed (i.e.,\n            the table already exists) (Martin Pitt)</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/app-pgrestore.html\" title=\"pg_restore\"><span class=\"application\">pg_restore</span></a> option to run the\n            entire session in a single transaction (Simon)</p>",
    "<p>Use option <code class=\"literal\">-1</code> or\n            <code class=\"literal\">--single-transaction</code>.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/libpq-misc.html\" title=\"33.11.&#xA0;Miscellaneous Functions\"><code class=\"function\">PQencryptPassword()</code></a> to encrypt\n            passwords (Tom)</p>",
    "<p>This allows passwords to be sent pre-encrypted for\n            commands like <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-alterrole.html\" title=\"ALTER ROLE\"><code class=\"command\">ALTER ROLE ...\n            PASSWORD</code></a>.</p>"
  ],
  [
    "<p>Add function <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/libpq-threading.html\" title=\"33.19.&#xA0;Behavior in Threaded Programs\"><code class=\"function\">\n            PQisthreadsafe()</code></a> (Bruce)</p>",
    "<p>This allows applications to query the thread-safety\n            status of the library.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/libpq-exec.html#LIBPQ-EXEC-MAIN\" title=\"33.3.1.&#xA0;Main Functions\"><code class=\"function\">PQdescribePrepared()</code></a>, <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/libpq-exec.html#LIBPQ-EXEC-MAIN\" title=\"33.3.1.&#xA0;Main Functions\"><code class=\"function\">PQdescribePortal()</code></a>, and related\n            functions to return information about previously\n            prepared statements and open cursors (Volkan\n            YAZICI)</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/libpq-ldap.html\" title=\"33.17.&#xA0;LDAP Lookup of Connection Parameters\"><acronym class=\"acronym\">\n            LDAP</acronym></a> lookups from <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/libpq-pgservice.html\" title=\"33.16.&#xA0;The Connection Service File\"><code class=\"filename\">pg_service.conf</code></a> (Laurenz\n            Albe)</p>"
  ],
  [
    "<p>Allow a hostname in <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/libpq-pgpass.html\" title=\"33.15.&#xA0;The Password File\"><code class=\"filename\">~/.pgpass</code></a> to match the default\n            socket directory (Bruce)</p>",
    "<p>A blank hostname continues to match any Unix-socket\n            connection, but this addition allows entries that are\n            specific to one of several postmasters on the\n            machine.</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-show.html\" title=\"SHOW\"><code class=\"command\">SHOW</code></a> to put its\n            result into a variable (Joachim Wieland)</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-copy.html\" title=\"COPY\"><code class=\"command\">COPY TO STDOUT</code></a>\n            (Joachim Wieland)</p>"
  ],
  [
    "<p>Add regression tests (Joachim Wieland, Michael)</p>"
  ],
  [
    "<p>Major source code cleanups (Joachim Wieland,\n            Michael)</p>"
  ],
  [
    "<p>Allow <acronym class=\"acronym\">MSVC</acronym> to\n            compile the <span class=\"productname\">PostgreSQL</span>\n            server (Magnus, Hiroshi Saito)</p>"
  ],
  [
    "<p>Add <acronym class=\"acronym\">MSVC</acronym> support\n            for utility commands and <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/app-pgdump.html\" title=\"pg_dump\"><span class=\"application\">pg_dump</span></a> (Hiroshi Saito)</p>"
  ],
  [
    "<p>Add support for Windows code pages <code class=\"literal\">1253</code>, <code class=\"literal\">1254</code>, <code class=\"literal\">1255</code>, and <code class=\"literal\">1257</code> (Kris Jurka)</p>"
  ],
  [
    "<p>Drop privileges on startup, so that the server can\n            be started from an administrative account (Magnus)</p>"
  ],
  [
    "<p>Stability fixes (Qingqing Zhou, Magnus)</p>"
  ],
  [
    "<p>Add native semaphore implementation (Qingqing\n            Zhou)</p>",
    "<p>The previous code mimicked SysV semaphores.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/gin.html\" title=\"Chapter&#xA0;64.&#xA0;GIN Indexes\"><acronym class=\"acronym\">GIN</acronym></a> (Generalized Inverted\n            iNdex) index access method (Teodor, Oleg)</p>"
  ],
  [
    "<p>Remove R-tree indexing (Tom)</p>",
    "<p>Rtree has been re-implemented using <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/gist.html\" title=\"Chapter&#xA0;62.&#xA0;GiST Indexes\"><acronym class=\"acronym\">GiST</acronym></a>. Among other differences,\n            this means that rtree indexes now have support for\n            crash recovery via write-ahead logging (WAL).</p>"
  ],
  [
    "<p>Reduce libraries needlessly linked into the backend\n            (Martijn van Oosterhout, Tom)</p>"
  ],
  [
    "<p>Add a configure flag to allow libedit to be\n            preferred over <acronym class=\"acronym\">GNU</acronym>\n            readline (Bruce)</p>",
    "<p>Use configure <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/install-procedure.html#CONFIGURE\" title=\"Configuration\"><code class=\"literal\">--with-libedit-preferred</code></a>.</p>"
  ],
  [
    "<p>Allow installation into directories containing\n            spaces (Peter)</p>"
  ],
  [
    "<p>Improve ability to relocate installation directories\n            (Tom)</p>"
  ],
  [
    "<p>Add support for <span class=\"productname\">Solaris\n            x86_64</span> using the <span class=\"productname\">Solaris</span> compiler (Pierre Girard,\n            Theo Schlossnagle, Bruce)</p>"
  ],
  [
    "<p>Add <span class=\"application\">DTrace</span> support\n            (Robert Lor)</p>"
  ],
  [
    "<p>Add <code class=\"literal\">PG_VERSION_NUM</code> for\n            use by third-party applications wanting to test the\n            backend version in C using &gt; and &lt; comparisons\n            (Bruce)</p>"
  ],
  [
    "<p>Add <code class=\"literal\">XLOG_BLCKSZ</code> as\n            independent from <code class=\"literal\">BLCKSZ</code>\n            (Mark Wong)</p>"
  ],
  [
    "<p>Add <code class=\"literal\">LWLOCK_STATS</code> define\n            to report locking activity (Tom)</p>"
  ],
  [
    "<p>Emit warnings for unknown <span class=\"application\">configure</span> options (Martijn van\n            Oosterhout)</p>"
  ],
  [
    "<p>Add server support for <span class=\"quote\">&#x201C;<span class=\"quote\">plugin</span>&#x201D;</span>\n            libraries that can be used for add-on tasks such as\n            debugging and performance measurement (Korry\n            Douglas)</p>",
    "<p>This consists of two features: a table of\n            <span class=\"quote\">&#x201C;<span class=\"quote\">rendezvous\n            variables</span>&#x201D;</span> that allows separately-loaded\n            shared libraries to communicate, and a new\n            configuration parameter <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-client.html#GUC-LOCAL-PRELOAD-LIBRARIES\">\n            <code class=\"varname\">local_preload_libraries</code></a> that\n            allows libraries to be loaded into specific sessions\n            without explicit cooperation from the client\n            application. This allows external add-ons to implement\n            features such as a PL/pgSQL debugger.</p>"
  ],
  [
    "<p>Rename existing configuration parameter <code class=\"varname\">preload_libraries</code> to <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-client.html#GUC-SHARED-PRELOAD-LIBRARIES\">\n            <code class=\"varname\">shared_preload_libraries</code></a> (Tom)</p>",
    "<p>This was done for clarity in comparison to\n            <code class=\"varname\">local_preload_libraries</code>.</p>"
  ],
  [
    "<p>Add new configuration parameter <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-preset.html#GUC-SERVER-VERSION-NUM\">\n            <code class=\"varname\">server_version_num</code></a>\n            (Greg Sabino Mullane)</p>",
    "<p>This is like <code class=\"varname\">server_version</code>, but is an integer,\n            e.g. <code class=\"literal\">80200</code>. This allows\n            applications to make version checks more easily.</p>"
  ],
  [
    "<p>Add a configuration parameter <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-query.html#GUC-SEQ-PAGE-COST\"><code class=\"varname\">\n            seq_page_cost</code></a> (Tom)</p>"
  ],
  [
    "<p>Re-implement the <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/regress.html\" title=\"Chapter&#xA0;32.&#xA0;Regression Tests\">regression\n            test</a> script as a C program (Magnus, Tom)</p>"
  ],
  [
    "<p>Allow loadable modules to allocate shared memory and\n            lightweight locks (Marc Munro)</p>"
  ],
  [
    "<p>Add automatic initialization and finalization of\n            dynamically loaded libraries (Ralf Engelschall,\n            Tom)</p>",
    "<p>New <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/xfunc-c.html#XFUNC-C-DYNLOAD\" title=\"37.9.1.&#xA0;Dynamic Loading\">functions</a>\n            <code class=\"function\">_PG_init()</code> and\n            <code class=\"function\">_PG_fini()</code> are called if\n            the library defines such symbols. Hence we no longer\n            need to specify an initialization function in\n            <code class=\"varname\">shared_preload_libraries</code>;\n            we can assume that the library used the <code class=\"function\">_PG_init()</code> convention instead.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/xfunc-c.html#XFUNC-C-DYNLOAD\" title=\"37.9.1.&#xA0;Dynamic Loading\"><code class=\"literal\">PG_MODULE_MAGIC</code></a> header block to\n            all shared object files (Martijn van Oosterhout)</p>",
    "<p>The magic block prevents version mismatches between\n            loadable object files and servers.</p>"
  ],
  [
    "<p>Add shared library support for AIX (Laurenz\n            Albe)</p>"
  ],
  [
    "<p>New <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/datatype-xml.html\" title=\"8.13.&#xA0;XML Type\"><acronym class=\"acronym\">XML</acronym></a> documentation section\n            (Bruce)</p>"
  ],
  [
    "<p>Major tsearch2 improvements (Oleg, Teodor)</p>",
    "<div class=\"itemizedlist\">\n              <ul class=\"itemizedlist\" style=\"list-style-type: circle;\">\n                <li class=\"listitem\">\n                  <p>multibyte encoding support, including\n                  <acronym class=\"acronym\">UTF8</acronym></p>\n                </li>\n                <li class=\"listitem\">\n                  <p>query rewriting support</p>\n                </li>\n                <li class=\"listitem\">\n                  <p>improved ranking functions</p>\n                </li>\n                <li class=\"listitem\">\n                  <p>thesaurus dictionary support</p>\n                </li>\n                <li class=\"listitem\">\n                  <p>Ispell dictionaries now recognize <span class=\"application\">MySpell</span> format, used by\n                  <span class=\"application\">OpenOffice</span></p>\n                </li>\n                <li class=\"listitem\">\n                  <p><acronym class=\"acronym\">GIN</acronym>\n                  support</p>\n                </li>\n              </ul>\n            </div>"
  ],
  [
    "<p>multibyte encoding support, including\n                  <acronym class=\"acronym\">UTF8</acronym></p>"
  ],
  [
    "<p>query rewriting support</p>"
  ],
  [
    "<p>improved ranking functions</p>"
  ],
  [
    "<p>thesaurus dictionary support</p>"
  ],
  [
    "<p>Ispell dictionaries now recognize <span class=\"application\">MySpell</span> format, used by\n                  <span class=\"application\">OpenOffice</span></p>"
  ],
  [
    "<p><acronym class=\"acronym\">GIN</acronym>\n                  support</p>"
  ],
  [
    "<p>Add adminpack module containing <span class=\"application\">Pgadmin</span> administration functions\n            (Dave)</p>",
    "<p>These functions provide additional file system\n            access routines not present in the default <span class=\"productname\">PostgreSQL</span> server.</p>"
  ],
  [
    "<p>Add sslinfo module (Victor Wagner)</p>",
    "<p>Reports information about the current connection's\n            <acronym class=\"acronym\">SSL</acronym> certificate.</p>"
  ],
  [
    "<p>Add pgrowlocks module (Tatsuo)</p>",
    "<p>This shows row locking information for a specified\n            table.</p>"
  ],
  [
    "<p>Add hstore module (Oleg, Teodor)</p>"
  ],
  [
    "<p>Add isn module, replacing isbn_issn (Jeremy\n            Kronuz)</p>",
    "<p>This new implementation supports <acronym class=\"acronym\">EAN13</acronym>, <acronym class=\"acronym\">UPC</acronym>, <acronym class=\"acronym\">ISBN</acronym> (books), <acronym class=\"acronym\">ISMN</acronym> (music), and <acronym class=\"acronym\">ISSN</acronym> (serials).</p>"
  ],
  [
    "<p>Add index information functions to pgstattuple\n            (ITAGAKI Takahiro, Satoshi Nagayasu)</p>"
  ],
  [
    "<p>Add pg_freespacemap module to display free space map\n            information (Mark Kirkwood)</p>"
  ],
  [
    "<p>pgcrypto now has all planned functionality (Marko\n            Kreen)</p>",
    "<div class=\"itemizedlist\">\n              <ul class=\"itemizedlist\" style=\"list-style-type: circle;\">\n                <li class=\"listitem\">\n                  <p>Include iMath library in pgcrypto to have the\n                  public-key encryption functions always\n                  available.</p>\n                </li>\n                <li class=\"listitem\">\n                  <p>Add SHA224 algorithm that was missing in\n                  OpenBSD code.</p>\n                </li>\n                <li class=\"listitem\">\n                  <p>Activate builtin code for SHA224/256/384/512\n                  hashes on older OpenSSL to have those algorithms\n                  always available.</p>\n                </li>\n                <li class=\"listitem\">\n                  <p>New function gen_random_bytes() that returns\n                  cryptographically strong randomness. Useful for\n                  generating encryption keys.</p>\n                </li>\n                <li class=\"listitem\">\n                  <p>Remove digest_exists(), hmac_exists() and\n                  cipher_exists() functions.</p>\n                </li>\n              </ul>\n            </div>"
  ],
  [
    "<p>Include iMath library in pgcrypto to have the\n                  public-key encryption functions always\n                  available.</p>"
  ],
  [
    "<p>Add SHA224 algorithm that was missing in\n                  OpenBSD code.</p>"
  ],
  [
    "<p>Activate builtin code for SHA224/256/384/512\n                  hashes on older OpenSSL to have those algorithms\n                  always available.</p>"
  ],
  [
    "<p>New function gen_random_bytes() that returns\n                  cryptographically strong randomness. Useful for\n                  generating encryption keys.</p>"
  ],
  [
    "<p>Remove digest_exists(), hmac_exists() and\n                  cipher_exists() functions.</p>"
  ],
  [
    "<p>Improvements to cube module (Joshua Reich)</p>",
    "<p>New functions are <code class=\"function\">cube(float[])</code>, <code class=\"function\">cube(float[], float[])</code>, and\n            <code class=\"function\">cube_subset(cube,\n            int4[])</code>.</p>"
  ],
  [
    "<p>Add async query capability to dblink (Kai\n            Londenberg, Joe Conway)</p>"
  ],
  [
    "<p>New operators for array-subset comparisons\n            (<code class=\"literal\">@&gt;</code>, <code class=\"literal\">&lt;@</code>, <code class=\"literal\">&amp;&amp;</code>) (Tom)</p>",
    "<p>Various contrib packages already had these operators\n            for their datatypes, but the naming wasn't consistent.\n            We have now added consistently named array-subset\n            comparison operators to the core code and all the\n            contrib packages that have such functionality. (The old\n            names remain available, but are deprecated.)</p>"
  ],
  [
    "<p>Add uninstall scripts for all contrib packages that\n            have install scripts (David, Josh Drake)</p>"
  ]
]