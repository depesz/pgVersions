[
  [
    "<p>Shore up <code class=\"literal\">GRANT ... WITH ADMIN\n          OPTION</code> restrictions (Noah Misch)</p>",
    "<p>Granting a role without <code class=\"literal\">ADMIN\n          OPTION</code> is supposed to prevent the grantee from\n          adding or removing members from the granted role, but\n          this restriction was easily bypassed by doing\n          <code class=\"literal\">SET ROLE</code> first. The security\n          impact is mostly that a role member can revoke the access\n          of others, contrary to the wishes of his grantor.\n          Unapproved role member additions are a lesser concern,\n          since an uncooperative role member could provide most of\n          his rights to others anyway by creating views or\n          <code class=\"literal\">SECURITY DEFINER</code> functions.\n          (CVE-2014-0060)</p>"
  ],
  [
    "<p>Prevent privilege escalation via manual calls to PL\n          validator functions (Andres Freund)</p>",
    "<p>The primary role of PL validator functions is to be\n          called implicitly during <code class=\"command\">CREATE\n          FUNCTION</code>, but they are also normal SQL functions\n          that a user can call explicitly. Calling a validator on a\n          function actually written in some other language was not\n          checked for and could be exploited for\n          privilege-escalation purposes. The fix involves adding a\n          call to a privilege-checking function in each validator\n          function. Non-core procedural languages will also need to\n          make this change to their own validator functions, if\n          any. (CVE-2014-0061)</p>"
  ],
  [
    "<p>Avoid multiple name lookups during table and index DDL\n          (Robert Haas, Andres Freund)</p>",
    "<p>If the name lookups come to different conclusions due\n          to concurrent activity, we might perform some parts of\n          the DDL on a different table than other parts. At least\n          in the case of <code class=\"command\">CREATE INDEX</code>,\n          this can be used to cause the permissions checks to be\n          performed against a different table than the index\n          creation, allowing for a privilege escalation attack.\n          (CVE-2014-0062)</p>"
  ],
  [
    "<p>Prevent buffer overrun with long datetime strings\n          (Noah Misch)</p>",
    "<p>The <code class=\"literal\">MAXDATELEN</code> constant\n          was too small for the longest possible value of type\n          <code class=\"type\">interval</code>, allowing a buffer\n          overrun in <code class=\"function\">interval_out()</code>.\n          Although the datetime input functions were more careful\n          about avoiding buffer overrun, the limit was short enough\n          to cause them to reject some valid inputs, such as input\n          containing a very long timezone name. The <span class=\"application\">ecpg</span> library contained these\n          vulnerabilities along with some of its own.\n          (CVE-2014-0063)</p>"
  ],
  [
    "<p>Prevent buffer overrun due to integer overflow in size\n          calculations (Noah Misch, Heikki Linnakangas)</p>",
    "<p>Several functions, mostly type input functions,\n          calculated an allocation size without checking for\n          overflow. If overflow did occur, a too-small buffer would\n          be allocated and then written past. (CVE-2014-0064)</p>"
  ],
  [
    "<p>Prevent overruns of fixed-size buffers (Peter\n          Eisentraut, Jozef Mlich)</p>",
    "<p>Use <code class=\"function\">strlcpy()</code> and\n          related functions to provide a clear guarantee that\n          fixed-size buffers are not overrun. Unlike the preceding\n          items, it is unclear whether these cases really represent\n          live issues, since in most cases there appear to be\n          previous constraints on the size of the input string.\n          Nonetheless it seems prudent to silence all Coverity\n          warnings of this type. (CVE-2014-0065)</p>"
  ],
  [
    "<p>Avoid crashing if <code class=\"function\">crypt()</code> returns NULL (Honza Horak,\n          Bruce Momjian)</p>",
    "<p>There are relatively few scenarios in which\n          <code class=\"function\">crypt()</code> could return NULL,\n          but <code class=\"filename\">contrib/chkpass</code> would\n          crash if it did. One practical case in which this could\n          be an issue is if <span class=\"application\">libc</span>\n          is configured to refuse to execute unapproved hashing\n          algorithms (e.g., <span class=\"quote\">&#x201C;<span class=\"quote\">FIPS mode</span>&#x201D;</span>). (CVE-2014-0066)</p>"
  ],
  [
    "<p>Document risks of <code class=\"literal\">make\n          check</code> in the regression testing instructions (Noah\n          Misch, Tom Lane)</p>",
    "<p>Since the temporary server started by <code class=\"literal\">make check</code> uses <span class=\"quote\">&#x201C;<span class=\"quote\">trust</span>&#x201D;</span>\n          authentication, another user on the same machine could\n          connect to it as database superuser, and then potentially\n          exploit the privileges of the operating-system user who\n          started the tests. A future release will probably\n          incorporate changes in the testing procedure to prevent\n          this risk, but some public discussion is needed first. So\n          for the moment, just warn people against using\n          <code class=\"literal\">make check</code> when there are\n          untrusted users on the same machine. (CVE-2014-0067)</p>"
  ],
  [
    "<p>Rework tuple freezing protocol (&#xC1;lvaro Herrera, Andres\n          Freund)</p>",
    "<p>The logic for tuple freezing was unable to handle some\n          cases involving freezing of <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/routine-vacuuming.html#VACUUM-FOR-MULTIXACT-WRAPAROUND\" title=\"24.1.5.1.&#xA0;Multixacts and Wraparound\"><em class=\"firstterm\">multixact</em> IDs</a>, with the practical\n          effect that shared row-level locks might be forgotten\n          once old enough.</p>",
    "<p>Fixing this required changing the WAL record format\n          for tuple freezing. While this is no issue for standalone\n          servers, when using replication it means that\n          <span class=\"emphasis\"><em>standby servers must be\n          upgraded to 9.3.3 or later before their masters\n          are</em></span>. An older standby will be unable to\n          interpret freeze records generated by a newer master, and\n          will fail with a PANIC message. (In such a case,\n          upgrading the standby should be sufficient to let it\n          resume execution.)</p>"
  ],
  [
    "<p>Create separate GUC parameters to control multixact\n          freezing (&#xC1;lvaro Herrera)</p>",
    "<p>9.3 requires multixact tuple labels to be frozen\n          before they grow too old, in the same fashion as plain\n          transaction ID labels have been frozen for some time.\n          Previously, the transaction ID freezing parameters were\n          used for multixact IDs too; but since the consumption\n          rates of transaction IDs and multixact IDs can be quite\n          different, this did not work very well. Introduce new\n          settings <a class=\"xref\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-client.html#GUC-VACUUM-MULTIXACT-FREEZE-MIN-AGE\">\n          vacuum_multixact_freeze_min_age</a>, <a class=\"xref\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-client.html#GUC-VACUUM-MULTIXACT-FREEZE-TABLE-AGE\">\n          vacuum_multixact_freeze_table_age</a>, and <a class=\"xref\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-autovacuum.html#GUC-AUTOVACUUM-MULTIXACT-FREEZE-MAX-AGE\">\n          autovacuum_multixact_freeze_max_age</a> to control when\n          to freeze multixacts.</p>"
  ],
  [
    "<p>Account for remote row locks propagated by local\n          updates (&#xC1;lvaro Herrera)</p>",
    "<p>If a row was locked by transaction A, and transaction\n          B updated it, the new version of the row created by B\n          would be locked by A, yet visible only to B. If\n          transaction B then again updated the row, A's lock\n          wouldn't get checked, thus possibly allowing B to\n          complete when it shouldn't. This case is new in 9.3 since\n          prior versions did not have any types of row locking that\n          would permit another transaction to update the row at\n          all.</p>",
    "<p>This oversight could allow referential integrity\n          checks to give false positives (for instance, allow\n          deletes that should have been rejected). Applications\n          using the new commands <code class=\"literal\">SELECT FOR\n          KEY SHARE</code> and <code class=\"literal\">SELECT FOR NO\n          KEY UPDATE</code> might also have suffered locking\n          failures of this kind.</p>"
  ],
  [
    "<p>Prevent <span class=\"quote\">&#x201C;<span class=\"quote\">forgetting</span>&#x201D;</span> valid row locks when\n          one of several holders of a row lock aborts (&#xC1;lvaro\n          Herrera)</p>",
    "<p>This was yet another mechanism by which a shared row\n          lock could be lost, thus possibly allowing updates that\n          should have been prevented by foreign-key\n          constraints.</p>"
  ],
  [
    "<p>Fix incorrect logic during update chain locking\n          (&#xC1;lvaro Herrera)</p>",
    "<p>This mistake could result in spurious <span class=\"quote\">&#x201C;<span class=\"quote\">could not serialize access\n          due to concurrent update</span>&#x201D;</span> errors in\n          <code class=\"literal\">REPEATABLE READ</code> and\n          <code class=\"literal\">SERIALIZABLE</code> transaction\n          isolation modes.</p>"
  ],
  [
    "<p>Handle wraparound correctly during extension or\n          truncation of <code class=\"filename\">pg_multixact/members</code> (Andres Freund,\n          &#xC1;lvaro Herrera)</p>"
  ],
  [
    "<p>Fix handling of 5-digit filenames in <code class=\"filename\">pg_multixact/members</code> (&#xC1;lvaro\n          Herrera)</p>",
    "<p>As of 9.3, these names can be more than 4 digits, but\n          the directory cleanup code ignored such files.</p>"
  ],
  [
    "<p>Improve performance of multixact cache code (&#xC1;lvaro\n          Herrera)</p>"
  ],
  [
    "<p>Optimize updating a row that's already locked by the\n          same transaction (Andres Freund, &#xC1;lvaro Herrera)</p>",
    "<p>This fixes a performance regression from pre-9.3\n          versions when doing <code class=\"literal\">SELECT FOR\n          UPDATE</code> followed by <code class=\"literal\">UPDATE/DELETE</code>.</p>"
  ],
  [
    "<p>During archive recovery, prefer highest timeline\n          number when WAL segments with the same ID are present in\n          both the archive and <code class=\"filename\">pg_xlog/</code> (Kyotaro Horiguchi)</p>",
    "<p>Previously, not-yet-archived segments could get\n          ignored during recovery. This reverts an undesirable\n          behavioral change in 9.3.0 back to the way things worked\n          pre-9.3.</p>"
  ],
  [
    "<p>Fix possible mis-replay of WAL records when some\n          segments of a relation aren't full size (Greg Stark, Tom\n          Lane)</p>",
    "<p>The WAL update could be applied to the wrong page,\n          potentially many pages past where it should have been.\n          Aside from corrupting data, this error has been observed\n          to result in significant <span class=\"quote\">&#x201C;<span class=\"quote\">bloat</span>&#x201D;</span> of\n          standby servers compared to their masters, due to updates\n          being applied far beyond where the end-of-file should\n          have been. This failure mode does not appear to be a\n          significant risk during crash recovery, only when\n          initially synchronizing a standby created from a base\n          backup taken from a quickly-changing master.</p>"
  ],
  [
    "<p>Fix bug in determining when recovery has reached\n          consistency (Tomonari Katsumata, Heikki Linnakangas)</p>",
    "<p>In some cases WAL replay would mistakenly conclude\n          that the database was already consistent at the start of\n          replay, thus possibly allowing hot-standby queries before\n          the database was really consistent. Other symptoms such\n          as <span class=\"quote\">&#x201C;<span class=\"quote\">PANIC: WAL\n          contains references to invalid pages</span>&#x201D;</span> were\n          also possible.</p>"
  ],
  [
    "<p>Fix WAL logging of visibility map changes (Heikki\n          Linnakangas)</p>"
  ],
  [
    "<p>Fix improper locking of btree index pages while\n          replaying a <code class=\"literal\">VACUUM</code> operation\n          in hot-standby mode (Andres Freund, Heikki Linnakangas,\n          Tom Lane)</p>",
    "<p>This error could result in <span class=\"quote\">&#x201C;<span class=\"quote\">PANIC: WAL contains\n          references to invalid pages</span>&#x201D;</span> failures.</p>"
  ],
  [
    "<p>Ensure that insertions into non-leaf GIN index pages\n          write a full-page WAL record when appropriate (Heikki\n          Linnakangas)</p>",
    "<p>The previous coding risked index corruption in the\n          event of a partial-page write during a system crash.</p>"
  ],
  [
    "<p>When <code class=\"literal\">pause_at_recovery_target</code> and\n          <code class=\"literal\">recovery_target_inclusive</code>\n          are both set, ensure the target record is applied before\n          pausing, not after (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Ensure walreceiver sends hot-standby feedback messages\n          on time even when there is a continuous stream of data\n          (Andres Freund, Amit Kapila)</p>"
  ],
  [
    "<p>Prevent timeout interrupts from taking control away\n          from mainline code unless <code class=\"varname\">ImmediateInterruptOK</code> is set (Andres\n          Freund, Tom Lane)</p>",
    "<p>This is a serious issue for any application making use\n          of statement timeouts, as it could cause all manner of\n          strange failures after a timeout occurred. We have seen\n          reports of <span class=\"quote\">&#x201C;<span class=\"quote\">stuck</span>&#x201D;</span> spinlocks, ERRORs being\n          unexpectedly promoted to PANICs, unkillable backends, and\n          other misbehaviors.</p>"
  ],
  [
    "<p>Fix race conditions during server process exit (Robert\n          Haas)</p>",
    "<p>Ensure that signal handlers don't attempt to use the\n          process's <code class=\"varname\">MyProc</code> pointer\n          after it's no longer valid.</p>"
  ],
  [
    "<p>Fix race conditions in walsender shutdown logic and\n          walreceiver SIGHUP signal handler (Tom Lane)</p>"
  ],
  [
    "<p>Fix unsafe references to <code class=\"varname\">errno</code> within error reporting logic\n          (Christian Kruse)</p>",
    "<p>This would typically lead to odd behaviors such as\n          missing or inappropriate <code class=\"literal\">HINT</code> fields.</p>"
  ],
  [
    "<p>Fix possible crashes from using <code class=\"function\">ereport()</code> too early during server\n          startup (Tom Lane)</p>",
    "<p>The principal case we've seen in the field is a crash\n          if the server is started in a directory it doesn't have\n          permission to read.</p>"
  ],
  [
    "<p>Clear retry flags properly in OpenSSL socket write\n          function (Alexander Kukushkin)</p>",
    "<p>This omission could result in a server lockup after\n          unexpected loss of an SSL-encrypted connection.</p>"
  ],
  [
    "<p>Fix length checking for Unicode identifiers\n          (<code class=\"literal\">U&amp;\"...\"</code> syntax)\n          containing escapes (Tom Lane)</p>",
    "<p>A spurious truncation warning would be printed for\n          such identifiers if the escaped form of the identifier\n          was too long, but the identifier actually didn't need\n          truncation after de-escaping.</p>"
  ],
  [
    "<p>Fix parsing of Unicode literals and identifiers just\n          before the end of a command string or function body (Tom\n          Lane)</p>"
  ],
  [
    "<p>Allow keywords that are type names to be used in lists\n          of roles (Stephen Frost)</p>",
    "<p>A previous patch allowed such keywords to be used\n          without quoting in places such as role identifiers; but\n          it missed cases where a list of role identifiers was\n          permitted, such as <code class=\"literal\">DROP\n          ROLE</code>.</p>"
  ],
  [
    "<p>Fix parser crash for <code class=\"literal\">EXISTS(SELECT * FROM zero_column_table)</code>\n          (Tom Lane)</p>"
  ],
  [
    "<p>Fix possible crash due to invalid plan for nested\n          sub-selects, such as <code class=\"literal\">WHERE (... x\n          IN (SELECT ...) ...) IN (SELECT ...)</code> (Tom\n          Lane)</p>"
  ],
  [
    "<p>Fix mishandling of <code class=\"literal\">WHERE</code>\n          conditions pulled up from a <code class=\"literal\">LATERAL</code> subquery (Tom Lane)</p>",
    "<p>The typical symptom of this bug was a <span class=\"quote\">&#x201C;<span class=\"quote\">JOIN qualification cannot\n          refer to other relations</span>&#x201D;</span> error, though\n          subtle logic errors in created plans seem possible as\n          well.</p>"
  ],
  [
    "<p>Disallow <code class=\"literal\">LATERAL</code>\n          references to the target table of an <code class=\"literal\">UPDATE/DELETE</code> (Tom Lane)</p>",
    "<p>While this might be allowed in some future release, it\n          was unintentional in 9.3, and didn't work quite right\n          anyway.</p>"
  ],
  [
    "<p>Fix <code class=\"literal\">UPDATE/DELETE</code> of an\n          inherited target table that has <code class=\"literal\">UNION ALL</code> subqueries (Tom Lane)</p>",
    "<p>Without this fix, <code class=\"literal\">UNION\n          ALL</code> subqueries aren't correctly inserted into the\n          update plans for inheritance child tables after the first\n          one, typically resulting in no update happening for those\n          child table(s).</p>"
  ],
  [
    "<p>Fix <code class=\"command\">ANALYZE</code> to not fail\n          on a column that's a domain over a range type (Tom\n          Lane)</p>"
  ],
  [
    "<p>Ensure that <code class=\"command\">ANALYZE</code>\n          creates statistics for a table column even when all the\n          values in it are <span class=\"quote\">&#x201C;<span class=\"quote\">too wide</span>&#x201D;</span> (Tom Lane)</p>",
    "<p><code class=\"command\">ANALYZE</code> intentionally\n          omits very wide values from its histogram and\n          most-common-values calculations, but it neglected to do\n          something sane in the case that all the sampled entries\n          are too wide.</p>"
  ],
  [
    "<p>In <code class=\"literal\">ALTER TABLE ... SET\n          TABLESPACE</code>, allow the database's default\n          tablespace to be used without a permissions check\n          (Stephen Frost)</p>",
    "<p><code class=\"literal\">CREATE TABLE</code> has always\n          allowed such usage, but <code class=\"literal\">ALTER\n          TABLE</code> didn't get the memo.</p>"
  ],
  [
    "<p>Fix support for extensions containing event triggers\n          (Tom Lane)</p>"
  ],
  [
    "<p>Fix <span class=\"quote\">&#x201C;<span class=\"quote\">cannot\n          accept a set</span>&#x201D;</span> error when some arms of a\n          <code class=\"literal\">CASE</code> return a set and others\n          don't (Tom Lane)</p>"
  ],
  [
    "<p>Fix memory leakage in JSON functions (Craig\n          Ringer)</p>"
  ],
  [
    "<p>Properly distinguish numbers from non-numbers when\n          generating JSON output (Andrew Dunstan)</p>"
  ],
  [
    "<p>Fix checks for all-zero client addresses in pgstat\n          functions (Kevin Grittner)</p>"
  ],
  [
    "<p>Fix possible misclassification of multibyte characters\n          by the text search parser (Tom Lane)</p>",
    "<p>Non-ASCII characters could be misclassified when using\n          C locale with a multibyte encoding. On Cygwin, non-C\n          locales could fail as well.</p>"
  ],
  [
    "<p>Fix possible misbehavior in <code class=\"function\">plainto_tsquery()</code> (Heikki\n          Linnakangas)</p>",
    "<p>Use <code class=\"function\">memmove()</code> not\n          <code class=\"function\">memcpy()</code> for copying\n          overlapping memory regions. There have been no field\n          reports of this actually causing trouble, but it's\n          certainly risky.</p>"
  ],
  [
    "<p>Fix placement of permissions checks in <code class=\"function\">pg_start_backup()</code> and <code class=\"function\">pg_stop_backup()</code> (Andres Freund, Magnus\n          Hagander)</p>",
    "<p>The previous coding might attempt to do catalog access\n          when it shouldn't.</p>"
  ],
  [
    "<p>Accept <code class=\"literal\">SHIFT_JIS</code> as an\n          encoding name for locale checking purposes (Tatsuo\n          Ishii)</p>"
  ],
  [
    "<p>Fix <code class=\"literal\">*</code>-qualification of\n          named parameters in SQL-language functions (Tom Lane)</p>",
    "<p>Given a composite-type parameter named <code class=\"literal\">foo</code>, <code class=\"literal\">$1.*</code>\n          worked fine, but <code class=\"literal\">foo.*</code> not\n          so much.</p>"
  ],
  [
    "<p>Fix misbehavior of <code class=\"function\">PQhost()</code> on Windows (Fujii Masao)</p>",
    "<p>It should return <code class=\"literal\">localhost</code> if no host has been\n          specified.</p>"
  ],
  [
    "<p>Improve error handling in <span class=\"application\">libpq</span> and <span class=\"application\">psql</span> for failures during\n          <code class=\"literal\">COPY TO STDOUT/FROM STDIN</code>\n          (Tom Lane)</p>",
    "<p>In particular this fixes an infinite loop that could\n          occur in 9.2 and up if the server connection was lost\n          during <code class=\"literal\">COPY FROM STDIN</code>.\n          Variants of that scenario might be possible in older\n          versions, or with other client applications.</p>"
  ],
  [
    "<p>Fix incorrect translation handling in some\n          <span class=\"application\">psql</span> <code class=\"literal\">\\d</code> commands (Peter Eisentraut, Tom\n          Lane)</p>"
  ],
  [
    "<p>Ensure <span class=\"application\">pg_basebackup</span>'s background process\n          is killed when exiting its foreground process (Magnus\n          Hagander)</p>"
  ],
  [
    "<p>Fix possible incorrect printing of filenames in\n          <span class=\"application\">pg_basebackup</span>'s verbose\n          mode (Magnus Hagander)</p>"
  ],
  [
    "<p>Avoid including tablespaces inside PGDATA twice in\n          base backups (Dimitri Fontaine, Magnus Hagander)</p>"
  ],
  [
    "<p>Fix misaligned descriptors in <span class=\"application\">ecpg</span> (MauMau)</p>"
  ],
  [
    "<p>In <span class=\"application\">ecpg</span>, handle lack\n          of a hostname in the connection parameters properly\n          (Michael Meskes)</p>"
  ],
  [
    "<p>Fix performance regression in <code class=\"filename\">contrib/dblink</code> connection startup (Joe\n          Conway)</p>",
    "<p>Avoid an unnecessary round trip when client and server\n          encodings match.</p>"
  ],
  [
    "<p>In <code class=\"filename\">contrib/isn</code>, fix\n          incorrect calculation of the check digit for ISMN values\n          (Fabien Coelho)</p>"
  ],
  [
    "<p>Fix <code class=\"filename\">contrib/pgbench</code>'s\n          progress logging to avoid overflow when the scale factor\n          is large (Tatsuo Ishii)</p>"
  ],
  [
    "<p>Fix <code class=\"filename\">contrib/pg_stat_statement</code>'s handling of\n          <code class=\"literal\">CURRENT_DATE</code> and related\n          constructs (Kyotaro Horiguchi)</p>"
  ],
  [
    "<p>Improve lost-connection error handling in <code class=\"filename\">contrib/postgres_fdw</code> (Tom Lane)</p>"
  ],
  [
    "<p>Ensure client-code-only installation procedure works\n          as documented (Peter Eisentraut)</p>"
  ],
  [
    "<p>In Mingw and Cygwin builds, install the <span class=\"application\">libpq</span> DLL in the <code class=\"filename\">bin</code> directory (Andrew Dunstan)</p>",
    "<p>This duplicates what the MSVC build has long done. It\n          should fix problems with programs like <span class=\"application\">psql</span> failing to start because they\n          can't find the DLL.</p>"
  ],
  [
    "<p>Avoid using the deprecated <code class=\"literal\">dllwrap</code> tool in Cygwin builds (Marco\n          Atzeri)</p>"
  ],
  [
    "<p>Enable building with Visual Studio 2013 (Brar\n          Piening)</p>"
  ],
  [
    "<p>Don't generate plain-text <code class=\"filename\">HISTORY</code> and <code class=\"filename\">src/test/regress/README</code> files anymore\n          (Tom Lane)</p>",
    "<p>These text files duplicated the main HTML and PDF\n          documentation formats. The trouble involved in\n          maintaining them greatly outweighs the likely audience\n          for plain-text format. Distribution tarballs will still\n          contain files by these names, but they'll just be stubs\n          directing the reader to consult the main documentation.\n          The plain-text <code class=\"filename\">INSTALL</code> file\n          will still be maintained, as there is arguably a use-case\n          for that.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2013i for DST law\n          changes in Jordan and historical changes in Cuba.</p>",
    "<p>In addition, the zones <code class=\"literal\">Asia/Riyadh87</code>, <code class=\"literal\">Asia/Riyadh88</code>, and <code class=\"literal\">Asia/Riyadh89</code> have been removed, as they\n          are no longer maintained by IANA, and never represented\n          actual civil timekeeping practice.</p>"
  ]
]