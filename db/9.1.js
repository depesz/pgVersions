[
  [
    "<p>Support unlogged tables using the <code class=\"literal\">UNLOGGED</code> option in <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-createtable.html\" title=\"CREATE TABLE\"><code class=\"command\">CREATE\n              TABLE</code></a> (Robert Haas)</p>",
    "<p>Such tables provide better update performance than\n              regular tables, but are not crash-safe: their\n              contents are automatically cleared in case of a\n              server crash. Their contents do not propagate to\n              replication slaves, either.</p>"
  ],
  [
    "<p>Allow <code class=\"literal\">FULL OUTER JOIN</code>\n              to be implemented as a hash join, and allow either\n              side of a <code class=\"literal\">LEFT OUTER\n              JOIN</code> or <code class=\"literal\">RIGHT OUTER\n              JOIN</code> to be hashed (Tom Lane)</p>",
    "<p>Previously <code class=\"literal\">FULL OUTER\n              JOIN</code> could only be implemented as a merge\n              join, and <code class=\"literal\">LEFT OUTER\n              JOIN</code> and <code class=\"literal\">RIGHT OUTER\n              JOIN</code> could hash only the nullable side of the\n              join. These changes provide additional query\n              optimization possibilities.</p>"
  ],
  [
    "<p>Merge duplicate fsync requests (Robert Haas, Greg\n              Smith)</p>",
    "<p>This greatly improves performance under heavy\n              write loads.</p>"
  ],
  [
    "<p>Improve performance of <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-wal.html#GUC-COMMIT-SIBLINGS\"><code class=\"varname\">\n              commit_siblings</code></a> (Greg Smith)</p>",
    "<p>This allows the use of <code class=\"varname\">commit_siblings</code> with less\n              overhead.</p>"
  ],
  [
    "<p>Reduce the memory requirement for large ispell\n              dictionaries (Pavel Stehule, Tom Lane)</p>"
  ],
  [
    "<p>Avoid leaving data files open after <span class=\"quote\">&#x201C;<span class=\"quote\">blind\n              writes</span>&#x201D;</span> (Alvaro Herrera)</p>",
    "<p>This fixes scenarios in which backends might hold\n              files open long after they were deleted, preventing\n              the kernel from reclaiming disk space.</p>"
  ],
  [
    "<p>Allow inheritance table scans to return\n              meaningfully-sorted results (Greg Stark, Hans-Jurgen\n              Schonig, Robert Haas, Tom Lane)</p>",
    "<p>This allows better optimization of queries that\n              use <code class=\"literal\">ORDER BY</code>,\n              <code class=\"literal\">LIMIT</code>, or <code class=\"literal\">MIN</code>/<code class=\"literal\">MAX</code>\n              with inherited tables.</p>"
  ],
  [
    "<p>Improve GIN index scan cost estimation (Teodor\n              Sigaev)</p>"
  ],
  [
    "<p>Improve cost estimation for aggregates and window\n              functions (Tom Lane)</p>"
  ],
  [
    "<p>Support host names and host suffixes (e.g.\n              <code class=\"literal\">.example.com</code>) in\n              <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/auth-pg-hba-conf.html\" title=\"20.1.&#xA0;The pg_hba.conf File\"><code class=\"filename\">pg_hba.conf</code></a> (Peter\n              Eisentraut)</p>",
    "<p>Previously only host <acronym class=\"acronym\">IP</acronym> addresses and <acronym class=\"acronym\">CIDR</acronym> values were supported.</p>"
  ],
  [
    "<p>Support the key word <code class=\"literal\">all</code> in the host column of <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/auth-pg-hba-conf.html\" title=\"20.1.&#xA0;The pg_hba.conf File\"><code class=\"filename\">pg_hba.conf</code></a> (Peter\n              Eisentraut)</p>",
    "<p>Previously people used <code class=\"literal\">0.0.0.0/0</code> or <code class=\"literal\">::/0</code> for this.</p>"
  ],
  [
    "<p>Reject <code class=\"literal\">local</code> lines in\n              <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/auth-pg-hba-conf.html\" title=\"20.1.&#xA0;The pg_hba.conf File\"><code class=\"filename\">pg_hba.conf</code></a> on platforms that\n              don't support Unix-socket connections (Magnus\n              Hagander)</p>",
    "<p>Formerly, such lines were silently ignored, which\n              could be surprising. This makes the behavior more\n              like other unsupported cases.</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/auth-methods.html#GSSAPI-AUTH\" title=\"20.3.3.&#xA0;GSSAPI Authentication\"><acronym class=\"acronym\">GSSAPI</acronym></a> to be used to\n              authenticate to servers via <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/auth-methods.html#SSPI-AUTH\" title=\"20.3.4.&#xA0;SSPI Authentication\"><acronym class=\"acronym\">SSPI</acronym></a> (Christian Ullrich)</p>",
    "<p>Specifically this allows Unix-based\n              <acronym class=\"acronym\">GSSAPI</acronym> clients to\n              do <acronym class=\"acronym\">SSPI</acronym>\n              authentication with Windows servers.</p>"
  ],
  [
    "<p><a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/auth-methods.html#AUTH-IDENT\" title=\"20.3.5.&#xA0;Ident Authentication\"><code class=\"literal\">ident</code></a> authentication over local\n              sockets is now known as <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/auth-methods.html#AUTH-PEER\" title=\"20.3.6.&#xA0;Peer Authentication\"><code class=\"literal\">peer</code></a> (Magnus Hagander)</p>",
    "<p>The old term is still accepted for backward\n              compatibility, but since the two methods are\n              fundamentally different, it seemed better to adopt\n              different names for them.</p>"
  ],
  [
    "<p>Rewrite <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/auth-methods.html#AUTH-PEER\" title=\"20.3.6.&#xA0;Peer Authentication\"><acronym class=\"acronym\">peer</acronym></a> authentication to avoid\n              use of credential control messages (Tom Lane)</p>",
    "<p>This change makes the peer authentication code\n              simpler and better-performing. However, it requires\n              the platform to provide the <code class=\"function\">getpeereid</code> function or an\n              equivalent socket operation. So far as is known, the\n              only platform for which peer authentication worked\n              before and now will not is pre-5.0 NetBSD.</p>"
  ],
  [
    "<p>Add details to the logging of restartpoints and\n              checkpoints, which is controlled by <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-logging.html#GUC-LOG-CHECKPOINTS\"><code class=\"varname\">\n              log_checkpoints</code></a> (Fujii Masao, Greg\n              Smith)</p>",
    "<p>New details include <acronym class=\"acronym\">WAL</acronym> file and sync activity.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-logging.html#GUC-LOG-FILE-MODE\"><code class=\"varname\">\n              log_file_mode</code></a> which controls the\n              permissions on log files created by the logging\n              collector (Martin Pihlak)</p>"
  ],
  [
    "<p>Reduce the default maximum line length for\n              <span class=\"application\">syslog</span> logging to\n              900 bytes plus prefixes (Noah Misch)</p>",
    "<p>This avoids truncation of long log lines on syslog\n              implementations that have a 1KB length limit, rather\n              than the more common 2KB.</p>"
  ],
  [
    "<p>Add <code class=\"structfield\">client_hostname</code> column to\n              <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/monitoring-stats.html#MONITORING-STATS-VIEWS-TABLE\" title=\"Table&#xA0;28.2.&#xA0;Collected Statistics Views\"><code class=\"structname\">\n              pg_stat_activity</code></a> (Peter Eisentraut)</p>",
    "<p>Previously only the client address was\n              reported.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/monitoring-stats.html#MONITORING-STATS-VIEWS-TABLE\" title=\"Table&#xA0;28.2.&#xA0;Collected Statistics Views\"><code class=\"structname\">\n              pg_stat_xact_*</code></a> statistics functions and\n              views (Joel Jacobson)</p>",
    "<p>These are like the database-wide statistics\n              counter views, but reflect counts for only the\n              current transaction.</p>"
  ],
  [
    "<p>Add time of last reset in database-level and\n              background writer statistics views (Tomas Vondra)</p>"
  ],
  [
    "<p>Add columns showing the number of vacuum and\n              analyze operations in <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/monitoring-stats.html#MONITORING-STATS-VIEWS-TABLE\" title=\"Table&#xA0;28.2.&#xA0;Collected Statistics Views\"><code class=\"structname\">\n              pg_stat_*_tables</code></a> views (Magnus\n              Hagander)</p>"
  ],
  [
    "<p>Add <code class=\"structfield\">buffers_backend_fsync</code> column to\n              <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/monitoring-stats.html#MONITORING-STATS-VIEWS-TABLE\" title=\"Table&#xA0;28.2.&#xA0;Collected Statistics Views\"><code class=\"structname\">\n              pg_stat_bgwriter</code></a> (Greg Smith)</p>",
    "<p>This new column counts the number of times a\n              backend fsyncs a buffer.</p>"
  ],
  [
    "<p>Provide auto-tuning of <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-wal.html#GUC-WAL-BUFFERS\"><code class=\"varname\">\n              wal_buffers</code></a> (Greg Smith)</p>",
    "<p>By default, the value of <code class=\"varname\">wal_buffers</code> is now chosen\n              automatically based on the value of <code class=\"varname\">shared_buffers</code>.</p>"
  ],
  [
    "<p>Increase the maximum values for <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-locks.html#GUC-DEADLOCK-TIMEOUT\"><code class=\"varname\">\n              deadlock_timeout</code></a>, <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-logging.html#GUC-LOG-MIN-DURATION-STATEMENT\">\n              <code class=\"varname\">log_min_duration_statement</code></a>, and\n              <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-autovacuum.html#GUC-LOG-AUTOVACUUM-MIN-DURATION\">\n              <code class=\"varname\">log_autovacuum_min_duration</code></a>\n              (Peter Eisentraut)</p>",
    "<p>The maximum value for each of these parameters was\n              previously only about 35 minutes. Much larger values\n              are now allowed.</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/warm-standby.html#SYNCHRONOUS-REPLICATION\" title=\"26.2.8.&#xA0;Synchronous Replication\">synchronous\n              replication</a> (Simon Riggs, Fujii Masao)</p>",
    "<p>This allows the primary server to wait for a\n              standby to write a transaction's information to disk\n              before acknowledging the commit. One standby at a\n              time can take the role of the synchronous standby, as\n              controlled by the <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-replication.html#GUC-SYNCHRONOUS-STANDBY-NAMES\">\n              <code class=\"varname\">synchronous_standby_names</code></a>\n              setting. Synchronous replication can be enabled or\n              disabled on a per-transaction basis using the\n              <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-wal.html#GUC-SYNCHRONOUS-COMMIT\"><code class=\"varname\">\n              synchronous_commit</code></a> setting.</p>"
  ],
  [
    "<p>Add protocol support for sending file system\n              backups to standby servers using the streaming\n              replication network connection (Magnus Hagander,\n              Heikki Linnakangas)</p>",
    "<p>This avoids the requirement of manually\n              transferring a file system backup when setting up a\n              standby server.</p>"
  ],
  [
    "<p>Add <code class=\"varname\">replication_timeout</code> setting (Fujii\n              Masao, Heikki Linnakangas)</p>",
    "<p>Replication connections that are idle for more\n              than the <code class=\"varname\">replication_timeout</code> interval will be\n              terminated automatically. Formerly, a failed\n              connection was typically not detected until the TCP\n              timeout elapsed, which is inconveniently long in many\n              situations.</p>"
  ],
  [
    "<p>Add command-line tool <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/app-pgbasebackup.html\" title=\"pg_basebackup\"><span class=\"application\">pg_basebackup</span></a> for creating a\n              new standby server or database backup (Magnus\n              Hagander)</p>"
  ],
  [
    "<p>Add a <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-createrole.html\" title=\"CREATE ROLE\">replication permission</a> for\n              roles (Magnus Hagander)</p>",
    "<p>This is a read-only permission used for streaming\n              replication. It allows a non-superuser role to be\n              used for replication connections. Previously only\n              superusers could initiate replication connections;\n              superusers still have this permission by default.</p>"
  ],
  [
    "<p>Add system view <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/monitoring-stats.html#MONITORING-STATS-VIEWS-TABLE\" title=\"Table&#xA0;28.2.&#xA0;Collected Statistics Views\"><code class=\"structname\">\n              pg_stat_replication</code></a> which displays\n              activity of <acronym class=\"acronym\">WAL</acronym>\n              sender processes (Itagaki Takahiro, Simon Riggs)</p>",
    "<p>This reports the status of all connected standby\n              servers.</p>"
  ],
  [
    "<p>Add monitoring function <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-admin.html#FUNCTIONS-RECOVERY-INFO-TABLE\" title=\"Table&#xA0;9.80.&#xA0;Recovery Information Functions\">\n              <code class=\"function\">pg_last_xact_replay_timestamp()</code></a>\n              (Fujii Masao)</p>",
    "<p>This returns the time at which the primary\n              generated the most recent commit or abort record\n              applied on the standby.</p>"
  ],
  [
    "<p>Add configuration parameter <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-replication.html#GUC-HOT-STANDBY-FEEDBACK\">\n              <code class=\"varname\">hot_standby_feedback</code></a>\n              to enable standbys to postpone cleanup of old row\n              versions on the primary (Simon Riggs)</p>",
    "<p>This helps avoid canceling long-running queries on\n              the standby.</p>"
  ],
  [
    "<p>Add the <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/monitoring-stats.html#MONITORING-STATS-VIEWS-TABLE\" title=\"Table&#xA0;28.2.&#xA0;Collected Statistics Views\"><code class=\"structname\">\n              pg_stat_database_conflicts</code></a> system view to\n              show queries that have been canceled and the reason\n              (Magnus Hagander)</p>",
    "<p>Cancellations can occur because of dropped\n              tablespaces, lock timeouts, old snapshots, pinned\n              buffers, and deadlocks.</p>"
  ],
  [
    "<p>Add a <code class=\"structfield\">conflicts</code>\n              count to <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/monitoring-stats.html#MONITORING-STATS-VIEWS-TABLE\" title=\"Table&#xA0;28.2.&#xA0;Collected Statistics Views\"><code class=\"structname\">\n              pg_stat_database</code></a> (Magnus Hagander)</p>",
    "<p>This is the number of conflicts that occurred in\n              the database.</p>"
  ],
  [
    "<p>Increase the maximum values for <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-replication.html#GUC-MAX-STANDBY-ARCHIVE-DELAY\">\n              <code class=\"varname\">max_standby_archive_delay</code></a> and\n              <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-replication.html#GUC-MAX-STANDBY-STREAMING-DELAY\">\n              <code class=\"varname\">max_standby_streaming_delay</code></a></p>",
    "<p>The maximum value for each of these parameters was\n              previously only about 35 minutes. Much larger values\n              are now allowed.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/errcodes-appendix.html#ERRCODES-TABLE\" title=\"Table&#xA0;A.1.&#xA0;PostgreSQL Error Codes\"><code class=\"literal\">\n              ERRCODE_T_R_DATABASE_DROPPED</code></a> error code to\n              report recovery conflicts due to dropped databases\n              (Tatsuo Ishii)</p>",
    "<p>This is useful for connection pooling\n              software.</p>"
  ],
  [
    "<p>Add functions to control streaming replication\n              replay (Simon Riggs)</p>",
    "<p>The new functions are <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-admin.html#FUNCTIONS-RECOVERY-CONTROL-TABLE\" title=\"Table&#xA0;9.81.&#xA0;Recovery Control Functions\"><code class=\"function\">\n              pg_xlog_replay_pause()</code></a>, <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-admin.html#FUNCTIONS-RECOVERY-CONTROL-TABLE\" title=\"Table&#xA0;9.81.&#xA0;Recovery Control Functions\"><code class=\"function\">\n              pg_xlog_replay_resume()</code></a>, and the status\n              function <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-admin.html#FUNCTIONS-RECOVERY-CONTROL-TABLE\" title=\"Table&#xA0;9.81.&#xA0;Recovery Control Functions\"><code class=\"function\">\n              pg_is_xlog_replay_paused()</code></a>.</p>"
  ],
  [
    "<p>Add <code class=\"filename\">recovery.conf</code>\n              setting <code class=\"varname\">pause_at_recovery_target</code> to pause\n              recovery at target (Simon Riggs)</p>",
    "<p>This allows a recovery server to be queried to\n              check whether the recovery point is the one\n              desired.</p>"
  ],
  [
    "<p>Add the ability to create named restore points\n              using <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-admin.html#FUNCTIONS-ADMIN-BACKUP-TABLE\" title=\"Table&#xA0;9.79.&#xA0;Backup Control Functions\"><code class=\"function\">\n              pg_create_restore_point()</code></a> (Jaime\n              Casanova)</p>",
    "<p>These named restore points can be specified as\n              recovery targets using the new <code class=\"filename\">recovery.conf</code> setting <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/recovery-target-settings.html#RECOVERY-TARGET-NAME\"><code class=\"varname\">\n              recovery_target_name</code></a>.</p>"
  ],
  [
    "<p>Allow standby recovery to switch to a new timeline\n              automatically (Heikki Linnakangas)</p>",
    "<p>Now standby servers scan the archive directory for\n              new timelines periodically.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-error-handling.html#GUC-RESTART-AFTER-CRASH\">\n              <code class=\"varname\">restart_after_crash</code></a>\n              setting which disables automatic server restart after\n              a backend crash (Robert Haas)</p>",
    "<p>This allows external cluster management software\n              to control whether the database server restarts or\n              not.</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/recovery-config.html\" title=\"Chapter&#xA0;27.&#xA0;Recovery Configuration\"><code class=\"filename\">\n              recovery.conf</code></a> to use the same quoting\n              behavior as <code class=\"filename\">postgresql.conf</code> (Dimitri\n              Fontaine)</p>",
    "<p>Previously all values had to be quoted.</p>"
  ],
  [
    "<p>Add a true <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/transaction-iso.html#XACT-SERIALIZABLE\" title=\"13.2.3.&#xA0;Serializable Isolation Level\">serializable\n            isolation level</a> (Kevin Grittner, Dan Ports)</p>",
    "<p>Previously, asking for serializable isolation\n            guaranteed only that a single MVCC snapshot would be\n            used for the entire transaction, which allowed certain\n            documented anomalies. The old snapshot isolation\n            behavior is still available by requesting the <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/transaction-iso.html#XACT-REPEATABLE-READ\" title=\"13.2.2.&#xA0;Repeatable Read Isolation Level\"><code class=\"literal\">\n            REPEATABLE READ</code></a> isolation level.</p>"
  ],
  [
    "<p>Allow data-modification commands (<code class=\"command\">INSERT</code>/<code class=\"command\">UPDATE</code>/<code class=\"command\">DELETE</code>) in <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/queries-with.html\" title=\"7.8.&#xA0;WITH Queries (Common Table Expressions)\"><code class=\"literal\">\n            WITH</code></a> clauses (Marko Tiikkaja, Hitoshi\n            Harada)</p>",
    "<p>These commands can use <code class=\"literal\">RETURNING</code> to pass data up to the\n            containing query.</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/queries-with.html\" title=\"7.8.&#xA0;WITH Queries (Common Table Expressions)\">\n            <code class=\"literal\">WITH</code></a> clauses to be\n            attached to <code class=\"command\">INSERT</code>,\n            <code class=\"command\">UPDATE</code>, <code class=\"command\">DELETE</code> statements (Marko Tiikkaja,\n            Hitoshi Harada)</p>"
  ],
  [
    "<p>Allow non-<a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/queries-table-expressions.html#QUERIES-GROUP\" title=\"7.2.3.&#xA0;The GROUP BY and HAVING Clauses\"><code class=\"literal\">GROUP\n            BY</code></a> columns in the query target list when the\n            primary key is specified in the <code class=\"literal\">GROUP BY</code> clause (Peter Eisentraut)</p>",
    "<p>The SQL standard allows this behavior, and because\n            of the primary key, the result is unambiguous.</p>"
  ],
  [
    "<p>Allow use of the key word <code class=\"literal\">DISTINCT</code> in <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/queries-union.html\" title=\"7.4.&#xA0;Combining Queries\"><code class=\"literal\">UNION</code>/<code class=\"literal\">INTERSECT</code>/<code class=\"literal\">EXCEPT</code></a> clauses (Tom Lane)</p>",
    "<p><code class=\"literal\">DISTINCT</code> is the default\n            behavior so use of this key word is redundant, but the\n            SQL standard allows it.</p>"
  ],
  [
    "<p>Fix ordinary queries with rules to use the same\n            snapshot behavior as <code class=\"command\">EXPLAIN\n            ANALYZE</code> (Marko Tiikkaja)</p>",
    "<p>Previously <code class=\"command\">EXPLAIN\n            ANALYZE</code> used slightly different snapshot timing\n            for queries involving rules. The <code class=\"command\">EXPLAIN ANALYZE</code> behavior was judged to\n            be more logical.</p>"
  ],
  [
    "<p>Add per-column <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/collation.html\" title=\"23.2.&#xA0;Collation Support\">collation</a> support\n              (Peter Eisentraut, Tom Lane)</p>",
    "<p>Previously collation (the sort ordering of text\n              strings) could only be chosen at database creation.\n              Collation can now be set per column, domain, index,\n              or expression, via the SQL-standard <code class=\"literal\">COLLATE</code> clause.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/extend-extensions.html\" title=\"37.15.&#xA0;Packaging Related Objects into an Extension\">\n            extensions</a> which simplify packaging of additions to\n            <span class=\"productname\">PostgreSQL</span> (Dimitri\n            Fontaine, Tom Lane)</p>",
    "<p>Extensions are controlled by the new <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-createextension.html\" title=\"CREATE EXTENSION\"><code class=\"command\">CREATE</code></a>/<a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-alterextension.html\" title=\"ALTER EXTENSION\"><code class=\"command\">ALTER</code></a>/<a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-dropextension.html\" title=\"DROP EXTENSION\"><code class=\"command\">DROP\n            EXTENSION</code></a> commands. This replaces ad-hoc\n            methods of grouping objects that are added to a\n            <span class=\"productname\">PostgreSQL</span>\n            installation.</p>"
  ],
  [
    "<p>Add support for <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-createforeigntable.html\" title=\"CREATE FOREIGN TABLE\">foreign tables</a> (Shigeru\n            Hanada, Robert Haas, Jan Urbanski, Heikki\n            Linnakangas)</p>",
    "<p>This allows data stored outside the database to be\n            used like native <span class=\"productname\">PostgreSQL</span>-stored data. Foreign\n            tables are currently read-only, however.</p>"
  ],
  [
    "<p>Allow new values to be added to an existing enum\n            type via <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-altertype.html\" title=\"ALTER TYPE\"><code class=\"command\">ALTER\n            TYPE</code></a> (Andrew Dunstan)</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-altertype.html\" title=\"ALTER TYPE\"><code class=\"command\">ALTER TYPE ...\n            ADD/DROP/ALTER/RENAME ATTRIBUTE</code></a> (Peter\n            Eisentraut)</p>",
    "<p>This allows modification of composite types.</p>"
  ],
  [
    "<p>Add <code class=\"literal\">RESTRICT</code>/<code class=\"literal\">CASCADE</code> to <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-altertype.html\" title=\"ALTER TYPE\"><code class=\"command\">ALTER TYPE</code></a> operations on typed\n              tables (Peter Eisentraut)</p>",
    "<p>This controls <code class=\"literal\">ADD</code>/<code class=\"literal\">DROP</code>/<code class=\"literal\">ALTER</code>/<code class=\"literal\">RENAME\n              ATTRIBUTE</code> cascading behavior.</p>"
  ],
  [
    "<p>Support <code class=\"literal\">ALTER TABLE\n              <em class=\"replaceable\"><code>name</code></em> {OF |\n              NOT OF} <em class=\"replaceable\"><code>type</code></em></code> (Noah\n              Misch)</p>",
    "<p>This syntax allows a standalone table to be made\n              into a typed table, or a typed table to be made\n              standalone.</p>"
  ],
  [
    "<p>Add support for more object types in <code class=\"command\">ALTER ... SET SCHEMA</code> commands\n              (Dimitri Fontaine)</p>",
    "<p>This command is now supported for conversions,\n              operators, operator classes, operator families, text\n              search configurations, text search dictionaries, text\n              search parsers, and text search templates.</p>"
  ],
  [
    "<p>Add <code class=\"command\">ALTER TABLE ... ADD\n              UNIQUE/PRIMARY KEY USING INDEX</code> (Gurjeet\n              Singh)</p>",
    "<p>This allows a primary key or unique constraint to\n              be defined using an existing unique index, including\n              a concurrently created unique index.</p>"
  ],
  [
    "<p>Allow <code class=\"command\">ALTER TABLE</code> to\n              add foreign keys without validation (Simon Riggs)</p>",
    "<p>The new option is called <code class=\"literal\">NOT\n              VALID</code>. The constraint's state can later be\n              modified to <code class=\"literal\">VALIDATED</code>\n              and validation checks performed. Together these allow\n              you to add a foreign key with minimal impact on read\n              and write operations.</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-altertable.html\" title=\"ALTER TABLE\"><code class=\"command\">ALTER TABLE\n              ... SET DATA TYPE</code></a> to avoid table rewrites\n              in appropriate cases (Noah Misch, Robert Haas)</p>",
    "<p>For example, converting a <code class=\"type\">varchar</code> column to <code class=\"type\">text</code> no longer requires a rewrite of\n              the table. However, increasing the length constraint\n              on a <code class=\"type\">varchar</code> column still\n              requires a table rewrite.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-createtable.html\" title=\"CREATE TABLE\"><code class=\"command\">CREATE\n              TABLE IF NOT EXISTS</code></a> syntax (Robert\n              Haas)</p>",
    "<p>This allows table creation without causing an\n              error if the table already exists.</p>"
  ],
  [
    "<p>Fix possible <span class=\"quote\">&#x201C;<span class=\"quote\">tuple concurrently updated</span>&#x201D;</span>\n              error when two backends attempt to add an inheritance\n              child to the same table at the same time (Robert\n              Haas)</p>",
    "<p><a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-altertable.html\" title=\"ALTER TABLE\"><code class=\"command\">ALTER\n              TABLE</code></a> now takes a stronger lock on the\n              parent table, so that the sessions cannot try to\n              update it simultaneously.</p>"
  ],
  [
    "<p>Add a <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-security-label.html\" title=\"SECURITY LABEL\"><code class=\"command\">SECURITY\n              LABEL</code></a> command (KaiGai Kohei)</p>",
    "<p>This allows security labels to be assigned to\n              objects.</p>"
  ],
  [
    "<p>Add transaction-level <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/explicit-locking.html#ADVISORY-LOCKS\" title=\"13.3.5.&#xA0;Advisory Locks\">advisory locks</a> (Marko\n            Tiikkaja)</p>",
    "<p>These are similar to the existing session-level\n            advisory locks, but such locks are automatically\n            released at transaction end.</p>"
  ],
  [
    "<p>Make <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-truncate.html\" title=\"TRUNCATE\"><code class=\"command\">TRUNCATE ... RESTART\n            IDENTITY</code></a> restart sequences transactionally\n            (Steve Singer)</p>",
    "<p>Previously the counter could have been left out of\n            sync if a backend crashed between the on-commit\n            truncation activity and commit completion.</p>"
  ],
  [
    "<p>Add <code class=\"literal\">ENCODING</code> option\n              to <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-copy.html\" title=\"COPY\"><code class=\"command\">COPY TO/FROM</code></a>\n              (Hitoshi Harada, Itagaki Takahiro)</p>",
    "<p>This allows the encoding of the <code class=\"command\">COPY</code> file to be specified separately\n              from client encoding.</p>"
  ],
  [
    "<p>Add bidirectional <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-copy.html\" title=\"COPY\"><code class=\"command\">COPY</code></a> protocol support (Fujii\n              Masao)</p>",
    "<p>This is currently only used by streaming\n              replication.</p>"
  ],
  [
    "<p>Make <code class=\"command\">EXPLAIN VERBOSE</code>\n              show the function call expression in a <code class=\"literal\">FunctionScan</code> node (Tom Lane)</p>"
  ],
  [
    "<p>Add additional details to the output of <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-vacuum.html\" title=\"VACUUM\"><code class=\"command\">VACUUM FULL\n              VERBOSE</code></a> and <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-cluster.html\" title=\"CLUSTER\"><code class=\"command\">CLUSTER VERBOSE</code></a> (Itagaki\n              Takahiro)</p>",
    "<p>New information includes the live and dead tuple\n              count and whether <code class=\"command\">CLUSTER</code> is using an index to\n              rebuild.</p>"
  ],
  [
    "<p>Prevent <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/routine-vacuuming.html#AUTOVACUUM\" title=\"24.1.6.&#xA0;The Autovacuum Daemon\">autovacuum</a>\n              from waiting if it cannot acquire a table lock\n              (Robert Haas)</p>",
    "<p>It will try to vacuum that table later.</p>"
  ],
  [
    "<p>Allow <code class=\"command\">CLUSTER</code> to sort\n              the table rather than scanning the index when it\n              seems likely to be cheaper (Leonardo Francalanci)</p>"
  ],
  [
    "<p>Add nearest-neighbor (order-by-operator) searching\n              to <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/gist.html\" title=\"Chapter&#xA0;62.&#xA0;GiST Indexes\"><acronym class=\"acronym\">GiST</acronym> indexes</a> (Teodor Sigaev,\n              Tom Lane)</p>",
    "<p>This allows <acronym class=\"acronym\">GiST</acronym> indexes to quickly return\n              the <em class=\"replaceable\"><code>N</code></em>\n              closest values in a query with <code class=\"literal\">LIMIT</code>. For example</p>",
    "<pre class=\"programlisting\">\n              SELECT * FROM places ORDER BY location &lt;-&gt; point '(101,456)' LIMIT 10;</pre>",
    "<p>finds the ten places closest to a given target\n              point.</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/gin.html\" title=\"Chapter&#xA0;64.&#xA0;GIN Indexes\"><acronym class=\"acronym\">GIN</acronym> indexes</a> to index null and\n              empty values (Tom Lane)</p>",
    "<p>This allows full <acronym class=\"acronym\">GIN</acronym> index scans, and fixes\n              various corner cases in which GIN scans would\n              fail.</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/gin.html\" title=\"Chapter&#xA0;64.&#xA0;GIN Indexes\"><acronym class=\"acronym\">GIN</acronym> indexes</a> to better\n              recognize duplicate search entries (Tom Lane)</p>",
    "<p>This reduces the cost of index scans, especially\n              in cases where it avoids unnecessary full index\n              scans.</p>"
  ],
  [
    "<p>Fix <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/gist.html\" title=\"Chapter&#xA0;62.&#xA0;GiST Indexes\"><acronym class=\"acronym\">GiST</acronym> indexes</a> to be fully\n              crash-safe (Heikki Linnakangas)</p>",
    "<p>Previously there were rare cases where a\n              <code class=\"command\">REINDEX</code> would be\n              required (you would be informed).</p>"
  ],
  [
    "<p>Allow <code class=\"type\">numeric</code> to use a\n            more compact, two-byte header in common cases (Robert\n            Haas)</p>",
    "<p>Previously all <code class=\"type\">numeric</code>\n            values had four-byte headers; this change saves on disk\n            storage.</p>"
  ],
  [
    "<p>Add support for dividing <code class=\"type\">money</code> by <code class=\"type\">money</code>\n            (Andy Balholm)</p>"
  ],
  [
    "<p>Allow binary I/O on type <code class=\"type\">void</code> (Radoslaw Smogura)</p>"
  ],
  [
    "<p>Improve hypotenuse calculations for geometric\n            operators (Paul Matthews)</p>",
    "<p>This avoids unnecessary overflows, and may also be\n            more accurate.</p>"
  ],
  [
    "<p>Support hashing array values (Tom Lane)</p>",
    "<p>This provides additional query optimization\n            possibilities.</p>"
  ],
  [
    "<p>Don't treat a composite type as sortable unless all\n            its column types are sortable (Tom Lane)</p>",
    "<p>This avoids possible <span class=\"quote\">&#x201C;<span class=\"quote\">could not identify a\n            comparison function</span>&#x201D;</span> failures at runtime,\n            if it is possible to implement the query without\n            sorting. Also, <code class=\"command\">ANALYZE</code>\n            won't try to use inappropriate statistics-gathering\n            methods for columns of such composite types.</p>"
  ],
  [
    "<p>Add support for casting between <code class=\"type\">money</code> and <code class=\"type\">numeric</code> (Andy Balholm)</p>"
  ],
  [
    "<p>Add support for casting from <code class=\"type\">int4</code> and <code class=\"type\">int8</code>\n              to <code class=\"type\">money</code> (Joey Adams)</p>"
  ],
  [
    "<p>Allow casting a table's row type to the table's\n              supertype if it's a typed table (Peter\n              Eisentraut)</p>",
    "<p>This is analogous to the existing facility that\n              allows casting a row type to a supertable's row\n              type.</p>"
  ],
  [
    "<p>Add <acronym class=\"acronym\">XML</acronym>\n              function <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-xml.html#XML-EXISTS\" title=\"9.14.2.2.&#xA0;XMLEXISTS\"><code class=\"literal\">XMLEXISTS</code></a> and <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-xml.html#XML-EXISTS\" title=\"9.14.2.2.&#xA0;XMLEXISTS\"><code class=\"function\">xpath_exists()</code></a> functions (Mike\n              Fowler)</p>",
    "<p>These are used for XPath matching.</p>"
  ],
  [
    "<p>Add <acronym class=\"acronym\">XML</acronym>\n              functions <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-xml.html#XML-IS-WELL-FORMED\" title=\"9.14.2.3.&#xA0;xml_is_well_formed\"><code class=\"function\">xml_is_well_formed()</code></a>, <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-xml.html#XML-IS-WELL-FORMED\" title=\"9.14.2.3.&#xA0;xml_is_well_formed\"><code class=\"function\">\n              xml_is_well_formed_document()</code></a>, <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-xml.html#XML-IS-WELL-FORMED\" title=\"9.14.2.3.&#xA0;xml_is_well_formed\"><code class=\"function\">\n              xml_is_well_formed_content()</code></a> (Mike\n              Fowler)</p>",
    "<p>These check whether the input is properly-formed\n              <acronym class=\"acronym\">XML</acronym>. They provide\n              functionality that was previously available only in\n              the deprecated <code class=\"filename\">contrib/xml2</code> module.</p>"
  ],
  [
    "<p>Add SQL function <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-string.html#FORMAT\"><code class=\"function\">format(text, ...)</code></a>, which behaves\n            analogously to C's <code class=\"function\">printf()</code> (Pavel Stehule, Robert\n            Haas)</p>",
    "<p>It currently supports formats for strings, SQL\n            literals, and SQL identifiers.</p>"
  ],
  [
    "<p>Add string functions <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-string.html#FUNCTIONS-STRING-OTHER\" title=\"Table&#xA0;9.9.&#xA0;Other String Functions\"><code class=\"function\">\n            concat()</code></a>, <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-string.html#FUNCTIONS-STRING-OTHER\" title=\"Table&#xA0;9.9.&#xA0;Other String Functions\"><code class=\"function\">\n            concat_ws()</code></a>, <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-string.html#FUNCTIONS-STRING-OTHER\" title=\"Table&#xA0;9.9.&#xA0;Other String Functions\"><code class=\"function\">\n            left()</code></a>, <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-string.html#FUNCTIONS-STRING-OTHER\" title=\"Table&#xA0;9.9.&#xA0;Other String Functions\"><code class=\"function\">\n            right()</code></a>, and <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-string.html#FUNCTIONS-STRING-OTHER\" title=\"Table&#xA0;9.9.&#xA0;Other String Functions\"><code class=\"function\">\n            reverse()</code></a> (Pavel Stehule)</p>",
    "<p>These improve compatibility with other database\n            products.</p>"
  ],
  [
    "<p>Add function <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-admin.html#FUNCTIONS-ADMIN-GENFILE\" title=\"9.26.9.&#xA0;Generic File Access Functions\"><code class=\"function\">\n            pg_read_binary_file()</code></a> to read binary files\n            (Dimitri Fontaine, Itagaki Takahiro)</p>"
  ],
  [
    "<p>Add a single-parameter version of function <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-admin.html#FUNCTIONS-ADMIN-GENFILE\" title=\"9.26.9.&#xA0;Generic File Access Functions\"><code class=\"function\">\n            pg_read_file()</code></a> to read an entire file\n            (Dimitri Fontaine, Itagaki Takahiro)</p>"
  ],
  [
    "<p>Add three-parameter forms of <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-array.html#ARRAY-FUNCTIONS-TABLE\" title=\"Table&#xA0;9.49.&#xA0;Array Functions\"><code class=\"function\">array_to_string()</code></a> and <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-array.html#ARRAY-FUNCTIONS-TABLE\" title=\"Table&#xA0;9.49.&#xA0;Array Functions\"><code class=\"function\">string_to_array()</code></a> for null value\n            processing control (Pavel Stehule)</p>"
  ],
  [
    "<p>Add the <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-info.html#FUNCTIONS-INFO-CATALOG-TABLE\" title=\"Table&#xA0;9.63.&#xA0;System Catalog Information Functions\">\n              <code class=\"function\">pg_describe_object()</code></a> function\n              (Alvaro Herrera)</p>",
    "<p>This function is used to obtain a human-readable\n              string describing an object, based on the <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/catalog-pg-class.html\" title=\"51.11.&#xA0;pg_class\"><code class=\"structname\">pg_class</code></a> OID, object OID, and\n              sub-object ID. It can be used to help interpret the\n              contents of <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/catalog-pg-depend.html\" title=\"51.18.&#xA0;pg_depend\"><code class=\"structname\">pg_depend</code></a>.</p>"
  ],
  [
    "<p>Update comments for built-in operators and their\n              underlying functions (Tom Lane)</p>",
    "<p>Functions that are meant to be used via an\n              associated operator are now commented as such.</p>"
  ],
  [
    "<p>Add variable <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-compatible.html#GUC-QUOTE-ALL-IDENTIFIERS\">\n              <code class=\"varname\">quote_all_identifiers</code></a> to force\n              the quoting of all identifiers in <code class=\"command\">EXPLAIN</code> and in system catalog\n              functions like <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-info.html#FUNCTIONS-INFO-CATALOG-TABLE\" title=\"Table&#xA0;9.63.&#xA0;System Catalog Information Functions\">\n              <code class=\"function\">pg_get_viewdef()</code></a>\n              (Robert Haas)</p>",
    "<p>This makes exporting schemas to tools and other\n              databases with different quoting rules easier.</p>"
  ],
  [
    "<p>Add columns to the <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/infoschema-sequences.html\" title=\"36.42.&#xA0;sequences\"><code class=\"structname\">information_schema.sequences</code></a>\n              system view (Peter Eisentraut)</p>",
    "<p>Previously, though the view existed, the columns\n              about the sequence parameters were unimplemented.</p>"
  ],
  [
    "<p>Allow <code class=\"literal\">public</code> as a\n              pseudo-role name in <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-info.html#FUNCTIONS-INFO-ACCESS-TABLE\" title=\"Table&#xA0;9.61.&#xA0;Access Privilege Inquiry Functions\">\n              <code class=\"function\">has_table_privilege()</code></a> and\n              related functions (Alvaro Herrera)</p>",
    "<p>This allows checking for public permissions.</p>"
  ],
  [
    "<p>Support <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-createtrigger.html\" title=\"CREATE TRIGGER\"><code class=\"literal\">INSTEAD\n              OF</code></a> triggers on views (Dean Rasheed)</p>",
    "<p>This feature can be used to implement fully\n              updatable views.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/plpgsql-control-structures.html#PLPGSQL-FOREACH-ARRAY\" title=\"42.6.5.&#xA0;Looping Through Arrays\"><code class=\"command\">FOREACH IN ARRAY</code></a> to PL/pgSQL\n              (Pavel Stehule)</p>",
    "<p>This is more efficient and readable than previous\n              methods of iterating through the elements of an array\n              value.</p>"
  ],
  [
    "<p>Allow <code class=\"command\">RAISE</code> without\n              parameters to be caught in the same places that could\n              catch a <code class=\"command\">RAISE ERROR</code> from\n              the same location (Piyush Newe)</p>",
    "<p>The previous coding threw the error from the block\n              containing the active exception handler. The new\n              behavior is more consistent with other DBMS\n              products.</p>"
  ],
  [
    "<p>Allow generic record arguments to PL/Perl\n              functions (Andrew Dunstan)</p>",
    "<p>PL/Perl functions can now be declared to accept\n              type <code class=\"type\">record</code>. The behavior\n              is the same as for any named composite type.</p>"
  ],
  [
    "<p>Convert PL/Perl array arguments to Perl arrays\n              (Alexey Klyukin, Alex Hunsaker)</p>",
    "<p>String representations are still available.</p>"
  ],
  [
    "<p>Convert PL/Perl composite-type arguments to Perl\n              hashes (Alexey Klyukin, Alex Hunsaker)</p>",
    "<p>String representations are still available.</p>"
  ],
  [
    "<p>Add table function support for PL/Python (Jan\n              Urbanski)</p>",
    "<p>PL/Python can now return multiple <code class=\"literal\">OUT</code> parameters and record sets.</p>"
  ],
  [
    "<p>Add a validator to PL/Python (Jan Urbanski)</p>",
    "<p>This allows PL/Python functions to be\n              syntax-checked at function creation time.</p>"
  ],
  [
    "<p>Allow exceptions for SQL queries in PL/Python (Jan\n              Urbanski)</p>",
    "<p>This allows access to SQL-generated exception\n              error codes from PL/Python exception blocks.</p>"
  ],
  [
    "<p>Add explicit subtransactions to PL/Python (Jan\n              Urbanski)</p>"
  ],
  [
    "<p>Add PL/Python functions for quoting strings (Jan\n              Urbanski)</p>",
    "<p>These functions are <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/plpython-util.html\" title=\"45.9.&#xA0;Utility Functions\"><code class=\"literal\">plpy.quote_ident</code></a>, <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/plpython-util.html\" title=\"45.9.&#xA0;Utility Functions\"><code class=\"literal\">plpy.quote_literal</code></a>, and\n              <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/plpython-util.html\" title=\"45.9.&#xA0;Utility Functions\"><code class=\"literal\">plpy.quote_nullable</code></a>.</p>"
  ],
  [
    "<p>Add traceback information to PL/Python errors (Jan\n              Urbanski)</p>"
  ],
  [
    "<p>Report PL/Python errors from iterators with\n              <code class=\"literal\">PLy_elog</code> (Jan\n              Urbanski)</p>"
  ],
  [
    "<p>Fix exception handling with Python 3 (Jan\n              Urbanski)</p>",
    "<p>Exception classes were previously not available in\n              <code class=\"literal\">plpy</code> under Python 3.</p>"
  ],
  [
    "<p>Mark <span class=\"application\">createlang</span> and\n            <span class=\"application\">droplang</span> as deprecated\n            now that they just invoke extension commands (Tom\n            Lane)</p>"
  ],
  [
    "<p>Add <span class=\"application\">psql</span> command\n              <code class=\"literal\">\\conninfo</code> to show\n              current connection information (David\n              Christensen)</p>"
  ],
  [
    "<p>Add <span class=\"application\">psql</span> command\n              <code class=\"literal\">\\sf</code> to show a function's\n              definition (Pavel Stehule)</p>"
  ],
  [
    "<p>Add <span class=\"application\">psql</span> command\n              <code class=\"literal\">\\dL</code> to list languages\n              (Fernando Ike)</p>"
  ],
  [
    "<p>Add the <code class=\"option\">S</code>\n              (<span class=\"quote\">&#x201C;<span class=\"quote\">system</span>&#x201D;</span>) option to <span class=\"application\">psql</span>'s <code class=\"literal\">\\dn</code> (list schemas) command (Tom\n              Lane)</p>",
    "<p><code class=\"literal\">\\dn</code> without\n              <code class=\"literal\">S</code> now suppresses system\n              schemas.</p>"
  ],
  [
    "<p>Allow <span class=\"application\">psql</span>'s\n              <code class=\"literal\">\\e</code> and <code class=\"literal\">\\ef</code> commands to accept a line number\n              to be used to position the cursor in the editor\n              (Pavel Stehule)</p>",
    "<p>This is passed to the editor according to the\n              <code class=\"envar\">PSQL_EDITOR_LINENUMBER_ARG</code>\n              environment variable.</p>"
  ],
  [
    "<p>Have <span class=\"application\">psql</span> set the\n              client encoding from the operating system locale by\n              default (Heikki Linnakangas)</p>",
    "<p>This only happens if the <code class=\"envar\">PGCLIENTENCODING</code> environment variable\n              is not set.</p>"
  ],
  [
    "<p>Make <code class=\"literal\">\\d</code> distinguish\n              between unique indexes and unique constraints (Josh\n              Kupershmidt)</p>"
  ],
  [
    "<p>Make <code class=\"literal\">\\dt+</code> report\n              <code class=\"function\">pg_table_size</code> instead\n              of <code class=\"function\">pg_relation_size</code>\n              when talking to 9.0 or later servers (Bernd\n              Helmle)</p>",
    "<p>This is a more useful measure of table size, but\n              note that it is not identical to what was previously\n              reported in the same display.</p>"
  ],
  [
    "<p>Additional tab completion support (Itagaki\n              Takahiro, Pavel Stehule, Andrey Popp, Christoph Berg,\n              David Fetter, Josh Kupershmidt)</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/app-pgdump.html\" title=\"pg_dump\"><span class=\"application\">pg_dump</span></a> and <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/app-pg-dumpall.html\" title=\"pg_dumpall\"><span class=\"application\">pg_dumpall</span></a> option\n              <code class=\"option\">--quote-all-identifiers</code>\n              to force quoting of all identifiers (Robert Haas)</p>"
  ],
  [
    "<p>Add <code class=\"literal\">directory</code> format\n              to <span class=\"application\">pg_dump</span> (Joachim\n              Wieland, Heikki Linnakangas)</p>",
    "<p>This is internally similar to the <code class=\"literal\">tar</code> <span class=\"application\">pg_dump</span> format.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_ctl</span> so it\n              no longer incorrectly reports that the server is not\n              running (Bruce Momjian)</p>",
    "<p>Previously this could happen if the server was\n              running but <span class=\"application\">pg_ctl</span>\n              could not authenticate.</p>"
  ],
  [
    "<p>Improve <span class=\"application\">pg_ctl</span>\n              start's <span class=\"quote\">&#x201C;<span class=\"quote\">wait</span>&#x201D;</span> (<code class=\"option\">-w</code>) option (Bruce Momjian, Tom\n              Lane)</p>",
    "<p>The wait mode is now significantly more robust. It\n              will not get confused by non-default postmaster port\n              numbers, non-default Unix-domain socket locations,\n              permission problems, or stale postmaster lock\n              files.</p>"
  ],
  [
    "<p>Add <code class=\"literal\">promote</code> option to\n              <span class=\"application\">pg_ctl</span> to switch a\n              standby server to primary (Fujii Masao)</p>"
  ],
  [
    "<p>Add a libpq connection option <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/libpq-connect.html#LIBPQ-CONNECT-CLIENT-ENCODING\">\n              <code class=\"literal\">client_encoding</code></a>\n              which behaves like the <code class=\"envar\">PGCLIENTENCODING</code> environment variable\n              (Heikki Linnakangas)</p>",
    "<p>The value <code class=\"literal\">auto</code> sets\n              the client encoding based on the operating system\n              locale.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/libpq-misc.html#LIBPQ-PQLIBVERSION\"><code class=\"function\">PQlibVersion()</code></a> function which\n              returns the libpq library version (Magnus\n              Hagander)</p>",
    "<p>libpq already had <code class=\"function\">PQserverVersion()</code> which returns the\n              server version.</p>"
  ],
  [
    "<p>Allow libpq-using clients to check the user name\n              of the server process when connecting via Unix-domain\n              sockets, with the new <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/libpq-connect.html#LIBPQ-CONNECT-REQUIREPEER\"><code class=\"literal\">\n              requirepeer</code></a> connection option (Peter\n              Eisentraut)</p>",
    "<p><span class=\"productname\">PostgreSQL</span>\n              already allowed servers to check the client user name\n              when connecting via Unix-domain sockets.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/libpq-connect.html#LIBPQ-PQPING\"><code class=\"function\">PQping()</code></a> and <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/libpq-connect.html#LIBPQ-PQPINGPARAMS\"><code class=\"function\">PQpingParams()</code></a> to libpq (Bruce\n              Momjian, Tom Lane)</p>",
    "<p>These functions allow detection of the server's\n              status without trying to open a new session.</p>"
  ],
  [
    "<p>Allow ECPG to accept dynamic cursor names even in\n              <code class=\"literal\">WHERE CURRENT OF</code> clauses\n              (Zoltan Boszormenyi)</p>"
  ],
  [
    "<p>Make <span class=\"application\">ecpglib</span>\n              write <code class=\"type\">double</code> values with a\n              precision of 15 digits, not 14 as formerly (Akira\n              Kurosawa)</p>"
  ],
  [
    "<p>Use <code class=\"literal\">+Olibmerrno</code> compile\n            flag with HP-UX C compilers that accept it (Ibrar\n            Ahmed)</p>",
    "<p>This avoids possible misbehavior of math library\n            calls on recent HP platforms.</p>"
  ],
  [
    "<p>Improved parallel make support (Peter\n              Eisentraut)</p>",
    "<p>This allows for faster compiles. Also,\n              <code class=\"literal\">make -k</code> now works more\n              consistently.</p>"
  ],
  [
    "<p>Require <acronym class=\"acronym\">GNU</acronym>\n              <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/install-requirements.html\" title=\"16.2.&#xA0;Requirements\"><span class=\"application\">make</span></a> 3.80 or newer (Peter\n              Eisentraut)</p>",
    "<p>This is necessary because of the parallel-make\n              improvements.</p>"
  ],
  [
    "<p>Add <code class=\"literal\">make\n              maintainer-check</code> target (Peter Eisentraut)</p>",
    "<p>This target performs various source code checks\n              that are not appropriate for either the build or the\n              regression tests. Currently: duplicate_oids, SGML\n              syntax and tabs check, NLS syntax check.</p>"
  ],
  [
    "<p>Support <code class=\"literal\">make check</code> in\n              <code class=\"filename\">contrib</code> (Peter\n              Eisentraut)</p>",
    "<p>Formerly only <code class=\"literal\">make\n              installcheck</code> worked, but now there is support\n              for testing in a temporary installation. The\n              top-level <code class=\"literal\">make\n              check-world</code> target now includes testing\n              <code class=\"filename\">contrib</code> this way.</p>"
  ],
  [
    "<p>On Windows, allow <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/app-pg-ctl.html\" title=\"pg_ctl\"><span class=\"application\">pg_ctl</span></a> to register the\n              service as auto-start or start-on-demand (Quan\n              Zongliang)</p>"
  ],
  [
    "<p>Add support for collecting <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/installation-platform-notes.html#WINDOWS-CRASH-DUMPS\" title=\"16.7.4.1.&#xA0;Collecting Crash Dumps on Windows\">crash\n              dumps</a> on Windows (Craig Ringer, Magnus\n              Hagander)</p>",
    "<p><span class=\"productname\">minidumps</span> can now\n              be generated by non-debug Windows binaries and\n              analyzed by standard debugging tools.</p>"
  ],
  [
    "<p>Enable building with the MinGW64 compiler (Andrew\n              Dunstan)</p>",
    "<p>This allows building 64-bit Windows binaries even\n              on non-Windows platforms via cross-compiling.</p>"
  ],
  [
    "<p>Revise the API for GUC variable assign hooks (Tom\n            Lane)</p>",
    "<p>The previous functions of assign hooks are now split\n            between check hooks and assign hooks, where the former\n            can fail but the latter shouldn't. This change will\n            impact add-on modules that define custom GUC\n            parameters.</p>"
  ],
  [
    "<p>Add latches to the source code to support waiting\n            for events (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Centralize data modification permissions-checking\n            logic (KaiGai Kohei)</p>"
  ],
  [
    "<p>Add missing <code class=\"function\">get_<em class=\"replaceable\"><code>object</code></em>_oid()</code>\n            functions, for consistency (Robert Haas)</p>"
  ],
  [
    "<p>Improve ability to use C++ compilers for <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/xfunc-c.html\" title=\"37.9.&#xA0;C-Language Functions\">compiling add-on\n            modules</a> by removing conflicting key words (Tom\n            Lane)</p>"
  ],
  [
    "<p>Add support for DragonFly <acronym class=\"acronym\">BSD</acronym> (Rumko)</p>"
  ],
  [
    "<p>Expose <code class=\"function\">quote_literal_cstr()</code> for backend use\n            (Robert Haas)</p>"
  ],
  [
    "<p>Run <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/install-procedure.html#BUILD\" title=\"Build\">regression\n            tests</a> in the default encoding (Peter\n            Eisentraut)</p>",
    "<p>Regression tests were previously always run with\n            <code class=\"literal\">SQL_ASCII</code> encoding.</p>"
  ],
  [
    "<p>Add <span class=\"application\">src/tools/git_changelog</span> to replace\n            <span class=\"application\">cvs2cl</span> and\n            <span class=\"application\">pgcvslog</span> (Robert Haas,\n            Tom Lane)</p>"
  ],
  [
    "<p>Add <span class=\"application\">git-external-diff</span> script to\n            <code class=\"filename\">src/tools</code> (Bruce\n            Momjian)</p>",
    "<p>This is used to generate context diffs from git.</p>"
  ],
  [
    "<p>Improve support for building with <span class=\"application\">Clang</span> (Peter Eisentraut)</p>"
  ],
  [
    "<p>Add source code hooks to check permissions (Robert\n              Haas, Stephen Frost)</p>"
  ],
  [
    "<p>Add post-object-creation function hooks for use by\n              security frameworks (KaiGai Kohei)</p>"
  ],
  [
    "<p>Add a client authentication hook (KaiGai\n              Kohei)</p>"
  ],
  [
    "<p>Modify <code class=\"filename\">contrib</code> modules\n            and procedural languages to install via the new\n            <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/extend-extensions.html\" title=\"37.15.&#xA0;Packaging Related Objects into an Extension\">\n            extension</a> mechanism (Tom Lane, Dimitri\n            Fontaine)</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/file-fdw.html\" title=\"F.15.&#xA0;file_fdw\"><code class=\"filename\">contrib/file_fdw</code></a> foreign-data\n            wrapper (Shigeru Hanada)</p>",
    "<p>Foreign tables using this foreign data wrapper can\n            read flat files in a manner very similar to\n            <code class=\"command\">COPY</code>.</p>"
  ],
  [
    "<p>Add nearest-neighbor search support to <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/pgtrgm.html\" title=\"F.32.&#xA0;pg_trgm\"><code class=\"filename\">contrib/pg_trgm</code></a> and <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/btree-gist.html\" title=\"F.7.&#xA0;btree_gist\"><code class=\"filename\">contrib/btree_gist</code></a> (Teodor\n            Sigaev)</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/btree-gist.html\" title=\"F.7.&#xA0;btree_gist\"><code class=\"filename\">contrib/btree_gist</code></a> support for\n            searching on not-equals (Jeff Davis)</p>"
  ],
  [
    "<p>Fix <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/fuzzystrmatch.html\" title=\"F.16.&#xA0;fuzzystrmatch\"><code class=\"filename\">contrib/fuzzystrmatch</code></a>'s\n            <code class=\"function\">levenshtein()</code> function to\n            handle multibyte characters (Alexander Korotkov)</p>"
  ],
  [
    "<p>Add <code class=\"function\">ssl_cipher()</code> and\n            <code class=\"function\">ssl_version()</code> functions\n            to <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sslinfo.html\" title=\"F.38.&#xA0;sslinfo\"><code class=\"filename\">contrib/sslinfo</code></a> (Robert Haas)</p>"
  ],
  [
    "<p>Fix <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/intarray.html\" title=\"F.19.&#xA0;intarray\"><code class=\"filename\">contrib/intarray</code></a> and <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/hstore.html\" title=\"F.17.&#xA0;hstore\"><code class=\"filename\">contrib/hstore</code></a> to give consistent\n            results with indexed empty arrays (Tom Lane)</p>",
    "<p>Previously an empty-array query that used an index\n            might return different results from one that used a\n            sequential scan.</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/intarray.html\" title=\"F.19.&#xA0;intarray\"><code class=\"filename\">contrib/intarray</code></a> to work properly\n            on multidimensional arrays (Tom Lane)</p>"
  ],
  [
    "<p>In <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/intarray.html\" title=\"F.19.&#xA0;intarray\"><code class=\"filename\">contrib/intarray</code></a>, avoid errors\n            complaining about the presence of nulls in cases where\n            no nulls are actually present (Tom Lane)</p>"
  ],
  [
    "<p>In <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/intarray.html\" title=\"F.19.&#xA0;intarray\"><code class=\"filename\">contrib/intarray</code></a>, fix behavior of\n            containment operators with respect to empty arrays (Tom\n            Lane)</p>",
    "<p>Empty arrays are now correctly considered to be\n            contained in any other array.</p>"
  ],
  [
    "<p>Remove <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/xml2.html\" title=\"F.46.&#xA0;xml2\"><code class=\"filename\">contrib/xml2</code></a>'s arbitrary limit on\n            the number of <em class=\"replaceable\"><code>parameter</code></em>=<em class=\"replaceable\"><code>value</code></em> pairs that can be\n            handled by <code class=\"function\">xslt_process()</code>\n            (Pavel Stehule)</p>",
    "<p>The previous limit was 10.</p>"
  ],
  [
    "<p>In <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/pageinspect.html\" title=\"F.23.&#xA0;pageinspect\"><code class=\"filename\">contrib/pageinspect</code></a>, fix\n            heap_page_item to return infomasks as 32-bit values\n            (Alvaro Herrera)</p>",
    "<p>This avoids returning negative values, which was\n            confusing. The underlying value is a 16-bit unsigned\n            integer.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sepgsql.html\" title=\"F.36.&#xA0;sepgsql\"><code class=\"filename\">contrib/sepgsql</code></a> to interface\n              permission checks with <acronym class=\"acronym\">SELinux</acronym> (KaiGai Kohei)</p>",
    "<p>This uses the new <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-security-label.html\" title=\"SECURITY LABEL\"><code class=\"command\">SECURITY\n              LABEL</code></a> facility.</p>"
  ],
  [
    "<p>Add contrib module <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/auth-delay.html\" title=\"F.3.&#xA0;auth_delay\"><code class=\"filename\">auth_delay</code></a> (KaiGai Kohei)</p>",
    "<p>This causes the server to pause before returning\n              authentication failure; it is designed to make brute\n              force password attacks more difficult.</p>"
  ],
  [
    "<p>Add <code class=\"filename\">dummy_seclabel</code>\n              contrib module (KaiGai Kohei)</p>",
    "<p>This is used for permission regression\n              testing.</p>"
  ],
  [
    "<p>Add support for <code class=\"literal\">LIKE</code>\n              and <code class=\"literal\">ILIKE</code> index searches\n              to <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/pgtrgm.html\" title=\"F.32.&#xA0;pg_trgm\"><code class=\"filename\">contrib/pg_trgm</code></a> (Alexander\n              Korotkov)</p>"
  ],
  [
    "<p>Add <code class=\"function\">levenshtein_less_equal()</code> function\n              to <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/fuzzystrmatch.html\" title=\"F.16.&#xA0;fuzzystrmatch\"><code class=\"filename\">contrib/fuzzystrmatch</code></a>, which is\n              optimized for small distances (Alexander\n              Korotkov)</p>"
  ],
  [
    "<p>Improve performance of index lookups on <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/seg.html\" title=\"F.35.&#xA0;seg\"><code class=\"filename\">contrib/seg</code></a> columns (Alexander\n              Korotkov)</p>"
  ],
  [
    "<p>Improve performance of <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/pgupgrade.html\" title=\"pg_upgrade\"><span class=\"application\">pg_upgrade</span></a> for databases\n              with many relations (Bruce Momjian)</p>"
  ],
  [
    "<p>Add flag to <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/pgbench.html\" title=\"pgbench\"><code class=\"filename\">contrib/pgbench</code></a> to report\n              per-statement latencies (Florian Pflug)</p>"
  ],
  [
    "<p>Move <code class=\"filename\">src/tools/test_fsync</code> to <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/pgtestfsync.html\" title=\"pg_test_fsync\"><code class=\"filename\">contrib/pg_test_fsync</code></a> (Bruce\n              Momjian, Tom Lane)</p>"
  ],
  [
    "<p>Add <code class=\"literal\">O_DIRECT</code> support\n              to <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/pgtestfsync.html\" title=\"pg_test_fsync\"><code class=\"filename\">contrib/pg_test_fsync</code></a> (Bruce\n              Momjian)</p>",
    "<p>This matches the use of <code class=\"literal\">O_DIRECT</code> by <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-wal.html#GUC-WAL-SYNC-METHOD\"><code class=\"varname\">\n              wal_sync_method</code></a>.</p>"
  ],
  [
    "<p>Add new tests to <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/pgtestfsync.html\" title=\"pg_test_fsync\"><code class=\"filename\">contrib/pg_test_fsync</code></a> (Bruce\n              Momjian)</p>"
  ],
  [
    "<p>Extensive <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/ecpg.html\" title=\"Chapter&#xA0;35.&#xA0;ECPG - Embedded SQL in C\"><span class=\"application\">\n            ECPG</span></a> documentation improvements (Satoshi\n            Nagayasu)</p>"
  ],
  [
    "<p>Extensive proofreading and documentation\n            improvements (Thom Brown, Josh Kupershmidt, Susanne\n            Ebrecht)</p>"
  ],
  [
    "<p>Add documentation for <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-error-handling.html#GUC-EXIT-ON-ERROR\"><code class=\"varname\">\n            exit_on_error</code></a> (Robert Haas)</p>",
    "<p>This parameter causes sessions to exit on any\n            error.</p>"
  ],
  [
    "<p>Add documentation for <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-info.html#FUNCTIONS-INFO-CATALOG-TABLE\" title=\"Table&#xA0;9.63.&#xA0;System Catalog Information Functions\">\n            <code class=\"function\">pg_options_to_table()</code></a>\n            (Josh Berkus)</p>",
    "<p>This function shows table storage options in a\n            readable form.</p>"
  ],
  [
    "<p>Document that it is possible to access all composite\n            type fields using <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-expressions.html#FIELD-SELECTION\" title=\"4.2.4.&#xA0;Field Selection\"><code class=\"literal\">(compositeval).*</code></a> syntax (Peter\n            Eisentraut)</p>"
  ],
  [
    "<p>Document that <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-string.html#FUNCTIONS-STRING-OTHER\" title=\"Table&#xA0;9.9.&#xA0;Other String Functions\"><code class=\"function\">\n            translate()</code></a> removes characters in\n            <code class=\"literal\">from</code> that don't have a\n            corresponding <code class=\"literal\">to</code> character\n            (Josh Kupershmidt)</p>"
  ],
  [
    "<p>Merge documentation for <code class=\"command\">CREATE\n            CONSTRAINT TRIGGER</code> and <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-createtrigger.html\" title=\"CREATE TRIGGER\"><code class=\"command\">CREATE\n            TRIGGER</code></a> (Alvaro Herrera)</p>"
  ],
  [
    "<p>Centralize <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/ddl-priv.html\" title=\"5.6.&#xA0;Privileges\">permission</a> and\n            <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/upgrading.html\" title=\"18.6.&#xA0;Upgrading a PostgreSQL Cluster\">upgrade</a>\n            documentation (Bruce Momjian)</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/kernel-resources.html#SYSVIPC-PARAMETERS\" title=\"Table&#xA0;18.1.&#xA0;System V IPC Parameters\">kernel\n            tuning documentation</a> for Solaris 10 (Josh\n            Berkus)</p>",
    "<p>Previously only Solaris 9 kernel tuning was\n            documented.</p>"
  ],
  [
    "<p>Handle non-ASCII characters consistently in\n            <code class=\"filename\">HISTORY</code> file (Peter\n            Eisentraut)</p>",
    "<p>While the <code class=\"filename\">HISTORY</code> file\n            is in English, we do have to deal with non-ASCII\n            letters in contributor names. These are now\n            transliterated so that they are reasonably legible\n            without assumptions about character set.</p>"
  ]
]