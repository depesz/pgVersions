[
  [
    "<p>Improve handling of URLs in <code class=\"function\">headline()</code> function (Teodor)</p>"
  ],
  [
    "<p>Improve handling of overlength headlines in\n          <code class=\"function\">headline()</code> function\n          (Teodor)</p>"
  ],
  [
    "<p>Prevent possible Assert failure or misconversion if an\n          encoding conversion is created with the wrong conversion\n          function for the specified pair of encodings (Tom,\n          Heikki)</p>"
  ],
  [
    "<p>Avoid unnecessary locking of small tables in\n          <code class=\"command\">VACUUM</code> (Heikki)</p>"
  ],
  [
    "<p>Fix uninitialized variables in <code class=\"filename\">contrib/tsearch2</code>'s <code class=\"function\">get_covers()</code> function (Teodor)</p>"
  ],
  [
    "<p>Fix bug in <code class=\"function\">to_char()</code>'s\n          handling of <code class=\"literal\">TH</code> format codes\n          (Andreas Scherbaum)</p>"
  ],
  [
    "<p>Make all documentation reference <code class=\"literal\">pgsql-bugs</code> and/or <code class=\"literal\">pgsql-hackers</code> as appropriate, instead of\n          the now-decommissioned <code class=\"literal\">pgsql-ports</code> and <code class=\"literal\">pgsql-patches</code> mailing lists (Tom)</p>"
  ]
]