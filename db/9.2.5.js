[
  [
    "<p>Prevent corruption of multi-byte characters when\n          attempting to case-fold identifiers (Andrew Dunstan)</p>",
    "<p><span class=\"productname\">PostgreSQL</span> case-folds\n          non-ASCII characters only when using a single-byte server\n          encoding.</p>"
  ],
  [
    "<p>Fix memory leak when creating B-tree indexes on range\n          columns (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Fix checkpoint memory leak in background writer when\n          <code class=\"literal\">wal_level = hot_standby</code>\n          (Naoya Anzai)</p>"
  ],
  [
    "<p>Fix memory leak caused by <code class=\"function\">lo_open()</code> failure (Heikki\n          Linnakangas)</p>"
  ],
  [
    "<p>Fix memory overcommit bug when <code class=\"varname\">work_mem</code> is using more than 24GB of\n          memory (Stephen Frost)</p>"
  ],
  [
    "<p>Serializable snapshot fixes (Kevin Grittner, Heikki\n          Linnakangas)</p>"
  ],
  [
    "<p>Fix deadlock bug in libpq when using SSL (Stephen\n          Frost)</p>"
  ],
  [
    "<p>Fix possible SSL state corruption in threaded libpq\n          applications (Nick Phillips, Stephen Frost)</p>"
  ],
  [
    "<p>Improve estimate of planner cost when choosing between\n          generic and custom plans (Tom Lane)</p>",
    "<p>This change will favor generic plans when planning\n          cost is high.</p>"
  ],
  [
    "<p>Properly compute row estimates for boolean columns\n          containing many NULL values (Andrew Gierth)</p>",
    "<p>Previously tests like <code class=\"literal\">col IS NOT\n          TRUE</code> and <code class=\"literal\">col IS NOT\n          FALSE</code> did not properly factor in NULL values when\n          estimating plan costs.</p>"
  ],
  [
    "<p>Fix accounting for qualifier evaluation costs in\n          <code class=\"literal\">UNION ALL</code> and inheritance\n          queries (Tom Lane)</p>",
    "<p>This fixes cases where suboptimal query plans could be\n          chosen if some <code class=\"literal\">WHERE</code> clauses\n          are expensive to calculate.</p>"
  ],
  [
    "<p>Prevent pushing down <code class=\"literal\">WHERE</code> clauses into unsafe <code class=\"literal\">UNION/INTERSECT</code> subqueries (Tom\n          Lane)</p>",
    "<p>Subqueries of a <code class=\"literal\">UNION</code> or\n          <code class=\"literal\">INTERSECT</code> that contain\n          set-returning functions or volatile functions in their\n          <code class=\"literal\">SELECT</code> lists could be\n          improperly optimized, leading to run-time errors or\n          incorrect query results.</p>"
  ],
  [
    "<p>Fix rare case of <span class=\"quote\">&#x201C;<span class=\"quote\">failed to locate grouping columns</span>&#x201D;</span>\n          planner failure (Tom Lane)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_dump</span> of\n          foreign tables with dropped columns (Andrew Dunstan)</p>",
    "<p>Previously such cases could cause a <span class=\"application\">pg_upgrade</span> error.</p>"
  ],
  [
    "<p>Reorder <span class=\"application\">pg_dump</span>\n          processing of extension-related rules and event triggers\n          (Joe Conway)</p>"
  ],
  [
    "<p>Force dumping of extension tables if specified by\n          <code class=\"command\">pg_dump -t</code> or <code class=\"literal\">-n</code> (Joe Conway)</p>"
  ],
  [
    "<p>Improve view dumping code's handling of dropped\n          columns in referenced tables (Tom Lane)</p>"
  ],
  [
    "<p>Fix <code class=\"command\">pg_restore -l</code> with\n          the directory archive to display the correct format name\n          (Fujii Masao)</p>"
  ],
  [
    "<p>Properly record index comments created using\n          <code class=\"literal\">UNIQUE</code> and <code class=\"literal\">PRIMARY KEY</code> syntax (Andres Freund)</p>",
    "<p>This fixes a parallel <span class=\"application\">pg_restore</span> failure.</p>"
  ],
  [
    "<p>Cause <code class=\"command\">pg_basebackup -x</code>\n          with an empty xlog directory to throw an error rather\n          than crashing (Magnus Hagander, Haruka Takatsuka)</p>"
  ],
  [
    "<p>Properly guarantee transmission of WAL files before\n          clean switchover (Fujii Masao)</p>",
    "<p>Previously, the streaming replication connection might\n          close before all WAL files had been replayed on the\n          standby.</p>"
  ],
  [
    "<p>Fix WAL segment timeline handling during recovery\n          (Mitsumasa Kondo, Heikki Linnakangas)</p>",
    "<p>WAL file recycling during standby recovery could lead\n          to premature recovery completion, resulting in data\n          loss.</p>"
  ],
  [
    "<p>Prevent errors in WAL replay due to references to\n          uninitialized empty pages (Andres Freund)</p>"
  ],
  [
    "<p>Fix <code class=\"command\">REINDEX TABLE</code> and\n          <code class=\"command\">REINDEX DATABASE</code> to properly\n          revalidate constraints and mark invalidated indexes as\n          valid (Noah Misch)</p>",
    "<p><code class=\"command\">REINDEX INDEX</code> has always\n          worked properly.</p>"
  ],
  [
    "<p>Avoid deadlocks during insertion into SP-GiST indexes\n          (Teodor Sigaev)</p>"
  ],
  [
    "<p>Fix possible deadlock during concurrent <code class=\"command\">CREATE INDEX CONCURRENTLY</code> operations\n          (Tom Lane)</p>"
  ],
  [
    "<p>Fix GiST index lookup crash (Tom Lane)</p>"
  ],
  [
    "<p>Fix <code class=\"function\">regexp_matches()</code>\n          handling of zero-length matches (Jeevan Chalke)</p>",
    "<p>Previously, zero-length matches like '^' could return\n          too many matches.</p>"
  ],
  [
    "<p>Fix crash for overly-complex regular expressions\n          (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Fix regular expression match failures for back\n          references combined with non-greedy quantifiers (Jeevan\n          Chalke)</p>"
  ],
  [
    "<p>Prevent <code class=\"command\">CREATE FUNCTION</code>\n          from checking <code class=\"command\">SET</code> variables\n          unless function body checking is enabled (Tom Lane)</p>"
  ],
  [
    "<p>Allow <code class=\"command\">ALTER DEFAULT\n          PRIVILEGES</code> to operate on schemas without requiring\n          CREATE permission (Tom Lane)</p>"
  ],
  [
    "<p>Loosen restriction on keywords used in queries (Tom\n          Lane)</p>",
    "<p>Specifically, lessen keyword restrictions for role\n          names, language names, <code class=\"command\">EXPLAIN</code> and <code class=\"command\">COPY</code> options, and <code class=\"command\">SET</code> values. This allows <code class=\"literal\">COPY ... (FORMAT BINARY)</code> to work as\n          expected; previously <code class=\"literal\">BINARY</code>\n          needed to be quoted.</p>"
  ],
  [
    "<p>Print proper line number during <code class=\"command\">COPY</code> failure (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Fix <code class=\"function\">pgp_pub_decrypt()</code> so\n          it works for secret keys with passwords (Marko Kreen)</p>"
  ],
  [
    "<p>Make <span class=\"application\">pg_upgrade</span> use\n          <code class=\"literal\">pg_dump\n          --quote-all-identifiers</code> to avoid problems with\n          keyword changes between releases (Tom Lane)</p>"
  ],
  [
    "<p>Remove rare inaccurate warning during vacuum of\n          index-less tables (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Ensure that <code class=\"command\">VACUUM\n          ANALYZE</code> still runs the ANALYZE phase if its\n          attempt to truncate the file is cancelled due to lock\n          conflicts (Kevin Grittner)</p>"
  ],
  [
    "<p>Avoid possible failure when performing transaction\n          control commands (e.g <code class=\"command\">ROLLBACK</code>) in prepared queries (Tom\n          Lane)</p>"
  ],
  [
    "<p>Ensure that floating-point data input accepts standard\n          spellings of <span class=\"quote\">&#x201C;<span class=\"quote\">infinity</span>&#x201D;</span> on all platforms (Tom\n          Lane)</p>",
    "<p>The C99 standard says that allowable spellings are\n          <code class=\"literal\">inf</code>, <code class=\"literal\">+inf</code>, <code class=\"literal\">-inf</code>,\n          <code class=\"literal\">infinity</code>, <code class=\"literal\">+infinity</code>, and <code class=\"literal\">-infinity</code>. Make sure we recognize these\n          even if the platform's <code class=\"function\">strtod</code> function doesn't.</p>"
  ],
  [
    "<p>Avoid unnecessary reporting when <code class=\"varname\">track_activities</code> is off (Tom Lane)</p>"
  ],
  [
    "<p>Expand ability to compare rows to records and arrays\n          (Rafal Rzepecki, Tom Lane)</p>"
  ],
  [
    "<p>Prevent crash when <span class=\"application\">psql</span>'s <code class=\"envar\">PSQLRC</code> variable contains a tilde (Bruce\n          Momjian)</p>"
  ],
  [
    "<p>Add spinlock support for ARM64 (Mark Salter)</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2013d for DST law\n          changes in Israel, Morocco, Palestine, and Paraguay.\n          Also, historical zone data corrections for Macquarie\n          Island.</p>"
  ]
]