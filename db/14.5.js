[
  [
    "<p>Do not let extension scripts replace objects not already belonging to the extension (Tom Lane)</p>",
    "<p>This change prevents extension scripts from doing <code class=\"command\">CREATE OR REPLACE</code> if there is an existing object that does not belong to the extension. It also prevents <code class=\"command\">CREATE IF NOT EXISTS</code> in the same situation. This prevents a form of trojan-horse attack in which a hostile database user could become the owner of an extension object and then modify it to compromise future uses of the object by other users. As a side benefit, it also reduces the risk of accidentally replacing objects one did not mean to.</p>",
    "<p>The <span class=\"productname\">PostgreSQL</span> Project thanks Sven Klemm for reporting this problem. (CVE-2022-2625)</p>"
  ],
  [
    "<p>Fix replay of <code class=\"command\">CREATE DATABASE</code> WAL records on standby servers (Kyotaro Horiguchi, Asim R Praveen, Paul Guo)</p>",
    "<p>Standby servers may encounter missing tablespace directories when replaying database-creation WAL records. Prior to this patch, a standby would fail to recover in such a case; however, such directories could be legitimately missing. Create the tablespace (as a plain directory), then check that it has been dropped again once replay reaches a consistent state.</p>"
  ],
  [
    "<p>Support <span class=\"quote\">“<span class=\"quote\">in place</span>”</span> tablespaces (Thomas Munro, Michael Paquier, Álvaro Herrera)</p>",
    "<p>Normally a Postgres tablespace is a symbolic link to a directory on some other filesystem. This change allows it to just be a plain directory. While this has no use for separating tables onto different filesystems, it is a convenient setup for testing. Moreover, it is necessary to support the <code class=\"command\">CREATE DATABASE</code> replay fix, which transiently creates a missing tablespace as an <span class=\"quote\">“<span class=\"quote\">in place</span>”</span> tablespace.</p>"
  ],
  [
    "<p>Fix permissions checks in <code class=\"command\">CREATE INDEX</code> (Nathan Bossart, Noah Misch)</p>",
    "<p>The fix for CVE-2022-1552 caused <code class=\"command\">CREATE INDEX</code> to apply the table owner's permissions while performing lookups of operator classes and other objects, where formerly the calling user's permissions were used. This broke dump/restore scenarios, because <span class=\"application\">pg_dump</span> issues <code class=\"command\">CREATE INDEX</code> before re-granting permissions.</p>"
  ],
  [
    "<p>In extended query protocol, force an immediate commit after <code class=\"command\">CREATE DATABASE</code> and other commands that can't run in a transaction block (Tom Lane)</p>",
    "<p>If the client does not send a Sync message immediately after such a command, but instead sends another command, any failure in that command would lead to rolling back the preceding command, typically leaving inconsistent state on-disk (such as a missing or extra database directory). The mechanisms intended to prevent that situation turn out to work for multiple commands in a simple-Query message, but not for a series of extended-protocol messages. To prevent inconsistency without breaking use-cases that work today, force an implicit commit after such commands.</p>"
  ],
  [
    "<p>Fix race condition when checking transaction visibility (Simon Riggs)</p>",
    "<p><code class=\"function\">TransactionIdIsInProgress</code> could report <code class=\"literal\">false</code> before the subject transaction is considered visible, leading to various misbehaviors. The race condition window is normally very narrow, but use of synchronous replication makes it much wider, because the wait for a synchronous replica happens in that window.</p>"
  ],
  [
    "<p>Fix incorrect plans when sorting by an expression that contains a non-top-level set-returning function (Richard Guo, Tom Lane)</p>"
  ],
  [
    "<p>Fix incorrect permissions-checking code for extended statistics (Richard Guo)</p>",
    "<p>If there are extended statistics on a table that the user has only partial <code class=\"literal\">SELECT</code> permissions on, some queries would fail with <span class=\"quote\">“<span class=\"quote\">unrecognized node type</span>”</span> errors.</p>"
  ],
  [
    "<p>Fix extended statistics machinery to handle MCV-type statistics on boolean-valued expressions (Tom Lane)</p>",
    "<p>Statistics collection worked fine, but a query containing such an expression in <code class=\"literal\">WHERE</code> would fail with <span class=\"quote\">“<span class=\"quote\">unknown clause type</span>”</span>.</p>"
  ],
  [
    "<p>Avoid planner core dump with <code class=\"literal\"><em class=\"replaceable\"><code>constant</code></em> = ANY(<em class=\"replaceable\"><code>array</code></em>)</code> clauses when there are MCV-type extended statistics on the <em class=\"replaceable\"><code>array</code></em> variable (Tom Lane)</p>"
  ],
  [
    "<p>Fix <code class=\"literal\">ALTER TABLE ... ENABLE/DISABLE TRIGGER</code> to handle recursion correctly for triggers on partitioned tables (Álvaro Herrera, Amit Langote)</p>",
    "<p>In certain cases, a <span class=\"quote\">“<span class=\"quote\">trigger does not exist</span>”</span> failure would occur because the command would try to adjust the trigger on a child partition that doesn't have it.</p>"
  ],
  [
    "<p>Allow cancellation of <code class=\"command\">ANALYZE</code> while it is computing extended statistics (Tom Lane, Justin Pryzby)</p>",
    "<p>In some scenarios with high statistics targets, it was possible to spend many seconds in an un-cancellable sort operation.</p>"
  ],
  [
    "<p>Improve syntax error messages for type <code class=\"type\">jsonpath</code> (Andrew Dunstan)</p>"
  ],
  [
    "<p>Ensure that <code class=\"function\">pg_stop_backup()</code> cleans up session state properly (Fujii Masao)</p>",
    "<p>This omission could lead to assertion failures or crashes later in the session.</p>"
  ],
  [
    "<p>Fix <code class=\"function\">trim_array()</code> to handle a zero-dimensional array argument sanely (Martin Kalcher)</p>"
  ],
  [
    "<p>Fix join alias matching in <code class=\"literal\">FOR [KEY] UPDATE/SHARE</code> clauses (Dean Rasheed)</p>",
    "<p>In corner cases, a misleading error could be reported.</p>"
  ],
  [
    "<p>Reject <code class=\"literal\">ROW()</code> expressions and functions in <code class=\"literal\">FROM</code> that have too many columns (Tom Lane)</p>",
    "<p>Cases with more than about 1600 columns are unsupported, and have always failed at execution. However, it emerges that some earlier code could be driven to assertion failures or crashes by queries with more than 32K columns. Add a parse-time check to prevent that.</p>"
  ],
  [
    "<p>Fix dumping of a view using a function in <code class=\"literal\">FROM</code> that returns a composite type, when column(s) of the composite type have been dropped since the view was made (Tom Lane)</p>",
    "<p>This oversight could lead to dump/reload or <span class=\"application\">pg_upgrade</span> failures, as the dumped view would have too many column aliases for the function.</p>"
  ],
  [
    "<p>Disallow nested backup operations in logical replication walsenders (Fujii Masao)</p>"
  ],
  [
    "<p>Fix memory leak in logical replication subscribers (Hou Zhijie)</p>"
  ],
  [
    "<p>Fix logical replication's checking of replica identity when the target table is partitioned (Shi Yu, Hou Zhijie)</p>",
    "<p>The replica identity columns have to be re-identified for the child partition.</p>"
  ],
  [
    "<p>Fix failures to update cached schema data in a logical replication subscriber after a schema change on the publisher (Shi Yu, Hou Zhijie)</p>"
  ],
  [
    "<p>Fix WAL consistency checking logic to correctly handle <code class=\"literal\">BRIN_EVACUATE_PAGE</code> flags (Haiyang Wang)</p>"
  ],
  [
    "<p>Fix erroneous assertion checks in shared hashtable management (Thomas Munro)</p>"
  ],
  [
    "<p>Avoid assertion failure when <code class=\"varname\">min_dynamic_shared_memory</code> is set to a non-default value (Thomas Munro)</p>"
  ],
  [
    "<p>Arrange to clean up after commit-time errors within <code class=\"function\">SPI_commit()</code>, rather than expecting callers to do that (Peter Eisentraut, Tom Lane)</p>",
    "<p>Proper cleanup is complicated and requires use of low-level facilities, so it's not surprising that no known caller got it right. This led to misbehaviors when a PL procedure issued <code class=\"command\">COMMIT</code> but a failure occurred (such as a deferred constraint check). To improve matters, redefine <code class=\"function\">SPI_commit()</code> as starting a new transaction, so that it becomes equivalent to <code class=\"function\">SPI_commit_and_chain()</code> except that you get default transaction characteristics instead of preserving the prior transaction's characteristics. To make this somewhat transparent API-wise, redefine <code class=\"function\">SPI_start_transaction()</code> as a no-op. All known callers of <code class=\"function\">SPI_commit()</code> immediately call <code class=\"function\">SPI_start_transaction()</code>, so they will not notice any change. Similar remarks apply to <code class=\"function\">SPI_rollback()</code>.</p>",
    "<p>Also fix PL/Python, which omitted any handling of such errors at all, resulting in jumping out of the Python interpreter. This is reported to crash Python 3.11. Older Python releases leak some memory but seem okay with it otherwise.</p>"
  ],
  [
    "<p>Improve <span class=\"application\">libpq</span>'s handling of idle states in pipeline mode (Álvaro Herrera, Kyotaro Horiguchi)</p>",
    "<p>This fixes <span class=\"quote\">“<span class=\"quote\">message type 0x33 arrived from server while idle</span>”</span> warnings, as well as possible loss of end-of-query NULL results from <code class=\"function\">PQgetResult()</code>.</p>"
  ],
  [
    "<p>Avoid core dump in <span class=\"application\">ecpglib</span> with unexpected orders of operations (Tom Lane)</p>",
    "<p>Certain operations such as <code class=\"command\">EXEC SQL PREPARE</code> would crash (rather than reporting an error as expected) if called before establishing any database connection.</p>"
  ],
  [
    "<p>In <span class=\"application\">ecpglib</span>, avoid redundant <code class=\"function\">newlocale()</code> calls (Noah Misch)</p>",
    "<p>Allocate a C locale object once per process when first connecting, rather than creating and freeing locale objects once per query. This mitigates a libc memory leak on AIX, and may offer some performance benefit everywhere.</p>"
  ],
  [
    "<p>In <span class=\"application\">psql</span>'s <code class=\"command\">\\watch</code> command, echo a newline after cancellation with control-C (Pavel Stehule)</p>",
    "<p>This prevents libedit (and possibly also libreadline) from becoming confused about which column the cursor is in.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_upgrade</span> to detect non-upgradable usages of functions taking <code class=\"type\">anyarray</code> (Justin Pryzby)</p>",
    "<p>Version 14 changed some built-in functions to take type <code class=\"type\">anycompatiblearray</code> instead of <code class=\"type\">anyarray</code>. While this is mostly transparent, user-defined aggregates and operators built atop these functions have to be declared with exactly matching types. The presence of an object referencing the old signature will cause <span class=\"application\">pg_upgrade</span> to fail, so change it to detect and report such cases before beginning the upgrade.</p>"
  ],
  [
    "<p>Fix possible report of wrong error condition after <code class=\"function\">clone()</code> failure in <span class=\"application\">pg_upgrade</span> with <code class=\"option\">--clone</code> option (Justin Pryzby)</p>"
  ],
  [
    "<p>Fix <code class=\"filename\">contrib/pg_stat_statements</code> to avoid problems with very large query-text files on 32-bit platforms (Tom Lane)</p>"
  ],
  [
    "<p>In <code class=\"filename\">contrib/postgres_fdw</code>, prevent batch insertion when there are <code class=\"literal\">WITH CHECK OPTION</code> constraints (Etsuro Fujita)</p>",
    "<p>Such constraints cannot be checked properly if more than one row is inserted at a time.</p>"
  ],
  [
    "<p>Fix <code class=\"filename\">contrib/postgres_fdw</code> to detect failure to send an asynchronous data fetch query (Fujii Masao)</p>"
  ],
  [
    "<p>Ensure that <code class=\"filename\">contrib/postgres_fdw</code> sends constants of <code class=\"type\">regconfig</code> and other <code class=\"type\">reg*</code> types with proper schema qualification (Tom Lane)</p>"
  ],
  [
    "<p>Block signals while allocating dynamic shared memory on Linux (Thomas Munro)</p>",
    "<p>This avoids problems when a signal interrupts <code class=\"function\">posix_fallocate()</code>.</p>"
  ],
  [
    "<p>Detect unexpected <code class=\"literal\">EEXIST</code> error from <code class=\"function\">shm_open()</code> (Thomas Munro)</p>",
    "<p>This avoids a possible crash on Solaris.</p>"
  ],
  [
    "<p>Avoid using <code class=\"function\">signalfd()</code> on <span class=\"productname\">illumos</span> systems (Thomas Munro)</p>",
    "<p>This appears to trigger hangs and kernel panics, so avoid the function until a fix is available.</p>"
  ]
]