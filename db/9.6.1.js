[
  [
    "<p>Fix WAL-logging of truncation of relation free space maps and visibility maps (Pavan Deolasee, Heikki Linnakangas)</p>",
    "<p>It was possible for these files to not be correctly restored during crash recovery, or to be written incorrectly on a standby server. Bogus entries in a free space map could lead to attempts to access pages that have been truncated away from the relation itself, typically producing errors like <span class=\"QUOTE\">\"could not read block <tt class=\"REPLACEABLE c2\">XXX</tt>: read only 0 of 8192 bytes\"</span>. Checksum failures in the visibility map are also possible, if checksumming is enabled.</p>",
    "<p>Procedures for determining whether there is a problem and repairing it if so are discussed at <a href=\"https://wiki.postgresql.org/wiki/Free_Space_Map_Problems\" target=\"_top\">https://wiki.postgresql.org/wiki/Free_Space_Map_Problems</a>.</p>"
  ],
  [
    "<p>Fix possible data corruption when <span class=\"APPLICATION\">pg_upgrade</span> rewrites a relation visibility map into 9.6 format (Tom Lane)</p>",
    "<p>On big-endian machines, bytes of the new visibility map were written in the wrong order, leading to a completely incorrect map. On Windows, the old map was read using text mode, leading to incorrect results if the map happened to contain consecutive bytes that matched a carriage return/line feed sequence. The latter error would almost always lead to a <span class=\"APPLICATION\">pg_upgrade</span> failure due to the map file appearing to be the wrong length.</p>",
    "<p>If you are using a big-endian machine (many non-Intel architectures are big-endian) and have used <span class=\"APPLICATION\">pg_upgrade</span> to upgrade from a pre-9.6 release, you should assume that all visibility maps are incorrect and need to be regenerated. It is sufficient to truncate each relation's visibility map with <tt class=\"FILENAME\">contrib/pg_visibility</tt>'s <code class=\"FUNCTION\">pg_truncate_visibility_map()</code> function. For more information see <a href=\"https://wiki.postgresql.org/wiki/Visibility_Map_Problems\" target=\"_top\">https://wiki.postgresql.org/wiki/Visibility_Map_Problems</a>.</p>"
  ],
  [
    "<p>Don't throw serialization errors for self-conflicting insertions in <tt class=\"COMMAND\">INSERT ... ON CONFLICT</tt> (Thomas Munro, Peter Geoghegan)</p>"
  ],
  [
    "<p>Fix use-after-free hazard in execution of aggregate functions using <tt class=\"LITERAL\">DISTINCT</tt> (Peter Geoghegan)</p>",
    "<p>This could lead to a crash or incorrect query results.</p>"
  ],
  [
    "<p>Fix incorrect handling of polymorphic aggregates used as window functions (Tom Lane)</p>",
    "<p>The aggregate's transition function was told that its first argument and result were of the aggregate's output type, rather than the state type. This led to errors or crashes with polymorphic transition functions.</p>"
  ],
  [
    "<p>Fix <tt class=\"COMMAND\">COPY</tt> with a column name list from a table that has row-level security enabled (Adam Brightwell)</p>"
  ],
  [
    "<p>Fix <tt class=\"COMMAND\">EXPLAIN</tt> to emit valid XML when <a href=\"https://www.postgresql.org/docs/9.6/runtime-config-statistics.html#GUC-TRACK-IO-TIMING\">track_io_timing</a> is on (Markus Winand)</p>",
    "<p>Previously the XML output-format option produced syntactically invalid tags such as <tt class=\"LITERAL\">&lt;I/O-Read-Time&gt;</tt>. That is now rendered as <tt class=\"LITERAL\">&lt;I-O-Read-Time&gt;</tt>.</p>"
  ],
  [
    "<p>Fix statistics update for <tt class=\"COMMAND\">TRUNCATE</tt> in a prepared transaction (Stas Kelvich)</p>"
  ],
  [
    "<p>Fix bugs in merging inherited <tt class=\"LITERAL\">CHECK</tt> constraints while creating or altering a table (Tom Lane, Amit Langote)</p>",
    "<p>Allow identical <tt class=\"LITERAL\">CHECK</tt> constraints to be added to a parent and child table in either order. Prevent merging of a valid constraint from the parent table with a <tt class=\"LITERAL\">NOT VALID</tt> constraint on the child. Likewise, prevent merging of a <tt class=\"LITERAL\">NO INHERIT</tt> child constraint with an inherited constraint.</p>"
  ],
  [
    "<p>Show a sensible value in <tt class=\"STRUCTNAME\">pg_settings</tt>.<tt class=\"STRUCTFIELD\">unit</tt> for <tt class=\"VARNAME\">min_wal_size</tt> and <tt class=\"VARNAME\">max_wal_size</tt> (Tom Lane)</p>"
  ],
  [
    "<p>Fix replacement of array elements in <code class=\"FUNCTION\">jsonb_set()</code> (Tom Lane)</p>",
    "<p>If the target is an existing JSON array element, it got deleted instead of being replaced with a new value.</p>"
  ],
  [
    "<p>Avoid very-low-probability data corruption due to testing tuple visibility without holding buffer lock (Thomas Munro, Peter Geoghegan, Tom Lane)</p>"
  ],
  [
    "<p>Preserve commit timestamps across server restart (Julien Rouhaud, Craig Ringer)</p>",
    "<p>With <a href=\"https://www.postgresql.org/docs/9.6/runtime-config-replication.html#GUC-TRACK-COMMIT-TIMESTAMP\">track_commit_timestamp</a> turned on, old commit timestamps became inaccessible after a clean server restart.</p>"
  ],
  [
    "<p>Fix logical WAL decoding to work properly when a subtransaction's WAL output is large enough to spill to disk (Andres Freund)</p>"
  ],
  [
    "<p>Fix dangling-pointer problem in logical WAL decoding (Stas Kelvich)</p>"
  ],
  [
    "<p>Round shared-memory allocation request to a multiple of the actual huge page size when attempting to use huge pages on Linux (Tom Lane)</p>",
    "<p>This avoids possible failures during <code class=\"FUNCTION\">munmap()</code> on systems with atypical default huge page sizes. Except in crash-recovery cases, there were no ill effects other than a log message.</p>"
  ],
  [
    "<p>Don't try to share SSL contexts across multiple connections in <span class=\"APPLICATION\">libpq</span> (Heikki Linnakangas)</p>",
    "<p>This led to assorted corner-case bugs, particularly when trying to use different SSL parameters for different connections.</p>"
  ],
  [
    "<p>Avoid corner-case memory leak in <span class=\"APPLICATION\">libpq</span> (Tom Lane)</p>",
    "<p>The reported problem involved leaking an error report during <code class=\"FUNCTION\">PQreset()</code>, but there might be related cases.</p>"
  ],
  [
    "<p>In <span class=\"APPLICATION\">pg_upgrade</span>, check library loadability in name order (Tom Lane)</p>",
    "<p>This is a workaround to deal with cross-extension dependencies from language transform modules to their base language and data type modules.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_upgrade</span> to work correctly for extensions containing index access methods (Tom Lane)</p>",
    "<p>To allow this, the server has been extended to support <tt class=\"COMMAND\">ALTER EXTENSION ADD/DROP ACCESS METHOD</tt>. That functionality should have been included in the original patch to support dynamic creation of access methods, but it was overlooked.</p>"
  ],
  [
    "<p>Improve error reporting in <span class=\"APPLICATION\">pg_upgrade</span>'s file copying/linking/rewriting steps (Tom Lane, Álvaro Herrera)</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_dump</span> to work against pre-7.4 servers (Amit Langote, Tom Lane)</p>"
  ],
  [
    "<p>Disallow specifying both <tt class=\"OPTION\">--source-server</tt> and <tt class=\"OPTION\">--source-target</tt> options to <span class=\"APPLICATION\">pg_rewind</span> (Michael Banck)</p>"
  ],
  [
    "<p>Make <span class=\"APPLICATION\">pg_rewind</span> turn off <tt class=\"VARNAME\">synchronous_commit</tt> in its session on the source server (Michael Banck, Michael Paquier)</p>",
    "<p>This allows <span class=\"APPLICATION\">pg_rewind</span> to work even when the source server is using synchronous replication that is not working for some reason.</p>"
  ],
  [
    "<p>In <span class=\"APPLICATION\">pg_xlogdump</span>, retry opening new WAL segments when using <tt class=\"OPTION\">--follow</tt> option (Magnus Hagander)</p>",
    "<p>This allows for a possible delay in the server's creation of the next segment.</p>"
  ],
  [
    "<p>Fix <tt class=\"FILENAME\">contrib/pg_visibility</tt> to report the correct TID for a corrupt tuple that has been the subject of a rolled-back update (Tom Lane)</p>"
  ],
  [
    "<p>Fix makefile dependencies so that parallel make of <span class=\"APPLICATION\">PL/Python</span> by itself will succeed reliably (Pavel Raiskup)</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"APPLICATION\">tzdata</span> release 2016h for DST law changes in Palestine and Turkey, plus historical corrections for Turkey and some regions of Russia. Switch to numeric abbreviations for some time zones in Antarctica, the former Soviet Union, and Sri Lanka.</p>",
    "<p>The IANA time zone database previously provided textual abbreviations for all time zones, sometimes making up abbreviations that have little or no currency among the local population. They are in process of reversing that policy in favor of using numeric UTC offsets in zones where there is no evidence of real-world use of an English abbreviation. At least for the time being, <span class=\"PRODUCTNAME\">PostgreSQL</span> will continue to accept such removed abbreviations for timestamp input. But they will not be shown in the <tt class=\"STRUCTNAME\">pg_timezone_names</tt> view nor used for output.</p>",
    "<p>In this update, <tt class=\"LITERAL\">AMT</tt> is no longer shown as being in use to mean Armenia Time. Therefore, we have changed the <tt class=\"LITERAL\">Default</tt> abbreviation set to interpret it as Amazon Time, thus UTC-4 not UTC+4.</p>"
  ]
]