[
  [
    "<p>Protect against indirect security threats caused by\n          index functions changing session-local state (Gurjeet\n          Singh, Tom)</p>",
    "<p>This change prevents allegedly-immutable index\n          functions from possibly subverting a superuser's session\n          (CVE-2009-4136).</p>"
  ],
  [
    "<p>Reject SSL certificates containing an embedded null\n          byte in the common name (CN) field (Magnus)</p>",
    "<p>This prevents unintended matching of a certificate to\n          a server or client name during SSL validation\n          (CVE-2009-4034).</p>"
  ],
  [
    "<p>Fix hash index corruption (Tom)</p>",
    "<p>The 8.4 change that made hash indexes keep entries\n          sorted by hash value failed to update the bucket\n          splitting and compaction routines to preserve the\n          ordering. So application of either of those operations\n          could lead to permanent corruption of an index, in the\n          sense that searches might fail to find entries that are\n          present. To deal with this, it is recommended to\n          <code class=\"literal\">REINDEX</code> any hash indexes you\n          may have after installing this update.</p>"
  ],
  [
    "<p>Fix possible crash during backend-startup-time cache\n          initialization (Tom)</p>"
  ],
  [
    "<p>Avoid crash on empty thesaurus dictionary (Tom)</p>"
  ],
  [
    "<p>Prevent signals from interrupting <code class=\"literal\">VACUUM</code> at unsafe times (Alvaro)</p>",
    "<p>This fix prevents a PANIC if a <code class=\"literal\">VACUUM FULL</code> is canceled after it's\n          already committed its tuple movements, as well as\n          transient errors if a plain <code class=\"literal\">VACUUM</code> is interrupted after having\n          truncated the table.</p>"
  ],
  [
    "<p>Fix possible crash due to integer overflow in hash\n          table size calculation (Tom)</p>",
    "<p>This could occur with extremely large planner\n          estimates for the size of a hashjoin's result.</p>"
  ],
  [
    "<p>Fix crash if a <code class=\"literal\">DROP</code> is\n          attempted on an internally-dependent object (Tom)</p>"
  ],
  [
    "<p>Fix very rare crash in <code class=\"type\">inet</code>/<code class=\"type\">cidr</code>\n          comparisons (Chris Mikkelson)</p>"
  ],
  [
    "<p>Ensure that shared tuple-level locks held by prepared\n          transactions are not ignored (Heikki)</p>"
  ],
  [
    "<p>Fix premature drop of temporary files used for a\n          cursor that is accessed within a subtransaction\n          (Heikki)</p>"
  ],
  [
    "<p>Fix memory leak in syslogger process when rotating to\n          a new CSV logfile (Tom)</p>"
  ],
  [
    "<p>Fix memory leak in postmaster when re-parsing\n          <code class=\"filename\">pg_hba.conf</code> (Tom)</p>"
  ],
  [
    "<p>Fix Windows permission-downgrade logic (Jesse\n          Morris)</p>",
    "<p>This fixes some cases where the database failed to\n          start on Windows, often with misleading error messages\n          such as <span class=\"quote\">&#x201C;<span class=\"quote\">could\n          not locate matching postgres\n          executable</span>&#x201D;</span>.</p>"
  ],
  [
    "<p>Make <code class=\"literal\">FOR UPDATE/SHARE</code> in\n          the primary query not propagate into <code class=\"literal\">WITH</code> queries (Tom)</p>",
    "<p>For example, in</p>",
    "<pre class=\"programlisting\">\n          WITH w AS (SELECT * FROM foo) SELECT * FROM w, bar ... FOR UPDATE</pre>",
    "<p>the <code class=\"literal\">FOR UPDATE</code> will now\n          affect <code class=\"literal\">bar</code> but not\n          <code class=\"literal\">foo</code>. This is more useful and\n          consistent than the original 8.4 behavior, which tried to\n          propagate <code class=\"literal\">FOR UPDATE</code> into\n          the <code class=\"literal\">WITH</code> query but always\n          failed due to assorted implementation restrictions. It\n          also follows the design rule that <code class=\"literal\">WITH</code> queries are executed as if\n          independent of the main query.</p>"
  ],
  [
    "<p>Fix bug with a <code class=\"literal\">WITH\n          RECURSIVE</code> query immediately inside another one\n          (Tom)</p>"
  ],
  [
    "<p>Fix concurrency bug in hash indexes (Tom)</p>",
    "<p>Concurrent insertions could cause index scans to\n          transiently report wrong results.</p>"
  ],
  [
    "<p>Fix incorrect logic for GiST index page splits, when\n          the split depends on a non-first column of the index\n          (Paul Ramsey)</p>"
  ],
  [
    "<p>Fix wrong search results for a multi-column GIN index\n          with <code class=\"literal\">fastupdate</code> enabled\n          (Teodor)</p>"
  ],
  [
    "<p>Fix bugs in WAL entry creation for GIN indexes\n          (Tom)</p>",
    "<p>These bugs were masked when <code class=\"varname\">full_page_writes</code> was on, but with it off\n          a WAL replay failure was certain if a crash occurred\n          before the next checkpoint.</p>"
  ],
  [
    "<p>Don't error out if recycling or removing an old WAL\n          file fails at the end of checkpoint (Heikki)</p>",
    "<p>It's better to treat the problem as non-fatal and\n          allow the checkpoint to complete. Future checkpoints will\n          retry the removal. Such problems are not expected in\n          normal operation, but have been seen to be caused by\n          misdesigned Windows anti-virus and backup software.</p>"
  ],
  [
    "<p>Ensure WAL files aren't repeatedly archived on Windows\n          (Heikki)</p>",
    "<p>This is another symptom that could happen if some\n          other process interfered with deletion of a\n          no-longer-needed file.</p>"
  ],
  [
    "<p>Fix PAM password processing to be more robust\n          (Tom)</p>",
    "<p>The previous code is known to fail with the\n          combination of the Linux <code class=\"literal\">pam_krb5</code> PAM module with Microsoft\n          Active Directory as the domain controller. It might have\n          problems elsewhere too, since it was making unjustified\n          assumptions about what arguments the PAM stack would pass\n          to it.</p>"
  ],
  [
    "<p>Raise the maximum authentication token (Kerberos\n          ticket) size in GSSAPI and SSPI authentication methods\n          (Ian Turner)</p>",
    "<p>While the old 2000-byte limit was more than enough for\n          Unix Kerberos implementations, tickets issued by Windows\n          Domain Controllers can be much larger.</p>"
  ],
  [
    "<p>Ensure that domain constraints are enforced in\n          constructs like <code class=\"literal\">ARRAY[...]::domain</code>, where the domain is\n          over an array type (Heikki)</p>"
  ],
  [
    "<p>Fix foreign-key logic for some cases involving\n          composite-type columns as foreign keys (Tom)</p>"
  ],
  [
    "<p>Ensure that a cursor's snapshot is not modified after\n          it is created (Alvaro)</p>",
    "<p>This could lead to a cursor delivering wrong results\n          if later operations in the same transaction modify the\n          data the cursor is supposed to return.</p>"
  ],
  [
    "<p>Fix <code class=\"literal\">CREATE TABLE</code> to\n          properly merge default expressions coming from different\n          inheritance parent tables (Tom)</p>",
    "<p>This used to work but was broken in 8.4.</p>"
  ],
  [
    "<p>Re-enable collection of access statistics for\n          sequences (Akira Kurosawa)</p>",
    "<p>This used to work but was broken in 8.3.</p>"
  ],
  [
    "<p>Fix processing of ownership dependencies during\n          <code class=\"literal\">CREATE OR REPLACE FUNCTION</code>\n          (Tom)</p>"
  ],
  [
    "<p>Fix incorrect handling of <code class=\"literal\">WHERE</code> <em class=\"replaceable\"><code>x</code></em>=<em class=\"replaceable\"><code>x</code></em> conditions (Tom)</p>",
    "<p>In some cases these could get ignored as redundant,\n          but they aren't &#x2014; they're equivalent to <em class=\"replaceable\"><code>x</code></em> <code class=\"literal\">IS NOT NULL</code>.</p>"
  ],
  [
    "<p>Fix incorrect plan construction when using hash\n          aggregation to implement <code class=\"literal\">DISTINCT</code> for textually identical\n          volatile expressions (Tom)</p>"
  ],
  [
    "<p>Fix Assert failure for a volatile <code class=\"literal\">SELECT DISTINCT ON</code> expression (Tom)</p>"
  ],
  [
    "<p>Fix <code class=\"function\">ts_stat()</code> to not\n          fail on an empty <code class=\"type\">tsvector</code> value\n          (Tom)</p>"
  ],
  [
    "<p>Make text search parser accept underscores in XML\n          attributes (Peter)</p>"
  ],
  [
    "<p>Fix encoding handling in <code class=\"type\">xml</code>\n          binary input (Heikki)</p>",
    "<p>If the XML header doesn't specify an encoding, we now\n          assume UTF-8 by default; the previous handling was\n          inconsistent.</p>"
  ],
  [
    "<p>Fix bug with calling <code class=\"literal\">plperl</code> from <code class=\"literal\">plperlu</code> or vice versa (Tom)</p>",
    "<p>An error exit from the inner function could result in\n          crashes due to failure to re-select the correct Perl\n          interpreter for the outer function.</p>"
  ],
  [
    "<p>Fix session-lifespan memory leak when a PL/Perl\n          function is redefined (Tom)</p>"
  ],
  [
    "<p>Ensure that Perl arrays are properly converted to\n          <span class=\"productname\">PostgreSQL</span> arrays when\n          returned by a set-returning PL/Perl function (Andrew\n          Dunstan, Abhijit Menon-Sen)</p>",
    "<p>This worked correctly already for non-set-returning\n          functions.</p>"
  ],
  [
    "<p>Fix rare crash in exception processing in PL/Python\n          (Peter)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">ecpg</span> problem with\n          comments in <code class=\"literal\">DECLARE CURSOR</code>\n          statements (Michael)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">ecpg</span> to not treat\n          recently-added keywords as reserved words (Tom)</p>",
    "<p>This affected the keywords <code class=\"literal\">CALLED</code>, <code class=\"literal\">CATALOG</code>, <code class=\"literal\">DEFINER</code>, <code class=\"literal\">ENUM</code>, <code class=\"literal\">FOLLOWING</code>, <code class=\"literal\">INVOKER</code>, <code class=\"literal\">OPTIONS</code>, <code class=\"literal\">PARTITION</code>, <code class=\"literal\">PRECEDING</code>, <code class=\"literal\">RANGE</code>, <code class=\"literal\">SECURITY</code>, <code class=\"literal\">SERVER</code>, <code class=\"literal\">UNBOUNDED</code>, and <code class=\"literal\">WRAPPER</code>.</p>"
  ],
  [
    "<p>Re-allow regular expression special characters in\n          <span class=\"application\">psql</span>'s <code class=\"literal\">\\df</code> function name parameter (Tom)</p>"
  ],
  [
    "<p>In <code class=\"filename\">contrib/fuzzystrmatch</code>, correct the\n          calculation of <code class=\"function\">levenshtein</code>\n          distances with non-default costs (Marcin Mank)</p>"
  ],
  [
    "<p>In <code class=\"filename\">contrib/pg_standby</code>,\n          disable triggering failover with a signal on Windows\n          (Fujii Masao)</p>",
    "<p>This never did anything useful, because Windows\n          doesn't have Unix-style signals, but recent changes made\n          it actually crash.</p>"
  ],
  [
    "<p>Put <code class=\"literal\">FREEZE</code> and\n          <code class=\"literal\">VERBOSE</code> options in the right\n          order in the <code class=\"literal\">VACUUM</code> command\n          that <code class=\"filename\">contrib/vacuumdb</code>\n          produces (Heikki)</p>"
  ],
  [
    "<p>Fix possible leak of connections when <code class=\"filename\">contrib/dblink</code> encounters an error\n          (Tatsuhito Kasahara)</p>"
  ],
  [
    "<p>Ensure <span class=\"application\">psql</span>'s flex\n          module is compiled with the correct system header\n          definitions (Tom)</p>",
    "<p>This fixes build failures on platforms where\n          <code class=\"literal\">--enable-largefile</code> causes\n          incompatible changes in the generated code.</p>"
  ],
  [
    "<p>Make the postmaster ignore any <code class=\"literal\">application_name</code> parameter in connection\n          request packets, to improve compatibility with future\n          libpq versions (Tom)</p>"
  ],
  [
    "<p>Update the timezone abbreviation files to match\n          current reality (Joachim Wieland)</p>",
    "<p>This includes adding <code class=\"literal\">IDT</code>\n          to the default timezone abbreviation set.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2009s for DST law\n          changes in Antarctica, Argentina, Bangladesh, Fiji,\n          Novokuznetsk, Pakistan, Palestine, Samoa, Syria; also\n          historical corrections for Hong Kong.</p>"
  ]
]