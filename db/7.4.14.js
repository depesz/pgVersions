[
  [
    "<p>Fix core dump when an untyped literal is taken as\n          ANYARRAY</p>"
  ],
  [
    "<p>Fix <code class=\"function\">string_to_array()</code> to\n          handle overlapping matches for the separator string</p>",
    "<p>For example, <code class=\"literal\">string_to_array('123xx456xxx789',\n          'xx')</code>.</p>"
  ],
  [
    "<p>Fix corner cases in pattern matching for <span class=\"application\">psql</span>'s <code class=\"literal\">\\d</code> commands</p>"
  ],
  [
    "<p>Fix index-corrupting bugs in /contrib/ltree\n          (Teodor)</p>"
  ],
  [
    "<p>Fix backslash escaping in /contrib/dbmirror</p>"
  ],
  [
    "<p>Adjust regression tests for recent changes in US DST\n          laws</p>"
  ]
]