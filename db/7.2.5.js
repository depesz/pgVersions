[
  [
    "<p>Prevent possible loss of committed transactions during\n          crash</p>",
    "<p>Due to insufficient interlocking between transaction\n          commit and checkpointing, it was possible for\n          transactions committed just before the most recent\n          checkpoint to be lost, in whole or in part, following a\n          database crash and restart. This is a serious bug that\n          has existed since <span class=\"productname\">PostgreSQL</span> 7.1.</p>"
  ],
  [
    "<p>Fix corner case for btree search in parallel with\n          first root page split</p>"
  ],
  [
    "<p>Fix buffer overrun in <code class=\"function\">to_ascii</code> (Guido Notari)</p>"
  ],
  [
    "<p>Fix core dump in deadlock detection on machines where\n          char is unsigned</p>"
  ],
  [
    "<p>Fix failure to respond to <code class=\"command\">pg_ctl\n          stop -m fast</code> after Async_NotifyHandler runs</p>"
  ],
  [
    "<p>Repair memory leaks in pg_dump</p>"
  ],
  [
    "<p>Avoid conflict with system definition of <code class=\"function\">isblank()</code> function or macro</p>"
  ]
]