[
  [
    "<p>Fix failure of <tt class=\"COMMAND\">ALTER TABLE SET</tt> with a custom relation option (Michael Paquier)</p>"
  ],
  [
    "<p>Disallow changing a multiply-inherited column's type if not all parent tables were changed (Tom Lane)</p>",
    "<p>Previously, this was allowed, whereupon queries on the now-out-of-sync parent would fail.</p>"
  ],
  [
    "<p>Prevent <tt class=\"COMMAND\">VACUUM</tt> from trying to freeze an old multixact ID involving a still-running transaction (Nathan Bossart, Jeremy Schneider)</p>",
    "<p>This case would lead to <tt class=\"COMMAND\">VACUUM</tt> failing until the old transaction terminates.</p>"
  ],
  [
    "<p>Ensure that offset expressions in <tt class=\"LITERAL\">WINDOW</tt> clauses are processed when a query's expressions are manipulated (Andrew Gierth)</p>",
    "<p>This oversight could result in assorted failures when the offsets are nontrivial expressions. One example is that a function parameter reference in such an expression would fail if the function was inlined.</p>"
  ],
  [
    "<p>Fix handling of whole-row variables in <tt class=\"LITERAL\">WITH CHECK OPTION</tt> expressions and row-level-security policy expressions (Andres Freund)</p>",
    "<p>Previously, such usage might result in bogus errors about row type mismatches.</p>"
  ],
  [
    "<p>Avoid postmaster failure if a parallel query requests a background worker when no postmaster child process array slots remain free (Tom Lane)</p>"
  ],
  [
    "<p>Prevent possible double-free if a <tt class=\"LITERAL\">BEFORE UPDATE</tt> trigger returns the old tuple as-is, and it is not the last such trigger (Thomas Munro)</p>"
  ],
  [
    "<p>Provide a relevant error context line when an error occurs while setting GUC parameters during parallel worker startup (Thomas Munro)</p>"
  ],
  [
    "<p>In serializable mode, ensure that row-level predicate locks are acquired on the correct version of the row (Thomas Munro, Heikki Linnakangas)</p>",
    "<p>If the visible version of the row is HOT-updated, the lock might be taken on its now-dead predecessor, resulting in subtle failures to guarantee serialization.</p>"
  ],
  [
    "<p>Ensure that <code class=\"FUNCTION\">fsync()</code> is applied only to files that are opened read/write (Andres Freund, Michael Paquier)</p>",
    "<p>Some code paths tried to do this after opening a file read-only, but on some platforms that causes <span class=\"QUOTE\">\"bad file descriptor\"</span> or similar errors.</p>"
  ],
  [
    "<p>Allow encoding conversion to succeed on longer strings than before (Álvaro Herrera, Tom Lane)</p>",
    "<p>Previously, there was a hard limit of 0.25GB on the input string, but now it will work as long as the converted output is not over 1GB.</p>"
  ],
  [
    "<p>Avoid creating unnecessarily-bulky tuple stores for window functions (Andrew Gierth)</p>",
    "<p>In some cases the tuple storage would include all columns of the source table(s), not just the ones that are needed by the query.</p>"
  ],
  [
    "<p>Allow <code class=\"FUNCTION\">repalloc()</code> to give back space when a large chunk is reduced in size (Tom Lane)</p>"
  ],
  [
    "<p>Ensure that temporary WAL and history files are removed at the end of archive recovery (Sawada Masahiko)</p>"
  ],
  [
    "<p>Avoid failure in archive recovery if <tt class=\"VARNAME\">recovery_min_apply_delay</tt> is enabled (Fujii Masao)</p>",
    "<p><tt class=\"VARNAME\">recovery_min_apply_delay</tt> is not typically used in this configuration, but it should work.</p>"
  ],
  [
    "<p>Avoid unwanted delay during shutdown of a logical replication walsender (Craig Ringer, Álvaro Herrera)</p>"
  ],
  [
    "<p>Correctly time-stamp replication messages for logical decoding (Jeff Janes)</p>",
    "<p>This oversight resulted, for example, in <tt class=\"STRUCTNAME\">pg_stat_subscription</tt>.<tt class=\"STRUCTFIELD\">last_msg_send_time</tt> usually reading as NULL.</p>"
  ],
  [
    "<p>In logical decoding, ensure that sub-transactions are correctly accounted for when reconstructing a snapshot (Masahiko Sawada)</p>",
    "<p>This error leads to assertion failures; it's unclear whether any bad effects exist in production builds.</p>"
  ],
  [
    "<p>Fix race condition during backend exit, when the backend process has previously waited for synchronous replication to occur (Dongming Liu)</p>"
  ],
  [
    "<p>Fix <tt class=\"COMMAND\">ALTER SYSTEM</tt> to cope with duplicate entries in <tt class=\"FILENAME\">postgresql.auto.conf</tt> (Ian Barwick)</p>",
    "<p><tt class=\"COMMAND\">ALTER SYSTEM</tt> itself will not generate such a state, but external tools that modify <tt class=\"FILENAME\">postgresql.auto.conf</tt> could do so. Duplicate entries for the target variable will now be removed, and then the new setting (if any) will be appended at the end.</p>"
  ],
  [
    "<p>Reject include directives with empty file names in configuration files, and report include-file recursion more clearly (Ian Barwick, Tom Lane)</p>"
  ],
  [
    "<p>Avoid logging complaints about abandoned connections when using PAM authentication (Tom Lane)</p>",
    "<p>libpq-based clients will typically make two connection attempts when a password is required, since they don't prompt their user for a password until their first connection attempt fails. Therefore the server is coded not to generate useless log spam when a client closes the connection upon being asked for a password. However, the PAM authentication code hadn't gotten that memo, and would generate several messages about a phantom authentication failure.</p>"
  ],
  [
    "<p>Fix some cases where an incomplete date specification is not detected in <tt class=\"TYPE\">time with time zone</tt> input (Alexander Lakhin)</p>",
    "<p>If a time zone with a time-varying UTC offset is specified, then a date must be as well, so that the offset can be resolved. Depending on the syntax used, this check was not enforced in some cases, allowing bogus output to be produced.</p>"
  ],
  [
    "<p>Fix misbehavior of <code class=\"FUNCTION\">bitshiftright()</code> (Tom Lane)</p>",
    "<p>The bitstring right shift operator failed to zero out padding space that exists in the last byte of the result when the bitstring length is not a multiple of 8. While invisible to most operations, any nonzero bits there would result in unexpected comparison behavior, since bitstring comparisons don't bother to ignore the extra bits, expecting them to always be zero.</p>",
    "<p>If you have inconsistent data as a result of saving the output of <code class=\"FUNCTION\">bitshiftright()</code> in a table, it's possible to fix it with something like</p>",
    "<pre class=\"PROGRAMLISTING\">UPDATE mytab SET bitcol = ~(~bitcol) WHERE bitcol != ~(~bitcol);</pre>"
  ],
  [
    "<p>Fix detection of edge-case integer overflow in interval multiplication (Yuya Watari)</p>"
  ],
  [
    "<p>Avoid crashes if <tt class=\"LITERAL\">ispell</tt> text search dictionaries contain wrong affix data (Arthur Zakirov)</p>"
  ],
  [
    "<p>Fix incorrect compression logic for GIN posting lists (Heikki Linnakangas)</p>",
    "<p>A GIN posting list item can require 7 bytes if the distance between adjacent indexed TIDs exceeds 16TB. One step in the logic was out of sync with that, and might try to write the value into a 6-byte buffer. In principle this could cause a stack overrun, but on most architectures it's likely that the next byte would be unused alignment padding, making the bug harmless. In any case the bug would be very difficult to hit.</p>"
  ],
  [
    "<p>Fix handling of infinity, NaN, and NULL values in KNN-GiST (Alexander Korotkov)</p>",
    "<p>The query's output order could be wrong (different from a plain sort's result) if some distances computed for non-null column values are infinity or NaN.</p>"
  ],
  [
    "<p>Fix handling of searches for NULL in KNN-SP-GiST (Nikita Glukhov)</p>"
  ],
  [
    "<p>On Windows, recognize additional spellings of the <span class=\"QUOTE\">\"Norwegian (Bokmål)\"</span> locale name (Tom Lane)</p>"
  ],
  [
    "<p>Avoid compile failure if an ECPG client includes <tt class=\"FILENAME\">ecpglib.h</tt> while having <tt class=\"LITERAL\">ENABLE_NLS</tt> defined (Tom Lane)</p>",
    "<p>This risk was created by a misplaced declaration: <code class=\"FUNCTION\">ecpg_gettext()</code> should not be visible to client code.</p>"
  ],
  [
    "<p>In <span class=\"APPLICATION\">psql</span>, resynchronize internal state about the server after an unexpected connection loss and successful reconnection (Peter Billen, Tom Lane)</p>",
    "<p>Ordinarily this is unnecessary since the state would be the same anyway. But it can matter in corner cases, such as where the connection might lead to one of several servers. This change causes <span class=\"APPLICATION\">psql</span> to re-issue any interactive messages that it would have issued at startup, for example about whether SSL is in use.</p>"
  ],
  [
    "<p>Avoid platform-specific null pointer dereference in <span class=\"APPLICATION\">psql</span> (Quentin Rameau)</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_dump</span>'s handling of circular dependencies in views (Tom Lane)</p>",
    "<p>In some cases a view may depend on an object that <span class=\"APPLICATION\">pg_dump</span> needs to dump later than the view; the most common example is that a query using <tt class=\"LITERAL\">GROUP BY</tt> on a primary-key column may be semantically invalid without the primary key. This is now handled by emitting a dummy <tt class=\"COMMAND\">CREATE VIEW</tt> command that just establishes the view's column names and types, and then later emitting <tt class=\"COMMAND\">CREATE OR REPLACE VIEW</tt> with the full view definition. Previously, the dummy definition was actually a <tt class=\"COMMAND\">CREATE TABLE</tt> command, and this was automagically converted to a view by a later <tt class=\"COMMAND\">CREATE RULE</tt> command. The new approach has been used successfully in <span class=\"PRODUCTNAME\">PostgreSQL</span> version 10 and later. We are back-patching it into older releases now because of reports that the previous method causes bogus error messages about the view's replica identity status. This change also avoids problems when trying to use the <tt class=\"OPTION\">--clean</tt> option during a restore involving such a view.</p>"
  ],
  [
    "<p>In <span class=\"APPLICATION\">pg_dump</span>, ensure stable output order for similarly-named triggers and row-level-security policy objects (Benjie Gillam)</p>",
    "<p>Previously, if two triggers on different tables had the same names, they would be sorted in OID-based order, which is less desirable than sorting them by table name. Likewise for RLS policies.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_dump</span> to work again with pre-8.3 source servers (Tom Lane)</p>",
    "<p>A previous fix caused <span class=\"APPLICATION\">pg_dump</span> to always try to query <tt class=\"STRUCTNAME\">pg_opfamily</tt>, but that catalog doesn't exist before version 8.3.</p>"
  ],
  [
    "<p>In <span class=\"APPLICATION\">pg_restore</span>, treat <tt class=\"OPTION\">-f -</tt> as meaning <span class=\"QUOTE\">\"output to stdout\"</span> (Álvaro Herrera)</p>",
    "<p>This synchronizes <span class=\"APPLICATION\">pg_restore</span>'s behavior with some other applications, and in particular makes pre-v12 branches act similarly to version 12's <span class=\"APPLICATION\">pg_restore</span>, simplifying creation of dump/restore scripts that work across multiple <span class=\"PRODUCTNAME\">PostgreSQL</span> versions. Before this change, <span class=\"APPLICATION\">pg_restore</span> interpreted such a switch as meaning <span class=\"QUOTE\">\"output to a file named <tt class=\"FILENAME\">-</tt>\"</span>, but few people would want that.</p>"
  ],
  [
    "<p>Improve <span class=\"APPLICATION\">pg_upgrade</span>'s checks for the use of a data type that has changed representation, such as <tt class=\"TYPE\">line</tt> (Tomas Vondra)</p>",
    "<p>The previous coding could be fooled by cases where the data type of interest underlies a stored column of a domain or composite type.</p>"
  ],
  [
    "<p>Detect file read errors during <span class=\"APPLICATION\">pg_basebackup</span> (Jeevan Chalke)</p>"
  ],
  [
    "<p>In <span class=\"APPLICATION\">pg_rewind</span> with an online source cluster, disable timeouts, much as <span class=\"APPLICATION\">pg_dump</span> does (Alexander Kukushkin)</p>"
  ],
  [
    "<p>Fix failure in <span class=\"APPLICATION\">pg_waldump</span> with the <tt class=\"OPTION\">-s</tt> option, when a continuation WAL record ends exactly at a page boundary (Andrey Lepikhov)</p>"
  ],
  [
    "<p>In <span class=\"APPLICATION\">pg_waldump</span>, include the <tt class=\"LITERAL\">newitemoff</tt> field in btree page split records (Peter Geoghegan)</p>"
  ],
  [
    "<p>In <span class=\"APPLICATION\">pg_waldump</span> with the <tt class=\"OPTION\">--bkp-details</tt> option, avoid emitting extra newlines for WAL records involving full-page writes (Andres Freund)</p>"
  ],
  [
    "<p>Fix small memory leak in <span class=\"APPLICATION\">pg_waldump</span> (Andres Freund)</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">vacuumdb</span> with a high <tt class=\"OPTION\">--jobs</tt> option to handle running out of file descriptors better (Michael Paquier)</p>"
  ],
  [
    "<p>Fix <tt class=\"FILENAME\">contrib/intarray</tt>'s GiST opclasses to not fail for empty arrays with <tt class=\"LITERAL\">&lt;@</tt> (Tom Lane)</p>",
    "<p>A clause like <tt class=\"LITERAL\"><tt class=\"REPLACEABLE c2\">array_column</tt> &lt;@ <tt class=\"REPLACEABLE c2\">constant_array</tt></tt> is considered indexable, but the index search may not find empty array values; of course, such entries should trivially match the search.</p>",
    "<p>The only practical back-patchable fix for this requires making <tt class=\"LITERAL\">&lt;@</tt> index searches scan the whole index, which is what this patch does. This is unfortunate: it means that the query performance is likely worse than a plain sequential scan would be.</p>",
    "<p>Applications whose performance is adversely impacted by this change have a couple of options. They could switch to a GIN index, which doesn't have this bug, or they could replace <tt class=\"LITERAL\"><tt class=\"REPLACEABLE c2\">array_column</tt> &lt;@ <tt class=\"REPLACEABLE c2\">constant_array</tt></tt> with <tt class=\"LITERAL\"><tt class=\"REPLACEABLE c2\">array_column</tt> &lt;@ <tt class=\"REPLACEABLE c2\">constant_array</tt> AND <tt class=\"REPLACEABLE c2\">array_column</tt> &amp;&amp; <tt class=\"REPLACEABLE c2\">constant_array</tt></tt>. That will provide about the same performance as before, and it will find all non-empty subsets of the given constant array, which is all that could reliably be expected of the query before.</p>"
  ],
  [
    "<p>Allow <tt class=\"LITERAL\">configure --with-python</tt> to succeed when only <tt class=\"FILENAME\">python3</tt> or only <tt class=\"FILENAME\">python2</tt> can be found (Peter Eisentraut, Tom Lane)</p>",
    "<p>Search for <tt class=\"FILENAME\">python</tt>, then <tt class=\"FILENAME\">python3</tt>, then <tt class=\"FILENAME\">python2</tt>, so that <span class=\"APPLICATION\">configure</span> can succeed in the increasingly-more-common situation where there is no executable named simply <tt class=\"FILENAME\">python</tt>. It's still possible to override this choice by setting the <tt class=\"ENVAR\">PYTHON</tt> environment variable.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">configure</span>'s test for presence of libperl so that it works on recent Red Hat releases (Tom Lane)</p>",
    "<p>Previously, it could fail if the user sets <tt class=\"LITERAL\">CFLAGS</tt> to <tt class=\"LITERAL\">-O0</tt>.</p>"
  ],
  [
    "<p>Ensure correct code generation for spinlocks on PowerPC (Noah Misch)</p>",
    "<p>The previous spinlock coding allowed the compiler to select register zero for use with an assembly instruction that does not accept that register, causing a build failure. We have seen only one long-ago report that matches this bug, but it could cause problems for people trying to build modified <span class=\"PRODUCTNAME\">PostgreSQL</span> code or use atypical compiler options.</p>"
  ],
  [
    "<p>On PowerPC, avoid depending on the xlc compiler's <code class=\"FUNCTION\">__fetch_and_add()</code> function (Noah Misch)</p>",
    "<p>xlc 13 and newer interpret this function in a way incompatible with our usage, resulting in an unusable build of <span class=\"PRODUCTNAME\">PostgreSQL</span>. Fix by using custom assembly code instead.</p>"
  ],
  [
    "<p>On AIX, don't use the compiler option <tt class=\"OPTION\">-qsrcmsg</tt> (Noah Misch)</p>",
    "<p>This avoids an internal compiler error with xlc v16.1.0, with little consequence other than changing the format of compiler error messages.</p>"
  ],
  [
    "<p>Fix MSVC build process to cope with spaces in the file path of OpenSSL (Andrew Dunstan)</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"APPLICATION\">tzdata</span> release 2019c for DST law changes in Fiji and Norfolk Island, plus historical corrections for Alberta, Austria, Belgium, British Columbia, Cambodia, Hong Kong, Indiana (Perry County), Kaliningrad, Kentucky, Michigan, Norfolk Island, South Korea, and Turkey.</p>"
  ]
]