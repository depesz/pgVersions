[
  [
    "<p>Fixed bug in <code class=\"command\">CREATE\n          SCHEMA</code> parsing in ECPG (Michael)</p>"
  ],
  [
    "<p>Fix compile error when <code class=\"option\">--enable-thread-safety</code> and <code class=\"option\">--with-perl</code> are used together (Peter)</p>"
  ],
  [
    "<p>Fix for subqueries that used hash joins (Tom)</p>",
    "<p>Certain subqueries that used hash joins would crash\n          because of improperly shared structures.</p>"
  ],
  [
    "<p>Fix free space map compaction bug (Tom)</p>",
    "<p>This fixes a bug where compaction of the free space\n          map could lead to a database server shutdown.</p>"
  ],
  [
    "<p>Fix for Borland compiler build of libpq (Bruce)</p>"
  ],
  [
    "<p>Fix <code class=\"function\">netmask()</code> and\n          <code class=\"function\">hostmask()</code> to return the\n          maximum-length masklen (Tom)</p>",
    "<p>Fix these functions to return values consistent with\n          pre-7.4 releases.</p>"
  ],
  [
    "<p>Several <code class=\"filename\">contrib/pg_autovacuum</code> fixes</p>",
    "<p>Fixes include improper variable initialization,\n          missing vacuum after <code class=\"command\">TRUNCATE</code>, and duration computation\n          overflow for long vacuums.</p>"
  ],
  [
    "<p>Allow compile of <code class=\"filename\">contrib/cube</code> under Cygwin (Jason\n          Tishler)</p>"
  ],
  [
    "<p>Fix Solaris use of password file when no passwords are\n          defined (Tom)</p>",
    "<p>Fix crash on Solaris caused by use of any type of\n          password authentication when no passwords were\n          defined.</p>"
  ],
  [
    "<p>JDBC fix for thread problems, other fixes</p>"
  ],
  [
    "<p>Fix for <code class=\"type\">bytea</code> index lookups\n          (Joe)</p>"
  ],
  [
    "<p>Fix information schema for bit data types (Peter)</p>"
  ],
  [
    "<p>Force zero_damaged_pages to be on during recovery from\n          WAL</p>"
  ],
  [
    "<p>Prevent some obscure cases of <span class=\"quote\">&#x201C;<span class=\"quote\">variable not in subplan\n          target lists</span>&#x201D;</span></p>"
  ],
  [
    "<p>Make <code class=\"function\">PQescapeBytea</code> and\n          <code class=\"function\">byteaout</code> consistent with\n          each other (Joe)</p>"
  ],
  [
    "<p>Escape <code class=\"type\">bytea</code> output for\n          bytes &gt; 0x7e(Joe)</p>",
    "<p>If different client encodings are used for\n          <code class=\"type\">bytea</code> output and input, it is\n          possible for <code class=\"type\">bytea</code> values to be\n          corrupted by the differing encodings. This fix escapes\n          all bytes that might be affected.</p>"
  ],
  [
    "<p>Added missing <code class=\"function\">SPI_finish()</code> calls to dblink's\n          <code class=\"function\">get_tuple_of_interest()</code>\n          (Joe)</p>"
  ],
  [
    "<p>New Czech FAQ</p>"
  ],
  [
    "<p>Fix information schema view <code class=\"literal\">constraint_column_usage</code> for foreign keys\n          (Peter)</p>"
  ],
  [
    "<p>ECPG fixes (Michael)</p>"
  ],
  [
    "<p>Fix bug with multiple <code class=\"literal\">IN</code>\n          subqueries and joins in the subqueries (Tom)</p>"
  ],
  [
    "<p>Allow <code class=\"literal\">COUNT('x')</code> to work\n          (Tom)</p>"
  ],
  [
    "<p>Install ECPG include files for Informix compatibility\n          into separate directory (Peter)</p>",
    "<p>Some names of ECPG include files for Informix\n          compatibility conflicted with operating system include\n          files. By installing them in their own directory, name\n          conflicts have been reduced.</p>"
  ],
  [
    "<p>Fix SSL memory leak (Neil)</p>",
    "<p>This release fixes a bug in 7.4 where SSL didn't free\n          all memory it allocated.</p>"
  ],
  [
    "<p>Prevent <code class=\"filename\">pg_service.conf</code>\n          from using service name as default dbname (Bruce)</p>"
  ],
  [
    "<p>Fix local ident authentication on FreeBSD (Tom)</p>"
  ]
]