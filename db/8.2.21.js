[
  [
    "<p>Avoid potential deadlock during catalog cache\n          initialization (Nikhil Sontakke)</p>",
    "<p>In some cases the cache loading code would acquire\n          share lock on a system index before locking the index's\n          catalog. This could deadlock against processes trying to\n          acquire exclusive locks in the other, more standard\n          order.</p>"
  ],
  [
    "<p>Fix dangling-pointer problem in <code class=\"literal\">BEFORE ROW UPDATE</code> trigger handling when\n          there was a concurrent update to the target tuple (Tom\n          Lane)</p>",
    "<p>This bug has been observed to result in intermittent\n          <span class=\"quote\">&#x201C;<span class=\"quote\">cannot extract\n          system attribute from virtual tuple</span>&#x201D;</span>\n          failures while trying to do <code class=\"literal\">UPDATE\n          RETURNING ctid</code>. There is a very small probability\n          of more serious errors, such as generating incorrect\n          index entries for the updated tuple.</p>"
  ],
  [
    "<p>Disallow <code class=\"command\">DROP TABLE</code> when\n          there are pending deferred trigger events for the table\n          (Tom Lane)</p>",
    "<p>Formerly the <code class=\"command\">DROP</code> would\n          go through, leading to <span class=\"quote\">&#x201C;<span class=\"quote\">could not open relation with OID\n          nnn</span>&#x201D;</span> errors when the triggers were\n          eventually fired.</p>"
  ],
  [
    "<p>Fix PL/Python memory leak involving array slices\n          (Daniel Popowich)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_restore</span> to\n          cope with long lines (over 1KB) in TOC files (Tom\n          Lane)</p>"
  ],
  [
    "<p>Put in more safeguards against crashing due to\n          division-by-zero with overly enthusiastic compiler\n          optimization (Aurelien Jarno)</p>"
  ],
  [
    "<p>Support use of dlopen() in FreeBSD and OpenBSD on MIPS\n          (Tom Lane)</p>",
    "<p>There was a hard-wired assumption that this system\n          function was not available on MIPS hardware on these\n          systems. Use a compile-time test instead, since more\n          recent versions have it.</p>"
  ],
  [
    "<p>Fix compilation failures on HP-UX (Heikki\n          Linnakangas)</p>"
  ],
  [
    "<p>Fix path separator used by <span class=\"application\">pg_regress</span> on Cygwin (Andrew\n          Dunstan)</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2011f for DST law\n          changes in Chile, Cuba, Falkland Islands, Morocco, Samoa,\n          and Turkey; also historical corrections for South\n          Australia, Alaska, and Hawaii.</p>"
  ]
]