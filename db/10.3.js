[
  [
    "<p>Document how to configure installations and applications to guard against search-path-dependent trojan-horse attacks from other users (Noah Misch)</p>",
    "<p>Using a <code class=\"varname\">search_path</code> setting that includes any schemas writable by a hostile user enables that user to capture control of queries and then run arbitrary SQL code with the permissions of the attacked user. While it is possible to write queries that are proof against such hijacking, it is notationally tedious, and it's very easy to overlook holes. Therefore, we now recommend configurations in which no untrusted schemas appear in one's search path. Relevant documentation appears in <a class=\"xref\" href=\"https://www.postgresql.org/docs/10/ddl-schemas.html#DDL-SCHEMAS-PATTERNS\" title=\"5.8.6. Usage Patterns\">Section 5.8.6</a> (for database administrators and users), <a class=\"xref\" href=\"https://www.postgresql.org/docs/10/libpq-connect.html\" title=\"33.1. Database Connection Control Functions\">Section 33.1</a> (for application authors), <a class=\"xref\" href=\"https://www.postgresql.org/docs/10/extend-extensions.html#EXTEND-EXTENSIONS-STYLE\" title=\"37.15.1. Defining Extension Objects\">Section 37.15.1</a> (for extension authors), and <a class=\"xref\" href=\"https://www.postgresql.org/docs/10/sql-createfunction.html\" title=\"CREATE FUNCTION\"><span class=\"refentrytitle\">CREATE FUNCTION</span></a> (for authors of <code class=\"literal\">SECURITY DEFINER</code> functions). (CVE-2018-1058)</p>"
  ],
  [
    "<p>Avoid use of insecure <code class=\"varname\">search_path</code> settings in <span class=\"application\">pg_dump</span> and other client programs (Noah Misch, Tom Lane)</p>",
    "<p><span class=\"application\">pg_dump</span>, <span class=\"application\">pg_upgrade</span>, <span class=\"application\">vacuumdb</span> and other <span class=\"productname\">PostgreSQL</span>-provided applications were themselves vulnerable to the type of hijacking described in the previous changelog entry; since these applications are commonly run by superusers, they present particularly attractive targets. To make them secure whether or not the installation as a whole has been secured, modify them to include only the <code class=\"structname\">pg_catalog</code> schema in their <code class=\"varname\">search_path</code> settings. Autovacuum worker processes now do the same, as well.</p>",
    "<p>In cases where user-provided functions are indirectly executed by these programs — for example, user-provided functions in index expressions — the tighter <code class=\"varname\">search_path</code> may result in errors, which will need to be corrected by adjusting those user-provided functions to not assume anything about what search path they are invoked under. That has always been good practice, but now it will be necessary for correct behavior. (CVE-2018-1058)</p>"
  ],
  [
    "<p>Prevent logical replication from trying to ship changes for unpublishable relations (Peter Eisentraut)</p>",
    "<p>A publication marked <code class=\"literal\">FOR ALL TABLES</code> would incorrectly ship changes in materialized views and <code class=\"structname\">information_schema</code> tables, which are supposed to be omitted from the change stream.</p>"
  ],
  [
    "<p>Fix misbehavior of concurrent-update rechecks with CTE references appearing in subplans (Tom Lane)</p>",
    "<p>If a CTE (<code class=\"literal\">WITH</code> clause reference) is used in an InitPlan or SubPlan, and the query requires a recheck due to trying to update or lock a concurrently-updated row, incorrect results could be obtained.</p>"
  ],
  [
    "<p>Fix planner failures with overlapping mergejoin clauses in an outer join (Tom Lane)</p>",
    "<p>These mistakes led to <span class=\"quote\">“<span class=\"quote\">left and right pathkeys do not match in mergejoin</span>”</span> or <span class=\"quote\">“<span class=\"quote\">outer pathkeys do not match mergeclauses</span>”</span> planner errors in corner cases.</p>"
  ],
  [
    "<p>Repair <span class=\"application\">pg_upgrade</span>'s failure to preserve <code class=\"structfield\">relfrozenxid</code> for materialized views (Tom Lane, Andres Freund)</p>",
    "<p>This oversight could lead to data corruption in materialized views after an upgrade, manifesting as <span class=\"quote\">“<span class=\"quote\">could not access status of transaction</span>”</span> or <span class=\"quote\">“<span class=\"quote\">found xmin from before relfrozenxid</span>”</span> errors. The problem would be more likely to occur in seldom-refreshed materialized views, or ones that were maintained only with <code class=\"command\">REFRESH MATERIALIZED VIEW CONCURRENTLY</code>.</p>",
    "<p>If such corruption is observed, it can be repaired by refreshing the materialized view (without <code class=\"literal\">CONCURRENTLY</code>).</p>"
  ],
  [
    "<p>Fix incorrect <span class=\"application\">pg_dump</span> output for some non-default sequence limit values (Alexey Bashtanov)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_dump</span>'s mishandling of <code class=\"literal\">STATISTICS</code> objects (Tom Lane)</p>",
    "<p>An extended statistics object's schema was mislabeled in the dump's table of contents, possibly leading to the wrong results in a schema-selective restore. Its ownership was not correctly restored, either. Also, change the logic so that statistics objects are dumped/restored, or not, as independent objects rather than tying them to the dump/restore decision for the table they are on. The original definition could not scale to the planned future extension to cross-table statistics.</p>"
  ],
  [
    "<p>Fix incorrect reporting of PL/Python function names in error <code class=\"literal\">CONTEXT</code> stacks (Tom Lane)</p>",
    "<p>An error occurring within a nested PL/Python function call (that is, one reached via a SPI query from another PL/Python function) would result in a stack trace showing the inner function's name twice, rather than the expected results. Also, an error in a nested PL/Python <code class=\"literal\">DO</code> block could result in a null pointer dereference crash on some platforms.</p>"
  ],
  [
    "<p>Allow <code class=\"filename\">contrib/auto_explain</code>'s <code class=\"varname\">log_min_duration</code> setting to range up to <code class=\"literal\">INT_MAX</code>, or about 24 days instead of 35 minutes (Tom Lane)</p>"
  ],
  [
    "<p>Mark assorted GUC variables as <code class=\"literal\">PGDLLIMPORT</code>, to ease porting extension modules to Windows (Metin Doslu)</p>"
  ]
]