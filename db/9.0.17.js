[
  [
    "<p>Restore GIN metapages unconditionally to avoid\n          torn-page risk (Heikki Linnakangas)</p>",
    "<p>Although this oversight could theoretically result in\n          a corrupted index, it is unlikely to have caused any\n          problems in practice, since the active part of a GIN\n          metapage is smaller than a standard 512-byte disk\n          sector.</p>"
  ],
  [
    "<p>Avoid race condition in checking transaction commit\n          status during receipt of a <code class=\"command\">NOTIFY</code> message (Marko Tiikkaja)</p>",
    "<p>This prevents a scenario wherein a sufficiently fast\n          client might respond to a notification before database\n          updates made by the notifier have become visible to the\n          recipient.</p>"
  ],
  [
    "<p>Allow regular-expression operators to be terminated\n          early by query cancel requests (Tom Lane)</p>",
    "<p>This prevents scenarios wherein a pathological regular\n          expression could lock up a server process uninterruptibly\n          for a long time.</p>"
  ],
  [
    "<p>Remove incorrect code that tried to allow <code class=\"literal\">OVERLAPS</code> with single-element row\n          arguments (Joshua Yanovski)</p>",
    "<p>This code never worked correctly, and since the case\n          is neither specified by the SQL standard nor documented,\n          it seemed better to remove it than fix it.</p>"
  ],
  [
    "<p>Avoid getting more than <code class=\"literal\">AccessShareLock</code> when de-parsing a rule\n          or view (Dean Rasheed)</p>",
    "<p>This oversight resulted in <span class=\"application\">pg_dump</span> unexpectedly acquiring\n          <code class=\"literal\">RowExclusiveLock</code> locks on\n          tables mentioned as the targets of <code class=\"literal\">INSERT</code>/<code class=\"literal\">UPDATE</code>/<code class=\"literal\">DELETE</code> commands in rules. While usually\n          harmless, that could interfere with concurrent\n          transactions that tried to acquire, for example,\n          <code class=\"literal\">ShareLock</code> on those\n          tables.</p>"
  ],
  [
    "<p>Improve performance of index endpoint probes during\n          planning (Tom Lane)</p>",
    "<p>This change fixes a significant performance problem\n          that occurred when there were many not-yet-committed rows\n          at the end of the index, which is a common situation for\n          indexes on sequentially-assigned values such as\n          timestamps or sequence-generated identifiers.</p>"
  ],
  [
    "<p>Fix test to see if hot standby connections can be\n          allowed immediately after a crash (Heikki\n          Linnakangas)</p>"
  ],
  [
    "<p>Prevent interrupts while reporting non-<code class=\"literal\">ERROR</code> messages (Tom Lane)</p>",
    "<p>This guards against rare server-process freezeups due\n          to recursive entry to <code class=\"function\">syslog()</code>, and perhaps other related\n          problems.</p>"
  ],
  [
    "<p>Prevent intermittent <span class=\"quote\">&#x201C;<span class=\"quote\">could not reserve shared memory\n          region</span>&#x201D;</span> failures on recent Windows versions\n          (MauMau)</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2014a for DST law\n          changes in Fiji and Turkey, plus historical changes in\n          Israel and Ukraine.</p>"
  ]
]