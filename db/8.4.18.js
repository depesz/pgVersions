[
  [
    "<p>Prevent corruption of multi-byte characters when\n          attempting to case-fold identifiers (Andrew Dunstan)</p>",
    "<p><span class=\"productname\">PostgreSQL</span> case-folds\n          non-ASCII characters only when using a single-byte server\n          encoding.</p>"
  ],
  [
    "<p>Fix memory leak caused by <code class=\"function\">lo_open()</code> failure (Heikki\n          Linnakangas)</p>"
  ],
  [
    "<p>Fix memory overcommit bug when <code class=\"varname\">work_mem</code> is using more than 24GB of\n          memory (Stephen Frost)</p>"
  ],
  [
    "<p>Fix deadlock bug in libpq when using SSL (Stephen\n          Frost)</p>"
  ],
  [
    "<p>Properly compute row estimates for boolean columns\n          containing many NULL values (Andrew Gierth)</p>",
    "<p>Previously tests like <code class=\"literal\">col IS NOT\n          TRUE</code> and <code class=\"literal\">col IS NOT\n          FALSE</code> did not properly factor in NULL values when\n          estimating plan costs.</p>"
  ],
  [
    "<p>Prevent pushing down <code class=\"literal\">WHERE</code> clauses into unsafe <code class=\"literal\">UNION/INTERSECT</code> subqueries (Tom\n          Lane)</p>",
    "<p>Subqueries of a <code class=\"literal\">UNION</code> or\n          <code class=\"literal\">INTERSECT</code> that contain\n          set-returning functions or volatile functions in their\n          <code class=\"literal\">SELECT</code> lists could be\n          improperly optimized, leading to run-time errors or\n          incorrect query results.</p>"
  ],
  [
    "<p>Fix rare case of <span class=\"quote\">&#x201C;<span class=\"quote\">failed to locate grouping columns</span>&#x201D;</span>\n          planner failure (Tom Lane)</p>"
  ],
  [
    "<p>Improve view dumping code's handling of dropped\n          columns in referenced tables (Tom Lane)</p>"
  ],
  [
    "<p>Fix possible deadlock during concurrent <code class=\"command\">CREATE INDEX CONCURRENTLY</code> operations\n          (Tom Lane)</p>"
  ],
  [
    "<p>Fix <code class=\"function\">regexp_matches()</code>\n          handling of zero-length matches (Jeevan Chalke)</p>",
    "<p>Previously, zero-length matches like '^' could return\n          too many matches.</p>"
  ],
  [
    "<p>Fix crash for overly-complex regular expressions\n          (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Fix regular expression match failures for back\n          references combined with non-greedy quantifiers (Jeevan\n          Chalke)</p>"
  ],
  [
    "<p>Prevent <code class=\"command\">CREATE FUNCTION</code>\n          from checking <code class=\"command\">SET</code> variables\n          unless function body checking is enabled (Tom Lane)</p>"
  ],
  [
    "<p>Fix <code class=\"function\">pgp_pub_decrypt()</code> so\n          it works for secret keys with passwords (Marko Kreen)</p>"
  ],
  [
    "<p>Remove rare inaccurate warning during vacuum of\n          index-less tables (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Avoid possible failure when performing transaction\n          control commands (e.g <code class=\"command\">ROLLBACK</code>) in prepared queries (Tom\n          Lane)</p>"
  ],
  [
    "<p>Ensure that floating-point data input accepts standard\n          spellings of <span class=\"quote\">&#x201C;<span class=\"quote\">infinity</span>&#x201D;</span> on all platforms (Tom\n          Lane)</p>",
    "<p>The C99 standard says that allowable spellings are\n          <code class=\"literal\">inf</code>, <code class=\"literal\">+inf</code>, <code class=\"literal\">-inf</code>,\n          <code class=\"literal\">infinity</code>, <code class=\"literal\">+infinity</code>, and <code class=\"literal\">-infinity</code>. Make sure we recognize these\n          even if the platform's <code class=\"function\">strtod</code> function doesn't.</p>"
  ],
  [
    "<p>Expand ability to compare rows to records and arrays\n          (Rafal Rzepecki, Tom Lane)</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2013d for DST law\n          changes in Israel, Morocco, Palestine, and Paraguay.\n          Also, historical zone data corrections for Macquarie\n          Island.</p>"
  ]
]