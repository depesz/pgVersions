[
  [
    "<p>Fix failure of <tt class=\"COMMAND\">ALTER TABLE ... ALTER COLUMN TYPE</tt> when the table has a partial exclusion constraint (Tom Lane)</p>"
  ],
  [
    "<p>Fix failure of <tt class=\"COMMAND\">COMMENT</tt> command for comments on domain constraints (Daniel Gustafsson, Michael Paquier)</p>"
  ],
  [
    "<p>Fix faulty generation of merge-append plans (Tom Lane)</p>",
    "<p>This mistake could lead to <span class=\"QUOTE\">\"could not find pathkey item to sort\"</span> errors.</p>"
  ],
  [
    "<p>Fix incorrect printing of queries with duplicate join names (Philip Dubé)</p>",
    "<p>This oversight caused a dump/restore failure for views containing such queries.</p>"
  ],
  [
    "<p>Fix misoptimization of <tt class=\"LITERAL\">{1,1}</tt> quantifiers in regular expressions (Tom Lane)</p>",
    "<p>Such quantifiers were treated as no-ops and optimized away; but the documentation specifies that they impose greediness, or non-greediness in the case of the non-greedy variant <tt class=\"LITERAL\">{1,1}?</tt>, on the subexpression they're attached to, and this did not happen. The misbehavior occurred only if the subexpression contained capturing parentheses or a back-reference.</p>"
  ],
  [
    "<p>Avoid possible failures while initializing a new process's <tt class=\"STRUCTNAME\">pg_stat_activity</tt> data (Tom Lane)</p>",
    "<p>Certain operations that could fail, such as converting strings extracted from an SSL certificate into the database encoding, were being performed inside a critical section. Failure there would result in database-wide lockup due to violating the access protocol for shared <tt class=\"STRUCTNAME\">pg_stat_activity</tt> data.</p>"
  ],
  [
    "<p>Fix race condition in check to see whether a pre-existing shared memory segment is still in use by a conflicting postmaster (Tom Lane)</p>"
  ],
  [
    "<p>Avoid attempting to do database accesses for parameter checking in processes that are not connected to a specific database (Vignesh C, Andres Freund)</p>",
    "<p>This error could result in failures like <span class=\"QUOTE\">\"cannot read pg_class without having selected a database\"</span>.</p>"
  ],
  [
    "<p>Avoid possible hang in <span class=\"APPLICATION\">libpq</span> if using SSL and OpenSSL's pending-data buffer contains an exact multiple of 256 bytes (David Binderman)</p>"
  ],
  [
    "<p>Improve <span class=\"APPLICATION\">initdb</span>'s handling of multiple equivalent names for the system time zone (Tom Lane, Andrew Gierth)</p>",
    "<p>Make <span class=\"APPLICATION\">initdb</span> examine the <tt class=\"FILENAME\">/etc/localtime</tt> symbolic link, if that exists, to break ties between equivalent names for the system time zone. This makes <span class=\"APPLICATION\">initdb</span> more likely to select the time zone name that the user would expect when multiple identical time zones exist. It will not change the behavior if <tt class=\"FILENAME\">/etc/localtime</tt> is not a symlink to a zone data file, nor if the time zone is determined from the <tt class=\"ENVAR\">TZ</tt> environment variable.</p>",
    "<p>Separately, prefer <tt class=\"LITERAL\">UTC</tt> over other spellings of that time zone, when neither <tt class=\"ENVAR\">TZ</tt> nor <tt class=\"FILENAME\">/etc/localtime</tt> provide a hint. This fixes an annoyance introduced by <span class=\"APPLICATION\">tzdata</span> 2019a's change to make the <tt class=\"LITERAL\">UCT</tt> and <tt class=\"LITERAL\">UTC</tt> zone names equivalent: <span class=\"APPLICATION\">initdb</span> was then preferring <tt class=\"LITERAL\">UCT</tt>, which almost nobody wants.</p>"
  ],
  [
    "<p>Fix ordering of <tt class=\"COMMAND\">GRANT</tt> commands emitted by <span class=\"APPLICATION\">pg_dump</span> and <span class=\"APPLICATION\">pg_dumpall</span> for databases and tablespaces (Nathan Bossart, Michael Paquier)</p>",
    "<p>If cascading grants had been issued, restore might fail due to the <tt class=\"COMMAND\">GRANT</tt> commands being given in an order that didn't respect their interdependencies.</p>"
  ],
  [
    "<p>Fix misleading error reports from <span class=\"APPLICATION\">reindexdb</span> (Julien Rouhaud)</p>"
  ],
  [
    "<p>Ensure that <span class=\"APPLICATION\">vacuumdb</span> returns correct status if an error occurs while using parallel jobs (Julien Rouhaud)</p>"
  ],
  [
    "<p>Fix <tt class=\"FILENAME\">contrib/auto_explain</tt> to not cause problems in parallel queries (Tom Lane)</p>",
    "<p>Previously, a parallel worker might try to log its query even if the parent query were not being logged by <tt class=\"FILENAME\">auto_explain</tt>. This would work sometimes, but it's confusing, and in some cases it resulted in failures like <span class=\"QUOTE\">\"could not find key N in shm TOC\"</span>.</p>",
    "<p>Also, fix an off-by-one error that resulted in not necessarily logging every query even when the sampling rate is set to 1.0.</p>"
  ],
  [
    "<p>In <tt class=\"FILENAME\">contrib/postgres_fdw</tt>, account for possible data modifications by local <tt class=\"LITERAL\">BEFORE ROW UPDATE</tt> triggers (Shohei Mochizuki)</p>",
    "<p>If a trigger modified a column that was otherwise not changed by the <tt class=\"COMMAND\">UPDATE</tt>, the new value was not transmitted to the remote server.</p>"
  ],
  [
    "<p>On Windows, avoid failure when the database encoding is set to SQL_ASCII and we attempt to log a non-ASCII string (Noah Misch)</p>",
    "<p>The code had been assuming that such strings must be in UTF-8, and would throw an error if they didn't appear to be validly encoded. Now, just transmit the untranslated bytes to the log.</p>"
  ],
  [
    "<p>Make <span class=\"APPLICATION\">PL/pgSQL</span>'s header files C++-safe (George Tarasov)</p>"
  ]
]