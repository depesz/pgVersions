[
  [
    "<p>Ensure that all temporary files made by <span class=\"application\">pg_upgrade</span> are non-world-readable\n          (Tom Lane, Noah Misch)</p>",
    "<p><span class=\"application\">pg_upgrade</span> normally\n          restricts its temporary files to be readable and writable\n          only by the calling user. But the temporary file\n          containing <code class=\"literal\">pg_dumpall -g</code>\n          output would be group- or world-readable, or even\n          writable, if the user's <code class=\"literal\">umask</code> setting allows. In typical usage\n          on multi-user machines, the <code class=\"literal\">umask</code> and/or the working directory's\n          permissions would be tight enough to prevent problems;\n          but there may be people using <span class=\"application\">pg_upgrade</span> in scenarios where this\n          oversight would permit disclosure of database passwords\n          to unfriendly eyes. (CVE-2018-1053)</p>"
  ],
  [
    "<p>Fix vacuuming of tuples that were updated while\n          key-share locked (Andres Freund, &#xC1;lvaro Herrera)</p>",
    "<p>In some cases <code class=\"command\">VACUUM</code>\n          would fail to remove such tuples even though they are now\n          dead, leading to assorted data corruption scenarios.</p>"
  ],
  [
    "<p>Fix inadequate buffer locking in some LSN fetches\n          (Jacob Champion, Asim Praveen, Ashwin Agrawal)</p>",
    "<p>These errors could result in misbehavior under\n          concurrent load. The potential consequences have not been\n          characterized fully.</p>"
  ],
  [
    "<p>Avoid unnecessary failure in a query on an inheritance\n          tree that occurs concurrently with some child table being\n          removed from the tree by <code class=\"command\">ALTER\n          TABLE NO INHERIT</code> (Tom Lane)</p>"
  ],
  [
    "<p>Repair failure with correlated sub-<code class=\"literal\">SELECT</code> inside <code class=\"literal\">VALUES</code> inside a <code class=\"literal\">LATERAL</code> subquery (Tom Lane)</p>"
  ],
  [
    "<p>Fix <span class=\"quote\">&#x201C;<span class=\"quote\">could not\n          devise a query plan for the given query</span>&#x201D;</span>\n          planner failure for some cases involving nested\n          <code class=\"literal\">UNION ALL</code> inside a lateral\n          subquery (Tom Lane)</p>"
  ],
  [
    "<p>Fix <code class=\"function\">has_sequence_privilege()</code> to support\n          <code class=\"literal\">WITH GRANT OPTION</code> tests, as\n          other privilege-testing functions do (Joe Conway)</p>"
  ],
  [
    "<p>In databases using UTF8 encoding, ignore any XML\n          declaration that asserts a different encoding (Pavel\n          Stehule, Noah Misch)</p>",
    "<p>We always store XML strings in the database encoding,\n          so allowing libxml to act on a declaration of another\n          encoding gave wrong results. In encodings other than\n          UTF8, we don't promise to support non-ASCII XML data\n          anyway, so retain the previous behavior for bug\n          compatibility. This change affects only <code class=\"function\">xpath()</code> and related functions; other\n          XML code paths already acted this way.</p>"
  ],
  [
    "<p>Provide for forward compatibility with future minor\n          protocol versions (Robert Haas, Badrul Chowdhury)</p>",
    "<p>Up to now, <span class=\"productname\">PostgreSQL</span>\n          servers simply rejected requests to use protocol versions\n          newer than 3.0, so that there was no functional\n          difference between the major and minor parts of the\n          protocol version number. Allow clients to request\n          versions 3.x without failing, sending back a message\n          showing that the server only understands 3.0. This makes\n          no difference at the moment, but back-patching this\n          change should allow speedier introduction of future minor\n          protocol upgrades.</p>"
  ],
  [
    "<p>Prevent stack-overflow crashes when planning extremely\n          deeply nested set operations (<code class=\"literal\">UNION</code>/<code class=\"literal\">INTERSECT</code>/<code class=\"literal\">EXCEPT</code>) (Tom Lane)</p>"
  ],
  [
    "<p>Fix null-pointer crashes for some types of LDAP URLs\n          appearing in <code class=\"filename\">pg_hba.conf</code>\n          (Thomas Munro)</p>"
  ],
  [
    "<p>Fix sample <code class=\"function\">INSTR()</code>\n          functions in the PL/pgSQL documentation (Yugo Nagata, Tom\n          Lane)</p>",
    "<p>These functions are stated to be <span class=\"trademark\">Oracle</span>&#xAE; compatible, but they weren't\n          exactly. In particular, there was a discrepancy in the\n          interpretation of a negative third parameter: Oracle\n          thinks that a negative value indicates the last place\n          where the target substring can begin, whereas our\n          functions took it as the last place where the target can\n          end. Also, Oracle throws an error for a zero or negative\n          fourth parameter, whereas our functions returned\n          zero.</p>",
    "<p>The sample code has been adjusted to match Oracle's\n          behavior more precisely. Users who have copied this code\n          into their applications may wish to update their\n          copies.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_dump</span> to make\n          ACL (permissions), comment, and security label entries\n          reliably identifiable in archive output formats (Tom\n          Lane)</p>",
    "<p>The <span class=\"quote\">&#x201C;<span class=\"quote\">tag</span>&#x201D;</span> portion of an ACL archive\n          entry was usually just the name of the associated object.\n          Make it start with the object type instead, bringing ACLs\n          into line with the convention already used for comment\n          and security label archive entries. Also, fix the comment\n          and security label entries for the whole database, if\n          present, to make their tags start with <code class=\"literal\">DATABASE</code> so that they also follow this\n          convention. This prevents false matches in code that\n          tries to identify large-object-related entries by seeing\n          if the tag starts with <code class=\"literal\">LARGE\n          OBJECT</code>. That could have resulted in misclassifying\n          entries as data rather than schema, with undesirable\n          results in a schema-only or data-only dump.</p>",
    "<p>Note that this change has user-visible results in the\n          output of <code class=\"command\">pg_restore\n          --list</code>.</p>"
  ],
  [
    "<p>In <span class=\"application\">ecpg</span>, detect\n          indicator arrays that do not have the correct length and\n          report an error (David Rader)</p>"
  ],
  [
    "<p>Avoid triggering a libc assertion in <code class=\"filename\">contrib/hstore</code>, due to use of\n          <code class=\"function\">memcpy()</code> with equal source\n          and destination pointers (Tomas Vondra)</p>"
  ],
  [
    "<p>Provide modern examples of how to auto-start Postgres\n          on macOS (Tom Lane)</p>",
    "<p>The scripts in <code class=\"filename\">contrib/start-scripts/osx</code> use\n          infrastructure that's been deprecated for over a decade,\n          and which no longer works at all in macOS releases of the\n          last couple of years. Add a new subdirectory <code class=\"filename\">contrib/start-scripts/macos</code> containing\n          scripts that use the newer <span class=\"application\">launchd</span> infrastructure.</p>"
  ],
  [
    "<p>Fix incorrect selection of configuration-specific\n          libraries for OpenSSL on Windows (Andrew Dunstan)</p>"
  ],
  [
    "<p>Support linking to MinGW-built versions of libperl\n          (Noah Misch)</p>",
    "<p>This allows building PL/Perl with some common Perl\n          distributions for Windows.</p>"
  ],
  [
    "<p>Fix MSVC build to test whether 32-bit libperl needs\n          <code class=\"literal\">-D_USE_32BIT_TIME_T</code> (Noah\n          Misch)</p>",
    "<p>Available Perl distributions are inconsistent about\n          what they expect, and lack any reliable means of\n          reporting it, so resort to a build-time test on what the\n          library being used actually does.</p>"
  ],
  [
    "<p>On Windows, install the crash dump handler earlier in\n          postmaster startup (Takayuki Tsunakawa)</p>",
    "<p>This may allow collection of a core dump for some\n          early-startup failures that did not produce a dump\n          before.</p>"
  ],
  [
    "<p>On Windows, avoid encoding-conversion-related crashes\n          when emitting messages very early in postmaster startup\n          (Takayuki Tsunakawa)</p>"
  ],
  [
    "<p>Use our existing Motorola 68K spinlock code on OpenBSD\n          as well as NetBSD (David Carlier)</p>"
  ],
  [
    "<p>Add support for spinlocks on Motorola 88K (David\n          Carlier)</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2018c for DST law\n          changes in Brazil, Sao Tome and Principe, plus historical\n          corrections for Bolivia, Japan, and South Sudan. The\n          <code class=\"literal\">US/Pacific-New</code> zone has been\n          removed (it was only an alias for <code class=\"literal\">America/Los_Angeles</code> anyway).</p>"
  ]
]