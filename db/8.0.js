[
  [
    "<p>Support cross-data-type index usage (Tom)</p>",
    "<p>Before this change, many queries would not use an\n            index if the data types did not match exactly. This\n            improvement makes index usage more intuitive and\n            consistent.</p>"
  ],
  [
    "<p>New buffer replacement strategy that improves\n            caching (Jan)</p>",
    "<p>Prior releases used a least-recently-used (LRU)\n            cache to keep recently referenced pages in memory. The\n            LRU algorithm did not consider the number of times a\n            specific cache entry was accessed, so large table scans\n            could force out useful cache pages. The new cache\n            algorithm uses four separate lists to track most\n            recently used and most frequently used cache pages and\n            dynamically optimize their replacement based on the\n            work load. This should lead to much more efficient use\n            of the shared buffer cache. Administrators who have\n            tested shared buffer sizes in the past should retest\n            with this new cache replacement policy.</p>"
  ],
  [
    "<p>Add subprocess to write dirty buffers periodically\n            to reduce checkpoint writes (Jan)</p>",
    "<p>In previous releases, the checkpoint process, which\n            runs every few minutes, would write all dirty buffers\n            to the operating system's buffer cache then flush all\n            dirty operating system buffers to disk. This resulted\n            in a periodic spike in disk usage that often hurt\n            performance. The new code uses a background writer to\n            trickle disk writes at a steady pace so checkpoints\n            have far fewer dirty pages to write to disk. Also, the\n            new code does not issue a global <code class=\"function\">sync()</code> call, but instead <code class=\"function\">fsync()</code>s just the files written since\n            the last checkpoint. This should improve performance\n            and minimize degradation during checkpoints.</p>"
  ],
  [
    "<p>Add ability to prolong vacuum to reduce performance\n            impact (Jan)</p>",
    "<p>On busy systems, <code class=\"command\">VACUUM</code>\n            performs many I/O requests which can hurt performance\n            for other users. This release allows you to slow down\n            <code class=\"command\">VACUUM</code> to reduce its\n            impact on other users, though this increases the total\n            duration of <code class=\"command\">VACUUM</code>.</p>"
  ],
  [
    "<p>Improve B-tree index performance for duplicate keys\n            (Dmitry Tkach, Tom)</p>",
    "<p>This improves the way indexes are scanned when many\n            duplicate values exist in the index.</p>"
  ],
  [
    "<p>Use dynamically-generated table size estimates while\n            planning (Tom)</p>",
    "<p>Formerly the planner estimated table sizes using the\n            values seen by the last <code class=\"command\">VACUUM</code> or <code class=\"command\">ANALYZE</code>, both as to physical table\n            size (number of pages) and number of rows. Now, the\n            current physical table size is obtained from the\n            kernel, and the number of rows is estimated by\n            multiplying the table size by the row density (rows per\n            page) seen by the last <code class=\"command\">VACUUM</code> or <code class=\"command\">ANALYZE</code>. This should produce more\n            reliable estimates in cases where the table size has\n            changed significantly since the last housekeeping\n            command.</p>"
  ],
  [
    "<p>Improved index usage with <code class=\"literal\">OR</code> clauses (Tom)</p>",
    "<p>This allows the optimizer to use indexes in\n            statements with many OR clauses that would not have\n            been indexed in the past. It can also use multi-column\n            indexes where the first column is specified and the\n            second column is part of an <code class=\"literal\">OR</code> clause.</p>"
  ],
  [
    "<p>Improve matching of partial index clauses (Tom)</p>",
    "<p>The server is now smarter about using partial\n            indexes in queries involving complex <code class=\"option\">WHERE</code> clauses.</p>"
  ],
  [
    "<p>Improve performance of the GEQO optimizer (Tom)</p>",
    "<p>The GEQO optimizer is used to plan queries involving\n            many tables (by default, twelve or more). This release\n            speeds up the way queries are analyzed to decrease time\n            spent in optimization.</p>"
  ],
  [
    "<p>Miscellaneous optimizer improvements</p>",
    "<p>There is not room here to list all the minor\n            improvements made, but numerous special cases work\n            better than in prior releases.</p>"
  ],
  [
    "<p>Improve lookup speed for C functions (Tom)</p>",
    "<p>This release uses a hash table to lookup information\n            for dynamically loaded C functions. This improves their\n            speed so they perform nearly as quickly as functions\n            that are built into the server executable.</p>"
  ],
  [
    "<p>Add type-specific <code class=\"command\">ANALYZE</code> statistics capability (Mark\n            Cave-Ayland)</p>",
    "<p>This feature allows more flexibility in generating\n            statistics for nonstandard data types.</p>"
  ],
  [
    "<p><code class=\"command\">ANALYZE</code> now collects\n            statistics for expression indexes (Tom)</p>",
    "<p>Expression indexes (also called functional indexes)\n            allow users to index not just columns but the results\n            of expressions and function calls. With this release,\n            the optimizer can gather and use statistics about the\n            contents of expression indexes. This will greatly\n            improve the quality of planning for queries in which an\n            expression index is relevant.</p>"
  ],
  [
    "<p>New two-stage sampling method for <code class=\"command\">ANALYZE</code> (Manfred Koizar)</p>",
    "<p>This gives better statistics when the density of\n            valid rows is very different in different regions of a\n            table.</p>"
  ],
  [
    "<p>Speed up <code class=\"command\">TRUNCATE</code>\n            (Tom)</p>",
    "<p>This buys back some of the performance loss observed\n            in 7.4, while still keeping <code class=\"command\">TRUNCATE</code> transaction-safe.</p>"
  ],
  [
    "<p>Add WAL file archiving and point-in-time recovery\n            (Simon Riggs)</p>"
  ],
  [
    "<p>Add tablespaces so admins can control disk layout\n            (Gavin)</p>"
  ],
  [
    "<p>Add a built-in log rotation program (Andreas\n            Pflug)</p>",
    "<p>It is now possible to log server messages\n            conveniently without relying on either <span class=\"application\">syslog</span> or an external log rotation\n            program.</p>"
  ],
  [
    "<p>Add new read-only server configuration parameters to\n            show server compile-time settings: <code class=\"varname\">block_size</code>, <code class=\"varname\">integer_datetimes</code>, <code class=\"varname\">max_function_args</code>, <code class=\"varname\">max_identifier_length</code>, <code class=\"varname\">max_index_keys</code> (Joe)</p>"
  ],
  [
    "<p>Make quoting of <code class=\"literal\">sameuser</code>, <code class=\"literal\">samegroup</code>, and <code class=\"literal\">all</code> remove special meaning of these\n            terms in <code class=\"filename\">pg_hba.conf</code>\n            (Andrew)</p>"
  ],
  [
    "<p>Use clearer IPv6 name <code class=\"literal\">::1/128</code> for <code class=\"literal\">localhost</code> in default <code class=\"filename\">pg_hba.conf</code> (Andrew)</p>"
  ],
  [
    "<p>Use CIDR format in <code class=\"filename\">pg_hba.conf</code> examples (Andrew)</p>"
  ],
  [
    "<p>Rename server configuration parameters <code class=\"varname\">SortMem</code> and <code class=\"varname\">VacuumMem</code> to <code class=\"varname\">work_mem</code> and <code class=\"varname\">maintenance_work_mem</code> (Old names still\n            supported) (Tom)</p>",
    "<p>This change was made to clarify that bulk operations\n            such as index and foreign key creation use <code class=\"varname\">maintenance_work_mem</code>, while\n            <code class=\"varname\">work_mem</code> is for workspaces\n            used during query execution.</p>"
  ],
  [
    "<p>Allow logging of session disconnections using server\n            configuration <code class=\"varname\">log_disconnections</code> (Andrew)</p>"
  ],
  [
    "<p>Add new server configuration parameter <code class=\"varname\">log_line_prefix</code> to allow control of\n            information emitted in each log line (Andrew)</p>",
    "<p>Available information includes user name, database\n            name, remote IP address, and session start time.</p>"
  ],
  [
    "<p>Remove server configuration parameters <code class=\"varname\">log_pid</code>, <code class=\"varname\">log_timestamp</code>, <code class=\"varname\">log_source_port</code>; functionality\n            superseded by <code class=\"varname\">log_line_prefix</code> (Andrew)</p>"
  ],
  [
    "<p>Replace the <code class=\"varname\">virtual_host</code> and <code class=\"varname\">tcpip_socket</code> parameters with a unified\n            <code class=\"varname\">listen_addresses</code> parameter\n            (Andrew, Tom)</p>",
    "<p><code class=\"varname\">virtual_host</code> could only\n            specify a single IP address to listen on. <code class=\"varname\">listen_addresses</code> allows multiple\n            addresses to be specified.</p>"
  ],
  [
    "<p>Listen on localhost by default, which eliminates the\n            need for the <code class=\"option\">-i</code> postmaster\n            switch in many scenarios (Andrew)</p>",
    "<p>Listening on localhost (<code class=\"literal\">127.0.0.1</code>) opens no new security holes\n            but allows configurations like Windows and JDBC, which\n            do not support local sockets, to work without special\n            adjustments.</p>"
  ],
  [
    "<p>Remove <code class=\"varname\">syslog</code> server\n            configuration parameter, and add more logical\n            <code class=\"varname\">log_destination</code> variable\n            to control log output location (Magnus)</p>"
  ],
  [
    "<p>Change server configuration parameter <code class=\"varname\">log_statement</code> to take values\n            <code class=\"varname\">all</code>, <code class=\"varname\">mod</code>, <code class=\"varname\">ddl</code>,\n            or <code class=\"varname\">none</code> to select which\n            queries are logged (Bruce)</p>",
    "<p>This allows administrators to log only data\n            definition changes or only data modification\n            statements.</p>"
  ],
  [
    "<p>Some logging-related configuration parameters could\n            formerly be adjusted by ordinary users, but only in the\n            <span class=\"quote\">&#x201C;<span class=\"quote\">more\n            verbose</span>&#x201D;</span> direction. They are now treated\n            more strictly: only superusers can set them. However, a\n            superuser can use <code class=\"command\">ALTER\n            USER</code> to provide per-user settings of these\n            values for non-superusers. Also, it is now possible for\n            superusers to set values of superuser-only\n            configuration parameters via <code class=\"literal\">PGOPTIONS</code>.</p>"
  ],
  [
    "<p>Allow configuration files to be placed outside the\n            data directory (mlw)</p>",
    "<p>By default, configuration files are kept in the\n            cluster's top directory. With this addition,\n            configuration files can be placed outside the data\n            directory, easing administration.</p>"
  ],
  [
    "<p>Plan prepared queries only when first executed so\n            constants can be used for statistics (Oliver\n            Jowett)</p>",
    "<p>Prepared statements plan queries once and execute\n            them many times. While prepared queries avoid the\n            overhead of re-planning on each use, the quality of the\n            plan suffers from not knowing the exact parameters to\n            be used in the query. In this release, planning of\n            unnamed prepared statements is delayed until the first\n            execution, and the actual parameter values of that\n            execution are used as optimization hints. This allows\n            use of out-of-line parameter passing without incurring\n            a performance penalty.</p>"
  ],
  [
    "<p>Allow <code class=\"command\">DECLARE CURSOR</code> to\n            take parameters (Oliver Jowett)</p>",
    "<p>It is now useful to issue <code class=\"command\">DECLARE CURSOR</code> in a <code class=\"function\">Parse</code> message with parameters. The\n            parameter values sent at <code class=\"function\">Bind</code> time will be substituted into\n            the execution of the cursor's query.</p>"
  ],
  [
    "<p>Fix hash joins and aggregates of <code class=\"type\">inet</code> and <code class=\"type\">cidr</code>\n            data types (Tom)</p>",
    "<p>Release 7.4 handled hashing of mixed <code class=\"type\">inet</code> and <code class=\"type\">cidr</code>\n            values incorrectly. (This bug did not exist in prior\n            releases because they wouldn't try to hash either data\n            type.)</p>"
  ],
  [
    "<p>Make <code class=\"varname\">log_duration</code> print\n            only when <code class=\"varname\">log_statement</code>\n            prints the query (Ed L.)</p>"
  ],
  [
    "<p>Add savepoints (nested transactions) (Alvaro)</p>"
  ],
  [
    "<p>Unsupported isolation levels are now accepted and\n            promoted to the nearest supported level (Peter)</p>",
    "<p>The SQL specification states that if a database\n            doesn't support a specific isolation level, it should\n            use the next more restrictive level. This change\n            complies with that recommendation.</p>"
  ],
  [
    "<p>Allow <code class=\"command\">BEGIN WORK</code> to\n            specify transaction isolation levels like <code class=\"command\">START TRANSACTION</code> does (Bruce)</p>"
  ],
  [
    "<p>Fix table permission checking for cases in which\n            rules generate a query type different from the\n            originally submitted query (Tom)</p>"
  ],
  [
    "<p>Implement dollar quoting to simplify single-quote\n            usage (Andrew, Tom, David Fetter)</p>",
    "<p>In previous releases, because single quotes had to\n            be used to quote a function's body, the use of single\n            quotes inside the function text required use of two\n            single quotes or other error-prone notations. With this\n            release we add the ability to use \"dollar quoting\" to\n            quote a block of text. The ability to use different\n            quoting delimiters at different nesting levels greatly\n            simplifies the task of quoting correctly, especially in\n            complex functions. Dollar quoting can be used anywhere\n            quoted text is needed.</p>"
  ],
  [
    "<p>Make <code class=\"literal\">CASE val WHEN compval1\n            THEN ...</code> evaluate <code class=\"literal\">val</code> only once (Tom)</p>",
    "<p><code class=\"option\">CASE</code> no longer evaluates\n            the tested expression multiple times. This has benefits\n            when the expression is complex or is volatile.</p>"
  ],
  [
    "<p>Test <code class=\"option\">HAVING</code> before\n            computing target list of an aggregate query (Tom)</p>",
    "<p>Fixes improper failure of cases such as <code class=\"literal\">SELECT SUM(win)/SUM(lose) ... GROUP BY ...\n            HAVING SUM(lose) &gt; 0</code>. This should work but\n            formerly could fail with divide-by-zero.</p>"
  ],
  [
    "<p>Replace <code class=\"varname\">max_expr_depth</code>\n            parameter with <code class=\"varname\">max_stack_depth</code> parameter, measured in\n            kilobytes of stack size (Tom)</p>",
    "<p>This gives us a fairly bulletproof defense against\n            crashing due to runaway recursive functions. Instead of\n            measuring the depth of expression nesting, we now\n            directly measure the size of the execution stack.</p>"
  ],
  [
    "<p>Allow arbitrary row expressions (Tom)</p>",
    "<p>This release allows SQL expressions to contain\n            arbitrary composite types, that is, row values. It also\n            allows functions to more easily take rows as arguments\n            and return row values.</p>"
  ],
  [
    "<p>Allow <code class=\"option\">LIKE</code>/<code class=\"option\">ILIKE</code> to be used as the operator in row\n            and subselect comparisons (Fabien Coelho)</p>"
  ],
  [
    "<p>Avoid locale-specific case conversion of basic ASCII\n            letters in identifiers and keywords (Tom)</p>",
    "<p>This solves the <span class=\"quote\">&#x201C;<span class=\"quote\">Turkish problem</span>&#x201D;</span> with mangling of\n            words containing <code class=\"literal\">I</code> and\n            <code class=\"literal\">i</code>. Folding of characters\n            outside the 7-bit-ASCII set is still locale-aware.</p>"
  ],
  [
    "<p>Improve syntax error reporting (Fabien, Tom)</p>",
    "<p>Syntax error reports are more useful than\n            before.</p>"
  ],
  [
    "<p>Change <code class=\"command\">EXECUTE</code> to\n            return a completion tag matching the executed statement\n            (Kris Jurka)</p>",
    "<p>Previous releases return an <code class=\"command\">EXECUTE</code> tag for any <code class=\"command\">EXECUTE</code> call. In this release, the tag\n            returned will reflect the command executed.</p>"
  ],
  [
    "<p>Avoid emitting <code class=\"option\">NATURAL CROSS\n            JOIN</code> in rule listings (Tom)</p>",
    "<p>Such a clause makes no logical sense, but in some\n            cases the rule decompiler formerly produced this\n            syntax.</p>"
  ],
  [
    "<p>Add <code class=\"command\">COMMENT ON</code> for\n            casts, conversions, languages, operator classes, and\n            large objects (Christopher)</p>"
  ],
  [
    "<p>Add new server configuration parameter <code class=\"varname\">default_with_oids</code> to control whether\n            tables are created with <code class=\"type\">OID</code>s\n            by default (Neil)</p>",
    "<p>This allows administrators to control whether\n            <code class=\"command\">CREATE TABLE</code> commands\n            create tables with or without <code class=\"type\">OID</code> columns by default. (Note: the\n            current factory default setting for <code class=\"varname\">default_with_oids</code> is <code class=\"literal\">TRUE</code>, but the default will become\n            <code class=\"literal\">FALSE</code> in future\n            releases.)</p>"
  ],
  [
    "<p>Add <code class=\"option\">WITH</code> / <code class=\"option\">WITHOUT OIDS</code> clause to <code class=\"command\">CREATE TABLE AS</code> (Neil)</p>"
  ],
  [
    "<p>Allow <code class=\"command\">ALTER TABLE DROP\n            COLUMN</code> to drop an <code class=\"type\">OID</code>\n            column (<code class=\"command\">ALTER TABLE SET WITHOUT\n            OIDS</code> still works) (Tom)</p>"
  ],
  [
    "<p>Allow composite types as table columns (Tom)</p>"
  ],
  [
    "<p>Allow <code class=\"command\">ALTER ... ADD\n            COLUMN</code> with defaults and <code class=\"option\">NOT NULL</code> constraints; works per SQL\n            spec (Rod)</p>",
    "<p>It is now possible for <code class=\"option\">ADD\n            COLUMN</code> to create a column that is not initially\n            filled with NULLs, but with a specified default\n            value.</p>"
  ],
  [
    "<p>Add <code class=\"command\">ALTER COLUMN TYPE</code>\n            to change column's type (Rod)</p>",
    "<p>It is now possible to alter a column's data type\n            without dropping and re-adding the column.</p>"
  ],
  [
    "<p>Allow multiple <code class=\"command\">ALTER</code>\n            actions in a single <code class=\"command\">ALTER\n            TABLE</code> command (Rod)</p>",
    "<p>This is particularly useful for <code class=\"command\">ALTER</code> commands that rewrite the table\n            (which include <code class=\"option\">ALTER COLUMN\n            TYPE</code> and <code class=\"option\">ADD COLUMN</code>\n            with a default). By grouping <code class=\"command\">ALTER</code> commands together, the table\n            need be rewritten only once.</p>"
  ],
  [
    "<p>Allow <code class=\"command\">ALTER TABLE</code> to\n            add <code class=\"type\">SERIAL</code> columns (Tom)</p>",
    "<p>This falls out from the new capability of specifying\n            defaults for new columns.</p>"
  ],
  [
    "<p>Allow changing the owners of aggregates,\n            conversions, databases, functions, operators, operator\n            classes, schemas, types, and tablespaces (Christopher,\n            Euler Taveira de Oliveira)</p>",
    "<p>Previously this required modifying the system tables\n            directly.</p>"
  ],
  [
    "<p>Allow temporary object creation to be limited to\n            <code class=\"option\">SECURITY DEFINER</code> functions\n            (Sean Chittenden)</p>"
  ],
  [
    "<p>Add <code class=\"option\">ALTER TABLE ... SET WITHOUT\n            CLUSTER</code> (Christopher)</p>",
    "<p>Prior to this release, there was no way to clear an\n            auto-cluster specification except to modify the system\n            tables.</p>"
  ],
  [
    "<p>Constraint/Index/<code class=\"type\">SERIAL</code>\n            names are now <em class=\"replaceable\"><code>table_column_type</code></em> with\n            numbers appended to guarantee uniqueness within the\n            schema (Tom)</p>",
    "<p>The SQL specification states that such names should\n            be unique within a schema.</p>"
  ],
  [
    "<p>Add <code class=\"function\">pg_get_serial_sequence()</code> to return a\n            <code class=\"type\">SERIAL</code> column's sequence name\n            (Christopher)</p>",
    "<p>This allows automated scripts to reliably find the\n            <code class=\"type\">SERIAL</code> sequence name.</p>"
  ],
  [
    "<p>Warn when primary/foreign key data type mismatch\n            requires costly lookup</p>"
  ],
  [
    "<p>New <code class=\"command\">ALTER INDEX</code> command\n            to allow moving of indexes between tablespaces\n            (Gavin)</p>"
  ],
  [
    "<p>Make <code class=\"command\">ALTER TABLE OWNER</code>\n            change dependent sequence ownership too (Alvaro)</p>"
  ],
  [
    "<p>Allow <code class=\"command\">CREATE SCHEMA</code> to\n            create triggers, indexes, and sequences (Neil)</p>"
  ],
  [
    "<p>Add <code class=\"option\">ALSO</code> keyword to\n            <code class=\"command\">CREATE RULE</code> (Fabien\n            Coelho)</p>",
    "<p>This allows <code class=\"option\">ALSO</code> to be\n            added to rule creation to contrast it with <code class=\"option\">INSTEAD</code> rules.</p>"
  ],
  [
    "<p>Add <code class=\"option\">NOWAIT</code> option to\n            <code class=\"command\">LOCK</code> (Tatsuo)</p>",
    "<p>This allows the <code class=\"command\">LOCK</code>\n            command to fail if it would have to wait for the\n            requested lock.</p>"
  ],
  [
    "<p>Allow <code class=\"command\">COPY</code> to read and\n            write comma-separated-value (CSV) files (Andrew,\n            Bruce)</p>"
  ],
  [
    "<p>Generate error if the <code class=\"command\">COPY</code> delimiter and NULL string\n            conflict (Bruce)</p>"
  ],
  [
    "<p><code class=\"command\">GRANT</code>/<code class=\"command\">REVOKE</code> behavior follows the SQL spec\n            more closely</p>"
  ],
  [
    "<p>Avoid locking conflict between <code class=\"command\">CREATE INDEX</code> and <code class=\"command\">CHECKPOINT</code> (Tom)</p>",
    "<p>In 7.3 and 7.4, a long-running B-tree index build\n            could block concurrent <code class=\"command\">CHECKPOINT</code>s from completing, thereby\n            causing WAL bloat because the WAL log could not be\n            recycled.</p>"
  ],
  [
    "<p>Database-wide <code class=\"command\">ANALYZE</code>\n            does not hold locks across tables (Tom)</p>",
    "<p>This reduces the potential for deadlocks against\n            other backends that want exclusive locks on tables. To\n            get the benefit of this change, do not execute\n            database-wide <code class=\"command\">ANALYZE</code>\n            inside a transaction block (<code class=\"command\">BEGIN</code> block); it must be able to\n            commit and start a new transaction for each table.</p>"
  ],
  [
    "<p><code class=\"command\">REINDEX</code> does not\n            exclusively lock the index's parent table anymore</p>",
    "<p>The index itself is still exclusively locked, but\n            readers of the table can continue if they are not using\n            the particular index being rebuilt.</p>"
  ],
  [
    "<p>Erase MD5 user passwords when a user is renamed\n            (Bruce)</p>",
    "<p><span class=\"productname\">PostgreSQL</span> uses the\n            user name as salt when encrypting passwords via MD5.\n            When a user's name is changed, the salt will no longer\n            match the stored MD5 password, so the stored password\n            becomes useless. In this release a notice is generated\n            and the password is cleared. A new password must then\n            be assigned if the user is to be able to log in with a\n            password.</p>"
  ],
  [
    "<p>New <span class=\"application\">pg_ctl</span>\n            <code class=\"option\">kill</code> option for Windows\n            (Andrew)</p>",
    "<p>Windows does not have a <code class=\"literal\">kill</code> command to send signals to\n            backends so this capability was added to <span class=\"application\">pg_ctl</span>.</p>"
  ],
  [
    "<p>Information schema improvements</p>"
  ],
  [
    "<p>Add <code class=\"option\">--pwfile</code> option to\n            <span class=\"application\">initdb</span> so the initial\n            password can be set by GUI tools (Magnus)</p>"
  ],
  [
    "<p>Detect locale/encoding mismatch in <span class=\"application\">initdb</span> (Peter)</p>"
  ],
  [
    "<p>Add <code class=\"option\">register</code> command to\n            <span class=\"application\">pg_ctl</span> to register\n            Windows operating system service (Dave Page)</p>"
  ],
  [
    "<p>More complete support for composite types (row\n            types) (Tom)</p>",
    "<p>Composite values can be used in many places where\n            only scalar values worked before.</p>"
  ],
  [
    "<p>Reject nonrectangular array values as erroneous\n            (Joe)</p>",
    "<p>Formerly, <code class=\"function\">array_in</code>\n            would silently build a surprising result.</p>"
  ],
  [
    "<p>Overflow in integer arithmetic operations is now\n            detected (Tom)</p>"
  ],
  [
    "<p>The arithmetic operators associated with the\n            single-byte <code class=\"type\">\"char\"</code> data type\n            have been removed.</p>",
    "<p>Formerly, the parser would select these operators in\n            many situations where an <span class=\"quote\">&#x201C;<span class=\"quote\">unable to select an\n            operator</span>&#x201D;</span> error would be more\n            appropriate, such as <code class=\"literal\">null *\n            null</code>. If you actually want to do arithmetic on a\n            <code class=\"type\">\"char\"</code> column, you can cast\n            it to integer explicitly.</p>"
  ],
  [
    "<p>Syntax checking of array input values considerably\n            tightened up (Joe)</p>",
    "<p>Junk that was previously allowed in odd places with\n            odd results now causes an <code class=\"literal\">ERROR</code>, for example, non-whitespace\n            after the closing right brace.</p>"
  ],
  [
    "<p>Empty-string array element values must now be\n            written as <code class=\"literal\">\"\"</code>, rather than\n            writing nothing (Joe)</p>",
    "<p>Formerly, both ways of writing an empty-string\n            element value were allowed, but now a quoted empty\n            string is required. The case where nothing at all\n            appears will probably be considered to be a NULL\n            element value in some future release.</p>"
  ],
  [
    "<p>Array element trailing whitespace is now ignored\n            (Joe)</p>",
    "<p>Formerly leading whitespace was ignored, but\n            trailing whitespace between an element value and the\n            delimiter or right brace was significant. Now trailing\n            whitespace is also ignored.</p>"
  ],
  [
    "<p>Emit array values with explicit array bounds when\n            lower bound is not one (Joe)</p>"
  ],
  [
    "<p>Accept <code class=\"literal\">YYYY-monthname-DD</code> as a date string\n            (Tom)</p>"
  ],
  [
    "<p>Make <code class=\"function\">netmask</code> and\n            <code class=\"function\">hostmask</code> functions return\n            maximum-length mask length (Tom)</p>"
  ],
  [
    "<p>Change factorial function to return <code class=\"type\">numeric</code> (Gavin)</p>",
    "<p>Returning <code class=\"type\">numeric</code> allows\n            the factorial function to work for a wider range of\n            input values.</p>"
  ],
  [
    "<p><code class=\"function\">to_char</code>/<code class=\"function\">to_date()</code> date conversion\n            improvements (Kurt Roeckx, Fabien Coelho)</p>"
  ],
  [
    "<p>Make <code class=\"function\">length()</code>\n            disregard trailing spaces in <code class=\"type\">CHAR(n)</code> (Gavin)</p>",
    "<p>This change was made to improve consistency:\n            trailing spaces are semantically insignificant in\n            <code class=\"type\">CHAR(n)</code> data, so they should\n            not be counted by <code class=\"function\">length()</code>.</p>"
  ],
  [
    "<p>Warn about empty string being passed to <code class=\"type\">OID</code>/<code class=\"type\">float4</code>/<code class=\"type\">float8</code>\n            data types (Neil)</p>",
    "<p>8.1 will throw an error instead.</p>"
  ],
  [
    "<p>Allow leading or trailing whitespace in <code class=\"type\">int2</code>/<code class=\"type\">int4</code>/<code class=\"type\">int8</code>/<code class=\"type\">float4</code>/<code class=\"type\">float8</code>\n            input routines (Neil)</p>"
  ],
  [
    "<p>Better support for IEEE <code class=\"literal\">Infinity</code> and <code class=\"literal\">NaN</code> values in <code class=\"type\">float4</code>/<code class=\"type\">float8</code>\n            (Neil)</p>",
    "<p>These should now work on all platforms that support\n            IEEE-compliant floating point arithmetic.</p>"
  ],
  [
    "<p>Add <code class=\"option\">week</code> option to\n            <code class=\"function\">date_trunc()</code> (Robert\n            Creager)</p>"
  ],
  [
    "<p>Fix <code class=\"function\">to_char</code> for\n            <code class=\"literal\">1 BC</code> (previously it\n            returned <code class=\"literal\">1 AD</code>) (Bruce)</p>"
  ],
  [
    "<p>Fix <code class=\"function\">date_part(year)</code>\n            for BC dates (previously it returned one less than the\n            correct year) (Bruce)</p>"
  ],
  [
    "<p>Fix <code class=\"function\">date_part()</code> to\n            return the proper millennium and century (Fabien\n            Coelho)</p>",
    "<p>In previous versions, the century and millennium\n            results had a wrong number and started in the wrong\n            year, as compared to standard reckoning of such\n            things.</p>"
  ],
  [
    "<p>Add <code class=\"function\">ceiling()</code> as an\n            alias for <code class=\"function\">ceil()</code>, and\n            <code class=\"function\">power()</code> as an alias for\n            <code class=\"function\">pow()</code> for standards\n            compliance (Neil)</p>"
  ],
  [
    "<p>Change <code class=\"function\">ln()</code>,\n            <code class=\"function\">log()</code>, <code class=\"function\">power()</code>, and <code class=\"function\">sqrt()</code> to emit the correct\n            <code class=\"literal\">SQLSTATE</code> error codes for\n            certain error conditions, as specified by SQL:2003\n            (Neil)</p>"
  ],
  [
    "<p>Add <code class=\"function\">width_bucket()</code>\n            function as defined by SQL:2003 (Neil)</p>"
  ],
  [
    "<p>Add <code class=\"function\">generate_series()</code>\n            functions to simplify working with numeric sets\n            (Joe)</p>"
  ],
  [
    "<p>Fix <code class=\"function\">upper/lower/initcap()</code> functions to\n            work with multibyte encodings (Tom)</p>"
  ],
  [
    "<p>Add boolean and bitwise integer <code class=\"option\">AND</code>/<code class=\"option\">OR</code>\n            aggregates (Fabien Coelho)</p>"
  ],
  [
    "<p>New session information functions to return network\n            addresses for client and server (Sean Chittenden)</p>"
  ],
  [
    "<p>Add function to determine the area of a closed path\n            (Sean Chittenden)</p>"
  ],
  [
    "<p>Add function to send cancel request to other\n            backends (Magnus)</p>"
  ],
  [
    "<p>Add <code class=\"type\">interval</code> plus\n            <code class=\"type\">datetime</code> operators (Tom)</p>",
    "<p>The reverse ordering, <code class=\"type\">datetime</code> plus <code class=\"type\">interval</code>, was already supported, but both\n            are required by the SQL standard.</p>"
  ],
  [
    "<p>Casting an integer to <code class=\"type\">BIT(N)</code> selects the rightmost N bits of\n            the integer (Tom)</p>",
    "<p>In prior releases, the leftmost N bits were\n            selected, but this was deemed unhelpful, not to mention\n            inconsistent with casting from bit to int.</p>"
  ],
  [
    "<p>Require <code class=\"type\">CIDR</code> values to\n            have all nonmasked bits be zero (Kevin Brintnall)</p>"
  ],
  [
    "<p>In <code class=\"literal\">READ COMMITTED</code>\n            serialization mode, volatile functions now see the\n            results of concurrent transactions committed up to the\n            beginning of each statement within the function, rather\n            than up to the beginning of the interactive command\n            that called the function.</p>"
  ],
  [
    "<p>Functions declared <code class=\"literal\">STABLE</code> or <code class=\"literal\">IMMUTABLE</code> always use the snapshot of\n            the calling query, and therefore do not see the effects\n            of actions taken after the calling query starts,\n            whether in their own transaction or other transactions.\n            Such a function must be read-only, too, meaning that it\n            cannot use any SQL commands other than <code class=\"command\">SELECT</code>. There is a considerable\n            performance gain from declaring a function <code class=\"literal\">STABLE</code> or <code class=\"literal\">IMMUTABLE</code> rather than <code class=\"literal\">VOLATILE</code>.</p>"
  ],
  [
    "<p>Nondeferred <code class=\"option\">AFTER</code>\n            triggers are now fired immediately after completion of\n            the triggering query, rather than upon finishing the\n            current interactive command. This makes a difference\n            when the triggering query occurred within a function:\n            the trigger is invoked before the function proceeds to\n            its next operation. For example, if a function inserts\n            a new row into a table, any nondeferred foreign key\n            checks occur before proceeding with the function.</p>"
  ],
  [
    "<p>Allow function parameters to be declared with names\n            (Dennis Bj&#xF6;rklund)</p>",
    "<p>This allows better documentation of functions.\n            Whether the names actually do anything depends on the\n            specific function language being used.</p>"
  ],
  [
    "<p>Allow PL/pgSQL parameter names to be referenced in\n            the function (Dennis Bj&#xF6;rklund)</p>",
    "<p>This basically creates an automatic alias for each\n            named parameter.</p>"
  ],
  [
    "<p>Do minimal syntax checking of PL/pgSQL functions at\n            creation time (Tom)</p>",
    "<p>This allows us to catch simple syntax errors\n            sooner.</p>"
  ],
  [
    "<p>More support for composite types (row and record\n            variables) in PL/pgSQL</p>",
    "<p>For example, it now works to pass a rowtype variable\n            to another function as a single variable.</p>"
  ],
  [
    "<p>Default values for PL/pgSQL variables can now\n            reference previously declared variables</p>"
  ],
  [
    "<p>Improve parsing of PL/pgSQL FOR loops (Tom)</p>",
    "<p>Parsing is now driven by presence of <code class=\"literal\">\"..\"</code> rather than data type of\n            <code class=\"option\">FOR</code> variable. This makes no\n            difference for correct functions, but should result in\n            more understandable error messages when a mistake is\n            made.</p>"
  ],
  [
    "<p>Major overhaul of PL/Perl server-side language\n            (Command Prompt, Andrew Dunstan)</p>"
  ],
  [
    "<p>In PL/Tcl, SPI commands are now run in\n            subtransactions. If an error occurs, the subtransaction\n            is cleaned up and the error is reported as an ordinary\n            Tcl error, which can be trapped with <code class=\"literal\">catch</code>. Formerly, it was not possible\n            to catch such errors.</p>"
  ],
  [
    "<p>Accept <code class=\"command\">ELSEIF</code> in\n            PL/pgSQL (Neil)</p>",
    "<p>Previously PL/pgSQL only allowed <code class=\"command\">ELSIF</code>, but many people are accustomed\n            to spelling this keyword <code class=\"command\">ELSEIF</code>.</p>"
  ],
  [
    "<p>Improve <span class=\"application\">psql</span>\n            information display about database objects\n            (Christopher)</p>"
  ],
  [
    "<p>Allow <span class=\"application\">psql</span> to\n            display group membership in <code class=\"command\">\\du</code> and <code class=\"command\">\\dg</code> (Markus Bertheau)</p>"
  ],
  [
    "<p>Prevent <span class=\"application\">psql</span>\n            <code class=\"command\">\\dn</code> from showing temporary\n            schemas (Bruce)</p>"
  ],
  [
    "<p>Allow <span class=\"application\">psql</span> to\n            handle tilde user expansion for file names (Zach\n            Irmen)</p>"
  ],
  [
    "<p>Allow <span class=\"application\">psql</span> to\n            display fancy prompts, including color, via\n            <span class=\"application\">readline</span> (Reece Hart,\n            Chet Ramey)</p>"
  ],
  [
    "<p>Make <span class=\"application\">psql</span>\n            <code class=\"command\">\\copy</code> match <code class=\"command\">COPY</code> command syntax fully (Tom)</p>"
  ],
  [
    "<p>Show the location of syntax errors (Fabien Coelho,\n            Tom)</p>"
  ],
  [
    "<p>Add <code class=\"command\">CLUSTER</code> information\n            to <span class=\"application\">psql</span> <code class=\"command\">\\d</code> display (Bruce)</p>"
  ],
  [
    "<p>Change <span class=\"application\">psql</span>\n            <code class=\"command\">\\copy stdin/stdout</code> to read\n            from command input/output (Bruce)</p>"
  ],
  [
    "<p>Add <code class=\"option\">pstdin</code>/<code class=\"option\">pstdout</code> to read from <span class=\"application\">psql</span>'s <code class=\"literal\">stdin</code>/<code class=\"literal\">stdout</code> (Mark Feit)</p>"
  ],
  [
    "<p>Add global <span class=\"application\">psql</span>\n            configuration file, <code class=\"filename\">psqlrc.sample</code> (Bruce)</p>",
    "<p>This allows a central file where global <span class=\"application\">psql</span> startup commands can be\n            stored.</p>"
  ],
  [
    "<p>Have <span class=\"application\">psql</span>\n            <code class=\"command\">\\d+</code> indicate if the table\n            has an <code class=\"type\">OID</code> column (Neil)</p>"
  ],
  [
    "<p>On Windows, use binary mode in <span class=\"application\">psql</span> when reading files so\n            control-Z is not seen as end-of-file</p>"
  ],
  [
    "<p>Have <code class=\"command\">\\dn+</code> show\n            permissions and description for schemas (Dennis\n            Bj&#xF6;rklund)</p>"
  ],
  [
    "<p>Improve tab completion support (Stefan Kaltenbrunn,\n            Greg Sabino Mullane)</p>"
  ],
  [
    "<p>Allow boolean settings to be set using upper or\n            lower case (Michael Paesold)</p>"
  ],
  [
    "<p>Use dependency information to improve the\n            reliability of <span class=\"application\">pg_dump</span>\n            (Tom)</p>",
    "<p>This should solve the longstanding problems with\n            related objects sometimes being dumped in the wrong\n            order.</p>"
  ],
  [
    "<p>Have <span class=\"application\">pg_dump</span> output\n            objects in alphabetical order if possible (Tom)</p>",
    "<p>This should make it easier to identify changes\n            between dump files.</p>"
  ],
  [
    "<p>Allow <span class=\"application\">pg_restore</span> to\n            ignore some SQL errors (Fabien Coelho)</p>",
    "<p>This makes <span class=\"application\">pg_restore</span>'s behavior similar to\n            the results of feeding a <span class=\"application\">pg_dump</span> output script to\n            <span class=\"application\">psql</span>. In most cases,\n            ignoring errors and plowing ahead is the most useful\n            thing to do. Also added was a pg_restore option to give\n            the old behavior of exiting on an error.</p>"
  ],
  [
    "<p><span class=\"application\">pg_restore</span>\n            <code class=\"option\">-l</code> display now includes\n            objects' schema names</p>"
  ],
  [
    "<p>New begin/end markers in <span class=\"application\">pg_dump</span> text output (Bruce)</p>"
  ],
  [
    "<p>Add start/stop times for <span class=\"application\">pg_dump</span>/<span class=\"application\">pg_dumpall</span> in verbose mode\n            (Bruce)</p>"
  ],
  [
    "<p>Allow most <span class=\"application\">pg_dump</span>\n            options in <span class=\"application\">pg_dumpall</span>\n            (Christopher)</p>"
  ],
  [
    "<p>Have <span class=\"application\">pg_dump</span> use\n            <code class=\"command\">ALTER OWNER</code> rather than\n            <code class=\"command\">SET SESSION AUTHORIZATION</code>\n            by default (Christopher)</p>"
  ],
  [
    "<p>Make libpq's <code class=\"option\">SIGPIPE</code>\n            handling thread-safe (Bruce)</p>"
  ],
  [
    "<p>Add <code class=\"function\">PQmbdsplen()</code> which\n            returns the display length of a character (Tatsuo)</p>"
  ],
  [
    "<p>Add thread locking to <span class=\"application\">SSL</span> and <span class=\"application\">Kerberos</span> connections (Manfred\n            Spraul)</p>"
  ],
  [
    "<p>Allow <code class=\"function\">PQoidValue()</code>,\n            <code class=\"function\">PQcmdTuples()</code>, and\n            <code class=\"function\">PQoidStatus()</code> to work on\n            <code class=\"command\">EXECUTE</code> commands\n            (Neil)</p>"
  ],
  [
    "<p>Add <code class=\"function\">PQserverVersion()</code>\n            to provide more convenient access to the server version\n            number (Greg Sabino Mullane)</p>"
  ],
  [
    "<p>Add <code class=\"function\">PQprepare/PQsendPrepared()</code> functions\n            to support preparing statements without necessarily\n            specifying the data types of their parameters (Abhijit\n            Menon-Sen)</p>"
  ],
  [
    "<p>Many ECPG improvements, including <code class=\"command\">SET DESCRIPTOR</code> (Michael)</p>"
  ],
  [
    "<p>Allow the database server to run natively on Windows\n            (Claudio, Magnus, Andrew)</p>"
  ],
  [
    "<p>Shell script commands converted to C versions for\n            Windows support (Andrew)</p>"
  ],
  [
    "<p>Create an extension makefile framework (Fabien\n            Coelho, Peter)</p>",
    "<p>This simplifies the task of building extensions\n            outside the original source tree.</p>"
  ],
  [
    "<p>Support relocatable installations (Bruce)</p>",
    "<p>Directory paths for installed files (such as the\n            <code class=\"filename\">/share</code> directory) are now\n            computed relative to the actual location of the\n            executables, so that an installation tree can be moved\n            to another place without reconfiguring and\n            rebuilding.</p>"
  ],
  [
    "<p>Use <code class=\"option\">--with-docdir</code> to\n            choose installation location of documentation; also\n            allow <code class=\"option\">--infodir</code> (Peter)</p>"
  ],
  [
    "<p>Add <code class=\"option\">--without-docdir</code> to\n            prevent installation of documentation (Peter)</p>"
  ],
  [
    "<p>Upgrade to <span class=\"application\">DocBook</span>\n            V4.2 SGML (Peter)</p>"
  ],
  [
    "<p>New <code class=\"literal\">PostgreSQL</code>\n            <span class=\"application\">CVS</span> tag (Marc)</p>",
    "<p>This was done to make it easier for organizations to\n            manage their own copies of the <span class=\"productname\">PostgreSQL</span> <span class=\"application\">CVS</span> repository. File version\n            stamps from the master repository will not get munged\n            by checking into or out of a copied repository.</p>"
  ],
  [
    "<p>Clarify locking code (Manfred Koizar)</p>"
  ],
  [
    "<p>Buffer manager cleanup (Neil)</p>"
  ],
  [
    "<p>Decouple platform tests from CPU spinlock code\n            (Bruce, Tom)</p>"
  ],
  [
    "<p>Add inlined test-and-set code on PA-RISC for\n            <span class=\"application\">gcc</span> (ViSolve, Tom)</p>"
  ],
  [
    "<p>Improve i386 spinlock code (Manfred Spraul)</p>"
  ],
  [
    "<p>Clean up spinlock assembly code to avoid warnings\n            from newer <span class=\"application\">gcc</span>\n            releases (Tom)</p>"
  ],
  [
    "<p>Remove JDBC from source tree; now a separate\n            project</p>"
  ],
  [
    "<p>Remove the libpgtcl client interface; now a separate\n            project</p>"
  ],
  [
    "<p>More accurately estimate memory and file descriptor\n            usage (Tom)</p>"
  ],
  [
    "<p>Improvements to the macOS startup scripts (Ray\n            A.)</p>"
  ],
  [
    "<p>New <code class=\"function\">fsync()</code> test\n            program (Bruce)</p>"
  ],
  [
    "<p>Major documentation improvements (Neil, Peter)</p>"
  ],
  [
    "<p>Remove <span class=\"application\">pg_encoding</span>;\n            not needed anymore</p>"
  ],
  [
    "<p>Remove <span class=\"application\">pg_id</span>; not\n            needed anymore</p>"
  ],
  [
    "<p>Remove <span class=\"application\">initlocation</span>; not needed\n            anymore</p>"
  ],
  [
    "<p>Auto-detect thread flags (no more manual testing)\n            (Bruce)</p>"
  ],
  [
    "<p>Use Olson's public domain <span class=\"application\">timezone</span> library (Magnus)</p>"
  ],
  [
    "<p>With threading enabled, use thread flags on Unixware\n            for backend executables too (Bruce)</p>",
    "<p>Unixware cannot mix threaded and nonthreaded object\n            files in the same executable, so everything must be\n            compiled as threaded.</p>"
  ],
  [
    "<p><span class=\"application\">psql</span> now uses a\n            <span class=\"application\">flex</span>-generated lexical\n            analyzer to process command strings</p>"
  ],
  [
    "<p>Reimplement the linked list data structure used\n            throughout the backend (Neil)</p>",
    "<p>This improves performance by allowing list append\n            and length operations to be more efficient.</p>"
  ],
  [
    "<p>Allow dynamically loaded modules to create their own\n            server configuration parameters (Thomas Hallgren)</p>"
  ],
  [
    "<p>New Brazilian version of FAQ (Euler Taveira de\n            Oliveira)</p>"
  ],
  [
    "<p>Add French FAQ (Guillaume Lelarge)</p>"
  ],
  [
    "<p>New <span class=\"application\">pgevent</span> for\n            Windows logging</p>"
  ],
  [
    "<p>Make libpq and ECPG build as proper shared libraries\n            on macOS (Tom)</p>"
  ],
  [
    "<p>Overhaul of <code class=\"filename\">contrib/dblink</code> (Joe)</p>"
  ],
  [
    "<p><code class=\"filename\">contrib/dbmirror</code>\n            improvements (Steven Singer)</p>"
  ],
  [
    "<p>New <code class=\"filename\">contrib/xml2</code> (John\n            Gray, Torchbox)</p>"
  ],
  [
    "<p>Updated <code class=\"filename\">contrib/mysql</code></p>"
  ],
  [
    "<p>New version of <code class=\"filename\">contrib/btree_gist</code> (Teodor)</p>"
  ],
  [
    "<p>New <code class=\"filename\">contrib/trgm</code>,\n            trigram matching for <span class=\"productname\">PostgreSQL</span> (Teodor)</p>"
  ],
  [
    "<p>Many <code class=\"filename\">contrib/tsearch2</code>\n            improvements (Teodor)</p>"
  ],
  [
    "<p>Add double metaphone to <code class=\"filename\">contrib/fuzzystrmatch</code> (Andrew)</p>"
  ],
  [
    "<p>Allow <code class=\"filename\">contrib/pg_autovacuum</code> to run as a\n            Windows service (Dave Page)</p>"
  ],
  [
    "<p>Add functions to <code class=\"filename\">contrib/dbsize</code> (Andreas Pflug)</p>"
  ],
  [
    "<p>Removed <code class=\"filename\">contrib/pg_logger</code>: obsoleted by\n            integrated logging subprocess</p>"
  ],
  [
    "<p>Removed <code class=\"filename\">contrib/rserv</code>:\n            obsoleted by various separate projects</p>"
  ]
]