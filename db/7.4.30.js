[
  [
    "<p>Use a separate interpreter for each calling SQL userid\n          in PL/Perl and PL/Tcl (Tom Lane)</p>",
    "<p>This change prevents security problems that can be\n          caused by subverting Perl or Tcl code that will be\n          executed later in the same session under another SQL user\n          identity (for example, within a <code class=\"literal\">SECURITY DEFINER</code> function). Most\n          scripting languages offer numerous ways that that might\n          be done, such as redefining standard functions or\n          operators called by the target function. Without this\n          change, any SQL user with Perl or Tcl language usage\n          rights can do essentially anything with the SQL\n          privileges of the target function's owner.</p>",
    "<p>The cost of this change is that intentional\n          communication among Perl and Tcl functions becomes more\n          difficult. To provide an escape hatch, PL/PerlU and\n          PL/TclU functions continue to use only one interpreter\n          per session. This is not considered a security issue\n          since all such functions execute at the trust level of a\n          database superuser already.</p>",
    "<p>It is likely that third-party procedural languages\n          that claim to offer trusted execution have similar\n          security issues. We advise contacting the authors of any\n          PL you are depending on for security-critical\n          purposes.</p>",
    "<p>Our thanks to Tim Bunce for pointing out this issue\n          (CVE-2010-3433).</p>"
  ],
  [
    "<p>Prevent possible crashes in <code class=\"function\">pg_get_expr()</code> by disallowing it from\n          being called with an argument that is not one of the\n          system catalog columns it's intended to be used with\n          (Heikki Linnakangas, Tom Lane)</p>"
  ],
  [
    "<p>Fix <span class=\"quote\">&#x201C;<span class=\"quote\">cannot\n          handle unplanned sub-select</span>&#x201D;</span> error (Tom\n          Lane)</p>",
    "<p>This occurred when a sub-select contains a join alias\n          reference that expands into an expression containing\n          another sub-select.</p>"
  ],
  [
    "<p>Take care to fsync the contents of lockfiles (both\n          <code class=\"filename\">postmaster.pid</code> and the\n          socket lockfile) while writing them (Tom Lane)</p>",
    "<p>This omission could result in corrupted lockfile\n          contents if the machine crashes shortly after postmaster\n          start. That could in turn prevent subsequent attempts to\n          start the postmaster from succeeding, until the lockfile\n          is manually removed.</p>"
  ],
  [
    "<p>Improve <code class=\"filename\">contrib/dblink</code>'s\n          handling of tables containing dropped columns (Tom\n          Lane)</p>"
  ],
  [
    "<p>Fix connection leak after <span class=\"quote\">&#x201C;<span class=\"quote\">duplicate connection\n          name</span>&#x201D;</span> errors in <code class=\"filename\">contrib/dblink</code> (Itagaki Takahiro)</p>"
  ],
  [
    "<p>Update build infrastructure and documentation to\n          reflect the source code repository's move from CVS to Git\n          (Magnus Hagander and others)</p>"
  ]
]