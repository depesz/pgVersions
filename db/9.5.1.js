[
  [
    "<p>Fix infinite loops and buffer-overrun problems in regular expressions (Tom Lane)</p>",
    "<p>Very large character ranges in bracket expressions could cause infinite loops in some cases, and memory overwrites in other cases. (CVE-2016-0773)</p>"
  ],
  [
    "<p>Fix an oversight that caused hash joins to miss joining to some tuples of the inner relation in rare cases (Tomas Vondra, Tom Lane)</p>"
  ],
  [
    "<p>Avoid pushdown of <tt class=\"LITERAL\">HAVING</tt> clauses when grouping sets are used (Andrew Gierth)</p>"
  ],
  [
    "<p>Fix deparsing of <tt class=\"LITERAL\">ON CONFLICT</tt> arbiter <tt class=\"LITERAL\">WHERE</tt> clauses (Peter Geoghegan)</p>"
  ],
  [
    "<p>Make <tt class=\"LITERAL\">%h</tt> and <tt class=\"LITERAL\">%r</tt> escapes in <tt class=\"VARNAME\">log_line_prefix</tt> work for messages emitted due to <tt class=\"VARNAME\">log_connections</tt> (Tom Lane)</p>",
    "<p>Previously, <tt class=\"LITERAL\">%h</tt>/<tt class=\"LITERAL\">%r</tt> started to work just after a new session had emitted the <span class=\"QUOTE\">\"connection received\"</span> log message; now they work for that message too.</p>"
  ],
  [
    "<p>Avoid leaking a token handle during SSPI authentication (Christian Ullrich)</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">psql</span>'s <tt class=\"LITERAL\">\\det</tt> command to interpret its pattern argument the same way as other <tt class=\"LITERAL\">\\d</tt> commands with potentially schema-qualified patterns do (Reece Hart)</p>"
  ],
  [
    "<p>In <span class=\"APPLICATION\">pg_ctl</span> on Windows, check service status to decide where to send output, rather than checking if standard output is a terminal (Michael Paquier)</p>"
  ],
  [
    "<p>Fix assorted corner-case bugs in <span class=\"APPLICATION\">pg_dump</span>'s processing of extension member objects (Tom Lane)</p>"
  ],
  [
    "<p>Fix improper quoting of domain constraint names in <span class=\"APPLICATION\">pg_dump</span> (Elvis Pranskevichus)</p>"
  ],
  [
    "<p>Make <span class=\"APPLICATION\">pg_dump</span> mark a view's triggers as needing to be processed after its rule, to prevent possible failure during parallel <span class=\"APPLICATION\">pg_restore</span> (Tom Lane)</p>"
  ],
  [
    "<p>Install guards in <span class=\"APPLICATION\">pgbench</span> against corner-case overflow conditions during evaluation of script-specified division or modulo operators (Fabien Coelho, Michael Paquier)</p>"
  ],
  [
    "<p>Suppress useless warning message when <span class=\"APPLICATION\">pg_receivexlog</span> connects to a pre-9.4 server (Marco Nenciarini)</p>"
  ],
  [
    "<p>Avoid dump/reload problems when using both <span class=\"APPLICATION\">plpython2</span> and <span class=\"APPLICATION\">plpython3</span> (Tom Lane)</p>",
    "<p>In principle, both versions of <span class=\"APPLICATION\">PL/Python</span> can be used in the same database, though not in the same session (because the two versions of <span class=\"APPLICATION\">libpython</span> cannot safely be used concurrently). However, <span class=\"APPLICATION\">pg_restore</span> and <span class=\"APPLICATION\">pg_upgrade</span> both do things that can fall foul of the same-session restriction. Work around that by changing the timing of the check.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">PL/Python</span> regression tests to pass with Python 3.5 (Peter Eisentraut)</p>"
  ],
  [
    "<p>Prevent certain <span class=\"APPLICATION\">PL/Java</span> parameters from being set by non-superusers (Noah Misch)</p>",
    "<p>This change mitigates a <span class=\"APPLICATION\">PL/Java</span> security bug (CVE-2016-0766), which was fixed in <span class=\"APPLICATION\">PL/Java</span> by marking these parameters as superuser-only. To fix the security hazard for sites that update <span class=\"PRODUCTNAME\">PostgreSQL</span> more frequently than <span class=\"APPLICATION\">PL/Java</span>, make the core code aware of them also.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">ecpg</span>-supplied header files to not contain comments continued from a preprocessor directive line onto the next line (Michael Meskes)</p>",
    "<p>Such a comment is rejected by <span class=\"APPLICATION\">ecpg</span>. It's not yet clear whether <span class=\"APPLICATION\">ecpg</span> itself should be changed.</p>"
  ],
  [
    "<p>Fix <code class=\"FUNCTION\">hstore_to_json_loose()</code>'s test for whether an <tt class=\"TYPE\">hstore</tt> value can be converted to a JSON number (Tom Lane)</p>",
    "<p>Previously this function could be fooled by non-alphanumeric trailing characters, leading to emitting syntactically-invalid JSON.</p>"
  ],
  [
    "<p>In <tt class=\"FILENAME\">contrib/postgres_fdw</tt>, fix bugs triggered by use of <tt class=\"LITERAL\">tableoid</tt> in data-modifying commands (Etsuro Fujita, Robert Haas)</p>"
  ],
  [
    "<p>Fix ill-advised restriction of <tt class=\"LITERAL\">NAMEDATALEN</tt> to be less than 256 (Robert Haas, Tom Lane)</p>"
  ],
  [
    "<p>Improve reproducibility of build output by ensuring filenames are given to the linker in a fixed order (Christoph Berg)</p>",
    "<p>This avoids possible bitwise differences in the produced executable files from one build to the next.</p>"
  ],
  [
    "<p>Ensure that <tt class=\"FILENAME\">dynloader.h</tt> is included in the installed header files in MSVC builds (Bruce Momjian, Michael Paquier)</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"APPLICATION\">tzdata</span> release 2016a for DST law changes in Cayman Islands, Metlakatla, and Trans-Baikal Territory (Zabaykalsky Krai), plus historical corrections for Pakistan.</p>"
  ]
]