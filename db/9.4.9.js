[
  [
    "<p>Fix possible mis-evaluation of nested <tt class=\"LITERAL\">CASE</tt>-<tt class=\"LITERAL\">WHEN</tt> expressions (Heikki Linnakangas, Michael Paquier, Tom Lane)</p>",
    "<p>A <tt class=\"LITERAL\">CASE</tt> expression appearing within the test value subexpression of another <tt class=\"LITERAL\">CASE</tt> could become confused about whether its own test value was null or not. Also, inlining of a SQL function implementing the equality operator used by a <tt class=\"LITERAL\">CASE</tt> expression could result in passing the wrong test value to functions called within a <tt class=\"LITERAL\">CASE</tt> expression in the SQL function's body. If the test values were of different data types, a crash might result; moreover such situations could be abused to allow disclosure of portions of server memory. (CVE-2016-5423)</p>"
  ],
  [
    "<p>Fix client programs' handling of special characters in database and role names (Noah Misch, Nathan Bossart, Michael Paquier)</p>",
    "<p>Numerous places in <span class=\"APPLICATION\">vacuumdb</span> and other client programs could become confused by database and role names containing double quotes or backslashes. Tighten up quoting rules to make that safe. Also, ensure that when a conninfo string is used as a database name parameter to these programs, it is correctly treated as such throughout.</p>",
    "<p>Fix handling of paired double quotes in <span class=\"APPLICATION\">psql</span>'s <tt class=\"COMMAND\">\\connect</tt> and <tt class=\"COMMAND\">\\password</tt> commands to match the documentation.</p>",
    "<p>Introduce a new <tt class=\"OPTION\">-reuse-previous</tt> option in <span class=\"APPLICATION\">psql</span>'s <tt class=\"COMMAND\">\\connect</tt> command to allow explicit control of whether to re-use connection parameters from a previous connection. (Without this, the choice is based on whether the database name looks like a conninfo string, as before.) This allows secure handling of database names containing special characters in <span class=\"APPLICATION\">pg_dumpall</span> scripts.</p>",
    "<p><span class=\"APPLICATION\">pg_dumpall</span> now refuses to deal with database and role names containing carriage returns or newlines, as it seems impractical to quote those characters safely on Windows. In future we may reject such names on the server side, but that step has not been taken yet.</p>",
    "<p>These are considered security fixes because crafted object names containing special characters could have been used to execute commands with superuser privileges the next time a superuser executes <span class=\"APPLICATION\">pg_dumpall</span> or other routine maintenance operations. (CVE-2016-5424)</p>"
  ],
  [
    "<p>Fix corner-case misbehaviors for <tt class=\"LITERAL\">IS NULL</tt>/<tt class=\"LITERAL\">IS NOT NULL</tt> applied to nested composite values (Andrew Gierth, Tom Lane)</p>",
    "<p>The SQL standard specifies that <tt class=\"LITERAL\">IS NULL</tt> should return TRUE for a row of all null values (thus <tt class=\"LITERAL\">ROW(NULL,NULL) IS NULL</tt> yields TRUE), but this is not meant to apply recursively (thus <tt class=\"LITERAL\">ROW(NULL, ROW(NULL,NULL)) IS NULL</tt> yields FALSE). The core executor got this right, but certain planner optimizations treated the test as recursive (thus producing TRUE in both cases), and <tt class=\"FILENAME\">contrib/postgres_fdw</tt> could produce remote queries that misbehaved similarly.</p>"
  ],
  [
    "<p>Make the <tt class=\"TYPE\">inet</tt> and <tt class=\"TYPE\">cidr</tt> data types properly reject IPv6 addresses with too many colon-separated fields (Tom Lane)</p>"
  ],
  [
    "<p>Prevent crash in <code class=\"FUNCTION\">close_ps()</code> (the <tt class=\"TYPE\">point</tt> <tt class=\"LITERAL\">##</tt> <tt class=\"TYPE\">lseg</tt> operator) for NaN input coordinates (Tom Lane)</p>",
    "<p>Make it return NULL instead of crashing.</p>"
  ],
  [
    "<p>Avoid possible crash in <code class=\"FUNCTION\">pg_get_expr()</code> when inconsistent values are passed to it (Michael Paquier, Thomas Munro)</p>"
  ],
  [
    "<p>Fix several one-byte buffer over-reads in <code class=\"FUNCTION\">to_number()</code> (Peter Eisentraut)</p>",
    "<p>In several cases the <code class=\"FUNCTION\">to_number()</code> function would read one more character than it should from the input string. There is a small chance of a crash, if the input happens to be adjacent to the end of memory.</p>"
  ],
  [
    "<p>Do not run the planner on the query contained in <tt class=\"LITERAL\">CREATE MATERIALIZED VIEW</tt> or <tt class=\"LITERAL\">CREATE TABLE AS</tt> when <tt class=\"LITERAL\">WITH NO DATA</tt> is specified (Michael Paquier, Tom Lane)</p>",
    "<p>This avoids some unnecessary failure conditions, for example if a stable function invoked by the materialized view depends on a table that doesn't exist yet.</p>"
  ],
  [
    "<p>Avoid unsafe intermediate state during expensive paths through <code class=\"FUNCTION\">heap_update()</code> (Masahiko Sawada, Andres Freund)</p>",
    "<p>Previously, these cases locked the target tuple (by setting its XMAX) but did not WAL-log that action, thus risking data integrity problems if the page were spilled to disk and then a database crash occurred before the tuple update could be completed.</p>"
  ],
  [
    "<p>Fix hint bit update during WAL replay of row locking operations (Andres Freund)</p>",
    "<p>The only known consequence of this problem is that row locks held by a prepared, but uncommitted, transaction might fail to be enforced after a crash and restart.</p>"
  ],
  [
    "<p>Avoid unnecessary <span class=\"QUOTE\">\"could not serialize access\"</span> errors when acquiring <tt class=\"LITERAL\">FOR KEY SHARE</tt> row locks in serializable mode (Álvaro Herrera)</p>"
  ],
  [
    "<p>Avoid crash in <tt class=\"LITERAL\">postgres -C</tt> when the specified variable has a null string value (Michael Paquier)</p>"
  ],
  [
    "<p>Fix possible loss of large subtransactions in logical decoding (Petru-Florin Mihancea)</p>"
  ],
  [
    "<p>Fix failure of logical decoding when a subtransaction contains no actual changes (Marko Tiikkaja, Andrew Gierth)</p>"
  ],
  [
    "<p>Ensure that backends see up-to-date statistics for shared catalogs (Tom Lane)</p>",
    "<p>The statistics collector failed to update the statistics file for shared catalogs after a request from a regular backend. This problem was partially masked because the autovacuum launcher regularly makes requests that did cause such updates; however, it became obvious with autovacuum disabled.</p>"
  ],
  [
    "<p>Avoid redundant writes of the statistics files when multiple backends request updates close together (Tom Lane, Tomas Vondra)</p>"
  ],
  [
    "<p>Avoid consuming a transaction ID during <tt class=\"COMMAND\">VACUUM</tt> (Alexander Korotkov)</p>",
    "<p>Some cases in <tt class=\"COMMAND\">VACUUM</tt> unnecessarily caused an XID to be assigned to the current transaction. Normally this is negligible, but if one is up against the XID wraparound limit, consuming more XIDs during anti-wraparound vacuums is a very bad thing.</p>"
  ],
  [
    "<p>Avoid canceling hot-standby queries during <tt class=\"COMMAND\">VACUUM FREEZE</tt> (Simon Riggs, Álvaro Herrera)</p>",
    "<p><tt class=\"COMMAND\">VACUUM FREEZE</tt> on an otherwise-idle master server could result in unnecessary cancellations of queries on its standby servers.</p>"
  ],
  [
    "<p>Prevent possible failure when vacuuming multixact IDs in an installation that has been pg_upgrade'd from pre-9.3 (Andrew Gierth, Álvaro Herrera)</p>",
    "<p>The usual symptom of this bug is errors like <span class=\"QUOTE\">\"MultiXactId <tt class=\"REPLACEABLE c2\">NNN</tt> has not been created yet -- apparent wraparound\"</span>.</p>"
  ],
  [
    "<p>When a manual <tt class=\"COMMAND\">ANALYZE</tt> specifies a column list, don't reset the table's <tt class=\"LITERAL\">changes_since_analyze</tt> counter (Tom Lane)</p>",
    "<p>If we're only analyzing some columns, we should not prevent routine auto-analyze from happening for the other columns.</p>"
  ],
  [
    "<p>Fix <tt class=\"COMMAND\">ANALYZE</tt>'s overestimation of <tt class=\"LITERAL\">n_distinct</tt> for a unique or nearly-unique column with many null entries (Tom Lane)</p>",
    "<p>The nulls could get counted as though they were themselves distinct values, leading to serious planner misestimates in some types of queries.</p>"
  ],
  [
    "<p>Prevent autovacuum from starting multiple workers for the same shared catalog (Álvaro Herrera)</p>",
    "<p>Normally this isn't much of a problem because the vacuum doesn't take long anyway; but in the case of a severely bloated catalog, it could result in all but one worker uselessly waiting instead of doing useful work on other tables.</p>"
  ],
  [
    "<p>Avoid duplicate buffer lock release when abandoning a b-tree index page deletion attempt (Tom Lane)</p>",
    "<p>This mistake prevented <tt class=\"COMMAND\">VACUUM</tt> from completing in some cases involving corrupt b-tree indexes.</p>"
  ],
  [
    "<p>Prevent infinite loop in GiST index build for geometric columns containing NaN component values (Tom Lane)</p>"
  ],
  [
    "<p>Fix <tt class=\"FILENAME\">contrib/btree_gin</tt> to handle the smallest possible <tt class=\"TYPE\">bigint</tt> value correctly (Peter Eisentraut)</p>"
  ],
  [
    "<p>Teach libpq to correctly decode server version from future servers (Peter Eisentraut)</p>",
    "<p>It's planned to switch to two-part instead of three-part server version numbers for releases after 9.6. Make sure that <code class=\"FUNCTION\">PQserverVersion()</code> returns the correct value for such cases.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">ecpg</span>'s code for <tt class=\"LITERAL\">unsigned long long</tt> array elements (Michael Meskes)</p>"
  ],
  [
    "<p>In <span class=\"APPLICATION\">pg_dump</span> with both <tt class=\"OPTION\">-c</tt> and <tt class=\"OPTION\">-C</tt> options, avoid emitting an unwanted <tt class=\"LITERAL\">CREATE SCHEMA public</tt> command (David Johnston, Tom Lane)</p>"
  ],
  [
    "<p>Improve handling of <span class=\"SYSTEMITEM\">SIGTERM</span>/control-C in parallel <span class=\"APPLICATION\">pg_dump</span> and <span class=\"APPLICATION\">pg_restore</span> (Tom Lane)</p>",
    "<p>Make sure that the worker processes will exit promptly, and also arrange to send query-cancel requests to the connected backends, in case they are doing something long-running such as a <tt class=\"COMMAND\">CREATE INDEX</tt>.</p>"
  ],
  [
    "<p>Fix error reporting in parallel <span class=\"APPLICATION\">pg_dump</span> and <span class=\"APPLICATION\">pg_restore</span> (Tom Lane)</p>",
    "<p>Previously, errors reported by <span class=\"APPLICATION\">pg_dump</span> or <span class=\"APPLICATION\">pg_restore</span> worker processes might never make it to the user's console, because the messages went through the master process, and there were various deadlock scenarios that would prevent the master process from passing on the messages. Instead, just print everything to <tt class=\"LITERAL\">stderr</tt>. In some cases this will result in duplicate messages (for instance, if all the workers report a server shutdown), but that seems better than no message.</p>"
  ],
  [
    "<p>Ensure that parallel <span class=\"APPLICATION\">pg_dump</span> or <span class=\"APPLICATION\">pg_restore</span> on Windows will shut down properly after an error (Kyotaro Horiguchi)</p>",
    "<p>Previously, it would report the error, but then just sit until manually stopped by the user.</p>"
  ],
  [
    "<p>Make <span class=\"APPLICATION\">pg_dump</span> behave better when built without zlib support (Kyotaro Horiguchi)</p>",
    "<p>It didn't work right for parallel dumps, and emitted some rather pointless warnings in other cases.</p>"
  ],
  [
    "<p>Make <span class=\"APPLICATION\">pg_basebackup</span> accept <tt class=\"LITERAL\">-Z 0</tt> as specifying no compression (Fujii Masao)</p>"
  ],
  [
    "<p>Fix makefiles' rule for building AIX shared libraries to be safe for parallel make (Noah Misch)</p>"
  ],
  [
    "<p>Fix TAP tests and MSVC scripts to work when build directory's path name contains spaces (Michael Paquier, Kyotaro Horiguchi)</p>"
  ],
  [
    "<p>Be more predictable about reporting <span class=\"QUOTE\">\"statement timeout\"</span> versus <span class=\"QUOTE\">\"lock timeout\"</span> (Tom Lane)</p>",
    "<p>On heavily loaded machines, the regression tests sometimes failed due to reporting <span class=\"QUOTE\">\"lock timeout\"</span> even though the statement timeout should have occurred first.</p>"
  ],
  [
    "<p>Make regression tests safe for Danish and Welsh locales (Jeff Janes, Tom Lane)</p>",
    "<p>Change some test data that triggered the unusual sorting rules of these locales.</p>"
  ],
  [
    "<p>Update our copy of the timezone code to match IANA's <span class=\"APPLICATION\">tzcode</span> release 2016c (Tom Lane)</p>",
    "<p>This is needed to cope with anticipated future changes in the time zone data files. It also fixes some corner-case bugs in coping with unusual time zones.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"APPLICATION\">tzdata</span> release 2016f for DST law changes in Kemerovo and Novosibirsk, plus historical corrections for Azerbaijan, Belarus, and Morocco.</p>"
  ]
]