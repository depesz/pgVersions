[
  [
    "<p>Disallow <code class=\"command\">LOAD</code> to\n          non-superusers</p>",
    "<p>On platforms that will automatically execute\n          initialization functions of a shared library (this\n          includes at least Windows and ELF-based Unixen),\n          <code class=\"command\">LOAD</code> can be used to make the\n          server execute arbitrary code. Thanks to NGS Software for\n          reporting this.</p>"
  ],
  [
    "<p>Add needed STRICT marking to some contrib functions\n          (Kris Jurka)</p>"
  ],
  [
    "<p>Avoid buffer overrun when plpgsql cursor declaration\n          has too many parameters (Neil)</p>"
  ],
  [
    "<p>Fix planning error for FULL and RIGHT outer joins</p>",
    "<p>The result of the join was mistakenly supposed to be\n          sorted the same as the left input. This could not only\n          deliver mis-sorted output to the user, but in case of\n          nested merge joins could give outright wrong answers.</p>"
  ],
  [
    "<p>Fix display of negative intervals in SQL and GERMAN\n          datestyles</p>"
  ]
]