[
  [
    "<p>Fix incorrect handling of NULL index entries in\n          indexed <code class=\"literal\">ROW()</code> comparisons\n          (Tom Lane)</p>",
    "<p>An index search using a row comparison such as\n          <code class=\"literal\">ROW(a, b) &gt; ROW('x', 'y')</code>\n          would stop upon reaching a NULL entry in the <code class=\"structfield\">b</code> column, ignoring the fact that\n          there might be non-NULL <code class=\"structfield\">b</code> values associated with later\n          values of <code class=\"structfield\">a</code>.</p>"
  ],
  [
    "<p>Avoid unlikely data-loss scenarios due to renaming\n          files without adequate <code class=\"function\">fsync()</code> calls before and after (Michael\n          Paquier, Tomas Vondra, Andres Freund)</p>"
  ],
  [
    "<p>Correctly handle cases where <code class=\"literal\">pg_subtrans</code> is close to XID wraparound\n          during server startup (Jeff Janes)</p>"
  ],
  [
    "<p>Fix corner-case crash due to trying to free\n          <code class=\"function\">localeconv()</code> output strings\n          more than once (Tom Lane)</p>"
  ],
  [
    "<p>Fix parsing of affix files for <code class=\"literal\">ispell</code> dictionaries (Tom Lane)</p>",
    "<p>The code could go wrong if the affix file contained\n          any characters whose byte length changes during\n          case-folding, for example <code class=\"literal\">I</code>\n          in Turkish UTF8 locales.</p>"
  ],
  [
    "<p>Avoid use of <code class=\"function\">sscanf()</code> to\n          parse <code class=\"literal\">ispell</code> dictionary\n          files (Artur Zakirov)</p>",
    "<p>This dodges a portability problem on FreeBSD-derived\n          platforms (including macOS).</p>"
  ],
  [
    "<p>Avoid a crash on old Windows versions (before\n          7SP1/2008R2SP1) with an AVX2-capable CPU and a Postgres\n          build done with Visual Studio 2013 (Christian\n          Ullrich)</p>",
    "<p>This is a workaround for a bug in Visual Studio 2013's\n          runtime library, which Microsoft have stated they will\n          not fix in that version.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">psql</span>'s tab\n          completion logic to handle multibyte characters properly\n          (Kyotaro Horiguchi, Robert Haas)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">psql</span>'s tab\n          completion for <code class=\"literal\">SECURITY\n          LABEL</code> (Tom Lane)</p>",
    "<p>Pressing TAB after <code class=\"literal\">SECURITY\n          LABEL</code> might cause a crash or offering of\n          inappropriate keywords.</p>"
  ],
  [
    "<p>Make <span class=\"application\">pg_ctl</span> accept a\n          wait timeout from the <code class=\"envar\">PGCTLTIMEOUT</code> environment variable, if none\n          is specified on the command line (Noah Misch)</p>",
    "<p>This eases testing of slower buildfarm members by\n          allowing them to globally specify a longer-than-normal\n          timeout for postmaster startup and shutdown.</p>"
  ],
  [
    "<p>Fix incorrect test for Windows service status in\n          <span class=\"application\">pg_ctl</span> (Manuel\n          Mathar)</p>",
    "<p>The previous set of minor releases attempted to fix\n          <span class=\"application\">pg_ctl</span> to properly\n          determine whether to send log messages to Window's Event\n          Log, but got the test backwards.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pgbench</span> to\n          correctly handle the combination of <code class=\"literal\">-C</code> and <code class=\"literal\">-M\n          prepared</code> options (Tom Lane)</p>"
  ],
  [
    "<p>In <span class=\"application\">pg_upgrade</span>, skip\n          creating a deletion script when the new data directory is\n          inside the old data directory (Bruce Momjian)</p>",
    "<p>Blind application of the script in such cases would\n          result in loss of the new data directory.</p>"
  ],
  [
    "<p>In PL/Perl, properly translate empty Postgres arrays\n          into empty Perl arrays (Alex Hunsaker)</p>"
  ],
  [
    "<p>Make PL/Python cope with function names that aren't\n          valid Python identifiers (Jim Nasby)</p>"
  ],
  [
    "<p>Fix multiple mistakes in the statistics returned by\n          <code class=\"filename\">contrib/pgstattuple</code>'s\n          <code class=\"function\">pgstatindex()</code> function (Tom\n          Lane)</p>"
  ],
  [
    "<p>Remove dependency on <code class=\"literal\">psed</code>\n          in MSVC builds, since it's no longer provided by core\n          Perl (Michael Paquier, Andrew Dunstan)</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2016c for DST law\n          changes in Azerbaijan, Chile, Haiti, Palestine, and\n          Russia (Altai, Astrakhan, Kirov, Sakhalin, Ulyanovsk\n          regions), plus historical corrections for Lithuania,\n          Moldova, and Russia (Kaliningrad, Samara, Volgograd).</p>"
  ]
]