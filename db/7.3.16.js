[
  [
    "<p>Fix corner cases in pattern matching for <span class=\"application\">psql</span>'s <code class=\"literal\">\\d</code> commands</p>"
  ],
  [
    "<p>Fix index-corrupting bugs in /contrib/ltree\n          (Teodor)</p>"
  ],
  [
    "<p>Back-port 7.4 spinlock code to improve performance and\n          support 64-bit architectures better</p>"
  ],
  [
    "<p>Fix SSL-related memory leak in libpq</p>"
  ],
  [
    "<p>Fix backslash escaping in /contrib/dbmirror</p>"
  ],
  [
    "<p>Adjust regression tests for recent changes in US DST\n          laws</p>"
  ]
]