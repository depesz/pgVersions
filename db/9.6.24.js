[
  [
    "<p>Make the server reject extraneous data after an SSL or GSS encryption handshake (Tom Lane)</p>",
    "<p>A man-in-the-middle with the ability to inject data into the TCP connection could stuff some cleartext data into the start of a supposedly encryption-protected database session. This could be abused to send faked SQL commands to the server, although that would only work if the server did not demand any authentication data. (However, a server relying on SSL certificate authentication might well not do so.)</p>",
    "<p>The <span class=\"PRODUCTNAME\">PostgreSQL</span> Project thanks Jacob Champion for reporting this problem. (CVE-2021-23214)</p>"
  ],
  [
    "<p>Make <span class=\"APPLICATION\">libpq</span> reject extraneous data after an SSL or GSS encryption handshake (Tom Lane)</p>",
    "<p>A man-in-the-middle with the ability to inject data into the TCP connection could stuff some cleartext data into the start of a supposedly encryption-protected database session. This could probably be abused to inject faked responses to the client's first few queries, although other details of libpq's behavior make that harder than it sounds. A different line of attack is to exfiltrate the client's password, or other sensitive data that might be sent early in the session. That has been shown to be possible with a server vulnerable to CVE-2021-23214.</p>",
    "<p>The <span class=\"PRODUCTNAME\">PostgreSQL</span> Project thanks Jacob Champion for reporting this problem. (CVE-2021-23222)</p>"
  ],
  [
    "<p>Fix physical replication for cases where the primary crashes after shipping a WAL segment that ends with a partial WAL record (Álvaro Herrera)</p>",
    "<p>If the primary did not survive long enough to finish writing the rest of the incomplete WAL record, then the previous crash-recovery logic had it back up and overwrite WAL starting from the beginning of the incomplete WAL record. This is problematic since standby servers may already have copies of that WAL segment. They will then see an inconsistent next segment, and will not be able to recover without manual intervention. To fix, do not back up over a WAL segment boundary when restarting after a crash. Instead write a new type of WAL record at the start of the next WAL segment, informing readers that the incomplete WAL record will never be finished and must be disregarded.</p>",
    "<p>When applying this update, it's best to update standby servers before the primary, so that they will be ready to handle this new WAL record type if the primary happens to crash.</p>"
  ],
  [
    "<p>Fix <tt class=\"COMMAND\">CREATE INDEX CONCURRENTLY</tt> to wait for the latest prepared transactions (Andrey Borodin)</p>",
    "<p>Rows inserted by just-prepared transactions might be omitted from the new index, causing queries relying on the index to miss such rows. The previous fix for this type of problem failed to account for <tt class=\"COMMAND\">PREPARE TRANSACTION</tt> commands that were still in progress when <tt class=\"COMMAND\">CREATE INDEX CONCURRENTLY</tt> checked for them. As before, in installations that have enabled prepared transactions (<tt class=\"VARNAME\">max_prepared_transactions</tt> &gt; 0), it's recommended to reindex any concurrently-built indexes in case this problem occurred when they were built.</p>"
  ],
  [
    "<p>Avoid race condition that can cause backends to fail to add entries for new rows to an index being built concurrently (Noah Misch, Andrey Borodin)</p>",
    "<p>While it's apparently rare in the field, this case could potentially affect any index built or reindexed with the <tt class=\"LITERAL\">CONCURRENTLY</tt> option. It is recommended to reindex any such indexes to make sure they are correct.</p>"
  ],
  [
    "<p>Fix <tt class=\"TYPE\">float4</tt> and <tt class=\"TYPE\">float8</tt> hash functions to produce uniform results for NaNs (Tom Lane)</p>",
    "<p>Since <span class=\"PRODUCTNAME\">PostgreSQL</span>'s floating-point types deem all NaNs to be equal, it's important for the hash functions to produce the same hash code for all bit-patterns that are NaNs according to the IEEE 754 standard. This failed to happen before, meaning that hash indexes and hash-based query plans might produce incorrect results for non-canonical NaN values. (<tt class=\"LITERAL\">'-NaN'::float8</tt> is one way to produce such a value on most machines.) It is advisable to reindex hash indexes on floating-point columns, if there is any possibility that they might contain such values.</p>"
  ],
  [
    "<p>Prevent data loss during crash recovery of <tt class=\"COMMAND\">CREATE TABLESPACE</tt>, when <tt class=\"VARNAME\">wal_level</tt> = <tt class=\"LITERAL\">minimal</tt> (Noah Misch)</p>",
    "<p>If the server crashed between <tt class=\"COMMAND\">CREATE TABLESPACE</tt> and the next checkpoint, replay would fully remove the contents of the new tablespace's directory, relying on subsequent WAL replay to restore everything within that directory. This interacts badly with optimizations that skip writing WAL (one example is <tt class=\"COMMAND\">COPY</tt> into a just-created table). Such optimizations are applied only when <tt class=\"VARNAME\">wal_level</tt> is <tt class=\"LITERAL\">minimal</tt>, which is not the default in v10 and later.</p>"
  ],
  [
    "<p>Don't discard a cast to the same type with unspecified type modifier (Tom Lane)</p>",
    "<p>For example, if column <tt class=\"LITERAL\">f1</tt> is of type <tt class=\"LITERAL\">numeric(18,3)</tt>, the parser used to simply discard a cast like <tt class=\"LITERAL\">f1::numeric</tt>, on the grounds that it would have no run-time effect. That's true, but the exposed type of the expression should still be considered to be plain <tt class=\"LITERAL\">numeric</tt>, not <tt class=\"LITERAL\">numeric(18,3)</tt>. This is important for correctly resolving the type of larger constructs, such as recursive <tt class=\"LITERAL\">UNION</tt>s.</p>"
  ],
  [
    "<p>Fix corner-case loss of precision in numeric <code class=\"FUNCTION\">power()</code> (Dean Rasheed)</p>",
    "<p>The result could be inaccurate when the first argument is very close to 1.</p>"
  ],
  [
    "<p>Avoid regular expression errors with capturing parentheses inside <tt class=\"LITERAL\">{0}</tt> (Tom Lane)</p>",
    "<p>Regular expressions like <tt class=\"LITERAL\">(.){0}...\\1</tt> drew <span class=\"QUOTE\">\"invalid backreference number\"</span>. Other regexp engines such as Perl don't complain, though, and for that matter ours doesn't either in some closely related cases. Worse, it could throw an assertion failure instead. Fix it so that no error is thrown and instead the back-reference is silently deemed to never match.</p>"
  ],
  [
    "<p>Prevent regular expression back-references from sometimes matching when they shouldn't (Tom Lane)</p>",
    "<p>The regexp engine was careless about clearing match data for capturing parentheses after rejecting a partial match. This could allow a later back-reference to match in places where it should fail for lack of a defined referent.</p>"
  ],
  [
    "<p>Fix regular expression performance bug with back-references inside iteration nodes (Tom Lane)</p>",
    "<p>Incorrect back-tracking logic could result in exponential time spent looking for a match. Fortunately the problem is masked in most cases by other optimizations.</p>"
  ],
  [
    "<p>Fix incorrect results from <tt class=\"LITERAL\">AT TIME ZONE</tt> applied to a <tt class=\"TYPE\">time with time zone</tt> value (Tom Lane)</p>",
    "<p>The results were incorrect if the target time zone was specified by a dynamic timezone abbreviation (that is, one that is defined as equivalent to a full time zone name, rather than a fixed UTC offset).</p>"
  ],
  [
    "<p>Clean up correctly if a transaction fails after exporting its snapshot (Dilip Kumar)</p>",
    "<p>This oversight would only cause a problem if the same session attempted to export a snapshot again. The most likely scenario for that is creation of a replication slot (followed by rollback) and then creation of another replication slot.</p>"
  ],
  [
    "<p>Prevent wraparound of overflowed-subtransaction tracking on standby servers (Kyotaro Horiguchi, Alexander Korotkov)</p>",
    "<p>This oversight could cause significant performance degradation (manifesting as excessive SubtransSLRU traffic) on standby servers.</p>"
  ],
  [
    "<p>Ensure that prepared transactions are properly accounted for during promotion of a standby server (Michael Paquier, Andres Freund)</p>",
    "<p>There was a narrow window where a prepared transaction could be omitted from a snapshot taken by a concurrently-running session. If that session then used the snapshot to perform data updates, erroneous results or data corruption could occur.</p>"
  ],
  [
    "<p>Fix detection of a relation that has grown to the maximum allowed length (Tom Lane)</p>",
    "<p>An attempt to extend a table or index past the limit of 2^32-1 blocks was rejected, but not soon enough to prevent inconsistent internal state from being created.</p>"
  ],
  [
    "<p>Correctly track the presence of data-modifying CTEs when expanding a <tt class=\"LITERAL\">DO INSTEAD</tt> rule (Greg Nancarrow, Tom Lane)</p>",
    "<p>The previous failure to do this could lead to problems such as unsafely choosing a parallel plan.</p>"
  ],
  [
    "<p>Ensure that walreceiver processes create all required archive notification files before exiting (Fujii Masao)</p>",
    "<p>If a walreceiver exited exactly at a WAL segment boundary, it failed to make a notification file for the last-received segment, thus delaying archiving of that segment on the standby.</p>"
  ],
  [
    "<p>Avoid trying to lock the <tt class=\"LITERAL\">OLD</tt> and <tt class=\"LITERAL\">NEW</tt> pseudo-relations in a rule that uses <tt class=\"LITERAL\">SELECT FOR UPDATE</tt> (Masahiko Sawada, Tom Lane)</p>"
  ],
  [
    "<p>Fix parser's processing of aggregate <tt class=\"LITERAL\">FILTER</tt> clauses (Tom Lane)</p>",
    "<p>If the <tt class=\"LITERAL\">FILTER</tt> expression is a plain boolean column, the semantic level of the aggregate could be mis-determined, leading to not-per-spec behavior. If the <tt class=\"LITERAL\">FILTER</tt> expression is itself a boolean-returning aggregate, an error should be thrown but was not, likely resulting in a crash at execution.</p>"
  ],
  [
    "<p>Avoid null-pointer-dereference crash when dropping a role that owns objects being dropped concurrently (Álvaro Herrera)</p>"
  ],
  [
    "<p>Prevent <span class=\"QUOTE\">\"snapshot reference leak\"</span> warning when <code class=\"FUNCTION\">lo_export()</code> or a related function fails (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Ensure that scans of SP-GiST indexes are counted in the statistics views (Tom Lane)</p>",
    "<p>Incrementing the number-of-index-scans counter was overlooked in the SP-GiST code, although per-tuple counters were advanced correctly.</p>"
  ],
  [
    "<p>Recalculate relevant wait intervals if <tt class=\"VARNAME\">recovery_min_apply_delay</tt> is changed during recovery (Soumyadeep Chakraborty, Ashwin Agrawal)</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">ecpg</span> to recover correctly after <code class=\"FUNCTION\">malloc()</code> failure while establishing a connection (Michael Paquier)</p>"
  ],
  [
    "<p>Allow <tt class=\"LITERAL\">EXIT</tt> out of the outermost block in a PL/pgSQL routine (Tom Lane)</p>",
    "<p>If the routine does not require an explicit <tt class=\"LITERAL\">RETURN</tt>, this usage should be valid, but it was rejected.</p>"
  ],
  [
    "<p>Remove <span class=\"APPLICATION\">pg_ctl</span>'s hard-coded limits on the total length of generated commands (Phil Krylov)</p>",
    "<p>For example, this removes a restriction on how many command-line options can be passed through to the postmaster. Individual path names that <span class=\"APPLICATION\">pg_ctl</span> deals with, such as the postmaster executable's name or the data directory name, are still limited to <tt class=\"LITERAL\">MAXPGPATH</tt> bytes in most cases.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_dump</span> to dump non-global default privileges correctly (Neil Chen, Masahiko Sawada)</p>",
    "<p>If a global (unrestricted) <tt class=\"COMMAND\">ALTER DEFAULT PRIVILEGES</tt> command revoked some present-by-default privilege, for example <tt class=\"LITERAL\">EXECUTE</tt> for functions, and then a restricted <tt class=\"COMMAND\">ALTER DEFAULT PRIVILEGES</tt> command granted that privilege again for a selected role or schema, <span class=\"APPLICATION\">pg_dump</span> failed to dump the restricted privilege grant correctly.</p>"
  ],
  [
    "<p>Improve <span class=\"APPLICATION\">pg_dump</span>'s performance by avoiding making per-table queries for RLS policies, and by avoiding repetitive calls to <code class=\"FUNCTION\">format_type()</code> (Tom Lane)</p>",
    "<p>These changes provide only marginal improvement when dumping from a local server, but a dump from a remote server can benefit substantially due to fewer network round-trips.</p>"
  ],
  [
    "<p>Fix incorrect filename in <span class=\"APPLICATION\">pg_restore</span>'s error message about an invalid large object TOC file (Daniel Gustafsson)</p>"
  ],
  [
    "<p>Fix failure of <tt class=\"FILENAME\">contrib/btree_gin</tt> indexes on <tt class=\"TYPE\">\"char\"</tt> (not <tt class=\"TYPE\">char(<tt class=\"REPLACEABLE c2\">n</tt>)</tt>) columns, when an indexscan using the <tt class=\"LITERAL\">&lt;</tt> or <tt class=\"LITERAL\">&lt;=</tt> operator is performed (Tom Lane)</p>",
    "<p>Such an indexscan failed to return all the entries it should.</p>"
  ],
  [
    "<p>Change <tt class=\"FILENAME\">contrib/pg_stat_statements</tt> to read its <span class=\"QUOTE\">\"query texts\"</span> file in units of at most 1GB (Tom Lane)</p>",
    "<p>Such large query text files are very unusual, but if they do occur, the previous coding would fail on Windows 64 (which rejects individual read requests of more than 2GB).</p>"
  ],
  [
    "<p>Fix null-pointer crash when <tt class=\"FILENAME\">contrib/postgres_fdw</tt> tries to report a data conversion error (Tom Lane)</p>"
  ],
  [
    "<p>Add spinlock support for the RISC-V architecture (Marek Szuba)</p>",
    "<p>This is essential for reasonable performance on that platform.</p>"
  ],
  [
    "<p>Set correct type identifier on OpenSSL BIO (I/O abstraction) objects created by <span class=\"PRODUCTNAME\">PostgreSQL</span> (Itamar Gafni)</p>",
    "<p>This oversight probably only matters for code that is doing tasks like auditing the OpenSSL installation. But it's nominally a violation of the OpenSSL API, so fix it.</p>"
  ],
  [
    "<p>Make <code class=\"FUNCTION\">pg_regexec()</code> robust against an out-of-range <tt class=\"REPLACEABLE c2\">search_start</tt> parameter (Tom Lane)</p>",
    "<p>Return <tt class=\"LITERAL\">REG_NOMATCH</tt>, instead of possibly crashing, when <tt class=\"REPLACEABLE c2\">search_start</tt> is past the end of the string. This case is probably unreachable within core <span class=\"PRODUCTNAME\">PostgreSQL</span>, but extensions might be more careless about the parameter value.</p>"
  ],
  [
    "<p>Ensure that <code class=\"FUNCTION\">GetSharedSecurityLabel()</code> can be used in a newly-started session that has not yet built its critical relation cache entries (Jeff Davis)</p>"
  ],
  [
    "<p>Use the CLDR project's data to map Windows time zone names to IANA time zones (Tom Lane)</p>",
    "<p>When running on Windows, <span class=\"APPLICATION\">initdb</span> attempts to set the new cluster's <tt class=\"VARNAME\">timezone</tt> parameter to the IANA time zone matching the system's prevailing time zone. We were using a mapping table that we'd generated years ago and updated only fitfully; unsurprisingly, it contained a number of errors as well as omissions of recently-added zones. It turns out that CLDR has been tracking the most appropriate mappings, so start using their data. This change will not affect any existing installation, only newly-initialized clusters.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"APPLICATION\">tzdata</span> release 2021e for DST law changes in Fiji, Jordan, Palestine, and Samoa, plus historical corrections for Barbados, Cook Islands, Guyana, Niue, Portugal, and Tonga.</p>",
    "<p>Also, the Pacific/Enderbury zone has been renamed to Pacific/Kanton. Also, the following zones have been merged into nearby, more-populous zones whose clocks have agreed with them since 1970: Africa/Accra, America/Atikokan, America/Blanc-Sablon, America/Creston, America/Curacao, America/Nassau, America/Port_of_Spain, Antarctica/DumontDUrville, and Antarctica/Syowa. In all these cases, the previous zone name remains as an alias.</p>"
  ]
]