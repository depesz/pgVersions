[
  [
    "<p>Avoid failures when <code class=\"command\">EXPLAIN</code> tries to display a simple-form\n          <code class=\"literal\">CASE</code> expression (Tom\n          Lane)</p>",
    "<p>If the <code class=\"literal\">CASE</code>'s test\n          expression was a constant, the planner could simplify the\n          <code class=\"literal\">CASE</code> into a form that\n          confused the expression-display code, resulting in\n          <span class=\"quote\">&#x201C;<span class=\"quote\">unexpected CASE\n          WHEN clause</span>&#x201D;</span> errors.</p>"
  ],
  [
    "<p>Fix assignment to an array slice that is before the\n          existing range of subscripts (Tom Lane)</p>",
    "<p>If there was a gap between the newly added subscripts\n          and the first pre-existing subscript, the code\n          miscalculated how many entries needed to be copied from\n          the old array's null bitmap, potentially leading to data\n          corruption or crash.</p>"
  ],
  [
    "<p>Avoid unexpected conversion overflow in planner for\n          very distant date values (Tom Lane)</p>",
    "<p>The <code class=\"type\">date</code> type supports a\n          wider range of dates than can be represented by the\n          <code class=\"type\">timestamp</code> types, but the\n          planner assumed it could always convert a date to\n          timestamp with impunity.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_restore</span>'s text\n          output for large objects (BLOBs) when <code class=\"varname\">standard_conforming_strings</code> is on (Tom\n          Lane)</p>",
    "<p>Although restoring directly to a database worked\n          correctly, string escaping was incorrect if <span class=\"application\">pg_restore</span> was asked for SQL text\n          output and <code class=\"varname\">standard_conforming_strings</code> had been\n          enabled in the source database.</p>"
  ],
  [
    "<p>Fix erroneous parsing of <code class=\"type\">tsquery</code> values containing <code class=\"literal\">... &amp; !(subexpression) | ...</code> (Tom\n          Lane)</p>",
    "<p>Queries containing this combination of operators were\n          not executed correctly. The same error existed in\n          <code class=\"filename\">contrib/intarray</code>'s\n          <code class=\"type\">query_int</code> type and <code class=\"filename\">contrib/ltree</code>'s <code class=\"type\">ltxtquery</code> type.</p>"
  ],
  [
    "<p>Fix buffer overrun in <code class=\"filename\">contrib/intarray</code>'s input function for\n          the <code class=\"type\">query_int</code> type (Apple)</p>",
    "<p>This bug is a security risk since the function's\n          return address could be overwritten. Thanks to Apple\n          Inc's security team for reporting this issue and\n          supplying the fix. (CVE-2010-4015)</p>"
  ],
  [
    "<p>Fix bug in <code class=\"filename\">contrib/seg</code>'s\n          GiST picksplit algorithm (Alexander Korotkov)</p>",
    "<p>This could result in considerable inefficiency, though\n          not actually incorrect answers, in a GiST index on a\n          <code class=\"type\">seg</code> column. If you have such an\n          index, consider <code class=\"command\">REINDEX</code>ing\n          it after installing this update. (This is identical to\n          the bug that was fixed in <code class=\"filename\">contrib/cube</code> in the previous\n          update.)</p>"
  ]
]