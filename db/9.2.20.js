[
  [
    "<p>Fix a race condition that could cause indexes built\n          with <code class=\"command\">CREATE INDEX\n          CONCURRENTLY</code> to be corrupt (Pavan Deolasee, Tom\n          Lane)</p>",
    "<p>If <code class=\"command\">CREATE INDEX\n          CONCURRENTLY</code> was used to build an index that\n          depends on a column not previously indexed, then rows\n          updated by transactions that ran concurrently with the\n          <code class=\"command\">CREATE INDEX</code> command could\n          have received incorrect index entries. If you suspect\n          this may have happened, the most reliable solution is to\n          rebuild affected indexes after installing this\n          update.</p>"
  ],
  [
    "<p>Unconditionally WAL-log creation of the <span class=\"quote\">&#x201C;<span class=\"quote\">init fork</span>&#x201D;</span> for\n          an unlogged table (Michael Paquier)</p>",
    "<p>Previously, this was skipped when <a class=\"xref\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-wal.html#GUC-WAL-LEVEL\">wal_level</a>\n          = <code class=\"literal\">minimal</code>, but actually it's\n          necessary even in that case to ensure that the unlogged\n          table is properly reset to empty after a crash.</p>"
  ],
  [
    "<p>Fix WAL page header validation when re-reading\n          segments (Takayuki Tsunakawa, Amit Kapila)</p>",
    "<p>In corner cases, a spurious <span class=\"quote\">&#x201C;<span class=\"quote\">out-of-sequence\n          TLI</span>&#x201D;</span> error could be reported during\n          recovery.</p>"
  ],
  [
    "<p>If the stats collector dies during hot standby,\n          restart it (Takayuki Tsunakawa)</p>"
  ],
  [
    "<p>Check for interrupts while hot standby is waiting for\n          a conflicting query (Simon Riggs)</p>"
  ],
  [
    "<p>Avoid constantly respawning the autovacuum launcher in\n          a corner case (Amit Khandekar)</p>",
    "<p>This fix avoids problems when autovacuum is nominally\n          off and there are some tables that require freezing, but\n          all such tables are already being processed by autovacuum\n          workers.</p>"
  ],
  [
    "<p>Fix check for when an extension member object can be\n          dropped (Tom Lane)</p>",
    "<p>Extension upgrade scripts should be able to drop\n          member objects, but this was disallowed for serial-column\n          sequences, and possibly other cases.</p>"
  ],
  [
    "<p>Make sure <code class=\"command\">ALTER TABLE</code>\n          preserves index tablespace assignments when rebuilding\n          indexes (Tom Lane, Michael Paquier)</p>",
    "<p>Previously, non-default settings of <a class=\"xref\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-client.html#GUC-DEFAULT-TABLESPACE\">default_tablespace</a>\n          could result in broken indexes.</p>"
  ],
  [
    "<p>Prevent dropping a foreign-key constraint if there are\n          pending trigger events for the referenced relation (Tom\n          Lane)</p>",
    "<p>This avoids <span class=\"quote\">&#x201C;<span class=\"quote\">could not find trigger <em class=\"replaceable\"><code>NNN</code></em></span>&#x201D;</span> or\n          <span class=\"quote\">&#x201C;<span class=\"quote\">relation\n          <em class=\"replaceable\"><code>NNN</code></em> has no\n          triggers</span>&#x201D;</span> errors.</p>"
  ],
  [
    "<p>Fix processing of OID column when a table with OIDs is\n          associated to a parent with OIDs via <code class=\"command\">ALTER TABLE ... INHERIT</code> (Amit\n          Langote)</p>",
    "<p>The OID column should be treated the same as regular\n          user columns in this case, but it wasn't, leading to odd\n          behavior in later inheritance changes.</p>"
  ],
  [
    "<p>Check for serializability conflicts before reporting\n          constraint-violation failures (Thomas Munro)</p>",
    "<p>When using serializable transaction isolation, it is\n          desirable that any error due to concurrent transactions\n          should manifest as a serialization failure, thereby\n          cueing the application that a retry might succeed.\n          Unfortunately, this does not reliably happen for\n          duplicate-key failures caused by concurrent insertions.\n          This change ensures that such an error will be reported\n          as a serialization error if the application explicitly\n          checked for the presence of a conflicting key (and did\n          not find it) earlier in the transaction.</p>"
  ],
  [
    "<p>Ensure that column typmods are determined accurately\n          for multi-row <code class=\"literal\">VALUES</code>\n          constructs (Tom Lane)</p>",
    "<p>This fixes problems occurring when the first value in\n          a column has a determinable typmod (e.g., length for a\n          <code class=\"type\">varchar</code> value) but later values\n          don't share the same limit.</p>"
  ],
  [
    "<p>Throw error for an unfinished Unicode surrogate pair\n          at the end of a Unicode string (Tom Lane)</p>",
    "<p>Normally, a Unicode surrogate leading character must\n          be followed by a Unicode surrogate trailing character,\n          but the check for this was missed if the leading\n          character was the last character in a Unicode string\n          literal (<code class=\"literal\">U&amp;'...'</code>) or\n          Unicode identifier (<code class=\"literal\">U&amp;\"...\"</code>).</p>"
  ],
  [
    "<p>Ensure that a purely negative text search query, such\n          as <code class=\"literal\">!foo</code>, matches empty\n          <code class=\"type\">tsvector</code>s (Tom Dunstan)</p>",
    "<p>Such matches were found by GIN index searches, but not\n          by sequential scans or GiST index searches.</p>"
  ],
  [
    "<p>Prevent crash when <code class=\"function\">ts_rewrite()</code> replaces a non-top-level\n          subtree with an empty query (Artur Zakirov)</p>"
  ],
  [
    "<p>Fix performance problems in <code class=\"function\">ts_rewrite()</code> (Tom Lane)</p>"
  ],
  [
    "<p>Fix <code class=\"function\">ts_rewrite()</code>'s\n          handling of nested NOT operators (Tom Lane)</p>"
  ],
  [
    "<p>Fix <code class=\"function\">array_fill()</code> to\n          handle empty arrays properly (Tom Lane)</p>"
  ],
  [
    "<p>Fix one-byte buffer overrun in <code class=\"function\">quote_literal_cstr()</code> (Heikki\n          Linnakangas)</p>",
    "<p>The overrun occurred only if the input consisted\n          entirely of single quotes and/or backslashes.</p>"
  ],
  [
    "<p>Prevent multiple calls of <code class=\"function\">pg_start_backup()</code> and <code class=\"function\">pg_stop_backup()</code> from running\n          concurrently (Michael Paquier)</p>",
    "<p>This avoids an assertion failure, and possibly worse\n          things, if someone tries to run these functions in\n          parallel.</p>"
  ],
  [
    "<p>Avoid discarding <code class=\"type\">interval</code>-to-<code class=\"type\">interval</code> casts that aren't really no-ops\n          (Tom Lane)</p>",
    "<p>In some cases, a cast that should result in zeroing\n          out low-order <code class=\"type\">interval</code> fields\n          was mistakenly deemed to be a no-op and discarded. An\n          example is that casting from <code class=\"type\">INTERVAL\n          MONTH</code> to <code class=\"type\">INTERVAL YEAR</code>\n          failed to clear the months field.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_dump</span> to dump\n          user-defined casts and transforms that use built-in\n          functions (Stephen Frost)</p>"
  ],
  [
    "<p>Fix possible <span class=\"application\">pg_basebackup</span> failure on standby\n          server when including WAL files (Amit Kapila, Robert\n          Haas)</p>"
  ],
  [
    "<p>Ensure that the Python exception objects we create for\n          PL/Python are properly reference-counted (Rafa de la\n          Torre, Tom Lane)</p>",
    "<p>This avoids failures if the objects are used after a\n          Python garbage collection cycle has occurred.</p>"
  ],
  [
    "<p>Fix PL/Tcl to support triggers on tables that have\n          <code class=\"literal\">.tupno</code> as a column name (Tom\n          Lane)</p>",
    "<p>This matches the (previously undocumented) behavior of\n          PL/Tcl's <code class=\"command\">spi_exec</code> and\n          <code class=\"command\">spi_execp</code> commands, namely\n          that a magic <code class=\"literal\">.tupno</code> column\n          is inserted only if there isn't a real column named\n          that.</p>"
  ],
  [
    "<p>Allow DOS-style line endings in <code class=\"filename\">~/.pgpass</code> files, even on Unix (Vik\n          Fearing)</p>",
    "<p>This change simplifies use of the same password file\n          across Unix and Windows machines.</p>"
  ],
  [
    "<p>Fix one-byte buffer overrun if <span class=\"application\">ecpg</span> is given a file name that ends\n          with a dot (Takayuki Tsunakawa)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">psql</span>'s tab\n          completion for <code class=\"command\">ALTER DEFAULT\n          PRIVILEGES</code> (Gilles Darold, Stephen Frost)</p>"
  ],
  [
    "<p>In <span class=\"application\">psql</span>, treat an\n          empty or all-blank setting of the <code class=\"envar\">PAGER</code> environment variable as meaning\n          <span class=\"quote\">&#x201C;<span class=\"quote\">no\n          pager</span>&#x201D;</span> (Tom Lane)</p>",
    "<p>Previously, such a setting caused output intended for\n          the pager to vanish entirely.</p>"
  ],
  [
    "<p>Improve <code class=\"filename\">contrib/dblink</code>'s\n          reporting of low-level <span class=\"application\">libpq</span> errors, such as out-of-memory\n          (Joe Conway)</p>"
  ],
  [
    "<p>On Windows, ensure that environment variable changes\n          are propagated to DLLs built with debug options\n          (Christian Ullrich)</p>"
  ],
  [
    "<p>Sync our copy of the timezone library with IANA\n          release tzcode2016j (Tom Lane)</p>",
    "<p>This fixes various issues, most notably that timezone\n          data installation failed if the target directory didn't\n          support hard links.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2016j for DST law\n          changes in northern Cyprus (adding a new zone\n          Asia/Famagusta), Russia (adding a new zone\n          Europe/Saratov), Tonga, and Antarctica/Casey. Historical\n          corrections for Italy, Kazakhstan, Malta, and Palestine.\n          Switch to preferring numeric zone abbreviations for\n          Tonga.</p>"
  ]
]