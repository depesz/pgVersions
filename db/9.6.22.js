[
  [
    "<p>Prevent integer overflows in array subscripting calculations (Tom Lane)</p>",
    "<p>The array code previously did not complain about cases where an array's lower bound plus length overflows an integer. This resulted in later entries in the array becoming inaccessible (since their subscripts could not be written as integers), but more importantly it confused subsequent assignment operations. This could lead to memory overwrites, with ensuing crashes or unwanted data modifications. (CVE-2021-32027)</p>"
  ],
  [
    "<p>Fix mishandling of <span class=\"QUOTE\">\"junk\"</span> columns in <tt class=\"LITERAL\">INSERT ... ON CONFLICT ... UPDATE</tt> target lists (Tom Lane)</p>",
    "<p>If the <tt class=\"LITERAL\">UPDATE</tt> list contains any multi-column sub-selects (which give rise to junk columns in addition to the results proper), the <tt class=\"LITERAL\">UPDATE</tt> path would end up storing tuples that include the values of the extra junk columns. That's fairly harmless in the short run, but if new columns are added to the table then the values would become accessible, possibly leading to malfunctions if they don't match the datatypes of the added columns.</p>",
    "<p>In addition, in versions supporting cross-partition updates, a cross-partition update triggered by such a case had the reverse problem: the junk columns were removed from the target list, typically causing an immediate crash due to malfunction of the multi-column sub-select mechanism. (CVE-2021-32028)</p>"
  ],
  [
    "<p>Allow <tt class=\"LITERAL\">ALTER ROLE/DATABASE ... SET</tt> to set the <tt class=\"VARNAME\">role</tt>, <tt class=\"VARNAME\">session_authorization</tt>, and <tt class=\"VARNAME\">temp_buffers</tt> parameters (Tom Lane)</p>",
    "<p>Previously, over-eager validity checks might reject these commands, even if the values would have worked when used later. This created a command ordering hazard for dump/reload and upgrade scenarios.</p>"
  ],
  [
    "<p>Fix bug with coercing the result of a <tt class=\"LITERAL\">COLLATE</tt> expression to a non-collatable type (Tom Lane)</p>",
    "<p>This led to a parse tree in which the <tt class=\"LITERAL\">COLLATE</tt> appears to be applied to a non-collatable value. While that normally has no real impact (since <tt class=\"LITERAL\">COLLATE</tt> has no effect at runtime), it was possible to construct views that would be rejected during dump/reload.</p>"
  ],
  [
    "<p>Disallow calling window functions and procedures via the <span class=\"QUOTE\">\"fast path\"</span> wire protocol message (Tom Lane)</p>",
    "<p>Only plain functions are supported here. While trying to call an aggregate function failed already, calling a window function would crash, and calling a procedure would work only if the procedure did no transaction control.</p>"
  ],
  [
    "<p>Extend <code class=\"FUNCTION\">pg_identify_object_as_address()</code> to support event triggers (Joel Jacobson)</p>"
  ],
  [
    "<p>Fix <code class=\"FUNCTION\">to_char()</code>'s handling of Roman-numeral month format codes with negative intervals (Julien Rouhaud)</p>",
    "<p>Previously, such cases would usually cause a crash.</p>"
  ],
  [
    "<p>Fix use of uninitialized value while parsing an <tt class=\"LITERAL\">\\{<tt class=\"REPLACEABLE c2\">m</tt>,<tt class=\"REPLACEABLE c2\">n</tt>\\}</tt> quantifier in a BRE-mode regular expression (Tom Lane)</p>",
    "<p>This error could cause the quantifier to act non-greedy, that is behave like an <tt class=\"LITERAL\">{<tt class=\"REPLACEABLE c2\">m</tt>,<tt class=\"REPLACEABLE c2\">n</tt>}?</tt> quantifier would do in full regular expressions.</p>"
  ],
  [
    "<p>Avoid divide-by-zero when estimating selectivity of a regular expression with a very long fixed prefix (Tom Lane)</p>",
    "<p>This typically led to a <tt class=\"LITERAL\">NaN</tt> selectivity value, causing assertion failures or strange planner behavior.</p>"
  ],
  [
    "<p>Fix access-off-the-end-of-the-table error in BRIN index bitmap scans (Tomas Vondra)</p>",
    "<p>If the page range size used by a BRIN index isn't a power of two, there were corner cases in which a bitmap scan could try to fetch pages past the actual end of the table, leading to <span class=\"QUOTE\">\"could not open file\"</span> errors.</p>"
  ],
  [
    "<p>Ensure that locks are released while shutting down a standby server's startup process (Fujii Masao)</p>",
    "<p>When a standby server is shut down while still in recovery, some locks might be left held. This causes assertion failures in debug builds; it's unclear whether any serious consequence could occur in production builds.</p>"
  ],
  [
    "<p>Ensure we default to <tt class=\"VARNAME\">wal_sync_method</tt> = <tt class=\"LITERAL\">fdatasync</tt> on recent FreeBSD (Thomas Munro)</p>",
    "<p>FreeBSD 13 supports <tt class=\"LITERAL\">open_datasync</tt>, which would normally become the default choice. However, it's unclear whether that is actually an improvement for Postgres, so preserve the existing default for now.</p>"
  ],
  [
    "<p>Ensure we finish cleaning up when interrupted while detaching a DSM segment (Thomas Munro)</p>",
    "<p>This error could result in temporary files not being cleaned up promptly after a parallel query.</p>"
  ],
  [
    "<p>Fix assorted minor memory leaks in the server (Tom Lane, Andres Freund)</p>"
  ],
  [
    "<p>Prevent infinite loop in <span class=\"APPLICATION\">libpq</span> if a ParameterDescription message with a corrupt length is received (Tom Lane)</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">psql</span> to restore the previous behavior of <tt class=\"LITERAL\">\\connect service=<tt class=\"REPLACEABLE c2\">something</tt></tt> (Tom Lane)</p>",
    "<p>A previous bug fix caused environment variables (such as <tt class=\"VARNAME\">PGPORT</tt>) to override entries in the service file in this context. Restore the previous behavior, in which the priority is the other way around.</p>"
  ],
  [
    "<p>Fix race condition in detection of file modification by <span class=\"APPLICATION\">psql</span>'s <tt class=\"LITERAL\">\\e</tt> and related commands (Laurenz Albe)</p>",
    "<p>A very fast typist could fool the code's file-timestamp-based detection of whether the temporary edit file was changed.</p>"
  ],
  [
    "<p>Fix missed file version check in <span class=\"APPLICATION\">pg_restore</span> (Tom Lane)</p>",
    "<p>When reading a custom-format archive from a non-seekable source, <span class=\"APPLICATION\">pg_restore</span> neglected to check the archive version. If it was fed a newer archive version than it can support, it would fail messily later on.</p>"
  ],
  [
    "<p>Add some more checks to <span class=\"APPLICATION\">pg_upgrade</span> for user tables containing non-upgradable data types (Tom Lane)</p>",
    "<p>Fix detection of some cases where a non-upgradable data type is embedded within a container type (such as an array or range). Also disallow upgrading when user tables contain columns of system-defined composite types, since those types' OIDs are not stable across versions.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_waldump</span> to count <tt class=\"LITERAL\">XACT</tt> records correctly when generating per-record statistics (Kyotaro Horiguchi)</p>"
  ],
  [
    "<p>Fix <tt class=\"FILENAME\">contrib/amcheck</tt> to not complain about the tuple flags <tt class=\"LITERAL\">HEAP_XMAX_LOCK_ONLY</tt> and <tt class=\"LITERAL\">HEAP_KEYS_UPDATED</tt> both being set (Julien Rouhaud)</p>",
    "<p>This is a valid state after <tt class=\"LITERAL\">SELECT FOR UPDATE</tt>.</p>"
  ],
  [
    "<p>Adjust VPATH build rules to support recent Oracle Developer Studio compiler versions (Noah Misch)</p>"
  ],
  [
    "<p>Fix testing of PL/Python for Python 3 on Solaris (Noah Misch)</p>"
  ]
]