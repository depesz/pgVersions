[
  [
    "<p>Remove public execute privilege from <code class=\"filename\">contrib/adminpack</code>'s <code class=\"function\">pg_logfile_rotate()</code> function (Stephen Frost)</p>",
    "<p><code class=\"function\">pg_logfile_rotate()</code> is a deprecated wrapper for the core function <code class=\"function\">pg_rotate_logfile()</code>. When that function was changed to rely on SQL privileges for access control rather than a hard-coded superuser check, <code class=\"function\">pg_logfile_rotate()</code> should have been updated as well, but the need for this was missed. Hence, if <code class=\"filename\">adminpack</code> is installed, any user could request a logfile rotation, creating a minor security issue.</p>",
    "<p>After installing this update, administrators should update <code class=\"filename\">adminpack</code> by performing <code class=\"literal\">ALTER EXTENSION adminpack UPDATE</code> in each database in which <code class=\"filename\">adminpack</code> is installed. (CVE-2018-1115)</p>"
  ],
  [
    "<p>Fix incorrect volatility markings on a few built-in functions (Thomas Munro, Tom Lane)</p>",
    "<p>The functions <code class=\"function\">query_to_xml</code>, <code class=\"function\">cursor_to_xml</code>, <code class=\"function\">cursor_to_xmlschema</code>, <code class=\"function\">query_to_xmlschema</code>, and <code class=\"function\">query_to_xml_and_xmlschema</code> should be marked volatile because they execute user-supplied queries that might contain volatile operations. They were not, leading to a risk of incorrect query optimization. This has been repaired for new installations by correcting the initial catalog data, but existing installations will continue to contain the incorrect markings. Practical use of these functions seems to pose little hazard, but in case of trouble, it can be fixed by manually updating these functions' <code class=\"structname\">pg_proc</code> entries, for example <code class=\"literal\">ALTER FUNCTION pg_catalog.query_to_xml(text, boolean, boolean, text) VOLATILE</code>. (Note that that will need to be done in each database of the installation.) Another option is to <span class=\"application\">pg_upgrade</span> the database to a version containing the corrected initial data.</p>"
  ],
  [
    "<p>Fix incorrect parallel-safety markings on a few built-in functions (Thomas Munro, Tom Lane)</p>",
    "<p>The functions <code class=\"function\">brin_summarize_new_values</code>, <code class=\"function\">brin_summarize_range</code>, <code class=\"function\">brin_desummarize_range</code>, <code class=\"function\">gin_clean_pending_list</code>, <code class=\"function\">cursor_to_xml</code>, <code class=\"function\">cursor_to_xmlschema</code>, <code class=\"function\">ts_rewrite</code>, <code class=\"function\">ts_stat</code>, <code class=\"function\">binary_upgrade_create_empty_extension</code>, and <code class=\"function\">pg_import_system_collations</code> should be marked parallel-unsafe; some because they perform database modifications directly, and others because they execute user-supplied queries that might do so. They were marked parallel-restricted instead, leading to a risk of unexpected query errors. This has been repaired for new installations by correcting the initial catalog data, but existing installations will continue to contain the incorrect markings. Practical use of these functions seems to pose little hazard unless <code class=\"varname\">force_parallel_mode</code> is turned on. In case of trouble, it can be fixed by manually updating these functions' <code class=\"structname\">pg_proc</code> entries, for example <code class=\"literal\">ALTER FUNCTION pg_catalog.brin_summarize_new_values(regclass) PARALLEL UNSAFE</code>. (Note that that will need to be done in each database of the installation.) Another option is to <span class=\"application\">pg_upgrade</span> the database to a version containing the corrected initial data.</p>"
  ],
  [
    "<p>Avoid re-using TOAST value OIDs that match dead-but-not-yet-vacuumed TOAST entries (Pavan Deolasee)</p>",
    "<p>Once the OID counter has wrapped around, it's possible to assign a TOAST value whose OID matches a previously deleted entry in the same TOAST table. If that entry were not yet vacuumed away, this resulted in <span class=\"quote\">“<span class=\"quote\">unexpected chunk number 0 (expected 1) for toast value <em class=\"replaceable\"><code>nnnnn</code></em></span>”</span> errors, which would persist until the dead entry was removed by <code class=\"command\">VACUUM</code>. Fix by not selecting such OIDs when creating a new TOAST entry.</p>"
  ],
  [
    "<p>Correctly enforce any <code class=\"literal\">CHECK</code> constraints on individual partitions during <code class=\"command\">COPY</code> to a partitioned table (Etsuro Fujita)</p>",
    "<p>Previously, only constraints declared for the partitioned table as a whole were checked.</p>"
  ],
  [
    "<p>Accept <code class=\"literal\">TRUE</code> and <code class=\"literal\">FALSE</code> as partition bound values (Amit Langote)</p>",
    "<p>Previously, only string-literal values were accepted for a boolean partitioning column. But then <span class=\"application\">pg_dump</span> would print such values as <code class=\"literal\">TRUE</code> or <code class=\"literal\">FALSE</code>, leading to dump/reload failures.</p>"
  ],
  [
    "<p>Fix memory management for partition key comparison functions (Álvaro Herrera, Amit Langote)</p>",
    "<p>This error could lead to crashes when using user-defined operator classes for partition keys.</p>"
  ],
  [
    "<p>Fix possible crash when a query inserts tuples in several partitions of a partitioned table, and those partitions don't have identical row types (Etsuro Fujita, Amit Langote)</p>"
  ],
  [
    "<p>Change <code class=\"command\">ANALYZE</code>'s algorithm for updating <code class=\"structname\">pg_class</code>.<code class=\"structfield\">reltuples</code> (David Gould)</p>",
    "<p>Previously, pages not actually scanned by <code class=\"command\">ANALYZE</code> were assumed to retain their old tuple density. In a large table where <code class=\"command\">ANALYZE</code> samples only a small fraction of the pages, this meant that the overall tuple density estimate could not change very much, so that <code class=\"structfield\">reltuples</code> would change nearly proportionally to changes in the table's physical size (<code class=\"structfield\">relpages</code>) regardless of what was actually happening in the table. This has been observed to result in <code class=\"structfield\">reltuples</code> becoming so much larger than reality as to effectively shut off autovacuuming. To fix, assume that <code class=\"command\">ANALYZE</code>'s sample is a statistically unbiased sample of the table (as it should be), and just extrapolate the density observed within those pages to the whole table.</p>"
  ],
  [
    "<p>Include extended-statistics objects in the set of table properties duplicated by <code class=\"command\">CREATE TABLE ... LIKE ... INCLUDING ALL</code> (David Rowley)</p>",
    "<p>Also add an <code class=\"literal\">INCLUDING STATISTICS</code> option, to allow finer-grained control over whether this happens.</p>"
  ],
  [
    "<p>Fix <code class=\"command\">CREATE TABLE ... LIKE</code> with <code class=\"type\">bigint</code> identity columns (Peter Eisentraut)</p>",
    "<p>On platforms where <code class=\"type\">long</code> is 32 bits (which includes 64-bit Windows as well as most 32-bit machines), copied sequence parameters would be truncated to 32 bits.</p>"
  ],
  [
    "<p>Avoid deadlocks in concurrent <code class=\"command\">CREATE INDEX CONCURRENTLY</code> commands that are run under <code class=\"literal\">SERIALIZABLE</code> or <code class=\"literal\">REPEATABLE READ</code> transaction isolation (Tom Lane)</p>"
  ],
  [
    "<p>Fix possible slow execution of <code class=\"command\">REFRESH MATERIALIZED VIEW CONCURRENTLY</code> (Thomas Munro)</p>"
  ],
  [
    "<p>Fix <code class=\"literal\">UPDATE/DELETE ... WHERE CURRENT OF</code> to not fail when the referenced cursor uses an index-only-scan plan (Yugo Nagata, Tom Lane)</p>"
  ],
  [
    "<p>Fix incorrect planning of join clauses pushed into parameterized paths (Andrew Gierth, Tom Lane)</p>",
    "<p>This error could result in misclassifying a condition as a <span class=\"quote\">“<span class=\"quote\">join filter</span>”</span> for an outer join when it should be a plain <span class=\"quote\">“<span class=\"quote\">filter</span>”</span> condition, leading to incorrect join output.</p>"
  ],
  [
    "<p>Fix possibly incorrect generation of an index-only-scan plan when the same table column appears in multiple index columns, and only some of those index columns use operator classes that can return the column value (Kyotaro Horiguchi)</p>"
  ],
  [
    "<p>Fix misoptimization of <code class=\"literal\">CHECK</code> constraints having provably-NULL subclauses of top-level <code class=\"literal\">AND</code>/<code class=\"literal\">OR</code> conditions (Tom Lane, Dean Rasheed)</p>",
    "<p>This could, for example, allow constraint exclusion to exclude a child table that should not be excluded from a query.</p>"
  ],
  [
    "<p>Prevent planner crash when a query has multiple <code class=\"literal\">GROUPING SETS</code>, none of which can be implemented by sorting (Andrew Gierth)</p>"
  ],
  [
    "<p>Fix executor crash due to double free in some <code class=\"literal\">GROUPING SETS</code> usages (Peter Geoghegan)</p>"
  ],
  [
    "<p>Fix misexecution of self-joins on transition tables (Thomas Munro)</p>"
  ],
  [
    "<p>Avoid crash if a table rewrite event trigger is added concurrently with a command that could call such a trigger (Álvaro Herrera, Andrew Gierth, Tom Lane)</p>"
  ],
  [
    "<p>Avoid failure if a query-cancel or session-termination interrupt occurs while committing a prepared transaction (Stas Kelvich)</p>"
  ],
  [
    "<p>Fix query-lifespan memory leakage in repeatedly executed hash joins (Tom Lane)</p>"
  ],
  [
    "<p>Fix possible leak or double free of visibility map buffer pins (Amit Kapila)</p>"
  ],
  [
    "<p>Avoid spuriously marking pages as all-visible (Dan Wood, Pavan Deolasee, Álvaro Herrera)</p>",
    "<p>This could happen if some tuples were locked (but not deleted). While queries would still function correctly, vacuum would normally ignore such pages, with the long-term effect that the tuples were never frozen. In recent releases this would eventually result in errors such as <span class=\"quote\">“<span class=\"quote\">found multixact <em class=\"replaceable\"><code>nnnnn</code></em> from before relminmxid <em class=\"replaceable\"><code>nnnnn</code></em></span>”</span>.</p>"
  ],
  [
    "<p>Fix overly strict sanity check in <code class=\"function\">heap_prepare_freeze_tuple</code> (Álvaro Herrera)</p>",
    "<p>This could result in incorrect <span class=\"quote\">“<span class=\"quote\">cannot freeze committed xmax</span>”</span> failures in databases that have been <span class=\"application\">pg_upgrade</span>'d from 9.2 or earlier.</p>"
  ],
  [
    "<p>Prevent dangling-pointer dereference when a C-coded before-update row trigger returns the <span class=\"quote\">“<span class=\"quote\">old</span>”</span> tuple (Rushabh Lathia)</p>"
  ],
  [
    "<p>Reduce locking during autovacuum worker scheduling (Jeff Janes)</p>",
    "<p>The previous behavior caused drastic loss of potential worker concurrency in databases with many tables.</p>"
  ],
  [
    "<p>Ensure client hostname is copied while copying <code class=\"structname\">pg_stat_activity</code> data to local memory (Edmund Horner)</p>",
    "<p>Previously the supposedly-local snapshot contained a pointer into shared memory, allowing the client hostname column to change unexpectedly if any existing session disconnected.</p>"
  ],
  [
    "<p>Handle <code class=\"structname\">pg_stat_activity</code> information for auxiliary processes correctly (Edmund Horner)</p>",
    "<p>The <code class=\"structfield\">application_name</code>, <code class=\"structfield\">client_hostname</code>, and <code class=\"structfield\">query</code> fields might show incorrect data for such processes.</p>"
  ],
  [
    "<p>Fix incorrect processing of multiple compound affixes in <code class=\"literal\">ispell</code> dictionaries (Arthur Zakirov)</p>"
  ],
  [
    "<p>Fix collation-aware searches (that is, indexscans using inequality operators) in SP-GiST indexes on text columns (Tom Lane)</p>",
    "<p>Such searches would return the wrong set of rows in most non-C locales.</p>"
  ],
  [
    "<p>Prevent query-lifespan memory leakage with SP-GiST operator classes that use traversal values (Anton Dignös)</p>"
  ],
  [
    "<p>Count the number of index tuples correctly during initial build of an SP-GiST index (Tomas Vondra)</p>",
    "<p>Previously, the tuple count was reported to be the same as that of the underlying table, which is wrong if the index is partial.</p>"
  ],
  [
    "<p>Count the number of index tuples correctly during vacuuming of a GiST index (Andrey Borodin)</p>",
    "<p>Previously it reported the estimated number of heap tuples, which might be inaccurate, and is certainly wrong if the index is partial.</p>"
  ],
  [
    "<p>Fix a corner case where a streaming standby gets stuck at a WAL continuation record (Kyotaro Horiguchi)</p>"
  ],
  [
    "<p>In logical decoding, avoid possible double processing of WAL data when a walsender restarts (Craig Ringer)</p>"
  ],
  [
    "<p>Fix logical replication to not assume that type OIDs match between the local and remote servers (Masahiko Sawada)</p>"
  ],
  [
    "<p>Allow <code class=\"function\">scalarltsel</code> and <code class=\"function\">scalargtsel</code> to be used on non-core datatypes (Tomas Vondra)</p>"
  ],
  [
    "<p>Reduce <span class=\"application\">libpq</span>'s memory consumption when a server error is reported after a large amount of query output has been collected (Tom Lane)</p>",
    "<p>Discard the previous output before, not after, processing the error message. On some platforms, notably Linux, this can make a difference in the application's subsequent memory footprint.</p>"
  ],
  [
    "<p>Fix double-free crashes in <span class=\"application\">ecpg</span> (Patrick Krecker, Jeevan Ladhe)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">ecpg</span> to handle <code class=\"type\">long long int</code> variables correctly in MSVC builds (Michael Meskes, Andrew Gierth)</p>"
  ],
  [
    "<p>Fix mis-quoting of values for list-valued GUC variables in dumps (Michael Paquier, Tom Lane)</p>",
    "<p>The <code class=\"varname\">local_preload_libraries</code>, <code class=\"varname\">session_preload_libraries</code>, <code class=\"varname\">shared_preload_libraries</code>, and <code class=\"varname\">temp_tablespaces</code> variables were not correctly quoted in <span class=\"application\">pg_dump</span> output. This would cause problems if settings for these variables appeared in <code class=\"command\">CREATE FUNCTION ... SET</code> or <code class=\"command\">ALTER DATABASE/ROLE ... SET</code> clauses.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_recvlogical</span> to not fail against pre-v10 <span class=\"productname\">PostgreSQL</span> servers (Michael Paquier)</p>",
    "<p>A previous fix caused <span class=\"application\">pg_recvlogical</span> to issue a command regardless of server version, but it should only be issued to v10 and later servers.</p>"
  ],
  [
    "<p>Ensure that <span class=\"application\">pg_rewind</span> deletes files on the target server if they are deleted from the source server during the run (Takayuki Tsunakawa)</p>",
    "<p>Failure to do this could result in data inconsistency on the target, particularly if the file in question is a WAL segment.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_rewind</span> to handle tables in non-default tablespaces correctly (Takayuki Tsunakawa)</p>"
  ],
  [
    "<p>Fix overflow handling in <span class=\"application\">PL/pgSQL</span> integer <code class=\"command\">FOR</code> loops (Tom Lane)</p>",
    "<p>The previous coding failed to detect overflow of the loop variable on some non-gcc compilers, leading to an infinite loop.</p>"
  ],
  [
    "<p>Adjust <span class=\"application\">PL/Python</span> regression tests to pass under Python 3.7 (Peter Eisentraut)</p>"
  ],
  [
    "<p>Support testing <span class=\"application\">PL/Python</span> and related modules when building with Python 3 and MSVC (Andrew Dunstan)</p>"
  ],
  [
    "<p>Fix errors in initial build of <code class=\"filename\">contrib/bloom</code> indexes (Tomas Vondra, Tom Lane)</p>",
    "<p>Fix possible omission of the table's last tuple from the index. Count the number of index tuples correctly, in case it is a partial index.</p>"
  ],
  [
    "<p>Rename internal <code class=\"function\">b64_encode</code> and <code class=\"function\">b64_decode</code> functions to avoid conflict with Solaris 11.4 built-in functions (Rainer Orth)</p>"
  ],
  [
    "<p>Sync our copy of the timezone library with IANA tzcode release 2018e (Tom Lane)</p>",
    "<p>This fixes the <span class=\"application\">zic</span> timezone data compiler to cope with negative daylight-savings offsets. While the <span class=\"productname\">PostgreSQL</span> project will not immediately ship such timezone data, <span class=\"application\">zic</span> might be used with timezone data obtained directly from IANA, so it seems prudent to update <span class=\"application\">zic</span> now.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2018d for DST law changes in Palestine and Antarctica (Casey Station), plus historical corrections for Portugal and its colonies, as well as Enderbury, Jamaica, Turks &amp; Caicos Islands, and Uruguay.</p>"
  ]
]