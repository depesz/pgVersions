[
  [
    "<p>Fix WAL page header initialization at the end of\n          archive recovery (Heikki)</p>",
    "<p>This could lead to failure to process the WAL in a\n          subsequent archive recovery.</p>"
  ],
  [
    "<p>Fix <span class=\"quote\">&#x201C;<span class=\"quote\">cannot\n          make new WAL entries during recovery</span>&#x201D;</span> error\n          (Tom)</p>"
  ],
  [
    "<p>Fix problem that could make expired rows visible after\n          a crash (Tom)</p>",
    "<p>This bug involved a page status bit potentially not\n          being set correctly after a server crash.</p>"
  ],
  [
    "<p>Disallow <code class=\"command\">RESET ROLE</code> and\n          <code class=\"command\">RESET SESSION AUTHORIZATION</code>\n          inside security-definer functions (Tom, Heikki)</p>",
    "<p>This covers a case that was missed in the previous\n          patch that disallowed <code class=\"command\">SET\n          ROLE</code> and <code class=\"command\">SET SESSION\n          AUTHORIZATION</code> inside security-definer functions.\n          (See CVE-2007-6600)</p>"
  ],
  [
    "<p>Make <code class=\"command\">LOAD</code> of an\n          already-loaded loadable module into a no-op (Tom)</p>",
    "<p>Formerly, <code class=\"command\">LOAD</code> would\n          attempt to unload and re-load the module, but this is\n          unsafe and not all that useful.</p>"
  ],
  [
    "<p>Make window function <code class=\"literal\">PARTITION\n          BY</code> and <code class=\"literal\">ORDER BY</code> items\n          always be interpreted as simple expressions (Tom)</p>",
    "<p>In 8.4.0 these lists were parsed following the rules\n          used for top-level <code class=\"literal\">GROUP BY</code>\n          and <code class=\"literal\">ORDER BY</code> lists. But this\n          was not correct per the SQL standard, and it led to\n          possible circularity.</p>"
  ],
  [
    "<p>Fix several errors in planning of semi-joins (Tom)</p>",
    "<p>These led to wrong query results in some cases where\n          <code class=\"literal\">IN</code> or <code class=\"literal\">EXISTS</code> was used together with another\n          join.</p>"
  ],
  [
    "<p>Fix handling of whole-row references to subqueries\n          that are within an outer join (Tom)</p>",
    "<p>An example is <code class=\"literal\">SELECT COUNT(ss.*)\n          FROM ... LEFT JOIN (SELECT ...) ss ON ...</code>. Here,\n          <code class=\"literal\">ss.*</code> would be treated as\n          <code class=\"literal\">ROW(NULL,NULL,...)</code> for\n          null-extended join rows, which is not the same as a\n          simple NULL. Now it is treated as a simple NULL.</p>"
  ],
  [
    "<p>Fix Windows shared-memory allocation code (Tsutomu\n          Yamada, Magnus)</p>",
    "<p>This bug led to the often-reported <span class=\"quote\">&#x201C;<span class=\"quote\">could not reattach to shared\n          memory</span>&#x201D;</span> error message.</p>"
  ],
  [
    "<p>Fix locale handling with plperl (Heikki)</p>",
    "<p>This bug could cause the server's locale setting to\n          change when a plperl function is called, leading to data\n          corruption.</p>"
  ],
  [
    "<p>Fix handling of reloptions to ensure setting one\n          option doesn't force default values for others (Itagaki\n          Takahiro)</p>"
  ],
  [
    "<p>Ensure that a <span class=\"quote\">&#x201C;<span class=\"quote\">fast shutdown</span>&#x201D;</span> request will\n          forcibly terminate open sessions, even if a <span class=\"quote\">&#x201C;<span class=\"quote\">smart\n          shutdown</span>&#x201D;</span> was already in progress (Fujii\n          Masao)</p>"
  ],
  [
    "<p>Avoid memory leak for <code class=\"function\">array_agg()</code> in <code class=\"literal\">GROUP BY</code> queries (Tom)</p>"
  ],
  [
    "<p>Treat <code class=\"function\">to_char(..., 'TH')</code>\n          as an uppercase ordinal suffix with <code class=\"literal\">'HH'</code>/<code class=\"literal\">'HH12'</code>\n          (Heikki)</p>",
    "<p>It was previously handled as <code class=\"literal\">'th'</code> (lowercase).</p>"
  ],
  [
    "<p>Include the fractional part in the result of\n          <code class=\"function\">EXTRACT(second)</code> and\n          <code class=\"function\">EXTRACT(milliseconds)</code> for\n          <code class=\"type\">time</code> and <code class=\"type\">time with time zone</code> inputs (Tom)</p>",
    "<p>This has always worked for floating-point datetime\n          configurations, but was broken in the integer datetime\n          code.</p>"
  ],
  [
    "<p>Fix overflow for <code class=\"literal\">INTERVAL\n          '<em class=\"replaceable\"><code>x</code></em> ms'</code>\n          when <em class=\"replaceable\"><code>x</code></em> is more\n          than 2 million and integer datetimes are in use (Alex\n          Hunsaker)</p>"
  ],
  [
    "<p>Improve performance when processing toasted values in\n          index scans (Tom)</p>",
    "<p>This is particularly useful for <a class=\"ulink\" href=\"http://postgis.net/\" target=\"_top\">PostGIS</a>.</p>"
  ],
  [
    "<p>Fix a typo that disabled <code class=\"varname\">commit_delay</code> (Jeff Janes)</p>"
  ],
  [
    "<p>Output early-startup messages to <code class=\"filename\">postmaster.log</code> if the server is started\n          in silent mode (Tom)</p>",
    "<p>Previously such error messages were discarded, leading\n          to difficulty in debugging.</p>"
  ],
  [
    "<p>Remove translated FAQs (Peter)</p>",
    "<p>They are now on the <a class=\"ulink\" href=\"http://wiki.postgresql.org/wiki/FAQ\" target=\"_top\">wiki</a>. The main FAQ was moved to the wiki some\n          time ago.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_ctl</span> to not go\n          into an infinite loop if <code class=\"filename\">postgresql.conf</code> is empty (Jeff\n          Davis)</p>"
  ],
  [
    "<p>Fix several errors in <span class=\"application\">pg_dump</span>'s <code class=\"literal\">--binary-upgrade</code> mode (Bruce, Tom)</p>",
    "<p><code class=\"literal\">pg_dump --binary-upgrade</code>\n          is used by pg_migrator.</p>"
  ],
  [
    "<p>Fix <code class=\"filename\">contrib/xml2</code>'s\n          <code class=\"function\">xslt_process()</code> to properly\n          handle the maximum number of parameters (twenty)\n          (Tom)</p>"
  ],
  [
    "<p>Improve robustness of <span class=\"application\">libpq</span>'s code to recover from errors\n          during <code class=\"command\">COPY FROM STDIN</code>\n          (Tom)</p>"
  ],
  [
    "<p>Avoid including conflicting readline and editline\n          header files when both libraries are installed (Zdenek\n          Kotala)</p>"
  ],
  [
    "<p>Work around gcc bug that causes <span class=\"quote\">&#x201C;<span class=\"quote\">floating-point\n          exception</span>&#x201D;</span> instead of <span class=\"quote\">&#x201C;<span class=\"quote\">division by\n          zero</span>&#x201D;</span> on some platforms (Tom)</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2009l for DST law\n          changes in Bangladesh, Egypt, Mauritius.</p>"
  ]
]