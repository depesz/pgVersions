[
  [
    "<p>Make <code class=\"function\">pg_get_ruledef()</code>\n          parenthesize negative constants (Tom)</p>",
    "<p>Before this fix, a negative constant in a view or rule\n          might be dumped as, say, <code class=\"literal\">-42::integer</code>, which is subtly incorrect:\n          it should be <code class=\"literal\">(-42)::integer</code>\n          due to operator precedence rules. Usually this would make\n          little difference, but it could interact with another\n          recent patch to cause <span class=\"productname\">PostgreSQL</span> to reject what had been a\n          valid <code class=\"command\">SELECT DISTINCT</code> view\n          query. Since this could result in <span class=\"application\">pg_dump</span> output failing to reload, it\n          is being treated as a high-priority fix. The only\n          released versions in which dump output is actually\n          incorrect are 8.3.1 and 8.2.7.</p>"
  ],
  [
    "<p>Make <code class=\"command\">ALTER AGGREGATE ... OWNER\n          TO</code> update <code class=\"structname\">pg_shdepend</code> (Tom)</p>",
    "<p>This oversight could lead to problems if the aggregate\n          was later involved in a <code class=\"command\">DROP\n          OWNED</code> or <code class=\"command\">REASSIGN\n          OWNED</code> operation.</p>"
  ]
]