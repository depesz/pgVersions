[
  [
    "<p>Asynchronous commit delays writes to WAL during\n            transaction commit (Simon)</p>",
    "<p>This feature dramatically increases performance for\n            short data-modifying transactions. The disadvantage is\n            that because disk writes are delayed, if the database\n            or operating system crashes before data is written to\n            the disk, committed data will be lost. This feature is\n            useful for applications that can accept some data loss.\n            Unlike turning off <code class=\"varname\">fsync</code>,\n            using asynchronous commit does not put database\n            consistency at risk; the worst case is that after a\n            crash the last few reportedly-committed transactions\n            might not be committed after all. This feature is\n            enabled by turning off <code class=\"varname\">synchronous_commit</code> (which can be done\n            per-session or per-transaction, if some transactions\n            are critical and others are not). <code class=\"varname\">wal_writer_delay</code> can be adjusted to\n            control the maximum delay before transactions actually\n            reach disk.</p>"
  ],
  [
    "<p>Checkpoint writes can be spread over a longer time\n            period to smooth the I/O spike during each checkpoint\n            (Itagaki Takahiro and Heikki Linnakangas)</p>",
    "<p>Previously all modified buffers were forced to disk\n            as quickly as possible during a checkpoint, causing an\n            I/O spike that decreased server performance. This new\n            approach spreads out disk writes during checkpoints,\n            reducing peak I/O usage. (User-requested and shutdown\n            checkpoints are still written as quickly as\n            possible.)</p>"
  ],
  [
    "<p>Heap-Only Tuples (<acronym class=\"acronym\">HOT</acronym>) accelerate space reuse for\n            most <code class=\"command\">UPDATE</code>s and\n            <code class=\"command\">DELETE</code>s (Pavan Deolasee,\n            with ideas from many others)</p>",
    "<p><code class=\"command\">UPDATE</code>s and\n            <code class=\"command\">DELETE</code>s leave dead tuples\n            behind, as do failed <code class=\"command\">INSERT</code>s. Previously only <code class=\"command\">VACUUM</code> could reclaim space taken by\n            dead tuples. With <acronym class=\"acronym\">HOT</acronym> dead tuple space can be\n            automatically reclaimed at the time of <code class=\"command\">INSERT</code> or <code class=\"command\">UPDATE</code> if no changes are made to\n            indexed columns. This allows for more consistent\n            performance. Also, <acronym class=\"acronym\">HOT</acronym> avoids adding duplicate index\n            entries.</p>"
  ],
  [
    "<p>Just-in-time background writer strategy improves\n            disk write efficiency (Greg Smith, Itagaki\n            Takahiro)</p>",
    "<p>This greatly reduces the need for manual tuning of\n            the background writer.</p>"
  ],
  [
    "<p>Per-field and per-row storage overhead have been\n            reduced (Greg Stark, Heikki Linnakangas)</p>",
    "<p>Variable-length data types with data values less\n            than 128 bytes long will see a storage decrease of 3 to\n            6 bytes. For example, two adjacent <code class=\"type\">char(1)</code> fields now use 4 bytes instead of\n            16. Row headers are also 4 bytes shorter than\n            before.</p>"
  ],
  [
    "<p>Using non-persistent transaction IDs for read-only\n            transactions reduces overhead and <code class=\"command\">VACUUM</code> requirements (Florian\n            Pflug)</p>",
    "<p>Non-persistent transaction IDs do not increment the\n            global transaction counter. Therefore, they reduce the\n            load on <code class=\"structname\">pg_clog</code> and\n            increase the time between forced vacuums to prevent\n            transaction ID wraparound. Other performance\n            improvements were also made that should improve\n            concurrency.</p>"
  ],
  [
    "<p>Avoid incrementing the command counter after a\n            read-only command (Tom)</p>",
    "<p>There was formerly a hard limit of 2<sup>32</sup> (4\n            billion) commands per transaction. Now only commands\n            that actually changed the database count, so while this\n            limit still exists, it should be significantly less\n            annoying.</p>"
  ],
  [
    "<p>Create a dedicated <acronym class=\"acronym\">WAL</acronym> writer process to off-load work\n            from backends (Simon)</p>"
  ],
  [
    "<p>Skip unnecessary WAL writes for <code class=\"command\">CLUSTER</code> and <code class=\"command\">COPY</code> (Simon)</p>",
    "<p>Unless WAL archiving is enabled, the system now\n            avoids WAL writes for <code class=\"command\">CLUSTER</code> and just <code class=\"function\">fsync()</code>s the table at the end of the\n            command. It also does the same for <code class=\"command\">COPY</code> if the table was created in the\n            same transaction.</p>"
  ],
  [
    "<p>Large sequential scans no longer force out\n            frequently used cached pages (Simon, Heikki, Tom)</p>"
  ],
  [
    "<p>Concurrent large sequential scans can now share disk\n            reads (Jeff Davis)</p>",
    "<p>This is accomplished by starting the new sequential\n            scan in the middle of the table (where another\n            sequential scan is already in-progress) and wrapping\n            around to the beginning to finish. This can affect the\n            order of returned rows in a query that does not specify\n            <code class=\"literal\">ORDER BY</code>. The <code class=\"varname\">synchronize_seqscans</code> configuration\n            parameter can be used to disable this if necessary.</p>"
  ],
  [
    "<p><code class=\"literal\">ORDER BY ... LIMIT</code> can\n            be done without sorting (Greg Stark)</p>",
    "<p>This is done by sequentially scanning the table and\n            tracking just the <span class=\"quote\">&#x201C;<span class=\"quote\">top N</span>&#x201D;</span> candidate rows, rather\n            than performing a full sort of the entire table. This\n            is useful when there is no matching index and the\n            <code class=\"literal\">LIMIT</code> is not large.</p>"
  ],
  [
    "<p>Put a rate limit on messages sent to the statistics\n            collector by backends (Tom)</p>",
    "<p>This reduces overhead for short transactions, but\n            might sometimes increase the delay before statistics\n            are tallied.</p>"
  ],
  [
    "<p>Improve hash join performance for cases with many\n            NULLs (Tom)</p>"
  ],
  [
    "<p>Speed up operator lookup for cases with non-exact\n            datatype matches (Tom)</p>"
  ],
  [
    "<p>Autovacuum is now enabled by default (Alvaro)</p>",
    "<p>Several changes were made to eliminate disadvantages\n            of having autovacuum enabled, thereby justifying the\n            change in default. Several other autovacuum parameter\n            defaults were also modified.</p>"
  ],
  [
    "<p>Support multiple concurrent autovacuum processes\n            (Alvaro, Itagaki Takahiro)</p>",
    "<p>This allows multiple vacuums to run concurrently.\n            This prevents vacuuming of a large table from delaying\n            vacuuming of smaller tables.</p>"
  ],
  [
    "<p>Automatically re-plan cached queries when table\n            definitions change or statistics are updated (Tom)</p>",
    "<p>Previously PL/pgSQL functions that referenced\n            temporary tables would fail if the temporary table was\n            dropped and recreated between function invocations,\n            unless <code class=\"literal\">EXECUTE</code> was used.\n            This improvement fixes that problem and many related\n            issues.</p>"
  ],
  [
    "<p>Add a <code class=\"varname\">temp_tablespaces</code>\n            parameter to control the tablespaces for temporary\n            tables and files (Jaime Casanova, Albert Cervera, Bernd\n            Helmle)</p>",
    "<p>This parameter defines a list of tablespaces to be\n            used. This enables spreading the I/O load across\n            multiple tablespaces. A random tablespace is chosen\n            each time a temporary object is created. Temporary\n            files are no longer stored in per-database <code class=\"filename\">pgsql_tmp/</code> directories but in\n            per-tablespace directories.</p>"
  ],
  [
    "<p>Place temporary tables' TOAST tables in special\n            schemas named <code class=\"literal\">pg_toast_temp_<em class=\"replaceable\"><code>nnn</code></em></code> (Tom)</p>",
    "<p>This allows low-level code to recognize these tables\n            as temporary, which enables various optimizations such\n            as not WAL-logging changes and using local rather than\n            shared buffers for access. This also fixes a bug\n            wherein backends unexpectedly held open file references\n            to temporary TOAST tables.</p>"
  ],
  [
    "<p>Fix problem that a constant flow of new connection\n            requests could indefinitely delay the postmaster from\n            completing a shutdown or a crash restart (Tom)</p>"
  ],
  [
    "<p>Guard against a very-low-probability data loss\n            scenario by preventing re-use of a deleted table's\n            relfilenode until after the next checkpoint\n            (Heikki)</p>"
  ],
  [
    "<p>Fix <code class=\"command\">CREATE CONSTRAINT\n            TRIGGER</code> to convert old-style foreign key trigger\n            definitions into regular foreign key constraints\n            (Tom)</p>",
    "<p>This will ease porting of foreign key constraints\n            carried forward from pre-7.3 databases, if they were\n            never converted using <code class=\"filename\">contrib/adddepend</code>.</p>"
  ],
  [
    "<p>Fix <code class=\"literal\">DEFAULT NULL</code> to\n            override inherited defaults (Tom)</p>",
    "<p><code class=\"literal\">DEFAULT NULL</code> was\n            formerly considered a noise phrase, but it should (and\n            now does) override non-null defaults that would\n            otherwise be inherited from a parent table or\n            domain.</p>"
  ],
  [
    "<p>Add new encodings EUC_JIS_2004 and SHIFT_JIS_2004\n            (Tatsuo)</p>",
    "<p>These new encodings can be converted to and from\n            UTF-8.</p>"
  ],
  [
    "<p>Change server startup log message from <span class=\"quote\">&#x201C;<span class=\"quote\">database system is\n            ready</span>&#x201D;</span> to <span class=\"quote\">&#x201C;<span class=\"quote\">database system is ready\n            to accept connections</span>&#x201D;</span>, and adjust its\n            timing</p>",
    "<p>The message now appears only when the postmaster is\n            really ready to accept connections.</p>"
  ],
  [
    "<p>Add <code class=\"varname\">log_autovacuum_min_duration</code> parameter\n            to support configurable logging of autovacuum activity\n            (Simon, Alvaro)</p>"
  ],
  [
    "<p>Add <code class=\"varname\">log_lock_waits</code>\n            parameter to log lock waiting (Simon)</p>"
  ],
  [
    "<p>Add <code class=\"varname\">log_temp_files</code>\n            parameter to log temporary file usage (Bill Moran)</p>"
  ],
  [
    "<p>Add <code class=\"varname\">log_checkpoints</code>\n            parameter to improve logging of checkpoints (Greg\n            Smith, Heikki)</p>"
  ],
  [
    "<p><code class=\"varname\">log_line_prefix</code> now\n            supports <code class=\"literal\">%s</code> and\n            <code class=\"literal\">%c</code> escapes in all\n            processes (Andrew)</p>",
    "<p>Previously these escapes worked only for user\n            sessions, not for background database processes.</p>"
  ],
  [
    "<p>Add <code class=\"varname\">log_restartpoints</code>\n            to control logging of point-in-time recovery restart\n            points (Simon)</p>"
  ],
  [
    "<p>Last transaction end time is now logged at end of\n            recovery and at each logged restart point (Simon)</p>"
  ],
  [
    "<p>Autovacuum now reports its activity start time in\n            <code class=\"literal\">pg_stat_activity</code> (Tom)</p>"
  ],
  [
    "<p>Allow server log output in comma-separated value\n            (CSV) format (Arul Shaji, Greg Smith, Andrew\n            Dunstan)</p>",
    "<p>CSV-format log files can easily be loaded into a\n            database table for subsequent analysis.</p>"
  ],
  [
    "<p>Use PostgreSQL-supplied timezone support for\n            formatting timestamps displayed in the server log\n            (Tom)</p>",
    "<p>This avoids Windows-specific problems with localized\n            time zone names that are in the wrong encoding. There\n            is a new <code class=\"varname\">log_timezone</code>\n            parameter that controls the timezone used in log\n            messages, independently of the client-visible\n            <code class=\"varname\">timezone</code> parameter.</p>"
  ],
  [
    "<p>New system view <code class=\"literal\">pg_stat_bgwriter</code> displays statistics\n            about background writer activity (Magnus)</p>"
  ],
  [
    "<p>Add new columns for database-wide tuple statistics\n            to <code class=\"literal\">pg_stat_database</code>\n            (Magnus)</p>"
  ],
  [
    "<p>Add an <code class=\"literal\">xact_start</code>\n            (transaction start time) column to <code class=\"literal\">pg_stat_activity</code> (Neil)</p>",
    "<p>This makes it easier to identify long-running\n            transactions.</p>"
  ],
  [
    "<p>Add <code class=\"literal\">n_live_tuples</code> and\n            <code class=\"literal\">n_dead_tuples</code> columns to\n            <code class=\"literal\">pg_stat_all_tables</code> and\n            related views (Glen Parker)</p>"
  ],
  [
    "<p>Merge <code class=\"varname\">stats_block_level</code>\n            and <code class=\"varname\">stats_row_level</code>\n            parameters into a single parameter <code class=\"varname\">track_counts</code>, which controls all\n            messages sent to the statistics collector process\n            (Tom)</p>"
  ],
  [
    "<p>Rename <code class=\"varname\">stats_command_string</code> parameter to\n            <code class=\"varname\">track_activities</code> (Tom)</p>"
  ],
  [
    "<p>Fix statistical counting of live and dead tuples to\n            recognize that committed and aborted transactions have\n            different effects (Tom)</p>"
  ],
  [
    "<p>Support Security Service Provider Interface\n            (<acronym class=\"acronym\">SSPI</acronym>) for\n            authentication on Windows (Magnus)</p>"
  ],
  [
    "<p>Support GSSAPI authentication (Henry Hotz,\n            Magnus)</p>",
    "<p>This should be preferred to native Kerberos\n            authentication because GSSAPI is an industry\n            standard.</p>"
  ],
  [
    "<p>Support a global SSL configuration file (Victor\n            Wagner)</p>"
  ],
  [
    "<p>Add <code class=\"varname\">ssl_ciphers</code>\n            parameter to control accepted SSL ciphers (Victor\n            Wagner)</p>"
  ],
  [
    "<p>Add a Kerberos realm parameter, <code class=\"varname\">krb_realm</code> (Magnus)</p>"
  ],
  [
    "<p>Change the timestamps recorded in transaction WAL\n            records from time_t to TimestampTz representation\n            (Tom)</p>",
    "<p>This provides sub-second resolution in WAL, which\n            can be useful for point-in-time recovery.</p>"
  ],
  [
    "<p>Reduce WAL disk space needed by warm standby servers\n            (Simon)</p>",
    "<p>This change allows a warm standby server to pass the\n            name of the earliest still-needed WAL file to the\n            recovery script, allowing automatic removal of\n            no-longer-needed WAL files. This is done using\n            <code class=\"literal\">%r</code> in the <code class=\"varname\">restore_command</code> parameter of\n            <code class=\"filename\">recovery.conf</code>.</p>"
  ],
  [
    "<p>New boolean configuration parameter, <code class=\"varname\">archive_mode</code>, controls archiving\n            (Simon)</p>",
    "<p>Previously setting <code class=\"varname\">archive_command</code> to an empty string\n            turned off archiving. Now <code class=\"varname\">archive_mode</code> turns archiving on and\n            off, independently of <code class=\"varname\">archive_command</code>. This is useful for\n            stopping archiving temporarily.</p>"
  ],
  [
    "<p>Full text search is integrated into the core\n            database system (Teodor, Oleg)</p>",
    "<p>Text search has been improved, moved into the core\n            code, and is now installed by default. <code class=\"filename\">contrib/tsearch2</code> now contains a\n            compatibility interface.</p>"
  ],
  [
    "<p>Add control over whether <code class=\"literal\">NULL</code>s sort first or last (Teodor,\n            Tom)</p>",
    "<p>The syntax is <code class=\"literal\">ORDER BY ...\n            NULLS FIRST/LAST</code>.</p>"
  ],
  [
    "<p>Allow per-column ascending/descending (<code class=\"literal\">ASC</code>/<code class=\"literal\">DESC</code>)\n            ordering options for indexes (Teodor, Tom)</p>",
    "<p>Previously a query using <code class=\"literal\">ORDER\n            BY</code> with mixed <code class=\"literal\">ASC</code>/<code class=\"literal\">DESC</code>\n            specifiers could not fully use an index. Now an index\n            can be fully used in such cases if the index was\n            created with matching <code class=\"literal\">ASC</code>/<code class=\"literal\">DESC</code>\n            specifications. <code class=\"literal\">NULL</code> sort\n            order within an index can be controlled, too.</p>"
  ],
  [
    "<p>Allow <code class=\"literal\">col IS NULL</code> to\n            use an index (Teodor)</p>"
  ],
  [
    "<p>Updatable cursors (Arul Shaji, Tom)</p>",
    "<p>This eliminates the need to reference a primary key\n            to <code class=\"command\">UPDATE</code> or <code class=\"command\">DELETE</code> rows returned by a cursor. The\n            syntax is <code class=\"literal\">UPDATE/DELETE WHERE\n            CURRENT OF</code>.</p>"
  ],
  [
    "<p>Allow <code class=\"literal\">FOR UPDATE</code> in\n            cursors (Arul Shaji, Tom)</p>"
  ],
  [
    "<p>Create a general mechanism that supports casts to\n            and from the standard string types (<code class=\"type\">TEXT</code>, <code class=\"type\">VARCHAR</code>,\n            <code class=\"type\">CHAR</code>) for <span class=\"emphasis\"><em>every</em></span> datatype, by invoking\n            the datatype's I/O functions (Tom)</p>",
    "<p>Previously, such casts were available only for types\n            that had specialized function(s) for the purpose. These\n            new casts are assignment-only in the to-string\n            direction, explicit-only in the other direction, and\n            therefore should create no surprising behavior.</p>"
  ],
  [
    "<p>Allow <code class=\"literal\">UNION</code> and related\n            constructs to return a domain type, when all inputs are\n            of that domain type (Tom)</p>",
    "<p>Formerly, the output would be considered to be of\n            the domain's base type.</p>"
  ],
  [
    "<p>Allow limited hashing when using two different data\n            types (Tom)</p>",
    "<p>This allows hash joins, hash indexes, hashed\n            subplans, and hash aggregation to be used in situations\n            involving cross-data-type comparisons, if the data\n            types have compatible hash functions. Currently,\n            cross-data-type hashing support exists for <code class=\"type\">smallint</code>/<code class=\"type\">integer</code>/<code class=\"type\">bigint</code>,\n            and for <code class=\"type\">float4</code>/<code class=\"type\">float8</code>.</p>"
  ],
  [
    "<p>Improve optimizer logic for detecting when variables\n            are equal in a <code class=\"literal\">WHERE</code>\n            clause (Tom)</p>",
    "<p>This allows mergejoins to work with descending sort\n            orders, and improves recognition of redundant sort\n            columns.</p>"
  ],
  [
    "<p>Improve performance when planning large inheritance\n            trees in cases where most tables are excluded by\n            constraints (Tom)</p>"
  ],
  [
    "<p>Arrays of composite types (David Fetter, Andrew,\n            Tom)</p>",
    "<p>In addition to arrays of explicitly-declared\n            composite types, arrays of the rowtypes of regular\n            tables and views are now supported, except for rowtypes\n            of system catalogs, sequences, and TOAST tables.</p>"
  ],
  [
    "<p>Server configuration parameters can now be set on a\n            per-function basis (Tom)</p>",
    "<p>For example, functions can now set their own\n            <code class=\"varname\">search_path</code> to prevent\n            unexpected behavior if a different <code class=\"varname\">search_path</code> exists at run-time.\n            Security definer functions should set <code class=\"varname\">search_path</code> to avoid security\n            loopholes.</p>"
  ],
  [
    "<p><code class=\"command\">CREATE/ALTER FUNCTION</code>\n            now supports <code class=\"literal\">COST</code> and\n            <code class=\"literal\">ROWS</code> options (Tom)</p>",
    "<p><code class=\"literal\">COST</code> allows\n            specification of the cost of a function call.\n            <code class=\"literal\">ROWS</code> allows specification\n            of the average number or rows returned by a\n            set-returning function. These values are used by the\n            optimizer in choosing the best plan.</p>"
  ],
  [
    "<p>Implement <code class=\"command\">CREATE TABLE LIKE\n            ... INCLUDING INDEXES</code> (Trevor Hardcastle, Nikhil\n            Sontakke, Neil)</p>"
  ],
  [
    "<p>Allow <code class=\"command\">CREATE INDEX\n            CONCURRENTLY</code> to ignore transactions in other\n            databases (Simon)</p>"
  ],
  [
    "<p>Add <code class=\"command\">ALTER VIEW ... RENAME\n            TO</code> and <code class=\"command\">ALTER SEQUENCE ...\n            RENAME TO</code> (David Fetter, Neil)</p>",
    "<p>Previously this could only be done via <code class=\"command\">ALTER TABLE ... RENAME TO</code>.</p>"
  ],
  [
    "<p>Make <code class=\"command\">CREATE/DROP/RENAME\n            DATABASE</code> wait briefly for conflicting backends\n            to exit before failing (Tom)</p>",
    "<p>This increases the likelihood that these commands\n            will succeed.</p>"
  ],
  [
    "<p>Allow triggers and rules to be deactivated in groups\n            using a configuration parameter, for replication\n            purposes (Jan)</p>",
    "<p>This allows replication systems to disable triggers\n            and rewrite rules as a group without modifying the\n            system catalogs directly. The behavior is controlled by\n            <code class=\"command\">ALTER TABLE</code> and a new\n            parameter <code class=\"varname\">session_replication_role</code>.</p>"
  ],
  [
    "<p>User-defined types can now have type modifiers\n            (Teodor, Tom)</p>",
    "<p>This allows a user-defined type to take a modifier,\n            like <code class=\"type\">ssnum(7)</code>. Previously\n            only built-in data types could have modifiers.</p>"
  ],
  [
    "<p>Non-superuser database owners now are able to add\n            trusted procedural languages to their databases by\n            default (Jeremy Drake)</p>",
    "<p>While this is reasonably safe, some administrators\n            might wish to revoke the privilege. It is controlled by\n            <code class=\"structname\">pg_pltemplate</code>.<code class=\"structfield\">tmpldbacreate</code>.</p>"
  ],
  [
    "<p>Allow a session's current parameter setting to be\n            used as the default for future sessions (Tom)</p>",
    "<p>This is done with <code class=\"literal\">SET ... FROM\n            CURRENT</code> in <code class=\"command\">CREATE/ALTER\n            FUNCTION</code>, <code class=\"command\">ALTER\n            DATABASE</code>, or <code class=\"command\">ALTER\n            ROLE</code>.</p>"
  ],
  [
    "<p>Implement new commands <code class=\"command\">DISCARD\n            ALL</code>, <code class=\"command\">DISCARD PLANS</code>,\n            <code class=\"command\">DISCARD TEMPORARY</code>,\n            <code class=\"command\">CLOSE ALL</code>, and\n            <code class=\"command\">DEALLOCATE ALL</code> (Marko\n            Kreen, Neil)</p>",
    "<p>These commands simplify resetting a database session\n            to its initial state, and are particularly useful for\n            connection-pooling software.</p>"
  ],
  [
    "<p>Make <code class=\"command\">CLUSTER</code> MVCC-safe\n            (Heikki Linnakangas)</p>",
    "<p>Formerly, <code class=\"command\">CLUSTER</code> would\n            discard all tuples that were committed dead, even if\n            there were still transactions that should be able to\n            see them under MVCC visibility rules.</p>"
  ],
  [
    "<p>Add new <code class=\"command\">CLUSTER</code> syntax:\n            <code class=\"literal\">CLUSTER <em class=\"replaceable\"><code>table</code></em> USING <em class=\"replaceable\"><code>index</code></em></code> (Holger\n            Schurig)</p>",
    "<p>The old <code class=\"command\">CLUSTER</code> syntax\n            is still supported, but the new form is considered more\n            logical.</p>"
  ],
  [
    "<p>Fix <code class=\"command\">EXPLAIN</code> so it can\n            show complex plans more accurately (Tom)</p>",
    "<p>References to subplan outputs are now always shown\n            correctly, instead of using <code class=\"literal\">?column<em class=\"replaceable\"><code>N</code></em>?</code> for\n            complicated cases.</p>"
  ],
  [
    "<p>Limit the amount of information reported when a user\n            is dropped (Alvaro)</p>",
    "<p>Previously, dropping (or attempting to drop) a user\n            who owned many objects could result in large\n            <code class=\"literal\">NOTICE</code> or <code class=\"literal\">ERROR</code> messages listing all these\n            objects; this caused problems for some client\n            applications. The length of the message is now limited,\n            although a full list is still sent to the server\n            log.</p>"
  ],
  [
    "<p>Support for the SQL/XML standard, including new\n            operators and an <code class=\"type\">XML</code> data\n            type (Nikolay Samokhvalov, Pavel Stehule, Peter)</p>"
  ],
  [
    "<p>Enumerated data types (<code class=\"type\">ENUM</code>) (Tom Dunstan)</p>",
    "<p>This feature provides convenient support for fields\n            that have a small, fixed set of allowed values. An\n            example of creating an <code class=\"literal\">ENUM</code> type is <code class=\"literal\">CREATE TYPE mood AS ENUM ('sad', 'ok',\n            'happy')</code>.</p>"
  ],
  [
    "<p>Universally Unique Identifier (<code class=\"type\">UUID</code>) data type (Gevik Babakhani,\n            Neil)</p>",
    "<p>This closely matches <acronym class=\"acronym\">RFC</acronym> 4122.</p>"
  ],
  [
    "<p>Widen the <code class=\"type\">MONEY</code> data type\n            to 64 bits (D'Arcy Cain)</p>",
    "<p>This greatly increases the range of supported\n            <code class=\"type\">MONEY</code> values.</p>"
  ],
  [
    "<p>Fix <code class=\"type\">float4</code>/<code class=\"type\">float8</code> to handle <code class=\"literal\">Infinity</code> and <code class=\"literal\">NAN</code> (Not A Number) consistently\n            (Bruce)</p>",
    "<p>The code formerly was not consistent about\n            distinguishing <code class=\"literal\">Infinity</code>\n            from overflow conditions.</p>"
  ],
  [
    "<p>Allow leading and trailing whitespace during input\n            of <code class=\"type\">boolean</code> values (Neil)</p>"
  ],
  [
    "<p>Prevent <code class=\"command\">COPY</code> from using\n            digits and lowercase letters as delimiters (Tom)</p>"
  ],
  [
    "<p>Add new regular expression functions <code class=\"function\">regexp_matches()</code>, <code class=\"function\">regexp_split_to_array()</code>, and\n            <code class=\"function\">regexp_split_to_table()</code>\n            (Jeremy Drake, Neil)</p>",
    "<p>These functions provide extraction of regular\n            expression subexpressions and allow splitting a string\n            using a POSIX regular expression.</p>"
  ],
  [
    "<p>Add <code class=\"function\">lo_truncate()</code> for\n            large object truncation (Kris Jurka)</p>"
  ],
  [
    "<p>Implement <code class=\"function\">width_bucket()</code> for the <code class=\"type\">float8</code> data type (Neil)</p>"
  ],
  [
    "<p>Add <code class=\"function\">pg_stat_clear_snapshot()</code> to discard\n            statistics snapshots collected during the current\n            transaction (Tom)</p>",
    "<p>The first request for statistics in a transaction\n            takes a statistics snapshot that does not change during\n            the transaction. This function allows the snapshot to\n            be discarded and a new snapshot loaded during the next\n            statistics query. This is particularly useful for\n            PL/pgSQL functions, which are confined to a single\n            transaction.</p>"
  ],
  [
    "<p>Add <code class=\"literal\">isodow</code> option to\n            <code class=\"function\">EXTRACT()</code> and\n            <code class=\"function\">date_part()</code> (Bruce)</p>",
    "<p>This returns the day of the week, with Sunday as\n            seven. (<code class=\"literal\">dow</code> returns Sunday\n            as zero.)</p>"
  ],
  [
    "<p>Add <code class=\"literal\">ID</code> (ISO day of\n            week) and <code class=\"literal\">IDDD</code> (ISO day of\n            year) format codes for <code class=\"function\">to_char()</code>, <code class=\"function\">to_date()</code>, and <code class=\"function\">to_timestamp()</code> (Brendan Jurd)</p>"
  ],
  [
    "<p>Make <code class=\"function\">to_timestamp()</code>\n            and <code class=\"function\">to_date()</code> assume\n            <code class=\"literal\">TM</code> (trim) option for\n            potentially variable-width fields (Bruce)</p>",
    "<p>This matches <span class=\"productname\">Oracle</span>'s behavior.</p>"
  ],
  [
    "<p>Fix off-by-one conversion error in <code class=\"function\">to_date()</code>/<code class=\"function\">to_timestamp()</code> <code class=\"literal\">D</code> (non-ISO day of week) fields\n            (Bruce)</p>"
  ],
  [
    "<p>Make <code class=\"function\">setseed()</code> return\n            void, rather than a useless integer value (Neil)</p>"
  ],
  [
    "<p>Add a hash function for <code class=\"type\">NUMERIC</code> (Neil)</p>",
    "<p>This allows hash indexes and hash-based plans to be\n            used with <code class=\"type\">NUMERIC</code>\n            columns.</p>"
  ],
  [
    "<p>Improve efficiency of <code class=\"literal\">LIKE</code>/<code class=\"literal\">ILIKE</code>, especially for multi-byte\n            character sets like UTF-8 (Andrew, Itagaki\n            Takahiro)</p>"
  ],
  [
    "<p>Make <code class=\"function\">currtid()</code>\n            functions require <code class=\"literal\">SELECT</code>\n            privileges on the target table (Tom)</p>"
  ],
  [
    "<p>Add several <code class=\"function\">txid_*()</code>\n            functions to query active transaction IDs (Jan)</p>",
    "<p>This is useful for various replication\n            solutions.</p>"
  ],
  [
    "<p>Add scrollable cursor support, including directional\n            control in <code class=\"command\">FETCH</code> (Pavel\n            Stehule)</p>"
  ],
  [
    "<p>Allow <code class=\"literal\">IN</code> as an\n            alternative to <code class=\"literal\">FROM</code> in\n            PL/pgSQL's <code class=\"command\">FETCH</code>\n            statement, for consistency with the backend's\n            <code class=\"command\">FETCH</code> command (Pavel\n            Stehule)</p>"
  ],
  [
    "<p>Add <code class=\"command\">MOVE</code> to PL/pgSQL\n            (Magnus, Pavel Stehule, Neil)</p>"
  ],
  [
    "<p>Implement <code class=\"command\">RETURN QUERY</code>\n            (Pavel Stehule, Neil)</p>",
    "<p>This adds convenient syntax for PL/pgSQL\n            set-returning functions that want to return the result\n            of a query. <code class=\"command\">RETURN QUERY</code>\n            is easier and more efficient than a loop around\n            <code class=\"command\">RETURN NEXT</code>.</p>"
  ],
  [
    "<p>Allow function parameter names to be qualified with\n            the function's name (Tom)</p>",
    "<p>For example, <code class=\"literal\">myfunc.myvar</code>. This is particularly\n            useful for specifying variables in a query where the\n            variable name might match a column name.</p>"
  ],
  [
    "<p>Make qualification of variables with block labels\n            work properly (Tom)</p>",
    "<p>Formerly, outer-level block labels could\n            unexpectedly interfere with recognition of inner-level\n            record or row references.</p>"
  ],
  [
    "<p>Tighten requirements for <code class=\"literal\">FOR</code> loop <code class=\"literal\">STEP</code> values (Tom)</p>",
    "<p>Prevent non-positive <code class=\"literal\">STEP</code> values, and handle loop\n            overflows.</p>"
  ],
  [
    "<p>Improve accuracy when reporting syntax error\n            locations (Tom)</p>"
  ],
  [
    "<p>Allow type-name arguments to PL/Perl <code class=\"function\">spi_prepare()</code> to be data type aliases\n            in addition to names found in <code class=\"literal\">pg_type</code> (Andrew)</p>"
  ],
  [
    "<p>Allow type-name arguments to PL/Python <code class=\"function\">plpy.prepare()</code> to be data type\n            aliases in addition to names found in <code class=\"literal\">pg_type</code> (Andrew)</p>"
  ],
  [
    "<p>Allow type-name arguments to PL/Tcl <code class=\"function\">spi_prepare</code> to be data type aliases\n            in addition to names found in <code class=\"literal\">pg_type</code> (Andrew)</p>"
  ],
  [
    "<p>Enable PL/PythonU to compile on Python 2.5 (Marko\n            Kreen)</p>"
  ],
  [
    "<p>Support a true PL/Python boolean type in compatible\n            Python versions (Python 2.3 and later) (Marko\n            Kreen)</p>"
  ],
  [
    "<p>Fix PL/Tcl problems with thread-enabled <code class=\"filename\">libtcl</code> spawning multiple threads\n            within the backend (Steve Marshall, Paul Bayer, Doug\n            Knight)</p>",
    "<p>This caused all sorts of unpleasantness.</p>"
  ],
  [
    "<p>List disabled triggers separately in <code class=\"literal\">\\d</code> output (Brendan Jurd)</p>"
  ],
  [
    "<p>In <code class=\"literal\">\\d</code> patterns, always\n            match <code class=\"literal\">$</code> literally\n            (Tom)</p>"
  ],
  [
    "<p>Show aggregate return types in <code class=\"literal\">\\da</code> output (Greg Sabino Mullane)</p>"
  ],
  [
    "<p>Add the function's volatility status to the output\n            of <code class=\"literal\">\\df+</code> (Neil)</p>"
  ],
  [
    "<p>Add <code class=\"literal\">\\prompt</code> capability\n            (Chad Wagner)</p>"
  ],
  [
    "<p>Allow <code class=\"literal\">\\pset</code>,\n            <code class=\"literal\">\\t</code>, and <code class=\"literal\">\\x</code> to specify <code class=\"literal\">on</code> or <code class=\"literal\">off</code>, rather than just toggling (Chad\n            Wagner)</p>"
  ],
  [
    "<p>Add <code class=\"literal\">\\sleep</code> capability\n            (Jan)</p>"
  ],
  [
    "<p>Enable <code class=\"literal\">\\timing</code> output\n            for <code class=\"literal\">\\copy</code> (Andrew)</p>"
  ],
  [
    "<p>Improve <code class=\"literal\">\\timing</code>\n            resolution on Windows (Itagaki Takahiro)</p>"
  ],
  [
    "<p>Flush <code class=\"literal\">\\o</code> output after\n            each backslash command (Tom)</p>"
  ],
  [
    "<p>Correctly detect and report errors while reading a\n            <code class=\"literal\">-f</code> input file (Peter)</p>"
  ],
  [
    "<p>Remove <code class=\"literal\">-u</code> option (this\n            option has long been deprecated) (Tom)</p>"
  ],
  [
    "<p>Add <code class=\"literal\">--tablespaces-only</code>\n            and <code class=\"literal\">--roles-only</code> options\n            to <span class=\"application\">pg_dumpall</span> (Dave\n            Page)</p>"
  ],
  [
    "<p>Add an output file option to <span class=\"application\">pg_dumpall</span> (Dave Page)</p>",
    "<p>This is primarily useful on Windows, where output\n            redirection of child <span class=\"application\">pg_dump</span> processes does not\n            work.</p>"
  ],
  [
    "<p>Allow <span class=\"application\">pg_dumpall</span> to\n            accept an initial-connection database name rather than\n            the default <code class=\"literal\">template1</code>\n            (Dave Page)</p>"
  ],
  [
    "<p>In <code class=\"literal\">-n</code> and <code class=\"literal\">-t</code> switches, always match <code class=\"literal\">$</code> literally (Tom)</p>"
  ],
  [
    "<p>Improve performance when a database has thousands of\n            objects (Tom)</p>"
  ],
  [
    "<p>Remove <code class=\"literal\">-u</code> option (this\n            option has long been deprecated) (Tom)</p>"
  ],
  [
    "<p>In <span class=\"application\">initdb</span>, allow\n            the location of the <code class=\"filename\">pg_xlog</code> directory to be specified\n            (Euler Taveira de Oliveira)</p>"
  ],
  [
    "<p>Enable server core dump generation in <span class=\"application\">pg_regress</span> on supported operating\n            systems (Andrew)</p>"
  ],
  [
    "<p>Add a <code class=\"literal\">-t</code> (timeout)\n            parameter to <span class=\"application\">pg_ctl</span>\n            (Bruce)</p>",
    "<p>This controls how long <span class=\"application\">pg_ctl</span> will wait when waiting for\n            server startup or shutdown. Formerly the timeout was\n            hard-wired as 60 seconds.</p>"
  ],
  [
    "<p>Add a <span class=\"application\">pg_ctl</span> option\n            to control generation of server core dumps (Andrew)</p>"
  ],
  [
    "<p>Allow Control-C to cancel <span class=\"application\">clusterdb</span>, <span class=\"application\">reindexdb</span>, and <span class=\"application\">vacuumdb</span> (Itagaki Takahiro,\n            Magnus)</p>"
  ],
  [
    "<p>Suppress command tag output for <span class=\"application\">createdb</span>, <span class=\"application\">createuser</span>, <span class=\"application\">dropdb</span>, and <span class=\"application\">dropuser</span> (Peter)</p>",
    "<p>The <code class=\"literal\">--quiet</code> option is\n            ignored and will be removed in 8.4. Progress messages\n            when acting on all databases now go to stdout instead\n            of stderr because they are not actually errors.</p>"
  ],
  [
    "<p>Interpret the <code class=\"literal\">dbName</code>\n            parameter of <code class=\"function\">PQsetdbLogin()</code> as a <code class=\"literal\">conninfo</code> string if it contains an\n            equals sign (Andrew)</p>",
    "<p>This allows use of <code class=\"literal\">conninfo</code> strings in client programs\n            that still use <code class=\"literal\">PQsetdbLogin()</code>.</p>"
  ],
  [
    "<p>Support a global <acronym class=\"acronym\">SSL</acronym> configuration file (Victor\n            Wagner)</p>"
  ],
  [
    "<p>Add environment variable <code class=\"varname\">PGSSLKEY</code> to control <acronym class=\"acronym\">SSL</acronym> hardware keys (Victor\n            Wagner)</p>"
  ],
  [
    "<p>Add <code class=\"function\">lo_truncate()</code> for\n            large object truncation (Kris Jurka)</p>"
  ],
  [
    "<p>Add <code class=\"function\">PQconnectionNeedsPassword()</code> that\n            returns true if the server required a password but none\n            was supplied (Joe Conway, Tom)</p>",
    "<p>If this returns true after a failed connection\n            attempt, a client application should prompt the user\n            for a password. In the past applications have had to\n            check for a specific error message string to decide\n            whether a password is needed; that approach is now\n            deprecated.</p>"
  ],
  [
    "<p>Add <code class=\"function\">PQconnectionUsedPassword()</code> that\n            returns true if the supplied password was actually used\n            (Joe Conway, Tom)</p>",
    "<p>This is useful in some security contexts where it is\n            important to know whether a user-supplied password is\n            actually valid.</p>"
  ],
  [
    "<p>Use V3 frontend/backend protocol (Michael)</p>",
    "<p>This adds support for server-side prepared\n            statements.</p>"
  ],
  [
    "<p>Use native threads, instead of pthreads, on Windows\n            (Magnus)</p>"
  ],
  [
    "<p>Improve thread-safety of ecpglib (Itagaki\n            Takahiro)</p>"
  ],
  [
    "<p>Make the ecpg libraries export only necessary API\n            symbols (Michael)</p>"
  ],
  [
    "<p>Allow the whole <span class=\"productname\">PostgreSQL</span> distribution to be\n            compiled with <span class=\"productname\">Microsoft\n            Visual C++</span> (Magnus and others)</p>",
    "<p>This allows Windows-based developers to use familiar\n            development and debugging tools. Windows executables\n            made with Visual C++ might also have better stability\n            and performance than those made with other tool sets.\n            The client-only Visual C++ build scripts have been\n            removed.</p>"
  ],
  [
    "<p>Drastically reduce postmaster's memory usage when it\n            has many child processes (Magnus)</p>"
  ],
  [
    "<p>Allow regression tests to be started by an\n            administrative user (Magnus)</p>"
  ],
  [
    "<p>Add native shared memory implementation (Magnus)</p>"
  ],
  [
    "<p>Add cursor-related functionality in SPI (Pavel\n            Stehule)</p>",
    "<p>Allow access to the cursor-related planning options,\n            and add <code class=\"command\">FETCH</code>/<code class=\"command\">MOVE</code> routines.</p>"
  ],
  [
    "<p>Allow execution of cursor commands through\n            <code class=\"function\">SPI_execute</code> (Tom)</p>",
    "<p>The macro <code class=\"literal\">SPI_ERROR_CURSOR</code> still exists but will\n            never be returned.</p>"
  ],
  [
    "<p>SPI plan pointers are now declared as <code class=\"literal\">SPIPlanPtr</code> instead of <code class=\"literal\">void *</code> (Tom)</p>",
    "<p>This does not break application code, but switching\n            is recommended to help catch simple programming\n            mistakes.</p>"
  ],
  [
    "<p>Add <span class=\"application\">configure</span>\n            option <code class=\"literal\">--enable-profiling</code>\n            to enable code profiling (works only with <span class=\"application\">gcc</span>) (Korry Douglas and Nikhil\n            Sontakke)</p>"
  ],
  [
    "<p>Add <span class=\"application\">configure</span>\n            option <code class=\"literal\">--with-system-tzdata</code> to use the\n            operating system's time zone database (Peter)</p>"
  ],
  [
    "<p>Fix <acronym class=\"acronym\">PGXS</acronym> so\n            extensions can be built against PostgreSQL\n            installations whose <span class=\"application\">pg_config</span> program does not appear\n            first in the <code class=\"varname\">PATH</code>\n            (Tom)</p>"
  ],
  [
    "<p>Support <code class=\"command\">gmake draft</code>\n            when building the <acronym class=\"acronym\">SGML</acronym> documentation (Bruce)</p>",
    "<p>Unless <code class=\"literal\">draft</code> is used,\n            the documentation build will now be repeated if\n            necessary to ensure the index is up-to-date.</p>"
  ],
  [
    "<p>Rename macro <code class=\"literal\">DLLIMPORT</code>\n            to <code class=\"literal\">PGDLLIMPORT</code> to avoid\n            conflicting with third party includes (like Tcl) that\n            define <code class=\"literal\">DLLIMPORT</code>\n            (Magnus)</p>"
  ],
  [
    "<p>Create <span class=\"quote\">&#x201C;<span class=\"quote\">operator families</span>&#x201D;</span> to improve\n            planning of queries involving cross-data-type\n            comparisons (Tom)</p>"
  ],
  [
    "<p>Update GIN <code class=\"function\">extractQuery()</code> API to allow\n            signalling that nothing can satisfy the query\n            (Teodor)</p>"
  ],
  [
    "<p>Move <code class=\"literal\">NAMEDATALEN</code>\n            definition from <code class=\"filename\">postgres_ext.h</code> to <code class=\"filename\">pg_config_manual.h</code> (Peter)</p>"
  ],
  [
    "<p>Provide <code class=\"function\">strlcpy()</code> and\n            <code class=\"function\">strlcat()</code> on all\n            platforms, and replace error-prone uses of <code class=\"function\">strncpy()</code>, <code class=\"function\">strncat()</code>, etc (Peter)</p>"
  ],
  [
    "<p>Create hooks to let an external plugin monitor (or\n            even replace) the planner and create plans for\n            hypothetical situations (Gurjeet Singh, Tom)</p>"
  ],
  [
    "<p>Create a function variable <code class=\"literal\">join_search_hook</code> to let plugins\n            override the join search order portion of the planner\n            (Julius Stroffek)</p>"
  ],
  [
    "<p>Add <code class=\"function\">tas()</code> support for\n            Renesas' M32R processor (Kazuhiro Inaoka)</p>"
  ],
  [
    "<p><code class=\"function\">quote_identifier()</code> and\n            <span class=\"application\">pg_dump</span> no longer\n            quote keywords that are unreserved according to the\n            grammar (Tom)</p>"
  ],
  [
    "<p>Change the on-disk representation of the\n            <code class=\"type\">NUMERIC</code> data type so that the\n            <code class=\"structfield\">sign_dscale</code> word comes\n            before the weight (Tom)</p>"
  ],
  [
    "<p>Use <acronym class=\"acronym\">SYSV</acronym>\n            semaphores rather than POSIX on Darwin &gt;= 6.0, i.e.,\n            macOS 10.2 and up (Chris Marcellino)</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/acronyms.html\" title=\"Appendix&#xA0;K.&#xA0;Acronyms\">acronym</a> and\n            <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/creating-cluster.html#CREATING-CLUSTER-NFS\" title=\"18.2.2.&#xA0;Use of Network File Systems\">NFS</a>\n            documentation sections (Bruce)</p>"
  ],
  [
    "<p>\"Postgres\" is now documented as an accepted alias\n            for \"PostgreSQL\" (Peter)</p>"
  ],
  [
    "<p>Add documentation about preventing database server\n            spoofing when the server is down (Bruce)</p>"
  ],
  [
    "<p>Move <code class=\"filename\">contrib</code>\n            <code class=\"filename\">README</code> content into the\n            main <span class=\"productname\">PostgreSQL</span>\n            documentation (Albert Cervera i Areny)</p>"
  ],
  [
    "<p>Add <code class=\"filename\">contrib/pageinspect</code> module for\n            low-level page inspection (Simon, Heikki)</p>"
  ],
  [
    "<p>Add <code class=\"filename\">contrib/pg_standby</code>\n            module for controlling warm standby operation\n            (Simon)</p>"
  ],
  [
    "<p>Add <code class=\"filename\">contrib/uuid-ossp</code>\n            module for generating <code class=\"type\">UUID</code>\n            values using the OSSP UUID library (Peter)</p>",
    "<p>Use <span class=\"application\">configure</span>\n            <code class=\"literal\">--with-ossp-uuid</code> to\n            activate. This takes advantage of the new <code class=\"type\">UUID</code> builtin type.</p>"
  ],
  [
    "<p>Add <code class=\"filename\">contrib/dict_int</code>,\n            <code class=\"filename\">contrib/dict_xsyn</code>, and\n            <code class=\"filename\">contrib/test_parser</code>\n            modules to provide sample add-on text search dictionary\n            templates and parsers (Sergey Karpov)</p>"
  ],
  [
    "<p>Allow <span class=\"application\">contrib/pgbench</span> to set the\n            fillfactor (Pavan Deolasee)</p>"
  ],
  [
    "<p>Add timestamps to <span class=\"application\">contrib/pgbench</span> <code class=\"literal\">-l</code> (Greg Smith)</p>"
  ],
  [
    "<p>Add usage count statistics to <code class=\"filename\">contrib/pgbuffercache</code> (Greg\n            Smith)</p>"
  ],
  [
    "<p>Add GIN support for <code class=\"filename\">contrib/hstore</code> (Teodor)</p>"
  ],
  [
    "<p>Add GIN support for <code class=\"filename\">contrib/pg_trgm</code> (Guillaume Smet,\n            Teodor)</p>"
  ],
  [
    "<p>Update OS/X startup scripts in <code class=\"filename\">contrib/start-scripts</code> (Mark Cotner,\n            David Fetter)</p>"
  ],
  [
    "<p>Restrict <code class=\"function\">pgrowlocks()</code>\n            and <code class=\"function\">dblink_get_pkey()</code> to\n            users who have <code class=\"literal\">SELECT</code>\n            privilege on the target table (Tom)</p>"
  ],
  [
    "<p>Restrict <code class=\"filename\">contrib/pgstattuple</code> functions to\n            superusers (Tom)</p>"
  ],
  [
    "<p><code class=\"filename\">contrib/xml2</code> is\n            deprecated and planned for removal in 8.4 (Peter)</p>",
    "<p>The new XML support in core PostgreSQL supersedes\n            this module.</p>"
  ]
]