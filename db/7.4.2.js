[
  [
    "<p>Fix <code class=\"structname\">pg_statistic</code>\n          alignment bug that could crash optimizer</p>",
    "<p>See above for details about this problem.</p>"
  ],
  [
    "<p>Allow non-super users to update <code class=\"structname\">pg_settings</code></p>"
  ],
  [
    "<p>Fix several optimizer bugs, most of which led to\n          <span class=\"quote\">&#x201C;<span class=\"quote\">variable not\n          found in subplan target lists</span>&#x201D;</span> errors</p>"
  ],
  [
    "<p>Avoid out-of-memory failure during startup of large\n          multiple index scan</p>"
  ],
  [
    "<p>Fix multibyte problem that could lead to <span class=\"quote\">&#x201C;<span class=\"quote\">out of memory</span>&#x201D;</span>\n          error during <code class=\"command\">COPY IN</code></p>"
  ],
  [
    "<p>Fix problems with <code class=\"command\">SELECT\n          INTO</code> / <code class=\"command\">CREATE TABLE\n          AS</code> from tables without OIDs</p>"
  ],
  [
    "<p>Fix problems with <code class=\"filename\">alter_table</code> regression test during\n          parallel testing</p>"
  ],
  [
    "<p>Fix problems with hitting open file limit, especially\n          on macOS (Tom)</p>"
  ],
  [
    "<p>Partial fix for Turkish-locale issues</p>",
    "<p>initdb will succeed now in Turkish locale, but there\n          are still some inconveniences associated with the\n          <code class=\"literal\">i/I</code> problem.</p>"
  ],
  [
    "<p>Make pg_dump set client encoding on restore</p>"
  ],
  [
    "<p>Other minor pg_dump fixes</p>"
  ],
  [
    "<p>Allow ecpg to again use C keywords as column names\n          (Michael)</p>"
  ],
  [
    "<p>Added ecpg <code class=\"literal\">WHENEVER\n          NOT_FOUND</code> to <code class=\"literal\">SELECT/INSERT/UPDATE/DELETE</code>\n          (Michael)</p>"
  ],
  [
    "<p>Fix ecpg crash for queries calling set-returning\n          functions (Michael)</p>"
  ],
  [
    "<p>Various other ecpg fixes (Michael)</p>"
  ],
  [
    "<p>Fixes for Borland compiler</p>"
  ],
  [
    "<p>Thread build improvements (Bruce)</p>"
  ],
  [
    "<p>Various other build fixes</p>"
  ],
  [
    "<p>Various JDBC fixes</p>"
  ]
]