[
  [
    "<p>Prevent possible compressed transaction log loss\n          (Tom)</p>"
  ],
  [
    "<p>Prevent non-superuser from increasing most recent\n          vacuum info (Tom)</p>"
  ],
  [
    "<p>Handle pre-1970 date values in newer versions of glibc\n          (Tom)</p>"
  ],
  [
    "<p>Fix possible hang during server shutdown</p>"
  ],
  [
    "<p>Prevent spinlock hangs on SMP PPC machines (Tomoyuki\n          Niijima)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_dump</span> to\n          properly dump FULL JOIN USING (Tom)</p>"
  ]
]