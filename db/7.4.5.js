[
  [
    "<p>Repair possible crash during concurrent B-tree index\n          insertions</p>",
    "<p>This patch fixes a rare case in which concurrent\n          insertions into a B-tree index could result in a server\n          panic. No permanent damage would result, but it's still\n          worth a re-release. The bug does not exist in pre-7.4\n          releases.</p>"
  ]
]