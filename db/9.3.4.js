[
  [
    "<p>Fix WAL replay of locking an already-updated tuple\n          (Andres Freund, &#xC1;lvaro Herrera)</p>",
    "<p>This error caused updated rows to not be found by\n          index scans, resulting in inconsistent query results\n          depending on whether an index scan was used. Subsequent\n          processing could result in constraint violations, since\n          the previously updated row would not be found by later\n          index searches, thus possibly allowing conflicting rows\n          to be inserted. Since this error is in WAL replay, it\n          would only manifest during crash recovery or on standby\n          servers. The improperly-replayed case most commonly\n          arises when a table row that is referenced by a\n          foreign-key constraint is updated concurrently with\n          creation of a referencing row.</p>"
  ],
  [
    "<p>Restore GIN metapages unconditionally to avoid\n          torn-page risk (Heikki Linnakangas)</p>",
    "<p>Although this oversight could theoretically result in\n          a corrupted index, it is unlikely to have caused any\n          problems in practice, since the active part of a GIN\n          metapage is smaller than a standard 512-byte disk\n          sector.</p>"
  ],
  [
    "<p>Avoid race condition in checking transaction commit\n          status during receipt of a <code class=\"command\">NOTIFY</code> message (Marko Tiikkaja)</p>",
    "<p>This prevents a scenario wherein a sufficiently fast\n          client might respond to a notification before database\n          updates made by the notifier have become visible to the\n          recipient.</p>"
  ],
  [
    "<p>Allow materialized views to be referenced in\n          <code class=\"command\">UPDATE</code> and <code class=\"command\">DELETE</code> commands (Michael Paquier)</p>",
    "<p>Previously such queries failed with a complaint about\n          not being able to lock rows in the materialized view.</p>"
  ],
  [
    "<p>Allow regular-expression operators to be terminated\n          early by query cancel requests (Tom Lane)</p>",
    "<p>This prevents scenarios wherein a pathological regular\n          expression could lock up a server process uninterruptibly\n          for a long time.</p>"
  ],
  [
    "<p>Remove incorrect code that tried to allow <code class=\"literal\">OVERLAPS</code> with single-element row\n          arguments (Joshua Yanovski)</p>",
    "<p>This code never worked correctly, and since the case\n          is neither specified by the SQL standard nor documented,\n          it seemed better to remove it than fix it.</p>"
  ],
  [
    "<p>Avoid getting more than <code class=\"literal\">AccessShareLock</code> when de-parsing a rule\n          or view (Dean Rasheed)</p>",
    "<p>This oversight resulted in <span class=\"application\">pg_dump</span> unexpectedly acquiring\n          <code class=\"literal\">RowExclusiveLock</code> locks on\n          tables mentioned as the targets of <code class=\"literal\">INSERT</code>/<code class=\"literal\">UPDATE</code>/<code class=\"literal\">DELETE</code> commands in rules. While usually\n          harmless, that could interfere with concurrent\n          transactions that tried to acquire, for example,\n          <code class=\"literal\">ShareLock</code> on those\n          tables.</p>"
  ],
  [
    "<p>Improve performance of index endpoint probes during\n          planning (Tom Lane)</p>",
    "<p>This change fixes a significant performance problem\n          that occurred when there were many not-yet-committed rows\n          at the end of the index, which is a common situation for\n          indexes on sequentially-assigned values such as\n          timestamps or sequence-generated identifiers.</p>"
  ],
  [
    "<p>Use non-default selectivity estimates for <code class=\"literal\"><em class=\"replaceable\"><code>value</code></em>\n          IN (<em class=\"replaceable\"><code>list</code></em>)</code> and\n          <code class=\"literal\"><em class=\"replaceable\"><code>value</code></em> <em class=\"replaceable\"><code>operator</code></em> ANY (<em class=\"replaceable\"><code>array</code></em>)</code> expressions\n          when the righthand side is a stable expression (Tom\n          Lane)</p>"
  ],
  [
    "<p>Remove the correct per-database statistics file during\n          <code class=\"command\">DROP DATABASE</code> (Tomas\n          Vondra)</p>",
    "<p>This fix prevents a permanent leak of statistics file\n          space. Users who have done many <code class=\"command\">DROP DATABASE</code> commands since upgrading\n          to <span class=\"productname\">PostgreSQL</span> 9.3 may\n          wish to check their statistics directory and delete\n          statistics files that do not correspond to any existing\n          database. Please note that <code class=\"filename\">db_0.stat</code> should not be removed.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">walsender</span> ping\n          logic to avoid inappropriate disconnects under continuous\n          load (Andres Freund, Heikki Linnakangas)</p>",
    "<p><span class=\"application\">walsender</span> failed to\n          send ping messages to the client if it was constantly\n          busy sending WAL data; but it expected to see ping\n          responses despite that, and would therefore disconnect\n          once <a class=\"xref\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-replication.html#GUC-WAL-SENDER-TIMEOUT\">wal_sender_timeout</a>\n          elapsed.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">walsender</span>'s\n          failure to shut down cleanly when client is <span class=\"application\">pg_receivexlog</span> (Fujii Masao)</p>"
  ],
  [
    "<p>Check WAL level and hot standby parameters correctly\n          when doing crash recovery that will be followed by\n          archive recovery (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Fix test to see if hot standby connections can be\n          allowed immediately after a crash (Heikki\n          Linnakangas)</p>"
  ],
  [
    "<p>Add read-only <a class=\"xref\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-preset.html#GUC-DATA-CHECKSUMS\">data_checksums</a>\n          parameter to display whether page checksums are enabled\n          (Heikki Linnakangas)</p>",
    "<p>Without this parameter, determining the state of\n          checksum processing was difficult.</p>"
  ],
  [
    "<p>Prevent interrupts while reporting non-<code class=\"literal\">ERROR</code> messages (Tom Lane)</p>",
    "<p>This guards against rare server-process freezeups due\n          to recursive entry to <code class=\"function\">syslog()</code>, and perhaps other related\n          problems.</p>"
  ],
  [
    "<p>Fix memory leak in PL/Perl when returning a composite\n          result, including multiple-OUT-parameter cases (Alex\n          Hunsaker)</p>"
  ],
  [
    "<p>Fix tracking of <span class=\"application\">psql</span>\n          script line numbers during <code class=\"literal\">\\copy</code> from out-of-line data (Kumar\n          Rajeev Rastogi, Amit Khandekar)</p>",
    "<p><code class=\"literal\">\\copy ... from</code>\n          incremented the script file line number for each data\n          line, even if the data was not coming from the script\n          file. This mistake resulted in wrong line numbers being\n          reported for any errors occurring later in the same\n          script file.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">contrib/postgres_fdw</span> to handle\n          multiple join conditions properly (Tom Lane)</p>",
    "<p>This oversight could result in sending <code class=\"literal\">WHERE</code> clauses to the remote server for\n          execution even though the clauses are not known to have\n          the same semantics on the remote server (for example,\n          clauses that use non-built-in operators). The query might\n          succeed anyway, but it could also fail with errors from\n          the remote server, or worse give silently wrong\n          answers.</p>"
  ],
  [
    "<p>Prevent intermittent <span class=\"quote\">&#x201C;<span class=\"quote\">could not reserve shared memory\n          region</span>&#x201D;</span> failures on recent Windows versions\n          (MauMau)</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2014a for DST law\n          changes in Fiji and Turkey, plus historical changes in\n          Israel and Ukraine.</p>"
  ]
]