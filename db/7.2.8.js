[
  [
    "<p>Repair ancient race condition that allowed a\n          transaction to be seen as committed for some purposes (eg\n          SELECT FOR UPDATE) slightly sooner than for other\n          purposes</p>",
    "<p>This is an extremely serious bug since it could lead\n          to apparent data inconsistencies being briefly visible to\n          applications.</p>"
  ],
  [
    "<p>Repair race condition between relation extension and\n          VACUUM</p>",
    "<p>This could theoretically have caused loss of a page's\n          worth of freshly-inserted data, although the scenario\n          seems of very low probability. There are no known cases\n          of it having caused more than an Assert failure.</p>"
  ],
  [
    "<p>Fix <code class=\"function\">EXTRACT(EPOCH)</code> for\n          <code class=\"type\">TIME WITH TIME ZONE</code> values</p>"
  ],
  [
    "<p>Additional buffer overrun checks in plpgsql (Neil)</p>"
  ],
  [
    "<p>Fix pg_dump to dump index names and trigger names\n          containing <code class=\"literal\">%</code> correctly\n          (Neil)</p>"
  ],
  [
    "<p>Prevent <code class=\"function\">to_char(interval)</code> from dumping core for\n          month-related formats</p>"
  ],
  [
    "<p>Fix <code class=\"filename\">contrib/pgcrypto</code> for\n          newer OpenSSL builds (Marko Kreen)</p>"
  ]
]