[
  [
    "<p>Restrict visibility of <tt class=\"STRUCTNAME\">pg_user_mappings</tt>.<tt class=\"STRUCTFIELD\">umoptions</tt>, to protect passwords stored as user mapping options (Michael Paquier, Feike Steenbergen)</p>",
    "<p>The previous coding allowed the owner of a foreign server object, or anyone he has granted server <tt class=\"LITERAL\">USAGE</tt> permission to, to see the options for all user mappings associated with that server. This might well include passwords for other users. Adjust the view definition to match the behavior of <tt class=\"STRUCTNAME\">information_schema.user_mapping_options</tt>, namely that these options are visible to the user being mapped, or if the mapping is for <tt class=\"LITERAL\">PUBLIC</tt> and the current user is the server owner, or if the current user is a superuser. (CVE-2017-7486)</p>",
    "<p>By itself, this patch will only fix the behavior in newly initdb'd databases. If you wish to apply this change in an existing database, follow the corrected procedure shown in the changelog entry for CVE-2017-7547, in <a href=\"https://www.postgresql.org/docs/9.5/release-9-5-8.html\">Section E.13</a>.</p>"
  ],
  [
    "<p>Prevent exposure of statistical information via leaky operators (Peter Eisentraut)</p>",
    "<p>Some selectivity estimation functions in the planner will apply user-defined operators to values obtained from <tt class=\"STRUCTNAME\">pg_statistic</tt>, such as most common values and histogram entries. This occurs before table permissions are checked, so a nefarious user could exploit the behavior to obtain these values for table columns he does not have permission to read. To fix, fall back to a default estimate if the operator's implementation function is not certified leak-proof and the calling user does not have permission to read the table column whose statistics are needed. At least one of these criteria is satisfied in most cases in practice. (CVE-2017-7484)</p>"
  ],
  [
    "<p>Restore <span class=\"APPLICATION\">libpq</span>'s recognition of the <tt class=\"ENVAR\">PGREQUIRESSL</tt> environment variable (Daniel Gustafsson)</p>",
    "<p>Processing of this environment variable was unintentionally dropped in <span class=\"PRODUCTNAME\">PostgreSQL</span> 9.3, but its documentation remained. This creates a security hazard, since users might be relying on the environment variable to force SSL-encrypted connections, but that would no longer be guaranteed. Restore handling of the variable, but give it lower priority than <tt class=\"ENVAR\">PGSSLMODE</tt>, to avoid breaking configurations that work correctly with post-9.3 code. (CVE-2017-7485)</p>"
  ],
  [
    "<p>Fix possibly-invalid initial snapshot during logical decoding (Petr Jelinek, Andres Freund)</p>",
    "<p>The initial snapshot created for a logical decoding replication slot was potentially incorrect. This could cause third-party tools that use logical decoding to copy incomplete/inconsistent initial data. This was more likely to happen if the source server was busy at the time of slot creation, or if another logical slot already existed.</p>",
    "<p>If you are using a replication tool that depends on logical decoding, and it should have copied a nonempty data set at the start of replication, it is advisable to recreate the replica after installing this update, or to verify its contents against the source server.</p>"
  ],
  [
    "<p>Fix possible corruption of <span class=\"QUOTE\">\"init forks\"</span> of unlogged indexes (Robert Haas, Michael Paquier)</p>",
    "<p>This could result in an unlogged index being set to an invalid state after a crash and restart. Such a problem would persist until the index was dropped and rebuilt.</p>"
  ],
  [
    "<p>Fix incorrect reconstruction of <tt class=\"STRUCTNAME\">pg_subtrans</tt> entries when a standby server replays a prepared but uncommitted two-phase transaction (Tom Lane)</p>",
    "<p>In most cases this turned out to have no visible ill effects, but in corner cases it could result in circular references in <tt class=\"STRUCTNAME\">pg_subtrans</tt>, potentially causing infinite loops in queries that examine rows modified by the two-phase transaction.</p>"
  ],
  [
    "<p>Avoid possible crash in <span class=\"APPLICATION\">walsender</span> due to failure to initialize a string buffer (Stas Kelvich, Fujii Masao)</p>"
  ],
  [
    "<p>Fix possible crash when rescanning a nearest-neighbor index-only scan on a GiST index (Tom Lane)</p>"
  ],
  [
    "<p>Fix postmaster's handling of <code class=\"FUNCTION\">fork()</code> failure for a background worker process (Tom Lane)</p>",
    "<p>Previously, the postmaster updated portions of its state as though the process had been launched successfully, resulting in subsequent confusion.</p>"
  ],
  [
    "<p>Fix crash or wrong answers when a <tt class=\"LITERAL\">GROUPING SETS</tt> column's data type is hashable but not sortable (Pavan Deolasee)</p>"
  ],
  [
    "<p>Avoid applying <span class=\"QUOTE\">\"physical targetlist\"</span> optimization to custom scans (Dmitry Ivanov, Tom Lane)</p>",
    "<p>This optimization supposed that retrieving all columns of a tuple is inexpensive, which is true for ordinary Postgres tuples; but it might not be the case for a custom scan provider.</p>"
  ],
  [
    "<p>Use the correct sub-expression when applying a <tt class=\"LITERAL\">FOR ALL</tt> row-level-security policy (Stephen Frost)</p>",
    "<p>In some cases the <tt class=\"LITERAL\">WITH CHECK</tt> restriction would be applied when the <tt class=\"LITERAL\">USING</tt> restriction is more appropriate.</p>"
  ],
  [
    "<p>Ensure parsing of queries in extension scripts sees the results of immediately-preceding DDL (Julien Rouhaud, Tom Lane)</p>",
    "<p>Due to lack of a cache flush step between commands in an extension script file, non-utility queries might not see the effects of an immediately preceding catalog change, such as <tt class=\"COMMAND\">ALTER TABLE ... RENAME</tt>.</p>"
  ],
  [
    "<p>Skip tablespace privilege checks when <tt class=\"COMMAND\">ALTER TABLE ... ALTER COLUMN TYPE</tt> rebuilds an existing index (Noah Misch)</p>",
    "<p>The command failed if the calling user did not currently have <tt class=\"LITERAL\">CREATE</tt> privilege for the tablespace containing the index. That behavior seems unhelpful, so skip the check, allowing the index to be rebuilt where it is.</p>"
  ],
  [
    "<p>Fix <tt class=\"COMMAND\">ALTER TABLE ... VALIDATE CONSTRAINT</tt> to not recurse to child tables when the constraint is marked <tt class=\"LITERAL\">NO INHERIT</tt> (Amit Langote)</p>",
    "<p>This fix prevents unwanted <span class=\"QUOTE\">\"constraint does not exist\"</span> failures when no matching constraint is present in the child tables.</p>"
  ],
  [
    "<p>Avoid dangling pointer in <tt class=\"COMMAND\">COPY ... TO</tt> when row-level security is active for the source table (Tom Lane)</p>",
    "<p>Usually this had no ill effects, but sometimes it would cause unexpected errors or crashes.</p>"
  ],
  [
    "<p>Avoid accessing an already-closed relcache entry in <tt class=\"COMMAND\">CLUSTER</tt> and <tt class=\"COMMAND\">VACUUM FULL</tt> (Tom Lane)</p>",
    "<p>With some bad luck, this could lead to indexes on the target relation getting rebuilt with the wrong persistence setting.</p>"
  ],
  [
    "<p>Fix <tt class=\"COMMAND\">VACUUM</tt> to account properly for pages that could not be scanned due to conflicting page pins (Andrew Gierth)</p>",
    "<p>This tended to lead to underestimation of the number of tuples in the table. In the worst case of a small heavily-contended table, <tt class=\"COMMAND\">VACUUM</tt> could incorrectly report that the table contained no tuples, leading to very bad planning choices.</p>"
  ],
  [
    "<p>Ensure that bulk-tuple-transfer loops within a hash join are interruptible by query cancel requests (Tom Lane, Thomas Munro)</p>"
  ],
  [
    "<p>Fix integer-overflow problems in <tt class=\"TYPE\">interval</tt> comparison (Kyotaro Horiguchi, Tom Lane)</p>",
    "<p>The comparison operators for type <tt class=\"TYPE\">interval</tt> could yield wrong answers for intervals larger than about 296000 years. Indexes on columns containing such large values should be reindexed, since they may be corrupt.</p>"
  ],
  [
    "<p>Fix <code class=\"FUNCTION\">cursor_to_xml()</code> to produce valid output with <tt class=\"REPLACEABLE c2\">tableforest</tt> = false (Thomas Munro, Peter Eisentraut)</p>",
    "<p>Previously it failed to produce a wrapping <tt class=\"LITERAL\">&lt;table&gt;</tt> element.</p>"
  ],
  [
    "<p>Fix roundoff problems in <code class=\"FUNCTION\">float8_timestamptz()</code> and <code class=\"FUNCTION\">make_interval()</code> (Tom Lane)</p>",
    "<p>These functions truncated, rather than rounded, when converting a floating-point value to integer microseconds; that could cause unexpectedly off-by-one results.</p>"
  ],
  [
    "<p>Fix <code class=\"FUNCTION\">pg_get_object_address()</code> to handle members of operator families correctly (Álvaro Herrera)</p>"
  ],
  [
    "<p>Improve performance of <tt class=\"STRUCTNAME\">pg_timezone_names</tt> view (Tom Lane, David Rowley)</p>"
  ],
  [
    "<p>Reduce memory management overhead for contexts containing many large blocks (Tom Lane)</p>"
  ],
  [
    "<p>Fix sloppy handling of corner-case errors from <code class=\"FUNCTION\">lseek()</code> and <code class=\"FUNCTION\">close()</code> (Tom Lane)</p>",
    "<p>Neither of these system calls are likely to fail in typical situations, but if they did, <tt class=\"FILENAME\">fd.c</tt> could get quite confused.</p>"
  ],
  [
    "<p>Fix incorrect check for whether postmaster is running as a Windows service (Michael Paquier)</p>",
    "<p>This could result in attempting to write to the event log when that isn't accessible, so that no logging happens at all.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">ecpg</span> to support <tt class=\"COMMAND\">COMMIT PREPARED</tt> and <tt class=\"COMMAND\">ROLLBACK PREPARED</tt> (Masahiko Sawada)</p>"
  ],
  [
    "<p>Fix a double-free error when processing dollar-quoted string literals in <span class=\"APPLICATION\">ecpg</span> (Michael Meskes)</p>"
  ],
  [
    "<p>In <span class=\"APPLICATION\">pg_dump</span>, fix incorrect schema and owner marking for comments and security labels of some types of database objects (Giuseppe Broccolo, Tom Lane)</p>",
    "<p>In simple cases this caused no ill effects; but for example, a schema-selective restore might omit comments it should include, because they were not marked as belonging to the schema of their associated object.</p>"
  ],
  [
    "<p>Avoid emitting an invalid list file in <tt class=\"LITERAL\">pg_restore -l</tt> when SQL object names contain newlines (Tom Lane)</p>",
    "<p>Replace newlines by spaces, which is sufficient to make the output valid for <tt class=\"LITERAL\">pg_restore -L</tt>'s purposes.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_upgrade</span> to transfer comments and security labels attached to <span class=\"QUOTE\">\"large objects\"</span> (blobs) (Stephen Frost)</p>",
    "<p>Previously, blobs were correctly transferred to the new database, but any comments or security labels attached to them were lost.</p>"
  ],
  [
    "<p>Improve error handling in <tt class=\"FILENAME\">contrib/adminpack</tt>'s <code class=\"FUNCTION\">pg_file_write()</code> function (Noah Misch)</p>",
    "<p>Notably, it failed to detect errors reported by <code class=\"FUNCTION\">fclose()</code>.</p>"
  ],
  [
    "<p>In <tt class=\"FILENAME\">contrib/dblink</tt>, avoid leaking the previous unnamed connection when establishing a new unnamed connection (Joe Conway)</p>"
  ],
  [
    "<p>Fix <tt class=\"FILENAME\">contrib/pg_trgm</tt>'s extraction of trigrams from regular expressions (Tom Lane)</p>",
    "<p>In some cases it would produce a broken data structure that could never match anything, leading to GIN or GiST indexscans that use a trigram index not finding any matches to the regular expression.</p>"
  ],
  [
    "<p>In <tt class=\"FILENAME\">contrib/postgres_fdw</tt>, transmit query cancellation requests to the remote server (Michael Paquier, Etsuro Fujita)</p>",
    "<p>Previously, a local query cancellation request did not cause an already-sent remote query to terminate early. This is a back-patch of work originally done for 9.6.</p>"
  ],
  [
    "<p>Support Tcl 8.6 in MSVC builds (Álvaro Herrera)</p>"
  ],
  [
    "<p>Sync our copy of the timezone library with IANA release tzcode2017b (Tom Lane)</p>",
    "<p>This fixes a bug affecting some DST transitions in January 2038.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"APPLICATION\">tzdata</span> release 2017b for DST law changes in Chile, Haiti, and Mongolia, plus historical corrections for Ecuador, Kazakhstan, Liberia, and Spain. Switch to numeric abbreviations for numerous time zones in South America, the Pacific and Indian oceans, and some Asian and Middle Eastern countries.</p>",
    "<p>The IANA time zone database previously provided textual abbreviations for all time zones, sometimes making up abbreviations that have little or no currency among the local population. They are in process of reversing that policy in favor of using numeric UTC offsets in zones where there is no evidence of real-world use of an English abbreviation. At least for the time being, <span class=\"PRODUCTNAME\">PostgreSQL</span> will continue to accept such removed abbreviations for timestamp input. But they will not be shown in the <tt class=\"STRUCTNAME\">pg_timezone_names</tt> view nor used for output.</p>"
  ],
  [
    "<p>Use correct daylight-savings rules for POSIX-style time zone names in MSVC builds (David Rowley)</p>",
    "<p>The Microsoft MSVC build scripts neglected to install the <tt class=\"FILENAME\">posixrules</tt> file in the timezone directory tree. This resulted in the timezone code falling back to its built-in rule about what DST behavior to assume for a POSIX-style time zone name. For historical reasons that still corresponds to the DST rules the USA was using before 2007 (i.e., change on first Sunday in April and last Sunday in October). With this fix, a POSIX-style zone name will use the current and historical DST transition dates of the <tt class=\"LITERAL\">US/Eastern</tt> zone. If you don't want that, remove the <tt class=\"FILENAME\">posixrules</tt> file, or replace it with a copy of some other zone file (see <a href=\"https://www.postgresql.org/docs/9.5/datatype-datetime.html#DATATYPE-TIMEZONES\">Section 8.5.3</a>). Note that due to caching, you may need to restart the server to get such changes to take effect.</p>"
  ]
]