[
  [
    "<p>Disable abbreviated keys for string sorting in non-<tt class=\"LITERAL\">C</tt> locales (Robert Haas)</p>",
    "<p><span class=\"PRODUCTNAME\">PostgreSQL</span> 9.5 introduced logic for speeding up comparisons of string data types by using the standard C library function <code class=\"FUNCTION\">strxfrm()</code> as a substitute for <code class=\"FUNCTION\">strcoll()</code>. It now emerges that most versions of glibc (Linux's implementation of the C library) have buggy implementations of <code class=\"FUNCTION\">strxfrm()</code> that, in some locales, can produce string comparison results that do not match <code class=\"FUNCTION\">strcoll()</code>. Until this problem can be better characterized, disable the optimization in all non-<tt class=\"LITERAL\">C</tt> locales. (<tt class=\"LITERAL\">C</tt> locale is safe since it uses neither <code class=\"FUNCTION\">strcoll()</code> nor <code class=\"FUNCTION\">strxfrm()</code>.)</p>",
    "<p>Unfortunately, this problem affects not only sorting but also entry ordering in B-tree indexes, which means that B-tree indexes on <tt class=\"TYPE\">text</tt>, <tt class=\"TYPE\">varchar</tt>, or <tt class=\"TYPE\">char</tt> columns may now be corrupt if they sort according to an affected locale and were built or modified under <span class=\"PRODUCTNAME\">PostgreSQL</span> 9.5.0 or 9.5.1. Users should <tt class=\"COMMAND\">REINDEX</tt> indexes that might be affected.</p>",
    "<p>It is not possible at this time to give an exhaustive list of known-affected locales. <tt class=\"LITERAL\">C</tt> locale is known safe, and there is no evidence of trouble in English-based locales such as <tt class=\"LITERAL\">en_US</tt>, but some other popular locales such as <tt class=\"LITERAL\">de_DE</tt> are affected in most glibc versions.</p>"
  ],
  [
    "<p>Maintain row-security status properly in cached plans (Stephen Frost)</p>",
    "<p>In a session that performs queries as more than one role, the plan cache might incorrectly re-use a plan that was generated for another role ID, thus possibly applying the wrong set of policies when row-level security (RLS) is in use. (CVE-2016-2193)</p>"
  ],
  [
    "<p>Add must-be-superuser checks to some new <tt class=\"FILENAME\">contrib/pageinspect</tt> functions (Andreas Seltenreich)</p>",
    "<p>Most functions in the <tt class=\"FILENAME\">pageinspect</tt> extension that inspect <tt class=\"TYPE\">bytea</tt> values disallow calls by non-superusers, but <code class=\"FUNCTION\">brin_page_type()</code> and <code class=\"FUNCTION\">brin_metapage_info()</code> failed to do so. Passing contrived <tt class=\"TYPE\">bytea</tt> values to them might crash the server or disclose a few bytes of server memory. Add the missing permissions checks to prevent misuse. (CVE-2016-3065)</p>"
  ],
  [
    "<p>Fix incorrect handling of indexed <tt class=\"LITERAL\">ROW()</tt> comparisons (Simon Riggs)</p>",
    "<p>Flaws in a minor optimization introduced in 9.5 caused incorrect results if the <tt class=\"LITERAL\">ROW()</tt> comparison matches the index ordering partially but not exactly (for example, differing column order, or the index contains both <tt class=\"LITERAL\">ASC</tt> and <tt class=\"LITERAL\">DESC</tt> columns). Pending a better solution, the optimization has been removed.</p>"
  ],
  [
    "<p>Fix incorrect handling of NULL index entries in indexed <tt class=\"LITERAL\">ROW()</tt> comparisons (Tom Lane)</p>",
    "<p>An index search using a row comparison such as <tt class=\"LITERAL\">ROW(a, b) &gt; ROW('x', 'y')</tt> would stop upon reaching a NULL entry in the <tt class=\"STRUCTFIELD\">b</tt> column, ignoring the fact that there might be non-NULL <tt class=\"STRUCTFIELD\">b</tt> values associated with later values of <tt class=\"STRUCTFIELD\">a</tt>.</p>"
  ],
  [
    "<p>Avoid unlikely data-loss scenarios due to renaming files without adequate <code class=\"FUNCTION\">fsync()</code> calls before and after (Michael Paquier, Tomas Vondra, Andres Freund)</p>"
  ],
  [
    "<p>Fix incorrect behavior when rechecking a just-modified row in a query that does <tt class=\"COMMAND\">SELECT FOR UPDATE/SHARE</tt> and contains some relations that need not be locked (Tom Lane)</p>",
    "<p>Rows from non-locked relations were incorrectly treated as containing all NULLs during the recheck, which could result in incorrectly deciding that the updated row no longer passes the <tt class=\"LITERAL\">WHERE</tt> condition, or in incorrectly outputting NULLs.</p>"
  ],
  [
    "<p>Fix bug in <code class=\"FUNCTION\">json_to_record()</code> when a field of its input object contains a sub-object with a field name matching one of the requested output column names (Tom Lane)</p>"
  ],
  [
    "<p>Fix nonsense result from two-argument form of <code class=\"FUNCTION\">jsonb_object()</code> when called with empty arrays (Michael Paquier, Andrew Dunstan)</p>"
  ],
  [
    "<p>Fix misbehavior in <code class=\"FUNCTION\">jsonb_set()</code> when converting a path array element into an integer for use as an array subscript (Michael Paquier)</p>"
  ],
  [
    "<p>Fix misformatting of negative time zone offsets by <code class=\"FUNCTION\">to_char()</code>'s <tt class=\"LITERAL\">OF</tt> format code (Thomas Munro, Tom Lane)</p>"
  ],
  [
    "<p>Fix possible incorrect logging of waits done by <tt class=\"COMMAND\">INSERT ... ON CONFLICT</tt> (Peter Geoghegan)</p>",
    "<p>Log messages would sometimes claim that the wait was due to an exclusion constraint although no such constraint was responsible.</p>"
  ],
  [
    "<p>Ignore <a href=\"https://www.postgresql.org/docs/9.5/standby-settings.html#RECOVERY-MIN-APPLY-DELAY\">recovery_min_apply_delay</a> parameter until recovery has reached a consistent state (Michael Paquier)</p>",
    "<p>Previously, standby servers would delay application of WAL records in response to <tt class=\"VARNAME\">recovery_min_apply_delay</tt> even while replaying the initial portion of WAL needed to make their database state valid. Since the standby is useless until it's reached a consistent database state, this was deemed unhelpful.</p>"
  ],
  [
    "<p>Correctly handle cases where <tt class=\"LITERAL\">pg_subtrans</tt> is close to XID wraparound during server startup (Jeff Janes)</p>"
  ],
  [
    "<p>Fix assorted bugs in logical decoding (Andres Freund)</p>",
    "<p>Trouble cases included tuples larger than one page when replica identity is <tt class=\"LITERAL\">FULL</tt>, <tt class=\"COMMAND\">UPDATE</tt>s that change a primary key within a transaction large enough to be spooled to disk, incorrect reports of <span class=\"QUOTE\">\"subxact logged without previous toplevel record\"</span>, and incorrect reporting of a transaction's commit time.</p>"
  ],
  [
    "<p>Fix planner error with nested security barrier views when the outer view has a <tt class=\"LITERAL\">WHERE</tt> clause containing a correlated subquery (Dean Rasheed)</p>"
  ],
  [
    "<p>Fix memory leak in GIN index searches (Tom Lane)</p>"
  ],
  [
    "<p>Fix corner-case crash due to trying to free <code class=\"FUNCTION\">localeconv()</code> output strings more than once (Tom Lane)</p>"
  ],
  [
    "<p>Fix parsing of affix files for <tt class=\"LITERAL\">ispell</tt> dictionaries (Tom Lane)</p>",
    "<p>The code could go wrong if the affix file contained any characters whose byte length changes during case-folding, for example <tt class=\"LITERAL\">I</tt> in Turkish UTF8 locales.</p>"
  ],
  [
    "<p>Avoid use of <code class=\"FUNCTION\">sscanf()</code> to parse <tt class=\"LITERAL\">ispell</tt> dictionary files (Artur Zakirov)</p>",
    "<p>This dodges a portability problem on FreeBSD-derived platforms (including macOS).</p>"
  ],
  [
    "<p>Fix atomic-operations code used on PPC with IBM's xlc compiler (Noah Misch)</p>",
    "<p>This error led to rare failures of concurrent operations on that platform.</p>"
  ],
  [
    "<p>Avoid a crash on old Windows versions (before 7SP1/2008R2SP1) with an AVX2-capable CPU and a Postgres build done with Visual Studio 2013 (Christian Ullrich)</p>",
    "<p>This is a workaround for a bug in Visual Studio 2013's runtime library, which Microsoft have stated they will not fix in that version.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">psql</span>'s tab completion logic to handle multibyte characters properly (Kyotaro Horiguchi, Robert Haas)</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">psql</span>'s tab completion for <tt class=\"LITERAL\">SECURITY LABEL</tt> (Tom Lane)</p>",
    "<p>Pressing TAB after <tt class=\"LITERAL\">SECURITY LABEL</tt> might cause a crash or offering of inappropriate keywords.</p>"
  ],
  [
    "<p>Make <span class=\"APPLICATION\">pg_ctl</span> accept a wait timeout from the <tt class=\"ENVAR\">PGCTLTIMEOUT</tt> environment variable, if none is specified on the command line (Noah Misch)</p>",
    "<p>This eases testing of slower buildfarm members by allowing them to globally specify a longer-than-normal timeout for postmaster startup and shutdown.</p>"
  ],
  [
    "<p>Fix incorrect test for Windows service status in <span class=\"APPLICATION\">pg_ctl</span> (Manuel Mathar)</p>",
    "<p>The previous set of minor releases attempted to fix <span class=\"APPLICATION\">pg_ctl</span> to properly determine whether to send log messages to Window's Event Log, but got the test backwards.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pgbench</span> to correctly handle the combination of <tt class=\"LITERAL\">-C</tt> and <tt class=\"LITERAL\">-M prepared</tt> options (Tom Lane)</p>"
  ],
  [
    "<p>In <span class=\"APPLICATION\">pg_upgrade</span>, skip creating a deletion script when the new data directory is inside the old data directory (Bruce Momjian)</p>",
    "<p>Blind application of the script in such cases would result in loss of the new data directory.</p>"
  ],
  [
    "<p>In PL/Perl, properly translate empty Postgres arrays into empty Perl arrays (Alex Hunsaker)</p>"
  ],
  [
    "<p>Make PL/Python cope with function names that aren't valid Python identifiers (Jim Nasby)</p>"
  ],
  [
    "<p>Fix multiple mistakes in the statistics returned by <tt class=\"FILENAME\">contrib/pgstattuple</tt>'s <code class=\"FUNCTION\">pgstatindex()</code> function (Tom Lane)</p>"
  ],
  [
    "<p>Remove dependency on <tt class=\"LITERAL\">psed</tt> in MSVC builds, since it's no longer provided by core Perl (Michael Paquier, Andrew Dunstan)</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"APPLICATION\">tzdata</span> release 2016c for DST law changes in Azerbaijan, Chile, Haiti, Palestine, and Russia (Altai, Astrakhan, Kirov, Sakhalin, Ulyanovsk regions), plus historical corrections for Lithuania, Moldova, and Russia (Kaliningrad, Samara, Volgograd).</p>"
  ]
]