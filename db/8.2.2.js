[
  [
    "<p>Remove security vulnerabilities that allowed connected\n          users to read backend memory (Tom)</p>",
    "<p>The vulnerabilities involve suppressing the normal\n          check that a SQL function returns the data type it's\n          declared to, and changing the data type of a table column\n          (CVE-2007-0555, CVE-2007-0556). These errors can easily\n          be exploited to cause a backend crash, and in principle\n          might be used to read database content that the user\n          should not be able to access.</p>"
  ],
  [
    "<p>Fix not-so-rare-anymore bug wherein btree index page\n          splits could fail due to choosing an infeasible split\n          point (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Fix Borland C compile scripts (L Bayuk)</p>"
  ],
  [
    "<p>Properly handle <code class=\"function\">to_char('CC')</code> for years ending in\n          <code class=\"literal\">00</code> (Tom)</p>",
    "<p>Year 2000 is in the twentieth century, not the\n          twenty-first.</p>"
  ],
  [
    "<p><code class=\"filename\">/contrib/tsearch2</code>\n          localization improvements (Tatsuo, Teodor)</p>"
  ],
  [
    "<p>Fix incorrect permission check in <code class=\"literal\">information_schema.key_column_usage</code> view\n          (Tom)</p>",
    "<p>The symptom is <span class=\"quote\">&#x201C;<span class=\"quote\">relation with OID nnnnn does not\n          exist</span>&#x201D;</span> errors. To get this fix without\n          using <code class=\"command\">initdb</code>, use\n          <code class=\"command\">CREATE OR REPLACE VIEW</code> to\n          install the corrected definition found in <code class=\"filename\">share/information_schema.sql</code>. Note you\n          will need to do this in each database.</p>"
  ],
  [
    "<p>Improve <code class=\"command\">VACUUM</code>\n          performance for databases with many tables (Tom)</p>"
  ],
  [
    "<p>Fix for rare Assert() crash triggered by <code class=\"literal\">UNION</code> (Tom)</p>"
  ],
  [
    "<p>Fix potentially incorrect results from index searches\n          using <code class=\"literal\">ROW</code> inequality\n          conditions (Tom)</p>"
  ],
  [
    "<p>Tighten security of multi-byte character processing\n          for UTF8 sequences over three bytes long (Tom)</p>"
  ],
  [
    "<p>Fix bogus <span class=\"quote\">&#x201C;<span class=\"quote\">permission denied</span>&#x201D;</span> failures\n          occurring on Windows due to attempts to fsync\n          already-deleted files (Magnus, Tom)</p>"
  ],
  [
    "<p>Fix bug that could cause the statistics collector to\n          hang on Windows (Magnus)</p>",
    "<p>This would in turn lead to autovacuum not working.</p>"
  ],
  [
    "<p>Fix possible crashes when an already-in-use PL/pgSQL\n          function is updated (Tom)</p>"
  ],
  [
    "<p>Improve PL/pgSQL handling of domain types (Sergiy\n          Vyshnevetskiy, Tom)</p>"
  ],
  [
    "<p>Fix possible errors in processing PL/pgSQL exception\n          blocks (Tom)</p>"
  ]
]