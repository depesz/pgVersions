[
  [
    "<p>Disallow <code class=\"command\">RESET ROLE</code> and\n          <code class=\"command\">RESET SESSION AUTHORIZATION</code>\n          inside security-definer functions (Tom, Heikki)</p>",
    "<p>This covers a case that was missed in the previous\n          patch that disallowed <code class=\"command\">SET\n          ROLE</code> and <code class=\"command\">SET SESSION\n          AUTHORIZATION</code> inside security-definer functions.\n          (See CVE-2007-6600)</p>"
  ],
  [
    "<p>Fix handling of sub-SELECTs appearing in the arguments\n          of an outer-level aggregate function (Tom)</p>"
  ],
  [
    "<p>Fix hash calculation for data type <code class=\"type\">interval</code> (Tom)</p>",
    "<p>This corrects wrong results for hash joins on interval\n          values. It also changes the contents of hash indexes on\n          interval columns. If you have any such indexes, you must\n          <code class=\"command\">REINDEX</code> them after\n          updating.</p>"
  ],
  [
    "<p>Fix overflow for <code class=\"literal\">INTERVAL\n          '<em class=\"replaceable\"><code>x</code></em> ms'</code>\n          when <em class=\"replaceable\"><code>x</code></em> is more\n          than 2 million and integer datetimes are in use (Alex\n          Hunsaker)</p>"
  ],
  [
    "<p>Fix calculation of distance between a point and a line\n          segment (Tom)</p>",
    "<p>This led to incorrect results from a number of\n          geometric operators.</p>"
  ],
  [
    "<p>Fix <code class=\"type\">money</code> data type to work\n          in locales where currency amounts have no fractional\n          digits, e.g. Japan (Itagaki Takahiro)</p>"
  ],
  [
    "<p>Properly round datetime input like <code class=\"literal\">00:12:57.9999999999999999999999999999</code>\n          (Tom)</p>"
  ],
  [
    "<p>Fix poor choice of page split point in GiST R-tree\n          operator classes (Teodor)</p>"
  ],
  [
    "<p>Fix portability issues in plperl initialization\n          (Andrew Dunstan)</p>"
  ],
  [
    "<p>Improve robustness of <span class=\"application\">libpq</span>'s code to recover from errors\n          during <code class=\"command\">COPY FROM STDIN</code>\n          (Tom)</p>"
  ],
  [
    "<p>Avoid including conflicting readline and editline\n          header files when both libraries are installed (Zdenek\n          Kotala)</p>"
  ]
]