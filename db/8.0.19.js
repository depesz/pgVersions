[
  [
    "<p>Fix backend crash when the client encoding cannot\n          represent a localized error message (Tom)</p>",
    "<p>We have addressed similar issues before, but it would\n          still fail if the <span class=\"quote\">&#x201C;<span class=\"quote\">character has no equivalent</span>&#x201D;</span>\n          message itself couldn't be converted. The fix is to\n          disable localization and send the plain ASCII error\n          message when we detect such a situation.</p>"
  ],
  [
    "<p>Fix possible crash when deeply nested functions are\n          invoked from a trigger (Tom)</p>"
  ],
  [
    "<p>Ensure an error is reported when a newly-defined\n          PL/pgSQL trigger function is invoked as a normal function\n          (Tom)</p>"
  ],
  [
    "<p>Fix incorrect tsearch2 headline generation when single\n          query item matches first word of text (Sushant Sinha)</p>"
  ],
  [
    "<p>Fix improper display of fractional seconds in interval\n          values when using a non-ISO datestyle in an <code class=\"option\">--enable-integer-datetimes</code> build (Ron\n          Mayer)</p>"
  ],
  [
    "<p>Ensure <code class=\"function\">SPI_getvalue</code> and\n          <code class=\"function\">SPI_getbinval</code> behave\n          correctly when the passed tuple and tuple descriptor have\n          different numbers of columns (Tom)</p>",
    "<p>This situation is normal when a table has had columns\n          added or removed, but these two functions didn't handle\n          it properly. The only likely consequence is an incorrect\n          error indication.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">ecpg</span>'s parsing of\n          <code class=\"command\">CREATE USER</code> (Michael)</p>"
  ],
  [
    "<p>Fix recent breakage of <code class=\"literal\">pg_ctl\n          restart</code> (Tom)</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2008i (for DST law\n          changes in Argentina, Brazil, Mauritius, Syria)</p>"
  ]
]