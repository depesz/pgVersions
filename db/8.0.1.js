[
  [
    "<p>Disallow <code class=\"command\">LOAD</code> to\n          non-superusers</p>",
    "<p>On platforms that will automatically execute\n          initialization functions of a shared library (this\n          includes at least Windows and ELF-based Unixen),\n          <code class=\"command\">LOAD</code> can be used to make the\n          server execute arbitrary code. Thanks to NGS Software for\n          reporting this.</p>"
  ],
  [
    "<p>Check that creator of an aggregate function has the\n          right to execute the specified transition functions</p>",
    "<p>This oversight made it possible to bypass denial of\n          EXECUTE permission on a function.</p>"
  ],
  [
    "<p>Fix security and 64-bit issues in contrib/intagg</p>"
  ],
  [
    "<p>Add needed STRICT marking to some contrib functions\n          (Kris Jurka)</p>"
  ],
  [
    "<p>Avoid buffer overrun when plpgsql cursor declaration\n          has too many parameters (Neil)</p>"
  ],
  [
    "<p>Make <code class=\"command\">ALTER TABLE ADD\n          COLUMN</code> enforce domain constraints in all cases</p>"
  ],
  [
    "<p>Fix planning error for FULL and RIGHT outer joins</p>",
    "<p>The result of the join was mistakenly supposed to be\n          sorted the same as the left input. This could not only\n          deliver mis-sorted output to the user, but in case of\n          nested merge joins could give outright wrong answers.</p>"
  ],
  [
    "<p>Improve planning of grouped aggregate queries</p>"
  ],
  [
    "<p><code class=\"command\">ROLLBACK TO <em class=\"replaceable\"><code>savepoint</code></em></code> closes\n          cursors created since the savepoint</p>"
  ],
  [
    "<p>Fix inadequate backend stack size on Windows</p>"
  ],
  [
    "<p>Avoid SHGetSpecialFolderPath() on Windows (Magnus)</p>"
  ],
  [
    "<p>Fix some problems in running pg_autovacuum as a\n          Windows service (Dave Page)</p>"
  ],
  [
    "<p>Multiple minor bug fixes in pg_dump/pg_restore</p>"
  ],
  [
    "<p>Fix ecpg segfault with named structs used in typedefs\n          (Michael)</p>"
  ]
]