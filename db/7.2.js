[
  [
    "<p>Create temporary files in a separate directory\n            (Bruce)</p>"
  ],
  [
    "<p>Delete orphaned temporary files on postmaster\n            startup (Bruce)</p>"
  ],
  [
    "<p>Added unique indexes to some system tables (Tom)</p>"
  ],
  [
    "<p>System table operator reorganization (Oleg Bartunov,\n            Teodor Sigaev, Tom)</p>"
  ],
  [
    "<p>Renamed pg_log to pg_clog (Tom)</p>"
  ],
  [
    "<p>Enable SIGTERM, SIGQUIT to kill backends (Jan)</p>"
  ],
  [
    "<p>Removed compile-time limit on number of backends\n            (Tom)</p>"
  ],
  [
    "<p>Better cleanup for semaphore resource failure\n            (Tatsuo, Tom)</p>"
  ],
  [
    "<p>Allow safe transaction ID wraparound (Tom)</p>"
  ],
  [
    "<p>Removed OIDs from some system tables (Tom)</p>"
  ],
  [
    "<p>Removed \"triggered data change violation\" error\n            check (Tom)</p>"
  ],
  [
    "<p>SPI portal creation of prepared/saved plans\n            (Jan)</p>"
  ],
  [
    "<p>Allow SPI column functions to work for system\n            columns (Tom)</p>"
  ],
  [
    "<p>Long value compression improvement (Tom)</p>"
  ],
  [
    "<p>Statistics collector for table, index access\n            (Jan)</p>"
  ],
  [
    "<p>Truncate extra-long sequence names to a reasonable\n            value (Tom)</p>"
  ],
  [
    "<p>Measure transaction times in milliseconds\n            (Thomas)</p>"
  ],
  [
    "<p>Fix TID sequential scans (Hiroshi)</p>"
  ],
  [
    "<p>Superuser ID now fixed at 1 (Peter E)</p>"
  ],
  [
    "<p>New pg_ctl \"reload\" option (Tom)</p>"
  ],
  [
    "<p>Optimizer improvements (Tom)</p>"
  ],
  [
    "<p>New histogram column statistics for optimizer\n            (Tom)</p>"
  ],
  [
    "<p>Reuse write-ahead log files rather than discarding\n            them (Tom)</p>"
  ],
  [
    "<p>Cache improvements (Tom)</p>"
  ],
  [
    "<p>IS NULL, IS NOT NULL optimizer improvement (Tom)</p>"
  ],
  [
    "<p>Improve lock manager to reduce lock contention\n            (Tom)</p>"
  ],
  [
    "<p>Keep relcache entries for index access support\n            functions (Tom)</p>"
  ],
  [
    "<p>Allow better selectivity with NaN and infinities in\n            NUMERIC (Tom)</p>"
  ],
  [
    "<p>R-tree performance improvements (Kenneth Been)</p>"
  ],
  [
    "<p>B-tree splits more efficient (Tom)</p>"
  ],
  [
    "<p>Change UPDATE, DELETE privileges to be distinct\n            (Peter E)</p>"
  ],
  [
    "<p>New REFERENCES, TRIGGER privileges (Peter E)</p>"
  ],
  [
    "<p>Allow GRANT/REVOKE to/from more than one user at a\n            time (Peter E)</p>"
  ],
  [
    "<p>New has_table_privilege() function (Joe Conway)</p>"
  ],
  [
    "<p>Allow non-superuser to vacuum database (Tom)</p>"
  ],
  [
    "<p>New SET SESSION AUTHORIZATION command (Peter E)</p>"
  ],
  [
    "<p>Fix bug in privilege modifications on newly created\n            tables (Tom)</p>"
  ],
  [
    "<p>Disallow access to pg_statistic for non-superuser,\n            add user-accessible views (Tom)</p>"
  ],
  [
    "<p>Fork postmaster before doing authentication to\n            prevent hangs (Peter E)</p>"
  ],
  [
    "<p>Add ident authentication over Unix domain sockets on\n            Linux, *BSD (Helge Bahmann, Oliver Elphick, Teodor\n            Sigaev, Bruce)</p>"
  ],
  [
    "<p>Add a password authentication method that uses MD5\n            encryption (Bruce)</p>"
  ],
  [
    "<p>Allow encryption of stored passwords using MD5\n            (Bruce)</p>"
  ],
  [
    "<p>PAM authentication (Dominic J. Eidson)</p>"
  ],
  [
    "<p>Load pg_hba.conf and pg_ident.conf only on startup\n            and SIGHUP (Bruce)</p>"
  ],
  [
    "<p>Interpretation of some time zone abbreviations as\n            Australian rather than North American now settable at\n            run time (Bruce)</p>"
  ],
  [
    "<p>New parameter to set default transaction isolation\n            level (Peter E)</p>"
  ],
  [
    "<p>New parameter to enable conversion of \"expr = NULL\"\n            into \"expr IS NULL\", off by default (Peter E)</p>"
  ],
  [
    "<p>New parameter to control memory usage by VACUUM\n            (Tom)</p>"
  ],
  [
    "<p>New parameter to set client authentication timeout\n            (Tom)</p>"
  ],
  [
    "<p>New parameter to set maximum number of open files\n            (Tom)</p>"
  ],
  [
    "<p>Statements added by INSERT rules now execute after\n            the INSERT (Jan)</p>"
  ],
  [
    "<p>Prevent unadorned relation names in target list\n            (Bruce)</p>"
  ],
  [
    "<p>NULLs now sort after all normal values in ORDER BY\n            (Tom)</p>"
  ],
  [
    "<p>New IS UNKNOWN, IS NOT UNKNOWN Boolean tests\n            (Tom)</p>"
  ],
  [
    "<p>New SHARE UPDATE EXCLUSIVE lock mode (Tom)</p>"
  ],
  [
    "<p>New EXPLAIN ANALYZE command that shows run times and\n            row counts (Martijn van Oosterhout)</p>"
  ],
  [
    "<p>Fix problem with LIMIT and subqueries (Tom)</p>"
  ],
  [
    "<p>Fix for LIMIT, DISTINCT ON pushed into subqueries\n            (Tom)</p>"
  ],
  [
    "<p>Fix nested EXCEPT/INTERSECT (Tom)</p>"
  ],
  [
    "<p>Fix SERIAL in temporary tables (Bruce)</p>"
  ],
  [
    "<p>Allow temporary sequences (Bruce)</p>"
  ],
  [
    "<p>Sequences now use int8 internally (Tom)</p>"
  ],
  [
    "<p>New SERIAL8 creates int8 columns with sequences,\n            default still SERIAL4 (Tom)</p>"
  ],
  [
    "<p>Make OIDs optional using WITHOUT OIDS (Tom)</p>"
  ],
  [
    "<p>Add %TYPE syntax to CREATE TYPE (Ian Lance\n            Taylor)</p>"
  ],
  [
    "<p>Add ALTER TABLE / DROP CONSTRAINT for CHECK\n            constraints (Christopher Kings-Lynne)</p>"
  ],
  [
    "<p>New CREATE OR REPLACE FUNCTION to alter existing\n            function (preserving the function OID) (Gavin\n            Sherry)</p>"
  ],
  [
    "<p>Add ALTER TABLE / ADD [ UNIQUE | PRIMARY ]\n            (Christopher Kings-Lynne)</p>"
  ],
  [
    "<p>Allow column renaming in views</p>"
  ],
  [
    "<p>Make ALTER TABLE / RENAME COLUMN update column names\n            of indexes (Brent Verner)</p>"
  ],
  [
    "<p>Fix for ALTER TABLE / ADD CONSTRAINT ... CHECK with\n            inherited tables (Stephan Szabo)</p>"
  ],
  [
    "<p>ALTER TABLE RENAME update foreign-key trigger\n            arguments correctly (Brent Verner)</p>"
  ],
  [
    "<p>DROP AGGREGATE and COMMENT ON AGGREGATE now accept\n            an aggtype (Tom)</p>"
  ],
  [
    "<p>Add automatic return type data casting for SQL\n            functions (Tom)</p>"
  ],
  [
    "<p>Allow GiST indexes to handle NULLs and multikey\n            indexes (Oleg Bartunov, Teodor Sigaev, Tom)</p>"
  ],
  [
    "<p>Enable partial indexes (Martijn van Oosterhout)</p>"
  ],
  [
    "<p>Add RESET ALL, SHOW ALL (Marko Kreen)</p>"
  ],
  [
    "<p>CREATE/ALTER USER/GROUP now allow options in any\n            order (Vince)</p>"
  ],
  [
    "<p>Add LOCK A, B, C functionality (Neil Padgett)</p>"
  ],
  [
    "<p>New ENCRYPTED/UNENCRYPTED option to CREATE/ALTER\n            USER (Bruce)</p>"
  ],
  [
    "<p>New light-weight VACUUM does not lock table; old\n            semantics are available as VACUUM FULL (Tom)</p>"
  ],
  [
    "<p>Disable COPY TO/FROM on views (Bruce)</p>"
  ],
  [
    "<p>COPY DELIMITERS string must be exactly one character\n            (Tom)</p>"
  ],
  [
    "<p>VACUUM warning about index tuples fewer than heap\n            now only appears when appropriate (Martijn van\n            Oosterhout)</p>"
  ],
  [
    "<p>Fix privilege checks for CREATE INDEX (Tom)</p>"
  ],
  [
    "<p>Disallow inappropriate use of CREATE/DROP\n            INDEX/TRIGGER/VIEW (Tom)</p>"
  ],
  [
    "<p>SUM(), AVG(), COUNT() now uses int8 internally for\n            speed (Tom)</p>"
  ],
  [
    "<p>Add convert(), convert2() (Tatsuo)</p>"
  ],
  [
    "<p>New function bit_length() (Peter E)</p>"
  ],
  [
    "<p>Make the \"n\" in CHAR(n)/VARCHAR(n) represents\n            letters, not bytes (Tatsuo)</p>"
  ],
  [
    "<p>CHAR(), VARCHAR() now reject strings that are too\n            long (Peter E)</p>"
  ],
  [
    "<p>BIT VARYING now rejects bit strings that are too\n            long (Peter E)</p>"
  ],
  [
    "<p>BIT now rejects bit strings that do not match\n            declared size (Peter E)</p>"
  ],
  [
    "<p>INET, CIDR text conversion functions (Alex\n            Pilosov)</p>"
  ],
  [
    "<p>INET, CIDR operators &lt;&lt; and &lt;&lt;=\n            indexable (Alex Pilosov)</p>"
  ],
  [
    "<p>Bytea \\### now requires valid three digit octal\n            number</p>"
  ],
  [
    "<p>Bytea comparison improvements, now supports =,\n            &lt;&gt;, &gt;, &gt;=, &lt;, and &lt;=</p>"
  ],
  [
    "<p>Bytea now supports B-tree indexes</p>"
  ],
  [
    "<p>Bytea now supports LIKE, LIKE...ESCAPE, NOT LIKE,\n            NOT LIKE...ESCAPE</p>"
  ],
  [
    "<p>Bytea now supports concatenation</p>"
  ],
  [
    "<p>New bytea functions: position, substring, trim,\n            btrim, and length</p>"
  ],
  [
    "<p>New encode() function mode, \"escaped\", converts\n            minimally escaped bytea to/from text</p>"
  ],
  [
    "<p>Add pg_database_encoding_max_length() (Tatsuo)</p>"
  ],
  [
    "<p>Add pg_client_encoding() function (Tatsuo)</p>"
  ],
  [
    "<p>now() returns time with millisecond precision\n            (Thomas)</p>"
  ],
  [
    "<p>New TIMESTAMP WITHOUT TIMEZONE data type\n            (Thomas)</p>"
  ],
  [
    "<p>Add ISO date/time specification with \"T\",\n            yyyy-mm-ddThh:mm:ss (Thomas)</p>"
  ],
  [
    "<p>New xid/int comparison functions (Hiroshi)</p>"
  ],
  [
    "<p>Add precision to TIME, TIMESTAMP, and INTERVAL data\n            types (Thomas)</p>"
  ],
  [
    "<p>Modify type coercion logic to attempt\n            binary-compatible functions first (Tom)</p>"
  ],
  [
    "<p>New encode() function installed by default (Marko\n            Kreen)</p>"
  ],
  [
    "<p>Improved to_*() conversion functions (Karel Zak)</p>"
  ],
  [
    "<p>Optimize LIKE/ILIKE when using single-byte encodings\n            (Tatsuo)</p>"
  ],
  [
    "<p>New functions in contrib/pgcrypto: crypt(), hmac(),\n            encrypt(), gen_salt() (Marko Kreen)</p>"
  ],
  [
    "<p>Correct description of translate() function\n            (Bruce)</p>"
  ],
  [
    "<p>Add INTERVAL argument for SET TIME ZONE (Thomas)</p>"
  ],
  [
    "<p>Add INTERVAL YEAR TO MONTH (etc.) syntax\n            (Thomas)</p>"
  ],
  [
    "<p>Optimize length functions when using single-byte\n            encodings (Tatsuo)</p>"
  ],
  [
    "<p>Fix path_inter, path_distance, path_length,\n            dist_ppath to handle closed paths (Curtis Barrett,\n            Tom)</p>"
  ],
  [
    "<p>octet_length(text) now returns non-compressed length\n            (Tatsuo, Bruce)</p>"
  ],
  [
    "<p>Handle \"July\" full name in date/time literals (Greg\n            Sabino Mullane)</p>"
  ],
  [
    "<p>Some datatype() function calls now evaluated\n            differently</p>"
  ],
  [
    "<p>Add support for Julian and ISO time specifications\n            (Thomas)</p>"
  ],
  [
    "<p>National language support in psql, <span class=\"application\">pg_dump</span>, libpq, and server (Peter\n            E)</p>"
  ],
  [
    "<p>Message translations in Chinese (simplified,\n            traditional), Czech, French, German, Hungarian,\n            Russian, Swedish (Peter E, Serguei A. Mokhov, Karel\n            Zak, Weiping He, Zhenbang Wei, Kovacs Zoltan)</p>"
  ],
  [
    "<p>Make trim, ltrim, rtrim, btrim, lpad, rpad,\n            translate multibyte aware (Tatsuo)</p>"
  ],
  [
    "<p>Add LATIN5,6,7,8,9,10 support (Tatsuo)</p>"
  ],
  [
    "<p>Add ISO 8859-5,6,7,8 support (Tatsuo)</p>"
  ],
  [
    "<p>Correct LATIN5 to mean ISO-8859-9, not ISO-8859-5\n            (Tatsuo)</p>"
  ],
  [
    "<p>Make mic2ascii() non-ASCII aware (Tatsuo)</p>"
  ],
  [
    "<p>Reject invalid multibyte character sequences\n            (Tatsuo)</p>"
  ],
  [
    "<p>Now uses portals for SELECT loops, allowing huge\n            result sets (Jan)</p>"
  ],
  [
    "<p>CURSOR and REFCURSOR support (Jan)</p>"
  ],
  [
    "<p>Can now return open cursors (Jan)</p>"
  ],
  [
    "<p>Add ELSEIF (Klaus Reger)</p>"
  ],
  [
    "<p>Improve PL/pgSQL error reporting, including location\n            of error (Tom)</p>"
  ],
  [
    "<p>Allow IS or FOR key words in cursor declaration, for\n            compatibility (Bruce)</p>"
  ],
  [
    "<p>Fix for SELECT ... FOR UPDATE (Tom)</p>"
  ],
  [
    "<p>Fix for PERFORM returning multiple rows (Tom)</p>"
  ],
  [
    "<p>Make PL/pgSQL use the server's type coercion code\n            (Tom)</p>"
  ],
  [
    "<p>Memory leak fix (Jan, Tom)</p>"
  ],
  [
    "<p>Make trailing semicolon optional (Tom)</p>"
  ],
  [
    "<p>New untrusted PL/Perl (Alex Pilosov)</p>"
  ],
  [
    "<p>PL/Perl is now built on some platforms even if\n            libperl is not shared (Peter E)</p>"
  ],
  [
    "<p>Now reports errorInfo (Vsevolod Lobko)</p>"
  ],
  [
    "<p>Add spi_lastoid function (bob@redivi.com)</p>"
  ],
  [
    "<p>...is new (Andrew Bosma)</p>"
  ],
  [
    "<p>\\d displays indexes in unique, primary groupings\n            (Christopher Kings-Lynne)</p>"
  ],
  [
    "<p>Allow trailing semicolons in backslash commands\n            (Greg Sabino Mullane)</p>"
  ],
  [
    "<p>Read password from /dev/tty if possible</p>"
  ],
  [
    "<p>Force new password prompt when changing user and\n            database (Tatsuo, Tom)</p>"
  ],
  [
    "<p>Format the correct number of columns for Unicode\n            (Patrice)</p>"
  ],
  [
    "<p>New function PQescapeString() to escape quotes in\n            command strings (Florian Weimer)</p>"
  ],
  [
    "<p>New function PQescapeBytea() escapes binary strings\n            for use as SQL string literals</p>"
  ],
  [
    "<p>Return OID of INSERT (Ken K)</p>"
  ],
  [
    "<p>Handle more data types (Ken K)</p>"
  ],
  [
    "<p>Handle single quotes and newlines in strings (Ken\n            K)</p>"
  ],
  [
    "<p>Handle NULL variables (Ken K)</p>"
  ],
  [
    "<p>Fix for time zone handling (Barry Lind)</p>"
  ],
  [
    "<p>Improved Druid support</p>"
  ],
  [
    "<p>Allow eight-bit characters with non-multibyte server\n            (Barry Lind)</p>"
  ],
  [
    "<p>Support BIT, BINARY types (Ned Wolpert)</p>"
  ],
  [
    "<p>Reduce memory usage (Michael Stephens, Dave\n            Cramer)</p>"
  ],
  [
    "<p>Update DatabaseMetaData (Peter E)</p>"
  ],
  [
    "<p>Add DatabaseMetaData.getCatalogs() (Peter E)</p>"
  ],
  [
    "<p>Encoding fixes (Anders Bengtsson)</p>"
  ],
  [
    "<p>Get/setCatalog methods (Jason Davies)</p>"
  ],
  [
    "<p>DatabaseMetaData.getColumns() now returns column\n            defaults (Jason Davies)</p>"
  ],
  [
    "<p>DatabaseMetaData.getColumns() performance\n            improvement (Jeroen van Vianen)</p>"
  ],
  [
    "<p>Some JDBC1 and JDBC2 merging (Anders Bengtsson)</p>"
  ],
  [
    "<p>Transaction performance improvements (Barry\n            Lind)</p>"
  ],
  [
    "<p>Array fixes (Greg Zoller)</p>"
  ],
  [
    "<p>Serialize addition</p>"
  ],
  [
    "<p>Fix batch processing (Rene Pijlman)</p>"
  ],
  [
    "<p>ExecSQL method reorganization (Anders Bengtsson)</p>"
  ],
  [
    "<p>GetColumn() fixes (Jeroen van Vianen)</p>"
  ],
  [
    "<p>Fix isWriteable() function (Rene Pijlman)</p>"
  ],
  [
    "<p>Improved passage of JDBC2 conformance tests (Rene\n            Pijlman)</p>"
  ],
  [
    "<p>Add bytea type capability (Barry Lind)</p>"
  ],
  [
    "<p>Add isNullable() (Rene Pijlman)</p>"
  ],
  [
    "<p>JDBC date/time test suite fixes (Liam Stewart)</p>"
  ],
  [
    "<p>Fix for SELECT 'id' AS xxx FROM table (Dave\n            Cramer)</p>"
  ],
  [
    "<p>Fix DatabaseMetaData to show precision properly\n            (Mark Lillywhite)</p>"
  ],
  [
    "<p>New getImported/getExported keys (Jason Davies)</p>"
  ],
  [
    "<p>MD5 password encryption support (Jeremy Wohl)</p>"
  ],
  [
    "<p>Fix to actually use type cache (Ned Wolpert)</p>"
  ],
  [
    "<p>Remove query size limit (Hiroshi)</p>"
  ],
  [
    "<p>Remove text field size limit (Hiroshi)</p>"
  ],
  [
    "<p>Fix for SQLPrimaryKeys in multibyte mode\n            (Hiroshi)</p>"
  ],
  [
    "<p>Allow ODBC procedure calls (Hiroshi)</p>"
  ],
  [
    "<p>Improve boolean handing (Aidan Mountford)</p>"
  ],
  [
    "<p>Most configuration options now settable via DSN\n            (Hiroshi)</p>"
  ],
  [
    "<p>Multibyte, performance fixes (Hiroshi)</p>"
  ],
  [
    "<p>Allow driver to be used with iODBC or unixODBC\n            (Peter E)</p>"
  ],
  [
    "<p>MD5 password encryption support (Bruce)</p>"
  ],
  [
    "<p>Add more compatibility functions to odbc.sql (Peter\n            E)</p>"
  ],
  [
    "<p>EXECUTE ... INTO implemented (Christof Petig)</p>"
  ],
  [
    "<p>Multiple row descriptor support (e.g. CARDINALITY)\n            (Christof Petig)</p>"
  ],
  [
    "<p>Fix for GRANT parameters (Lee Kindness)</p>"
  ],
  [
    "<p>Fix INITIALLY DEFERRED bug</p>"
  ],
  [
    "<p>Various bug fixes (Michael, Christof Petig)</p>"
  ],
  [
    "<p>Auto allocation for indicator variable arrays (int\n            *ind_p=NULL)</p>"
  ],
  [
    "<p>Auto allocation for string arrays (char\n            **foo_pp=NULL)</p>"
  ],
  [
    "<p>ECPGfree_auto_mem fixed</p>"
  ],
  [
    "<p>All function names with external linkage are now\n            prefixed by ECPG</p>"
  ],
  [
    "<p>Fixes for arrays of structures (Michael)</p>"
  ],
  [
    "<p>Python fix fetchone() (Gerhard Haring)</p>"
  ],
  [
    "<p>Use UTF, Unicode in Tcl where appropriate (Vsevolod\n            Lobko, Reinhard Max)</p>"
  ],
  [
    "<p>Add Tcl COPY TO/FROM (ljb)</p>"
  ],
  [
    "<p>Prevent output of default index op class in\n            <span class=\"application\">pg_dump</span> (Tom)</p>"
  ],
  [
    "<p>Fix libpgeasy memory leak (Bruce)</p>"
  ],
  [
    "<p>Configure, dynamic loader, and shared library fixes\n            (Peter E)</p>"
  ],
  [
    "<p>Fixes in QNX 4 port (Bernd Tegge)</p>"
  ],
  [
    "<p>Fixes in Cygwin and Windows ports (Jason Tishler,\n            Gerhard Haring, Dmitry Yurtaev, Darko Prenosil, Mikhail\n            Terekhov)</p>"
  ],
  [
    "<p>Fix for Windows socket communication failures\n            (Magnus, Mikhail Terekhov)</p>"
  ],
  [
    "<p>Hurd compile fix (Oliver Elphick)</p>"
  ],
  [
    "<p>BeOS fixes (Cyril Velter)</p>"
  ],
  [
    "<p>Remove configure --enable-unicode-conversion, now\n            enabled by multibyte (Tatsuo)</p>"
  ],
  [
    "<p>AIX fixes (Tatsuo, Andreas)</p>"
  ],
  [
    "<p>Fix parallel make (Peter E)</p>"
  ],
  [
    "<p>Install SQL language manual pages into OS-specific\n            directories (Peter E)</p>"
  ],
  [
    "<p>Rename config.h to pg_config.h (Peter E)</p>"
  ],
  [
    "<p>Reorganize installation layout of header files\n            (Peter E)</p>"
  ],
  [
    "<p>Remove SEP_CHAR (Bruce)</p>"
  ],
  [
    "<p>New GUC hooks (Tom)</p>"
  ],
  [
    "<p>Merge GUC and command line handling (Marko\n            Kreen)</p>"
  ],
  [
    "<p>Remove EXTEND INDEX (Martijn van Oosterhout,\n            Tom)</p>"
  ],
  [
    "<p>New pgjindent utility to indent java code\n            (Bruce)</p>"
  ],
  [
    "<p>Remove define of true/false when compiling under C++\n            (Leandro Fanzone, Tom)</p>"
  ],
  [
    "<p>pgindent fixes (Bruce, Tom)</p>"
  ],
  [
    "<p>Replace strcasecmp() with strcmp() where appropriate\n            (Peter E)</p>"
  ],
  [
    "<p>Dynahash portability improvements (Tom)</p>"
  ],
  [
    "<p>Add 'volatile' usage in spinlock structures</p>"
  ],
  [
    "<p>Improve signal handling logic (Tom)</p>"
  ],
  [
    "<p>New contrib/rtree_gist (Oleg Bartunov, Teodor\n            Sigaev)</p>"
  ],
  [
    "<p>New contrib/tsearch full-text indexing (Oleg, Teodor\n            Sigaev)</p>"
  ],
  [
    "<p>Add contrib/dblink for remote database access (Joe\n            Conway)</p>"
  ],
  [
    "<p>contrib/ora2pg Oracle conversion utility (Gilles\n            Darold)</p>"
  ],
  [
    "<p>contrib/xml XML conversion utility (John Gray)</p>"
  ],
  [
    "<p>contrib/fulltextindex fixes (Christopher\n            Kings-Lynne)</p>"
  ],
  [
    "<p>New contrib/fuzzystrmatch with levenshtein and\n            metaphone, soundex merged (Joe Conway)</p>"
  ],
  [
    "<p>Add contrib/intarray boolean queries, binary search,\n            fixes (Oleg Bartunov)</p>"
  ],
  [
    "<p>New pg_upgrade utility (Bruce)</p>"
  ],
  [
    "<p>Add new pg_resetxlog options (Bruce, Tom)</p>"
  ]
]