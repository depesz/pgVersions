[
  [
    "<p>Restore functionality of <code class=\"command\">ALTER {ROLE|DATABASE} SET role</code> (Tom Lane, Noah Misch) <a class=\"ulink\" href=\"https://postgr.es/c/07c6e0f61\" target=\"_top\">§</a></p>",
    "<p>The fix for CVE-2024-10978 accidentally caused settings for <code class=\"varname\">role</code> to not be applied if they come from non-interactive sources, including previous <code class=\"command\">ALTER {ROLE|DATABASE}</code> commands and the <code class=\"varname\">PGOPTIONS</code> environment variable.</p>"
  ],
  [
    "<p>Fix cases where a logical replication slot's <code class=\"structfield\">restart_lsn</code> could go backwards (Masahiko Sawada) <a class=\"ulink\" href=\"https://postgr.es/c/15dc1abb1\" target=\"_top\">§</a></p>",
    "<p>Previously, restarting logical replication could sometimes cause the slot's restart point to be recomputed as an older value than had previously been advertised in <code class=\"structname\">pg_replication_slots</code>. This is bad, since for example WAL files might have been removed on the basis of the later <code class=\"structfield\">restart_lsn</code> value, in which case replication would fail to restart.</p>"
  ],
  [
    "<p>Count index scans in <code class=\"filename\">contrib/bloom</code> indexes in the statistics views, such as the <code class=\"structname\">pg_stat_user_indexes</code>.<code class=\"structfield\">idx_scan</code> counter (Masahiro Ikeda) <a class=\"ulink\" href=\"https://postgr.es/c/e493ea866\" target=\"_top\">§</a></p>"
  ],
  [
    "<p>Fix crash when checking to see if an index's opclass options have changed (Alexander Korotkov) <a class=\"ulink\" href=\"https://postgr.es/c/5411e8213\" target=\"_top\">§</a></p>",
    "<p>Some forms of <code class=\"command\">ALTER TABLE</code> would fail if the table has an index with non-default operator class options.</p>"
  ]
]