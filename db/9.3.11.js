[
  [
    "<p>Fix infinite loops and buffer-overrun problems in\n          regular expressions (Tom Lane)</p>",
    "<p>Very large character ranges in bracket expressions\n          could cause infinite loops in some cases, and memory\n          overwrites in other cases. (CVE-2016-0773)</p>"
  ],
  [
    "<p>Perform an immediate shutdown if the <code class=\"filename\">postmaster.pid</code> file is removed (Tom\n          Lane)</p>",
    "<p>The postmaster now checks every minute or so that\n          <code class=\"filename\">postmaster.pid</code> is still\n          there and still contains its own PID. If not, it performs\n          an immediate shutdown, as though it had received\n          <span class=\"systemitem\">SIGQUIT</span>. The main\n          motivation for this change is to ensure that failed\n          buildfarm runs will get cleaned up without manual\n          intervention; but it also serves to limit the bad effects\n          if a DBA forcibly removes <code class=\"filename\">postmaster.pid</code> and then starts a new\n          postmaster.</p>"
  ],
  [
    "<p>In <code class=\"literal\">SERIALIZABLE</code>\n          transaction isolation mode, serialization anomalies could\n          be missed due to race conditions during insertions (Kevin\n          Grittner, Thomas Munro)</p>"
  ],
  [
    "<p>Fix failure to emit appropriate WAL records when doing\n          <code class=\"literal\">ALTER TABLE ... SET\n          TABLESPACE</code> for unlogged relations (Michael\n          Paquier, Andres Freund)</p>",
    "<p>Even though the relation's data is unlogged, the move\n          must be logged or the relation will be inaccessible after\n          a standby is promoted to master.</p>"
  ],
  [
    "<p>Fix possible misinitialization of unlogged relations\n          at the end of crash recovery (Andres Freund, Michael\n          Paquier)</p>"
  ],
  [
    "<p>Ensure walsender slots are fully re-initialized when\n          being re-used (Magnus Hagander)</p>"
  ],
  [
    "<p>Fix <code class=\"command\">ALTER COLUMN TYPE</code> to\n          reconstruct inherited check constraints properly (Tom\n          Lane)</p>"
  ],
  [
    "<p>Fix <code class=\"command\">REASSIGN OWNED</code> to\n          change ownership of composite types properly (&#xC1;lvaro\n          Herrera)</p>"
  ],
  [
    "<p>Fix <code class=\"command\">REASSIGN OWNED</code> and\n          <code class=\"command\">ALTER OWNER</code> to correctly\n          update granted-permissions lists when changing owners of\n          data types, foreign data wrappers, or foreign servers\n          (Bruce Momjian, &#xC1;lvaro Herrera)</p>"
  ],
  [
    "<p>Fix <code class=\"command\">REASSIGN OWNED</code> to\n          ignore foreign user mappings, rather than fail (&#xC1;lvaro\n          Herrera)</p>"
  ],
  [
    "<p>Fix possible crash after doing query rewrite for an\n          updatable view (Stephen Frost)</p>"
  ],
  [
    "<p>Fix planner's handling of <code class=\"literal\">LATERAL</code> references (Tom Lane)</p>",
    "<p>This fixes some corner cases that led to <span class=\"quote\">&#x201C;<span class=\"quote\">failed to build any N-way\n          joins</span>&#x201D;</span> or <span class=\"quote\">&#x201C;<span class=\"quote\">could not devise a query plan</span>&#x201D;</span>\n          planner failures.</p>"
  ],
  [
    "<p>Add more defenses against bad planner cost estimates\n          for GIN index scans when the index's internal statistics\n          are very out-of-date (Tom Lane)</p>"
  ],
  [
    "<p>Make planner cope with hypothetical GIN indexes\n          suggested by an index advisor plug-in (Julien\n          Rouhaud)</p>"
  ],
  [
    "<p>Speed up generation of unique table aliases in\n          <code class=\"command\">EXPLAIN</code> and rule dumping,\n          and ensure that generated aliases do not exceed\n          <code class=\"literal\">NAMEDATALEN</code> (Tom Lane)</p>"
  ],
  [
    "<p>Fix dumping of whole-row Vars in <code class=\"literal\">ROW()</code> and <code class=\"literal\">VALUES()</code> lists (Tom Lane)</p>"
  ],
  [
    "<p>Fix possible internal overflow in <code class=\"type\">numeric</code> division (Dean Rasheed)</p>"
  ],
  [
    "<p>Fix enforcement of restrictions inside parentheses\n          within regular expression lookahead constraints (Tom\n          Lane)</p>",
    "<p>Lookahead constraints aren't allowed to contain\n          backrefs, and parentheses within them are always\n          considered non-capturing, according to the manual.\n          However, the code failed to handle these cases properly\n          inside a parenthesized subexpression, and would give\n          unexpected results.</p>"
  ],
  [
    "<p>Conversion of regular expressions to indexscan bounds\n          could produce incorrect bounds from regexps containing\n          lookahead constraints (Tom Lane)</p>"
  ],
  [
    "<p>Fix regular-expression compiler to handle loops of\n          constraint arcs (Tom Lane)</p>",
    "<p>The code added for CVE-2007-4772 was both incomplete,\n          in that it didn't handle loops involving more than one\n          state, and incorrect, in that it could cause assertion\n          failures (though there seem to be no bad consequences of\n          that in a non-assert build). Multi-state loops would\n          cause the compiler to run until the query was canceled or\n          it reached the too-many-states error condition.</p>"
  ],
  [
    "<p>Improve memory-usage accounting in regular-expression\n          compiler (Tom Lane)</p>",
    "<p>This causes the code to emit <span class=\"quote\">&#x201C;<span class=\"quote\">regular expression is too\n          complex</span>&#x201D;</span> errors in some cases that\n          previously used unreasonable amounts of time and\n          memory.</p>"
  ],
  [
    "<p>Improve performance of regular-expression compiler\n          (Tom Lane)</p>"
  ],
  [
    "<p>Make <code class=\"literal\">%h</code> and <code class=\"literal\">%r</code> escapes in <code class=\"varname\">log_line_prefix</code> work for messages\n          emitted due to <code class=\"varname\">log_connections</code> (Tom Lane)</p>",
    "<p>Previously, <code class=\"literal\">%h</code>/<code class=\"literal\">%r</code>\n          started to work just after a new session had emitted the\n          <span class=\"quote\">&#x201C;<span class=\"quote\">connection\n          received</span>&#x201D;</span> log message; now they work for\n          that message too.</p>"
  ],
  [
    "<p>On Windows, ensure the shared-memory mapping handle\n          gets closed in child processes that don't need it (Tom\n          Lane, Amit Kapila)</p>",
    "<p>This oversight resulted in failure to recover from\n          crashes whenever <code class=\"varname\">logging_collector</code> is turned on.</p>"
  ],
  [
    "<p>Fix possible failure to detect socket EOF in\n          non-blocking mode on Windows (Tom Lane)</p>",
    "<p>It's not entirely clear whether this problem can\n          happen in pre-9.5 branches, but if it did, the symptom\n          would be that a walsender process would wait indefinitely\n          rather than noticing a loss of connection.</p>"
  ],
  [
    "<p>Avoid leaking a token handle during SSPI\n          authentication (Christian Ullrich)</p>"
  ],
  [
    "<p>In <span class=\"application\">psql</span>, ensure that\n          <span class=\"application\">libreadline</span>'s idea of\n          the screen size is updated when the terminal window size\n          changes (Merlin Moncure)</p>",
    "<p>Previously, <span class=\"application\">libreadline</span> did not notice if the\n          window was resized during query output, leading to\n          strange behavior during later input of multiline\n          queries.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">psql</span>'s\n          <code class=\"literal\">\\det</code> command to interpret\n          its pattern argument the same way as other <code class=\"literal\">\\d</code> commands with potentially\n          schema-qualified patterns do (Reece Hart)</p>"
  ],
  [
    "<p>Avoid possible crash in <span class=\"application\">psql</span>'s <code class=\"literal\">\\c</code> command when previous connection was\n          via Unix socket and command specifies a new hostname and\n          same username (Tom Lane)</p>"
  ],
  [
    "<p>In <code class=\"literal\">pg_ctl start -w</code>, test\n          child process status directly rather than relying on\n          heuristics (Tom Lane, Michael Paquier)</p>",
    "<p>Previously, <span class=\"application\">pg_ctl</span>\n          relied on an assumption that the new postmaster would\n          always create <code class=\"filename\">postmaster.pid</code> within five seconds. But\n          that can fail on heavily-loaded systems, causing\n          <span class=\"application\">pg_ctl</span> to report\n          incorrectly that the postmaster failed to start.</p>",
    "<p>Except on Windows, this change also means that a\n          <code class=\"literal\">pg_ctl start -w</code> done\n          immediately after another such command will now reliably\n          fail, whereas previously it would report success if done\n          within two seconds of the first command.</p>"
  ],
  [
    "<p>In <code class=\"literal\">pg_ctl start -w</code>, don't\n          attempt to use a wildcard listen address to connect to\n          the postmaster (Kondo Yuta)</p>",
    "<p>On Windows, <span class=\"application\">pg_ctl</span>\n          would fail to detect postmaster startup if <code class=\"varname\">listen_addresses</code> is set to <code class=\"literal\">0.0.0.0</code> or <code class=\"literal\">::</code>, because it would try to use that\n          value verbatim as the address to connect to, which\n          doesn't work. Instead assume that <code class=\"literal\">127.0.0.1</code> or <code class=\"literal\">::1</code>, respectively, is the right thing to\n          use.</p>"
  ],
  [
    "<p>In <span class=\"application\">pg_ctl</span> on Windows,\n          check service status to decide where to send output,\n          rather than checking if standard output is a terminal\n          (Michael Paquier)</p>"
  ],
  [
    "<p>In <span class=\"application\">pg_dump</span> and\n          <span class=\"application\">pg_basebackup</span>, adopt the\n          GNU convention for handling tar-archive members exceeding\n          8GB (Tom Lane)</p>",
    "<p>The POSIX standard for <code class=\"literal\">tar</code> file format does not allow archive\n          member files to exceed 8GB, but most modern\n          implementations of <span class=\"application\">tar</span>\n          support an extension that fixes that. Adopt this\n          extension so that <span class=\"application\">pg_dump</span> with <code class=\"option\">-Ft</code> no longer fails on tables with more\n          than 8GB of data, and so that <span class=\"application\">pg_basebackup</span> can handle files\n          larger than 8GB. In addition, fix some portability issues\n          that could cause failures for members between 4GB and 8GB\n          on some platforms. Potentially these problems could cause\n          unrecoverable data loss due to unreadable backup\n          files.</p>"
  ],
  [
    "<p>Fix assorted corner-case bugs in <span class=\"application\">pg_dump</span>'s processing of extension\n          member objects (Tom Lane)</p>"
  ],
  [
    "<p>Make <span class=\"application\">pg_dump</span> mark a\n          view's triggers as needing to be processed after its\n          rule, to prevent possible failure during parallel\n          <span class=\"application\">pg_restore</span> (Tom\n          Lane)</p>"
  ],
  [
    "<p>Ensure that relation option values are properly quoted\n          in <span class=\"application\">pg_dump</span> (Kouhei\n          Sutou, Tom Lane)</p>",
    "<p>A reloption value that isn't a simple identifier or\n          number could lead to dump/reload failures due to syntax\n          errors in CREATE statements issued by <span class=\"application\">pg_dump</span>. This is not an issue with\n          any reloption currently supported by core <span class=\"productname\">PostgreSQL</span>, but extensions could\n          allow reloptions that cause the problem.</p>"
  ],
  [
    "<p>Avoid repeated password prompts during parallel\n          <span class=\"application\">pg_dump</span> (Zeus\n          Kronion)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_upgrade</span>'s\n          file-copying code to handle errors properly on Windows\n          (Bruce Momjian)</p>"
  ],
  [
    "<p>Install guards in <span class=\"application\">pgbench</span> against corner-case overflow\n          conditions during evaluation of script-specified division\n          or modulo operators (Fabien Coelho, Michael Paquier)</p>"
  ],
  [
    "<p>Fix failure to localize messages emitted by\n          <span class=\"application\">pg_receivexlog</span> and\n          <span class=\"application\">pg_recvlogical</span> (Ioseph\n          Kim)</p>"
  ],
  [
    "<p>Avoid dump/reload problems when using both\n          <span class=\"application\">plpython2</span> and\n          <span class=\"application\">plpython3</span> (Tom Lane)</p>",
    "<p>In principle, both versions of <span class=\"application\">PL/Python</span> can be used in the same\n          database, though not in the same session (because the two\n          versions of <span class=\"application\">libpython</span>\n          cannot safely be used concurrently). However,\n          <span class=\"application\">pg_restore</span> and\n          <span class=\"application\">pg_upgrade</span> both do\n          things that can fall foul of the same-session\n          restriction. Work around that by changing the timing of\n          the check.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">PL/Python</span>\n          regression tests to pass with Python 3.5 (Peter\n          Eisentraut)</p>"
  ],
  [
    "<p>Fix premature clearing of <span class=\"application\">libpq</span>'s input buffer when socket EOF\n          is seen (Tom Lane)</p>",
    "<p>This mistake caused <span class=\"application\">libpq</span> to sometimes not report the\n          backend's final error message before reporting\n          <span class=\"quote\">&#x201C;<span class=\"quote\">server closed\n          the connection unexpectedly</span>&#x201D;</span>.</p>"
  ],
  [
    "<p>Prevent certain <span class=\"application\">PL/Java</span> parameters from being set by\n          non-superusers (Noah Misch)</p>",
    "<p>This change mitigates a <span class=\"application\">PL/Java</span> security bug\n          (CVE-2016-0766), which was fixed in <span class=\"application\">PL/Java</span> by marking these parameters\n          as superuser-only. To fix the security hazard for sites\n          that update <span class=\"productname\">PostgreSQL</span>\n          more frequently than <span class=\"application\">PL/Java</span>, make the core code aware of\n          them also.</p>"
  ],
  [
    "<p>Improve <span class=\"application\">libpq</span>'s\n          handling of out-of-memory situations (Michael Paquier,\n          Amit Kapila, Heikki Linnakangas)</p>"
  ],
  [
    "<p>Fix order of arguments in <span class=\"application\">ecpg</span>-generated <code class=\"literal\">typedef</code> statements (Michael Meskes)</p>"
  ],
  [
    "<p>Use <code class=\"literal\">%g</code> not <code class=\"literal\">%f</code> format in <span class=\"application\">ecpg</span>'s <code class=\"function\">PGTYPESnumeric_from_double()</code> (Tom\n          Lane)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">ecpg</span>-supplied\n          header files to not contain comments continued from a\n          preprocessor directive line onto the next line (Michael\n          Meskes)</p>",
    "<p>Such a comment is rejected by <span class=\"application\">ecpg</span>. It's not yet clear whether\n          <span class=\"application\">ecpg</span> itself should be\n          changed.</p>"
  ],
  [
    "<p>Fix <code class=\"function\">hstore_to_json_loose()</code>'s test for\n          whether an <code class=\"type\">hstore</code> value can be\n          converted to a JSON number (Tom Lane)</p>",
    "<p>Previously this function could be fooled by\n          non-alphanumeric trailing characters, leading to emitting\n          syntactically-invalid JSON.</p>"
  ],
  [
    "<p>Ensure that <code class=\"filename\">contrib/pgcrypto</code>'s <code class=\"function\">crypt()</code> function can be interrupted by\n          query cancel (Andreas Karlsson)</p>"
  ],
  [
    "<p>Accept <span class=\"application\">flex</span> versions\n          later than 2.5.x (Tom Lane, Michael Paquier)</p>",
    "<p>Now that flex 2.6.0 has been released, the version\n          checks in our build scripts needed to be adjusted.</p>"
  ],
  [
    "<p>Improve reproducibility of build output by ensuring\n          filenames are given to the linker in a fixed order\n          (Christoph Berg)</p>",
    "<p>This avoids possible bitwise differences in the\n          produced executable files from one build to the next.</p>"
  ],
  [
    "<p>Install our <code class=\"filename\">missing</code>\n          script where PGXS builds can find it (Jim Nasby)</p>",
    "<p>This allows sane behavior in a PGXS build done on a\n          machine where build tools such as <span class=\"application\">bison</span> are missing.</p>"
  ],
  [
    "<p>Ensure that <code class=\"filename\">dynloader.h</code>\n          is included in the installed header files in MSVC builds\n          (Bruce Momjian, Michael Paquier)</p>"
  ],
  [
    "<p>Add variant regression test expected-output file to\n          match behavior of current <span class=\"application\">libxml2</span> (Tom Lane)</p>",
    "<p>The fix for <span class=\"application\">libxml2</span>'s\n          CVE-2015-7499 causes it not to output error context\n          reports in some cases where it used to do so. This seems\n          to be a bug, but we'll probably have to live with it for\n          some time, so work around it.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2016a for DST law\n          changes in Cayman Islands, Metlakatla, and Trans-Baikal\n          Territory (Zabaykalsky Krai), plus historical corrections\n          for Pakistan.</p>"
  ]
]