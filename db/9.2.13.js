[
  [
    "<p>Fix rare failure to invalidate relation cache init\n          file (Tom Lane)</p>",
    "<p>With just the wrong timing of concurrent activity, a\n          <code class=\"command\">VACUUM FULL</code> on a system\n          catalog might fail to update the <span class=\"quote\">&#x201C;<span class=\"quote\">init file</span>&#x201D;</span>\n          that's used to avoid cache-loading work for new sessions.\n          This would result in later sessions being unable to\n          access that catalog at all. This is a very ancient bug,\n          but it's so hard to trigger that no reproducible case had\n          been seen until recently.</p>"
  ],
  [
    "<p>Avoid deadlock between incoming sessions and\n          <code class=\"literal\">CREATE/DROP DATABASE</code> (Tom\n          Lane)</p>",
    "<p>A new session starting in a database that is the\n          target of a <code class=\"command\">DROP DATABASE</code>\n          command, or is the template for a <code class=\"command\">CREATE DATABASE</code> command, could cause the\n          command to wait for five seconds and then fail, even if\n          the new session would have exited before that.</p>"
  ]
]