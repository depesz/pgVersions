[
  [
    "<p>Require schema qualification to cast to a temporary type when using functional cast syntax (Noah Misch)</p>",
    "<p>We have long required invocations of temporary functions to explicitly specify the temporary schema, that is <code class=\"literal\">pg_temp.<em class=\"replaceable\"><code>func_name</code></em>(<em class=\"replaceable\"><code>args</code></em>)</code>. Require this as well for casting to temporary types using functional notation, for example <code class=\"literal\">pg_temp.<em class=\"replaceable\"><code>type_name</code></em>(<em class=\"replaceable\"><code>arg</code></em>)</code>. Otherwise it's possible to capture a function call using a temporary object, allowing privilege escalation in much the same ways that we blocked in CVE-2007-2138. (CVE-2019-10208)</p>"
  ],
  [
    "<p>Fix failure of <code class=\"command\">ALTER TABLE ... ALTER COLUMN TYPE</code> when altering multiple columns' types in one command (Tom Lane)</p>",
    "<p>This fixes a regression introduced in the most recent minor releases: indexes using the altered columns were not processed correctly, leading to strange failures during <code class=\"command\">ALTER TABLE</code>.</p>"
  ],
  [
    "<p>Install dependencies to prevent dropping partition key columns (Tom Lane)</p>",
    "<p><code class=\"command\">ALTER TABLE ... DROP COLUMN</code> will refuse to drop a column that is a partition key column. However, indirect drops (such as a cascade from dropping a key column's data type) had no such check, allowing the deletion of a key column. This resulted in a badly broken partitioned table that could neither be accessed nor dropped.</p>",
    "<p>This fix adds <code class=\"structname\">pg_depend</code> entries that enforce that the whole partitioned table, not just the key column, will be dropped if a cascaded drop forces removal of the key column. However, such entries will only be created when a partitioned table is created; so this fix does not remove the risk for pre-existing partitioned tables. The issue can only arise for partition key columns of non-built-in data types, so it seems not to be a hazard for most users.</p>"
  ],
  [
    "<p>Don't optimize away <code class=\"literal\">GROUP BY</code> columns when the table involved is an inheritance parent (David Rowley)</p>",
    "<p>Normally, if a table's primary key column(s) are included in <code class=\"literal\">GROUP BY</code>, it's safe to drop any other grouping columns, since the primary key columns are enough to make the groups unique. This rule does not work if the query is also reading inheritance child tables, though; the parent's uniqueness does not extend to the children.</p>"
  ],
  [
    "<p>Avoid using unnecessary sort steps for some queries with <code class=\"literal\">GROUPING SETS</code> (Andrew Gierth, Richard Guo)</p>"
  ],
  [
    "<p>Fix failure to access trigger transition tables during <code class=\"literal\">EvalPlanQual</code> rechecks (Alex Aktsipetrov)</p>",
    "<p>Triggers that rely on transition tables sometimes failed in the presence of concurrent updates.</p>"
  ],
  [
    "<p>Fix mishandling of multi-column foreign keys when rebuilding a foreign key constraint (Tom Lane)</p>",
    "<p><code class=\"command\">ALTER TABLE</code> could make an incorrect decision about whether revalidation of a foreign key is necessary, if not all columns of the key are of the same type. It seems likely that the error would always have been in the conservative direction, that is revalidating unnecessarily.</p>"
  ],
  [
    "<p>Don't build extended statistics for inheritance trees (Tomas Vondra)</p>",
    "<p>This avoids a <span class=\"quote\">“<span class=\"quote\">tuple already updated by self</span>”</span> error during <code class=\"command\">ANALYZE</code>.</p>"
  ],
  [
    "<p>Avoid spurious deadlock errors when upgrading a tuple lock (Oleksii Kliukin)</p>",
    "<p>When two or more transactions are waiting for a transaction T1 to release a tuple-level lock, and T1 upgrades its lock to a higher level, a spurious deadlock among the waiting transactions could be reported when T1 finishes.</p>"
  ],
  [
    "<p>Fix failure to resolve deadlocks involving multiple parallel worker processes (Rui Hai Jiang)</p>",
    "<p>It is not clear whether this bug is reachable with non-artificial queries, but if it did happen, the queries involved in an otherwise-resolvable deadlock would block until canceled.</p>"
  ],
  [
    "<p>Prevent incorrect canonicalization of date ranges with <code class=\"literal\">infinity</code> endpoints (Laurenz Albe)</p>",
    "<p>It's incorrect to try to convert an open range to a closed one or vice versa by incrementing or decrementing the endpoint value, if the endpoint is infinite; so leave the range alone in such cases.</p>"
  ],
  [
    "<p>Fix loss of fractional digits when converting very large <code class=\"type\">money</code> values to <code class=\"type\">numeric</code> (Tom Lane)</p>"
  ],
  [
    "<p>Fix spinlock assembly code for MIPS CPUs so that it works on MIPS r6 (YunQiang Su)</p>"
  ],
  [
    "<p>Make <span class=\"application\">libpq</span> ignore carriage return (<code class=\"literal\">\\r</code>) in connection service files (Tom Lane, Michael Paquier)</p>",
    "<p>In some corner cases, service files containing Windows-style newlines could be mis-parsed, resulting in connection failures.</p>"
  ],
  [
    "<p>In <span class=\"application\">psql</span>, avoid offering incorrect tab completion options after <code class=\"literal\">SET <em class=\"replaceable\"><code>variable</code></em> =</code> (Tom Lane)</p>"
  ],
  [
    "<p>Fix a small memory leak in <span class=\"application\">psql</span>'s <code class=\"literal\">\\d</code> command (Tom Lane)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_dump</span> to ensure that custom operator classes are dumped in the right order (Tom Lane)</p>",
    "<p>If a user-defined opclass is the subtype opclass of a user-defined range type, related objects were dumped in the wrong order, producing an unrestorable dump. (The underlying failure to handle opclass dependencies might manifest in other cases too, but this is the only known case.)</p>"
  ],
  [
    "<p>Fix possible lockup in <span class=\"application\">pgbench</span> when using <code class=\"option\">-R</code> option (Fabien Coelho)</p>"
  ],
  [
    "<p>Fix <code class=\"filename\">contrib/passwordcheck</code> to coexist with other users of <code class=\"varname\">check_password_hook</code> (Michael Paquier)</p>"
  ],
  [
    "<p>Fix <code class=\"filename\">contrib/sepgsql</code> tests to work under recent SELinux releases (Mike Palmiotto)</p>"
  ],
  [
    "<p>Improve stability of <code class=\"filename\">src/test/recovery</code> regression tests (Michael Paquier)</p>"
  ],
  [
    "<p>Reduce <span class=\"systemitem\">stderr</span> output from <span class=\"application\">pg_upgrade</span>'s test script (Tom Lane)</p>"
  ],
  [
    "<p>Fix TAP tests to work with msys Perl, in cases where the build directory is on a non-root msys mount point (Noah Misch)</p>"
  ],
  [
    "<p>Support building Postgres with Microsoft Visual Studio 2019 (Haribabu Kommi)</p>"
  ],
  [
    "<p>In Visual Studio builds, honor <code class=\"literal\">WindowsSDKVersion</code> environment variable, if that's set (Peifeng Qiu)</p>",
    "<p>This fixes build failures in some configurations.</p>"
  ],
  [
    "<p>Support OpenSSL 1.1.0 and newer in Visual Studio builds (Juan José Santamaría Flecha, Michael Paquier)</p>"
  ],
  [
    "<p>Allow <span class=\"application\">make</span> options to be passed down to <span class=\"application\">gmake</span> when non-GNU make is invoked at the top level (Thomas Munro)</p>"
  ],
  [
    "<p>Avoid choosing <code class=\"literal\">localtime</code> or <code class=\"literal\">posixrules</code> as <code class=\"varname\">TimeZone</code> during <span class=\"application\">initdb</span> (Tom Lane)</p>",
    "<p>In some cases <span class=\"application\">initdb</span> would choose one of these artificial zone names over the <span class=\"quote\">“<span class=\"quote\">real</span>”</span> zone name. Prefer any other match to the C library's timezone behavior over these two.</p>"
  ],
  [
    "<p>Adjust <code class=\"structname\">pg_timezone_names</code> view to show the <code class=\"literal\">Factory</code> time zone if and only if it has a short abbreviation (Tom Lane)</p>",
    "<p>Historically, IANA set up this artificial zone with an <span class=\"quote\">“<span class=\"quote\">abbreviation</span>”</span> like <code class=\"literal\">Local time zone must be set--see zic manual page</code>. Modern versions of the tzdb database show <code class=\"literal\">-00</code> instead, but some platforms alter the data to show one or another of the historical phrases. Show this zone only if it uses the modern abbreviation.</p>"
  ],
  [
    "<p>Sync our copy of the timezone library with IANA tzcode release 2019b (Tom Lane)</p>",
    "<p>This adds support for <span class=\"application\">zic</span>'s new <code class=\"option\">-b slim</code> option to reduce the size of the installed zone files. We are not currently using that, but may enable it in future.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2019b for DST law changes in Brazil, plus historical corrections for Hong Kong, Italy, and Palestine.</p>"
  ]
]