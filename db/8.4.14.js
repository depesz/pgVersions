[
  [
    "<p>Fix planner's assignment of executor parameters, and\n          fix executor's rescan logic for CTE plan nodes (Tom\n          Lane)</p>",
    "<p>These errors could result in wrong answers from\n          queries that scan the same <code class=\"literal\">WITH</code> subquery multiple times.</p>"
  ],
  [
    "<p>Improve page-splitting decisions in GiST indexes\n          (Alexander Korotkov, Robert Haas, Tom Lane)</p>",
    "<p>Multi-column GiST indexes might suffer unexpected\n          bloat due to this error.</p>"
  ],
  [
    "<p>Fix cascading privilege revoke to stop if privileges\n          are still held (Tom Lane)</p>",
    "<p>If we revoke a grant option from some role <em class=\"replaceable\"><code>X</code></em>, but <em class=\"replaceable\"><code>X</code></em> still holds that option\n          via a grant from someone else, we should not recursively\n          revoke the corresponding privilege from role(s)\n          <em class=\"replaceable\"><code>Y</code></em> that\n          <em class=\"replaceable\"><code>X</code></em> had granted\n          it to.</p>"
  ],
  [
    "<p>Fix handling of <code class=\"literal\">SIGFPE</code>\n          when PL/Perl is in use (Andres Freund)</p>",
    "<p>Perl resets the process's <code class=\"literal\">SIGFPE</code> handler to <code class=\"literal\">SIG_IGN</code>, which could result in crashes\n          later on. Restore the normal Postgres signal handler\n          after initializing PL/Perl.</p>"
  ],
  [
    "<p>Prevent PL/Perl from crashing if a recursive PL/Perl\n          function is redefined while being executed (Tom Lane)</p>"
  ],
  [
    "<p>Work around possible misoptimization in PL/Perl (Tom\n          Lane)</p>",
    "<p>Some Linux distributions contain an incorrect version\n          of <code class=\"filename\">pthread.h</code> that results\n          in incorrect compiled code in PL/Perl, leading to crashes\n          if a PL/Perl function calls another one that throws an\n          error.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2012f for DST law\n          changes in Fiji</p>"
  ]
]