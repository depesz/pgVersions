[
  [
    "<p>Improve behavior of <span class=\"application\">libpq</span>'s quoting functions (Andres Freund, Tom Lane) <a class=\"ulink\" href=\"https://postgr.es/c/a92db3d02\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/3abe6e04c\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/3977bd298\" target=\"_top\">§</a></p>",
    "<p>The changes made for CVE-2025-1094 had one serious oversight: <code class=\"function\">PQescapeLiteral()</code> and <code class=\"function\">PQescapeIdentifier()</code> failed to honor their string length parameter, instead always reading to the input string's trailing null. This resulted in including unwanted text in the output, if the caller intended to truncate the string via the length parameter. With very bad luck it could cause a crash due to reading off the end of memory.</p>",
    "<p>In addition, modify all these quoting functions so that when invalid encoding is detected, an invalid sequence is substituted for just the first byte of the presumed character, not all of it. This reduces the risk of problems if a calling application performs additional processing on the quoted string.</p>"
  ],
  [
    "<p>Fix small memory leak in <span class=\"application\">pg_createsubscriber</span> (Ranier Vilela) <a class=\"ulink\" href=\"https://postgr.es/c/ff6d9cfcb\" target=\"_top\">§</a></p>"
  ],
  [
    "<p>Fix meson build system to correctly detect availability of the <code class=\"filename\">bsd_auth.h</code> system header (Nazir Bilal Yavuz) <a class=\"ulink\" href=\"https://postgr.es/c/c9a1d2135\" target=\"_top\">§</a></p>"
  ]
]