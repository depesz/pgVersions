[
  [
    "<p>Fix incorrect password transformation in <code class=\"filename\">contrib/pgcrypto</code>'s DES <code class=\"function\">crypt()</code> function (Solar Designer)</p>",
    "<p>If a password string contained the byte value\n          <code class=\"literal\">0x80</code>, the remainder of the\n          password was ignored, causing the password to be much\n          weaker than it appeared. With this fix, the rest of the\n          string is properly included in the DES hash. Any stored\n          password values that are affected by this bug will thus\n          no longer match, so the stored values may need to be\n          updated. (CVE-2012-2143)</p>"
  ],
  [
    "<p>Ignore <code class=\"literal\">SECURITY DEFINER</code>\n          and <code class=\"literal\">SET</code> attributes for a\n          procedural language's call handler (Tom Lane)</p>",
    "<p>Applying such attributes to a call handler could crash\n          the server. (CVE-2012-2655)</p>"
  ],
  [
    "<p>Allow numeric timezone offsets in <code class=\"type\">timestamp</code> input to be up to 16 hours away\n          from UTC (Tom Lane)</p>",
    "<p>Some historical time zones have offsets larger than 15\n          hours, the previous limit. This could result in dumped\n          data values being rejected during reload.</p>"
  ],
  [
    "<p>Fix timestamp conversion to cope when the given time\n          is exactly the last DST transition time for the current\n          timezone (Tom Lane)</p>",
    "<p>This oversight has been there a long time, but was not\n          noticed previously because most DST-using zones are\n          presumed to have an indefinite sequence of future DST\n          transitions.</p>"
  ],
  [
    "<p>Fix <code class=\"type\">text</code> to <code class=\"type\">name</code> and <code class=\"type\">char</code> to\n          <code class=\"type\">name</code> casts to perform string\n          truncation correctly in multibyte encodings (Karl\n          Schnaitter)</p>"
  ],
  [
    "<p>Fix memory copying bug in <code class=\"function\">to_tsquery()</code> (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Fix slow session startup when <code class=\"structname\">pg_attribute</code> is very large (Tom\n          Lane)</p>",
    "<p>If <code class=\"structname\">pg_attribute</code>\n          exceeds one-fourth of <code class=\"varname\">shared_buffers</code>, cache rebuilding code\n          that is sometimes needed during session start would\n          trigger the synchronized-scan logic, causing it to take\n          many times longer than normal. The problem was\n          particularly acute if many new sessions were starting at\n          once.</p>"
  ],
  [
    "<p>Ensure sequential scans check for query cancel\n          reasonably often (Merlin Moncure)</p>",
    "<p>A scan encountering many consecutive pages that\n          contain no live tuples would not respond to interrupts\n          meanwhile.</p>"
  ],
  [
    "<p>Ensure the Windows implementation of <code class=\"function\">PGSemaphoreLock()</code> clears <code class=\"varname\">ImmediateInterruptOK</code> before returning\n          (Tom Lane)</p>",
    "<p>This oversight meant that a query-cancel interrupt\n          received later in the same query could be accepted at an\n          unsafe time, with unpredictable but not good\n          consequences.</p>"
  ],
  [
    "<p>Show whole-row variables safely when printing views or\n          rules (Abbas Butt, Tom Lane)</p>",
    "<p>Corner cases involving ambiguous names (that is, the\n          name could be either a table or column name of the query)\n          were printed in an ambiguous way, risking that the view\n          or rule would be interpreted differently after dump and\n          reload. Avoid the ambiguous case by attaching a no-op\n          cast.</p>"
  ],
  [
    "<p>Ensure autovacuum worker processes perform stack depth\n          checking properly (Heikki Linnakangas)</p>",
    "<p>Previously, infinite recursion in a function invoked\n          by auto-<code class=\"command\">ANALYZE</code> could crash\n          worker processes.</p>"
  ],
  [
    "<p>Fix logging collector to not lose log coherency under\n          high load (Andrew Dunstan)</p>",
    "<p>The collector previously could fail to reassemble\n          large messages if it got too busy.</p>"
  ],
  [
    "<p>Fix logging collector to ensure it will restart file\n          rotation after receiving <span class=\"systemitem\">SIGHUP</span> (Tom Lane)</p>"
  ],
  [
    "<p>Fix PL/pgSQL's <code class=\"command\">GET\n          DIAGNOSTICS</code> command when the target is the\n          function's first variable (Tom Lane)</p>"
  ],
  [
    "<p>Fix several performance problems in <span class=\"application\">pg_dump</span> when the database contains\n          many objects (Jeff Janes, Tom Lane)</p>",
    "<p><span class=\"application\">pg_dump</span> could get\n          very slow if the database contained many schemas, or if\n          many objects are in dependency loops, or if there are\n          many owned sequences.</p>"
  ],
  [
    "<p>Fix <code class=\"filename\">contrib/dblink</code>'s\n          <code class=\"function\">dblink_exec()</code> to not leak\n          temporary database connections upon error (Tom Lane)</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2012c for DST law\n          changes in Antarctica, Armenia, Chile, Cuba, Falkland\n          Islands, Gaza, Haiti, Hebron, Morocco, Syria, and Tokelau\n          Islands; also historical corrections for Canada.</p>"
  ]
]