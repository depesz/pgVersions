[
  [
    "<p>Prevent index corruption when a transaction inserts\n          rows and then aborts close to the end of a concurrent\n          <code class=\"command\">VACUUM</code> on the same table\n          (Tom)</p>"
  ],
  [
    "<p>Make <code class=\"command\">CREATE DOMAIN ... DEFAULT\n          NULL</code> work properly (Tom)</p>"
  ],
  [
    "<p>Fix crash when <code class=\"varname\">log_min_error_statement</code> logging runs out\n          of memory (Tom)</p>"
  ],
  [
    "<p>Require non-superusers who use <code class=\"filename\">/contrib/dblink</code> to use only password\n          authentication, as a security measure (Joe)</p>"
  ]
]