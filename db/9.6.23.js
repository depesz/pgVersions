[
  [
    "<p>Disallow SSL renegotiation more completely (Michael Paquier)</p>",
    "<p>SSL renegotiation has been disabled for some time, but the server would still cooperate with a client-initiated renegotiation request. A maliciously crafted renegotiation request could result in a server crash (see OpenSSL issue CVE-2021-3449). Disable the feature altogether on OpenSSL versions that permit doing so, which are 1.1.0h and newer.</p>"
  ],
  [
    "<p>Reject <tt class=\"LITERAL\">SELECT ... GROUP BY GROUPING SETS (()) FOR UPDATE</tt> (Tom Lane)</p>",
    "<p>This should be disallowed, just as <tt class=\"LITERAL\">FOR UPDATE</tt> with a plain <tt class=\"LITERAL\">GROUP BY</tt> is disallowed, but the test for that failed to handle empty grouping sets correctly. The end result would be a null-pointer dereference in the executor.</p>"
  ],
  [
    "<p>Reject cases where a query in <tt class=\"LITERAL\">WITH</tt> rewrites to just <tt class=\"COMMAND\">NOTIFY</tt> (Tom Lane)</p>",
    "<p>Such cases previously crashed.</p>"
  ],
  [
    "<p>In <tt class=\"TYPE\">numeric</tt> multiplication, round the result rather than failing if it would have more than 16383 digits after the decimal point (Dean Rasheed)</p>"
  ],
  [
    "<p>Fix corner-case errors and loss of precision when raising <tt class=\"TYPE\">numeric</tt> values to very large powers (Dean Rasheed)</p>"
  ],
  [
    "<p>Fix division-by-zero failure in <code class=\"FUNCTION\">to_char()</code> with <tt class=\"LITERAL\">EEEE</tt> format and a <tt class=\"TYPE\">numeric</tt> input value less than 10^(-1001) (Dean Rasheed)</p>"
  ],
  [
    "<p>Fix <code class=\"FUNCTION\">pg_size_pretty(bigint)</code> to round negative values consistently with the way it rounds positive ones (and consistently with the <tt class=\"TYPE\">numeric</tt> version) (Dean Rasheed, David Rowley)</p>"
  ],
  [
    "<p>Make <tt class=\"LITERAL\">pg_filenode_relation(0, 0)</tt> return NULL rather than failing (Justin Pryzby)</p>"
  ],
  [
    "<p>Make <tt class=\"COMMAND\">ALTER EXTENSION</tt> lock the extension when adding or removing a member object (Tom Lane)</p>",
    "<p>The previous coding allowed <tt class=\"COMMAND\">ALTER EXTENSION ADD/DROP</tt> to occur concurrently with <tt class=\"COMMAND\">DROP EXTENSION</tt>, leading to a crash or corrupt catalog entries.</p>"
  ],
  [
    "<p>Avoid alias conflicts in queries generated for <tt class=\"COMMAND\">REFRESH MATERIALIZED VIEW CONCURRENTLY</tt> (Tom Lane, Bharath Rupireddy)</p>",
    "<p>This command failed on materialized views containing columns with certain names, notably <tt class=\"STRUCTFIELD\">mv</tt> and <tt class=\"STRUCTFIELD\">newdata</tt>.</p>"
  ],
  [
    "<p>Fix <tt class=\"COMMAND\">PREPARE TRANSACTION</tt> to check correctly for conflicting session-lifespan and transaction-lifespan locks (Tom Lane)</p>",
    "<p>A transaction cannot be prepared if it has both session-lifespan and transaction-lifespan locks on the same advisory-lock ID value. This restriction was not fully checked, which could lead to a PANIC during <tt class=\"COMMAND\">PREPARE TRANSACTION</tt>.</p>"
  ],
  [
    "<p>Fix misbehavior of <tt class=\"COMMAND\">DROP OWNED BY</tt> when the target role is listed more than once in an RLS policy (Tom Lane)</p>"
  ],
  [
    "<p>Skip unnecessary error tests when removing a role from an RLS policy during <tt class=\"COMMAND\">DROP OWNED BY</tt> (Tom Lane)</p>",
    "<p>Notably, this fixes some cases where it was necessary to be a superuser to use <tt class=\"COMMAND\">DROP OWNED BY</tt>.</p>"
  ],
  [
    "<p>Allow index state flags to be updated transactionally (Michael Paquier, Andrey Lepikhov)</p>",
    "<p>This avoids failures when dealing with index predicates that aren't really immutable. While that's not considered a supported case, the original reason for using a non-transactional update here is long gone, so we may as well change it.</p>"
  ],
  [
    "<p>Avoid corrupting the plan cache entry when <tt class=\"COMMAND\">CREATE DOMAIN</tt> or <tt class=\"COMMAND\">ALTER DOMAIN</tt> appears in a cached plan (Tom Lane)</p>"
  ],
  [
    "<p>Make <tt class=\"STRUCTNAME\">pg_settings</tt>.<tt class=\"STRUCTFIELD\">pending_restart</tt> show as true when the pertinent entry in <tt class=\"FILENAME\">postgresql.conf</tt> has been removed (Álvaro Herrera)</p>",
    "<p><tt class=\"STRUCTFIELD\">pending_restart</tt> correctly showed the case where an entry that cannot be changed without a postmaster restart has been modified, but not where the entry had been removed altogether.</p>"
  ],
  [
    "<p>Fix corner-case failure of a new standby to follow a new primary (Dilip Kumar, Robert Haas)</p>",
    "<p>Under a narrow combination of conditions, the standby could wind up trying to follow the wrong WAL timeline.</p>"
  ],
  [
    "<p>Update minimum recovery point when WAL replay of a transaction abort record causes file truncation (Fujii Masao)</p>",
    "<p>File truncation is irreversible, so it's no longer safe to stop recovery at a point earlier than that record. The corresponding case for transaction commit was fixed years ago, but this one was overlooked.</p>"
  ],
  [
    "<p>Ensure that a standby server's startup process will respond to a shutdown signal promptly while waiting for WAL to arrive (Fujii Masao, Soumyadeep Chakraborty)</p>"
  ],
  [
    "<p>Add locking to avoid reading incorrect relmapper data in the face of a concurrent write from another process (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Fix error cases and memory leaks in logical decoding of speculative insertions (Dilip Kumar)</p>"
  ],
  [
    "<p>Fix plan cache reference leaks in some error cases in <tt class=\"COMMAND\">CREATE TABLE ... AS EXECUTE</tt> (Tom Lane)</p>"
  ],
  [
    "<p>Fix possible race condition when releasing BackgroundWorkerSlots (Tom Lane)</p>",
    "<p>It's likely that this doesn't fix any observable bug on Intel hardware, but machines with weaker memory ordering rules could have problems.</p>"
  ],
  [
    "<p>Fix latent crash in sorting code (Ronan Dunklau)</p>",
    "<p>One code path could attempt to free a null pointer. The case appears unreachable in the core server's use of sorting, but perhaps it could be triggered by extensions.</p>"
  ],
  [
    "<p>Prevent infinite loops in SP-GiST index insertion (Tom Lane)</p>",
    "<p>In the event that INCLUDE columns take up enough space to prevent a leaf index tuple from ever fitting on a page, the text_ops operator class would get into an infinite loop vainly trying to make the tuple fit. While pre-v11 versions don't have INCLUDE columns, back-patch this anti-looping fix to them anyway, as it seems like a good defense against bugs in operator classes.</p>"
  ],
  [
    "<p>Ensure that SP-GiST index insertion can be terminated by a query cancel request (Tom Lane, Álvaro Herrera)</p>"
  ],
  [
    "<p>Fix uninitialized-variable bug that could cause <span class=\"APPLICATION\">PL/pgSQL</span> to act as though an <tt class=\"LITERAL\">INTO</tt> clause specified <tt class=\"LITERAL\">STRICT</tt>, even though it didn't (Tom Lane)</p>"
  ],
  [
    "<p>Don't abort the process for an out-of-memory failure in libpq's printing functions (Tom Lane)</p>"
  ],
  [
    "<p>In <span class=\"APPLICATION\">ecpg</span>, allow the <tt class=\"TYPE\">numeric</tt> value <span class=\"SYSTEMITEM\">INT_MIN</span> (usually -2147483648) to be converted to integer (John Naylor)</p>"
  ],
  [
    "<p>In <span class=\"APPLICATION\">psql</span> and other client programs, avoid overrunning the ends of strings when dealing with invalidly-encoded data (Tom Lane)</p>",
    "<p>An incorrectly-encoded multibyte character near the end of a string could cause various processing loops to run past the string's terminating NUL, with results ranging from no detectable issue to a program crash, depending on what happens to be in the following memory. This is reminiscent of CVE-2006-2313, although these particular cases do not appear to have interesting security consequences.</p>"
  ],
  [
    "<p>Avoid <span class=\"QUOTE\">\"invalid creation date in header\"</span> warnings observed when running <span class=\"APPLICATION\">pg_restore</span> on an archive file created in a different time zone (Tom Lane)</p>"
  ],
  [
    "<p>Make <span class=\"APPLICATION\">pg_upgrade</span> carry forward the old installation's <tt class=\"LITERAL\">oldestXID</tt> value (Bertrand Drouvot)</p>",
    "<p>Previously, the new installation's <tt class=\"LITERAL\">oldestXID</tt> was set to a value old enough to (usually) force immediate anti-wraparound autovacuuming. That's not desirable from a performance standpoint; what's worse, installations using large values of <tt class=\"VARNAME\">autovacuum_freeze_max_age</tt> could suffer unwanted forced shutdowns soon after an upgrade.</p>"
  ],
  [
    "<p>Extend <span class=\"APPLICATION\">pg_upgrade</span> to detect and warn about extensions that should be upgraded (Bruce Momjian)</p>",
    "<p>A script file is now produced containing the <tt class=\"COMMAND\">ALTER EXTENSION UPDATE</tt> commands needed to bring extensions up to the versions that are considered default in the new installation.</p>"
  ],
  [
    "<p>In <tt class=\"FILENAME\">contrib/postgres_fdw</tt>, avoid attempting catalog lookups after an error (Tom Lane)</p>",
    "<p>While this usually worked, it's not very safe since the error might have been one that made catalog access nonfunctional. A side effect of the fix is that messages about data conversion errors will now mention the query's table and column aliases (if used) rather than the true underlying name of a foreign table or column.</p>"
  ],
  [
    "<p>In <tt class=\"FILENAME\">contrib/pgcrypto</tt>, avoid symbol name conflicts with OpenSSL (Tom Lane)</p>",
    "<p>Operations using SHA224 hashing could show failures under valgrind checking. It appears that this is only a stomp of alignment-padding bytes and so has no real consequences, but let's fix it to be sure.</p>"
  ],
  [
    "<p>Improve the isolation-test infrastructure (Tom Lane, Michael Paquier)</p>",
    "<p>Allow isolation test steps to be annotated to show the expected completion order. This allows getting stable results from otherwise-racy test cases, without the long delays that we previously used (not entirely successfully) to fend off race conditions. Allow non-quoted identifiers as isolation test session/step names (formerly, all such names had to be double-quoted). Detect and warn about unused steps in isolation tests. Improve display of query results in isolation tests. Remove isolationtester's <span class=\"QUOTE\">\"dry-run\"</span> mode. Remove memory leaks in isolationtester itself.</p>"
  ],
  [
    "<p>Reduce overhead of cache-clobber testing (Tom Lane)</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">PL/Python</span>'s regression tests to pass with Python 3.10 (Honza Horak)</p>"
  ],
  [
    "<p>Make <tt class=\"LITERAL\">printf(\"%s\", NULL)</tt> print <tt class=\"LITERAL\">(null)</tt> instead of crashing (Tom Lane)</p>",
    "<p>This should improve server robustness in corner cases, and it syncs our <code class=\"FUNCTION\">printf</code> implementation with common libraries.</p>"
  ],
  [
    "<p>Fix incorrect log message when point-in-time recovery stops at a <tt class=\"COMMAND\">ROLLBACK PREPARED</tt> record (Simon Riggs)</p>"
  ],
  [
    "<p>Clarify error messages referring to <span class=\"QUOTE\">\"non-negative\"</span> values (Bharath Rupireddy)</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">configure</span> to work with OpenLDAP 2.5, which no longer has a separate <tt class=\"FILENAME\">libldap_r</tt> library (Adrian Ho, Tom Lane)</p>",
    "<p>If there is no <tt class=\"FILENAME\">libldap_r</tt> library, we now silently assume that <tt class=\"FILENAME\">libldap</tt> is thread-safe.</p>"
  ],
  [
    "<p>Add new make targets <tt class=\"LITERAL\">world-bin</tt> and <tt class=\"LITERAL\">install-world-bin</tt> (Andrew Dunstan)</p>",
    "<p>These are the same as <tt class=\"LITERAL\">world</tt> and <tt class=\"LITERAL\">install-world</tt> respectively, except that they do not build or install the documentation.</p>"
  ],
  [
    "<p>Fix make rule for TAP tests (<tt class=\"LITERAL\">prove_installcheck</tt>) to work in PGXS usage (Andrew Dunstan)</p>"
  ],
  [
    "<p>Avoid assuming that strings returned by GSSAPI libraries are null-terminated (Tom Lane)</p>",
    "<p>The GSSAPI spec provides for a string pointer and length. It seems that in practice the next byte after the string is usually zero, so that our previous coding didn't actually fail; but we do have a report of AddressSanitizer complaints.</p>"
  ],
  [
    "<p>Enable building with GSSAPI on MSVC (Michael Paquier)</p>",
    "<p>Fix various incompatibilities with modern Kerberos builds.</p>"
  ],
  [
    "<p>In MSVC builds, include <tt class=\"OPTION\">--with-pgport</tt> in the set of configure options reported by <span class=\"APPLICATION\">pg_config</span>, if it had been specified (Andrew Dunstan)</p>"
  ]
]