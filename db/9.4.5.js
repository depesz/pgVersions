[
  [
    "<p>Guard against stack overflows in <tt class=\"TYPE\">json</tt> parsing (Oskari Saarenmaa)</p>",
    "<p>If an application constructs PostgreSQL <tt class=\"TYPE\">json</tt> or <tt class=\"TYPE\">jsonb</tt> values from arbitrary user input, the application's users can reliably crash the PostgreSQL server, causing momentary denial of service. (CVE-2015-5289)</p>"
  ],
  [
    "<p>Fix <tt class=\"FILENAME\">contrib/pgcrypto</tt> to detect and report too-short <code class=\"FUNCTION\">crypt()</code> salts (Josh Kupershmidt)</p>",
    "<p>Certain invalid salt arguments crashed the server or disclosed a few bytes of server memory. We have not ruled out the viability of attacks that arrange for presence of confidential information in the disclosed bytes, but they seem unlikely. (CVE-2015-5288)</p>"
  ],
  [
    "<p>Fix subtransaction cleanup after a portal (cursor) belonging to an outer subtransaction fails (Tom Lane, Michael Paquier)</p>",
    "<p>A function executed in an outer-subtransaction cursor could cause an assertion failure or crash by referencing a relation created within an inner subtransaction.</p>"
  ],
  [
    "<p>Fix possible deadlock during WAL insertion when <tt class=\"VARNAME\">commit_delay</tt> is set (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Ensure all relations referred to by an updatable view are properly locked during an update statement (Dean Rasheed)</p>"
  ],
  [
    "<p>Fix insertion of relations into the relation cache <span class=\"QUOTE\">\"init file\"</span> (Tom Lane)</p>",
    "<p>An oversight in a patch in the most recent minor releases caused <tt class=\"STRUCTNAME\">pg_trigger_tgrelid_tgname_index</tt> to be omitted from the init file. Subsequent sessions detected this, then deemed the init file to be broken and silently ignored it, resulting in a significant degradation in session startup time. In addition to fixing the bug, install some guards so that any similar future mistake will be more obvious.</p>"
  ],
  [
    "<p>Avoid O(N^2) behavior when inserting many tuples into a SPI query result (Neil Conway)</p>"
  ],
  [
    "<p>Improve <tt class=\"COMMAND\">LISTEN</tt> startup time when there are many unread notifications (Matt Newell)</p>"
  ],
  [
    "<p>Fix performance problem when a session alters large numbers of foreign key constraints (Jan Wieck, Tom Lane)</p>",
    "<p>This was seen primarily when restoring <span class=\"APPLICATION\">pg_dump</span> output for databases with many thousands of tables.</p>"
  ],
  [
    "<p>Disable SSL renegotiation by default (Michael Paquier, Andres Freund)</p>",
    "<p>While use of SSL renegotiation is a good idea in theory, we have seen too many bugs in practice, both in the underlying OpenSSL library and in our usage of it. Renegotiation will be removed entirely in 9.5 and later. In the older branches, just change the default value of <tt class=\"VARNAME\">ssl_renegotiation_limit</tt> to zero (disabled).</p>"
  ],
  [
    "<p>Lower the minimum values of the <tt class=\"LITERAL\">*_freeze_max_age</tt> parameters (Andres Freund)</p>",
    "<p>This is mainly to make tests of related behavior less time-consuming, but it may also be of value for installations with limited disk space.</p>"
  ],
  [
    "<p>Limit the maximum value of <tt class=\"VARNAME\">wal_buffers</tt> to 2GB to avoid server crashes (Josh Berkus)</p>"
  ],
  [
    "<p>Avoid logging complaints when a parameter that can only be set at server start appears multiple times in <tt class=\"FILENAME\">postgresql.conf</tt>, and fix counting of line numbers after an <tt class=\"LITERAL\">include_dir</tt> directive (Tom Lane)</p>"
  ],
  [
    "<p>Fix rare internal overflow in multiplication of <tt class=\"TYPE\">numeric</tt> values (Dean Rasheed)</p>"
  ],
  [
    "<p>Guard against hard-to-reach stack overflows involving record types, range types, <tt class=\"TYPE\">json</tt>, <tt class=\"TYPE\">jsonb</tt>, <tt class=\"TYPE\">tsquery</tt>, <tt class=\"TYPE\">ltxtquery</tt> and <tt class=\"TYPE\">query_int</tt> (Noah Misch)</p>"
  ],
  [
    "<p>Fix handling of <tt class=\"LITERAL\">DOW</tt> and <tt class=\"LITERAL\">DOY</tt> in datetime input (Greg Stark)</p>",
    "<p>These tokens aren't meant to be used in datetime values, but previously they resulted in opaque internal error messages rather than <span class=\"QUOTE\">\"invalid input syntax\"</span>.</p>"
  ],
  [
    "<p>Add more query-cancel checks to regular expression matching (Tom Lane)</p>"
  ],
  [
    "<p>Add recursion depth protections to regular expression, <tt class=\"LITERAL\">SIMILAR TO</tt>, and <tt class=\"LITERAL\">LIKE</tt> matching (Tom Lane)</p>",
    "<p>Suitable search patterns and a low stack depth limit could lead to stack-overrun crashes.</p>"
  ],
  [
    "<p>Fix potential infinite loop in regular expression execution (Tom Lane)</p>",
    "<p>A search pattern that can apparently match a zero-length string, but actually doesn't match because of a back reference, could lead to an infinite loop.</p>"
  ],
  [
    "<p>In regular expression execution, correctly record match data for capturing parentheses within a quantifier even when the match is zero-length (Tom Lane)</p>"
  ],
  [
    "<p>Fix low-memory failures in regular expression compilation (Andreas Seltenreich)</p>"
  ],
  [
    "<p>Fix low-probability memory leak during regular expression execution (Tom Lane)</p>"
  ],
  [
    "<p>Fix rare low-memory failure in lock cleanup during transaction abort (Tom Lane)</p>"
  ],
  [
    "<p>Fix <span class=\"QUOTE\">\"unexpected out-of-memory situation during sort\"</span> errors when using tuplestores with small <tt class=\"VARNAME\">work_mem</tt> settings (Tom Lane)</p>"
  ],
  [
    "<p>Fix very-low-probability stack overrun in <code class=\"FUNCTION\">qsort</code> (Tom Lane)</p>"
  ],
  [
    "<p>Fix <span class=\"QUOTE\">\"invalid memory alloc request size\"</span> failure in hash joins with large <tt class=\"VARNAME\">work_mem</tt> settings (Tomas Vondra, Tom Lane)</p>"
  ],
  [
    "<p>Fix assorted planner bugs (Tom Lane)</p>",
    "<p>These mistakes could lead to incorrect query plans that would give wrong answers, or to assertion failures in assert-enabled builds, or to odd planner errors such as <span class=\"QUOTE\">\"could not devise a query plan for the given query\"</span>, <span class=\"QUOTE\">\"could not find pathkey item to sort\"</span>, <span class=\"QUOTE\">\"plan should not reference subplan's variable\"</span>, or <span class=\"QUOTE\">\"failed to assign all NestLoopParams to plan nodes\"</span>. Thanks are due to Andreas Seltenreich and Piotr Stefaniak for fuzz testing that exposed these problems.</p>"
  ],
  [
    "<p>Improve planner's performance for <tt class=\"COMMAND\">UPDATE</tt>/<tt class=\"COMMAND\">DELETE</tt> on large inheritance sets (Tom Lane, Dean Rasheed)</p>"
  ],
  [
    "<p>Ensure standby promotion trigger files are removed at postmaster startup (Michael Paquier, Fujii Masao)</p>",
    "<p>This prevents unwanted promotion from occurring if these files appear in a database backup that is used to initialize a new standby server.</p>"
  ],
  [
    "<p>During postmaster shutdown, ensure that per-socket lock files are removed and listen sockets are closed before we remove the <tt class=\"FILENAME\">postmaster.pid</tt> file (Tom Lane)</p>",
    "<p>This avoids race-condition failures if an external script attempts to start a new postmaster as soon as <tt class=\"LITERAL\">pg_ctl stop</tt> returns.</p>"
  ],
  [
    "<p>Ensure that the postmaster does not exit until all its child processes are gone, even in an immediate shutdown (Tom Lane)</p>",
    "<p>Like the previous item, this avoids possible race conditions against a subsequently-started postmaster.</p>"
  ],
  [
    "<p>Fix postmaster's handling of a startup-process crash during crash recovery (Tom Lane)</p>",
    "<p>If, during a crash recovery cycle, the startup process crashes without having restored database consistency, we'd try to launch a new startup process, which typically would just crash again, leading to an infinite loop.</p>"
  ],
  [
    "<p>Make emergency autovacuuming for multixact wraparound more robust (Andres Freund)</p>"
  ],
  [
    "<p>Do not print a <tt class=\"LITERAL\">WARNING</tt> when an autovacuum worker is already gone when we attempt to signal it, and reduce log verbosity for such signals (Tom Lane)</p>"
  ],
  [
    "<p>Prevent autovacuum launcher from sleeping unduly long if the server clock is moved backwards a large amount (Álvaro Herrera)</p>"
  ],
  [
    "<p>Ensure that cleanup of a GIN index's pending-insertions list is interruptable by cancel requests (Jeff Janes)</p>"
  ],
  [
    "<p>Allow all-zeroes pages in GIN indexes to be reused (Heikki Linnakangas)</p>",
    "<p>Such a page might be left behind after a crash.</p>"
  ],
  [
    "<p>Fix handling of all-zeroes pages in SP-GiST indexes (Heikki Linnakangas)</p>",
    "<p><tt class=\"COMMAND\">VACUUM</tt> attempted to recycle such pages, but did so in a way that wasn't crash-safe.</p>"
  ],
  [
    "<p>Fix off-by-one error that led to otherwise-harmless warnings about <span class=\"QUOTE\">\"apparent wraparound\"</span> in subtrans/multixact truncation (Thomas Munro)</p>"
  ],
  [
    "<p>Fix misreporting of <tt class=\"COMMAND\">CONTINUE</tt> and <tt class=\"COMMAND\">MOVE</tt> statement types in <span class=\"APPLICATION\">PL/pgSQL</span>'s error context messages (Pavel Stehule, Tom Lane)</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">PL/Perl</span> to handle non-<acronym class=\"ACRONYM\">ASCII</acronym> error message texts correctly (Alex Hunsaker)</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">PL/Python</span> crash when returning the string representation of a <tt class=\"TYPE\">record</tt> result (Tom Lane)</p>"
  ],
  [
    "<p>Fix some places in <span class=\"APPLICATION\">PL/Tcl</span> that neglected to check for failure of <code class=\"FUNCTION\">malloc()</code> calls (Michael Paquier, Álvaro Herrera)</p>"
  ],
  [
    "<p>In <tt class=\"FILENAME\">contrib/isn</tt>, fix output of ISBN-13 numbers that begin with 979 (Fabien Coelho)</p>",
    "<p>EANs beginning with 979 (but not 9790) are considered ISBNs, but they must be printed in the new 13-digit format, not the 10-digit format.</p>"
  ],
  [
    "<p>Improve <tt class=\"FILENAME\">contrib/pg_stat_statements</tt>' handling of query-text garbage collection (Peter Geoghegan)</p>",
    "<p>The external file containing query texts could bloat to very large sizes; once it got past 1GB attempts to trim it would fail, soon leading to situations where the file could not be read at all.</p>"
  ],
  [
    "<p>Improve <tt class=\"FILENAME\">contrib/postgres_fdw</tt>'s handling of collation-related decisions (Tom Lane)</p>",
    "<p>The main user-visible effect is expected to be that comparisons involving <tt class=\"TYPE\">varchar</tt> columns will be sent to the remote server for execution in more cases than before.</p>"
  ],
  [
    "<p>Improve <span class=\"APPLICATION\">libpq</span>'s handling of out-of-memory conditions (Michael Paquier, Heikki Linnakangas)</p>"
  ],
  [
    "<p>Fix memory leaks and missing out-of-memory checks in <span class=\"APPLICATION\">ecpg</span> (Michael Paquier)</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">psql</span>'s code for locale-aware formatting of numeric output (Tom Lane)</p>",
    "<p>The formatting code invoked by <tt class=\"LITERAL\">\\pset numericlocale on</tt> did the wrong thing for some uncommon cases such as numbers with an exponent but no decimal point. It could also mangle already-localized output from the <tt class=\"TYPE\">money</tt> data type.</p>"
  ],
  [
    "<p>Prevent crash in <span class=\"APPLICATION\">psql</span>'s <tt class=\"COMMAND\">\\c</tt> command when there is no current connection (Noah Misch)</p>"
  ],
  [
    "<p>Make <span class=\"APPLICATION\">pg_dump</span> handle inherited <tt class=\"LITERAL\">NOT VALID</tt> check constraints correctly (Tom Lane)</p>"
  ],
  [
    "<p>Fix selection of default <span class=\"APPLICATION\">zlib</span> compression level in <span class=\"APPLICATION\">pg_dump</span>'s directory output format (Andrew Dunstan)</p>"
  ],
  [
    "<p>Ensure that temporary files created during a <span class=\"APPLICATION\">pg_dump</span> run with <acronym class=\"ACRONYM\">tar</acronym>-format output are not world-readable (Michael Paquier)</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_dump</span> and <span class=\"APPLICATION\">pg_upgrade</span> to support cases where the <tt class=\"LITERAL\">postgres</tt> or <tt class=\"LITERAL\">template1</tt> database is in a non-default tablespace (Marti Raudsepp, Bruce Momjian)</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_dump</span> to handle object privileges sanely when dumping from a server too old to have a particular privilege type (Tom Lane)</p>",
    "<p>When dumping data types from pre-9.2 servers, and when dumping functions or procedural languages from pre-7.3 servers, <span class=\"APPLICATION\">pg_dump</span> would produce <tt class=\"COMMAND\">GRANT</tt>/<tt class=\"COMMAND\">REVOKE</tt> commands that revoked the owner's grantable privileges and instead granted all privileges to <tt class=\"LITERAL\">PUBLIC</tt>. Since the privileges involved are just <tt class=\"LITERAL\">USAGE</tt> and <tt class=\"LITERAL\">EXECUTE</tt>, this isn't a security problem, but it's certainly a surprising representation of the older systems' behavior. Fix it to leave the default privilege state alone in these cases.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_dump</span> to dump shell types (Tom Lane)</p>",
    "<p>Shell types (that is, not-yet-fully-defined types) aren't useful for much, but nonetheless <span class=\"APPLICATION\">pg_dump</span> should dump them.</p>"
  ],
  [
    "<p>Fix assorted minor memory leaks in <span class=\"APPLICATION\">pg_dump</span> and other client-side programs (Michael Paquier)</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pgbench</span>'s progress-report behavior when a query, or <span class=\"APPLICATION\">pgbench</span> itself, gets stuck (Fabien Coelho)</p>"
  ],
  [
    "<p>Fix spinlock assembly code for Alpha hardware (Tom Lane)</p>"
  ],
  [
    "<p>Fix spinlock assembly code for PPC hardware to be compatible with <acronym class=\"ACRONYM\">AIX</acronym>'s native assembler (Tom Lane)</p>",
    "<p>Building with <span class=\"APPLICATION\">gcc</span> didn't work if <span class=\"APPLICATION\">gcc</span> had been configured to use the native assembler, which is becoming more common.</p>"
  ],
  [
    "<p>On <acronym class=\"ACRONYM\">AIX</acronym>, test the <tt class=\"LITERAL\">-qlonglong</tt> compiler option rather than just assuming it's safe to use (Noah Misch)</p>"
  ],
  [
    "<p>On <acronym class=\"ACRONYM\">AIX</acronym>, use <tt class=\"LITERAL\">-Wl,-brtllib</tt> link option to allow symbols to be resolved at runtime (Noah Misch)</p>",
    "<p>Perl relies on this ability in 5.8.0 and later.</p>"
  ],
  [
    "<p>Avoid use of inline functions when compiling with 32-bit <span class=\"APPLICATION\">xlc</span>, due to compiler bugs (Noah Misch)</p>"
  ],
  [
    "<p>Use <tt class=\"FILENAME\">librt</tt> for <code class=\"FUNCTION\">sched_yield()</code> when necessary, which it is on some Solaris versions (Oskari Saarenmaa)</p>"
  ],
  [
    "<p>Translate encoding <tt class=\"LITERAL\">UHC</tt> as Windows code page 949 (Noah Misch)</p>",
    "<p>This fixes presentation of non-ASCII log messages from processes that are not attached to any particular database, such as the postmaster.</p>"
  ],
  [
    "<p>On Windows, avoid failure when doing encoding conversion to UTF16 outside a transaction, such as for log messages (Noah Misch)</p>"
  ],
  [
    "<p>Fix postmaster startup failure due to not copying <code class=\"FUNCTION\">setlocale()</code>'s return value (Noah Misch)</p>",
    "<p>This has been reported on Windows systems with the ANSI code page set to CP936 (<span class=\"QUOTE\">\"Chinese (Simplified, PRC)\"</span>), and may occur with other multibyte code pages.</p>"
  ],
  [
    "<p>Fix Windows <tt class=\"FILENAME\">install.bat</tt> script to handle target directory names that contain spaces (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Make the numeric form of the <span class=\"PRODUCTNAME\">PostgreSQL</span> version number (e.g., <tt class=\"LITERAL\">90405</tt>) readily available to extension Makefiles, as a variable named <tt class=\"VARNAME\">VERSION_NUM</tt> (Michael Paquier)</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"APPLICATION\">tzdata</span> release 2015g for DST law changes in Cayman Islands, Fiji, Moldova, Morocco, Norfolk Island, North Korea, Turkey, and Uruguay. There is a new zone name <tt class=\"LITERAL\">America/Fort_Nelson</tt> for the Canadian Northern Rockies.</p>"
  ]
]