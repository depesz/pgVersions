[
  [
    "<p>Remove security vulnerabilities that allowed connected\n          users to read backend memory (Tom)</p>",
    "<p>The vulnerabilities involve suppressing the normal\n          check that a SQL function returns the data type it's\n          declared to, and changing the data type of a table column\n          (CVE-2007-0555, CVE-2007-0556). These errors can easily\n          be exploited to cause a backend crash, and in principle\n          might be used to read database content that the user\n          should not be able to access.</p>"
  ],
  [
    "<p>Fix rare bug wherein btree index page splits could\n          fail due to choosing an infeasible split point (Heikki\n          Linnakangas)</p>"
  ],
  [
    "<p>Fix for rare Assert() crash triggered by <code class=\"literal\">UNION</code> (Tom)</p>"
  ],
  [
    "<p>Tighten security of multi-byte character processing\n          for UTF8 sequences over three bytes long (Tom)</p>"
  ]
]