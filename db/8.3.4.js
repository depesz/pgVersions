[
  [
    "<p>Fix bug in btree WAL recovery code (Heikki)</p>",
    "<p>Recovery failed if the WAL ended partway through a\n          page split operation.</p>"
  ],
  [
    "<p>Fix potential use of wrong cutoff XID for HOT page\n          pruning (Alvaro)</p>",
    "<p>This error created a risk of corruption in system\n          catalogs that are consulted by <code class=\"command\">VACUUM</code>: dead tuple versions might be\n          removed too soon. The impact of this on actual database\n          operations would be minimal, since the system doesn't\n          follow MVCC rules while examining catalogs, but it might\n          result in transiently wrong output from <span class=\"application\">pg_dump</span> or other client\n          programs.</p>"
  ],
  [
    "<p>Fix potential miscalculation of <code class=\"structfield\">datfrozenxid</code> (Alvaro)</p>",
    "<p>This error may explain some recent reports of failure\n          to remove old <code class=\"structname\">pg_clog</code>\n          data.</p>"
  ],
  [
    "<p>Fix incorrect HOT updates after <code class=\"structname\">pg_class</code> is reindexed (Tom)</p>",
    "<p>Corruption of <code class=\"structname\">pg_class</code>\n          could occur if <code class=\"literal\">REINDEX TABLE\n          pg_class</code> was followed in the same session by an\n          <code class=\"literal\">ALTER TABLE RENAME</code> or\n          <code class=\"literal\">ALTER TABLE SET SCHEMA</code>\n          command.</p>"
  ],
  [
    "<p>Fix missed <span class=\"quote\">&#x201C;<span class=\"quote\">combo cid</span>&#x201D;</span> case (Karl\n          Schnaitter)</p>",
    "<p>This error made rows incorrectly invisible to a\n          transaction in which they had been deleted by multiple\n          subtransactions that all aborted.</p>"
  ],
  [
    "<p>Prevent autovacuum from crashing if the table it's\n          currently checking is deleted at just the wrong time\n          (Alvaro)</p>"
  ],
  [
    "<p>Widen local lock counters from 32 to 64 bits (Tom)</p>",
    "<p>This responds to reports that the counters could\n          overflow in sufficiently long transactions, leading to\n          unexpected <span class=\"quote\">&#x201C;<span class=\"quote\">lock\n          is already held</span>&#x201D;</span> errors.</p>"
  ],
  [
    "<p>Fix possible duplicate output of tuples during a GiST\n          index scan (Teodor)</p>"
  ],
  [
    "<p>Regenerate foreign key checking queries from scratch\n          when either table is modified (Tom)</p>",
    "<p>Previously, 8.3 would attempt to replan the query, but\n          would work from previously generated query text. This led\n          to failures if a table or column was renamed.</p>"
  ],
  [
    "<p>Fix missed permissions checks when a view contains a\n          simple <code class=\"literal\">UNION ALL</code> construct\n          (Heikki)</p>",
    "<p>Permissions for the referenced tables were checked\n          properly, but not permissions for the view itself.</p>"
  ],
  [
    "<p>Add checks in executor startup to ensure that the\n          tuples produced by an <code class=\"command\">INSERT</code>\n          or <code class=\"command\">UPDATE</code> will match the\n          target table's current rowtype (Tom)</p>",
    "<p>This situation is believed to be impossible in 8.3,\n          but it can happen in prior releases, so a check seems\n          prudent.</p>"
  ],
  [
    "<p>Fix possible repeated drops during <code class=\"command\">DROP OWNED</code> (Tom)</p>",
    "<p>This would typically result in strange errors such as\n          <span class=\"quote\">&#x201C;<span class=\"quote\">cache lookup\n          failed for relation NNN</span>&#x201D;</span>.</p>"
  ],
  [
    "<p>Fix several memory leaks in XML operations (Kris\n          Jurka, Tom)</p>"
  ],
  [
    "<p>Fix <code class=\"function\">xmlserialize()</code> to\n          raise error properly for unacceptable target data type\n          (Tom)</p>"
  ],
  [
    "<p>Fix a couple of places that mis-handled multibyte\n          characters in text search configuration file parsing\n          (Tom)</p>",
    "<p>Certain characters occurring in configuration files\n          would always cause <span class=\"quote\">&#x201C;<span class=\"quote\">invalid byte sequence for encoding</span>&#x201D;</span>\n          failures.</p>"
  ],
  [
    "<p>Provide file name and line number location for all\n          errors reported in text search configuration files\n          (Tom)</p>"
  ],
  [
    "<p>Fix <code class=\"literal\">AT TIME ZONE</code> to first\n          try to interpret its timezone argument as a timezone\n          abbreviation, and only try it as a full timezone name if\n          that fails, rather than the other way around as formerly\n          (Tom)</p>",
    "<p>The timestamp input functions have always resolved\n          ambiguous zone names in this order. Making <code class=\"literal\">AT TIME ZONE</code> do so as well improves\n          consistency, and fixes a compatibility bug introduced in\n          8.1: in ambiguous cases we now behave the same as 8.0 and\n          before did, since in the older versions <code class=\"literal\">AT TIME ZONE</code> accepted <span class=\"emphasis\"><em>only</em></span> abbreviations.</p>"
  ],
  [
    "<p>Fix datetime input functions to correctly detect\n          integer overflow when running on a 64-bit platform\n          (Tom)</p>"
  ],
  [
    "<p>Prevent integer overflows during units conversion when\n          displaying a configuration parameter that has units\n          (Tom)</p>"
  ],
  [
    "<p>Improve performance of writing very long log messages\n          to syslog (Tom)</p>"
  ],
  [
    "<p>Allow spaces in the suffix part of an LDAP URL in\n          <code class=\"filename\">pg_hba.conf</code> (Tom)</p>"
  ],
  [
    "<p>Fix bug in backwards scanning of a cursor on a\n          <code class=\"literal\">SELECT DISTINCT ON</code> query\n          (Tom)</p>"
  ],
  [
    "<p>Fix planner bug that could improperly push down\n          <code class=\"literal\">IS NULL</code> tests below an outer\n          join (Tom)</p>",
    "<p>This was triggered by occurrence of <code class=\"literal\">IS NULL</code> tests for the same relation in\n          all arms of an upper <code class=\"literal\">OR</code>\n          clause.</p>"
  ],
  [
    "<p>Fix planner bug with nested sub-select expressions\n          (Tom)</p>",
    "<p>If the outer sub-select has no direct dependency on\n          the parent query, but the inner one does, the outer value\n          might not get recalculated for new parent query rows.</p>"
  ],
  [
    "<p>Fix planner to estimate that <code class=\"literal\">GROUP BY</code> expressions yielding boolean\n          results always result in two groups, regardless of the\n          expressions' contents (Tom)</p>",
    "<p>This is very substantially more accurate than the\n          regular <code class=\"literal\">GROUP BY</code> estimate\n          for certain boolean tests like <em class=\"replaceable\"><code>col</code></em> <code class=\"literal\">IS NULL</code>.</p>"
  ],
  [
    "<p>Fix PL/pgSQL to not fail when a <code class=\"literal\">FOR</code> loop's target variable is a record\n          containing composite-type fields (Tom)</p>"
  ],
  [
    "<p>Fix PL/Tcl to behave correctly with Tcl 8.5, and to be\n          more careful about the encoding of data sent to or from\n          Tcl (Tom)</p>"
  ],
  [
    "<p>Improve performance of <code class=\"function\">PQescapeBytea()</code> (Rudolf Leitgeb)</p>"
  ],
  [
    "<p>On Windows, work around a Microsoft bug by preventing\n          <span class=\"application\">libpq</span> from trying to\n          send more than 64kB per system call (Magnus)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">ecpg</span> to handle\n          variables properly in <code class=\"command\">SET</code>\n          commands (Michael)</p>"
  ],
  [
    "<p>Improve <span class=\"application\">pg_dump</span> and\n          <span class=\"application\">pg_restore</span>'s error\n          reporting after failure to send a SQL command (Tom)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_ctl</span> to\n          properly preserve postmaster command-line arguments\n          across a <code class=\"literal\">restart</code> (Bruce)</p>"
  ],
  [
    "<p>Fix erroneous WAL file cutoff point calculation in\n          <span class=\"application\">pg_standby</span> (Simon)</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2008f (for DST law\n          changes in Argentina, Bahamas, Brazil, Mauritius,\n          Morocco, Pakistan, Palestine, and Paraguay)</p>"
  ]
]