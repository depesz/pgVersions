[
  [
    "<p>Fix incorrect volatility markings on a few built-in functions (Thomas Munro, Tom Lane)</p>",
    "<p>The functions <code class=\"FUNCTION\">query_to_xml</code>, <code class=\"FUNCTION\">cursor_to_xml</code>, <code class=\"FUNCTION\">cursor_to_xmlschema</code>, <code class=\"FUNCTION\">query_to_xmlschema</code>, and <code class=\"FUNCTION\">query_to_xml_and_xmlschema</code> should be marked volatile because they execute user-supplied queries that might contain volatile operations. They were not, leading to a risk of incorrect query optimization. This has been repaired for new installations by correcting the initial catalog data, but existing installations will continue to contain the incorrect markings. Practical use of these functions seems to pose little hazard, but in case of trouble, it can be fixed by manually updating these functions' <tt class=\"STRUCTNAME\">pg_proc</tt> entries, for example <tt class=\"LITERAL\">ALTER FUNCTION pg_catalog.query_to_xml(text, boolean, boolean, text) VOLATILE</tt>. (Note that that will need to be done in each database of the installation.) Another option is to <span class=\"APPLICATION\">pg_upgrade</span> the database to a version containing the corrected initial data.</p>"
  ],
  [
    "<p>Avoid re-using TOAST value OIDs that match dead-but-not-yet-vacuumed TOAST entries (Pavan Deolasee)</p>",
    "<p>Once the OID counter has wrapped around, it's possible to assign a TOAST value whose OID matches a previously deleted entry in the same TOAST table. If that entry were not yet vacuumed away, this resulted in <span class=\"QUOTE\">\"unexpected chunk number 0 (expected 1) for toast value <tt class=\"REPLACEABLE c2\">nnnnn</tt>\"</span> errors, which would persist until the dead entry was removed by <tt class=\"COMMAND\">VACUUM</tt>. Fix by not selecting such OIDs when creating a new TOAST entry.</p>"
  ],
  [
    "<p>Change <tt class=\"COMMAND\">ANALYZE</tt>'s algorithm for updating <tt class=\"STRUCTNAME\">pg_class</tt>.<tt class=\"STRUCTFIELD\">reltuples</tt> (David Gould)</p>",
    "<p>Previously, pages not actually scanned by <tt class=\"COMMAND\">ANALYZE</tt> were assumed to retain their old tuple density. In a large table where <tt class=\"COMMAND\">ANALYZE</tt> samples only a small fraction of the pages, this meant that the overall tuple density estimate could not change very much, so that <tt class=\"STRUCTFIELD\">reltuples</tt> would change nearly proportionally to changes in the table's physical size (<tt class=\"STRUCTFIELD\">relpages</tt>) regardless of what was actually happening in the table. This has been observed to result in <tt class=\"STRUCTFIELD\">reltuples</tt> becoming so much larger than reality as to effectively shut off autovacuuming. To fix, assume that <tt class=\"COMMAND\">ANALYZE</tt>'s sample is a statistically unbiased sample of the table (as it should be), and just extrapolate the density observed within those pages to the whole table.</p>"
  ],
  [
    "<p>Avoid deadlocks in concurrent <tt class=\"COMMAND\">CREATE INDEX CONCURRENTLY</tt> commands that are run under <tt class=\"LITERAL\">SERIALIZABLE</tt> or <tt class=\"LITERAL\">REPEATABLE READ</tt> transaction isolation (Tom Lane)</p>"
  ],
  [
    "<p>Fix possible slow execution of <tt class=\"COMMAND\">REFRESH MATERIALIZED VIEW CONCURRENTLY</tt> (Thomas Munro)</p>"
  ],
  [
    "<p>Fix <tt class=\"LITERAL\">UPDATE/DELETE ... WHERE CURRENT OF</tt> to not fail when the referenced cursor uses an index-only-scan plan (Yugo Nagata, Tom Lane)</p>"
  ],
  [
    "<p>Fix incorrect planning of join clauses pushed into parameterized paths (Andrew Gierth, Tom Lane)</p>",
    "<p>This error could result in misclassifying a condition as a <span class=\"QUOTE\">\"join filter\"</span> for an outer join when it should be a plain <span class=\"QUOTE\">\"filter\"</span> condition, leading to incorrect join output.</p>"
  ],
  [
    "<p>Fix possibly incorrect generation of an index-only-scan plan when the same table column appears in multiple index columns, and only some of those index columns use operator classes that can return the column value (Kyotaro Horiguchi)</p>"
  ],
  [
    "<p>Fix misoptimization of <tt class=\"LITERAL\">CHECK</tt> constraints having provably-NULL subclauses of top-level <tt class=\"LITERAL\">AND</tt>/<tt class=\"LITERAL\">OR</tt> conditions (Tom Lane, Dean Rasheed)</p>",
    "<p>This could, for example, allow constraint exclusion to exclude a child table that should not be excluded from a query.</p>"
  ],
  [
    "<p>Fix executor crash due to double free in some <tt class=\"LITERAL\">GROUPING SET</tt> usages (Peter Geoghegan)</p>"
  ],
  [
    "<p>Avoid crash if a table rewrite event trigger is added concurrently with a command that could call such a trigger (Álvaro Herrera, Andrew Gierth, Tom Lane)</p>"
  ],
  [
    "<p>Avoid failure if a query-cancel or session-termination interrupt occurs while committing a prepared transaction (Stas Kelvich)</p>"
  ],
  [
    "<p>Fix query-lifespan memory leakage in repeatedly executed hash joins (Tom Lane)</p>"
  ],
  [
    "<p>Fix overly strict sanity check in <code class=\"FUNCTION\">heap_prepare_freeze_tuple</code> (Álvaro Herrera)</p>",
    "<p>This could result in incorrect <span class=\"QUOTE\">\"cannot freeze committed xmax\"</span> failures in databases that have been <span class=\"APPLICATION\">pg_upgrade</span>'d from 9.2 or earlier.</p>"
  ],
  [
    "<p>Prevent dangling-pointer dereference when a C-coded before-update row trigger returns the <span class=\"QUOTE\">\"old\"</span> tuple (Rushabh Lathia)</p>"
  ],
  [
    "<p>Reduce locking during autovacuum worker scheduling (Jeff Janes)</p>",
    "<p>The previous behavior caused drastic loss of potential worker concurrency in databases with many tables.</p>"
  ],
  [
    "<p>Ensure client hostname is copied while copying <tt class=\"STRUCTNAME\">pg_stat_activity</tt> data to local memory (Edmund Horner)</p>",
    "<p>Previously the supposedly-local snapshot contained a pointer into shared memory, allowing the client hostname column to change unexpectedly if any existing session disconnected.</p>"
  ],
  [
    "<p>Fix incorrect processing of multiple compound affixes in <tt class=\"LITERAL\">ispell</tt> dictionaries (Arthur Zakirov)</p>"
  ],
  [
    "<p>Fix collation-aware searches (that is, indexscans using inequality operators) in SP-GiST indexes on text columns (Tom Lane)</p>",
    "<p>Such searches would return the wrong set of rows in most non-C locales.</p>"
  ],
  [
    "<p>Count the number of index tuples correctly during initial build of an SP-GiST index (Tomas Vondra)</p>",
    "<p>Previously, the tuple count was reported to be the same as that of the underlying table, which is wrong if the index is partial.</p>"
  ],
  [
    "<p>Count the number of index tuples correctly during vacuuming of a GiST index (Andrey Borodin)</p>",
    "<p>Previously it reported the estimated number of heap tuples, which might be inaccurate, and is certainly wrong if the index is partial.</p>"
  ],
  [
    "<p>Fix a corner case where a streaming standby gets stuck at a WAL continuation record (Kyotaro Horiguchi)</p>"
  ],
  [
    "<p>In logical decoding, avoid possible double processing of WAL data when a walsender restarts (Craig Ringer)</p>"
  ],
  [
    "<p>Allow <code class=\"FUNCTION\">scalarltsel</code> and <code class=\"FUNCTION\">scalargtsel</code> to be used on non-core datatypes (Tomas Vondra)</p>"
  ],
  [
    "<p>Reduce <span class=\"APPLICATION\">libpq</span>'s memory consumption when a server error is reported after a large amount of query output has been collected (Tom Lane)</p>",
    "<p>Discard the previous output before, not after, processing the error message. On some platforms, notably Linux, this can make a difference in the application's subsequent memory footprint.</p>"
  ],
  [
    "<p>Fix double-free crashes in <span class=\"APPLICATION\">ecpg</span> (Patrick Krecker, Jeevan Ladhe)</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">ecpg</span> to handle <tt class=\"TYPE\">long long int</tt> variables correctly in MSVC builds (Michael Meskes, Andrew Gierth)</p>"
  ],
  [
    "<p>Fix mis-quoting of values for list-valued GUC variables in dumps (Michael Paquier, Tom Lane)</p>",
    "<p>The <tt class=\"VARNAME\">local_preload_libraries</tt>, <tt class=\"VARNAME\">session_preload_libraries</tt>, <tt class=\"VARNAME\">shared_preload_libraries</tt>, and <tt class=\"VARNAME\">temp_tablespaces</tt> variables were not correctly quoted in <span class=\"APPLICATION\">pg_dump</span> output. This would cause problems if settings for these variables appeared in <tt class=\"COMMAND\">CREATE FUNCTION ... SET</tt> or <tt class=\"COMMAND\">ALTER DATABASE/ROLE ... SET</tt> clauses.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_recvlogical</span> to not fail against pre-v10 <span class=\"PRODUCTNAME\">PostgreSQL</span> servers (Michael Paquier)</p>",
    "<p>A previous fix caused <span class=\"APPLICATION\">pg_recvlogical</span> to issue a command regardless of server version, but it should only be issued to v10 and later servers.</p>"
  ],
  [
    "<p>Ensure that <span class=\"APPLICATION\">pg_rewind</span> deletes files on the target server if they are deleted from the source server during the run (Takayuki Tsunakawa)</p>",
    "<p>Failure to do this could result in data inconsistency on the target, particularly if the file in question is a WAL segment.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_rewind</span> to handle tables in non-default tablespaces correctly (Takayuki Tsunakawa)</p>"
  ],
  [
    "<p>Fix overflow handling in <span class=\"APPLICATION\">PL/pgSQL</span> integer <tt class=\"COMMAND\">FOR</tt> loops (Tom Lane)</p>",
    "<p>The previous coding failed to detect overflow of the loop variable on some non-gcc compilers, leading to an infinite loop.</p>"
  ],
  [
    "<p>Adjust <span class=\"APPLICATION\">PL/Python</span> regression tests to pass under Python 3.7 (Peter Eisentraut)</p>"
  ],
  [
    "<p>Support testing <span class=\"APPLICATION\">PL/Python</span> and related modules when building with Python 3 and MSVC (Andrew Dunstan)</p>"
  ],
  [
    "<p>Support building with Microsoft Visual Studio 2015 (Michael Paquier)</p>",
    "<p>Various fixes needed for VS2015 compatibility were previously back-patched into the 9.5 branch, but this one was missed.</p>"
  ],
  [
    "<p>Rename internal <code class=\"FUNCTION\">b64_encode</code> and <code class=\"FUNCTION\">b64_decode</code> functions to avoid conflict with Solaris 11.4 built-in functions (Rainer Orth)</p>"
  ],
  [
    "<p>Sync our copy of the timezone library with IANA tzcode release 2018e (Tom Lane)</p>",
    "<p>This fixes the <span class=\"APPLICATION\">zic</span> timezone data compiler to cope with negative daylight-savings offsets. While the <span class=\"PRODUCTNAME\">PostgreSQL</span> project will not immediately ship such timezone data, <span class=\"APPLICATION\">zic</span> might be used with timezone data obtained directly from IANA, so it seems prudent to update <span class=\"APPLICATION\">zic</span> now.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"APPLICATION\">tzdata</span> release 2018d for DST law changes in Palestine and Antarctica (Casey Station), plus historical corrections for Portugal and its colonies, as well as Enderbury, Jamaica, Turks &amp; Caicos Islands, and Uruguay.</p>"
  ]
]