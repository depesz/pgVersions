[
  [
    "<p>Fix incorrect handling of NULL index entries in indexed <tt class=\"LITERAL\">ROW()</tt> comparisons (Tom Lane)</p>",
    "<p>An index search using a row comparison such as <tt class=\"LITERAL\">ROW(a, b) &gt; ROW('x', 'y')</tt> would stop upon reaching a NULL entry in the <tt class=\"STRUCTFIELD\">b</tt> column, ignoring the fact that there might be non-NULL <tt class=\"STRUCTFIELD\">b</tt> values associated with later values of <tt class=\"STRUCTFIELD\">a</tt>.</p>"
  ],
  [
    "<p>Avoid unlikely data-loss scenarios due to renaming files without adequate <code class=\"FUNCTION\">fsync()</code> calls before and after (Michael Paquier, Tomas Vondra, Andres Freund)</p>"
  ],
  [
    "<p>Fix bug in <code class=\"FUNCTION\">json_to_record()</code> when a field of its input object contains a sub-object with a field name matching one of the requested output column names (Tom Lane)</p>"
  ],
  [
    "<p>Fix misformatting of negative time zone offsets by <code class=\"FUNCTION\">to_char()</code>'s <tt class=\"LITERAL\">OF</tt> format code (Thomas Munro, Tom Lane)</p>"
  ],
  [
    "<p>Ignore <a href=\"https://www.postgresql.org/docs/9.4/standby-settings.html#RECOVERY-MIN-APPLY-DELAY\">recovery_min_apply_delay</a> parameter until recovery has reached a consistent state (Michael Paquier)</p>",
    "<p>Previously, standby servers would delay application of WAL records in response to <tt class=\"VARNAME\">recovery_min_apply_delay</tt> even while replaying the initial portion of WAL needed to make their database state valid. Since the standby is useless until it's reached a consistent database state, this was deemed unhelpful.</p>"
  ],
  [
    "<p>Correctly handle cases where <tt class=\"LITERAL\">pg_subtrans</tt> is close to XID wraparound during server startup (Jeff Janes)</p>"
  ],
  [
    "<p>Fix assorted bugs in logical decoding (Andres Freund)</p>",
    "<p>Trouble cases included tuples larger than one page when replica identity is <tt class=\"LITERAL\">FULL</tt>, <tt class=\"COMMAND\">UPDATE</tt>s that change a primary key within a transaction large enough to be spooled to disk, incorrect reports of <span class=\"QUOTE\">\"subxact logged without previous toplevel record\"</span>, and incorrect reporting of a transaction's commit time.</p>"
  ],
  [
    "<p>Fix planner error with nested security barrier views when the outer view has a <tt class=\"LITERAL\">WHERE</tt> clause containing a correlated subquery (Dean Rasheed)</p>"
  ],
  [
    "<p>Fix corner-case crash due to trying to free <code class=\"FUNCTION\">localeconv()</code> output strings more than once (Tom Lane)</p>"
  ],
  [
    "<p>Fix parsing of affix files for <tt class=\"LITERAL\">ispell</tt> dictionaries (Tom Lane)</p>",
    "<p>The code could go wrong if the affix file contained any characters whose byte length changes during case-folding, for example <tt class=\"LITERAL\">I</tt> in Turkish UTF8 locales.</p>"
  ],
  [
    "<p>Avoid use of <code class=\"FUNCTION\">sscanf()</code> to parse <tt class=\"LITERAL\">ispell</tt> dictionary files (Artur Zakirov)</p>",
    "<p>This dodges a portability problem on FreeBSD-derived platforms (including macOS).</p>"
  ],
  [
    "<p>Avoid a crash on old Windows versions (before 7SP1/2008R2SP1) with an AVX2-capable CPU and a Postgres build done with Visual Studio 2013 (Christian Ullrich)</p>",
    "<p>This is a workaround for a bug in Visual Studio 2013's runtime library, which Microsoft have stated they will not fix in that version.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">psql</span>'s tab completion logic to handle multibyte characters properly (Kyotaro Horiguchi, Robert Haas)</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">psql</span>'s tab completion for <tt class=\"LITERAL\">SECURITY LABEL</tt> (Tom Lane)</p>",
    "<p>Pressing TAB after <tt class=\"LITERAL\">SECURITY LABEL</tt> might cause a crash or offering of inappropriate keywords.</p>"
  ],
  [
    "<p>Make <span class=\"APPLICATION\">pg_ctl</span> accept a wait timeout from the <tt class=\"ENVAR\">PGCTLTIMEOUT</tt> environment variable, if none is specified on the command line (Noah Misch)</p>",
    "<p>This eases testing of slower buildfarm members by allowing them to globally specify a longer-than-normal timeout for postmaster startup and shutdown.</p>"
  ],
  [
    "<p>Fix incorrect test for Windows service status in <span class=\"APPLICATION\">pg_ctl</span> (Manuel Mathar)</p>",
    "<p>The previous set of minor releases attempted to fix <span class=\"APPLICATION\">pg_ctl</span> to properly determine whether to send log messages to Window's Event Log, but got the test backwards.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pgbench</span> to correctly handle the combination of <tt class=\"LITERAL\">-C</tt> and <tt class=\"LITERAL\">-M prepared</tt> options (Tom Lane)</p>"
  ],
  [
    "<p>In <span class=\"APPLICATION\">pg_upgrade</span>, skip creating a deletion script when the new data directory is inside the old data directory (Bruce Momjian)</p>",
    "<p>Blind application of the script in such cases would result in loss of the new data directory.</p>"
  ],
  [
    "<p>In PL/Perl, properly translate empty Postgres arrays into empty Perl arrays (Alex Hunsaker)</p>"
  ],
  [
    "<p>Make PL/Python cope with function names that aren't valid Python identifiers (Jim Nasby)</p>"
  ],
  [
    "<p>Fix multiple mistakes in the statistics returned by <tt class=\"FILENAME\">contrib/pgstattuple</tt>'s <code class=\"FUNCTION\">pgstatindex()</code> function (Tom Lane)</p>"
  ],
  [
    "<p>Remove dependency on <tt class=\"LITERAL\">psed</tt> in MSVC builds, since it's no longer provided by core Perl (Michael Paquier, Andrew Dunstan)</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"APPLICATION\">tzdata</span> release 2016c for DST law changes in Azerbaijan, Chile, Haiti, Palestine, and Russia (Altai, Astrakhan, Kirov, Sakhalin, Ulyanovsk regions), plus historical corrections for Lithuania, Moldova, and Russia (Kaliningrad, Samara, Volgograd).</p>"
  ]
]