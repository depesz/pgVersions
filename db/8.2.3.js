[
  [
    "<p>Remove overly-restrictive check for type length in\n          constraints and functional indexes(Tom)</p>"
  ],
  [
    "<p>Fix optimization so MIN/MAX in subqueries can again\n          use indexes (Tom)</p>"
  ]
]