[
  [
    "<p>Restrict visibility of <code class=\"structname\">pg_user_mappings</code>.<code class=\"structfield\">umoptions</code>, to protect passwords\n          stored as user mapping options (Michael Paquier, Feike\n          Steenbergen)</p>",
    "<p>The previous coding allowed the owner of a foreign\n          server object, or anyone he has granted server\n          <code class=\"literal\">USAGE</code> permission to, to see\n          the options for all user mappings associated with that\n          server. This might well include passwords for other\n          users. Adjust the view definition to match the behavior\n          of <code class=\"structname\">information_schema.user_mapping_options</code>,\n          namely that these options are visible to the user being\n          mapped, or if the mapping is for <code class=\"literal\">PUBLIC</code> and the current user is the\n          server owner, or if the current user is a superuser.\n          (CVE-2017-7486)</p>",
    "<p>By itself, this patch will only fix the behavior in\n          newly initdb'd databases. If you wish to apply this\n          change in an existing database, follow the corrected\n          procedure shown in the changelog entry for CVE-2017-7547,\n          in <a class=\"xref\" href=\"https://www.postgresql.org/docs/current/static/release-9-3-18.html\" title=\"E.44.&#xA0;Release 9.3.18\">Section&#xA0;E.44</a>.</p>"
  ],
  [
    "<p>Prevent exposure of statistical information via leaky\n          operators (Peter Eisentraut)</p>",
    "<p>Some selectivity estimation functions in the planner\n          will apply user-defined operators to values obtained from\n          <code class=\"structname\">pg_statistic</code>, such as\n          most common values and histogram entries. This occurs\n          before table permissions are checked, so a nefarious user\n          could exploit the behavior to obtain these values for\n          table columns he does not have permission to read. To\n          fix, fall back to a default estimate if the operator's\n          implementation function is not certified leak-proof and\n          the calling user does not have permission to read the\n          table column whose statistics are needed. At least one of\n          these criteria is satisfied in most cases in practice.\n          (CVE-2017-7484)</p>"
  ],
  [
    "<p>Restore <span class=\"application\">libpq</span>'s\n          recognition of the <code class=\"envar\">PGREQUIRESSL</code> environment variable (Daniel\n          Gustafsson)</p>",
    "<p>Processing of this environment variable was\n          unintentionally dropped in <span class=\"productname\">PostgreSQL</span> 9.3, but its\n          documentation remained. This creates a security hazard,\n          since users might be relying on the environment variable\n          to force SSL-encrypted connections, but that would no\n          longer be guaranteed. Restore handling of the variable,\n          but give it lower priority than <code class=\"envar\">PGSSLMODE</code>, to avoid breaking\n          configurations that work correctly with post-9.3 code.\n          (CVE-2017-7485)</p>"
  ],
  [
    "<p>Fix possible corruption of <span class=\"quote\">&#x201C;<span class=\"quote\">init forks</span>&#x201D;</span> of\n          unlogged indexes (Robert Haas, Michael Paquier)</p>",
    "<p>This could result in an unlogged index being set to an\n          invalid state after a crash and restart. Such a problem\n          would persist until the index was dropped and\n          rebuilt.</p>"
  ],
  [
    "<p>Fix incorrect reconstruction of <code class=\"structname\">pg_subtrans</code> entries when a standby\n          server replays a prepared but uncommitted two-phase\n          transaction (Tom Lane)</p>",
    "<p>In most cases this turned out to have no visible ill\n          effects, but in corner cases it could result in circular\n          references in <code class=\"structname\">pg_subtrans</code>, potentially causing\n          infinite loops in queries that examine rows modified by\n          the two-phase transaction.</p>"
  ],
  [
    "<p>Ensure parsing of queries in extension scripts sees\n          the results of immediately-preceding DDL (Julien Rouhaud,\n          Tom Lane)</p>",
    "<p>Due to lack of a cache flush step between commands in\n          an extension script file, non-utility queries might not\n          see the effects of an immediately preceding catalog\n          change, such as <code class=\"command\">ALTER TABLE ...\n          RENAME</code>.</p>"
  ],
  [
    "<p>Skip tablespace privilege checks when <code class=\"command\">ALTER TABLE ... ALTER COLUMN TYPE</code>\n          rebuilds an existing index (Noah Misch)</p>",
    "<p>The command failed if the calling user did not\n          currently have <code class=\"literal\">CREATE</code>\n          privilege for the tablespace containing the index. That\n          behavior seems unhelpful, so skip the check, allowing the\n          index to be rebuilt where it is.</p>"
  ],
  [
    "<p>Fix <code class=\"command\">ALTER TABLE ... VALIDATE\n          CONSTRAINT</code> to not recurse to child tables when the\n          constraint is marked <code class=\"literal\">NO\n          INHERIT</code> (Amit Langote)</p>",
    "<p>This fix prevents unwanted <span class=\"quote\">&#x201C;<span class=\"quote\">constraint does not\n          exist</span>&#x201D;</span> failures when no matching constraint\n          is present in the child tables.</p>"
  ],
  [
    "<p>Fix <code class=\"command\">VACUUM</code> to account\n          properly for pages that could not be scanned due to\n          conflicting page pins (Andrew Gierth)</p>",
    "<p>This tended to lead to underestimation of the number\n          of tuples in the table. In the worst case of a small\n          heavily-contended table, <code class=\"command\">VACUUM</code> could incorrectly report that the\n          table contained no tuples, leading to very bad planning\n          choices.</p>"
  ],
  [
    "<p>Ensure that bulk-tuple-transfer loops within a hash\n          join are interruptible by query cancel requests (Tom\n          Lane, Thomas Munro)</p>"
  ],
  [
    "<p>Fix <code class=\"function\">cursor_to_xml()</code> to\n          produce valid output with <em class=\"replaceable\"><code>tableforest</code></em> = false\n          (Thomas Munro, Peter Eisentraut)</p>",
    "<p>Previously it failed to produce a wrapping\n          <code class=\"literal\">&lt;table&gt;</code> element.</p>"
  ],
  [
    "<p>Improve performance of <code class=\"structname\">pg_timezone_names</code> view (Tom Lane,\n          David Rowley)</p>"
  ],
  [
    "<p>Fix sloppy handling of corner-case errors from\n          <code class=\"function\">lseek()</code> and <code class=\"function\">close()</code> (Tom Lane)</p>",
    "<p>Neither of these system calls are likely to fail in\n          typical situations, but if they did, <code class=\"filename\">fd.c</code> could get quite confused.</p>"
  ],
  [
    "<p>Fix incorrect check for whether postmaster is running\n          as a Windows service (Michael Paquier)</p>",
    "<p>This could result in attempting to write to the event\n          log when that isn't accessible, so that no logging\n          happens at all.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">ecpg</span> to support\n          <code class=\"command\">COMMIT PREPARED</code> and\n          <code class=\"command\">ROLLBACK PREPARED</code> (Masahiko\n          Sawada)</p>"
  ],
  [
    "<p>Fix a double-free error when processing dollar-quoted\n          string literals in <span class=\"application\">ecpg</span>\n          (Michael Meskes)</p>"
  ],
  [
    "<p>In <span class=\"application\">pg_dump</span>, fix\n          incorrect schema and owner marking for comments and\n          security labels of some types of database objects\n          (Giuseppe Broccolo, Tom Lane)</p>",
    "<p>In simple cases this caused no ill effects; but for\n          example, a schema-selective restore might omit comments\n          it should include, because they were not marked as\n          belonging to the schema of their associated object.</p>"
  ],
  [
    "<p>Avoid emitting an invalid list file in <code class=\"literal\">pg_restore -l</code> when SQL object names\n          contain newlines (Tom Lane)</p>",
    "<p>Replace newlines by spaces, which is sufficient to\n          make the output valid for <code class=\"literal\">pg_restore -L</code>'s purposes.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_upgrade</span> to\n          transfer comments and security labels attached to\n          <span class=\"quote\">&#x201C;<span class=\"quote\">large\n          objects</span>&#x201D;</span> (blobs) (Stephen Frost)</p>",
    "<p>Previously, blobs were correctly transferred to the\n          new database, but any comments or security labels\n          attached to them were lost.</p>"
  ],
  [
    "<p>Improve error handling in <code class=\"filename\">contrib/adminpack</code>'s <code class=\"function\">pg_file_write()</code> function (Noah\n          Misch)</p>",
    "<p>Notably, it failed to detect errors reported by\n          <code class=\"function\">fclose()</code>.</p>"
  ],
  [
    "<p>In <code class=\"filename\">contrib/dblink</code>, avoid\n          leaking the previous unnamed connection when establishing\n          a new unnamed connection (Joe Conway)</p>"
  ],
  [
    "<p>Fix <code class=\"filename\">contrib/pg_trgm</code>'s\n          extraction of trigrams from regular expressions (Tom\n          Lane)</p>",
    "<p>In some cases it would produce a broken data structure\n          that could never match anything, leading to GIN or GiST\n          indexscans that use a trigram index not finding any\n          matches to the regular expression.</p>"
  ],
  [
    "<p>In <code class=\"filename\">contrib/postgres_fdw</code>,\n          transmit query cancellation requests to the remote server\n          (Michael Paquier, Etsuro Fujita)</p>",
    "<p>Previously, a local query cancellation request did not\n          cause an already-sent remote query to terminate early.\n          This is a back-patch of work originally done for 9.6.</p>"
  ],
  [
    "<p>Support OpenSSL 1.1.0 (Heikki Linnakangas, Andreas\n          Karlsson, Tom Lane)</p>",
    "<p>This is a back-patch of work previously done in newer\n          branches; it's needed since many platforms are adopting\n          newer OpenSSL versions.</p>"
  ],
  [
    "<p>Support Tcl 8.6 in MSVC builds (&#xC1;lvaro Herrera)</p>"
  ],
  [
    "<p>Sync our copy of the timezone library with IANA\n          release tzcode2017b (Tom Lane)</p>",
    "<p>This fixes a bug affecting some DST transitions in\n          January 2038.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2017b for DST law\n          changes in Chile, Haiti, and Mongolia, plus historical\n          corrections for Ecuador, Kazakhstan, Liberia, and Spain.\n          Switch to numeric abbreviations for numerous time zones\n          in South America, the Pacific and Indian oceans, and some\n          Asian and Middle Eastern countries.</p>",
    "<p>The IANA time zone database previously provided\n          textual abbreviations for all time zones, sometimes\n          making up abbreviations that have little or no currency\n          among the local population. They are in process of\n          reversing that policy in favor of using numeric UTC\n          offsets in zones where there is no evidence of real-world\n          use of an English abbreviation. At least for the time\n          being, <span class=\"productname\">PostgreSQL</span> will\n          continue to accept such removed abbreviations for\n          timestamp input. But they will not be shown in the\n          <code class=\"structname\">pg_timezone_names</code> view\n          nor used for output.</p>"
  ],
  [
    "<p>Use correct daylight-savings rules for POSIX-style\n          time zone names in MSVC builds (David Rowley)</p>",
    "<p>The Microsoft MSVC build scripts neglected to install\n          the <code class=\"filename\">posixrules</code> file in the\n          timezone directory tree. This resulted in the timezone\n          code falling back to its built-in rule about what DST\n          behavior to assume for a POSIX-style time zone name. For\n          historical reasons that still corresponds to the DST\n          rules the USA was using before 2007 (i.e., change on\n          first Sunday in April and last Sunday in October). With\n          this fix, a POSIX-style zone name will use the current\n          and historical DST transition dates of the <code class=\"literal\">US/Eastern</code> zone. If you don't want that,\n          remove the <code class=\"filename\">posixrules</code> file,\n          or replace it with a copy of some other zone file (see\n          <a class=\"xref\" href=\"https://www.postgresql.org/docs/current/static/datatype-datetime.html#DATATYPE-TIMEZONES\" title=\"8.5.3.&#xA0;Time Zones\">Section&#xA0;8.5.3</a>). Note\n          that due to caching, you may need to restart the server\n          to get such changes to take effect.</p>"
  ]
]