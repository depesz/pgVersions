[
  [
    "<p>Fix character string comparison for Windows locales\n          that consider different character combinations as equal\n          (Tom)</p>",
    "<p>This fix applies only on Windows and only when using\n          UTF-8 database encoding. The same fix was made for all\n          other cases over two years ago, but Windows with UTF-8\n          uses a separate code path that was not updated. If you\n          are using a locale that considers some non-identical\n          strings as equal, you may need to <code class=\"command\">REINDEX</code> to fix existing indexes on\n          textual columns.</p>"
  ],
  [
    "<p>Repair potential deadlock between concurrent\n          <code class=\"command\">VACUUM FULL</code> operations on\n          different system catalogs (Tom)</p>"
  ],
  [
    "<p>Fix longstanding <code class=\"command\">LISTEN</code>/<code class=\"command\">NOTIFY</code> race condition (Tom)</p>",
    "<p>In rare cases a session that had just executed a\n          <code class=\"command\">LISTEN</code> might not get a\n          notification, even though one would be expected because\n          the concurrent transaction executing <code class=\"command\">NOTIFY</code> was observed to commit later.</p>",
    "<p>A side effect of the fix is that a transaction that\n          has executed a not-yet-committed <code class=\"command\">LISTEN</code> command will not see any row in\n          <code class=\"structname\">pg_listener</code> for the\n          <code class=\"command\">LISTEN</code>, should it choose to\n          look; formerly it would have. This behavior was never\n          documented one way or the other, but it is possible that\n          some applications depend on the old behavior.</p>"
  ],
  [
    "<p>Disallow <code class=\"command\">LISTEN</code> and\n          <code class=\"command\">UNLISTEN</code> within a prepared\n          transaction (Tom)</p>",
    "<p>This was formerly allowed but trying to do it had\n          various unpleasant consequences, notably that the\n          originating backend could not exit as long as an\n          <code class=\"command\">UNLISTEN</code> remained\n          uncommitted.</p>"
  ],
  [
    "<p>Disallow dropping a temporary table within a prepared\n          transaction (Heikki)</p>",
    "<p>This was correctly disallowed by 8.1, but the check\n          was inadvertently broken in 8.2.</p>"
  ],
  [
    "<p>Fix rare crash when an error occurs during a query\n          using a hash index (Heikki)</p>"
  ],
  [
    "<p>Fix memory leaks in certain usages of set-returning\n          functions (Neil)</p>"
  ],
  [
    "<p>Fix input of datetime values for February 29 in years\n          BC (Tom)</p>",
    "<p>The former coding was mistaken about which years were\n          leap years.</p>"
  ],
  [
    "<p>Fix <span class=\"quote\">&#x201C;<span class=\"quote\">unrecognized node type</span>&#x201D;</span> error in\n          some variants of <code class=\"command\">ALTER OWNER</code>\n          (Tom)</p>"
  ],
  [
    "<p>Ensure <code class=\"structname\">pg_stat_activity</code>.<code class=\"structfield\">waiting</code> flag is cleared when a lock\n          wait is aborted (Tom)</p>"
  ],
  [
    "<p>Fix handling of process permissions on Windows Vista\n          (Dave, Magnus)</p>",
    "<p>In particular, this fix allows starting the server as\n          the Administrator user.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2008a (in particular,\n          recent Chile changes); adjust timezone abbreviation\n          <code class=\"literal\">VET</code> (Venezuela) to mean\n          UTC-4:30, not UTC-4:00 (Tom)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_ctl</span> to\n          correctly extract the postmaster's port number from\n          command-line options (Itagaki Takahiro, Tom)</p>",
    "<p>Previously, <code class=\"literal\">pg_ctl start\n          -w</code> could try to contact the postmaster on the\n          wrong port, leading to bogus reports of startup\n          failure.</p>"
  ],
  [
    "<p>Use <code class=\"option\">-fwrapv</code> to defend\n          against possible misoptimization in recent <span class=\"application\">gcc</span> versions (Tom)</p>",
    "<p>This is known to be necessary when building\n          <span class=\"productname\">PostgreSQL</span> with\n          <span class=\"application\">gcc</span> 4.3 or later.</p>"
  ],
  [
    "<p>Correctly enforce <code class=\"varname\">statement_timeout</code> values longer than\n          <code class=\"literal\">INT_MAX</code> microseconds (about\n          35 minutes) (Tom)</p>",
    "<p>This bug affects only builds with <code class=\"option\">--enable-integer-datetimes</code>.</p>"
  ],
  [
    "<p>Fix <span class=\"quote\">&#x201C;<span class=\"quote\">unexpected PARAM_SUBLINK ID</span>&#x201D;</span>\n          planner error when constant-folding simplifies a\n          sub-select (Tom)</p>"
  ],
  [
    "<p>Fix logical errors in constraint-exclusion handling of\n          <code class=\"literal\">IS NULL</code> and <code class=\"literal\">NOT</code> expressions (Tom)</p>",
    "<p>The planner would sometimes exclude partitions that\n          should not have been excluded because of the possibility\n          of NULL results.</p>"
  ],
  [
    "<p>Fix another cause of <span class=\"quote\">&#x201C;<span class=\"quote\">failed to build any N-way joins</span>&#x201D;</span>\n          planner errors (Tom)</p>",
    "<p>This could happen in cases where a clauseless join\n          needed to be forced before a join clause could be\n          exploited.</p>"
  ],
  [
    "<p>Fix incorrect constant propagation in outer-join\n          planning (Tom)</p>",
    "<p>The planner could sometimes incorrectly conclude that\n          a variable could be constrained to be equal to a\n          constant, leading to wrong query results.</p>"
  ],
  [
    "<p>Fix display of constant expressions in <code class=\"literal\">ORDER BY</code> and <code class=\"literal\">GROUP\n          BY</code> (Tom)</p>",
    "<p>An explicitly casted constant would be shown\n          incorrectly. This could for example lead to corruption of\n          a view definition during dump and reload.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">libpq</span> to handle\n          NOTICE messages correctly during COPY OUT (Tom)</p>",
    "<p>This failure has only been observed to occur when a\n          user-defined datatype's output routine issues a NOTICE,\n          but there is no guarantee it couldn't happen due to other\n          causes.</p>"
  ]
]