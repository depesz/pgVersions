[
  [
    "<p>Fix bug that allowed any logged-in user to\n          <code class=\"command\">SET ROLE</code> to any other\n          database user id (CVE-2006-0553)</p>",
    "<p>Due to inadequate validity checking, a user could\n          exploit the special case that <code class=\"command\">SET\n          ROLE</code> normally uses to restore the previous role\n          setting after an error. This allowed ordinary users to\n          acquire superuser status, for example. The\n          escalation-of-privilege risk exists only in 8.1.0-8.1.2.\n          However, in all releases back to 7.3 there is a related\n          bug in <code class=\"command\">SET SESSION\n          AUTHORIZATION</code> that allows unprivileged users to\n          crash the server, if it has been compiled with Asserts\n          enabled (which is not the default). Thanks to Akio Ishida\n          for reporting this problem.</p>"
  ],
  [
    "<p>Fix bug with row visibility logic in self-inserted\n          rows (Tom)</p>",
    "<p>Under rare circumstances a row inserted by the current\n          command could be seen as already valid, when it should\n          not be. Repairs bug created in 8.0.4, 7.4.9, and 7.3.11\n          releases.</p>"
  ],
  [
    "<p>Fix race condition that could lead to <span class=\"quote\">&#x201C;<span class=\"quote\">file already\n          exists</span>&#x201D;</span> errors during pg_clog and\n          pg_subtrans file creation (Tom)</p>"
  ],
  [
    "<p>Fix cases that could lead to crashes if a\n          cache-invalidation message arrives at just the wrong time\n          (Tom)</p>"
  ],
  [
    "<p>Properly check <code class=\"literal\">DOMAIN</code>\n          constraints for <code class=\"literal\">UNKNOWN</code>\n          parameters in prepared statements (Neil)</p>"
  ],
  [
    "<p>Ensure <code class=\"command\">ALTER COLUMN TYPE</code>\n          will process <code class=\"literal\">FOREIGN KEY</code>,\n          <code class=\"literal\">UNIQUE</code>, and <code class=\"literal\">PRIMARY KEY</code> constraints in the proper\n          order (Nakano Yoshihisa)</p>"
  ],
  [
    "<p>Fixes to allow restoring dumps that have cross-schema\n          references to custom operators or operator classes\n          (Tom)</p>"
  ],
  [
    "<p>Allow <span class=\"application\">pg_restore</span> to\n          continue properly after a <code class=\"command\">COPY</code> failure; formerly it tried to treat\n          the remaining <code class=\"command\">COPY</code> data as\n          SQL commands (Stephen Frost)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_ctl</span>\n          <code class=\"literal\">unregister</code> crash when the\n          data directory is not specified (Magnus)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">libpq</span>\n          <code class=\"function\">PQprint</code> HTML tags\n          (Christoph Zwerschke)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">ecpg</span> crash on\n          AMD64 and PPC (Neil)</p>"
  ],
  [
    "<p>Allow <code class=\"literal\">SETOF</code> and\n          <code class=\"literal\">%TYPE</code> to be used together in\n          function result type declarations</p>"
  ],
  [
    "<p>Recover properly if error occurs during argument\n          passing in <span class=\"application\">PL/Python</span>\n          (Neil)</p>"
  ],
  [
    "<p>Fix memory leak in <code class=\"function\">plperl_return_next</code> (Neil)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">PL/Perl</span>'s\n          handling of locales on Win32 to match the backend\n          (Andrew)</p>"
  ],
  [
    "<p>Various optimizer fixes (Tom)</p>"
  ],
  [
    "<p>Fix crash when <code class=\"literal\">log_min_messages</code> is set to <code class=\"literal\">DEBUG3</code> or above in <code class=\"filename\">postgresql.conf</code> on Win32 (Bruce)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pgxs</span> <code class=\"literal\">-L</code> library path specification for Win32,\n          Cygwin, macOS, AIX (Bruce)</p>"
  ],
  [
    "<p>Check that SID is enabled while checking for Win32\n          admin privileges (Magnus)</p>"
  ],
  [
    "<p>Properly reject out-of-range date inputs (Kris\n          Jurka)</p>"
  ],
  [
    "<p>Portability fix for testing presence of <code class=\"function\">finite</code> and <code class=\"function\">isinf</code> during configure (Tom)</p>"
  ],
  [
    "<p>Improve speed of <code class=\"command\">COPY IN</code>\n          via libpq, by avoiding a kernel call per data line (Alon\n          Goldshuv)</p>"
  ],
  [
    "<p>Improve speed of <code class=\"filename\">/contrib/tsearch2</code> index creation\n          (Tom)</p>"
  ]
]