[
  [
    "<p>Prevent execution of <code class=\"function\">enum_recv</code> from SQL (Tom Lane)</p>",
    "<p>The function was misdeclared, allowing a simple SQL\n          command to crash the server. In principle an attacker\n          might be able to use it to examine the contents of server\n          memory. Our thanks to Sumit Soni (via Secunia SVCRP) for\n          reporting this issue. (CVE-2013-0255)</p>"
  ],
  [
    "<p>Fix multiple problems in detection of when a\n          consistent database state has been reached during WAL\n          replay (Fujii Masao, Heikki Linnakangas, Simon Riggs,\n          Andres Freund)</p>"
  ],
  [
    "<p>Update minimum recovery point when truncating a\n          relation file (Heikki Linnakangas)</p>",
    "<p>Once data has been discarded, it's no longer safe to\n          stop recovery at an earlier point in the timeline.</p>"
  ],
  [
    "<p>Fix recycling of WAL segments after changing recovery\n          target timeline (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Fix missing cancellations in hot standby mode (Noah\n          Misch, Simon Riggs)</p>",
    "<p>The need to cancel conflicting hot-standby queries\n          would sometimes be missed, allowing those queries to see\n          inconsistent data.</p>"
  ],
  [
    "<p>Prevent recovery pause feature from pausing before\n          users can connect (Tom Lane)</p>"
  ],
  [
    "<p>Fix SQL grammar to allow subscripting or field\n          selection from a sub-SELECT result (Tom Lane)</p>"
  ],
  [
    "<p>Fix performance problems with autovacuum truncation in\n          busy workloads (Jan Wieck)</p>",
    "<p>Truncation of empty pages at the end of a table\n          requires exclusive lock, but autovacuum was coded to fail\n          (and release the table lock) when there are conflicting\n          lock requests. Under load, it is easily possible that\n          truncation would never occur, resulting in table bloat.\n          Fix by performing a partial truncation, releasing the\n          lock, then attempting to re-acquire the lock and\n          continue. This fix also greatly reduces the average time\n          before autovacuum releases the lock after a conflicting\n          request arrives.</p>"
  ],
  [
    "<p>Protect against race conditions when scanning\n          <code class=\"structname\">pg_tablespace</code> (Stephen\n          Frost, Tom Lane)</p>",
    "<p><code class=\"command\">CREATE DATABASE</code> and\n          <code class=\"command\">DROP DATABASE</code> could\n          misbehave if there were concurrent updates of\n          <code class=\"structname\">pg_tablespace</code>\n          entries.</p>"
  ],
  [
    "<p>Prevent <code class=\"command\">DROP OWNED</code> from\n          trying to drop whole databases or tablespaces (&#xC1;lvaro\n          Herrera)</p>",
    "<p>For safety, ownership of these objects must be\n          reassigned, not dropped.</p>"
  ],
  [
    "<p>Fix error in <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-client.html#GUC-VACUUM-FREEZE-TABLE-AGE\"><code class=\"varname\">\n          vacuum_freeze_table_age</code></a> implementation (Andres\n          Freund)</p>",
    "<p>In installations that have existed for more than\n          <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-client.html#GUC-VACUUM-FREEZE-MIN-AGE\"><code class=\"varname\">\n          vacuum_freeze_min_age</code></a> transactions, this\n          mistake prevented autovacuum from using partial-table\n          scans, so that a full-table scan would always happen\n          instead.</p>"
  ],
  [
    "<p>Prevent misbehavior when a <code class=\"symbol\">RowExpr</code> or <code class=\"symbol\">XmlExpr</code> is parse-analyzed twice (Andres\n          Freund, Tom Lane)</p>",
    "<p>This mistake could be user-visible in contexts such as\n          <code class=\"literal\">CREATE TABLE LIKE INCLUDING\n          INDEXES</code>.</p>"
  ],
  [
    "<p>Improve defenses against integer overflow in hashtable\n          sizing calculations (Jeff Davis)</p>"
  ],
  [
    "<p>Fix failure to ignore leftover temporary tables after\n          a server crash (Tom Lane)</p>"
  ],
  [
    "<p>Reject out-of-range dates in <code class=\"function\">to_date()</code> (Hitoshi Harada)</p>"
  ],
  [
    "<p>Fix <code class=\"function\">pg_extension_config_dump()</code> to handle\n          extension-update cases properly (Tom Lane)</p>",
    "<p>This function will now replace any existing entry for\n          the target table, making it usable in extension update\n          scripts.</p>"
  ],
  [
    "<p>Fix PL/Python's handling of functions used as triggers\n          on multiple tables (Andres Freund)</p>"
  ],
  [
    "<p>Ensure that non-ASCII prompt strings are translated to\n          the correct code page on Windows (Alexander Law, Noah\n          Misch)</p>",
    "<p>This bug affected <span class=\"application\">psql</span> and some other client\n          programs.</p>"
  ],
  [
    "<p>Fix possible crash in <span class=\"application\">psql</span>'s <code class=\"command\">\\?</code> command when not connected to a\n          database (Meng Qingzhong)</p>"
  ],
  [
    "<p>Fix possible error if a relation file is removed while\n          <span class=\"application\">pg_basebackup</span> is running\n          (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Make <span class=\"application\">pg_dump</span> exclude\n          data of unlogged tables when running on a hot-standby\n          server (Magnus Hagander)</p>",
    "<p>This would fail anyway because the data is not\n          available on the standby server, so it seems most\n          convenient to assume <code class=\"option\">--no-unlogged-table-data</code>\n          automatically.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_upgrade</span> to\n          deal with invalid indexes safely (Bruce Momjian)</p>"
  ],
  [
    "<p>Fix one-byte buffer overrun in <span class=\"application\">libpq</span>'s <code class=\"function\">PQprintTuples</code> (Xi Wang)</p>",
    "<p>This ancient function is not used anywhere by\n          <span class=\"productname\">PostgreSQL</span> itself, but\n          it might still be used by some client code.</p>"
  ],
  [
    "<p>Make <span class=\"application\">ecpglib</span> use\n          translated messages properly (Chen Huajun)</p>"
  ],
  [
    "<p>Properly install <span class=\"application\">ecpg_compat</span> and <span class=\"application\">pgtypes</span> libraries on MSVC (Jiang\n          Guiqing)</p>"
  ],
  [
    "<p>Include our version of <code class=\"function\">isinf()</code> in <span class=\"application\">libecpg</span> if it's not provided by the\n          system (Jiang Guiqing)</p>"
  ],
  [
    "<p>Rearrange configure's tests for supplied functions so\n          it is not fooled by bogus exports from\n          libedit/libreadline (Christoph Berg)</p>"
  ],
  [
    "<p>Ensure Windows build number increases over time\n          (Magnus Hagander)</p>"
  ],
  [
    "<p>Make <span class=\"application\">pgxs</span> build\n          executables with the right <code class=\"literal\">.exe</code> suffix when cross-compiling for\n          Windows (Zoltan Boszormenyi)</p>"
  ],
  [
    "<p>Add new timezone abbreviation <code class=\"literal\">FET</code> (Tom Lane)</p>",
    "<p>This is now used in some eastern-European time\n          zones.</p>"
  ]
]