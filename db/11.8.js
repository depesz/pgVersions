[
  [
    "<p>Propagate <code class=\"command\">ALTER TABLE ... SET STORAGE</code> to indexes (Peter Eisentraut)</p>",
    "<p>Non-expression index columns have always copied the <code class=\"structfield\">attstorage</code> property of their table column at creation. Update them when <code class=\"command\">ALTER TABLE ... SET STORAGE</code> is done, to maintain consistency.</p>"
  ],
  [
    "<p>Preserve the <code class=\"structfield\">indisclustered</code> setting of indexes rewritten by <code class=\"command\">ALTER TABLE</code> (Amit Langote, Justin Pryzby)</p>",
    "<p>Previously, <code class=\"command\">ALTER TABLE</code> lost track of which index had been used for <code class=\"command\">CLUSTER</code>.</p>"
  ],
  [
    "<p>Preserve the replica identity properties of indexes rewritten by <code class=\"command\">ALTER TABLE</code> (Quan Zongliang, Peter Eisentraut)</p>"
  ],
  [
    "<p>Lock objects sooner during <code class=\"command\">DROP OWNED BY</code> (Álvaro Herrera)</p>",
    "<p>This avoids failures in race-condition cases where another session is deleting some of the same objects.</p>"
  ],
  [
    "<p>Fix error-case processing for <code class=\"command\">CREATE ROLE ... IN ROLE</code> (Andrew Gierth)</p>",
    "<p>Some error cases would be reported as <span class=\"quote\">“<span class=\"quote\">unexpected node type</span>”</span> or the like, instead of the intended message.</p>"
  ],
  [
    "<p>Ensure that when a partition is detached, any triggers cloned from its formerly-parent table are removed (Justin Pryzby)</p>"
  ],
  [
    "<p>Ensure that unique indexes over partitioned tables match the equality semantics of the partitioning key (Guancheng Luo)</p>",
    "<p>This would only be an issue with index opclasses that have unusual notions of equality, but it's wrong in theory, so check.</p>"
  ],
  [
    "<p>Ensure that members of the <code class=\"literal\">pg_read_all_stats</code> role can read all statistics views, as expected (Magnus Hagander)</p>",
    "<p>The functions underlying the <code class=\"structname\">pg_stat_progress_*</code> views had not gotten this memo.</p>"
  ],
  [
    "<p>Repair performance regression in <code class=\"structname\">information_schema</code>.<code class=\"structname\">triggers</code> view (Tom Lane)</p>",
    "<p>This patch redefines that view so that an outer <code class=\"literal\">WHERE</code> clause constraining the table name can be pushed down into the view, allowing its calculations to be done only for triggers belonging to the table of interest rather than all triggers in the database. In a database with many triggers this would make a significant speed difference for queries of that form. Since things worked that way before v11, this is a potential performance regression. Users who find this to be a problem can fix it by replacing the view definition (or, perhaps, just deleting and reinstalling the whole <code class=\"structname\">information_schema</code> schema).</p>"
  ],
  [
    "<p>Fix full text search to handle NOT above a phrase search correctly (Tom Lane)</p>",
    "<p>Queries such as <code class=\"literal\">!(foo&lt;-&gt;bar)</code> failed to find matching rows when implemented as a GiST or GIN index search.</p>"
  ],
  [
    "<p>Fix full text search for cases where a phrase search includes an item with both prefix matching and a weight restriction (Tom Lane)</p>"
  ],
  [
    "<p>Fix <code class=\"function\">ts_headline()</code> to make better headline selections when working with phrase queries (Tom Lane)</p>"
  ],
  [
    "<p>Fix bugs in <code class=\"varname\">gin_fuzzy_search_limit</code> processing (Adé Heyward, Tom Lane)</p>",
    "<p>A small value of <code class=\"varname\">gin_fuzzy_search_limit</code> could result in unexpected slowness due to unintentionally rescanning the same index page many times. Another code path failed to apply the intended filtering at all, possibly returning too many values.</p>"
  ],
  [
    "<p>Allow input of type <code class=\"type\">circle</code> to accept the format <span class=\"quote\">“<span class=\"quote\"><code class=\"literal\">(<em class=\"replaceable\"><code>x</code></em>,<em class=\"replaceable\"><code>y</code></em>),<em class=\"replaceable\"><code>r</code></em></code></span>”</span> as the documentation says it does (David Zhang)</p>"
  ],
  [
    "<p>Make the <code class=\"function\">get_bit()</code> and <code class=\"function\">set_bit()</code> functions cope with <code class=\"type\">bytea</code> strings longer than 256MB (Movead Li)</p>",
    "<p>Since the bit number argument is only <code class=\"type\">int4</code>, it's impossible to use these functions to access bits beyond the first 256MB of a long <code class=\"type\">bytea</code>. We'll widen the argument to <code class=\"type\">int8</code> in v13, but in the meantime, allow these functions to work on the initial substring of a long <code class=\"type\">bytea</code>.</p>"
  ],
  [
    "<p>Ignore file-not-found errors in <code class=\"function\">pg_ls_waldir()</code> and allied functions (Tom Lane)</p>",
    "<p>This prevents a race condition failure if a file is removed between when we see its directory entry and when we attempt to <code class=\"function\">stat()</code> it.</p>"
  ],
  [
    "<p>Avoid possibly leaking an open-file descriptor for a directory in <code class=\"function\">pg_ls_dir()</code>, <code class=\"function\">pg_timezone_names()</code>, <code class=\"function\">pg_tablespace_databases()</code>, and allied functions (Justin Pryzby)</p>"
  ],
  [
    "<p>Fix polymorphic-function type resolution to correctly infer the actual type of an <code class=\"type\">anyarray</code> output when given only an <code class=\"type\">anyrange</code> input (Tom Lane)</p>"
  ],
  [
    "<p>Avoid leakage of a hashed subplan's hash tables across multiple executions (Andreas Karlsson, Tom Lane)</p>",
    "<p>This mistake could result in severe memory bloat if a query re-executed a hashed subplan enough times.</p>"
  ],
  [
    "<p>Avoid unlikely crash when <code class=\"command\">REINDEX</code> is terminated by a session-shutdown signal (Tom Lane)</p>"
  ],
  [
    "<p>Fix low-probability crash after constraint violation errors in partitioned tables (Andres Freund)</p>"
  ],
  [
    "<p>Prevent printout of possibly-incorrect hash join table statistics in <code class=\"command\">EXPLAIN</code> (Konstantin Knizhnik, Tom Lane, Thomas Munro)</p>"
  ],
  [
    "<p>Fix reporting of elapsed time for heap truncation steps in <code class=\"command\">VACUUM VERBOSE</code> (Tatsuhito Kasahara)</p>"
  ],
  [
    "<p>Fix possible undercounting of deleted B-tree index pages in <code class=\"command\">VACUUM VERBOSE</code> output (Peter Geoghegan)</p>"
  ],
  [
    "<p>Fix wrong bookkeeping for oldest deleted page in a B-tree index (Peter Geoghegan)</p>",
    "<p>This could cause subtly wrong decisions about when <code class=\"command\">VACUUM</code> can skip an index cleanup scan; although it appears there may be no significant user-visible effects from that.</p>"
  ],
  [
    "<p>Ensure that TimelineHistoryRead and TimelineHistoryWrite wait states are reported in all code paths that read or write timeline history files (Masahiro Ikeda)</p>"
  ],
  [
    "<p>Avoid possibly showing <span class=\"quote\">“<span class=\"quote\">waiting</span>”</span> twice in a process's PS status (Masahiko Sawada)</p>"
  ],
  [
    "<p>Avoid failure if autovacuum tries to access a just-dropped temporary schema (Tom Lane)</p>",
    "<p>This hazard only arises if a superuser manually drops a temporary schema; which isn't normal practice, but should work.</p>"
  ],
  [
    "<p>Avoid premature recycling of WAL segments during crash recovery (Jehan-Guillaume de Rorthais)</p>",
    "<p>WAL segments that become ready to be archived during crash recovery were potentially recycled without being archived.</p>"
  ],
  [
    "<p>Avoid scanning irrelevant timelines during archive recovery (Kyotaro Horiguchi)</p>",
    "<p>This can eliminate many attempts to fetch non-existent WAL files from archive storage, which is helpful if archive access is slow.</p>"
  ],
  [
    "<p>Remove bogus <span class=\"quote\">“<span class=\"quote\">subtransaction logged without previous top-level txn record</span>”</span> error check in logical decoding (Arseny Sher, Amit Kapila)</p>",
    "<p>This condition is legitimately reachable in various scenarios, so remove the check.</p>"
  ],
  [
    "<p>Ensure that a replication slot's <code class=\"literal\">io_in_progress_lock</code> is released in failure code paths (Pavan Deolasee)</p>",
    "<p>This could result in a walsender later becoming stuck waiting for the lock.</p>"
  ],
  [
    "<p>Fix race conditions in synchronous standby management (Tom Lane)</p>",
    "<p>During a change in the <code class=\"varname\">synchronous_standby_names</code> setting, there was a window in which wrong decisions could be made about whether it is OK to release transactions that are waiting for synchronous commit. Another hazard for similarly wrong decisions existed if a walsender process exited and was immediately replaced by another.</p>"
  ],
  [
    "<p>Ensure <code class=\"varname\">nextXid</code> can't go backwards on a standby server (Eka Palamadai)</p>",
    "<p>This race condition could allow incorrect hot standby feedback messages to be sent back to the primary server, potentially allowing <code class=\"command\">VACUUM</code> to run too soon on the primary.</p>"
  ],
  [
    "<p>Add missing SQLSTATE values to a few error reports (Sawada Masahiko)</p>"
  ],
  [
    "<p>Fix PL/pgSQL to reliably refuse to execute an event trigger function as a plain function (Tom Lane)</p>"
  ],
  [
    "<p>Fix memory leak in <span class=\"application\">libpq</span> when using <code class=\"literal\">sslmode=verify-full</code> (Roman Peshkurov)</p>",
    "<p>Certificate verification during connection startup could leak some memory. This would become an issue if a client process opened many database connections during its lifetime.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">ecpg</span> to treat an argument of just <span class=\"quote\">“<span class=\"quote\"><code class=\"literal\">-</code></span>”</span> as meaning <span class=\"quote\">“<span class=\"quote\">read from stdin</span>”</span> on all platforms (Tom Lane)</p>"
  ],
  [
    "<p>Allow tab-completion of the filename argument to <span class=\"application\">psql</span>'s <code class=\"command\">\\gx</code> command (Vik Fearing)</p>"
  ],
  [
    "<p>Add <span class=\"application\">pg_dump</span> support for <code class=\"command\">ALTER ... DEPENDS ON EXTENSION</code> (Álvaro Herrera)</p>",
    "<p><span class=\"application\">pg_dump</span> previously ignored dependencies added this way, causing them to be forgotten during dump/restore or <span class=\"application\">pg_upgrade</span>.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_dump</span> to dump comments on RLS policy objects (Tom Lane)</p>"
  ],
  [
    "<p>In <span class=\"application\">pg_dump</span>, postpone restore of event triggers till the end (Fabrízio de Royes Mello, Hamid Akhtar, Tom Lane)</p>",
    "<p>This minimizes the risk that an event trigger could interfere with the restoration of other objects.</p>"
  ],
  [
    "<p>Make <span class=\"application\">pg_verify_checksums</span> skip tablespace subdirectories that belong to a different <span class=\"productname\">PostgreSQL</span> major version (Michael Banck, Bernd Helmle)</p>",
    "<p>Such subdirectories don't really belong to our database cluster, and so must not be processed.</p>"
  ],
  [
    "<p>Ignore temporary copies of <code class=\"filename\">pg_internal.init</code> in <span class=\"application\">pg_verify_checksums</span> and related programs (Michael Paquier)</p>"
  ],
  [
    "<p>Fix quoting of <code class=\"option\">--encoding</code>, <code class=\"option\">--lc-ctype</code> and <code class=\"option\">--lc-collate</code> values in <span class=\"application\">createdb</span> utility (Michael Paquier)</p>"
  ],
  [
    "<p><code class=\"filename\">contrib/lo</code>'s <code class=\"function\">lo_manage()</code> function crashed if called directly rather than as a trigger (Tom Lane)</p>"
  ],
  [
    "<p>In <code class=\"filename\">contrib/ltree</code>, protect against overflow of <code class=\"type\">ltree</code> and <code class=\"type\">lquery</code> length fields (Nikita Glukhov)</p>"
  ],
  [
    "<p>Work around failure in <code class=\"filename\">contrib/pageinspect</code>'s <code class=\"function\">bt_metap()</code> function when an oldest_xact value exceeds 2^31-1 (Peter Geoghegan)</p>",
    "<p>Such XIDs will now be reported as negative integers, which isn't great but it beats throwing an error. v13 will widen the output argument to <code class=\"type\">int8</code> to provide saner reporting.</p>"
  ],
  [
    "<p>Fix cache reference leak in <code class=\"filename\">contrib/sepgsql</code> (Michael Luo)</p>"
  ],
  [
    "<p>Avoid failures when dealing with Unix-style locale names on Windows (Juan José Santamaría Flecha)</p>"
  ],
  [
    "<p>Use <span class=\"application\">pkg-config</span>, if available, to locate <span class=\"application\">libxml2</span> during <span class=\"application\">configure</span> (Hugh McMaster, Tom Lane, Peter Eisentraut)</p>",
    "<p>If <span class=\"application\">pkg-config</span> is not present or lacks knowledge of <span class=\"application\">libxml2</span>, we still query <span class=\"application\">xml2-config</span> as before.</p>",
    "<p>This change could break build processes that try to make <span class=\"productname\">PostgreSQL</span> use a non-default version of <span class=\"application\">libxml2</span> by putting that version's <span class=\"application\">xml2-config</span> into the <code class=\"varname\">PATH</code>. Instead, set <code class=\"varname\">XML2_CONFIG</code> to point to the non-default <span class=\"application\">xml2-config</span>. That method will work with either older or newer <span class=\"productname\">PostgreSQL</span> releases.</p>"
  ],
  [
    "<p>In MSVC builds, cope with spaces in the path name for Python (Victor Wagner)</p>"
  ],
  [
    "<p>In MSVC builds, fix detection of Visual Studio version to work with more language settings (Andrew Dunstan)</p>"
  ],
  [
    "<p>In MSVC builds, use <code class=\"literal\">-Wno-deprecated</code> with bison versions newer than 3.0, as non-Windows builds already do (Andrew Dunstan)</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2020a for DST law changes in Morocco and the Canadian Yukon, plus historical corrections for Shanghai.</p>",
    "<p>The America/Godthab zone has been renamed to America/Nuuk to reflect current English usage; however, the old name remains available as a compatibility link.</p>",
    "<p>Also, update <span class=\"application\">initdb</span>'s list of known Windows time zone names to include recent additions, improving the odds that it will correctly translate the system time zone setting on that platform.</p>"
  ]
]