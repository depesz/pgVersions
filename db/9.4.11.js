[
  [
    "<p>Fix a race condition that could cause indexes built with <tt class=\"COMMAND\">CREATE INDEX CONCURRENTLY</tt> to be corrupt (Pavan Deolasee, Tom Lane)</p>",
    "<p>If <tt class=\"COMMAND\">CREATE INDEX CONCURRENTLY</tt> was used to build an index that depends on a column not previously indexed, then rows updated by transactions that ran concurrently with the <tt class=\"COMMAND\">CREATE INDEX</tt> command could have received incorrect index entries. If you suspect this may have happened, the most reliable solution is to rebuild affected indexes after installing this update.</p>"
  ],
  [
    "<p>Ensure that the special snapshot used for catalog scans is not invalidated by premature data pruning (Tom Lane)</p>",
    "<p>Backends failed to account for this snapshot when advertising their oldest xmin, potentially allowing concurrent vacuuming operations to remove data that was still needed. This led to transient failures along the lines of <span class=\"QUOTE\">\"cache lookup failed for relation 1255\"</span>.</p>"
  ],
  [
    "<p>Unconditionally WAL-log creation of the <span class=\"QUOTE\">\"init fork\"</span> for an unlogged table (Michael Paquier)</p>",
    "<p>Previously, this was skipped when <a href=\"https://www.postgresql.org/docs/9.4/runtime-config-wal.html#GUC-WAL-LEVEL\">wal_level</a> = <tt class=\"LITERAL\">minimal</tt>, but actually it's necessary even in that case to ensure that the unlogged table is properly reset to empty after a crash.</p>"
  ],
  [
    "<p>Reduce interlocking on standby servers during the replay of btree index vacuuming operations (Simon Riggs)</p>",
    "<p>This change avoids substantial replication delays that sometimes occurred while replaying such operations.</p>"
  ],
  [
    "<p>If the stats collector dies during hot standby, restart it (Takayuki Tsunakawa)</p>"
  ],
  [
    "<p>Ensure that hot standby feedback works correctly when it's enabled at standby server start (Ants Aasma, Craig Ringer)</p>"
  ],
  [
    "<p>Check for interrupts while hot standby is waiting for a conflicting query (Simon Riggs)</p>"
  ],
  [
    "<p>Avoid constantly respawning the autovacuum launcher in a corner case (Amit Khandekar)</p>",
    "<p>This fix avoids problems when autovacuum is nominally off and there are some tables that require freezing, but all such tables are already being processed by autovacuum workers.</p>"
  ],
  [
    "<p>Fix check for when an extension member object can be dropped (Tom Lane)</p>",
    "<p>Extension upgrade scripts should be able to drop member objects, but this was disallowed for serial-column sequences, and possibly other cases.</p>"
  ],
  [
    "<p>Make sure <tt class=\"COMMAND\">ALTER TABLE</tt> preserves index tablespace assignments when rebuilding indexes (Tom Lane, Michael Paquier)</p>",
    "<p>Previously, non-default settings of <a href=\"https://www.postgresql.org/docs/9.4/runtime-config-client.html#GUC-DEFAULT-TABLESPACE\">default_tablespace</a> could result in broken indexes.</p>"
  ],
  [
    "<p>Fix incorrect updating of trigger function properties when changing a foreign-key constraint's deferrability properties with <tt class=\"COMMAND\">ALTER TABLE ... ALTER CONSTRAINT</tt> (Tom Lane)</p>",
    "<p>This led to odd failures during subsequent exercise of the foreign key, as the triggers were fired at the wrong times.</p>"
  ],
  [
    "<p>Prevent dropping a foreign-key constraint if there are pending trigger events for the referenced relation (Tom Lane)</p>",
    "<p>This avoids <span class=\"QUOTE\">\"could not find trigger <tt class=\"REPLACEABLE c2\">NNN</tt>\"</span> or <span class=\"QUOTE\">\"relation <tt class=\"REPLACEABLE c2\">NNN</tt> has no triggers\"</span> errors.</p>"
  ],
  [
    "<p>Fix processing of OID column when a table with OIDs is associated to a parent with OIDs via <tt class=\"COMMAND\">ALTER TABLE ... INHERIT</tt> (Amit Langote)</p>",
    "<p>The OID column should be treated the same as regular user columns in this case, but it wasn't, leading to odd behavior in later inheritance changes.</p>"
  ],
  [
    "<p>Fix <tt class=\"COMMAND\">CREATE OR REPLACE VIEW</tt> to update the view query before attempting to apply the new view options (Dean Rasheed)</p>",
    "<p>Previously the command would fail if the new options were inconsistent with the old view definition.</p>"
  ],
  [
    "<p>Report correct object identity during <tt class=\"COMMAND\">ALTER TEXT SEARCH CONFIGURATION</tt> (Artur Zakirov)</p>",
    "<p>The wrong catalog OID was reported to extensions such as logical decoding.</p>"
  ],
  [
    "<p>Check for serializability conflicts before reporting constraint-violation failures (Thomas Munro)</p>",
    "<p>When using serializable transaction isolation, it is desirable that any error due to concurrent transactions should manifest as a serialization failure, thereby cueing the application that a retry might succeed. Unfortunately, this does not reliably happen for duplicate-key failures caused by concurrent insertions. This change ensures that such an error will be reported as a serialization error if the application explicitly checked for the presence of a conflicting key (and did not find it) earlier in the transaction.</p>"
  ],
  [
    "<p>Prevent multicolumn expansion of <tt class=\"REPLACEABLE c2\">foo</tt><tt class=\"LITERAL\">.*</tt> in an <tt class=\"COMMAND\">UPDATE</tt> source expression (Tom Lane)</p>",
    "<p>This led to <span class=\"QUOTE\">\"UPDATE target count mismatch --- internal error\"</span>. Now the syntax is understood as a whole-row variable, as it would be in other contexts.</p>"
  ],
  [
    "<p>Ensure that column typmods are determined accurately for multi-row <tt class=\"LITERAL\">VALUES</tt> constructs (Tom Lane)</p>",
    "<p>This fixes problems occurring when the first value in a column has a determinable typmod (e.g., length for a <tt class=\"TYPE\">varchar</tt> value) but later values don't share the same limit.</p>"
  ],
  [
    "<p>Throw error for an unfinished Unicode surrogate pair at the end of a Unicode string (Tom Lane)</p>",
    "<p>Normally, a Unicode surrogate leading character must be followed by a Unicode surrogate trailing character, but the check for this was missed if the leading character was the last character in a Unicode string literal (<tt class=\"LITERAL\">U&amp;'...'</tt>) or Unicode identifier (<tt class=\"LITERAL\">U&amp;\"...\"</tt>).</p>"
  ],
  [
    "<p>Ensure that a purely negative text search query, such as <tt class=\"LITERAL\">!foo</tt>, matches empty <tt class=\"TYPE\">tsvector</tt>s (Tom Dunstan)</p>",
    "<p>Such matches were found by GIN index searches, but not by sequential scans or GiST index searches.</p>"
  ],
  [
    "<p>Prevent crash when <code class=\"FUNCTION\">ts_rewrite()</code> replaces a non-top-level subtree with an empty query (Artur Zakirov)</p>"
  ],
  [
    "<p>Fix performance problems in <code class=\"FUNCTION\">ts_rewrite()</code> (Tom Lane)</p>"
  ],
  [
    "<p>Fix <code class=\"FUNCTION\">ts_rewrite()</code>'s handling of nested NOT operators (Tom Lane)</p>"
  ],
  [
    "<p>Fix <code class=\"FUNCTION\">array_fill()</code> to handle empty arrays properly (Tom Lane)</p>"
  ],
  [
    "<p>Fix one-byte buffer overrun in <code class=\"FUNCTION\">quote_literal_cstr()</code> (Heikki Linnakangas)</p>",
    "<p>The overrun occurred only if the input consisted entirely of single quotes and/or backslashes.</p>"
  ],
  [
    "<p>Prevent multiple calls of <code class=\"FUNCTION\">pg_start_backup()</code> and <code class=\"FUNCTION\">pg_stop_backup()</code> from running concurrently (Michael Paquier)</p>",
    "<p>This avoids an assertion failure, and possibly worse things, if someone tries to run these functions in parallel.</p>"
  ],
  [
    "<p>Avoid discarding <tt class=\"TYPE\">interval</tt>-to-<tt class=\"TYPE\">interval</tt> casts that aren't really no-ops (Tom Lane)</p>",
    "<p>In some cases, a cast that should result in zeroing out low-order <tt class=\"TYPE\">interval</tt> fields was mistakenly deemed to be a no-op and discarded. An example is that casting from <tt class=\"TYPE\">INTERVAL MONTH</tt> to <tt class=\"TYPE\">INTERVAL YEAR</tt> failed to clear the months field.</p>"
  ],
  [
    "<p>Ensure that cached plans are invalidated by changes in foreign-table options (Amit Langote, Etsuro Fujita, Ashutosh Bapat)</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_dump</span> to dump user-defined casts and transforms that use built-in functions (Stephen Frost)</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_restore</span> with <tt class=\"OPTION\">--create --if-exists</tt> to behave more sanely if an archive contains unrecognized <tt class=\"COMMAND\">DROP</tt> commands (Tom Lane)</p>",
    "<p>This doesn't fix any live bug, but it may improve the behavior in future if <span class=\"APPLICATION\">pg_restore</span> is used with an archive generated by a later <span class=\"APPLICATION\">pg_dump</span> version.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_basebackup</span>'s rate limiting in the presence of slow I/O (Antonin Houska)</p>",
    "<p>If disk I/O was transiently much slower than the specified rate limit, the calculation overflowed, effectively disabling the rate limit for the rest of the run.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_basebackup</span>'s handling of symlinked <tt class=\"FILENAME\">pg_stat_tmp</tt> and <tt class=\"FILENAME\">pg_replslot</tt> subdirectories (Magnus Hagander, Michael Paquier)</p>"
  ],
  [
    "<p>Fix possible <span class=\"APPLICATION\">pg_basebackup</span> failure on standby server when including WAL files (Amit Kapila, Robert Haas)</p>"
  ],
  [
    "<p>Ensure that the Python exception objects we create for PL/Python are properly reference-counted (Rafa de la Torre, Tom Lane)</p>",
    "<p>This avoids failures if the objects are used after a Python garbage collection cycle has occurred.</p>"
  ],
  [
    "<p>Fix PL/Tcl to support triggers on tables that have <tt class=\"LITERAL\">.tupno</tt> as a column name (Tom Lane)</p>",
    "<p>This matches the (previously undocumented) behavior of PL/Tcl's <tt class=\"COMMAND\">spi_exec</tt> and <tt class=\"COMMAND\">spi_execp</tt> commands, namely that a magic <tt class=\"LITERAL\">.tupno</tt> column is inserted only if there isn't a real column named that.</p>"
  ],
  [
    "<p>Allow DOS-style line endings in <tt class=\"FILENAME\">~/.pgpass</tt> files, even on Unix (Vik Fearing)</p>",
    "<p>This change simplifies use of the same password file across Unix and Windows machines.</p>"
  ],
  [
    "<p>Fix one-byte buffer overrun if <span class=\"APPLICATION\">ecpg</span> is given a file name that ends with a dot (Takayuki Tsunakawa)</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">psql</span>'s tab completion for <tt class=\"COMMAND\">ALTER DEFAULT PRIVILEGES</tt> (Gilles Darold, Stephen Frost)</p>"
  ],
  [
    "<p>In <span class=\"APPLICATION\">psql</span>, treat an empty or all-blank setting of the <tt class=\"ENVAR\">PAGER</tt> environment variable as meaning <span class=\"QUOTE\">\"no pager\"</span> (Tom Lane)</p>",
    "<p>Previously, such a setting caused output intended for the pager to vanish entirely.</p>"
  ],
  [
    "<p>Improve <tt class=\"FILENAME\">contrib/dblink</tt>'s reporting of low-level <span class=\"APPLICATION\">libpq</span> errors, such as out-of-memory (Joe Conway)</p>"
  ],
  [
    "<p>Teach <tt class=\"FILENAME\">contrib/dblink</tt> to ignore irrelevant server options when it uses a <tt class=\"FILENAME\">contrib/postgres_fdw</tt> foreign server as the source of connection options (Corey Huinker)</p>",
    "<p>Previously, if the foreign server object had options that were not also <span class=\"APPLICATION\">libpq</span> connection options, an error occurred.</p>"
  ],
  [
    "<p>On Windows, ensure that environment variable changes are propagated to DLLs built with debug options (Christian Ullrich)</p>"
  ],
  [
    "<p>Sync our copy of the timezone library with IANA release tzcode2016j (Tom Lane)</p>",
    "<p>This fixes various issues, most notably that timezone data installation failed if the target directory didn't support hard links.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"APPLICATION\">tzdata</span> release 2016j for DST law changes in northern Cyprus (adding a new zone Asia/Famagusta), Russia (adding a new zone Europe/Saratov), Tonga, and Antarctica/Casey. Historical corrections for Italy, Kazakhstan, Malta, and Palestine. Switch to preferring numeric zone abbreviations for Tonga.</p>"
  ]
]