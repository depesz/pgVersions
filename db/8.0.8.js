[
  [
    "<p>Change the server to reject invalidly-encoded\n          multibyte characters in all cases (Tatsuo, Tom)</p>",
    "<p>While <span class=\"productname\">PostgreSQL</span> has\n          been moving in this direction for some time, the checks\n          are now applied uniformly to all encodings and all\n          textual input, and are now always errors not merely\n          warnings. This change defends against SQL-injection\n          attacks of the type described in CVE-2006-2313.</p>"
  ],
  [
    "<p>Reject unsafe uses of <code class=\"literal\">\\'</code>\n          in string literals</p>",
    "<p>As a server-side defense against SQL-injection attacks\n          of the type described in CVE-2006-2314, the server now\n          only accepts <code class=\"literal\">''</code> and not\n          <code class=\"literal\">\\'</code> as a representation of\n          ASCII single quote in SQL string literals. By default,\n          <code class=\"literal\">\\'</code> is rejected only when\n          <code class=\"varname\">client_encoding</code> is set to a\n          client-only encoding (SJIS, BIG5, GBK, GB18030, or UHC),\n          which is the scenario in which SQL injection is possible.\n          A new configuration parameter <code class=\"varname\">backslash_quote</code> is available to adjust\n          this behavior when needed. Note that full security\n          against CVE-2006-2314 might require client-side changes;\n          the purpose of <code class=\"varname\">backslash_quote</code> is in part to make it\n          obvious that insecure clients are insecure.</p>"
  ],
  [
    "<p>Modify <span class=\"application\">libpq</span>'s\n          string-escaping routines to be aware of encoding\n          considerations and <code class=\"varname\">standard_conforming_strings</code></p>",
    "<p>This fixes <span class=\"application\">libpq</span>-using applications for the\n          security issues described in CVE-2006-2313 and\n          CVE-2006-2314, and also future-proofs them against the\n          planned changeover to SQL-standard string literal syntax.\n          Applications that use multiple <span class=\"productname\">PostgreSQL</span> connections concurrently\n          should migrate to <code class=\"function\">PQescapeStringConn()</code> and <code class=\"function\">PQescapeByteaConn()</code> to ensure that\n          escaping is done correctly for the settings in use in\n          each database connection. Applications that do string\n          escaping <span class=\"quote\">&#x201C;<span class=\"quote\">by\n          hand</span>&#x201D;</span> should be modified to rely on library\n          routines instead.</p>"
  ],
  [
    "<p>Fix some incorrect encoding conversion functions</p>",
    "<p><code class=\"function\">win1251_to_iso</code>,\n          <code class=\"function\">alt_to_iso</code>, <code class=\"function\">euc_tw_to_big5</code>, <code class=\"function\">euc_tw_to_mic</code>, <code class=\"function\">mic_to_euc_tw</code> were all broken to\n          varying extents.</p>"
  ],
  [
    "<p>Clean up stray remaining uses of <code class=\"literal\">\\'</code> in strings (Bruce, Jan)</p>"
  ],
  [
    "<p>Fix bug that sometimes caused OR'd index scans to miss\n          rows they should have returned</p>"
  ],
  [
    "<p>Fix WAL replay for case where a btree index has been\n          truncated</p>"
  ],
  [
    "<p>Fix <code class=\"literal\">SIMILAR TO</code> for\n          patterns involving <code class=\"literal\">|</code>\n          (Tom)</p>"
  ],
  [
    "<p>Fix <code class=\"command\">SELECT INTO</code> and\n          <code class=\"command\">CREATE TABLE AS</code> to create\n          tables in the default tablespace, not the base directory\n          (Kris Jurka)</p>"
  ],
  [
    "<p>Fix server to use custom DH SSL parameters correctly\n          (Michael Fuhr)</p>"
  ],
  [
    "<p>Fix for Bonjour on Intel Macs (Ashley Clark)</p>"
  ],
  [
    "<p>Fix various minor memory leaks</p>"
  ],
  [
    "<p>Fix problem with password prompting on some Win32\n          systems (Robert Kinberg)</p>"
  ]
]