[
  [
    "<p>Prevent error recursion crashes when encoding\n          conversion fails (Tom)</p>",
    "<p>This change extends fixes made in the last two minor\n          releases for related failure scenarios. The previous\n          fixes were narrowly tailored for the original problem\n          reports, but we have now recognized that <span class=\"emphasis\"><em>any</em></span> error thrown by an\n          encoding conversion function could potentially lead to\n          infinite recursion while trying to report the error. The\n          solution therefore is to disable translation and encoding\n          conversion and report the plain-ASCII form of any error\n          message, if we find we have gotten into a recursive error\n          reporting situation. (CVE-2009-0922)</p>"
  ],
  [
    "<p>Disallow <code class=\"command\">CREATE\n          CONVERSION</code> with the wrong encodings for the\n          specified conversion function (Heikki)</p>",
    "<p>This prevents one possible scenario for encoding\n          conversion failure. The previous change is a backstop to\n          guard against other kinds of failures in the same\n          area.</p>"
  ],
  [
    "<p>Fix <code class=\"function\">xpath()</code> to not\n          modify the path expression unless necessary, and to make\n          a saner attempt at it when necessary (Andrew)</p>",
    "<p>The SQL standard suggests that <code class=\"function\">xpath</code> should work on data that is a\n          document fragment, but <span class=\"application\">libxml</span> doesn't support that, and\n          indeed it's not clear that this is sensible according to\n          the XPath standard. <code class=\"function\">xpath</code>\n          attempted to work around this mismatch by modifying both\n          the data and the path expression, but the modification\n          was buggy and could cause valid searches to fail. Now,\n          <code class=\"function\">xpath</code> checks whether the\n          data is in fact a well-formed document, and if so invokes\n          <span class=\"application\">libxml</span> with no change to\n          the data or path expression. Otherwise, a different\n          modification method that is somewhat less likely to fail\n          is used.</p>",
    "<div class=\"note\">\n            <h3 class=\"title\">Note</h3>\n            <p>The new modification method is still not 100%\n            satisfactory, and it seems likely that no real solution\n            is possible. This patch should therefore be viewed as a\n            band-aid to keep from breaking existing applications\n            unnecessarily. It is likely that <span class=\"productname\">PostgreSQL</span> 8.4 will simply reject\n            use of <code class=\"function\">xpath</code> on data that\n            is not a well-formed document.</p>\n          </div>"
  ],
  [
    "<p>Fix core dump when <code class=\"function\">to_char()</code> is given format codes that\n          are inappropriate for the type of the data argument\n          (Tom)</p>"
  ],
  [
    "<p>Fix possible failure in text search when C locale is\n          used with a multi-byte encoding (Teodor)</p>",
    "<p>Crashes were possible on platforms where <code class=\"type\">wchar_t</code> is narrower than <code class=\"type\">int</code>; Windows in particular.</p>"
  ],
  [
    "<p>Fix extreme inefficiency in text search parser's\n          handling of an email-like string containing multiple\n          <code class=\"literal\">@</code> characters (Heikki)</p>"
  ],
  [
    "<p>Fix planner problem with sub-<code class=\"command\">SELECT</code> in the output list of a larger\n          subquery (Tom)</p>",
    "<p>The known symptom of this bug is a <span class=\"quote\">&#x201C;<span class=\"quote\">failed to locate grouping\n          columns</span>&#x201D;</span> error that is dependent on the\n          datatype involved; but there could be other issues as\n          well.</p>"
  ],
  [
    "<p>Fix decompilation of <code class=\"literal\">CASE\n          WHEN</code> with an implicit coercion (Tom)</p>",
    "<p>This mistake could lead to Assert failures in an\n          Assert-enabled build, or an <span class=\"quote\">&#x201C;<span class=\"quote\">unexpected CASE WHEN\n          clause</span>&#x201D;</span> error message in other cases, when\n          trying to examine or dump a view.</p>"
  ],
  [
    "<p>Fix possible misassignment of the owner of a TOAST\n          table's rowtype (Tom)</p>",
    "<p>If <code class=\"command\">CLUSTER</code> or a rewriting\n          variant of <code class=\"command\">ALTER TABLE</code> were\n          executed by someone other than the table owner, the\n          <code class=\"structname\">pg_type</code> entry for the\n          table's TOAST table would end up marked as owned by that\n          someone. This caused no immediate problems, since the\n          permissions on the TOAST rowtype aren't examined by any\n          ordinary database operation. However, it could lead to\n          unexpected failures if one later tried to drop the role\n          that issued the command (in 8.1 or 8.2), or <span class=\"quote\">&#x201C;<span class=\"quote\">owner of data type appears\n          to be invalid</span>&#x201D;</span> warnings from <span class=\"application\">pg_dump</span> after having done so (in\n          8.3).</p>"
  ],
  [
    "<p>Change <code class=\"command\">UNLISTEN</code> to exit\n          quickly if the current session has never executed any\n          <code class=\"command\">LISTEN</code> command (Tom)</p>",
    "<p>Most of the time this is not a particularly useful\n          optimization, but since <code class=\"command\">DISCARD\n          ALL</code> invokes <code class=\"command\">UNLISTEN</code>,\n          the previous coding caused a substantial performance\n          problem for applications that made heavy use of\n          <code class=\"command\">DISCARD ALL</code>.</p>"
  ],
  [
    "<p>Fix PL/pgSQL to not treat <code class=\"literal\">INTO</code> after <code class=\"command\">INSERT</code> as an INTO-variables clause\n          anywhere in the string, not only at the start; in\n          particular, don't fail for <code class=\"command\">INSERT\n          INTO</code> within <code class=\"command\">CREATE\n          RULE</code> (Tom)</p>"
  ],
  [
    "<p>Clean up PL/pgSQL error status variables fully at\n          block exit (Ashesh Vashi and Dave Page)</p>",
    "<p>This is not a problem for PL/pgSQL itself, but the\n          omission could cause the PL/pgSQL Debugger to crash while\n          examining the state of a function.</p>"
  ],
  [
    "<p>Retry failed calls to <code class=\"function\">CallNamedPipe()</code> on Windows (Steve\n          Marshall, Magnus)</p>",
    "<p>It appears that this function can sometimes fail\n          transiently; we previously treated any failure as a hard\n          error, which could confuse <code class=\"command\">LISTEN</code>/<code class=\"command\">NOTIFY</code> as well as other operations.</p>"
  ],
  [
    "<p>Add <code class=\"literal\">MUST</code> (Mauritius\n          Island Summer Time) to the default list of known timezone\n          abbreviations (Xavier Bugaud)</p>"
  ]
]