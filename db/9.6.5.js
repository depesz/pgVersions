[
  [
    "<p>Show foreign tables in <tt class=\"STRUCTNAME\">information_schema</tt>.<tt class=\"STRUCTNAME\">table_privileges</tt> view (Peter Eisentraut)</p>",
    "<p>All other relevant <tt class=\"STRUCTNAME\">information_schema</tt> views include foreign tables, but this one ignored them.</p>",
    "<p>Since this view definition is installed by <span class=\"APPLICATION\">initdb</span>, merely upgrading will not fix the problem. If you need to fix this in an existing installation, you can, as a superuser, do this in <span class=\"APPLICATION\">psql</span>:</p>",
    "<pre class=\"PROGRAMLISTING\">SET search_path TO information_schema;\nCREATE OR REPLACE VIEW table_privileges AS\n    SELECT CAST(u_grantor.rolname AS sql_identifier) AS grantor,\n           CAST(grantee.rolname AS sql_identifier) AS grantee,\n           CAST(current_database() AS sql_identifier) AS table_catalog,\n           CAST(nc.nspname AS sql_identifier) AS table_schema,\n           CAST(c.relname AS sql_identifier) AS table_name,\n           CAST(c.prtype AS character_data) AS privilege_type,\n           CAST(\n             CASE WHEN\n                  -- object owner always has grant options\n                  pg_has_role(grantee.oid, c.relowner, 'USAGE')\n                  OR c.grantable\n                  THEN 'YES' ELSE 'NO' END AS yes_or_no) AS is_grantable,\n           CAST(CASE WHEN c.prtype = 'SELECT' THEN 'YES' ELSE 'NO' END AS yes_or_no) AS with_hierarchy\n\n    FROM (\n            SELECT oid, relname, relnamespace, relkind, relowner, (aclexplode(coalesce(relacl, acldefault('r', relowner)))).* FROM pg_class\n         ) AS c (oid, relname, relnamespace, relkind, relowner, grantor, grantee, prtype, grantable),\n         pg_namespace nc,\n         pg_authid u_grantor,\n         (\n           SELECT oid, rolname FROM pg_authid\n           UNION ALL\n           SELECT 0::oid, 'PUBLIC'\n         ) AS grantee (oid, rolname)\n\n    WHERE c.relnamespace = nc.oid\n          AND c.relkind IN ('r', 'v', 'f')\n          AND c.grantee = grantee.oid\n          AND c.grantor = u_grantor.oid\n          AND c.prtype IN ('INSERT', 'SELECT', 'UPDATE', 'DELETE', 'TRUNCATE', 'REFERENCES', 'TRIGGER')\n          AND (pg_has_role(u_grantor.oid, 'USAGE')\n               OR pg_has_role(grantee.oid, 'USAGE')\n               OR grantee.rolname = 'PUBLIC');</pre>",
    "<p>This must be repeated in each database to be fixed, including <tt class=\"LITERAL\">template0</tt>.</p>"
  ],
  [
    "<p>Clean up handling of a fatal exit (e.g., due to receipt of <span class=\"SYSTEMITEM\">SIGTERM</span>) that occurs while trying to execute a <tt class=\"COMMAND\">ROLLBACK</tt> of a failed transaction (Tom Lane)</p>",
    "<p>This situation could result in an assertion failure. In production builds, the exit would still occur, but it would log an unexpected message about <span class=\"QUOTE\">\"cannot drop active portal\"</span>.</p>"
  ],
  [
    "<p>Remove assertion that could trigger during a fatal exit (Tom Lane)</p>"
  ],
  [
    "<p>Correctly identify columns that are of a range type or domain type over a composite type or domain type being searched for (Tom Lane)</p>",
    "<p>Certain <tt class=\"COMMAND\">ALTER</tt> commands that change the definition of a composite type or domain type are supposed to fail if there are any stored values of that type in the database, because they lack the infrastructure needed to update or check such values. Previously, these checks could miss relevant values that are wrapped inside range types or sub-domains, possibly allowing the database to become inconsistent.</p>"
  ],
  [
    "<p>Prevent crash when passing fixed-length pass-by-reference data types to parallel worker processes (Tom Lane)</p>"
  ],
  [
    "<p>Fix crash in <span class=\"APPLICATION\">pg_restore</span> when using parallel mode and using a list file to select a subset of items to restore (Fabrízio de Royes Mello)</p>"
  ],
  [
    "<p>Change <span class=\"APPLICATION\">ecpg</span>'s parser to allow <tt class=\"LITERAL\">RETURNING</tt> clauses without attached C variables (Michael Meskes)</p>",
    "<p>This allows <span class=\"APPLICATION\">ecpg</span> programs to contain SQL constructs that use <tt class=\"LITERAL\">RETURNING</tt> internally (for example, inside a CTE) rather than using it to define values to be returned to the client.</p>"
  ],
  [
    "<p>Change <span class=\"APPLICATION\">ecpg</span>'s parser to recognize backslash continuation of C preprocessor command lines (Michael Meskes)</p>"
  ],
  [
    "<p>Improve selection of compiler flags for PL/Perl on Windows (Tom Lane)</p>",
    "<p>This fix avoids possible crashes of PL/Perl due to inconsistent assumptions about the width of <tt class=\"TYPE\">time_t</tt> values. A side-effect that may be visible to extension developers is that <tt class=\"LITERAL\">_USE_32BIT_TIME_T</tt> is no longer defined globally in <span class=\"PRODUCTNAME\">PostgreSQL</span> Windows builds. This is not expected to cause problems, because type <tt class=\"TYPE\">time_t</tt> is not used in any <span class=\"PRODUCTNAME\">PostgreSQL</span> API definitions.</p>"
  ],
  [
    "<p>Fix <tt class=\"LITERAL\">make check</tt> to behave correctly when invoked via a non-GNU make program (Thomas Munro)</p>"
  ]
]