[
  [
    "<p>Force the default <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-wal.html#GUC-WAL-SYNC-METHOD\"><code class=\"varname\">\n          wal_sync_method</code></a> to be <code class=\"literal\">fdatasync</code> on Linux (Tom Lane, Marti\n          Raudsepp)</p>",
    "<p>The default on Linux has actually been <code class=\"literal\">fdatasync</code> for many years, but recent\n          kernel changes caused <span class=\"productname\">PostgreSQL</span> to choose <code class=\"literal\">open_datasync</code> instead. This choice did\n          not result in any performance improvement, and caused\n          outright failures on certain filesystems, notably\n          <code class=\"literal\">ext4</code> with the <code class=\"literal\">data=journal</code> mount option.</p>"
  ],
  [
    "<p>Fix assorted bugs in WAL replay logic for GIN indexes\n          (Tom Lane)</p>",
    "<p>This could result in <span class=\"quote\">&#x201C;<span class=\"quote\">bad buffer id: 0</span>&#x201D;</span> failures or\n          corruption of index contents during replication.</p>"
  ],
  [
    "<p>Fix recovery from base backup when the starting\n          checkpoint WAL record is not in the same WAL segment as\n          its redo point (Jeff Davis)</p>"
  ],
  [
    "<p>Add support for detecting register-stack overrun on\n          <code class=\"literal\">IA64</code> (Tom Lane)</p>",
    "<p>The <code class=\"literal\">IA64</code> architecture has\n          two hardware stacks. Full prevention of stack-overrun\n          failures requires checking both.</p>"
  ],
  [
    "<p>Add a check for stack overflow in <code class=\"function\">copyObject()</code> (Tom Lane)</p>",
    "<p>Certain code paths could crash due to stack overflow\n          given a sufficiently complex query.</p>"
  ],
  [
    "<p>Fix detection of page splits in temporary GiST indexes\n          (Heikki Linnakangas)</p>",
    "<p>It is possible to have a <span class=\"quote\">&#x201C;<span class=\"quote\">concurrent</span>&#x201D;</span>\n          page split in a temporary index, if for example there is\n          an open cursor scanning the index when an insertion is\n          done. GiST failed to detect this case and hence could\n          deliver wrong results when execution of the cursor\n          continued.</p>"
  ],
  [
    "<p>Avoid memory leakage while <code class=\"command\">ANALYZE</code>'ing complex index expressions\n          (Tom Lane)</p>"
  ],
  [
    "<p>Ensure an index that uses a whole-row Var still\n          depends on its table (Tom Lane)</p>",
    "<p>An index declared like <code class=\"literal\">create\n          index i on t (foo(t.*))</code> would not automatically\n          get dropped when its table was dropped.</p>"
  ],
  [
    "<p>Do not <span class=\"quote\">&#x201C;<span class=\"quote\">inline</span>&#x201D;</span> a SQL function with\n          multiple <code class=\"literal\">OUT</code> parameters (Tom\n          Lane)</p>",
    "<p>This avoids a possible crash due to loss of\n          information about the expected result rowtype.</p>"
  ],
  [
    "<p>Behave correctly if <code class=\"literal\">ORDER\n          BY</code>, <code class=\"literal\">LIMIT</code>,\n          <code class=\"literal\">FOR UPDATE</code>, or <code class=\"literal\">WITH</code> is attached to the <code class=\"literal\">VALUES</code> part of <code class=\"literal\">INSERT ... VALUES</code> (Tom Lane)</p>"
  ],
  [
    "<p>Fix constant-folding of <code class=\"literal\">COALESCE()</code> expressions (Tom Lane)</p>",
    "<p>The planner would sometimes attempt to evaluate\n          sub-expressions that in fact could never be reached,\n          possibly leading to unexpected errors.</p>"
  ],
  [
    "<p>Add print functionality for <code class=\"structname\">InhRelation</code> nodes (Tom Lane)</p>",
    "<p>This avoids a failure when <code class=\"varname\">debug_print_parse</code> is enabled and certain\n          types of query are executed.</p>"
  ],
  [
    "<p>Fix incorrect calculation of distance from a point to\n          a horizontal line segment (Tom Lane)</p>",
    "<p>This bug affected several different geometric\n          distance-measurement operators.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">PL/pgSQL</span>'s\n          handling of <span class=\"quote\">&#x201C;<span class=\"quote\">simple</span>&#x201D;</span> expressions to not fail in\n          recursion or error-recovery cases (Tom Lane)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">PL/Python</span>'s\n          handling of set-returning functions (Jan Urbanski)</p>",
    "<p>Attempts to call SPI functions within the iterator\n          generating a set result would fail.</p>"
  ],
  [
    "<p>Fix bug in <code class=\"filename\">contrib/cube</code>'s GiST picksplit algorithm\n          (Alexander Korotkov)</p>",
    "<p>This could result in considerable inefficiency, though\n          not actually incorrect answers, in a GiST index on a\n          <code class=\"type\">cube</code> column. If you have such\n          an index, consider <code class=\"command\">REINDEX</code>ing it after installing this\n          update.</p>"
  ],
  [
    "<p>Don't emit <span class=\"quote\">&#x201C;<span class=\"quote\">identifier will be truncated</span>&#x201D;</span>\n          notices in <code class=\"filename\">contrib/dblink</code>\n          except when creating new connections (Itagaki\n          Takahiro)</p>"
  ],
  [
    "<p>Fix potential coredump on missing public key in\n          <code class=\"filename\">contrib/pgcrypto</code> (Marti\n          Raudsepp)</p>"
  ],
  [
    "<p>Fix memory leak in <code class=\"filename\">contrib/xml2</code>'s XPath query functions\n          (Tom Lane)</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2010o for DST law\n          changes in Fiji and Samoa; also historical corrections\n          for Hong Kong.</p>"
  ]
]