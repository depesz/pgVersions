[
  [
    "<p>Fix temporary memory leak when using non-hashed\n          aggregates (Tom)</p>"
  ],
  [
    "<p>ECPG fixes, including some for Informix compatibility\n          (Michael)</p>"
  ],
  [
    "<p>Fixes for compiling with thread-safety, particularly\n          Solaris (Bruce)</p>"
  ],
  [
    "<p>Fix error in COPY IN termination when using the old\n          network protocol (ljb)</p>"
  ],
  [
    "<p>Several important fixes in pg_autovacuum, including\n          fixes for large tables, unsigned oids, stability, temp\n          tables, and debug mode (Matthew T. O'Connor)</p>"
  ],
  [
    "<p>Fix problem with reading tar-format dumps on NetBSD\n          and BSD/OS (Bruce)</p>"
  ],
  [
    "<p>Several JDBC fixes</p>"
  ],
  [
    "<p>Fix ALTER SEQUENCE RESTART where last_value equals the\n          restart value (Tom)</p>"
  ],
  [
    "<p>Repair failure to recalculate nested sub-selects\n          (Tom)</p>"
  ],
  [
    "<p>Fix problems with non-constant expressions in\n          LIMIT/OFFSET</p>"
  ],
  [
    "<p>Support FULL JOIN with no join clause, such as X FULL\n          JOIN Y ON TRUE (Tom)</p>"
  ],
  [
    "<p>Fix another zero-column table bug (Tom)</p>"
  ],
  [
    "<p>Improve handling of non-qualified identifiers in GROUP\n          BY clauses in sub-selects (Tom)</p>",
    "<p>Select-list aliases within the sub-select will now\n          take precedence over names from outer query levels.</p>"
  ],
  [
    "<p>Do not generate <span class=\"quote\">&#x201C;<span class=\"quote\">NATURAL CROSS JOIN</span>&#x201D;</span> when\n          decompiling rules (Tom)</p>"
  ],
  [
    "<p>Add checks for invalid field length in binary COPY\n          (Tom)</p>",
    "<p>This fixes a difficult-to-exploit security hole.</p>"
  ],
  [
    "<p>Avoid locking conflict between <code class=\"command\">ANALYZE</code> and <code class=\"command\">LISTEN</code>/<code class=\"command\">NOTIFY</code></p>"
  ],
  [
    "<p>Numerous translation updates (various\n          contributors)</p>"
  ]
]