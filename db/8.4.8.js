[
  [
    "<p>Fix <span class=\"application\">pg_upgrade</span>'s\n          handling of TOAST tables (Bruce Momjian)</p>",
    "<p>The <code class=\"structname\">pg_class</code>.<code class=\"structfield\">relfrozenxid</code> value for TOAST tables\n          was not correctly copied into the new installation during\n          <span class=\"application\">pg_upgrade</span>. This could\n          later result in <code class=\"literal\">pg_clog</code>\n          files being discarded while they were still needed to\n          validate tuples in the TOAST tables, leading to\n          <span class=\"quote\">&#x201C;<span class=\"quote\">could not access\n          status of transaction</span>&#x201D;</span> failures.</p>",
    "<p>This error poses a significant risk of data loss for\n          installations that have been upgraded with <span class=\"application\">pg_upgrade</span>. This patch corrects the\n          problem for future uses of <span class=\"application\">pg_upgrade</span>, but does not in itself\n          cure the issue in installations that have been processed\n          with a buggy version of <span class=\"application\">pg_upgrade</span>.</p>"
  ],
  [
    "<p>Suppress incorrect <span class=\"quote\">&#x201C;<span class=\"quote\">PD_ALL_VISIBLE flag was incorrectly\n          set</span>&#x201D;</span> warning (Heikki Linnakangas)</p>",
    "<p><code class=\"command\">VACUUM</code> would sometimes\n          issue this warning in cases that are actually valid.</p>"
  ],
  [
    "<p>Disallow including a composite type in itself (Tom\n          Lane)</p>",
    "<p>This prevents scenarios wherein the server could\n          recurse infinitely while processing the composite type.\n          While there are some possible uses for such a structure,\n          they don't seem compelling enough to justify the effort\n          required to make sure it always works safely.</p>"
  ],
  [
    "<p>Avoid potential deadlock during catalog cache\n          initialization (Nikhil Sontakke)</p>",
    "<p>In some cases the cache loading code would acquire\n          share lock on a system index before locking the index's\n          catalog. This could deadlock against processes trying to\n          acquire exclusive locks in the other, more standard\n          order.</p>"
  ],
  [
    "<p>Fix dangling-pointer problem in <code class=\"literal\">BEFORE ROW UPDATE</code> trigger handling when\n          there was a concurrent update to the target tuple (Tom\n          Lane)</p>",
    "<p>This bug has been observed to result in intermittent\n          <span class=\"quote\">&#x201C;<span class=\"quote\">cannot extract\n          system attribute from virtual tuple</span>&#x201D;</span>\n          failures while trying to do <code class=\"literal\">UPDATE\n          RETURNING ctid</code>. There is a very small probability\n          of more serious errors, such as generating incorrect\n          index entries for the updated tuple.</p>"
  ],
  [
    "<p>Disallow <code class=\"command\">DROP TABLE</code> when\n          there are pending deferred trigger events for the table\n          (Tom Lane)</p>",
    "<p>Formerly the <code class=\"command\">DROP</code> would\n          go through, leading to <span class=\"quote\">&#x201C;<span class=\"quote\">could not open relation with OID\n          nnn</span>&#x201D;</span> errors when the triggers were\n          eventually fired.</p>"
  ],
  [
    "<p>Prevent crash triggered by constant-false WHERE\n          conditions during GEQO optimization (Tom Lane)</p>"
  ],
  [
    "<p>Improve planner's handling of semi-join and anti-join\n          cases (Tom Lane)</p>"
  ],
  [
    "<p>Fix selectivity estimation for text search to account\n          for NULLs (Jesper Krogh)</p>"
  ],
  [
    "<p>Improve PL/pgSQL's ability to handle row types with\n          dropped columns (Pavel Stehule)</p>",
    "<p>This is a back-patch of fixes previously made in\n          9.0.</p>"
  ],
  [
    "<p>Fix PL/Python memory leak involving array slices\n          (Daniel Popowich)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_restore</span> to\n          cope with long lines (over 1KB) in TOC files (Tom\n          Lane)</p>"
  ],
  [
    "<p>Put in more safeguards against crashing due to\n          division-by-zero with overly enthusiastic compiler\n          optimization (Aurelien Jarno)</p>"
  ],
  [
    "<p>Support use of dlopen() in FreeBSD and OpenBSD on MIPS\n          (Tom Lane)</p>",
    "<p>There was a hard-wired assumption that this system\n          function was not available on MIPS hardware on these\n          systems. Use a compile-time test instead, since more\n          recent versions have it.</p>"
  ],
  [
    "<p>Fix compilation failures on HP-UX (Heikki\n          Linnakangas)</p>"
  ],
  [
    "<p>Fix version-incompatibility problem with <span class=\"application\">libintl</span> on Windows (Hiroshi\n          Inoue)</p>"
  ],
  [
    "<p>Fix usage of <span class=\"application\">xcopy</span> in\n          Windows build scripts to work correctly under Windows 7\n          (Andrew Dunstan)</p>",
    "<p>This affects the build scripts only, not installation\n          or usage.</p>"
  ],
  [
    "<p>Fix path separator used by <span class=\"application\">pg_regress</span> on Cygwin (Andrew\n          Dunstan)</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2011f for DST law\n          changes in Chile, Cuba, Falkland Islands, Morocco, Samoa,\n          and Turkey; also historical corrections for South\n          Australia, Alaska, and Hawaii.</p>"
  ]
]