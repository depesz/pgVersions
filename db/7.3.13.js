[
  [
    "<p>Fix character string comparison for locales that\n          consider different character combinations as equal, such\n          as Hungarian (Tom)</p>",
    "<p>This might require <code class=\"command\">REINDEX</code> to fix existing indexes on\n          textual columns.</p>"
  ],
  [
    "<p>Set locale environment variables during postmaster\n          startup to ensure that <span class=\"application\">plperl</span> won't change the locale\n          later</p>",
    "<p>This fixes a problem that occurred if the <span class=\"application\">postmaster</span> was started with\n          environment variables specifying a different locale than\n          what <span class=\"application\">initdb</span> had been\n          told. Under these conditions, any use of <span class=\"application\">plperl</span> was likely to lead to corrupt\n          indexes. You might need <code class=\"command\">REINDEX</code> to fix existing indexes on\n          textual columns if this has happened to you.</p>"
  ],
  [
    "<p>Fix longstanding bug in strpos() and regular\n          expression handling in certain rarely used Asian\n          multi-byte character sets (Tatsuo)</p>"
  ],
  [
    "<p>Fix bug in <code class=\"filename\">/contrib/pgcrypto</code> gen_salt, which\n          caused it not to use all available salt space for MD5 and\n          XDES algorithms (Marko Kreen, Solar Designer)</p>",
    "<p>Salts for Blowfish and standard DES are\n          unaffected.</p>"
  ],
  [
    "<p>Fix <code class=\"filename\">/contrib/dblink</code> to\n          throw an error, rather than crashing, when the number of\n          columns specified is different from what's actually\n          returned by the query (Joe)</p>"
  ]
]