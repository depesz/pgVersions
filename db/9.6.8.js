[
  [
    "<p>Document how to configure installations and applications to guard against search-path-dependent trojan-horse attacks from other users (Noah Misch)</p>",
    "<p>Using a <tt class=\"VARNAME\">search_path</tt> setting that includes any schemas writable by a hostile user enables that user to capture control of queries and then run arbitrary SQL code with the permissions of the attacked user. While it is possible to write queries that are proof against such hijacking, it is notationally tedious, and it's very easy to overlook holes. Therefore, we now recommend configurations in which no untrusted schemas appear in one's search path. Relevant documentation appears in <a href=\"https://www.postgresql.org/docs/9.6/ddl-schemas.html#DDL-SCHEMAS-PATTERNS\">Section 5.8.6</a> (for database administrators and users), <a href=\"https://www.postgresql.org/docs/9.6/libpq-connect.html\">Section 32.1</a> (for application authors), <a href=\"https://www.postgresql.org/docs/9.6/extend-extensions.html#EXTEND-EXTENSIONS-STYLE\">Section 36.15.1</a> (for extension authors), and <a href=\"https://www.postgresql.org/docs/9.6/sql-createfunction.html\">CREATE FUNCTION</a> (for authors of <tt class=\"LITERAL\">SECURITY DEFINER</tt> functions). (CVE-2018-1058)</p>"
  ],
  [
    "<p>Avoid use of insecure <tt class=\"VARNAME\">search_path</tt> settings in <span class=\"APPLICATION\">pg_dump</span> and other client programs (Noah Misch, Tom Lane)</p>",
    "<p><span class=\"APPLICATION\">pg_dump</span>, <span class=\"APPLICATION\">pg_upgrade</span>, <span class=\"APPLICATION\">vacuumdb</span> and other <span class=\"PRODUCTNAME\">PostgreSQL</span>-provided applications were themselves vulnerable to the type of hijacking described in the previous changelog entry; since these applications are commonly run by superusers, they present particularly attractive targets. To make them secure whether or not the installation as a whole has been secured, modify them to include only the <tt class=\"STRUCTNAME\">pg_catalog</tt> schema in their <tt class=\"VARNAME\">search_path</tt> settings. Autovacuum worker processes now do the same, as well.</p>",
    "<p>In cases where user-provided functions are indirectly executed by these programs — for example, user-provided functions in index expressions — the tighter <tt class=\"VARNAME\">search_path</tt> may result in errors, which will need to be corrected by adjusting those user-provided functions to not assume anything about what search path they are invoked under. That has always been good practice, but now it will be necessary for correct behavior. (CVE-2018-1058)</p>"
  ],
  [
    "<p>Fix misbehavior of concurrent-update rechecks with CTE references appearing in subplans (Tom Lane)</p>",
    "<p>If a CTE (<tt class=\"LITERAL\">WITH</tt> clause reference) is used in an InitPlan or SubPlan, and the query requires a recheck due to trying to update or lock a concurrently-updated row, incorrect results could be obtained.</p>"
  ],
  [
    "<p>Fix planner failures with overlapping mergejoin clauses in an outer join (Tom Lane)</p>",
    "<p>These mistakes led to <span class=\"QUOTE\">\"left and right pathkeys do not match in mergejoin\"</span> or <span class=\"QUOTE\">\"outer pathkeys do not match mergeclauses\"</span> planner errors in corner cases.</p>"
  ],
  [
    "<p>Repair <span class=\"APPLICATION\">pg_upgrade</span>'s failure to preserve <tt class=\"STRUCTFIELD\">relfrozenxid</tt> for materialized views (Tom Lane, Andres Freund)</p>",
    "<p>This oversight could lead to data corruption in materialized views after an upgrade, manifesting as <span class=\"QUOTE\">\"could not access status of transaction\"</span> or <span class=\"QUOTE\">\"found xmin from before relfrozenxid\"</span> errors. The problem would be more likely to occur in seldom-refreshed materialized views, or ones that were maintained only with <tt class=\"COMMAND\">REFRESH MATERIALIZED VIEW CONCURRENTLY</tt>.</p>",
    "<p>If such corruption is observed, it can be repaired by refreshing the materialized view (without <tt class=\"LITERAL\">CONCURRENTLY</tt>).</p>"
  ],
  [
    "<p>Fix incorrect reporting of PL/Python function names in error <tt class=\"LITERAL\">CONTEXT</tt> stacks (Tom Lane)</p>",
    "<p>An error occurring within a nested PL/Python function call (that is, one reached via a SPI query from another PL/Python function) would result in a stack trace showing the inner function's name twice, rather than the expected results. Also, an error in a nested PL/Python <tt class=\"LITERAL\">DO</tt> block could result in a null pointer dereference crash on some platforms.</p>"
  ],
  [
    "<p>Allow <tt class=\"FILENAME\">contrib/auto_explain</tt>'s <tt class=\"VARNAME\">log_min_duration</tt> setting to range up to <tt class=\"LITERAL\">INT_MAX</tt>, or about 24 days instead of 35 minutes (Tom Lane)</p>"
  ],
  [
    "<p>Mark assorted GUC variables as <tt class=\"LITERAL\">PGDLLIMPORT</tt>, to ease porting extension modules to Windows (Metin Doslu)</p>"
  ]
]