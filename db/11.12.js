[
  [
    "<p>Prevent integer overflows in array subscripting calculations (Tom Lane)</p>",
    "<p>The array code previously did not complain about cases where an array's lower bound plus length overflows an integer. This resulted in later entries in the array becoming inaccessible (since their subscripts could not be written as integers), but more importantly it confused subsequent assignment operations. This could lead to memory overwrites, with ensuing crashes or unwanted data modifications. (CVE-2021-32027)</p>"
  ],
  [
    "<p>Fix mishandling of <span class=\"quote\">“<span class=\"quote\">junk</span>”</span> columns in <code class=\"literal\">INSERT ... ON CONFLICT ... UPDATE</code> target lists (Tom Lane)</p>",
    "<p>If the <code class=\"literal\">UPDATE</code> list contains any multi-column sub-selects (which give rise to junk columns in addition to the results proper), the <code class=\"literal\">UPDATE</code> path would end up storing tuples that include the values of the extra junk columns. That's fairly harmless in the short run, but if new columns are added to the table then the values would become accessible, possibly leading to malfunctions if they don't match the datatypes of the added columns.</p>",
    "<p>In addition, in versions supporting cross-partition updates, a cross-partition update triggered by such a case had the reverse problem: the junk columns were removed from the target list, typically causing an immediate crash due to malfunction of the multi-column sub-select mechanism. (CVE-2021-32028)</p>"
  ],
  [
    "<p>Fix possibly-incorrect computation of <code class=\"command\">UPDATE ... RETURNING</code> outputs for joined cross-partition updates (Amit Langote, Etsuro Fujita)</p>",
    "<p>If an <code class=\"command\">UPDATE</code> for a partitioned table caused a row to be moved to another partition with a physically different row type (for example, one with a different set of dropped columns), computation of <code class=\"literal\">RETURNING</code> results for that row could produce errors or wrong answers. No error is observed unless the <code class=\"command\">UPDATE</code> involves other tables being joined to the target table. (CVE-2021-32029)</p>"
  ],
  [
    "<p>Fix adjustment of constraint deferrability properties in partitioned tables (Álvaro Herrera)</p>",
    "<p>When applied to a foreign-key constraint of a partitioned table, <code class=\"command\">ALTER TABLE ... ALTER CONSTRAINT</code> failed to adjust the <code class=\"literal\">DEFERRABLE</code> and/or <code class=\"literal\">INITIALLY DEFERRED</code> markings of the constraints and triggers of leaf partitions. This led to unexpected behavior of such constraints. After updating to this version, any misbehaving partitioned tables can be fixed by executing a new <code class=\"command\">ALTER</code> command to set the desired properties.</p>",
    "<p>This change also disallows applying such an <code class=\"command\">ALTER</code> directly to the constraints of leaf partitions. The only supported case is for the whole partitioning hierarchy to have identical constraint properties, so such <code class=\"command\">ALTER</code>s must be applied at the partition root.</p>"
  ],
  [
    "<p>Forbid marking an identity column as nullable (Vik Fearing)</p>",
    "<p><code class=\"literal\">GENERATED ALWAYS AS IDENTITY</code> implies <code class=\"literal\">NOT NULL</code>, so don't allow it to be combined with an explicit <code class=\"literal\">NULL</code> specification.</p>"
  ],
  [
    "<p>Allow <code class=\"literal\">ALTER ROLE/DATABASE ... SET</code> to set the <code class=\"varname\">role</code>, <code class=\"varname\">session_authorization</code>, and <code class=\"varname\">temp_buffers</code> parameters (Tom Lane)</p>",
    "<p>Previously, over-eager validity checks might reject these commands, even if the values would have worked when used later. This created a command ordering hazard for dump/reload and upgrade scenarios.</p>"
  ],
  [
    "<p>Fix bug with coercing the result of a <code class=\"literal\">COLLATE</code> expression to a non-collatable type (Tom Lane)</p>",
    "<p>This led to a parse tree in which the <code class=\"literal\">COLLATE</code> appears to be applied to a non-collatable value. While that normally has no real impact (since <code class=\"literal\">COLLATE</code> has no effect at runtime), it was possible to construct views that would be rejected during dump/reload.</p>"
  ],
  [
    "<p>Disallow calling window functions and procedures via the <span class=\"quote\">“<span class=\"quote\">fast path</span>”</span> wire protocol message (Tom Lane)</p>",
    "<p>Only plain functions are supported here. While trying to call an aggregate function failed already, calling a window function would crash, and calling a procedure would work only if the procedure did no transaction control.</p>"
  ],
  [
    "<p>Extend <code class=\"function\">pg_identify_object_as_address()</code> to support event triggers (Joel Jacobson)</p>"
  ],
  [
    "<p>Fix <code class=\"function\">to_char()</code>'s handling of Roman-numeral month format codes with negative intervals (Julien Rouhaud)</p>",
    "<p>Previously, such cases would usually cause a crash.</p>"
  ],
  [
    "<p>Check that the argument of <code class=\"function\">pg_import_system_collations()</code> is a valid schema OID (Tom Lane)</p>"
  ],
  [
    "<p>Fix use of uninitialized value while parsing an <code class=\"literal\">\\{<em class=\"replaceable\"><code>m</code></em>,<em class=\"replaceable\"><code>n</code></em>\\}</code> quantifier in a BRE-mode regular expression (Tom Lane)</p>",
    "<p>This error could cause the quantifier to act non-greedy, that is behave like an <code class=\"literal\">{<em class=\"replaceable\"><code>m</code></em>,<em class=\"replaceable\"><code>n</code></em>}?</code> quantifier would do in full regular expressions.</p>"
  ],
  [
    "<p>Don't ignore system columns when estimating the number of groups using extended statistics (Tomas Vondra)</p>",
    "<p>This led to strange estimates for queries such as <code class=\"literal\">SELECT ... GROUP BY a, b, ctid</code>.</p>"
  ],
  [
    "<p>Avoid divide-by-zero when estimating selectivity of a regular expression with a very long fixed prefix (Tom Lane)</p>",
    "<p>This typically led to a <code class=\"literal\">NaN</code> selectivity value, causing assertion failures or strange planner behavior.</p>"
  ],
  [
    "<p>Fix access-off-the-end-of-the-table error in BRIN index bitmap scans (Tomas Vondra)</p>",
    "<p>If the page range size used by a BRIN index isn't a power of two, there were corner cases in which a bitmap scan could try to fetch pages past the actual end of the table, leading to <span class=\"quote\">“<span class=\"quote\">could not open file</span>”</span> errors.</p>"
  ],
  [
    "<p>Avoid incorrect timeline change while recovering uncommitted two-phase transactions from WAL (Soumyadeep Chakraborty, Jimmy Yih, Kevin Yeap)</p>",
    "<p>This error could lead to subsequent WAL records being written under the wrong timeline ID, leading to consistency problems, or even complete failure to be able to restart the server, later on.</p>"
  ],
  [
    "<p>Ensure that locks are released while shutting down a standby server's startup process (Fujii Masao)</p>",
    "<p>When a standby server is shut down while still in recovery, some locks might be left held. This causes assertion failures in debug builds; it's unclear whether any serious consequence could occur in production builds.</p>"
  ],
  [
    "<p>Fix crash when a logical replication worker does <code class=\"command\">ALTER SUBSCRIPTION REFRESH</code> (Peter Smith)</p>",
    "<p>The core code won't do this, but a replica trigger could.</p>"
  ],
  [
    "<p>Ensure we default to <code class=\"varname\">wal_sync_method</code> = <code class=\"literal\">fdatasync</code> on recent FreeBSD (Thomas Munro)</p>",
    "<p>FreeBSD 13 supports <code class=\"literal\">open_datasync</code>, which would normally become the default choice. However, it's unclear whether that is actually an improvement for Postgres, so preserve the existing default for now.</p>"
  ],
  [
    "<p>Ensure we finish cleaning up when interrupted while detaching a DSM segment (Thomas Munro)</p>",
    "<p>This error could result in temporary files not being cleaned up promptly after a parallel query.</p>"
  ],
  [
    "<p>Fix memory leak while initializing server's SSL parameters (Michael Paquier)</p>",
    "<p>This is ordinarily insignificant, but if the postmaster is repeatedly sent <span class=\"systemitem\">SIGHUP</span> signals, the leak can build up over time.</p>"
  ],
  [
    "<p>Fix assorted minor memory leaks in the server (Tom Lane, Andres Freund)</p>"
  ],
  [
    "<p>Fix failure when a PL/pgSQL <code class=\"command\">DO</code> block makes use of both composite-type variables and transaction control (Tom Lane)</p>",
    "<p>Previously, such cases led to errors about leaked tuple descriptors.</p>"
  ],
  [
    "<p>Prevent infinite loop in <span class=\"application\">libpq</span> if a ParameterDescription message with a corrupt length is received (Tom Lane)</p>"
  ],
  [
    "<p>When <span class=\"application\">initdb</span> prints instructions about how to start the server, make the path shown for <span class=\"application\">pg_ctl</span> use backslash separators on Windows (Nitin Jadhav)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">psql</span> to restore the previous behavior of <code class=\"literal\">\\connect service=<em class=\"replaceable\"><code>something</code></em></code> (Tom Lane)</p>",
    "<p>A previous bug fix caused environment variables (such as <code class=\"varname\">PGPORT</code>) to override entries in the service file in this context. Restore the previous behavior, in which the priority is the other way around.</p>"
  ],
  [
    "<p>Fix race condition in detection of file modification by <span class=\"application\">psql</span>'s <code class=\"literal\">\\e</code> and related commands (Laurenz Albe)</p>",
    "<p>A very fast typist could fool the code's file-timestamp-based detection of whether the temporary edit file was changed.</p>"
  ],
  [
    "<p>Fix missed file version check in <span class=\"application\">pg_restore</span> (Tom Lane)</p>",
    "<p>When reading a custom-format archive from a non-seekable source, <span class=\"application\">pg_restore</span> neglected to check the archive version. If it was fed a newer archive version than it can support, it would fail messily later on.</p>"
  ],
  [
    "<p>Add some more checks to <span class=\"application\">pg_upgrade</span> for user tables containing non-upgradable data types (Tom Lane)</p>",
    "<p>Fix detection of some cases where a non-upgradable data type is embedded within a container type (such as an array or range). Also disallow upgrading when user tables contain columns of system-defined composite types, since those types' OIDs are not stable across versions.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_waldump</span> to count <code class=\"literal\">XACT</code> records correctly when generating per-record statistics (Kyotaro Horiguchi)</p>"
  ],
  [
    "<p>Fix <code class=\"filename\">contrib/amcheck</code> to not complain about the tuple flags <code class=\"literal\">HEAP_XMAX_LOCK_ONLY</code> and <code class=\"literal\">HEAP_KEYS_UPDATED</code> both being set (Julien Rouhaud)</p>",
    "<p>This is a valid state after <code class=\"literal\">SELECT FOR UPDATE</code>.</p>"
  ],
  [
    "<p>Adjust VPATH build rules to support recent Oracle Developer Studio compiler versions (Noah Misch)</p>"
  ],
  [
    "<p>Fix testing of PL/Python for Python 3 on Solaris (Noah Misch)</p>"
  ]
]