[
  [
    "<p>Change encoding function signature to prevent\n          misuse</p>"
  ],
  [
    "<p>Change <code class=\"filename\">contrib/tsearch2</code>\n          to avoid unsafe use of <code class=\"type\">INTERNAL</code>\n          function results</p>"
  ],
  [
    "<p>Repair ancient race condition that allowed a\n          transaction to be seen as committed for some purposes (eg\n          SELECT FOR UPDATE) slightly sooner than for other\n          purposes</p>",
    "<p>This is an extremely serious bug since it could lead\n          to apparent data inconsistencies being briefly visible to\n          applications.</p>"
  ],
  [
    "<p>Repair race condition between relation extension and\n          VACUUM</p>",
    "<p>This could theoretically have caused loss of a page's\n          worth of freshly-inserted data, although the scenario\n          seems of very low probability. There are no known cases\n          of it having caused more than an Assert failure.</p>"
  ],
  [
    "<p>Fix comparisons of <code class=\"type\">TIME WITH TIME\n          ZONE</code> values</p>",
    "<p>The comparison code was wrong in the case where the\n          <code class=\"literal\">--enable-integer-datetimes</code>\n          configuration switch had been used. NOTE: if you have an\n          index on a <code class=\"type\">TIME WITH TIME ZONE</code>\n          column, it will need to be <code class=\"command\">REINDEX</code>ed after installing this update,\n          because the fix corrects the sort order of column\n          values.</p>"
  ],
  [
    "<p>Fix <code class=\"function\">EXTRACT(EPOCH)</code> for\n          <code class=\"type\">TIME WITH TIME ZONE</code> values</p>"
  ],
  [
    "<p>Fix mis-display of negative fractional seconds in\n          <code class=\"type\">INTERVAL</code> values</p>",
    "<p>This error only occurred when the <code class=\"literal\">--enable-integer-datetimes</code> configuration\n          switch had been used.</p>"
  ],
  [
    "<p>Ensure operations done during backend shutdown are\n          counted by statistics collector</p>",
    "<p>This is expected to resolve reports of <span class=\"application\">pg_autovacuum</span> not vacuuming the\n          system catalogs often enough &#x2014; it was not being told\n          about catalog deletions caused by temporary table removal\n          during backend exit.</p>"
  ],
  [
    "<p>Additional buffer overrun checks in plpgsql (Neil)</p>"
  ],
  [
    "<p>Fix pg_dump to dump trigger names containing\n          <code class=\"literal\">%</code> correctly (Neil)</p>"
  ],
  [
    "<p>Fix <code class=\"filename\">contrib/pgcrypto</code> for\n          newer OpenSSL builds (Marko Kreen)</p>"
  ],
  [
    "<p>Still more 64-bit fixes for <code class=\"filename\">contrib/intagg</code></p>"
  ],
  [
    "<p>Prevent incorrect optimization of functions returning\n          <code class=\"type\">RECORD</code></p>"
  ],
  [
    "<p>Prevent <code class=\"function\">to_char(interval)</code> from dumping core for\n          month-related formats</p>"
  ],
  [
    "<p>Prevent crash on <code class=\"literal\">COALESCE(NULL,NULL)</code></p>"
  ],
  [
    "<p>Fix <code class=\"function\">array_map</code> to call PL\n          functions correctly</p>"
  ],
  [
    "<p>Fix permission checking in <code class=\"command\">ALTER\n          DATABASE RENAME</code></p>"
  ],
  [
    "<p>Fix <code class=\"command\">ALTER LANGUAGE\n          RENAME</code></p>"
  ],
  [
    "<p>Make <code class=\"function\">RemoveFromWaitQueue</code>\n          clean up after itself</p>",
    "<p>This fixes a lock management error that would only be\n          visible if a transaction was kicked out of a wait for a\n          lock (typically by query cancel) and then the holder of\n          the lock released it within a very narrow window.</p>"
  ],
  [
    "<p>Fix problem with untyped parameter appearing in\n          <code class=\"command\">INSERT ... SELECT</code></p>"
  ],
  [
    "<p>Fix <code class=\"command\">CLUSTER</code> failure after\n          <code class=\"command\">ALTER TABLE SET WITHOUT\n          OIDS</code></p>"
  ]
]