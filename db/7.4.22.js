[
  [
    "<p>Fix datetime input functions to correctly detect\n          integer overflow when running on a 64-bit platform\n          (Tom)</p>"
  ],
  [
    "<p>Improve performance of writing very long log messages\n          to syslog (Tom)</p>"
  ],
  [
    "<p>Fix bug in backwards scanning of a cursor on a\n          <code class=\"literal\">SELECT DISTINCT ON</code> query\n          (Tom)</p>"
  ],
  [
    "<p>Fix planner to estimate that <code class=\"literal\">GROUP BY</code> expressions yielding boolean\n          results always result in two groups, regardless of the\n          expressions' contents (Tom)</p>",
    "<p>This is very substantially more accurate than the\n          regular <code class=\"literal\">GROUP BY</code> estimate\n          for certain boolean tests like <em class=\"replaceable\"><code>col</code></em> <code class=\"literal\">IS NULL</code>.</p>"
  ],
  [
    "<p>Improve <span class=\"application\">pg_dump</span> and\n          <span class=\"application\">pg_restore</span>'s error\n          reporting after failure to send a SQL command (Tom)</p>"
  ]
]