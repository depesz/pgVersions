[
  [
    "<p>Fix bugs in indexing of in-doubt HOT-updated tuples\n          (Tom Lane)</p>",
    "<p>These bugs could result in index corruption after\n          reindexing a system catalog. They are not believed to\n          affect user indexes.</p>"
  ],
  [
    "<p>Fix multiple bugs in GiST index page split processing\n          (Heikki Linnakangas)</p>",
    "<p>The probability of occurrence was low, but these could\n          lead to index corruption.</p>"
  ],
  [
    "<p>Fix possible buffer overrun in <code class=\"function\">tsvector_concat()</code> (Tom Lane)</p>",
    "<p>The function could underestimate the amount of memory\n          needed for its result, leading to server crashes.</p>"
  ],
  [
    "<p>Fix crash in <code class=\"function\">xml_recv</code>\n          when processing a <span class=\"quote\">&#x201C;<span class=\"quote\">standalone</span>&#x201D;</span> parameter (Tom\n          Lane)</p>"
  ],
  [
    "<p>Make <code class=\"function\">pg_options_to_table</code>\n          return NULL for an option with no value (Tom Lane)</p>",
    "<p>Previously such cases would result in a server\n          crash.</p>"
  ],
  [
    "<p>Avoid possibly accessing off the end of memory in\n          <code class=\"command\">ANALYZE</code> and in SJIS-2004\n          encoding conversion (Noah Misch)</p>",
    "<p>This fixes some very-low-probability server crash\n          scenarios.</p>"
  ],
  [
    "<p>Prevent intermittent hang in interactions of startup\n          process with bgwriter process (Simon Riggs)</p>",
    "<p>This affected recovery in non-hot-standby cases.</p>"
  ],
  [
    "<p>Fix race condition in relcache init file invalidation\n          (Tom Lane)</p>",
    "<p>There was a window wherein a new backend process could\n          read a stale init file but miss the inval messages that\n          would tell it the data is stale. The result would be\n          bizarre failures in catalog accesses, typically\n          <span class=\"quote\">&#x201C;<span class=\"quote\">could not read\n          block 0 in file ...</span>&#x201D;</span> later during\n          startup.</p>"
  ],
  [
    "<p>Fix memory leak at end of a GiST index scan (Tom\n          Lane)</p>",
    "<p>Commands that perform many separate GiST index scans,\n          such as verification of a new GiST-based exclusion\n          constraint on a table already containing many rows, could\n          transiently require large amounts of memory due to this\n          leak.</p>"
  ],
  [
    "<p>Fix incorrect memory accounting (leading to possible\n          memory bloat) in tuplestores supporting holdable cursors\n          and plpgsql's <code class=\"literal\">RETURN NEXT</code>\n          command (Tom Lane)</p>"
  ],
  [
    "<p>Fix performance problem when constructing a large,\n          lossy bitmap (Tom Lane)</p>"
  ],
  [
    "<p>Fix join selectivity estimation for unique columns\n          (Tom Lane)</p>",
    "<p>This fixes an erroneous planner heuristic that could\n          lead to poor estimates of the result size of a join.</p>"
  ],
  [
    "<p>Fix nested PlaceHolderVar expressions that appear only\n          in sub-select target lists (Tom Lane)</p>",
    "<p>This mistake could result in outputs of an outer join\n          incorrectly appearing as NULL.</p>"
  ],
  [
    "<p>Allow nested <code class=\"literal\">EXISTS</code>\n          queries to be optimized properly (Tom Lane)</p>"
  ],
  [
    "<p>Fix array- and path-creating functions to ensure\n          padding bytes are zeroes (Tom Lane)</p>",
    "<p>This avoids some situations where the planner will\n          think that semantically-equal constants are not equal,\n          resulting in poor optimization.</p>"
  ],
  [
    "<p>Fix <code class=\"command\">EXPLAIN</code> to handle\n          gating Result nodes within inner-indexscan subplans (Tom\n          Lane)</p>",
    "<p>The usual symptom of this oversight was <span class=\"quote\">&#x201C;<span class=\"quote\">bogus varno</span>&#x201D;</span>\n          errors.</p>"
  ],
  [
    "<p>Work around gcc 4.6.0 bug that breaks WAL replay (Tom\n          Lane)</p>",
    "<p>This could lead to loss of committed transactions\n          after a server crash.</p>"
  ],
  [
    "<p>Fix dump bug for <code class=\"literal\">VALUES</code>\n          in a view (Tom Lane)</p>"
  ],
  [
    "<p>Disallow <code class=\"literal\">SELECT FOR\n          UPDATE/SHARE</code> on sequences (Tom Lane)</p>",
    "<p>This operation doesn't work as expected and can lead\n          to failures.</p>"
  ],
  [
    "<p>Fix <code class=\"command\">VACUUM</code> so that it\n          always updates <code class=\"literal\">pg_class</code>.<code class=\"literal\">reltuples</code>/<code class=\"literal\">relpages</code> (Tom Lane)</p>",
    "<p>This fixes some scenarios where autovacuum could make\n          increasingly poor decisions about when to vacuum\n          tables.</p>"
  ],
  [
    "<p>Defend against integer overflow when computing size of\n          a hash table (Tom Lane)</p>"
  ],
  [
    "<p>Fix cases where <code class=\"command\">CLUSTER</code>\n          might attempt to access already-removed TOAST data (Tom\n          Lane)</p>"
  ],
  [
    "<p>Fix portability bugs in use of credentials control\n          messages for <span class=\"quote\">&#x201C;<span class=\"quote\">peer</span>&#x201D;</span> authentication (Tom Lane)</p>"
  ],
  [
    "<p>Fix SSPI login when multiple roundtrips are required\n          (Ahmed Shinwari, Magnus Hagander)</p>",
    "<p>The typical symptom of this problem was <span class=\"quote\">&#x201C;<span class=\"quote\">The function requested is\n          not supported</span>&#x201D;</span> errors during SSPI\n          login.</p>"
  ],
  [
    "<p>Throw an error if <code class=\"filename\">pg_hba.conf</code> contains <code class=\"literal\">hostssl</code> but SSL is disabled (Tom\n          Lane)</p>",
    "<p>This was concluded to be more user-friendly than the\n          previous behavior of silently ignoring such lines.</p>"
  ],
  [
    "<p>Fix typo in <code class=\"function\">pg_srand48</code>\n          seed initialization (Andres Freund)</p>",
    "<p>This led to failure to use all bits of the provided\n          seed. This function is not used on most platforms (only\n          those without <code class=\"function\">srandom</code>), and\n          the potential security exposure from a\n          less-random-than-expected seed seems minimal in any\n          case.</p>"
  ],
  [
    "<p>Avoid integer overflow when the sum of <code class=\"literal\">LIMIT</code> and <code class=\"literal\">OFFSET</code> values exceeds 2^63 (Heikki\n          Linnakangas)</p>"
  ],
  [
    "<p>Add overflow checks to <code class=\"type\">int4</code>\n          and <code class=\"type\">int8</code> versions of\n          <code class=\"function\">generate_series()</code> (Robert\n          Haas)</p>"
  ],
  [
    "<p>Fix trailing-zero removal in <code class=\"function\">to_char()</code> (Marti Raudsepp)</p>",
    "<p>In a format with <code class=\"literal\">FM</code> and\n          no digit positions after the decimal point, zeroes to the\n          left of the decimal point could be removed\n          incorrectly.</p>"
  ],
  [
    "<p>Fix <code class=\"function\">pg_size_pretty()</code> to\n          avoid overflow for inputs close to 2^63 (Tom Lane)</p>"
  ],
  [
    "<p>Weaken plpgsql's check for typmod matching in record\n          values (Tom Lane)</p>",
    "<p>An overly enthusiastic check could lead to discarding\n          length modifiers that should have been kept.</p>"
  ],
  [
    "<p>Correctly handle quotes in locale names during\n          <span class=\"application\">initdb</span> (Heikki\n          Linnakangas)</p>",
    "<p>The case can arise with some Windows locales, such as\n          <span class=\"quote\">&#x201C;<span class=\"quote\">People's\n          Republic of China</span>&#x201D;</span>.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_upgrade</span> to\n          preserve toast tables' relfrozenxids during an upgrade\n          from 8.3 (Bruce Momjian)</p>",
    "<p>Failure to do this could lead to <code class=\"filename\">pg_clog</code> files being removed too soon\n          after the upgrade.</p>"
  ],
  [
    "<p>In <span class=\"application\">pg_ctl</span>, support\n          silent mode for service registrations on Windows\n          (MauMau)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">psql</span>'s counting\n          of script file line numbers during <code class=\"literal\">COPY</code> from a different file (Tom\n          Lane)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_restore</span>'s\n          direct-to-database mode for <code class=\"varname\">standard_conforming_strings</code> (Tom\n          Lane)</p>",
    "<p><span class=\"application\">pg_restore</span> could emit\n          incorrect commands when restoring directly to a database\n          server from an archive file that had been made with\n          <code class=\"varname\">standard_conforming_strings</code>\n          set to <code class=\"literal\">on</code>.</p>"
  ],
  [
    "<p>Be more user-friendly about unsupported cases for\n          parallel <span class=\"application\">pg_restore</span> (Tom\n          Lane)</p>",
    "<p>This change ensures that such cases are detected and\n          reported before any restore actions have been taken.</p>"
  ],
  [
    "<p>Fix write-past-buffer-end and memory leak in\n          <span class=\"application\">libpq</span>'s LDAP service\n          lookup code (Albe Laurenz)</p>"
  ],
  [
    "<p>In <span class=\"application\">libpq</span>, avoid\n          failures when using nonblocking I/O and an SSL connection\n          (Martin Pihlak, Tom Lane)</p>"
  ],
  [
    "<p>Improve libpq's handling of failures during connection\n          startup (Tom Lane)</p>",
    "<p>In particular, the response to a server report of\n          <code class=\"function\">fork()</code> failure during SSL\n          connection startup is now saner.</p>"
  ],
  [
    "<p>Improve <span class=\"application\">libpq</span>'s error\n          reporting for SSL failures (Tom Lane)</p>"
  ],
  [
    "<p>Fix <code class=\"function\">PQsetvalue()</code> to\n          avoid possible crash when adding a new tuple to a\n          <code class=\"structname\">PGresult</code> originally\n          obtained from a server query (Andrew Chernow)</p>"
  ],
  [
    "<p>Make <span class=\"application\">ecpglib</span> write\n          <code class=\"type\">double</code> values with 15 digits\n          precision (Akira Kurosawa)</p>"
  ],
  [
    "<p>In <span class=\"application\">ecpglib</span>, be sure\n          <code class=\"literal\">LC_NUMERIC</code> setting is\n          restored after an error (Michael Meskes)</p>"
  ],
  [
    "<p>Apply upstream fix for blowfish signed-character bug\n          (CVE-2011-2483) (Tom Lane)</p>",
    "<p><code class=\"filename\">contrib/pg_crypto</code>'s\n          blowfish encryption code could give wrong results on\n          platforms where char is signed (which is most), leading\n          to encrypted passwords being weaker than they should\n          be.</p>"
  ],
  [
    "<p>Fix memory leak in <code class=\"filename\">contrib/seg</code> (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Fix <code class=\"function\">pgstatindex()</code> to\n          give consistent results for empty indexes (Tom Lane)</p>"
  ],
  [
    "<p>Allow building with perl 5.14 (Alex Hunsaker)</p>"
  ],
  [
    "<p>Update configure script's method for probing existence\n          of system functions (Tom Lane)</p>",
    "<p>The version of autoconf we used in 8.3 and 8.2 could\n          be fooled by compilers that perform link-time\n          optimization.</p>"
  ],
  [
    "<p>Fix assorted issues with build and install file paths\n          containing spaces (Tom Lane)</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2011i for DST law\n          changes in Canada, Egypt, Russia, Samoa, and South\n          Sudan.</p>"
  ]
]