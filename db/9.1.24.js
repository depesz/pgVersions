[
  [
    "<p>Fix EvalPlanQual rechecks involving CTE scans (Tom\n          Lane)</p>",
    "<p>The recheck would always see the CTE as returning no\n          rows, typically leading to failure to update rows that\n          were recently updated.</p>"
  ],
  [
    "<p>Fix improper repetition of previous results from\n          hashed aggregation in a subquery (Andrew Gierth)</p>",
    "<p>The test to see if we can reuse a previously-computed\n          hash table of the aggregate state values neglected the\n          possibility of an outer query reference appearing in an\n          aggregate argument expression. A change in the value of\n          such a reference should lead to recalculating the hash\n          table, but did not.</p>"
  ],
  [
    "<p>Fix timeout length when <code class=\"command\">VACUUM</code> is waiting for exclusive table\n          lock so that it can truncate the table (Simon Riggs)</p>",
    "<p>The timeout was meant to be 50 milliseconds, but it\n          was actually only 50 microseconds, causing <code class=\"command\">VACUUM</code> to give up on truncation much\n          more easily than intended. Set it to the intended\n          value.</p>"
  ],
  [
    "<p>Remove artificial restrictions on the values accepted\n          by <code class=\"function\">numeric_in()</code> and\n          <code class=\"function\">numeric_recv()</code> (Tom\n          Lane)</p>",
    "<p>We allow numeric values up to the limit of the storage\n          format (more than <code class=\"literal\">1e100000</code>),\n          so it seems fairly pointless that <code class=\"function\">numeric_in()</code> rejected\n          scientific-notation exponents above 1000. Likewise, it\n          was silly for <code class=\"function\">numeric_recv()</code> to reject more than 1000\n          digits in an input value.</p>"
  ],
  [
    "<p>Avoid very-low-probability data corruption due to\n          testing tuple visibility without holding buffer lock\n          (Thomas Munro, Peter Geoghegan, Tom Lane)</p>"
  ],
  [
    "<p>Fix file descriptor leakage when truncating a\n          temporary relation of more than 1GB (Andres Freund)</p>"
  ],
  [
    "<p>Disallow starting a standalone backend with\n          <code class=\"literal\">standby_mode</code> turned on\n          (Michael Paquier)</p>",
    "<p>This can't do anything useful, since there will be no\n          WAL receiver process to fetch more WAL data; and it could\n          result in misbehavior in code that wasn't designed with\n          this situation in mind.</p>"
  ],
  [
    "<p>Don't try to share SSL contexts across multiple\n          connections in <span class=\"application\">libpq</span>\n          (Heikki Linnakangas)</p>",
    "<p>This led to assorted corner-case bugs, particularly\n          when trying to use different SSL parameters for different\n          connections.</p>"
  ],
  [
    "<p>Avoid corner-case memory leak in <span class=\"application\">libpq</span> (Tom Lane)</p>",
    "<p>The reported problem involved leaking an error report\n          during <code class=\"function\">PQreset()</code>, but there\n          might be related cases.</p>"
  ],
  [
    "<p>Make <span class=\"application\">ecpg</span>'s\n          <code class=\"option\">--help</code> and <code class=\"option\">--version</code> options work consistently with\n          our other executables (Haribabu Kommi)</p>"
  ],
  [
    "<p>Fix <code class=\"filename\">contrib/intarray/bench/bench.pl</code> to\n          print the results of the <code class=\"command\">EXPLAIN</code> it does when given the\n          <code class=\"option\">-e</code> option (Daniel\n          Gustafsson)</p>"
  ],
  [
    "<p>Prevent failure of obsolete dynamic time zone\n          abbreviations (Tom Lane)</p>",
    "<p>If a dynamic time zone abbreviation does not match any\n          entry in the referenced time zone, treat it as equivalent\n          to the time zone name. This avoids unexpected failures\n          when IANA removes abbreviations from their time zone\n          database, as they did in <span class=\"application\">tzdata</span> release 2016f and seem likely\n          to do again in the future. The consequences were not\n          limited to not recognizing the individual abbreviation;\n          any mismatch caused the <code class=\"structname\">pg_timezone_abbrevs</code> view to fail\n          altogether.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2016h for DST law\n          changes in Palestine and Turkey, plus historical\n          corrections for Turkey and some regions of Russia. Switch\n          to numeric abbreviations for some time zones in\n          Antarctica, the former Soviet Union, and Sri Lanka.</p>",
    "<p>The IANA time zone database previously provided\n          textual abbreviations for all time zones, sometimes\n          making up abbreviations that have little or no currency\n          among the local population. They are in process of\n          reversing that policy in favor of using numeric UTC\n          offsets in zones where there is no evidence of real-world\n          use of an English abbreviation. At least for the time\n          being, <span class=\"productname\">PostgreSQL</span> will\n          continue to accept such removed abbreviations for\n          timestamp input. But they will not be shown in the\n          <code class=\"structname\">pg_timezone_names</code> view\n          nor used for output.</p>",
    "<p>In this update, <code class=\"literal\">AMT</code> is no\n          longer shown as being in use to mean Armenia Time.\n          Therefore, we have changed the <code class=\"literal\">Default</code> abbreviation set to interpret it\n          as Amazon Time, thus UTC-4 not UTC+4.</p>"
  ]
]