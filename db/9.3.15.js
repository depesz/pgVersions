[
  [
    "<p>Fix WAL-logging of truncation of relation free space\n          maps and visibility maps (Pavan Deolasee, Heikki\n          Linnakangas)</p>",
    "<p>It was possible for these files to not be correctly\n          restored during crash recovery, or to be written\n          incorrectly on a standby server. Bogus entries in a free\n          space map could lead to attempts to access pages that\n          have been truncated away from the relation itself,\n          typically producing errors like <span class=\"quote\">&#x201C;<span class=\"quote\">could not read block\n          <em class=\"replaceable\"><code>XXX</code></em>: read only\n          0 of 8192 bytes</span>&#x201D;</span>. Checksum failures in the\n          visibility map are also possible, if checksumming is\n          enabled.</p>",
    "<p>Procedures for determining whether there is a problem\n          and repairing it if so are discussed at <a class=\"ulink\" href=\"https://wiki.postgresql.org/wiki/Free_Space_Map_Problems\" target=\"_top\">https://wiki.postgresql.org/wiki/Free_Space_Map_Problems</a>.</p>"
  ],
  [
    "<p>Fix <code class=\"command\">SELECT FOR\n          UPDATE/SHARE</code> to correctly lock tuples that have\n          been updated by a subsequently-aborted transaction\n          (&#xC1;lvaro Herrera)</p>",
    "<p>In 9.5 and later, the <code class=\"command\">SELECT</code> would sometimes fail to return\n          such tuples at all. A failure has not been proven to\n          occur in earlier releases, but might be possible with\n          concurrent updates.</p>"
  ],
  [
    "<p>Fix EvalPlanQual rechecks involving CTE scans (Tom\n          Lane)</p>",
    "<p>The recheck would always see the CTE as returning no\n          rows, typically leading to failure to update rows that\n          were recently updated.</p>"
  ],
  [
    "<p>Fix improper repetition of previous results from\n          hashed aggregation in a subquery (Andrew Gierth)</p>",
    "<p>The test to see if we can reuse a previously-computed\n          hash table of the aggregate state values neglected the\n          possibility of an outer query reference appearing in an\n          aggregate argument expression. A change in the value of\n          such a reference should lead to recalculating the hash\n          table, but did not.</p>"
  ],
  [
    "<p>Fix <code class=\"command\">EXPLAIN</code> to emit valid\n          XML when <a class=\"xref\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-statistics.html#GUC-TRACK-IO-TIMING\">track_io_timing</a>\n          is on (Markus Winand)</p>",
    "<p>Previously the XML output-format option produced\n          syntactically invalid tags such as <code class=\"literal\">&lt;I/O-Read-Time&gt;</code>. That is now\n          rendered as <code class=\"literal\">&lt;I-O-Read-Time&gt;</code>.</p>"
  ],
  [
    "<p>Suppress printing of zeroes for unmeasured times in\n          <code class=\"command\">EXPLAIN</code> (Maksim\n          Milyutin)</p>",
    "<p>Certain option combinations resulted in printing zero\n          values for times that actually aren't ever measured in\n          that combination. Our general policy in <code class=\"command\">EXPLAIN</code> is not to print such fields at\n          all, so do that consistently in all cases.</p>"
  ],
  [
    "<p>Fix timeout length when <code class=\"command\">VACUUM</code> is waiting for exclusive table\n          lock so that it can truncate the table (Simon Riggs)</p>",
    "<p>The timeout was meant to be 50 milliseconds, but it\n          was actually only 50 microseconds, causing <code class=\"command\">VACUUM</code> to give up on truncation much\n          more easily than intended. Set it to the intended\n          value.</p>"
  ],
  [
    "<p>Fix bugs in merging inherited <code class=\"literal\">CHECK</code> constraints while creating or\n          altering a table (Tom Lane, Amit Langote)</p>",
    "<p>Allow identical <code class=\"literal\">CHECK</code>\n          constraints to be added to a parent and child table in\n          either order. Prevent merging of a valid constraint from\n          the parent table with a <code class=\"literal\">NOT\n          VALID</code> constraint on the child. Likewise, prevent\n          merging of a <code class=\"literal\">NO INHERIT</code>\n          child constraint with an inherited constraint.</p>"
  ],
  [
    "<p>Remove artificial restrictions on the values accepted\n          by <code class=\"function\">numeric_in()</code> and\n          <code class=\"function\">numeric_recv()</code> (Tom\n          Lane)</p>",
    "<p>We allow numeric values up to the limit of the storage\n          format (more than <code class=\"literal\">1e100000</code>),\n          so it seems fairly pointless that <code class=\"function\">numeric_in()</code> rejected\n          scientific-notation exponents above 1000. Likewise, it\n          was silly for <code class=\"function\">numeric_recv()</code> to reject more than 1000\n          digits in an input value.</p>"
  ],
  [
    "<p>Avoid very-low-probability data corruption due to\n          testing tuple visibility without holding buffer lock\n          (Thomas Munro, Peter Geoghegan, Tom Lane)</p>"
  ],
  [
    "<p>Fix file descriptor leakage when truncating a\n          temporary relation of more than 1GB (Andres Freund)</p>"
  ],
  [
    "<p>Disallow starting a standalone backend with\n          <code class=\"literal\">standby_mode</code> turned on\n          (Michael Paquier)</p>",
    "<p>This can't do anything useful, since there will be no\n          WAL receiver process to fetch more WAL data; and it could\n          result in misbehavior in code that wasn't designed with\n          this situation in mind.</p>"
  ],
  [
    "<p>Don't try to share SSL contexts across multiple\n          connections in <span class=\"application\">libpq</span>\n          (Heikki Linnakangas)</p>",
    "<p>This led to assorted corner-case bugs, particularly\n          when trying to use different SSL parameters for different\n          connections.</p>"
  ],
  [
    "<p>Avoid corner-case memory leak in <span class=\"application\">libpq</span> (Tom Lane)</p>",
    "<p>The reported problem involved leaking an error report\n          during <code class=\"function\">PQreset()</code>, but there\n          might be related cases.</p>"
  ],
  [
    "<p>Make <span class=\"application\">ecpg</span>'s\n          <code class=\"option\">--help</code> and <code class=\"option\">--version</code> options work consistently with\n          our other executables (Haribabu Kommi)</p>"
  ],
  [
    "<p>In <span class=\"application\">pg_dump</span>, never\n          dump range constructor functions (Tom Lane)</p>",
    "<p>This oversight led to <span class=\"application\">pg_upgrade</span> failures with extensions\n          containing range types, due to duplicate creation of the\n          constructor functions.</p>"
  ],
  [
    "<p>In <span class=\"application\">pg_xlogdump</span>, retry\n          opening new WAL segments when using <code class=\"option\">--follow</code> option (Magnus Hagander)</p>",
    "<p>This allows for a possible delay in the server's\n          creation of the next segment.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_xlogdump</span> to\n          cope with a WAL file that begins with a continuation\n          record spanning more than one page (Pavan Deolasee)</p>"
  ],
  [
    "<p>Fix <code class=\"filename\">contrib/intarray/bench/bench.pl</code> to\n          print the results of the <code class=\"command\">EXPLAIN</code> it does when given the\n          <code class=\"option\">-e</code> option (Daniel\n          Gustafsson)</p>"
  ],
  [
    "<p>Update Windows time zone mapping to recognize some\n          time zone names added in recent Windows versions (Michael\n          Paquier)</p>"
  ],
  [
    "<p>Prevent failure of obsolete dynamic time zone\n          abbreviations (Tom Lane)</p>",
    "<p>If a dynamic time zone abbreviation does not match any\n          entry in the referenced time zone, treat it as equivalent\n          to the time zone name. This avoids unexpected failures\n          when IANA removes abbreviations from their time zone\n          database, as they did in <span class=\"application\">tzdata</span> release 2016f and seem likely\n          to do again in the future. The consequences were not\n          limited to not recognizing the individual abbreviation;\n          any mismatch caused the <code class=\"structname\">pg_timezone_abbrevs</code> view to fail\n          altogether.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2016h for DST law\n          changes in Palestine and Turkey, plus historical\n          corrections for Turkey and some regions of Russia. Switch\n          to numeric abbreviations for some time zones in\n          Antarctica, the former Soviet Union, and Sri Lanka.</p>",
    "<p>The IANA time zone database previously provided\n          textual abbreviations for all time zones, sometimes\n          making up abbreviations that have little or no currency\n          among the local population. They are in process of\n          reversing that policy in favor of using numeric UTC\n          offsets in zones where there is no evidence of real-world\n          use of an English abbreviation. At least for the time\n          being, <span class=\"productname\">PostgreSQL</span> will\n          continue to accept such removed abbreviations for\n          timestamp input. But they will not be shown in the\n          <code class=\"structname\">pg_timezone_names</code> view\n          nor used for output.</p>",
    "<p>In this update, <code class=\"literal\">AMT</code> is no\n          longer shown as being in use to mean Armenia Time.\n          Therefore, we have changed the <code class=\"literal\">Default</code> abbreviation set to interpret it\n          as Amazon Time, thus UTC-4 not UTC+4.</p>"
  ]
]