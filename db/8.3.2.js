[
  [
    "<p>Fix <code class=\"literal\">ERRORDATA_STACK_SIZE\n          exceeded</code> crash that occurred on Windows when using\n          UTF-8 database encoding and a different client encoding\n          (Tom)</p>"
  ],
  [
    "<p>Fix incorrect archive truncation point calculation for\n          the <code class=\"literal\">%r</code> macro in <code class=\"varname\">restore_command</code> parameters (Simon)</p>",
    "<p>This could lead to data loss if a warm-standby script\n          relied on <code class=\"literal\">%r</code> to decide when\n          to throw away WAL segment files.</p>"
  ],
  [
    "<p>Fix <code class=\"command\">ALTER TABLE ADD COLUMN ...\n          PRIMARY KEY</code> so that the new column is correctly\n          checked to see if it's been initialized to all non-nulls\n          (Brendan Jurd)</p>",
    "<p>Previous versions neglected to check this requirement\n          at all.</p>"
  ],
  [
    "<p>Fix <code class=\"command\">REASSIGN OWNED</code> so\n          that it works on procedural languages too (Alvaro)</p>"
  ],
  [
    "<p>Fix problems with <code class=\"command\">SELECT FOR\n          UPDATE/SHARE</code> occurring as a subquery in a query\n          with a non-<code class=\"command\">SELECT</code> top-level\n          operation (Tom)</p>"
  ],
  [
    "<p>Fix possible <code class=\"command\">CREATE TABLE</code>\n          failure when inheriting the <span class=\"quote\">&#x201C;<span class=\"quote\">same</span>&#x201D;</span>\n          constraint from multiple parent relations that inherited\n          that constraint from a common ancestor (Tom)</p>"
  ],
  [
    "<p>Fix <code class=\"function\">pg_get_ruledef()</code> to\n          show the alias, if any, attached to the target table of\n          an <code class=\"command\">UPDATE</code> or <code class=\"command\">DELETE</code> (Tom)</p>"
  ],
  [
    "<p>Restore the pre-8.3 behavior that an out-of-range\n          block number in a TID being used in a TidScan plan\n          results in silently not matching any rows (Tom)</p>",
    "<p>8.3.0 and 8.3.1 threw an error instead.</p>"
  ],
  [
    "<p>Fix GIN bug that could result in a <code class=\"literal\">too many LWLocks taken</code> failure\n          (Teodor)</p>"
  ],
  [
    "<p>Fix broken GiST comparison function for <code class=\"type\">tsquery</code> (Teodor)</p>"
  ],
  [
    "<p>Fix <code class=\"function\">tsvector_update_trigger()</code> and\n          <code class=\"function\">ts_stat()</code> to accept domains\n          over the types they expect to work with (Tom)</p>"
  ],
  [
    "<p>Fix failure to support enum data types as foreign keys\n          (Tom)</p>"
  ],
  [
    "<p>Avoid possible crash when decompressing corrupted data\n          (Zdenek Kotala)</p>"
  ],
  [
    "<p>Fix race conditions between delayed unlinks and\n          <code class=\"command\">DROP DATABASE</code> (Heikki)</p>",
    "<p>In the worst case this could result in deleting a\n          newly created table in a new database that happened to\n          get the same OID as the recently-dropped one; but of\n          course that is an extremely low-probability scenario.</p>"
  ],
  [
    "<p>Repair two places where SIGTERM exit of a backend\n          could leave corrupted state in shared memory (Tom)</p>",
    "<p>Neither case is very important if SIGTERM is used to\n          shut down the whole database cluster together, but there\n          was a problem if someone tried to SIGTERM individual\n          backends.</p>"
  ],
  [
    "<p>Fix possible crash due to incorrect plan generated for\n          an <code class=\"literal\"><em class=\"replaceable\"><code>x</code></em> IN (SELECT <em class=\"replaceable\"><code>y</code></em> FROM ...)</code> clause\n          when <em class=\"replaceable\"><code>x</code></em> and\n          <em class=\"replaceable\"><code>y</code></em> have\n          different data types; and make sure the behavior is\n          semantically correct when the conversion from <em class=\"replaceable\"><code>y</code></em>'s type to <em class=\"replaceable\"><code>x</code></em>'s type is lossy\n          (Tom)</p>"
  ],
  [
    "<p>Fix oversight that prevented the planner from\n          substituting known Param values as if they were constants\n          (Tom)</p>",
    "<p>This mistake partially disabled optimization of\n          unnamed extended-Query statements in 8.3.0 and 8.3.1: in\n          particular the LIKE-to-indexscan optimization would never\n          be applied if the LIKE pattern was passed as a parameter,\n          and constraint exclusion depending on a parameter value\n          didn't work either.</p>"
  ],
  [
    "<p>Fix planner failure when an indexable <code class=\"function\">MIN</code> or <code class=\"function\">MAX</code> aggregate is used with <code class=\"literal\">DISTINCT</code> or <code class=\"literal\">ORDER\n          BY</code> (Tom)</p>"
  ],
  [
    "<p>Fix planner to ensure it never uses a <span class=\"quote\">&#x201C;<span class=\"quote\">physical\n          tlist</span>&#x201D;</span> for a plan node that is feeding a\n          Sort node (Tom)</p>",
    "<p>This led to the sort having to push around more data\n          than it really needed to, since unused column values were\n          included in the sorted data.</p>"
  ],
  [
    "<p>Avoid unnecessary copying of query strings (Tom)</p>",
    "<p>This fixes a performance problem introduced in 8.3.0\n          when a very large number of commands are submitted as a\n          single query string.</p>"
  ],
  [
    "<p>Make <code class=\"function\">TransactionIdIsCurrentTransactionId()</code>\n          use binary search instead of linear search when checking\n          child-transaction XIDs (Heikki)</p>",
    "<p>This fixes some cases in which 8.3.0 was significantly\n          slower than earlier releases.</p>"
  ],
  [
    "<p>Fix conversions between ISO-8859-5 and other encodings\n          to handle Cyrillic <span class=\"quote\">&#x201C;<span class=\"quote\">Yo</span>&#x201D;</span> characters (<code class=\"literal\">e</code> and <code class=\"literal\">E</code>\n          with two dots) (Sergey Burladyan)</p>"
  ],
  [
    "<p>Fix several datatype input functions, notably\n          <code class=\"function\">array_in()</code>, that were\n          allowing unused bytes in their results to contain\n          uninitialized, unpredictable values (Tom)</p>",
    "<p>This could lead to failures in which two apparently\n          identical literal values were not seen as equal,\n          resulting in the parser complaining about unmatched\n          <code class=\"literal\">ORDER BY</code> and <code class=\"literal\">DISTINCT</code> expressions.</p>"
  ],
  [
    "<p>Fix a corner case in regular-expression substring\n          matching (<code class=\"literal\">substring(<em class=\"replaceable\"><code>string</code></em> from <em class=\"replaceable\"><code>pattern</code></em>)</code>)\n          (Tom)</p>",
    "<p>The problem occurs when there is a match to the\n          pattern overall but the user has specified a\n          parenthesized subexpression and that subexpression hasn't\n          got a match. An example is <code class=\"literal\">substring('foo' from 'foo(bar)?')</code>. This\n          should return NULL, since <code class=\"literal\">(bar)</code> isn't matched, but it was\n          mistakenly returning the whole-pattern match instead (ie,\n          <code class=\"literal\">foo</code>).</p>"
  ],
  [
    "<p>Prevent cancellation of an auto-vacuum that was\n          launched to prevent XID wraparound (Alvaro)</p>"
  ],
  [
    "<p>Improve <code class=\"command\">ANALYZE</code>'s\n          handling of in-doubt tuples (those inserted or deleted by\n          a not-yet-committed transaction) so that the counts it\n          reports to the stats collector are more likely to be\n          correct (Pavan Deolasee)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">initdb</span> to reject\n          a relative path for its <code class=\"literal\">--xlogdir</code> (<code class=\"literal\">-X</code>) option (Tom)</p>"
  ],
  [
    "<p>Make <span class=\"application\">psql</span> print tab\n          characters as an appropriate number of spaces, rather\n          than <code class=\"literal\">\\x09</code> as was done in\n          8.3.0 and 8.3.1 (Bruce)</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2008c (for DST law\n          changes in Morocco, Iraq, Choibalsan, Pakistan, Syria,\n          Cuba, and Argentina/San_Luis)</p>"
  ],
  [
    "<p>Add <code class=\"function\">ECPGget_PGconn()</code>\n          function to <span class=\"application\">ecpglib</span>\n          (Michael)</p>"
  ],
  [
    "<p>Fix incorrect result from <span class=\"application\">ecpg</span>'s <code class=\"function\">PGTYPEStimestamp_sub()</code> function\n          (Michael)</p>"
  ],
  [
    "<p>Fix handling of continuation line markers in\n          <span class=\"application\">ecpg</span> (Michael)</p>"
  ],
  [
    "<p>Fix possible crashes in <code class=\"filename\">contrib/cube</code> functions (Tom)</p>"
  ],
  [
    "<p>Fix core dump in <code class=\"filename\">contrib/xml2</code>'s <code class=\"function\">xpath_table()</code> function when the input\n          query returns a NULL value (Tom)</p>"
  ],
  [
    "<p>Fix <code class=\"filename\">contrib/xml2</code>'s\n          makefile to not override <code class=\"literal\">CFLAGS</code>, and make it auto-configure\n          properly for <span class=\"application\">libxslt</span>\n          present or not (Tom)</p>"
  ]
]