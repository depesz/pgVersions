[
  [
    "<p>Protect against indirect security threats caused by\n          index functions changing session-local state (Gurjeet\n          Singh, Tom)</p>",
    "<p>This change prevents allegedly-immutable index\n          functions from possibly subverting a superuser's session\n          (CVE-2009-4136).</p>"
  ],
  [
    "<p>Reject SSL certificates containing an embedded null\n          byte in the common name (CN) field (Magnus)</p>",
    "<p>This prevents unintended matching of a certificate to\n          a server or client name during SSL validation\n          (CVE-2009-4034).</p>"
  ],
  [
    "<p>Fix possible crash during backend-startup-time cache\n          initialization (Tom)</p>"
  ],
  [
    "<p>Prevent signals from interrupting <code class=\"literal\">VACUUM</code> at unsafe times (Alvaro)</p>",
    "<p>This fix prevents a PANIC if a <code class=\"literal\">VACUUM FULL</code> is canceled after it's\n          already committed its tuple movements, as well as\n          transient errors if a plain <code class=\"literal\">VACUUM</code> is interrupted after having\n          truncated the table.</p>"
  ],
  [
    "<p>Fix possible crash due to integer overflow in hash\n          table size calculation (Tom)</p>",
    "<p>This could occur with extremely large planner\n          estimates for the size of a hashjoin's result.</p>"
  ],
  [
    "<p>Fix very rare crash in <code class=\"type\">inet</code>/<code class=\"type\">cidr</code>\n          comparisons (Chris Mikkelson)</p>"
  ],
  [
    "<p>Fix PAM password processing to be more robust\n          (Tom)</p>",
    "<p>The previous code is known to fail with the\n          combination of the Linux <code class=\"literal\">pam_krb5</code> PAM module with Microsoft\n          Active Directory as the domain controller. It might have\n          problems elsewhere too, since it was making unjustified\n          assumptions about what arguments the PAM stack would pass\n          to it.</p>"
  ],
  [
    "<p>Make the postmaster ignore any <code class=\"literal\">application_name</code> parameter in connection\n          request packets, to improve compatibility with future\n          libpq versions (Tom)</p>"
  ]
]