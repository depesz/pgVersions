[
  [
    "<p>Clear the OpenSSL error queue before OpenSSL calls,\n          rather than assuming it's clear already; and make sure we\n          leave it clear afterwards (Peter Geoghegan, Dave Vitek,\n          Peter Eisentraut)</p>",
    "<p>This change prevents problems when there are multiple\n          connections using OpenSSL within a single process and not\n          all the code involved follows the same rules for when to\n          clear the error queue. Failures have been reported\n          specifically when a client application uses SSL\n          connections in <span class=\"application\">libpq</span>\n          concurrently with SSL connections using the PHP, Python,\n          or Ruby wrappers for OpenSSL. It's possible for similar\n          problems to arise within the server as well, if an\n          extension module establishes an outgoing SSL\n          connection.</p>"
  ],
  [
    "<p>Fix <span class=\"quote\">&#x201C;<span class=\"quote\">failed to\n          build any <em class=\"replaceable\"><code>N</code></em>-way\n          joins</span>&#x201D;</span> planner error with a full join\n          enclosed in the right-hand side of a left join (Tom\n          Lane)</p>"
  ],
  [
    "<p>Fix possible misbehavior of <code class=\"literal\">TH</code>, <code class=\"literal\">th</code>, and\n          <code class=\"literal\">Y,YYY</code> format codes in\n          <code class=\"function\">to_timestamp()</code> (Tom\n          Lane)</p>",
    "<p>These could advance off the end of the input string,\n          causing subsequent format codes to read garbage.</p>"
  ],
  [
    "<p>Fix dumping of rules and views in which the <em class=\"replaceable\"><code>array</code></em> argument of a\n          <code class=\"literal\"><em class=\"replaceable\"><code>value</code></em> <em class=\"replaceable\"><code>operator</code></em> ANY (<em class=\"replaceable\"><code>array</code></em>)</code> construct\n          is a sub-SELECT (Tom Lane)</p>"
  ],
  [
    "<p>Make <span class=\"application\">pg_regress</span> use a\n          startup timeout from the <code class=\"envar\">PGCTLTIMEOUT</code> environment variable, if\n          that's set (Tom Lane)</p>",
    "<p>This is for consistency with a behavior recently added\n          to <span class=\"application\">pg_ctl</span>; it eases\n          automated testing on slow machines.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_upgrade</span> to\n          correctly restore extension membership for operator\n          families containing only one operator class (Tom\n          Lane)</p>",
    "<p>In such a case, the operator family was restored into\n          the new database, but it was no longer marked as part of\n          the extension. This had no immediate ill effects, but\n          would cause later <span class=\"application\">pg_dump</span> runs to emit output that\n          would cause (harmless) errors on restore.</p>"
  ],
  [
    "<p>Rename internal function <code class=\"function\">strtoi()</code> to <code class=\"function\">strtoint()</code> to avoid conflict with a\n          NetBSD library function (Thomas Munro)</p>"
  ],
  [
    "<p>Fix reporting of errors from <code class=\"function\">bind()</code> and <code class=\"function\">listen()</code> system calls on Windows (Tom\n          Lane)</p>"
  ],
  [
    "<p>Reduce verbosity of compiler output when building with\n          Microsoft Visual Studio (Christian Ullrich)</p>"
  ],
  [
    "<p>Avoid possibly-unsafe use of Windows' <code class=\"function\">FormatMessage()</code> function (Christian\n          Ullrich)</p>",
    "<p>Use the <code class=\"literal\">FORMAT_MESSAGE_IGNORE_INSERTS</code> flag where\n          appropriate. No live bug is known to exist here, but it\n          seems like a good idea to be careful.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2016d for DST law\n          changes in Russia and Venezuela. There are new zone names\n          <code class=\"literal\">Europe/Kirov</code> and\n          <code class=\"literal\">Asia/Tomsk</code> to reflect the\n          fact that these regions now have different time zone\n          histories from adjacent regions.</p>"
  ]
]