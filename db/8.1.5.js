[
  [
    "<p>Disallow aggregate functions in <code class=\"command\">UPDATE</code> commands, except within\n          sub-SELECTs (Tom)</p>",
    "<p>The behavior of such an aggregate was unpredictable,\n          and in 8.1.X could cause a crash, so it has been\n          disabled. The SQL standard does not allow this\n          either.</p>"
  ],
  [
    "<p>Fix core dump when an untyped literal is taken as\n          ANYARRAY</p>"
  ],
  [
    "<p>Fix core dump in duration logging for extended query\n          protocol when a <code class=\"command\">COMMIT</code> or\n          <code class=\"command\">ROLLBACK</code> is executed</p>"
  ],
  [
    "<p>Fix mishandling of AFTER triggers when query contains\n          a SQL function returning multiple rows (Tom)</p>"
  ],
  [
    "<p>Fix <code class=\"command\">ALTER TABLE ... TYPE</code>\n          to recheck <code class=\"literal\">NOT NULL</code> for\n          <code class=\"literal\">USING</code> clause (Tom)</p>"
  ],
  [
    "<p>Fix <code class=\"function\">string_to_array()</code> to\n          handle overlapping matches for the separator string</p>",
    "<p>For example, <code class=\"literal\">string_to_array('123xx456xxx789',\n          'xx')</code>.</p>"
  ],
  [
    "<p>Fix <code class=\"function\">to_timestamp()</code> for\n          <code class=\"literal\">AM</code>/<code class=\"literal\">PM</code> formats (Bruce)</p>"
  ],
  [
    "<p>Fix autovacuum's calculation that decides whether\n          <code class=\"command\">ANALYZE</code> is needed\n          (Alvaro)</p>"
  ],
  [
    "<p>Fix corner cases in pattern matching for <span class=\"application\">psql</span>'s <code class=\"literal\">\\d</code> commands</p>"
  ],
  [
    "<p>Fix index-corrupting bugs in /contrib/ltree\n          (Teodor)</p>"
  ],
  [
    "<p>Numerous robustness fixes in <span class=\"application\">ecpg</span> (Joachim Wieland)</p>"
  ],
  [
    "<p>Fix backslash escaping in /contrib/dbmirror</p>"
  ],
  [
    "<p>Minor fixes in /contrib/dblink and\n          /contrib/tsearch2</p>"
  ],
  [
    "<p>Efficiency improvements in hash tables and bitmap\n          index scans (Tom)</p>"
  ],
  [
    "<p>Fix instability of statistics collection on Windows\n          (Tom, Andrew)</p>"
  ],
  [
    "<p>Fix <code class=\"varname\">statement_timeout</code> to\n          use the proper units on Win32 (Bruce)</p>",
    "<p>In previous Win32 8.1.X versions, the delay was off by\n          a factor of 100.</p>"
  ],
  [
    "<p>Fixes for <acronym class=\"acronym\">MSVC</acronym> and\n          <span class=\"productname\">Borland C++</span> compilers\n          (Hiroshi Saito)</p>"
  ],
  [
    "<p>Fixes for <span class=\"systemitem\">AIX</span> and\n          <span class=\"productname\">Intel</span> compilers\n          (Tom)</p>"
  ],
  [
    "<p>Fix rare bug in continuous archiving (Tom)</p>"
  ]
]