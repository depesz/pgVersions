[
  [
    "<p>Improve behavior of <span class=\"application\">libpq</span>'s quoting functions (Andres Freund, Tom Lane) <a class=\"ulink\" href=\"https://postgr.es/c/111f4dd27\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/991a60a9f\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/644b7d686\" target=\"_top\">§</a></p>",
    "<p>The changes made for CVE-2025-1094 had one serious oversight: <code class=\"function\">PQescapeLiteral()</code> and <code class=\"function\">PQescapeIdentifier()</code> failed to honor their string length parameter, instead always reading to the input string's trailing null. This resulted in including unwanted text in the output, if the caller intended to truncate the string via the length parameter. With very bad luck it could cause a crash due to reading off the end of memory.</p>",
    "<p>In addition, modify all these quoting functions so that when invalid encoding is detected, an invalid sequence is substituted for just the first byte of the presumed character, not all of it. This reduces the risk of problems if a calling application performs additional processing on the quoted string.</p>"
  ],
  [
    "<p>Fix meson build system to correctly detect availability of the <code class=\"filename\">bsd_auth.h</code> system header (Nazir Bilal Yavuz) <a class=\"ulink\" href=\"https://postgr.es/c/01cdb98e4\" target=\"_top\">§</a></p>"
  ]
]