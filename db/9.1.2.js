[
  [
    "<p>Fix bugs in <code class=\"literal\">information_schema.referential_constraints</code>\n          view (Tom Lane)</p>",
    "<p>This view was being insufficiently careful about\n          matching the foreign-key constraint to the depended-on\n          primary or unique key constraint. That could result in\n          failure to show a foreign key constraint at all, or\n          showing it multiple times, or claiming that it depends on\n          a different constraint than the one it really does.</p>",
    "<p>Since the view definition is installed by <span class=\"application\">initdb</span>, merely upgrading will not\n          fix the problem. If you need to fix this in an existing\n          installation, you can (as a superuser) drop the\n          <code class=\"literal\">information_schema</code> schema\n          then re-create it by sourcing <code class=\"filename\"><em class=\"replaceable\"><code>SHAREDIR</code></em>/information_schema.sql</code>.\n          (Run <code class=\"literal\">pg_config --sharedir</code> if\n          you're uncertain where <em class=\"replaceable\"><code>SHAREDIR</code></em> is.) This must\n          be repeated in each database to be fixed.</p>"
  ],
  [
    "<p>Make <code class=\"filename\">contrib/citext</code>'s\n          upgrade script fix collations of <code class=\"type\">citext</code> columns and indexes (Tom Lane)</p>",
    "<p>Existing <code class=\"type\">citext</code> columns and\n          indexes aren't correctly marked as being of a collatable\n          data type during <span class=\"application\">pg_upgrade</span> from a pre-9.1 server, or\n          when a pre-9.1 dump containing the <code class=\"type\">citext</code> type is loaded into a 9.1 server.\n          That leads to operations on these columns failing with\n          errors such as <span class=\"quote\">&#x201C;<span class=\"quote\">could not determine which collation to use for\n          string comparison</span>&#x201D;</span>. This change allows them\n          to be fixed by the same script that upgrades the\n          <code class=\"type\">citext</code> module into a proper 9.1\n          extension during <code class=\"literal\">CREATE EXTENSION\n          citext FROM unpackaged</code>.</p>",
    "<p>If you have a previously-upgraded database that is\n          suffering from this problem, and you already ran the\n          <code class=\"command\">CREATE EXTENSION</code> command,\n          you can manually run (as superuser) the <code class=\"command\">UPDATE</code> commands found at the end of\n          <code class=\"filename\"><em class=\"replaceable\"><code>SHAREDIR</code></em>/extension/citext--unpackaged--1.0.sql</code>.\n          (Run <code class=\"literal\">pg_config --sharedir</code> if\n          you're uncertain where <em class=\"replaceable\"><code>SHAREDIR</code></em> is.) There is no\n          harm in doing this again if unsure.</p>"
  ],
  [
    "<p>Fix possible crash during <code class=\"command\">UPDATE</code> or <code class=\"command\">DELETE</code> that joins to the output of a\n          scalar-returning function (Tom Lane)</p>",
    "<p>A crash could only occur if the target row had been\n          concurrently updated, so this problem surfaced only\n          intermittently.</p>"
  ],
  [
    "<p>Fix incorrect replay of WAL records for GIN index\n          updates (Tom Lane)</p>",
    "<p>This could result in transiently failing to find index\n          entries after a crash, or on a hot-standby server. The\n          problem would be repaired by the next <code class=\"command\">VACUUM</code> of the index, however.</p>"
  ],
  [
    "<p>Fix TOAST-related data corruption during <code class=\"literal\">CREATE TABLE dest AS SELECT * FROM src</code>\n          or <code class=\"literal\">INSERT INTO dest SELECT * FROM\n          src</code> (Tom Lane)</p>",
    "<p>If a table has been modified by <code class=\"command\">ALTER TABLE ADD COLUMN</code>, attempts to copy\n          its data verbatim to another table could produce corrupt\n          results in certain corner cases. The problem can only\n          manifest in this precise form in 8.4 and later, but we\n          patched earlier versions as well in case there are other\n          code paths that could trigger the same bug.</p>"
  ],
  [
    "<p>Fix possible failures during hot standby startup\n          (Simon Riggs)</p>"
  ],
  [
    "<p>Start hot standby faster when initial snapshot is\n          incomplete (Simon Riggs)</p>"
  ],
  [
    "<p>Fix race condition during toast table access from\n          stale syscache entries (Tom Lane)</p>",
    "<p>The typical symptom was transient errors like\n          <span class=\"quote\">&#x201C;<span class=\"quote\">missing chunk\n          number 0 for toast value NNNNN in\n          pg_toast_2619</span>&#x201D;</span>, where the cited toast table\n          would always belong to a system catalog.</p>"
  ],
  [
    "<p>Track dependencies of functions on items used in\n          parameter default expressions (Tom Lane)</p>",
    "<p>Previously, a referenced object could be dropped\n          without having dropped or modified the function, leading\n          to misbehavior when the function was used. Note that\n          merely installing this update will not fix the missing\n          dependency entries; to do that, you'd need to\n          <code class=\"command\">CREATE OR REPLACE</code> each such\n          function afterwards. If you have functions whose defaults\n          depend on non-built-in objects, doing so is\n          recommended.</p>"
  ],
  [
    "<p>Fix incorrect management of placeholder variables in\n          nestloop joins (Tom Lane)</p>",
    "<p>This bug is known to lead to <span class=\"quote\">&#x201C;<span class=\"quote\">variable not found in\n          subplan target list</span>&#x201D;</span> planner errors, and\n          could possibly result in wrong query output when outer\n          joins are involved.</p>"
  ],
  [
    "<p>Fix window functions that sort by expressions\n          involving aggregates (Tom Lane)</p>",
    "<p>Previously these could fail with <span class=\"quote\">&#x201C;<span class=\"quote\">could not find pathkey item\n          to sort</span>&#x201D;</span> planner errors.</p>"
  ],
  [
    "<p>Fix <span class=\"quote\">&#x201C;<span class=\"quote\">MergeAppend child's targetlist doesn't match\n          MergeAppend</span>&#x201D;</span> planner errors (Tom Lane)</p>"
  ],
  [
    "<p>Fix index matching for operators with both collatable\n          and noncollatable inputs (Tom Lane)</p>",
    "<p>In 9.1.0, an indexable operator that has a\n          non-collatable left-hand input type and a collatable\n          right-hand input type would not be recognized as matching\n          the left-hand column's index. An example is the\n          <code class=\"type\">hstore</code> <code class=\"literal\">?</code> <code class=\"type\">text</code>\n          operator.</p>"
  ],
  [
    "<p>Allow inlining of set-returning SQL functions with\n          multiple OUT parameters (Tom Lane)</p>"
  ],
  [
    "<p>Don't trust deferred-unique indexes for join removal\n          (Tom Lane and Marti Raudsepp)</p>",
    "<p>A deferred uniqueness constraint might not hold\n          intra-transaction, so assuming that it does could give\n          incorrect query results.</p>"
  ],
  [
    "<p>Make <code class=\"function\">DatumGetInetP()</code>\n          unpack inet datums that have a 1-byte header, and add a\n          new macro, <code class=\"function\">DatumGetInetPP()</code>, that does not (Heikki\n          Linnakangas)</p>",
    "<p>This change affects no core code, but might prevent\n          crashes in add-on code that expects <code class=\"function\">DatumGetInetP()</code> to produce an unpacked\n          datum as per usual convention.</p>"
  ],
  [
    "<p>Improve locale support in <code class=\"type\">money</code> type's input and output (Tom\n          Lane)</p>",
    "<p>Aside from not supporting all standard <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-client.html#GUC-LC-MONETARY\"><code class=\"varname\">lc_monetary</code></a> formatting options, the\n          input and output functions were inconsistent, meaning\n          there were locales in which dumped <code class=\"type\">money</code> values could not be re-read.</p>"
  ],
  [
    "<p>Don't let <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-compatible.html#GUC-TRANSFORM-NULL-EQUALS\">\n          <code class=\"varname\">transform_null_equals</code></a>\n          affect <code class=\"literal\">CASE foo WHEN NULL\n          ...</code> constructs (Heikki Linnakangas)</p>",
    "<p><code class=\"varname\">transform_null_equals</code> is\n          only supposed to affect <code class=\"literal\">foo =\n          NULL</code> expressions written directly by the user, not\n          equality checks generated internally by this form of\n          <code class=\"literal\">CASE</code>.</p>"
  ],
  [
    "<p>Change foreign-key trigger creation order to better\n          support self-referential foreign keys (Tom Lane)</p>",
    "<p>For a cascading foreign key that references its own\n          table, a row update will fire both the <code class=\"literal\">ON UPDATE</code> trigger and the <code class=\"literal\">CHECK</code> trigger as one event. The\n          <code class=\"literal\">ON UPDATE</code> trigger must\n          execute first, else the <code class=\"literal\">CHECK</code> will check a non-final state of\n          the row and possibly throw an inappropriate error.\n          However, the firing order of these triggers is determined\n          by their names, which generally sort in creation order\n          since the triggers have auto-generated names following\n          the convention <span class=\"quote\">&#x201C;<span class=\"quote\">RI_ConstraintTrigger_NNNN</span>&#x201D;</span>. A\n          proper fix would require modifying that convention, which\n          we will do in 9.2, but it seems risky to change it in\n          existing releases. So this patch just changes the\n          creation order of the triggers. Users encountering this\n          type of error should drop and re-create the foreign key\n          constraint to get its triggers into the right order.</p>"
  ],
  [
    "<p>Fix <code class=\"literal\">IF EXISTS</code> to work\n          correctly in <code class=\"command\">DROP OPERATOR\n          FAMILY</code> (Robert Haas)</p>"
  ],
  [
    "<p>Disallow dropping of an extension from within its own\n          script (Tom Lane)</p>",
    "<p>This prevents odd behavior in case of incorrect\n          management of extension dependencies.</p>"
  ],
  [
    "<p>Don't mark auto-generated types as extension members\n          (Robert Haas)</p>",
    "<p>Relation rowtypes and automatically-generated array\n          types do not need to have their own extension membership\n          entries in <code class=\"structname\">pg_depend</code>, and\n          creating such entries complicates matters for extension\n          upgrades.</p>"
  ],
  [
    "<p>Cope with invalid pre-existing <code class=\"varname\">search_path</code> settings during <code class=\"command\">CREATE EXTENSION</code> (Tom Lane)</p>"
  ],
  [
    "<p>Avoid floating-point underflow while tracking buffer\n          allocation rate (Greg Matthews)</p>",
    "<p>While harmless in itself, on certain platforms this\n          would result in annoying kernel log messages.</p>"
  ],
  [
    "<p>Prevent autovacuum transactions from running in\n          serializable mode (Tom Lane)</p>",
    "<p>Autovacuum formerly used the cluster-wide default\n          transaction isolation level, but there is no need for it\n          to use anything higher than READ COMMITTED, and using\n          SERIALIZABLE could result in unnecessary delays for other\n          processes.</p>"
  ],
  [
    "<p>Ensure walsender processes respond promptly to\n          <span class=\"systemitem\">SIGTERM</span> (Magnus\n          Hagander)</p>"
  ],
  [
    "<p>Exclude <code class=\"filename\">postmaster.opts</code>\n          from base backups (Magnus Hagander)</p>"
  ],
  [
    "<p>Preserve configuration file name and line number\n          values when starting child processes under Windows (Tom\n          Lane)</p>",
    "<p>Formerly, these would not be displayed correctly in\n          the <code class=\"structname\">pg_settings</code> view.</p>"
  ],
  [
    "<p>Fix incorrect field alignment in <span class=\"application\">ecpg</span>'s SQLDA area (Zoltan\n          Boszormenyi)</p>"
  ],
  [
    "<p>Preserve blank lines within commands in <span class=\"application\">psql</span>'s command history (Robert\n          Haas)</p>",
    "<p>The former behavior could cause problems if an empty\n          line was removed from within a string literal, for\n          example.</p>"
  ],
  [
    "<p>Avoid platform-specific infinite loop in <span class=\"application\">pg_dump</span> (Steve Singer)</p>"
  ],
  [
    "<p>Fix compression of plain-text output format in\n          <span class=\"application\">pg_dump</span> (Adrian Klaver\n          and Tom Lane)</p>",
    "<p><span class=\"application\">pg_dump</span> has\n          historically understood <code class=\"literal\">-Z</code>\n          with no <code class=\"literal\">-F</code> switch to mean\n          that it should emit a gzip-compressed version of its\n          plain text output. Restore that behavior.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_dump</span> to dump\n          user-defined casts between auto-generated types, such as\n          table rowtypes (Tom Lane)</p>"
  ],
  [
    "<p>Fix missed quoting of foreign server names in\n          <span class=\"application\">pg_dump</span> (Tom Lane)</p>"
  ],
  [
    "<p>Assorted fixes for <span class=\"application\">pg_upgrade</span> (Bruce Momjian)</p>",
    "<p>Handle exclusion constraints correctly, avoid failures\n          on Windows, don't complain about mismatched toast table\n          names in 8.4 databases.</p>"
  ],
  [
    "<p>In PL/pgSQL, allow foreign tables to define row types\n          (Alexander Soudakov)</p>"
  ],
  [
    "<p>Fix up conversions of PL/Perl functions' results (Alex\n          Hunsaker and Tom Lane)</p>",
    "<p>Restore the pre-9.1 behavior that PL/Perl functions\n          returning <code class=\"type\">void</code> ignore the\n          result value of their last Perl statement; 9.1.0 would\n          throw an error if that statement returned a reference.\n          Also, make sure it works to return a string value for a\n          composite type, so long as the string meets the type's\n          input format. In addition, throw errors for attempts to\n          return Perl arrays or hashes when the function's declared\n          result type is not an array or composite type,\n          respectively. (Pre-9.1 versions rather uselessly returned\n          strings like <code class=\"literal\">ARRAY(0x221a9a0)</code> or <code class=\"literal\">HASH(0x221aa90)</code> in such cases.)</p>"
  ],
  [
    "<p>Ensure PL/Perl strings are always correctly\n          UTF8-encoded (Amit Khandekar and Alex Hunsaker)</p>"
  ],
  [
    "<p>Use the preferred version of <span class=\"application\">xsubpp</span> to build PL/Perl, not\n          necessarily the operating system's main copy (David\n          Wheeler and Alex Hunsaker)</p>"
  ],
  [
    "<p>Correctly propagate SQLSTATE in PL/Python exceptions\n          (Mika Eloranta and Jan Urbanski)</p>"
  ],
  [
    "<p>Do not install PL/Python extension files for Python\n          major versions other than the one built against (Peter\n          Eisentraut)</p>"
  ],
  [
    "<p>Change all the <code class=\"filename\">contrib</code>\n          extension script files to report a useful error message\n          if they are fed to <span class=\"application\">psql</span>\n          (Andrew Dunstan and Tom Lane)</p>",
    "<p>This should help teach people about the new method of\n          using <code class=\"command\">CREATE EXTENSION</code> to\n          load these files. In most cases, sourcing the scripts\n          directly would fail anyway, but with harder-to-interpret\n          messages.</p>"
  ],
  [
    "<p>Fix incorrect coding in <code class=\"filename\">contrib/dict_int</code> and <code class=\"filename\">contrib/dict_xsyn</code> (Tom Lane)</p>",
    "<p>Some functions incorrectly assumed that memory\n          returned by <code class=\"function\">palloc()</code> is\n          guaranteed zeroed.</p>"
  ],
  [
    "<p>Remove <code class=\"filename\">contrib/sepgsql</code>\n          tests from the regular regression test mechanism (Tom\n          Lane)</p>",
    "<p>Since these tests require root privileges for setup,\n          they're impractical to run automatically. Switch over to\n          a manual approach instead, and provide a testing script\n          to help with that.</p>"
  ],
  [
    "<p>Fix assorted errors in <code class=\"filename\">contrib/unaccent</code>'s configuration file\n          parsing (Tom Lane)</p>"
  ],
  [
    "<p>Honor query cancel interrupts promptly in <code class=\"function\">pgstatindex()</code> (Robert Haas)</p>"
  ],
  [
    "<p>Fix incorrect quoting of log file name in macOS start\n          script (Sidar Lopez)</p>"
  ],
  [
    "<p>Revert unintentional enabling of <code class=\"literal\">WAL_DEBUG</code> (Robert Haas)</p>",
    "<p>Fortunately, as debugging tools go, this one is pretty\n          cheap; but it's not intended to be enabled by default, so\n          revert.</p>"
  ],
  [
    "<p>Ensure VPATH builds properly install all server header\n          files (Peter Eisentraut)</p>"
  ],
  [
    "<p>Shorten file names reported in verbose error messages\n          (Peter Eisentraut)</p>",
    "<p>Regular builds have always reported just the name of\n          the C file containing the error message call, but VPATH\n          builds formerly reported an absolute path name.</p>"
  ],
  [
    "<p>Fix interpretation of Windows timezone names for\n          Central America (Tom Lane)</p>",
    "<p>Map <span class=\"quote\">&#x201C;<span class=\"quote\">Central\n          America Standard Time</span>&#x201D;</span> to <code class=\"literal\">CST6</code>, not <code class=\"literal\">CST6CDT</code>, because DST is generally not\n          observed anywhere in Central America.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2011n for DST law\n          changes in Brazil, Cuba, Fiji, Palestine, Russia, and\n          Samoa; also historical corrections for Alaska and British\n          East Africa.</p>"
  ]
]