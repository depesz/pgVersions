[
  [
    "<p>Fix incorrect volatility markings on a few built-in functions\n(Thomas Munro, Tom Lane)</p>",
    "<p>The functions <code class=\"function\">query_to_xml</code>,\n<code class=\"function\">cursor_to_xml</code>, <code class=\"function\">cursor_to_xmlschema</code>, <code class=\"function\">query_to_xmlschema</code>, and <code class=\"function\">query_to_xml_and_xmlschema</code> should be marked\nvolatile because they execute user-supplied queries that might\ncontain volatile operations. They were not, leading to a risk of\nincorrect query optimization. This has been repaired for new\ninstallations by correcting the initial catalog data, but existing\ninstallations will continue to contain the incorrect markings.\nPractical use of these functions seems to pose little hazard, but\nin case of trouble, it can be fixed by manually updating these\nfunctions' <code class=\"structname\">pg_proc</code> entries, for\nexample <code class=\"literal\">ALTER FUNCTION\npg_catalog.query_to_xml(text, boolean, boolean, text)\nVOLATILE</code>. (Note that that will need to be done in each\ndatabase of the installation.) Another option is to <span class=\"application\">pg_upgrade</span> the database to a version\ncontaining the corrected initial data.</p>"
  ],
  [
    "<p>Avoid re-using TOAST value OIDs that match\ndead-but-not-yet-vacuumed TOAST entries (Pavan Deolasee)</p>",
    "<p>Once the OID counter has wrapped around, it's possible to assign\na TOAST value whose OID matches a previously deleted entry in the\nsame TOAST table. If that entry were not yet vacuumed away, this\nresulted in <span class=\"quote\">&#x201C;<span class=\"quote\">unexpected\nchunk number 0 (expected 1) for toast value <em class=\"replaceable\"><code>nnnnn</code></em></span>&#x201D;</span> errors, which\nwould persist until the dead entry was removed by <code class=\"command\">VACUUM</code>. Fix by not selecting such OIDs when\ncreating a new TOAST entry.</p>"
  ],
  [
    "<p>Change <code class=\"command\">ANALYZE</code>'s algorithm for\nupdating <code class=\"structname\">pg_class</code>.<code class=\"structfield\">reltuples</code> (David Gould)</p>",
    "<p>Previously, pages not actually scanned by <code class=\"command\">ANALYZE</code> were assumed to retain their old tuple\ndensity. In a large table where <code class=\"command\">ANALYZE</code> samples only a small fraction of the\npages, this meant that the overall tuple density estimate could not\nchange very much, so that <code class=\"structfield\">reltuples</code> would change nearly proportionally\nto changes in the table's physical size (<code class=\"structfield\">relpages</code>) regardless of what was actually\nhappening in the table. This has been observed to result in\n<code class=\"structfield\">reltuples</code> becoming so much larger\nthan reality as to effectively shut off autovacuuming. To fix,\nassume that <code class=\"command\">ANALYZE</code>'s sample is a\nstatistically unbiased sample of the table (as it should be), and\njust extrapolate the density observed within those pages to the\nwhole table.</p>"
  ],
  [
    "<p>Fix <code class=\"literal\">UPDATE/DELETE ... WHERE CURRENT\nOF</code> to not fail when the referenced cursor uses an\nindex-only-scan plan (Yugo Nagata, Tom Lane)</p>"
  ],
  [
    "<p>Fix incorrect planning of join clauses pushed into parameterized\npaths (Andrew Gierth, Tom Lane)</p>",
    "<p>This error could result in misclassifying a condition as a\n<span class=\"quote\">&#x201C;<span class=\"quote\">join filter</span>&#x201D;</span>\nfor an outer join when it should be a plain <span class=\"quote\">&#x201C;<span class=\"quote\">filter</span>&#x201D;</span> condition,\nleading to incorrect join output.</p>"
  ],
  [
    "<p>Fix misoptimization of <code class=\"literal\">CHECK</code>\nconstraints having provably-NULL subclauses of top-level\n<code class=\"literal\">AND</code>/<code class=\"literal\">OR</code>\nconditions (Tom Lane, Dean Rasheed)</p>",
    "<p>This could, for example, allow constraint exclusion to exclude a\nchild table that should not be excluded from a query.</p>"
  ],
  [
    "<p>Avoid failure if a query-cancel or session-termination interrupt\noccurs while committing a prepared transaction (Stas Kelvich)</p>"
  ],
  [
    "<p>Fix query-lifespan memory leakage in repeatedly executed hash\njoins (Tom Lane)</p>"
  ],
  [
    "<p>Fix overly strict sanity check in <code class=\"function\">heap_prepare_freeze_tuple</code> (&#xC1;lvaro Herrera)</p>",
    "<p>This could result in incorrect <span class=\"quote\">&#x201C;<span class=\"quote\">cannot freeze committed xmax</span>&#x201D;</span> failures in\ndatabases that have been <span class=\"application\">pg_upgrade</span>'d from 9.2 or earlier.</p>"
  ],
  [
    "<p>Prevent dangling-pointer dereference when a C-coded\nbefore-update row trigger returns the <span class=\"quote\">&#x201C;<span class=\"quote\">old</span>&#x201D;</span> tuple (Rushabh\nLathia)</p>"
  ],
  [
    "<p>Reduce locking during autovacuum worker scheduling (Jeff\nJanes)</p>",
    "<p>The previous behavior caused drastic loss of potential worker\nconcurrency in databases with many tables.</p>"
  ],
  [
    "<p>Ensure client hostname is copied while copying <code class=\"structname\">pg_stat_activity</code> data to local memory (Edmund\nHorner)</p>",
    "<p>Previously the supposedly-local snapshot contained a pointer\ninto shared memory, allowing the client hostname column to change\nunexpectedly if any existing session disconnected.</p>"
  ],
  [
    "<p>Fix incorrect processing of multiple compound affixes in\n<code class=\"literal\">ispell</code> dictionaries (Arthur\nZakirov)</p>"
  ],
  [
    "<p>Fix collation-aware searches (that is, indexscans using\ninequality operators) in SP-GiST indexes on text columns (Tom\nLane)</p>",
    "<p>Such searches would return the wrong set of rows in most non-C\nlocales.</p>"
  ],
  [
    "<p>Count the number of index tuples correctly during initial build\nof an SP-GiST index (Tomas Vondra)</p>",
    "<p>Previously, the tuple count was reported to be the same as that\nof the underlying table, which is wrong if the index is\npartial.</p>"
  ],
  [
    "<p>Count the number of index tuples correctly during vacuuming of a\nGiST index (Andrey Borodin)</p>",
    "<p>Previously it reported the estimated number of heap tuples,\nwhich might be inaccurate, and is certainly wrong if the index is\npartial.</p>"
  ],
  [
    "<p>Allow <code class=\"function\">scalarltsel</code> and <code class=\"function\">scalargtsel</code> to be used on non-core datatypes\n(Tomas Vondra)</p>"
  ],
  [
    "<p>Reduce <span class=\"application\">libpq</span>'s memory\nconsumption when a server error is reported after a large amount of\nquery output has been collected (Tom Lane)</p>",
    "<p>Discard the previous output before, not after, processing the\nerror message. On some platforms, notably Linux, this can make a\ndifference in the application's subsequent memory footprint.</p>"
  ],
  [
    "<p>Fix double-free crashes in <span class=\"application\">ecpg</span>\n(Patrick Krecker, Jeevan Ladhe)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">ecpg</span> to handle <code class=\"type\">long long int</code> variables correctly in MSVC builds\n(Michael Meskes, Andrew Gierth)</p>"
  ],
  [
    "<p>Fix mis-quoting of values for list-valued GUC variables in dumps\n(Michael Paquier, Tom Lane)</p>",
    "<p>The <code class=\"varname\">local_preload_libraries</code>,\n<code class=\"varname\">session_preload_libraries</code>,\n<code class=\"varname\">shared_preload_libraries</code>, and\n<code class=\"varname\">temp_tablespaces</code> variables were not\ncorrectly quoted in <span class=\"application\">pg_dump</span>\noutput. This would cause problems if settings for these variables\nappeared in <code class=\"command\">CREATE FUNCTION ... SET</code> or\n<code class=\"command\">ALTER DATABASE/ROLE ... SET</code>\nclauses.</p>"
  ],
  [
    "<p>Fix overflow handling in <span class=\"application\">PL/pgSQL</span> integer <code class=\"command\">FOR</code> loops (Tom Lane)</p>",
    "<p>The previous coding failed to detect overflow of the loop\nvariable on some non-gcc compilers, leading to an infinite\nloop.</p>"
  ],
  [
    "<p>Adjust <span class=\"application\">PL/Python</span> regression\ntests to pass under Python 3.7 (Peter Eisentraut)</p>"
  ],
  [
    "<p>Support testing <span class=\"application\">PL/Python</span> and\nrelated modules when building with Python 3 and MSVC (Andrew\nDunstan)</p>"
  ],
  [
    "<p>Rename internal <code class=\"function\">b64_encode</code> and\n<code class=\"function\">b64_decode</code> functions to avoid\nconflict with Solaris 11.4 built-in functions (Rainer Orth)</p>"
  ],
  [
    "<p>Sync our copy of the timezone library with IANA tzcode release\n2018e (Tom Lane)</p>",
    "<p>This fixes the <span class=\"application\">zic</span> timezone\ndata compiler to cope with negative daylight-savings offsets. While\nthe <span class=\"productname\">PostgreSQL</span> project will not\nimmediately ship such timezone data, <span class=\"application\">zic</span> might be used with timezone data obtained\ndirectly from IANA, so it seems prudent to update <span class=\"application\">zic</span> now.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2018d for DST law changes in\nPalestine and Antarctica (Casey Station), plus historical\ncorrections for Portugal and its colonies, as well as Enderbury,\nJamaica, Turks &amp; Caicos Islands, and Uruguay.</p>"
  ]
]