[
  [
    "<p>Ensure that <tt class=\"LITERAL\">INSERT ... ON CONFLICT DO UPDATE</tt> checks table permissions and RLS policies in all cases (Dean Rasheed)</p>",
    "<p>The update path of <tt class=\"LITERAL\">INSERT ... ON CONFLICT DO UPDATE</tt> requires <tt class=\"LITERAL\">SELECT</tt> permission on the columns of the arbiter index, but it failed to check for that in the case of an arbiter specified by constraint name. In addition, for a table with row level security enabled, it failed to check updated rows against the table's <tt class=\"LITERAL\">SELECT</tt> policies (regardless of how the arbiter index was specified). (CVE-2017-15099)</p>"
  ],
  [
    "<p>Fix crash due to rowtype mismatch in <code class=\"FUNCTION\">json{b}_populate_recordset()</code> (Michael Paquier, Tom Lane)</p>",
    "<p>These functions used the result rowtype specified in the <tt class=\"LITERAL\">FROM ... AS</tt> clause without checking that it matched the actual rowtype of the supplied tuple value. If it didn't, that would usually result in a crash, though disclosure of server memory contents seems possible as well. (CVE-2017-15098)</p>"
  ],
  [
    "<p>Fix sample server-start scripts to become <tt class=\"LITERAL\">$PGUSER</tt> before opening <tt class=\"LITERAL\">$PGLOG</tt> (Noah Misch)</p>",
    "<p>Previously, the postmaster log file was opened while still running as root. The database owner could therefore mount an attack against another system user by making <tt class=\"LITERAL\">$PGLOG</tt> be a symbolic link to some other file, which would then become corrupted by appending log messages.</p>",
    "<p>By default, these scripts are not installed anywhere. Users who have made use of them will need to manually recopy them, or apply the same changes to their modified versions. If the existing <tt class=\"LITERAL\">$PGLOG</tt> file is root-owned, it will need to be removed or renamed out of the way before restarting the server with the corrected script. (CVE-2017-12172)</p>"
  ],
  [
    "<p>Fix BRIN index summarization to handle concurrent table extension correctly (Álvaro Herrera)</p>",
    "<p>Previously, a race condition allowed some table rows to be omitted from the index. It may be necessary to reindex existing BRIN indexes to recover from past occurrences of this problem.</p>"
  ],
  [
    "<p>Fix possible failures during concurrent updates of a BRIN index (Tom Lane)</p>",
    "<p>These race conditions could result in errors like <span class=\"QUOTE\">\"invalid index offnum\"</span> or <span class=\"QUOTE\">\"inconsistent range map\"</span>.</p>"
  ],
  [
    "<p>Fix crash when logical decoding is invoked from a SPI-using function, in particular any function written in a PL language (Tom Lane)</p>"
  ],
  [
    "<p>Fix incorrect query results when multiple <tt class=\"LITERAL\">GROUPING SETS</tt> columns contain the same simple variable (Tom Lane)</p>"
  ],
  [
    "<p>Fix incorrect parallelization decisions for nested queries (Amit Kapila, Kuntal Ghosh)</p>"
  ],
  [
    "<p>Fix parallel query handling to not fail when a recently-used role is dropped (Amit Kapila)</p>"
  ],
  [
    "<p>Fix <code class=\"FUNCTION\">json_build_array()</code>, <code class=\"FUNCTION\">json_build_object()</code>, and their <tt class=\"TYPE\">jsonb</tt> equivalents to handle explicit <tt class=\"LITERAL\">VARIADIC</tt> arguments correctly (Michael Paquier)</p>"
  ],
  [
    "<p>Properly reject attempts to convert infinite float values to type <tt class=\"TYPE\">numeric</tt> (Tom Lane, KaiGai Kohei)</p>",
    "<p>Previously the behavior was platform-dependent.</p>"
  ],
  [
    "<p>Fix corner-case crashes when columns have been added to the end of a view (Tom Lane)</p>"
  ],
  [
    "<p>Record proper dependencies when a view or rule contains <tt class=\"STRUCTNAME\">FieldSelect</tt> or <tt class=\"STRUCTNAME\">FieldStore</tt> expression nodes (Tom Lane)</p>",
    "<p>Lack of these dependencies could allow a column or data type <tt class=\"COMMAND\">DROP</tt> to go through when it ought to fail, thereby causing later uses of the view or rule to get errors. This patch does not do anything to protect existing views/rules, only ones created in the future.</p>"
  ],
  [
    "<p>Correctly detect hashability of range data types (Tom Lane)</p>",
    "<p>The planner mistakenly assumed that any range type could be hashed for use in hash joins or hash aggregation, but actually it must check whether the range's subtype has hash support. This does not affect any of the built-in range types, since they're all hashable anyway.</p>"
  ],
  [
    "<p>Correctly ignore <tt class=\"STRUCTNAME\">RelabelType</tt> expression nodes when determining relation distinctness (David Rowley)</p>",
    "<p>This allows the intended optimization to occur when a subquery has a result column of type <tt class=\"TYPE\">varchar</tt>.</p>"
  ],
  [
    "<p>Prevent sharing transition states between ordered-set aggregates (David Rowley)</p>",
    "<p>This causes a crash with the built-in ordered-set aggregates, and probably with user-written ones as well. v11 and later will include provisions for dealing with such cases safely, but in released branches, just disable the optimization.</p>"
  ],
  [
    "<p>Prevent <tt class=\"VARNAME\">idle_in_transaction_session_timeout</tt> from being ignored when a <tt class=\"VARNAME\">statement_timeout</tt> occurred earlier (Lukas Fittl)</p>"
  ],
  [
    "<p>Fix low-probability loss of <tt class=\"COMMAND\">NOTIFY</tt> messages due to XID wraparound (Marko Tiikkaja, Tom Lane)</p>",
    "<p>If a session executed no queries, but merely listened for notifications, for more than 2 billion transactions, it started to miss some notifications from concurrently-committing transactions.</p>"
  ],
  [
    "<p>Avoid <span class=\"SYSTEMITEM\">SIGBUS</span> crash on Linux when a DSM memory request exceeds the space available in <span class=\"SYSTEMITEM\">tmpfs</span> (Thomas Munro)</p>"
  ],
  [
    "<p>Reduce the frequency of data flush requests during bulk file copies to avoid performance problems on macOS, particularly with its new APFS file system (Tom Lane)</p>"
  ],
  [
    "<p>Prevent low-probability crash in processing of nested trigger firings (Tom Lane)</p>"
  ],
  [
    "<p>Allow <tt class=\"COMMAND\">COPY</tt>'s <tt class=\"LITERAL\">FREEZE</tt> option to work when the transaction isolation level is <tt class=\"LITERAL\">REPEATABLE READ</tt> or higher (Noah Misch)</p>",
    "<p>This case was unintentionally broken by a previous bug fix.</p>"
  ],
  [
    "<p>Correctly restore the umask setting when file creation fails in <tt class=\"COMMAND\">COPY</tt> or <code class=\"FUNCTION\">lo_export()</code> (Peter Eisentraut)</p>"
  ],
  [
    "<p>Give a better error message for duplicate column names in <tt class=\"COMMAND\">ANALYZE</tt> (Nathan Bossart)</p>"
  ],
  [
    "<p>Add missing cases in <code class=\"FUNCTION\">GetCommandLogLevel()</code>, preventing errors when certain SQL commands are used while <tt class=\"VARNAME\">log_statement</tt> is set to <tt class=\"LITERAL\">ddl</tt> (Michael Paquier)</p>"
  ],
  [
    "<p>Fix mis-parsing of the last line in a non-newline-terminated <tt class=\"FILENAME\">pg_hba.conf</tt> file (Tom Lane)</p>"
  ],
  [
    "<p>Fix <code class=\"FUNCTION\">AggGetAggref()</code> to return the correct <tt class=\"STRUCTNAME\">Aggref</tt> nodes to aggregate final functions whose transition calculations have been merged (Tom Lane)</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_dump</span> to ensure that it emits <tt class=\"COMMAND\">GRANT</tt> commands in a valid order (Stephen Frost)</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_basebackup</span>'s matching of tablespace paths to canonicalize both paths before comparing (Michael Paquier)</p>",
    "<p>This is particularly helpful on Windows.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">libpq</span> to not require user's home directory to exist (Tom Lane)</p>",
    "<p>In v10, failure to find the home directory while trying to read <tt class=\"FILENAME\">~/.pgpass</tt> was treated as a hard error, but it should just cause that file to not be found. Both v10 and previous release branches made the same mistake when reading <tt class=\"FILENAME\">~/.pg_service.conf</tt>, though this was less obvious since that file is not sought unless a service name is specified.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">libpq</span> to guard against integer overflow in the row count of a <tt class=\"STRUCTNAME\">PGresult</tt> (Michael Paquier)</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">ecpg</span>'s handling of out-of-scope cursor declarations with pointer or array variables (Michael Meskes)</p>"
  ],
  [
    "<p>In ecpglib, correctly handle backslashes in string literals depending on whether <tt class=\"VARNAME\">standard_conforming_strings</tt> is set (Tsunakawa Takayuki)</p>"
  ],
  [
    "<p>Make ecpglib's Informix-compatibility mode ignore fractional digits in integer input strings, as expected (Gao Zengqi, Michael Meskes)</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">ecpg</span>'s regression tests to work reliably on Windows (Christian Ullrich, Michael Meskes)</p>"
  ],
  [
    "<p>Fix missing temp-install prerequisites for <tt class=\"LITERAL\">check</tt>-like Make targets (Noah Misch)</p>",
    "<p>Some non-default test procedures that are meant to work like <tt class=\"LITERAL\">make check</tt> failed to ensure that the temporary installation was up to date.</p>"
  ],
  [
    "<p>Sync our copy of the timezone library with IANA release tzcode2017c (Tom Lane)</p>",
    "<p>This fixes various issues; the only one likely to be user-visible is that the default DST rules for a POSIX-style zone name, if no <tt class=\"FILENAME\">posixrules</tt> file exists in the timezone data directory, now match current US law rather than what it was a dozen years ago.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"APPLICATION\">tzdata</span> release 2017c for DST law changes in Fiji, Namibia, Northern Cyprus, Sudan, Tonga, and Turks &amp; Caicos Islands, plus historical corrections for Alaska, Apia, Burma, Calcutta, Detroit, Ireland, Namibia, and Pago Pago.</p>"
  ]
]