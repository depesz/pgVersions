[
  [
    "<p>Allow IPv6 server connections (Nigel Kukard, Johan\n            Jordaan, Bruce, Tom, Kurt Roeckx, Andrew Dunstan)</p>"
  ],
  [
    "<p>Fix SSL to handle errors cleanly (Nathan\n            Mueller)</p>",
    "<p>In prior releases, certain SSL API error reports\n            were not handled correctly. This release fixes those\n            problems.</p>"
  ],
  [
    "<p>SSL protocol security and performance improvements\n            (Sean Chittenden)</p>",
    "<p>SSL key renegotiation was happening too frequently,\n            causing poor SSL performance. Also, initial key\n            handling was improved.</p>"
  ],
  [
    "<p>Print lock information when a deadlock is detected\n            (Tom)</p>",
    "<p>This allows easier debugging of deadlock\n            situations.</p>"
  ],
  [
    "<p>Update <code class=\"filename\">/tmp</code> socket\n            modification times regularly to avoid their removal\n            (Tom)</p>",
    "<p>This should help prevent <code class=\"filename\">/tmp</code> directory cleaner administration\n            scripts from removing server socket files.</p>"
  ],
  [
    "<p>Enable PAM for macOS (Aaron Hillegass)</p>"
  ],
  [
    "<p>Make B-tree indexes fully WAL-safe (Tom)</p>",
    "<p>In prior releases, under certain rare cases, a\n            server crash could cause B-tree indexes to become\n            corrupt. This release removes those last few rare\n            cases.</p>"
  ],
  [
    "<p>Allow B-tree index compaction and empty page reuse\n            (Tom)</p>"
  ],
  [
    "<p>Fix inconsistent index lookups during split of first\n            root page (Tom)</p>",
    "<p>In prior releases, when a single-page index split\n            into two pages, there was a brief period when another\n            database session could miss seeing an index entry. This\n            release fixes that rare failure case.</p>"
  ],
  [
    "<p>Improve free space map allocation logic (Tom)</p>"
  ],
  [
    "<p>Preserve free space information between server\n            restarts (Tom)</p>",
    "<p>In prior releases, the free space map was not saved\n            when the postmaster was stopped, so newly started\n            servers had no free space information. This release\n            saves the free space map, and reloads it when the\n            server is restarted.</p>"
  ],
  [
    "<p>Add start time to <code class=\"literal\">pg_stat_activity</code> (Neil)</p>"
  ],
  [
    "<p>New code to detect corrupt disk pages; erase with\n            <code class=\"varname\">zero_damaged_pages</code>\n            (Tom)</p>"
  ],
  [
    "<p>New client/server protocol: faster, no username\n            length limit, allow clean exit from <code class=\"command\">COPY</code> (Tom)</p>"
  ],
  [
    "<p>Add transaction status, table ID, column ID to\n            client/server protocol (Tom)</p>"
  ],
  [
    "<p>Add binary I/O to client/server protocol (Tom)</p>"
  ],
  [
    "<p>Remove autocommit server setting; move to client\n            applications (Tom)</p>"
  ],
  [
    "<p>New error message wording, error codes, and three\n            levels of error detail (Tom, Joe, Peter)</p>"
  ],
  [
    "<p>Add hashing for <code class=\"literal\">GROUP\n            BY</code> aggregates (Tom)</p>"
  ],
  [
    "<p>Make nested-loop joins be smarter about multicolumn\n            indexes (Tom)</p>"
  ],
  [
    "<p>Allow multikey hash joins (Tom)</p>"
  ],
  [
    "<p>Improve constant folding (Tom)</p>"
  ],
  [
    "<p>Add ability to inline simple SQL functions (Tom)</p>"
  ],
  [
    "<p>Reduce memory usage for queries using complex\n            functions (Tom)</p>",
    "<p>In prior releases, functions returning allocated\n            memory would not free it until the query completed.\n            This release allows the freeing of function-allocated\n            memory when the function call completes, reducing the\n            total memory used by functions.</p>"
  ],
  [
    "<p>Improve GEQO optimizer performance (Tom)</p>",
    "<p>This release fixes several inefficiencies in the way\n            the GEQO optimizer manages potential query paths.</p>"
  ],
  [
    "<p>Allow <code class=\"literal\">IN</code>/<code class=\"literal\">NOT IN</code> to be handled via hash tables\n            (Tom)</p>"
  ],
  [
    "<p>Improve <code class=\"literal\">NOT IN (<em class=\"replaceable\"><code>subquery</code></em>)</code>\n            performance (Tom)</p>"
  ],
  [
    "<p>Allow most <code class=\"literal\">IN</code>\n            subqueries to be processed as joins (Tom)</p>"
  ],
  [
    "<p>Pattern matching operations can use indexes\n            regardless of locale (Peter)</p>",
    "<p>There is no way for non-ASCII locales to use the\n            standard indexes for <code class=\"literal\">LIKE</code>\n            comparisons. This release adds a way to create a\n            special index for <code class=\"literal\">LIKE</code>.</p>"
  ],
  [
    "<p>Allow the postmaster to preload libraries using\n            <code class=\"varname\">preload_libraries</code>\n            (Joe)</p>",
    "<p>For shared libraries that require a long time to\n            load, this option is available so the library can be\n            preloaded in the postmaster and inherited by all\n            database sessions.</p>"
  ],
  [
    "<p>Improve optimizer cost computations, particularly\n            for subqueries (Tom)</p>"
  ],
  [
    "<p>Avoid sort when subquery <code class=\"literal\">ORDER\n            BY</code> matches upper query (Tom)</p>"
  ],
  [
    "<p>Deduce that <code class=\"literal\">WHERE a.x = b.y\n            AND b.y = 42</code> also means <code class=\"literal\">a.x = 42</code> (Tom)</p>"
  ],
  [
    "<p>Allow hash/merge joins on complex joins (Tom)</p>"
  ],
  [
    "<p>Allow hash joins for more data types (Tom)</p>"
  ],
  [
    "<p>Allow join optimization of explicit inner joins,\n            disable with <code class=\"varname\">join_collapse_limit</code> (Tom)</p>"
  ],
  [
    "<p>Add parameter <code class=\"varname\">from_collapse_limit</code> to control\n            conversion of subqueries to joins (Tom)</p>"
  ],
  [
    "<p>Use faster and more powerful regular expression code\n            from Tcl (Henry Spencer, Tom)</p>"
  ],
  [
    "<p>Use bit-mapped relation sets in the optimizer\n            (Tom)</p>"
  ],
  [
    "<p>Improve connection startup time (Tom)</p>",
    "<p>The new client/server protocol requires fewer\n            network packets to start a database session.</p>"
  ],
  [
    "<p>Improve trigger/constraint performance (Stephan)</p>"
  ],
  [
    "<p>Improve speed of <code class=\"literal\">col IN\n            (const, const, const, ...)</code> (Tom)</p>"
  ],
  [
    "<p>Fix hash indexes which were broken in rare cases\n            (Tom)</p>"
  ],
  [
    "<p>Improve hash index concurrency and speed (Tom)</p>",
    "<p>Prior releases suffered from poor hash index\n            performance, particularly for high concurrency\n            situations. This release fixes that, and the\n            development group is interested in reports comparing\n            B-tree and hash index performance.</p>"
  ],
  [
    "<p>Align shared buffers on 32-byte boundary for copy\n            speed improvement (Manfred Spraul)</p>",
    "<p>Certain CPU's perform faster data copies when\n            addresses are 32-byte aligned.</p>"
  ],
  [
    "<p>Data type <code class=\"type\">numeric</code>\n            reimplemented for better performance (Tom)</p>",
    "<p><code class=\"type\">numeric</code> used to be stored\n            in base 100. The new code uses base 10000, for\n            significantly better performance.</p>"
  ],
  [
    "<p>Rename server parameter <code class=\"varname\">server_min_messages</code> to <code class=\"varname\">log_min_messages</code> (Bruce)</p>",
    "<p>This was done so most parameters that control the\n            server logs begin with <code class=\"literal\">log_</code>.</p>"
  ],
  [
    "<p>Rename <code class=\"varname\">show_*_stats</code> to\n            <code class=\"varname\">log_*_stats</code> (Bruce)</p>"
  ],
  [
    "<p>Rename <code class=\"varname\">show_source_port</code>\n            to <code class=\"varname\">log_source_port</code>\n            (Bruce)</p>"
  ],
  [
    "<p>Rename <code class=\"varname\">hostname_lookup</code>\n            to <code class=\"varname\">log_hostname</code>\n            (Bruce)</p>"
  ],
  [
    "<p>Add <code class=\"varname\">checkpoint_warning</code>\n            to warn of excessive checkpointing (Bruce)</p>",
    "<p>In prior releases, it was difficult to determine if\n            checkpoint was happening too frequently. This feature\n            adds a warning to the server logs when excessive\n            checkpointing happens.</p>"
  ],
  [
    "<p>New read-only server parameters for localization\n            (Tom)</p>"
  ],
  [
    "<p>Change debug server log messages to output as\n            <code class=\"literal\">DEBUG</code> rather than\n            <code class=\"literal\">LOG</code> (Bruce)</p>"
  ],
  [
    "<p>Prevent server log variables from being turned off\n            by non-superusers (Bruce)</p>",
    "<p>This is a security feature so non-superusers cannot\n            disable logging that was enabled by the\n            administrator.</p>"
  ],
  [
    "<p><code class=\"varname\">log_min_messages</code>/<code class=\"varname\">client_min_messages</code> now controls\n            <code class=\"varname\">debug_*</code> output (Bruce)</p>",
    "<p>This centralizes client debug information so all\n            debug output can be sent to either the client or server\n            logs.</p>"
  ],
  [
    "<p>Add macOS Rendezvous server support (Chris\n            Campbell)</p>",
    "<p>This allows macOS hosts to query the network for\n            available <span class=\"productname\">PostgreSQL</span>\n            servers.</p>"
  ],
  [
    "<p>Add ability to print only slow statements using\n            <code class=\"varname\">log_min_duration_statement</code>\n            (Christopher)</p>",
    "<p>This is an often requested debugging feature that\n            allows administrators to see only slow queries in their\n            server logs.</p>"
  ],
  [
    "<p>Allow <code class=\"filename\">pg_hba.conf</code> to\n            accept netmasks in CIDR format (Andrew Dunstan)</p>",
    "<p>This allows administrators to merge the host IP\n            address and netmask fields into a single CIDR field in\n            <code class=\"filename\">pg_hba.conf</code>.</p>"
  ],
  [
    "<p>New read-only parameter <code class=\"varname\">is_superuser</code> (Tom)</p>"
  ],
  [
    "<p>New parameter <code class=\"varname\">log_error_verbosity</code> to control error\n            detail (Tom)</p>",
    "<p>This works with the new error reporting feature to\n            supply additional error information like hints, file\n            names and line numbers.</p>"
  ],
  [
    "<p><code class=\"literal\">postgres\n            --describe-config</code> now dumps server config\n            variables (Aizaz Ahmed, Peter)</p>",
    "<p>This option is useful for administration tools that\n            need to know the configuration variable names and their\n            minimums, maximums, defaults, and descriptions.</p>"
  ],
  [
    "<p>Add new columns in <code class=\"literal\">pg_settings</code>: <code class=\"literal\">context</code>, <code class=\"literal\">type</code>, <code class=\"literal\">source</code>, <code class=\"literal\">min_val</code>, <code class=\"literal\">max_val</code> (Joe)</p>"
  ],
  [
    "<p>Make default <code class=\"varname\">shared_buffers</code> 1000 and <code class=\"varname\">max_connections</code> 100, if possible\n            (Tom)</p>",
    "<p>Prior versions defaulted to 64 shared buffers so\n            <span class=\"productname\">PostgreSQL</span> would start\n            on even very old systems. This release tests the amount\n            of shared memory allowed by the platform and selects\n            more reasonable default values if possible. Of course,\n            users are still encouraged to evaluate their resource\n            load and size <code class=\"varname\">shared_buffers</code> accordingly.</p>"
  ],
  [
    "<p>New <code class=\"filename\">pg_hba.conf</code> record\n            type <code class=\"literal\">hostnossl</code> to prevent\n            SSL connections (Jon Jensen)</p>",
    "<p>In prior releases, there was no way to prevent SSL\n            connections if both the client and server supported\n            SSL. This option allows that capability.</p>"
  ],
  [
    "<p>Remove parameter <code class=\"varname\">geqo_random_seed</code> (Tom)</p>"
  ],
  [
    "<p>Add server parameter <code class=\"varname\">regex_flavor</code> to control regular\n            expression processing (Tom)</p>"
  ],
  [
    "<p>Make <code class=\"command\">pg_ctl</code> better\n            handle nonstandard ports (Greg)</p>"
  ],
  [
    "<p>New SQL-standard information schema (Peter)</p>"
  ],
  [
    "<p>Add read-only transactions (Peter)</p>"
  ],
  [
    "<p>Print key name and value in foreign-key violation\n            messages (Dmitry Tkach)</p>"
  ],
  [
    "<p>Allow users to see their own queries in <code class=\"literal\">pg_stat_activity</code> (Kevin Brown)</p>",
    "<p>In prior releases, only the superuser could see\n            query strings using <code class=\"literal\">pg_stat_activity</code>. Now ordinary users\n            can see their own query strings.</p>"
  ],
  [
    "<p>Fix aggregates in subqueries to match SQL standard\n            (Tom)</p>",
    "<p>The SQL standard says that an aggregate function\n            appearing within a nested subquery belongs to the outer\n            query if its argument contains only outer-query\n            variables. Prior <span class=\"productname\">PostgreSQL</span> releases did not handle\n            this fine point correctly.</p>"
  ],
  [
    "<p>Add option to prevent auto-addition of tables\n            referenced in query (Nigel J. Andrews)</p>",
    "<p>By default, tables mentioned in the query are\n            automatically added to the <code class=\"literal\">FROM</code> clause if they are not already\n            there. This is compatible with historic <span class=\"productname\">POSTGRES</span> behavior but is contrary\n            to the SQL standard. This option allows selecting\n            standard-compatible behavior.</p>"
  ],
  [
    "<p>Allow <code class=\"literal\">UPDATE ... SET col =\n            DEFAULT</code> (Rod)</p>",
    "<p>This allows <code class=\"command\">UPDATE</code> to\n            set a column to its declared default value.</p>"
  ],
  [
    "<p>Allow expressions to be used in <code class=\"literal\">LIMIT</code>/<code class=\"literal\">OFFSET</code> (Tom)</p>",
    "<p>In prior releases, <code class=\"literal\">LIMIT</code>/<code class=\"literal\">OFFSET</code> could only use constants, not\n            expressions.</p>"
  ],
  [
    "<p>Implement <code class=\"literal\">CREATE TABLE AS\n            EXECUTE</code> (Neil, Peter)</p>"
  ],
  [
    "<p>Make <code class=\"command\">CREATE SEQUENCE</code>\n            grammar more conforming to SQL:2003 (Neil)</p>"
  ],
  [
    "<p>Add statement-level triggers (Neil)</p>",
    "<p>While this allows a trigger to fire at the end of a\n            statement, it does not allow the trigger to access all\n            rows modified by the statement. This capability is\n            planned for a future release.</p>"
  ],
  [
    "<p>Add check constraints for domains (Rod)</p>",
    "<p>This greatly increases the usefulness of domains by\n            allowing them to use check constraints.</p>"
  ],
  [
    "<p>Add <code class=\"command\">ALTER DOMAIN</code>\n            (Rod)</p>",
    "<p>This allows manipulation of existing domains.</p>"
  ],
  [
    "<p>Fix several zero-column table bugs (Tom)</p>",
    "<p><span class=\"productname\">PostgreSQL</span> supports\n            zero-column tables. This fixes various bugs that occur\n            when using such tables.</p>"
  ],
  [
    "<p>Have <code class=\"literal\">ALTER TABLE ... ADD\n            PRIMARY KEY</code> add not-null constraint (Rod)</p>",
    "<p>In prior releases, <code class=\"literal\">ALTER TABLE\n            ... ADD PRIMARY</code> would add a unique index, but\n            not a not-null constraint. That is fixed in this\n            release.</p>"
  ],
  [
    "<p>Add <code class=\"literal\">ALTER TABLE ... WITHOUT\n            OIDS</code> (Rod)</p>",
    "<p>This allows control over whether new and updated\n            rows will have an OID column. This is most useful for\n            saving storage space.</p>"
  ],
  [
    "<p>Add <code class=\"literal\">ALTER SEQUENCE</code> to\n            modify minimum, maximum, increment, cache, cycle values\n            (Rod)</p>"
  ],
  [
    "<p>Add <code class=\"literal\">ALTER TABLE ... CLUSTER\n            ON</code> (Alvaro Herrera)</p>",
    "<p>This command is used by <code class=\"command\">pg_dump</code> to record the cluster column\n            for each table previously clustered. This information\n            is used by database-wide cluster to cluster all\n            previously clustered tables.</p>"
  ],
  [
    "<p>Improve automatic type casting for domains (Rod,\n            Tom)</p>"
  ],
  [
    "<p>Allow dollar signs in identifiers, except as first\n            character (Tom)</p>"
  ],
  [
    "<p>Disallow dollar signs in operator names, so\n            <code class=\"literal\">x=$1</code> works (Tom)</p>"
  ],
  [
    "<p>Allow copying table schema using <code class=\"literal\">LIKE <em class=\"replaceable\"><code>subtable</code></em></code>, also\n            SQL:2003 feature <code class=\"literal\">INCLUDING\n            DEFAULTS</code> (Rod)</p>"
  ],
  [
    "<p>Add <code class=\"literal\">WITH GRANT OPTION</code>\n            clause to <code class=\"command\">GRANT</code>\n            (Peter)</p>",
    "<p>This enabled <code class=\"command\">GRANT</code> to\n            give other users the ability to grant privileges on an\n            object.</p>"
  ],
  [
    "<p>Add <code class=\"literal\">ON COMMIT</code> clause to\n            <code class=\"command\">CREATE TABLE</code> for temporary\n            tables (Gavin)</p>",
    "<p>This adds the ability for a table to be dropped or\n            all rows deleted on transaction commit.</p>"
  ],
  [
    "<p>Allow cursors outside transactions using\n            <code class=\"literal\">WITH HOLD</code> (Neil)</p>",
    "<p>In previous releases, cursors were removed at the\n            end of the transaction that created them. Cursors can\n            now be created with the <code class=\"literal\">WITH\n            HOLD</code> option, which allows them to continue to be\n            accessed after the creating transaction has\n            committed.</p>"
  ],
  [
    "<p><code class=\"literal\">FETCH 0</code> and\n            <code class=\"literal\">MOVE 0</code> now do nothing\n            (Bruce)</p>",
    "<p>In previous releases, <code class=\"literal\">FETCH\n            0</code> fetched all remaining rows, and <code class=\"literal\">MOVE 0</code> moved to the end of the\n            cursor.</p>"
  ],
  [
    "<p>Cause <code class=\"command\">FETCH</code> and\n            <code class=\"command\">MOVE</code> to return the number\n            of rows fetched/moved, or zero if at the beginning/end\n            of cursor, per SQL standard (Bruce)</p>",
    "<p>In prior releases, the row count returned by\n            <code class=\"command\">FETCH</code> and <code class=\"command\">MOVE</code> did not accurately reflect the\n            number of rows processed.</p>"
  ],
  [
    "<p>Properly handle <code class=\"literal\">SCROLL</code>\n            with cursors, or report an error (Neil)</p>",
    "<p>Allowing random access (both forward and backward\n            scrolling) to some kinds of queries cannot be done\n            without some additional work. If <code class=\"literal\">SCROLL</code> is specified when the cursor is\n            created, this additional work will be performed.\n            Furthermore, if the cursor has been created with\n            <code class=\"literal\">NO SCROLL</code>, no random\n            access is allowed.</p>"
  ],
  [
    "<p>Implement SQL-compatible options <code class=\"literal\">FIRST</code>, <code class=\"literal\">LAST</code>, <code class=\"literal\">ABSOLUTE\n            <em class=\"replaceable\"><code>n</code></em></code>,\n            <code class=\"literal\">RELATIVE <em class=\"replaceable\"><code>n</code></em></code> for\n            <code class=\"command\">FETCH</code> and <code class=\"command\">MOVE</code> (Tom)</p>"
  ],
  [
    "<p>Allow <code class=\"command\">EXPLAIN</code> on\n            <code class=\"command\">DECLARE CURSOR</code> (Tom)</p>"
  ],
  [
    "<p>Allow <code class=\"command\">CLUSTER</code> to use\n            index marked as pre-clustered by default (Alvaro\n            Herrera)</p>"
  ],
  [
    "<p>Allow <code class=\"command\">CLUSTER</code> to\n            cluster all tables (Alvaro Herrera)</p>",
    "<p>This allows all previously clustered tables in a\n            database to be reclustered with a single command.</p>"
  ],
  [
    "<p>Prevent <code class=\"command\">CLUSTER</code> on\n            partial indexes (Tom)</p>"
  ],
  [
    "<p>Allow DOS and Mac line-endings in <code class=\"command\">COPY</code> files (Bruce)</p>"
  ],
  [
    "<p>Disallow literal carriage return as a data value,\n            backslash-carriage-return and <code class=\"literal\">\\r</code> are still allowed (Bruce)</p>"
  ],
  [
    "<p><code class=\"command\">COPY</code> changes (binary,\n            <code class=\"literal\">\\.</code>) (Tom)</p>"
  ],
  [
    "<p>Recover from <code class=\"command\">COPY</code>\n            failure cleanly (Tom)</p>"
  ],
  [
    "<p>Prevent possible memory leaks in <code class=\"command\">COPY</code> (Tom)</p>"
  ],
  [
    "<p>Make <code class=\"command\">TRUNCATE</code>\n            transaction-safe (Rod)</p>",
    "<p><code class=\"command\">TRUNCATE</code> can now be\n            used inside a transaction. If the transaction aborts,\n            the changes made by the <code class=\"command\">TRUNCATE</code> are automatically rolled\n            back.</p>"
  ],
  [
    "<p>Allow prepare/bind of utility commands like\n            <code class=\"command\">FETCH</code> and <code class=\"command\">EXPLAIN</code> (Tom)</p>"
  ],
  [
    "<p>Add <code class=\"command\">EXPLAIN EXECUTE</code>\n            (Neil)</p>"
  ],
  [
    "<p>Improve <code class=\"command\">VACUUM</code>\n            performance on indexes by reducing WAL traffic\n            (Tom)</p>"
  ],
  [
    "<p>Functional indexes have been generalized into\n            indexes on expressions (Tom)</p>",
    "<p>In prior releases, functional indexes only supported\n            a simple function applied to one or more column names.\n            This release allows any type of scalar expression.</p>"
  ],
  [
    "<p>Have <code class=\"command\">SHOW TRANSACTION\n            ISOLATION</code> match input to <code class=\"command\">SET TRANSACTION ISOLATION</code> (Tom)</p>"
  ],
  [
    "<p>Have <code class=\"command\">COMMENT ON\n            DATABASE</code> on nonlocal database generate a\n            warning, rather than an error (Rod)</p>",
    "<p>Database comments are stored in database-local\n            tables so comments on a database have to be stored in\n            each database.</p>"
  ],
  [
    "<p>Improve reliability of <code class=\"command\">LISTEN</code>/<code class=\"command\">NOTIFY</code> (Tom)</p>"
  ],
  [
    "<p>Allow <code class=\"command\">REINDEX</code> to\n            reliably reindex nonshared system catalog indexes\n            (Tom)</p>",
    "<p>This allows system tables to be reindexed without\n            the requirement of a standalone session, which was\n            necessary in previous releases. The only tables that\n            now require a standalone session for reindexing are the\n            global system tables <code class=\"literal\">pg_database</code>, <code class=\"literal\">pg_shadow</code>, and <code class=\"literal\">pg_group</code>.</p>"
  ],
  [
    "<p>New server parameter <code class=\"varname\">extra_float_digits</code> to control\n            precision display of floating-point numbers (Pedro\n            Ferreira, Tom)</p>",
    "<p>This controls output precision which was causing\n            regression testing problems.</p>"
  ],
  [
    "<p>Allow <code class=\"literal\">+1300</code> as a\n            numeric time-zone specifier, for FJST (Tom)</p>"
  ],
  [
    "<p>Remove rarely used functions <code class=\"function\">oidrand</code>, <code class=\"function\">oidsrand</code>, and <code class=\"function\">userfntest</code> functions (Neil)</p>"
  ],
  [
    "<p>Add <code class=\"function\">md5()</code> function to\n            main server, already in <code class=\"filename\">contrib/pgcrypto</code> (Joe)</p>",
    "<p>An MD5 function was frequently requested. For more\n            complex encryption capabilities, use <code class=\"filename\">contrib/pgcrypto</code>.</p>"
  ],
  [
    "<p>Increase date range of <code class=\"type\">timestamp</code> (John Cochran)</p>"
  ],
  [
    "<p>Change <code class=\"literal\">EXTRACT(EPOCH FROM\n            timestamp)</code> so <code class=\"type\">timestamp\n            without time zone</code> is assumed to be in local\n            time, not GMT (Tom)</p>"
  ],
  [
    "<p>Trap division by zero in case the operating system\n            doesn't prevent it (Tom)</p>"
  ],
  [
    "<p>Change the <code class=\"type\">numeric</code> data\n            type internally to base 10000 (Tom)</p>"
  ],
  [
    "<p>New <code class=\"function\">hostmask()</code>\n            function (Greg Wickham)</p>"
  ],
  [
    "<p>Fixes for <code class=\"function\">to_char()</code>\n            and <code class=\"function\">to_timestamp()</code>\n            (Karel)</p>"
  ],
  [
    "<p>Allow functions that can take any argument data type\n            and return any data type, using <code class=\"type\">anyelement</code> and <code class=\"type\">anyarray</code> (Joe)</p>",
    "<p>This allows the creation of functions that can work\n            with any data type.</p>"
  ],
  [
    "<p>Arrays can now be specified as <code class=\"literal\">ARRAY[1,2,3]</code>, <code class=\"literal\">ARRAY[['a','b'],['c','d']]</code>, or\n            <code class=\"literal\">ARRAY[ARRAY[ARRAY[2]]]</code>\n            (Joe)</p>"
  ],
  [
    "<p>Allow proper comparisons for arrays, including\n            <code class=\"literal\">ORDER BY</code> and <code class=\"literal\">DISTINCT</code> support (Joe)</p>"
  ],
  [
    "<p>Allow indexes on array columns (Joe)</p>"
  ],
  [
    "<p>Allow array concatenation with <code class=\"literal\">||</code> (Joe)</p>"
  ],
  [
    "<p>Allow <code class=\"literal\">WHERE</code>\n            qualification <code class=\"literal\"><em class=\"replaceable\"><code>expr</code></em> <em class=\"replaceable\"><code>op</code></em> ANY/SOME/ALL\n            (<em class=\"replaceable\"><code>array_expr</code></em>)</code>\n            (Joe)</p>",
    "<p>This allows arrays to behave like a list of values,\n            for purposes like <code class=\"literal\">SELECT * FROM\n            tab WHERE col IN (array_val)</code>.</p>"
  ],
  [
    "<p>New array functions <code class=\"function\">array_append</code>, <code class=\"function\">array_cat</code>, <code class=\"function\">array_lower</code>, <code class=\"function\">array_prepend</code>, <code class=\"function\">array_to_string</code>, <code class=\"function\">array_upper</code>, <code class=\"function\">string_to_array</code> (Joe)</p>"
  ],
  [
    "<p>Allow user defined aggregates to use polymorphic\n            functions (Joe)</p>"
  ],
  [
    "<p>Allow assignments to empty arrays (Joe)</p>"
  ],
  [
    "<p>Allow 60 in seconds fields of <code class=\"type\">time</code>, <code class=\"type\">timestamp</code>, and <code class=\"type\">interval</code> input values (Tom)</p>",
    "<p>Sixty-second values are needed for leap seconds.</p>"
  ],
  [
    "<p>Allow <code class=\"type\">cidr</code> data type to be\n            cast to <code class=\"type\">text</code> (Tom)</p>"
  ],
  [
    "<p>Disallow invalid time zone names in SET TIMEZONE</p>"
  ],
  [
    "<p>Trim trailing spaces when <code class=\"type\">char</code> is cast to <code class=\"type\">varchar</code> or <code class=\"type\">text</code>\n            (Tom)</p>"
  ],
  [
    "<p>Make <code class=\"type\">float(<em class=\"replaceable\"><code>p</code></em>)</code> measure the\n            precision <em class=\"replaceable\"><code>p</code></em>\n            in binary digits, not decimal digits (Tom)</p>"
  ],
  [
    "<p>Add IPv6 support to the <code class=\"type\">inet</code> and <code class=\"type\">cidr</code>\n            data types (Michael Graff)</p>"
  ],
  [
    "<p>Add <code class=\"function\">family()</code> function\n            to report whether address is IPv4 or IPv6 (Michael\n            Graff)</p>"
  ],
  [
    "<p>Have <code class=\"literal\">SHOW datestyle</code>\n            generate output similar to that used by <code class=\"literal\">SET datestyle</code> (Tom)</p>"
  ],
  [
    "<p>Make <code class=\"literal\">EXTRACT(TIMEZONE)</code>\n            and <code class=\"literal\">SET/SHOW TIME ZONE</code>\n            follow the SQL convention for the sign of time zone\n            offsets, i.e., positive is east from UTC (Tom)</p>"
  ],
  [
    "<p>Fix <code class=\"literal\">date_trunc('quarter',\n            ...)</code> (B&#xF6;jthe Zolt&#xE1;n)</p>",
    "<p>Prior releases returned an incorrect value for this\n            function call.</p>"
  ],
  [
    "<p>Make <code class=\"function\">initcap()</code> more\n            compatible with Oracle (Mike Nolan)</p>",
    "<p><code class=\"function\">initcap()</code> now\n            uppercases a letter appearing after any\n            non-alphanumeric character, rather than only after\n            whitespace.</p>"
  ],
  [
    "<p>Allow only <code class=\"varname\">datestyle</code>\n            field order for date values not in ISO-8601 format\n            (Greg)</p>"
  ],
  [
    "<p>Add new <code class=\"varname\">datestyle</code>\n            values <code class=\"literal\">MDY</code>, <code class=\"literal\">DMY</code>, and <code class=\"literal\">YMD</code> to set input field order; honor\n            <code class=\"literal\">US</code> and <code class=\"literal\">European</code> for backward compatibility\n            (Tom)</p>"
  ],
  [
    "<p>String literals like <code class=\"literal\">'now'</code> or <code class=\"literal\">'today'</code> will no longer work as a\n            column default. Use functions such as <code class=\"function\">now()</code>, <code class=\"function\">current_timestamp</code> instead. (change\n            required for prepared statements) (Tom)</p>"
  ],
  [
    "<p>Treat NaN as larger than any other value in\n            <code class=\"function\">min()</code>/<code class=\"function\">max()</code> (Tom)</p>",
    "<p>NaN was already sorted after ordinary numeric values\n            for most purposes, but <code class=\"function\">min()</code> and <code class=\"function\">max()</code> didn't get this right.</p>"
  ],
  [
    "<p>Prevent interval from suppressing <code class=\"literal\">:00</code> seconds display</p>"
  ],
  [
    "<p>New functions <code class=\"function\">pg_get_triggerdef(prettyprint)</code> and\n            <code class=\"function\">pg_conversion_is_visible()</code>\n            (Christopher)</p>"
  ],
  [
    "<p>Allow time to be specified as <code class=\"literal\">040506</code> or <code class=\"literal\">0405</code> (Tom)</p>"
  ],
  [
    "<p>Input date order must now be <code class=\"literal\">YYYY-MM-DD</code> (with 4-digit year) or\n            match <code class=\"varname\">datestyle</code></p>"
  ],
  [
    "<p>Make <code class=\"function\">pg_get_constraintdef</code> support unique,\n            primary-key, and check constraints (Christopher)</p>"
  ],
  [
    "<p>Prevent PL/pgSQL crash when <code class=\"literal\">RETURN NEXT</code> is used on a zero-row\n            record variable (Tom)</p>"
  ],
  [
    "<p>Make PL/Python's <code class=\"function\">spi_execute</code> interface handle null\n            values properly (Andrew Bosma)</p>"
  ],
  [
    "<p>Allow PL/pgSQL to declare variables of composite\n            types without <code class=\"literal\">%ROWTYPE</code>\n            (Tom)</p>"
  ],
  [
    "<p>Fix PL/Python's <code class=\"function\">_quote()</code> function to handle big\n            integers</p>"
  ],
  [
    "<p>Make PL/Python an untrusted language, now called\n            <code class=\"literal\">plpythonu</code> (Kevin Jacobs,\n            Tom)</p>",
    "<p>The Python language no longer supports a restricted\n            execution environment, so the trusted version of\n            PL/Python was removed. If this situation changes, a\n            version of PL/Python that can be used by non-superusers\n            will be readded.</p>"
  ],
  [
    "<p>Allow polymorphic PL/pgSQL functions (Joe, Tom)</p>"
  ],
  [
    "<p>Allow polymorphic SQL functions (Joe)</p>"
  ],
  [
    "<p>Improved compiled function caching mechanism in\n            PL/pgSQL with full support for polymorphism (Joe)</p>"
  ],
  [
    "<p>Add new parameter <code class=\"literal\">$0</code> in\n            PL/pgSQL representing the function's actual return type\n            (Joe)</p>"
  ],
  [
    "<p>Allow PL/Tcl and PL/Python to use the same trigger\n            on multiple tables (Tom)</p>"
  ],
  [
    "<p>Fixed PL/Tcl's <code class=\"function\">spi_prepare</code> to accept fully qualified\n            type names in the parameter type list (Jan)</p>"
  ],
  [
    "<p>Add <code class=\"literal\">\\pset pager always</code>\n            to always use pager (Greg)</p>",
    "<p>This forces the pager to be used even if the number\n            of rows is less than the screen height. This is\n            valuable for rows that wrap across several screen\n            rows.</p>"
  ],
  [
    "<p>Improve tab completion (Rod, Ross Reedstrom, Ian\n            Barwick)</p>"
  ],
  [
    "<p>Reorder <code class=\"literal\">\\?</code> help into\n            groupings (Harald Armin Massa, Bruce)</p>"
  ],
  [
    "<p>Add backslash commands for listing schemas, casts,\n            and conversions (Christopher)</p>"
  ],
  [
    "<p><code class=\"command\">\\encoding</code> now changes\n            based on the server parameter <code class=\"varname\">client_encoding</code> (Tom)</p>",
    "<p>In previous versions, <code class=\"command\">\\encoding</code> was not aware of encoding\n            changes made using <code class=\"literal\">SET\n            client_encoding</code>.</p>"
  ],
  [
    "<p>Save editor buffer into readline history (Ross)</p>",
    "<p>When <code class=\"command\">\\e</code> is used to edit\n            a query, the result is saved in the readline history\n            for retrieval using the up arrow.</p>"
  ],
  [
    "<p>Improve <code class=\"command\">\\d</code> display\n            (Christopher)</p>"
  ],
  [
    "<p>Enhance HTML mode to be more standards-conforming\n            (Greg)</p>"
  ],
  [
    "<p>New <code class=\"command\">\\set AUTOCOMMIT off</code>\n            capability (Tom)</p>",
    "<p>This takes the place of the removed server parameter\n            <code class=\"varname\">autocommit</code>.</p>"
  ],
  [
    "<p>New <code class=\"command\">\\set VERBOSITY</code> to\n            control error detail (Tom)</p>",
    "<p>This controls the new error reporting details.</p>"
  ],
  [
    "<p>New prompt escape sequence <code class=\"literal\">%x</code> to show transaction status\n            (Tom)</p>"
  ],
  [
    "<p>Long options for <span class=\"application\">psql</span> are now available on all\n            platforms</p>"
  ],
  [
    "<p>Multiple pg_dump fixes, including tar format and\n            large objects</p>"
  ],
  [
    "<p>Allow pg_dump to dump specific schemas (Neil)</p>"
  ],
  [
    "<p>Make pg_dump preserve column storage characteristics\n            (Christopher)</p>",
    "<p>This preserves <code class=\"literal\">ALTER TABLE ...\n            SET STORAGE</code> information.</p>"
  ],
  [
    "<p>Make pg_dump preserve <code class=\"command\">CLUSTER</code> characteristics\n            (Christopher)</p>"
  ],
  [
    "<p>Have pg_dumpall use <code class=\"command\">GRANT</code>/<code class=\"command\">REVOKE</code> to dump database-level\n            privileges (Tom)</p>"
  ],
  [
    "<p>Allow pg_dumpall to support the options <code class=\"option\">-a</code>, <code class=\"option\">-s</code>,\n            <code class=\"option\">-x</code> of pg_dump (Tom)</p>"
  ],
  [
    "<p>Prevent pg_dump from lowercasing identifiers\n            specified on the command line (Tom)</p>"
  ],
  [
    "<p>pg_dump options <code class=\"option\">--use-set-session-authorization</code> and\n            <code class=\"option\">--no-reconnect</code> now do\n            nothing, all dumps use <code class=\"command\">SET\n            SESSION AUTHORIZATION</code></p>",
    "<p>pg_dump no longer reconnects to switch users, but\n            instead always uses <code class=\"command\">SET SESSION\n            AUTHORIZATION</code>. This will reduce password\n            prompting during restores.</p>"
  ],
  [
    "<p>Long options for <span class=\"application\">pg_dump</span> are now available on all\n            platforms</p>",
    "<p><span class=\"productname\">PostgreSQL</span> now\n            includes its own long-option processing routines.</p>"
  ],
  [
    "<p>Add function <code class=\"function\">PQfreemem</code>\n            for freeing memory on Windows, suggested for\n            <code class=\"command\">NOTIFY</code> (Bruce)</p>",
    "<p>Windows requires that memory allocated in a library\n            be freed by a function in the same library, hence\n            <code class=\"function\">free()</code> doesn't work for\n            freeing memory allocated by libpq. <code class=\"function\">PQfreemem</code> is the proper way to free\n            libpq memory, especially on Windows, and is recommended\n            for other platforms as well.</p>"
  ],
  [
    "<p>Document service capability, and add sample file\n            (Bruce)</p>",
    "<p>This allows clients to look up connection\n            information in a central file on the client\n            machine.</p>"
  ],
  [
    "<p>Make <code class=\"function\">PQsetdbLogin</code> have\n            the same defaults as <code class=\"function\">PQconnectdb</code> (Tom)</p>"
  ],
  [
    "<p>Allow libpq to cleanly fail when result sets are too\n            large (Tom)</p>"
  ],
  [
    "<p>Improve performance of function <code class=\"function\">PQunescapeBytea</code> (Ben Lamb)</p>"
  ],
  [
    "<p>Allow thread-safe libpq with <code class=\"filename\">configure</code> option <code class=\"option\">--enable-thread-safety</code> (Lee Kindness,\n            Philip Yarra)</p>"
  ],
  [
    "<p>Allow function <code class=\"function\">pqInternalNotice</code> to accept a format\n            string and arguments instead of just a preformatted\n            message (Tom, Sean Chittenden)</p>"
  ],
  [
    "<p>Control SSL negotiation with <code class=\"literal\">sslmode</code> values <code class=\"literal\">disable</code>, <code class=\"literal\">allow</code>, <code class=\"literal\">prefer</code>, and <code class=\"literal\">require</code> (Jon Jensen)</p>"
  ],
  [
    "<p>Allow new error codes and levels of text (Tom)</p>"
  ],
  [
    "<p>Allow access to the underlying table and column of a\n            query result (Tom)</p>",
    "<p>This is helpful for query-builder applications that\n            want to know the underlying table and column names\n            associated with a specific result set.</p>"
  ],
  [
    "<p>Allow access to the current transaction status\n            (Tom)</p>"
  ],
  [
    "<p>Add ability to pass binary data directly to the\n            server (Tom)</p>"
  ],
  [
    "<p>Add function <code class=\"function\">PQexecPrepared</code> and <code class=\"function\">PQsendQueryPrepared</code> functions which\n            perform bind/execute of previously prepared statements\n            (Tom)</p>"
  ],
  [
    "<p>Allow <code class=\"function\">setNull</code> on\n            updateable result sets</p>"
  ],
  [
    "<p>Allow <code class=\"function\">executeBatch</code> on\n            a prepared statement (Barry)</p>"
  ],
  [
    "<p>Support SSL connections (Barry)</p>"
  ],
  [
    "<p>Handle schema names in result sets (Paul\n            Sorenson)</p>"
  ],
  [
    "<p>Add refcursor support (Nic Ferrier)</p>"
  ],
  [
    "<p>Prevent possible memory leak or core dump during\n            libpgtcl shutdown (Tom)</p>"
  ],
  [
    "<p>Add Informix compatibility to ECPG (Michael)</p>",
    "<p>This allows ECPG to process embedded C programs that\n            were written using certain Informix extensions.</p>"
  ],
  [
    "<p>Add type <code class=\"type\">decimal</code> to ECPG\n            that is fixed length, for Informix (Michael)</p>"
  ],
  [
    "<p>Allow thread-safe embedded SQL programs with\n            <code class=\"filename\">configure</code> option\n            <code class=\"option\">--enable-thread-safety</code> (Lee\n            Kindness, Bruce)</p>",
    "<p>This allows multiple threads to access the database\n            at the same time.</p>"
  ],
  [
    "<p>Moved Python client PyGreSQL to <a class=\"ulink\" href=\"http://www.pygresql.org\" target=\"_top\">http://www.pygresql.org</a> (Marc)</p>"
  ],
  [
    "<p>Prevent need for separate platform geometry\n            regression result files (Tom)</p>"
  ],
  [
    "<p>Improved PPC locking primitive (Reinhard Max)</p>"
  ],
  [
    "<p>New function <code class=\"function\">palloc0</code>\n            to allocate and clear memory (Bruce)</p>"
  ],
  [
    "<p>Fix locking code for s390x CPU (64-bit) (Tom)</p>"
  ],
  [
    "<p>Allow OpenBSD to use local ident credentials\n            (William Ahern)</p>"
  ],
  [
    "<p>Make query plan trees read-only to executor\n            (Tom)</p>"
  ],
  [
    "<p>Add macOS startup scripts (David Wheeler)</p>"
  ],
  [
    "<p>Allow libpq to compile with Borland C++ compiler\n            (Lester Godwin, Karl Waclawek)</p>"
  ],
  [
    "<p>Use our own version of <code class=\"function\">getopt_long()</code> if needed (Peter)</p>"
  ],
  [
    "<p>Convert administration scripts to C (Peter)</p>"
  ],
  [
    "<p>Bison &gt;= 1.85 is now required to build the\n            <span class=\"productname\">PostgreSQL</span> grammar, if\n            building from CVS</p>"
  ],
  [
    "<p>Merge documentation into one book (Peter)</p>"
  ],
  [
    "<p>Add Windows compatibility functions (Bruce)</p>"
  ],
  [
    "<p>Allow client interfaces to compile under MinGW\n            (Bruce)</p>"
  ],
  [
    "<p>New <code class=\"function\">ereport()</code> function\n            for error reporting (Tom)</p>"
  ],
  [
    "<p>Support Intel compiler on Linux (Peter)</p>"
  ],
  [
    "<p>Improve Linux startup scripts (Slawomir Sudnik,\n            Darko Prenosil)</p>"
  ],
  [
    "<p>Add support for AMD Opteron and Itanium (Jeffrey W.\n            Baker, Bruce)</p>"
  ],
  [
    "<p>Remove <code class=\"option\">--enable-recode</code>\n            option from <code class=\"command\">configure</code></p>",
    "<p>This was no longer needed now that we have\n            <code class=\"command\">CREATE CONVERSION</code>.</p>"
  ],
  [
    "<p>Generate a compile error if spinlock code is not\n            found (Bruce)</p>",
    "<p>Platforms without spinlock code will now fail to\n            compile, rather than silently using semaphores. This\n            failure can be disabled with a new <code class=\"command\">configure</code> option.</p>"
  ],
  [
    "<p>Change dbmirror license to BSD</p>"
  ],
  [
    "<p>Improve earthdistance (Bruno Wolff III)</p>"
  ],
  [
    "<p>Portability improvements to pgcrypto (Marko\n            Kreen)</p>"
  ],
  [
    "<p>Prevent crash in xml (John Gray, Michael\n            Richards)</p>"
  ],
  [
    "<p>Update oracle</p>"
  ],
  [
    "<p>Update mysql</p>"
  ],
  [
    "<p>Update cube (Bruno Wolff III)</p>"
  ],
  [
    "<p>Update earthdistance to use cube (Bruno Wolff\n            III)</p>"
  ],
  [
    "<p>Update btree_gist (Oleg)</p>"
  ],
  [
    "<p>New tsearch2 full-text search module (Oleg,\n            Teodor)</p>"
  ],
  [
    "<p>Add hash-based crosstab function to tablefuncs\n            (Joe)</p>"
  ],
  [
    "<p>Add serial column to order <code class=\"function\">connectby()</code> siblings in tablefuncs\n            (Nabil Sayegh,Joe)</p>"
  ],
  [
    "<p>Add named persistent connections to dblink (Shridhar\n            Daithanka)</p>"
  ],
  [
    "<p>New pg_autovacuum allows automatic <code class=\"command\">VACUUM</code> (Matthew T. O'Connor)</p>"
  ],
  [
    "<p>Make pgbench honor environment variables\n            <code class=\"envar\">PGHOST</code>, <code class=\"envar\">PGPORT</code>, <code class=\"envar\">PGUSER</code> (Tatsuo)</p>"
  ],
  [
    "<p>Improve intarray (Teodor Sigaev)</p>"
  ],
  [
    "<p>Improve pgstattuple (Rod)</p>"
  ],
  [
    "<p>Fix bug in <code class=\"function\">metaphone()</code>\n            in fuzzystrmatch</p>"
  ],
  [
    "<p>Improve adddepend (Rod)</p>"
  ],
  [
    "<p>Update spi/timetravel (B&#xF6;jthe Zolt&#xE1;n)</p>"
  ],
  [
    "<p>Fix dbase <code class=\"option\">-s</code> option and\n            improve non-ASCII handling (Thomas Behr, M&#xE1;rcio\n            Smiderle)</p>"
  ],
  [
    "<p>Remove array module because features now included by\n            default (Joe)</p>"
  ]
]