[
  [
    "<p>Fix <code class=\"command\">VACUUM</code>'s tests to see\n          whether it can update <code class=\"structfield\">relfrozenxid</code> (Andres Freund)</p>",
    "<p>In some cases <code class=\"command\">VACUUM</code>\n          (either manual or autovacuum) could incorrectly advance a\n          table's <code class=\"structfield\">relfrozenxid</code>\n          value, allowing tuples to escape freezing, causing those\n          rows to become invisible once 2^31 transactions have\n          elapsed. The probability of data loss is fairly low since\n          multiple incorrect advancements would need to happen\n          before actual loss occurs, but it's not zero. Users\n          upgrading from releases 9.0.4 or 8.4.8 or earlier are not\n          affected, but all later versions contain the bug.</p>",
    "<p>The issue can be ameliorated by, after upgrading,\n          vacuuming all tables in all databases while having\n          <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-client.html#GUC-VACUUM-FREEZE-TABLE-AGE\"><code class=\"varname\">\n          vacuum_freeze_table_age</code></a> set to zero. This will\n          fix any latent corruption but will not be able to fix all\n          pre-existing data errors. However, an installation can be\n          presumed safe after performing this vacuuming if it has\n          executed fewer than 2^31 update transactions in its\n          lifetime (check this with <code class=\"literal\">SELECT\n          txid_current() &lt; 2^31</code>).</p>"
  ],
  [
    "<p>Fix initialization of <code class=\"filename\">pg_clog</code> and <code class=\"filename\">pg_subtrans</code> during hot standby startup\n          (Andres Freund, Heikki Linnakangas)</p>",
    "<p>This bug can cause data loss on standby servers at the\n          moment they start to accept hot-standby queries, by\n          marking committed transactions as uncommitted. The\n          likelihood of such corruption is small unless, at the\n          time of standby startup, the primary server has executed\n          many updating transactions since its last checkpoint.\n          Symptoms include missing rows, rows that should have been\n          deleted being still visible, and obsolete versions of\n          updated rows being still visible alongside their newer\n          versions.</p>",
    "<p>This bug was introduced in versions 9.3.0, 9.2.5,\n          9.1.10, and 9.0.14. Standby servers that have only been\n          running earlier releases are not at risk. It's\n          recommended that standby servers that have ever run any\n          of the buggy releases be re-cloned from the primary\n          (e.g., with a new base backup) after upgrading.</p>"
  ],
  [
    "<p>Truncate <code class=\"filename\">pg_multixact</code>\n          contents during WAL replay (Andres Freund)</p>",
    "<p>This avoids ever-increasing disk space consumption in\n          standby servers.</p>"
  ],
  [
    "<p>Fix race condition in GIN index posting tree page\n          deletion (Heikki Linnakangas)</p>",
    "<p>This could lead to transient wrong answers or query\n          failures.</p>"
  ],
  [
    "<p>Avoid flattening a subquery whose <code class=\"literal\">SELECT</code> list contains a volatile function\n          wrapped inside a sub-<code class=\"literal\">SELECT</code>\n          (Tom Lane)</p>",
    "<p>This avoids unexpected results due to extra\n          evaluations of the volatile function.</p>"
  ],
  [
    "<p>Fix planner's processing of non-simple-variable\n          subquery outputs nested within outer joins (Tom Lane)</p>",
    "<p>This error could lead to incorrect plans for queries\n          involving multiple levels of subqueries within\n          <code class=\"literal\">JOIN</code> syntax.</p>"
  ],
  [
    "<p>Fix premature deletion of temporary files (Andres\n          Freund)</p>"
  ],
  [
    "<p>Fix possible read past end of memory in rule printing\n          (Peter Eisentraut)</p>"
  ],
  [
    "<p>Fix array slicing of <code class=\"type\">int2vector</code> and <code class=\"type\">oidvector</code> values (Tom Lane)</p>",
    "<p>Expressions of this kind are now implicitly promoted\n          to regular <code class=\"type\">int2</code> or <code class=\"type\">oid</code> arrays.</p>"
  ],
  [
    "<p>Fix incorrect behaviors when using a SQL-standard,\n          simple GMT offset timezone (Tom Lane)</p>",
    "<p>In some cases, the system would use the simple GMT\n          offset value when it should have used the regular\n          timezone setting that had prevailed before the simple\n          offset was selected. This change also causes the\n          <code class=\"function\">timeofday</code> function to honor\n          the simple GMT offset zone.</p>"
  ],
  [
    "<p>Prevent possible misbehavior when logging translations\n          of Windows error codes (Tom Lane)</p>"
  ],
  [
    "<p>Properly quote generated command lines in <span class=\"application\">pg_ctl</span> (Naoya Anzai and Tom\n          Lane)</p>",
    "<p>This fix applies only to Windows.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_dumpall</span> to\n          work when a source database sets <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-client.html#GUC-DEFAULT-TRANSACTION-READ-ONLY\">\n          <code class=\"varname\">default_transaction_read_only</code></a> via\n          <code class=\"command\">ALTER DATABASE SET</code> (Kevin\n          Grittner)</p>",
    "<p>Previously, the generated script would fail during\n          restore.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">ecpg</span>'s processing\n          of lists of variables declared <code class=\"type\">varchar</code> (Zolt&#xE1;n B&#xF6;sz&#xF6;rm&#xE9;nyi)</p>"
  ],
  [
    "<p>Make <code class=\"filename\">contrib/lo</code> defend\n          against incorrect trigger definitions (Marc Cousin)</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2013h for DST law\n          changes in Argentina, Brazil, Jordan, Libya,\n          Liechtenstein, Morocco, and Palestine. Also, new timezone\n          abbreviations WIB, WIT, WITA for Indonesia.</p>"
  ]
]