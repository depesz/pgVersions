[
  [
    "<p>Fix Windows shared-memory allocation code (Tsutomu\n          Yamada, Magnus)</p>",
    "<p>This bug led to the often-reported <span class=\"quote\">&#x201C;<span class=\"quote\">could not reattach to shared\n          memory</span>&#x201D;</span> error message.</p>"
  ],
  [
    "<p>Force WAL segment switch during <code class=\"function\">pg_start_backup()</code> (Heikki)</p>",
    "<p>This avoids corner cases that could render a base\n          backup unusable.</p>"
  ],
  [
    "<p>Disallow <code class=\"command\">RESET ROLE</code> and\n          <code class=\"command\">RESET SESSION AUTHORIZATION</code>\n          inside security-definer functions (Tom, Heikki)</p>",
    "<p>This covers a case that was missed in the previous\n          patch that disallowed <code class=\"command\">SET\n          ROLE</code> and <code class=\"command\">SET SESSION\n          AUTHORIZATION</code> inside security-definer functions.\n          (See CVE-2007-6600)</p>"
  ],
  [
    "<p>Make <code class=\"command\">LOAD</code> of an\n          already-loaded loadable module into a no-op (Tom)</p>",
    "<p>Formerly, <code class=\"command\">LOAD</code> would\n          attempt to unload and re-load the module, but this is\n          unsafe and not all that useful.</p>"
  ],
  [
    "<p>Disallow empty passwords during LDAP authentication\n          (Magnus)</p>"
  ],
  [
    "<p>Fix handling of sub-SELECTs appearing in the arguments\n          of an outer-level aggregate function (Tom)</p>"
  ],
  [
    "<p>Fix bugs associated with fetching a whole-row value\n          from the output of a Sort or Materialize plan node\n          (Tom)</p>"
  ],
  [
    "<p>Prevent <code class=\"varname\">synchronize_seqscans</code> from changing the\n          results of scrollable and <code class=\"literal\">WITH\n          HOLD</code> cursors (Tom)</p>"
  ],
  [
    "<p>Revert planner change that disabled partial-index and\n          constraint exclusion optimizations when there were more\n          than 100 clauses in an AND or OR list (Tom)</p>"
  ],
  [
    "<p>Fix hash calculation for data type <code class=\"type\">interval</code> (Tom)</p>",
    "<p>This corrects wrong results for hash joins on interval\n          values. It also changes the contents of hash indexes on\n          interval columns. If you have any such indexes, you must\n          <code class=\"command\">REINDEX</code> them after\n          updating.</p>"
  ],
  [
    "<p>Treat <code class=\"function\">to_char(..., 'TH')</code>\n          as an uppercase ordinal suffix with <code class=\"literal\">'HH'</code>/<code class=\"literal\">'HH12'</code>\n          (Heikki)</p>",
    "<p>It was previously handled as <code class=\"literal\">'th'</code> (lowercase).</p>"
  ],
  [
    "<p>Fix overflow for <code class=\"literal\">INTERVAL\n          '<em class=\"replaceable\"><code>x</code></em> ms'</code>\n          when <em class=\"replaceable\"><code>x</code></em> is more\n          than 2 million and integer datetimes are in use (Alex\n          Hunsaker)</p>"
  ],
  [
    "<p>Fix calculation of distance between a point and a line\n          segment (Tom)</p>",
    "<p>This led to incorrect results from a number of\n          geometric operators.</p>"
  ],
  [
    "<p>Fix <code class=\"type\">money</code> data type to work\n          in locales where currency amounts have no fractional\n          digits, e.g. Japan (Itagaki Takahiro)</p>"
  ],
  [
    "<p>Fix <code class=\"literal\">LIKE</code> for case where\n          pattern contains <code class=\"literal\">%_</code>\n          (Tom)</p>"
  ],
  [
    "<p>Properly round datetime input like <code class=\"literal\">00:12:57.9999999999999999999999999999</code>\n          (Tom)</p>"
  ],
  [
    "<p>Fix memory leaks in XML operations (Tom)</p>"
  ],
  [
    "<p>Fix poor choice of page split point in GiST R-tree\n          operator classes (Teodor)</p>"
  ],
  [
    "<p>Ensure that a <span class=\"quote\">&#x201C;<span class=\"quote\">fast shutdown</span>&#x201D;</span> request will\n          forcibly terminate open sessions, even if a <span class=\"quote\">&#x201C;<span class=\"quote\">smart\n          shutdown</span>&#x201D;</span> was already in progress (Fujii\n          Masao)</p>"
  ],
  [
    "<p>Avoid performance degradation in bulk inserts into GIN\n          indexes when the input values are (nearly) in sorted\n          order (Tom)</p>"
  ],
  [
    "<p>Correctly enforce NOT NULL domain constraints in some\n          contexts in PL/pgSQL (Tom)</p>"
  ],
  [
    "<p>Fix portability issues in plperl initialization\n          (Andrew Dunstan)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_ctl</span> to not go\n          into an infinite loop if <code class=\"filename\">postgresql.conf</code> is empty (Jeff\n          Davis)</p>"
  ],
  [
    "<p>Improve <span class=\"application\">pg_dump</span>'s\n          efficiency when there are many large objects (Tamas\n          Vincze)</p>"
  ],
  [
    "<p>Use <code class=\"literal\">SIGUSR1</code>, not\n          <code class=\"literal\">SIGQUIT</code>, as the failover\n          signal for <span class=\"application\">pg_standby</span>\n          (Heikki)</p>"
  ],
  [
    "<p>Make <span class=\"application\">pg_standby</span>'s\n          <code class=\"literal\">maxretries</code> option behave as\n          documented (Fujii Masao)</p>"
  ],
  [
    "<p>Make <code class=\"filename\">contrib/hstore</code>\n          throw an error when a key or value is too long to fit in\n          its data structure, rather than silently truncating it\n          (Andrew Gierth)</p>"
  ],
  [
    "<p>Fix <code class=\"filename\">contrib/xml2</code>'s\n          <code class=\"function\">xslt_process()</code> to properly\n          handle the maximum number of parameters (twenty)\n          (Tom)</p>"
  ],
  [
    "<p>Improve robustness of <span class=\"application\">libpq</span>'s code to recover from errors\n          during <code class=\"command\">COPY FROM STDIN</code>\n          (Tom)</p>"
  ],
  [
    "<p>Avoid including conflicting readline and editline\n          header files when both libraries are installed (Zdenek\n          Kotala)</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2009l for DST law\n          changes in Bangladesh, Egypt, Jordan, Pakistan,\n          Argentina/San_Luis, Cuba, Jordan (historical correction\n          only), Mauritius, Morocco, Palestine, Syria, Tunisia.</p>"
  ]
]