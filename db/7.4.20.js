[
  [
    "<p>Fix conversions between ISO-8859-5 and other encodings\n          to handle Cyrillic <span class=\"quote\">&#x201C;<span class=\"quote\">Yo</span>&#x201D;</span> characters (<code class=\"literal\">e</code> and <code class=\"literal\">E</code>\n          with two dots) (Sergey Burladyan)</p>"
  ],
  [
    "<p>Fix a few datatype input functions that were allowing\n          unused bytes in their results to contain uninitialized,\n          unpredictable values (Tom)</p>",
    "<p>This could lead to failures in which two apparently\n          identical literal values were not seen as equal,\n          resulting in the parser complaining about unmatched\n          <code class=\"literal\">ORDER BY</code> and <code class=\"literal\">DISTINCT</code> expressions.</p>"
  ],
  [
    "<p>Fix a corner case in regular-expression substring\n          matching (<code class=\"literal\">substring(<em class=\"replaceable\"><code>string</code></em> from <em class=\"replaceable\"><code>pattern</code></em>)</code>)\n          (Tom)</p>",
    "<p>The problem occurs when there is a match to the\n          pattern overall but the user has specified a\n          parenthesized subexpression and that subexpression hasn't\n          got a match. An example is <code class=\"literal\">substring('foo' from 'foo(bar)?')</code>. This\n          should return NULL, since <code class=\"literal\">(bar)</code> isn't matched, but it was\n          mistakenly returning the whole-pattern match instead (ie,\n          <code class=\"literal\">foo</code>).</p>"
  ],
  [
    "<p>Fix incorrect result from <span class=\"application\">ecpg</span>'s <code class=\"function\">PGTYPEStimestamp_sub()</code> function\n          (Michael)</p>"
  ],
  [
    "<p>Fix <code class=\"literal\">DatumGetBool</code> macro to\n          not fail with <span class=\"application\">gcc</span> 4.3\n          (Tom)</p>",
    "<p>This problem affects <span class=\"quote\">&#x201C;<span class=\"quote\">old style</span>&#x201D;</span> (V0) C functions that\n          return boolean. The fix is already in 8.3, but the need\n          to back-patch it was not realized at the time.</p>"
  ],
  [
    "<p>Fix longstanding <code class=\"command\">LISTEN</code>/<code class=\"command\">NOTIFY</code> race condition (Tom)</p>",
    "<p>In rare cases a session that had just executed a\n          <code class=\"command\">LISTEN</code> might not get a\n          notification, even though one would be expected because\n          the concurrent transaction executing <code class=\"command\">NOTIFY</code> was observed to commit later.</p>",
    "<p>A side effect of the fix is that a transaction that\n          has executed a not-yet-committed <code class=\"command\">LISTEN</code> command will not see any row in\n          <code class=\"structname\">pg_listener</code> for the\n          <code class=\"command\">LISTEN</code>, should it choose to\n          look; formerly it would have. This behavior was never\n          documented one way or the other, but it is possible that\n          some applications depend on the old behavior.</p>"
  ],
  [
    "<p>Fix display of constant expressions in <code class=\"literal\">ORDER BY</code> and <code class=\"literal\">GROUP\n          BY</code> (Tom)</p>",
    "<p>An explicitly casted constant would be shown\n          incorrectly. This could for example lead to corruption of\n          a view definition during dump and reload.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">libpq</span> to handle\n          NOTICE messages correctly during COPY OUT (Tom)</p>",
    "<p>This failure has only been observed to occur when a\n          user-defined datatype's output routine issues a NOTICE,\n          but there is no guarantee it couldn't happen due to other\n          causes.</p>"
  ]
]