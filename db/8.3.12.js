[
  [
    "<p>Use a separate interpreter for each calling SQL userid\n          in PL/Perl and PL/Tcl (Tom Lane)</p>",
    "<p>This change prevents security problems that can be\n          caused by subverting Perl or Tcl code that will be\n          executed later in the same session under another SQL user\n          identity (for example, within a <code class=\"literal\">SECURITY DEFINER</code> function). Most\n          scripting languages offer numerous ways that that might\n          be done, such as redefining standard functions or\n          operators called by the target function. Without this\n          change, any SQL user with Perl or Tcl language usage\n          rights can do essentially anything with the SQL\n          privileges of the target function's owner.</p>",
    "<p>The cost of this change is that intentional\n          communication among Perl and Tcl functions becomes more\n          difficult. To provide an escape hatch, PL/PerlU and\n          PL/TclU functions continue to use only one interpreter\n          per session. This is not considered a security issue\n          since all such functions execute at the trust level of a\n          database superuser already.</p>",
    "<p>It is likely that third-party procedural languages\n          that claim to offer trusted execution have similar\n          security issues. We advise contacting the authors of any\n          PL you are depending on for security-critical\n          purposes.</p>",
    "<p>Our thanks to Tim Bunce for pointing out this issue\n          (CVE-2010-3433).</p>"
  ],
  [
    "<p>Prevent possible crashes in <code class=\"function\">pg_get_expr()</code> by disallowing it from\n          being called with an argument that is not one of the\n          system catalog columns it's intended to be used with\n          (Heikki Linnakangas, Tom Lane)</p>"
  ],
  [
    "<p>Treat exit code 128 (<code class=\"literal\">ERROR_WAIT_NO_CHILDREN</code>) as non-fatal on\n          Windows (Magnus Hagander)</p>",
    "<p>Under high load, Windows processes will sometimes fail\n          at startup with this error code. Formerly the postmaster\n          treated this as a panic condition and restarted the whole\n          database, but that seems to be an overreaction.</p>"
  ],
  [
    "<p>Fix incorrect usage of non-strict OR joinclauses in\n          Append indexscans (Tom Lane)</p>",
    "<p>This is a back-patch of an 8.4 fix that was missed in\n          the 8.3 branch. This corrects an error introduced in\n          8.3.8 that could cause incorrect results for outer joins\n          when the inner relation is an inheritance tree or\n          <code class=\"literal\">UNION ALL</code> subquery.</p>"
  ],
  [
    "<p>Fix possible duplicate scans of <code class=\"literal\">UNION ALL</code> member relations (Tom\n          Lane)</p>"
  ],
  [
    "<p>Fix <span class=\"quote\">&#x201C;<span class=\"quote\">cannot\n          handle unplanned sub-select</span>&#x201D;</span> error (Tom\n          Lane)</p>",
    "<p>This occurred when a sub-select contains a join alias\n          reference that expands into an expression containing\n          another sub-select.</p>"
  ],
  [
    "<p>Fix failure to mark cached plans as transient (Tom\n          Lane)</p>",
    "<p>If a plan is prepared while <code class=\"command\">CREATE INDEX CONCURRENTLY</code> is in progress\n          for one of the referenced tables, it is supposed to be\n          re-planned once the index is ready for use. This was not\n          happening reliably.</p>"
  ],
  [
    "<p>Reduce PANIC to ERROR in some occasionally-reported\n          btree failure cases, and provide additional detail in the\n          resulting error messages (Tom Lane)</p>",
    "<p>This should improve the system's robustness with\n          corrupted indexes.</p>"
  ],
  [
    "<p>Prevent show_session_authorization() from crashing\n          within autovacuum processes (Tom Lane)</p>"
  ],
  [
    "<p>Defend against functions returning setof record where\n          not all the returned rows are actually of the same\n          rowtype (Tom Lane)</p>"
  ],
  [
    "<p>Fix possible failure when hashing a pass-by-reference\n          function result (Tao Ma, Tom Lane)</p>"
  ],
  [
    "<p>Improve merge join's handling of NULLs in the join\n          columns (Tom Lane)</p>",
    "<p>A merge join can now stop entirely upon reaching the\n          first NULL, if the sort order is such that NULLs sort\n          high.</p>"
  ],
  [
    "<p>Take care to fsync the contents of lockfiles (both\n          <code class=\"filename\">postmaster.pid</code> and the\n          socket lockfile) while writing them (Tom Lane)</p>",
    "<p>This omission could result in corrupted lockfile\n          contents if the machine crashes shortly after postmaster\n          start. That could in turn prevent subsequent attempts to\n          start the postmaster from succeeding, until the lockfile\n          is manually removed.</p>"
  ],
  [
    "<p>Avoid recursion while assigning XIDs to heavily-nested\n          subtransactions (Andres Freund, Robert Haas)</p>",
    "<p>The original coding could result in a crash if there\n          was limited stack space.</p>"
  ],
  [
    "<p>Avoid holding open old WAL segments in the walwriter\n          process (Magnus Hagander, Heikki Linnakangas)</p>",
    "<p>The previous coding would prevent removal of\n          no-longer-needed segments.</p>"
  ],
  [
    "<p>Fix <code class=\"varname\">log_line_prefix</code>'s\n          <code class=\"literal\">%i</code> escape, which could\n          produce junk early in backend startup (Tom Lane)</p>"
  ],
  [
    "<p>Fix possible data corruption in <code class=\"command\">ALTER TABLE ... SET TABLESPACE</code> when\n          archiving is enabled (Jeff Davis)</p>"
  ],
  [
    "<p>Allow <code class=\"command\">CREATE DATABASE</code> and\n          <code class=\"command\">ALTER DATABASE ... SET\n          TABLESPACE</code> to be interrupted by query-cancel\n          (Guillaume Lelarge)</p>"
  ],
  [
    "<p>Fix <code class=\"command\">REASSIGN OWNED</code> to\n          handle operator classes and families (Asko Tiidumaa)</p>"
  ],
  [
    "<p>Fix possible core dump when comparing two empty\n          <code class=\"type\">tsquery</code> values (Tom Lane)</p>"
  ],
  [
    "<p>Fix <code class=\"literal\">LIKE</code>'s handling of\n          patterns containing <code class=\"literal\">%</code>\n          followed by <code class=\"literal\">_</code> (Tom Lane)</p>",
    "<p>We've fixed this before, but there were still some\n          incorrectly-handled cases.</p>"
  ],
  [
    "<p>In PL/Python, defend against null pointer results from\n          <code class=\"function\">PyCObject_AsVoidPtr</code> and\n          <code class=\"function\">PyCObject_FromVoidPtr</code>\n          (Peter Eisentraut)</p>"
  ],
  [
    "<p>Make psql recognize <code class=\"command\">DISCARD\n          ALL</code> as a command that should not be encased in a\n          transaction block in autocommit-off mode (Itagaki\n          Takahiro)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">ecpg</span> to process\n          data from <code class=\"literal\">RETURNING</code> clauses\n          correctly (Michael Meskes)</p>"
  ],
  [
    "<p>Improve <code class=\"filename\">contrib/dblink</code>'s\n          handling of tables containing dropped columns (Tom\n          Lane)</p>"
  ],
  [
    "<p>Fix connection leak after <span class=\"quote\">&#x201C;<span class=\"quote\">duplicate connection\n          name</span>&#x201D;</span> errors in <code class=\"filename\">contrib/dblink</code> (Itagaki Takahiro)</p>"
  ],
  [
    "<p>Fix <code class=\"filename\">contrib/dblink</code> to\n          handle connection names longer than 62 bytes correctly\n          (Itagaki Takahiro)</p>"
  ],
  [
    "<p>Add <code class=\"function\">hstore(text, text)</code>\n          function to <code class=\"filename\">contrib/hstore</code>\n          (Robert Haas)</p>",
    "<p>This function is the recommended substitute for the\n          now-deprecated <code class=\"literal\">=&gt;</code>\n          operator. It was back-patched so that future-proofed code\n          can be used with older server versions. Note that the\n          patch will be effective only after <code class=\"filename\">contrib/hstore</code> is installed or\n          reinstalled in a particular database. Users might prefer\n          to execute the <code class=\"command\">CREATE\n          FUNCTION</code> command by hand, instead.</p>"
  ],
  [
    "<p>Update build infrastructure and documentation to\n          reflect the source code repository's move from CVS to Git\n          (Magnus Hagander and others)</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2010l for DST law\n          changes in Egypt and Palestine; also historical\n          corrections for Finland.</p>",
    "<p>This change also adds new names for two Micronesian\n          timezones: Pacific/Chuuk is now preferred over\n          Pacific/Truk (and the preferred abbreviation is CHUT not\n          TRUT) and Pacific/Pohnpei is preferred over\n          Pacific/Ponape.</p>"
  ],
  [
    "<p>Make Windows' <span class=\"quote\">&#x201C;<span class=\"quote\">N. Central Asia Standard Time</span>&#x201D;</span>\n          timezone map to Asia/Novosibirsk, not Asia/Almaty (Magnus\n          Hagander)</p>",
    "<p>Microsoft changed the DST behavior of this zone in the\n          timezone update from KB976098. Asia/Novosibirsk is a\n          better match to its new behavior.</p>"
  ]
]