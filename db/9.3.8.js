[
  [
    "<p>Avoid failures while <code class=\"function\">fsync</code>'ing data directory during crash\n          restart (Abhijit Menon-Sen, Tom Lane)</p>",
    "<p>In the previous minor releases we added a patch to\n          <code class=\"function\">fsync</code> everything in the\n          data directory after a crash. Unfortunately its response\n          to any error condition was to fail, thereby preventing\n          the server from starting up, even when the problem was\n          quite harmless. An example is that an unwritable file in\n          the data directory would prevent restart on some\n          platforms; but it is common to make SSL certificate files\n          unwritable by the server. Revise this behavior so that\n          permissions failures are ignored altogether, and other\n          types of failures are logged but do not prevent\n          continuing.</p>",
    "<p>Also apply the same rules in <code class=\"literal\">initdb --sync-only</code>. This case is less\n          critical but it should act similarly.</p>"
  ],
  [
    "<p>Fix <code class=\"function\">pg_get_functiondef()</code>\n          to show functions' <code class=\"literal\">LEAKPROOF</code>\n          property, if set (Jeevan Chalke)</p>"
  ],
  [
    "<p>Remove <span class=\"application\">configure</span>'s\n          check prohibiting linking to a threaded <span class=\"application\">libpython</span> on <span class=\"systemitem\">OpenBSD</span> (Tom Lane)</p>",
    "<p>The failure this restriction was meant to prevent\n          seems to not be a problem anymore on current <span class=\"systemitem\">OpenBSD</span> versions.</p>"
  ],
  [
    "<p>Allow <span class=\"application\">libpq</span> to use\n          TLS protocol versions beyond v1 (Noah Misch)</p>",
    "<p>For a long time, <span class=\"application\">libpq</span> was coded so that the only SSL\n          protocol it would allow was TLS v1. Now that newer TLS\n          versions are becoming popular, allow it to negotiate the\n          highest commonly-supported TLS version with the server.\n          (<span class=\"productname\">PostgreSQL</span> servers were\n          already capable of such negotiation, so no change is\n          needed on the server side.) This is a back-patch of a\n          change already released in 9.4.0.</p>"
  ]
]