[
  [
    "<p>Fix handling of unknown-type arguments in <code class=\"literal\">DISTINCT</code> <code class=\"type\">\"any\"</code> aggregate functions (Tom Lane)</p>",
    "<p>This error led to a <code class=\"type\">text</code>-type value being interpreted as an <code class=\"type\">unknown</code>-type value (that is, a zero-terminated string) at runtime. This could result in disclosure of server memory following the <code class=\"type\">text</code> value.</p>",
    "<p>The <span class=\"productname\">PostgreSQL</span> Project thanks Jingzhou Fu for reporting this problem. (CVE-2023-5868)</p>"
  ],
  [
    "<p>Detect integer overflow while computing new array dimensions (Tom Lane)</p>",
    "<p>When assigning new elements to array subscripts that are outside the current array bounds, an undetected integer overflow could occur in edge cases. Memory stomps that are potentially exploitable for arbitrary code execution are possible, and so is disclosure of server memory.</p>",
    "<p>The <span class=\"productname\">PostgreSQL</span> Project thanks Pedro Gallegos for reporting this problem. (CVE-2023-5869)</p>"
  ],
  [
    "<p>Prevent the <code class=\"literal\">pg_signal_backend</code> role from signalling background workers and autovacuum processes (Noah Misch, Jelte Fennema-Nio)</p>",
    "<p>The documentation says that <code class=\"literal\">pg_signal_backend</code> cannot issue signals to superuser-owned processes. It was able to signal these background processes, though, because they advertise a role OID of zero. Treat that as indicating superuser ownership. The security implications of cancelling one of these process types are fairly small so far as the core code goes (we'll just start another one), but extensions might add background workers that are more vulnerable.</p>",
    "<p>Also ensure that the <code class=\"varname\">is_superuser</code> parameter is set correctly in such processes. No specific security consequences are known for that oversight, but it might be significant for some extensions.</p>",
    "<p>The <span class=\"productname\">PostgreSQL</span> Project thanks Hemanth Sandrana and Mahendrakar Srinivasarao for reporting this problem. (CVE-2023-5870)</p>"
  ],
  [
    "<p>Fix misbehavior during recursive page split in GiST index build (Heikki Linnakangas)</p>",
    "<p>Fix a case where the location of a page downlink was incorrectly tracked, and introduce some logic to allow recovering from such situations rather than silently doing the wrong thing. This error could result in incorrect answers from subsequent index searches. It may be advisable to reindex all GiST indexes after installing this update.</p>"
  ],
  [
    "<p>Prevent de-duplication of btree index entries for <code class=\"type\">interval</code> columns (Noah Misch)</p>",
    "<p>There are <code class=\"type\">interval</code> values that are distinguishable but compare equal, for example <code class=\"literal\">24:00:00</code> and <code class=\"literal\">1 day</code>. This breaks assumptions made by btree de-duplication, so <code class=\"type\">interval</code> columns need to be excluded from de-duplication. This oversight can cause incorrect results from index-only scans. Moreover, after updating <span class=\"application\">amcheck</span> will report an error for almost all such indexes. Users should reindex any btree indexes on <code class=\"type\">interval</code> columns.</p>"
  ],
  [
    "<p>Fix partition step generation and runtime partition pruning for hash-partitioned tables with multiple partition keys (David Rowley)</p>",
    "<p>Some cases involving an <code class=\"literal\">IS NULL</code> condition on one of the partition keys could result in a crash.</p>"
  ],
  [
    "<p>Fix edge case in btree mark/restore processing of ScalarArrayOpExpr clauses (Peter Geoghegan)</p>",
    "<p>When restoring an indexscan to a previously marked position, the code could miss required setup steps if the scan had advanced exactly to the end of the matches for a ScalarArrayOpExpr (that is, an <code class=\"literal\">indexcol = ANY(ARRAY[])</code>) clause. This could result in missing some rows that should have been fetched.</p>"
  ],
  [
    "<p>Fix intra-query memory leak when a set-returning function repeatedly returns zero rows (Tom Lane)</p>"
  ],
  [
    "<p>Don't crash if <code class=\"function\">cursor_to_xmlschema()</code> is applied to a non-data-returning Portal (Boyu Yang)</p>"
  ],
  [
    "<p>Throw the intended error if <code class=\"function\">pgrowlocks()</code> is applied to a partitioned table (David Rowley)</p>",
    "<p>Previously, a not-on-point complaint <span class=\"quote\">“<span class=\"quote\">only heap AM is supported</span>”</span> would be raised.</p>"
  ],
  [
    "<p>Handle invalid indexes more cleanly in assorted SQL functions (Noah Misch)</p>",
    "<p>Report an error if <code class=\"function\">pgstatindex()</code>, <code class=\"function\">pgstatginindex()</code>, <code class=\"function\">pgstathashindex()</code>, or <code class=\"function\">pgstattuple()</code> is applied to an invalid index. If <code class=\"function\">brin_desummarize_range()</code>, <code class=\"function\">brin_summarize_new_values()</code>, <code class=\"function\">brin_summarize_range()</code>, or <code class=\"function\">gin_clean_pending_list()</code> is applied to an invalid index, do nothing except to report a debug-level message. Formerly these functions attempted to process the index, and might fail in strange ways depending on what the failed <code class=\"command\">CREATE INDEX</code> had left behind.</p>"
  ],
  [
    "<p>Avoid premature memory allocation failure with long inputs to <code class=\"function\">to_tsvector()</code> (Tom Lane)</p>"
  ],
  [
    "<p>Fix over-allocation of the constructed <code class=\"type\">tsvector</code> in <code class=\"function\">tsvectorrecv()</code> (Denis Erokhin)</p>",
    "<p>If the incoming vector includes position data, the binary receive function left wasted space (roughly equal to the size of the position data) in the finished <code class=\"type\">tsvector</code>. In extreme cases this could lead to <span class=\"quote\">“<span class=\"quote\">maximum total lexeme length exceeded</span>”</span> failures for vectors that were under the length limit when emitted. In any case it could lead to wasted space on-disk.</p>"
  ],
  [
    "<p>Fix incorrect coding in <code class=\"function\">gtsvector_picksplit()</code> (Alexander Lakhin)</p>",
    "<p>This could lead to poor page-split decisions in GiST indexes on <code class=\"type\">tsvector</code> columns.</p>"
  ],
  [
    "<p>Improve checks for corrupt PGLZ compressed data (Flavien Guedez)</p>"
  ],
  [
    "<p>Fix <code class=\"command\">COMMIT AND CHAIN</code>/<code class=\"command\">ROLLBACK AND CHAIN</code> to work properly when there is an unreleased savepoint (Liu Xiang, Tom Lane)</p>",
    "<p>Instead of propagating the current transaction's properties to the new transaction, they propagated some previous transaction's properties.</p>"
  ],
  [
    "<p>Avoid crash in <code class=\"command\">EXPLAIN</code> if a parameter marked to be displayed by <code class=\"command\">EXPLAIN</code> has a NULL boot-time value (Xing Guo, Aleksander Alekseev, Tom Lane)</p>",
    "<p>No built-in parameter fits this description, but an extension could define such a parameter.</p>"
  ],
  [
    "<p>Ensure we have a snapshot while dropping <code class=\"literal\">ON COMMIT DROP</code> temp tables (Tom Lane)</p>",
    "<p>This prevents possible misbehavior if any catalog entries for the temp tables have fields wide enough to require toasting (such as a very complex <code class=\"literal\">CHECK</code> condition).</p>"
  ],
  [
    "<p>Avoid improper response to shutdown signals in child processes just forked by <code class=\"function\">system()</code> (Nathan Bossart)</p>",
    "<p>This fix avoids a race condition in which a child process that has been forked off by <code class=\"function\">system()</code>, but hasn't yet exec'd the intended child program, might receive and act on a signal intended for the parent server process. That would lead to duplicate cleanup actions being performed, which will not end well.</p>"
  ],
  [
    "<p>Cope with torn reads of <code class=\"filename\">pg_control</code> in frontend programs (Thomas Munro)</p>",
    "<p>On some file systems, reading <code class=\"filename\">pg_control</code> may not be an atomic action when the server concurrently writes that file. This is detectable via a bad CRC. Retry a few times to see if the file becomes valid before we report error.</p>"
  ],
  [
    "<p>Avoid torn reads of <code class=\"filename\">pg_control</code> in relevant SQL functions (Thomas Munro)</p>",
    "<p>Acquire the appropriate lock before reading <code class=\"filename\">pg_control</code>, to ensure we get a consistent view of that file.</p>"
  ],
  [
    "<p>Avoid integer overflow when computing size of backend activity string array (Jakub Wartak)</p>",
    "<p>On 64-bit machines we will allow values of <code class=\"varname\">track_activity_query_size</code> large enough to cause 32-bit overflow when multiplied by the allowed number of connections. The code actually allocating the per-backend local array was careless about this though, and allocated the array incorrectly.</p>"
  ],
  [
    "<p>Fix briefly showing inconsistent progress statistics for <code class=\"command\">ANALYZE</code> on inherited tables (Heikki Linnakangas)</p>",
    "<p>The block-level counters should be reset to zero at the same time we update the current-relation field.</p>"
  ],
  [
    "<p>Track the dependencies of cached <code class=\"command\">CALL</code> statements, and re-plan them when needed (Tom Lane)</p>",
    "<p>DDL commands, such as replacement of a function that has been inlined into a <code class=\"command\">CALL</code> argument, can create the need to re-plan a <code class=\"command\">CALL</code> that has been cached by PL/pgSQL. That was not happening, leading to misbehavior or strange errors such as <span class=\"quote\">“<span class=\"quote\">cache lookup failed</span>”</span>.</p>"
  ],
  [
    "<p>Track nesting depth correctly when inspecting <code class=\"type\">RECORD</code>-type Vars from outer query levels (Richard Guo)</p>",
    "<p>This oversight could lead to assertion failures, core dumps, or <span class=\"quote\">“<span class=\"quote\">bogus varno</span>”</span> errors.</p>"
  ],
  [
    "<p>Avoid <span class=\"quote\">“<span class=\"quote\">record type has not been registered</span>”</span> failure when deparsing a view that contains references to fields of composite constants (Tom Lane)</p>"
  ],
  [
    "<p>Fix error-handling bug in <code class=\"type\">RECORD</code> type cache management (Thomas Munro)</p>",
    "<p>An out-of-memory error occurring at just the wrong point could leave behind inconsistent state that would lead to an infinite loop.</p>"
  ],
  [
    "<p>Fix assertion failure when logical decoding is retried in the same session after an error (Hou Zhijie)</p>"
  ],
  [
    "<p>Treat out-of-memory failures as fatal while reading WAL (Michael Paquier)</p>",
    "<p>Previously this would be treated as a bogus-data condition, leading to the conclusion that we'd reached the end of WAL, which is incorrect and could lead to inconsistent WAL replay.</p>"
  ],
  [
    "<p>Fix possible recovery failure due to trying to allocate memory based on a bogus WAL record length field (Thomas Munro, Michael Paquier)</p>"
  ],
  [
    "<p>Ensure that standby-mode WAL recovery reports an error when an invalid page header is found (Yugo Nagata, Kyotaro Horiguchi)</p>"
  ],
  [
    "<p>Fix datatype size confusion in logical tape management (Ranier Vilela)</p>",
    "<p>Integer overflow was possible on platforms where long is wider than int, although it would take a multiple-terabyte temporary file to cause a problem.</p>"
  ],
  [
    "<p>Avoid unintended close of syslogger process's stdin (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Avoid doing plan cache revalidation of utility statements that do not receive interesting processing during parse analysis (Tom Lane)</p>",
    "<p>Aside from saving a few cycles, this prevents failure after a cache invalidation for statements that must not set a snapshot, such as <code class=\"command\">SET TRANSACTION ISOLATION LEVEL</code>.</p>"
  ],
  [
    "<p>Keep by-reference <code class=\"structfield\">attmissingval</code> values in a long-lived context while they are being used (Andrew Dunstan)</p>",
    "<p>This avoids possible use of dangling pointers when a tuple slot outlives the tuple descriptor with which its value was constructed.</p>"
  ],
  [
    "<p>Recalculate the effective value of <code class=\"varname\">search_path</code> after <code class=\"command\">ALTER ROLE</code> (Jeff Davis)</p>",
    "<p>This ensures that after renaming a role, the meaning of the special string <code class=\"literal\">$user</code> is re-determined.</p>"
  ],
  [
    "<p>Fix order of operations in <code class=\"function\">GenericXLogFinish</code> (Jeff Davis)</p>",
    "<p>This code violated the conditions required for crash safety by writing WAL before marking changed buffers dirty. No core code uses this function, but extensions do (<code class=\"filename\">contrib/bloom</code> does, for example).</p>"
  ],
  [
    "<p>Remove incorrect assertion in PL/Python exception handling (Alexander Lakhin)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_restore</span> so that selective restores will include both table-level and column-level ACLs for selected tables (Euler Taveira, Tom Lane)</p>",
    "<p>Formerly, only the table-level ACL would get restored if both types were present.</p>"
  ],
  [
    "<p>Add logic to <span class=\"application\">pg_upgrade</span> to check for use of <code class=\"type\">abstime</code>, <code class=\"type\">reltime</code>, and <code class=\"type\">tinterval</code> data types (Álvaro Herrera)</p>",
    "<p>These obsolete data types were removed in <span class=\"productname\">PostgreSQL</span> version 12, so check to make sure they aren't present in an older database before claiming it can be upgraded.</p>"
  ],
  [
    "<p>Avoid generating invalid temporary slot names in <span class=\"application\">pg_basebackup</span> (Jelte Fennema)</p>",
    "<p>This has only been seen to occur when the server connection runs through <span class=\"application\">pgbouncer</span>.</p>"
  ],
  [
    "<p>Avoid false <span class=\"quote\">“<span class=\"quote\">too many client connections</span>”</span> errors in <span class=\"application\">pgbench</span> on Windows (Noah Misch)</p>"
  ],
  [
    "<p>In <code class=\"filename\">contrib/amcheck</code>, do not report interrupted page deletion as corruption (Noah Misch)</p>",
    "<p>This fix prevents false-positive reports of <span class=\"quote\">“<span class=\"quote\">the first child of leftmost target page is not leftmost of its level</span>”</span>, <span class=\"quote\">“<span class=\"quote\">block NNNN is not leftmost</span>”</span> or <span class=\"quote\">“<span class=\"quote\">left link/right link pair in index XXXX not in agreement</span>”</span>. They appeared if <span class=\"application\">amcheck</span> ran after an unfinished btree index page deletion and before <code class=\"command\">VACUUM</code> had cleaned things up.</p>"
  ],
  [
    "<p>Fix failure of <code class=\"filename\">contrib/btree_gin</code> indexes on <code class=\"type\">interval</code> columns, when an indexscan using the <code class=\"literal\">&lt;</code> or <code class=\"literal\">&lt;=</code> operator is performed (Dean Rasheed)</p>",
    "<p>Such an indexscan failed to return all the entries it should.</p>"
  ],
  [
    "<p>Add support for LLVM 16 and 17 (Thomas Munro, Dmitry Dolgov)</p>"
  ],
  [
    "<p>Suppress assorted build-time warnings on recent <span class=\"productname\">macOS</span> (Tom Lane)</p>",
    "<p><span class=\"productname\">Xcode 15</span> (released with <span class=\"productname\">macOS Sonoma</span>) changed the linker's behavior in a way that causes many duplicate-library warnings while building <span class=\"productname\">PostgreSQL</span>. These were harmless, but they're annoying so avoid citing the same libraries twice. Also remove use of the <code class=\"option\">-multiply_defined suppress</code> linker switch, which apparently has been a no-op for a long time, and is now actively complained of.</p>"
  ],
  [
    "<p>When building <code class=\"filename\">contrib/unaccent</code>'s rules file, fall back to using <code class=\"literal\">python</code> if <code class=\"literal\">--with-python</code> was not given and make variable <code class=\"literal\">PYTHON</code> was not set (Japin Li)</p>"
  ],
  [
    "<p>Remove <code class=\"literal\">PHOT</code> (Phoenix Islands Time) from the default timezone abbreviations list (Tom Lane)</p>",
    "<p>Presence of this abbreviation in the default list can cause failures on recent Debian and Ubuntu releases, as they no longer install the underlying tzdb entry by default. Since this is a made-up abbreviation for a zone with a total human population of about two dozen, it seems unlikely that anyone will miss it. If someone does, they can put it back via a custom abbreviations file.</p>"
  ]
]