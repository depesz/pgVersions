[
  [
    "<p>Ensure cached plans are marked as dependent on the calling role when RLS applies to a non-top-level table reference (Nathan Bossart) <a class=\"ulink\" href=\"https://postgr.es/c/4e51030af\" target=\"_top\">§</a></p>",
    "<p>If a CTE, subquery, sublink, security invoker view, or coercion projection in a query references a table with row-level security policies, we neglected to mark the resulting plan as potentially dependent on which role is executing it. This could lead to later query executions in the same session using the wrong plan, and then returning or hiding rows that should have been hidden or returned instead.</p>",
    "<p>The <span class=\"productname\">PostgreSQL</span> Project thanks Wolfgang Walther for reporting this problem. (CVE-2024-10976)</p>"
  ],
  [
    "<p>Make <span class=\"application\">libpq</span> discard error messages received during SSL or GSS protocol negotiation (Jacob Champion) <a class=\"ulink\" href=\"https://postgr.es/c/e6c945476\" target=\"_top\">§</a></p>",
    "<p>An error message received before encryption negotiation is completed might have been injected by a man-in-the-middle, rather than being real server output. Reporting it opens the door to various security hazards; for example, the message might spoof a query result that a careless user could mistake for correct output. The best answer seems to be to discard such data and rely only on <span class=\"application\">libpq</span>'s own report of the connection failure.</p>",
    "<p>The <span class=\"productname\">PostgreSQL</span> Project thanks Jacob Champion for reporting this problem. (CVE-2024-10977)</p>"
  ],
  [
    "<p>Fix unintended interactions between <code class=\"command\">SET SESSION AUTHORIZATION</code> and <code class=\"command\">SET ROLE</code> (Tom Lane) <a class=\"ulink\" href=\"https://postgr.es/c/2a68808e2\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/00b94e8e2\" target=\"_top\">§</a></p>",
    "<p>The SQL standard mandates that <code class=\"command\">SET SESSION AUTHORIZATION</code> have a side-effect of doing <code class=\"command\">SET ROLE NONE</code>. Our implementation of that was flawed, creating more interaction between the two settings than intended. Notably, rolling back a transaction that had done <code class=\"command\">SET SESSION AUTHORIZATION</code> would revert <code class=\"literal\">ROLE</code> to <code class=\"literal\">NONE</code> even if that had not been the previous state, so that the effective user ID might now be different from what it had been before the transaction. Transiently setting <code class=\"varname\">session_authorization</code> in a function <code class=\"literal\">SET</code> clause had a similar effect. A related bug was that if a parallel worker inspected <code class=\"literal\">current_setting('role')</code>, it saw <code class=\"literal\">none</code> even when it should see something else.</p>",
    "<p>The <span class=\"productname\">PostgreSQL</span> Project thanks Tom Lane for reporting this problem. (CVE-2024-10978)</p>"
  ],
  [
    "<p>Prevent trusted PL/Perl code from changing environment variables (Andrew Dunstan, Noah Misch) <a class=\"ulink\" href=\"https://postgr.es/c/d15ec27c9\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/f89bd92c9\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/c1fff7b1b\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/256e34653\" target=\"_top\">§</a></p>",
    "<p>The ability to manipulate process environment variables such as <code class=\"literal\">PATH</code> gives an attacker opportunities to execute arbitrary code. Therefore, <span class=\"quote\">“<span class=\"quote\">trusted</span>”</span> PLs must not offer the ability to do that. To fix <code class=\"literal\">plperl</code>, replace <code class=\"varname\">%ENV</code> with a tied hash that rejects any modification attempt with a warning. Untrusted <code class=\"literal\">plperlu</code> retains the ability to change the environment.</p>",
    "<p>The <span class=\"productname\">PostgreSQL</span> Project thanks Coby Abrams for reporting this problem. (CVE-2024-10979)</p>"
  ],
  [
    "<p>Fix updates of catalog state for foreign-key constraints when attaching or detaching table partitions (Jehan-Guillaume de Rorthais, Tender Wang, Álvaro Herrera) <a class=\"ulink\" href=\"https://postgr.es/c/46a8c27a7\" target=\"_top\">§</a></p>",
    "<p>If the referenced table is partitioned, then different catalog entries are needed for a referencing table that is stand-alone versus one that is a partition. <code class=\"literal\">ATTACH/DETACH PARTITION</code> commands failed to perform this conversion correctly. In particular, after <code class=\"literal\">DETACH</code> the now stand-alone table would be missing foreign-key enforcement triggers, which could result in the table later containing rows that fail the foreign-key constraint. A subsequent re-<code class=\"literal\">ATTACH</code> could fail with surprising errors, too.</p>",
    "<p>The way to fix this is to do <code class=\"command\">ALTER TABLE DROP CONSTRAINT</code> on the now stand-alone table for each faulty constraint, and then re-add the constraint. If re-adding the constraint fails, then some erroneous data has crept in. You will need to manually re-establish consistency between the referencing and referenced tables, then re-add the constraint.</p>",
    "<p>This query can be used to identify broken constraints and construct the commands needed to recreate them:</p>",
    "<pre class=\"programlisting\">\nSELECT conrelid::pg_catalog.regclass AS \"constrained table\",\n       conname AS constraint,\n       confrelid::pg_catalog.regclass AS \"references\",\n       pg_catalog.format('ALTER TABLE %s DROP CONSTRAINT %I;',\n                         conrelid::pg_catalog.regclass, conname) AS \"drop\",\n       pg_catalog.format('ALTER TABLE %s ADD CONSTRAINT %I %s;',\n                         conrelid::pg_catalog.regclass, conname,\n                         pg_catalog.pg_get_constraintdef(oid)) AS \"add\"\nFROM pg_catalog.pg_constraint c\nWHERE contype = 'f' AND conparentid = 0 AND\n   (SELECT count(*) FROM pg_catalog.pg_constraint c2\n    WHERE c2.conparentid = c.oid) &lt;&gt;\n   (SELECT count(*) FROM pg_catalog.pg_inherits i\n    WHERE (i.inhparent = c.conrelid OR i.inhparent = c.confrelid) AND\n      EXISTS (SELECT 1 FROM pg_catalog.pg_partitioned_table\n              WHERE partrelid = i.inhparent));\n</pre>",
    "<p>Since it is possible that one or more of the <code class=\"literal\">ADD CONSTRAINT</code> steps will fail, you should save the query's output in a file and then attempt to perform each step.</p>"
  ],
  [
    "<p>Avoid possible crashes and <span class=\"quote\">“<span class=\"quote\">could not open relation</span>”</span> errors in queries on a partitioned table occurring concurrently with a <code class=\"command\">DETACH CONCURRENTLY</code> and immediate drop of a partition (Álvaro Herrera, Kuntal Gosh) <a class=\"ulink\" href=\"https://postgr.es/c/3ad4c8615\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/1b4bdf915\" target=\"_top\">§</a></p>"
  ],
  [
    "<p>Disallow <code class=\"command\">ALTER TABLE ATTACH PARTITION</code> if the table to be attached has a foreign key referencing the partitioned table (Álvaro Herrera) <a class=\"ulink\" href=\"https://postgr.es/c/e97121d90\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/a54a5c426\" target=\"_top\">§</a></p>",
    "<p>This arrangement is not supported, and other ways of creating it already fail.</p>"
  ],
  [
    "<p>Don't use partitionwise joins or grouping if the query's collation for the key column doesn't match the partition key's collation (Jian He, Webbo Han) <a class=\"ulink\" href=\"https://postgr.es/c/62df5484f\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/96f9b29a3\" target=\"_top\">§</a></p>",
    "<p>Such plans could produce incorrect results.</p>"
  ],
  [
    "<p>Fix possible <span class=\"quote\">“<span class=\"quote\">could not find pathkey item to sort</span>”</span> error when the output of a <code class=\"literal\">UNION ALL</code> member query needs to be sorted, and the sort column is an expression (Andrei Lepikhov, Tom Lane) <a class=\"ulink\" href=\"https://postgr.es/c/4ca708eb3\" target=\"_top\">§</a></p>"
  ],
  [
    "<p>Allow cancellation of the second stage of index build for large hash indexes (Pavel Borisov) <a class=\"ulink\" href=\"https://postgr.es/c/b49013f2e\" target=\"_top\">§</a></p>"
  ],
  [
    "<p>Fix assertion failure or confusing error message for <code class=\"literal\">COPY (<em class=\"replaceable\"><code>query</code></em>) TO ...</code>, when the <em class=\"replaceable\"><code>query</code></em> is rewritten by a <code class=\"literal\">DO INSTEAD NOTIFY</code> rule (Tender Wang, Tom Lane) <a class=\"ulink\" href=\"https://postgr.es/c/5e94f616c\" target=\"_top\">§</a></p>"
  ],
  [
    "<p>Fix detection of skewed data during parallel hash join (Thomas Munro) <a class=\"ulink\" href=\"https://postgr.es/c/20d948994\" target=\"_top\">§</a></p>",
    "<p>After repartitioning the inner side of a hash join because one partition has accumulated too many tuples, we check to see if all the partition's tuples went into the same child partition, which suggests that they all have the same hash value and further repartitioning cannot improve matters. This check malfunctioned in some cases, allowing repeated futile repartitioning which would eventually end in a resource-exhaustion error.</p>"
  ],
  [
    "<p>Fix race condition in committing a serializable transaction (Heikki Linnakangas) <a class=\"ulink\" href=\"https://postgr.es/c/520ec2474\" target=\"_top\">§</a></p>",
    "<p>Mis-processing of a recently committed transaction could lead to an assertion failure or a <span class=\"quote\">“<span class=\"quote\">could not access status of transaction</span>”</span> error.</p>"
  ],
  [
    "<p>Fix race condition in <code class=\"command\">COMMIT PREPARED</code> that resulted in orphaned 2PC files (wuchengwen) <a class=\"ulink\" href=\"https://postgr.es/c/5f1510787\" target=\"_top\">§</a></p>",
    "<p>A concurrent <code class=\"command\">PREPARE TRANSACTION</code> could cause <code class=\"command\">COMMIT PREPARED</code> to not remove the on-disk two-phase state file for the completed transaction. There was no immediate ill effect, but a subsequent crash-and-recovery could fail with <span class=\"quote\">“<span class=\"quote\">could not access status of transaction</span>”</span>, requiring manual removal of the orphaned file to restore service.</p>"
  ],
  [
    "<p>Avoid invalid memory accesses after skipping an invalid toast index during <code class=\"command\">VACUUM FULL</code> (Tender Wang) <a class=\"ulink\" href=\"https://postgr.es/c/6530b869c\" target=\"_top\">§</a></p>",
    "<p>A list tracking yet-to-be-rebuilt indexes was not properly updated in this code path, risking assertion failures or crashes later on.</p>"
  ],
  [
    "<p>Fix ways in which an <span class=\"quote\">“<span class=\"quote\">in place</span>”</span> catalog update could be lost (Noah Misch) <a class=\"ulink\" href=\"https://postgr.es/c/82c2d9e02\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/f51b34b3e\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/b9ee1339b\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/ad24b7565\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/11e3f288f\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/bb3054297\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/803655e66\" target=\"_top\">§</a></p>",
    "<p>Normal row updates write a new version of the row to preserve rollback-ability of the transaction. However, certain system catalog updates are intentionally non-transactional and are done with an in-place update of the row. These patches fix race conditions that could cause the effects of an in-place update to be lost. As an example, it was possible to forget having set <code class=\"structname\">pg_class</code>.<code class=\"structfield\">relhasindex</code> to true, preventing updates of the new index and thus causing index corruption.</p>"
  ],
  [
    "<p>Reset catalog caches at end of recovery (Noah Misch) <a class=\"ulink\" href=\"https://postgr.es/c/dca68242a\" target=\"_top\">§</a></p>",
    "<p>This prevents scenarios wherein an in-place catalog update could be lost due to using stale data from a catalog cache.</p>"
  ],
  [
    "<p>Avoid using parallel query while holding off interrupts (Francesco Degrassi, Noah Misch, Tom Lane) <a class=\"ulink\" href=\"https://postgr.es/c/5c698e898\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/989ccd26c\" target=\"_top\">§</a></p>",
    "<p>This situation cannot arise normally, but it can be reached with test scenarios such as using a SQL-language function as B-tree support (which would be far too slow for production usage). If it did occur it would result in an indefinite wait.</p>"
  ],
  [
    "<p>Report the active query ID for statistics purposes at the start of processing of Bind and Execute protocol messages (Sami Imseih) <a class=\"ulink\" href=\"https://postgr.es/c/b36ee879c\" target=\"_top\">§</a></p>",
    "<p>This allows more of the work done in extended query protocol to be attributed to the correct query.</p>"
  ],
  [
    "<p>Guard against stack overflow in <span class=\"application\">libxml2</span> with too-deeply-nested XML input (Tom Lane, with hat tip to Nick Wellnhofer) <a class=\"ulink\" href=\"https://postgr.es/c/7721fff06\" target=\"_top\">§</a></p>",
    "<p>Use <code class=\"function\">xmlXPathCtxtCompile()</code> rather than <code class=\"function\">xmlXPathCompile()</code>, because the latter fails to protect itself against recursion-to-stack-overflow in <span class=\"application\">libxml2</span> releases before 2.13.4.</p>"
  ],
  [
    "<p>Do not ignore a concurrent <code class=\"command\">REINDEX CONCURRENTLY</code> that is working on an index with predicates or expressions (Michail Nikolaev) <a class=\"ulink\" href=\"https://postgr.es/c/902151548\" target=\"_top\">§</a></p>",
    "<p>Normally, <code class=\"command\">REINDEX CONCURRENTLY</code> does not need to wait for other <code class=\"command\">REINDEX CONCURRENTLY</code> operations on other tables. However, this optimization is not applied if the other <code class=\"command\">REINDEX CONCURRENTLY</code> is processing an index with predicates or expressions, on the chance that such expressions contain user-defined code that accesses other tables. Careless coding created a race condition such that that rule was not applied uniformly, possibly allowing inconsistent behavior.</p>"
  ],
  [
    "<p>Fix <span class=\"quote\">“<span class=\"quote\">failed to find plan for subquery/CTE</span>”</span> errors in <code class=\"command\">EXPLAIN</code> (Richard Guo, Tom Lane) <a class=\"ulink\" href=\"https://postgr.es/c/bc5446a21\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/120dd0337\" target=\"_top\">§</a></p>",
    "<p>This case arose while trying to print references to fields of a RECORD-type output of a subquery when the subquery has been optimized out of the plan altogether (which is possible at least in the case that it has a constant-false <code class=\"literal\">WHERE</code> condition). Nothing remains in the plan to identify the original field names, so fall back to printing <code class=\"literal\">f<em class=\"replaceable\"><code>N</code></em></code> for the <em class=\"replaceable\"><code>N</code></em>'th record column. (That's actually the right thing anyway, if the record output arose from a <code class=\"literal\">ROW()</code> constructor.)</p>"
  ],
  [
    "<p>Disallow a <code class=\"literal\">USING</code> clause when altering the type of a generated column (Peter Eisentraut) <a class=\"ulink\" href=\"https://postgr.es/c/ecd19a3cc\" target=\"_top\">§</a></p>",
    "<p>A generated column already has an expression specifying the column contents, so including <code class=\"literal\">USING</code> doesn't make sense.</p>"
  ],
  [
    "<p>Ignore not-yet-defined Portals in the <code class=\"structname\">pg_cursors</code> view (Tom Lane) <a class=\"ulink\" href=\"https://postgr.es/c/3922c9e9f\" target=\"_top\">§</a></p>",
    "<p>It is possible for user-defined code that inspects this view to be called while a new cursor is being set up, and if that happens a null pointer dereference would ensue. Avoid the problem by defining the view to exclude incompletely-set-up cursors.</p>"
  ],
  [
    "<p>Avoid <span class=\"quote\">“<span class=\"quote\">unexpected table_index_fetch_tuple call during logical decoding</span>”</span> error while decoding a transaction involving insertion of a column default value (Takeshi Ideriha, Hou Zhijie) <a class=\"ulink\" href=\"https://postgr.es/c/efe706e27\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/581092c90\" target=\"_top\">§</a></p>"
  ],
  [
    "<p>Reduce memory consumption of logical decoding (Masahiko Sawada) <a class=\"ulink\" href=\"https://postgr.es/c/5c1ed0a51\" target=\"_top\">§</a></p>",
    "<p>Use a smaller default block size to store tuple data received during logical replication. This reduces memory wastage, which has been reported to be severe while processing long-running transactions, even leading to out-of-memory failures.</p>"
  ],
  [
    "<p>Re-disable sending of stateless (TLSv1.2) session tickets (Daniel Gustafsson) <a class=\"ulink\" href=\"https://postgr.es/c/8cea8c023\" target=\"_top\">§</a></p>",
    "<p>A previous change to prevent sending of stateful (TLSv1.3) session tickets accidentally re-enabled sending of stateless ones. Thus, while we intended to prevent clients from thinking that TLS session resumption is supported, some still did.</p>"
  ],
  [
    "<p>Avoid <span class=\"quote\">“<span class=\"quote\">wrong tuple length</span>”</span> failure when dropping a database with many ACL (permission) entries (Ayush Tiwari) <a class=\"ulink\" href=\"https://postgr.es/c/b3bb1e24b\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/3acbe198e\" target=\"_top\">§</a></p>"
  ],
  [
    "<p>Allow adjusting the <code class=\"varname\">session_authorization</code> and <code class=\"varname\">role</code> settings in parallel workers (Tom Lane) <a class=\"ulink\" href=\"https://postgr.es/c/546a26b3d\" target=\"_top\">§</a></p>",
    "<p>Our code intends to allow modifiable server settings to be set by function <code class=\"literal\">SET</code> clauses, but not otherwise within a parallel worker. <code class=\"literal\">SET</code> clauses failed for these two settings, though.</p>"
  ],
  [
    "<p>Fix behavior of stable functions called from a <code class=\"command\">CALL</code> statement's argument list, when the <code class=\"command\">CALL</code> is within a PL/pgSQL <code class=\"literal\">EXCEPTION</code> block (Tom Lane) <a class=\"ulink\" href=\"https://postgr.es/c/ab13c46ff\" target=\"_top\">§</a></p>",
    "<p>As with a similar fix in our previous quarterly releases, this case allowed such functions to be passed the wrong snapshot, causing them to see stale values of rows modified since the start of the outer transaction.</p>"
  ],
  [
    "<p>Fix <span class=\"quote\">“<span class=\"quote\">cache lookup failed for function</span>”</span> errors in edge cases in PL/pgSQL's <code class=\"command\">CALL</code> (Tom Lane) <a class=\"ulink\" href=\"https://postgr.es/c/7f875fb5b\" target=\"_top\">§</a></p>"
  ],
  [
    "<p>Fix thread safety of our fallback (non-OpenSSL) MD5 implementation on big-endian hardware (Heikki Linnakangas) <a class=\"ulink\" href=\"https://postgr.es/c/7696b2ea5\" target=\"_top\">§</a></p>",
    "<p>Thread safety is not currently a concern in the server, but it is for libpq.</p>"
  ],
  [
    "<p>Parse <span class=\"application\">libpq</span>'s <code class=\"literal\">keepalives</code> connection option in the same way as other integer-valued options (Yuto Sasaki) <a class=\"ulink\" href=\"https://postgr.es/c/e7af9b52f\" target=\"_top\">§</a></p>",
    "<p>The coding used here rejected trailing whitespace in the option value, unlike other cases. This turns out to be problematic in <span class=\"application\">ecpg</span>'s usage, for example.</p>"
  ],
  [
    "<p>Avoid use of <code class=\"function\">pnstrdup()</code> in <span class=\"application\">ecpglib</span> (Jacob Champion) <a class=\"ulink\" href=\"https://postgr.es/c/355718553\" target=\"_top\">§</a></p>",
    "<p>That function will call <code class=\"function\">exit()</code> on out-of-memory, which is undesirable in a library. The calling code already handles allocation failures properly.</p>"
  ],
  [
    "<p>In <span class=\"application\">ecpglib</span>, fix out-of-bounds read when parsing incorrect datetime input (Bruce Momjian, Pavel Nekrasov) <a class=\"ulink\" href=\"https://postgr.es/c/9a51d4af1\" target=\"_top\">§</a></p>",
    "<p>It was possible to try to read the location just before the start of a constant array. Real-world consequences seem minimal, though.</p>"
  ],
  [
    "<p>Include the source timeline history in <span class=\"application\">pg_rewind</span>'s debug output (Heikki Linnakangas) <a class=\"ulink\" href=\"https://postgr.es/c/bb5592cac\" target=\"_top\">§</a></p>",
    "<p>This was the intention to begin with, but a coding error caused the source history to always print as empty.</p>"
  ],
  [
    "<p>Fix misbehavior with junction points on Windows, particularly in <span class=\"application\">pg_rewind</span> (Alexandra Wang) <a class=\"ulink\" href=\"https://postgr.es/c/02a4ec478\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/ca9921936\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/8a5e4982f\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/895f23d9e\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/ce14dbbca\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/1bf47d897\" target=\"_top\">§</a></p>",
    "<p>This entailed back-patching previous fixes by Thomas Munro, Peter Eisentraut, Alexander Lakhin, and Juan José Santamaría Flecha. Those changes were originally not back-patched out of caution, but they have been in use in later branches for long enough to deem them safe.</p>"
  ],
  [
    "<p>Avoid trying to reindex temporary tables and indexes in <span class=\"application\">vacuumdb</span> and in parallel <span class=\"application\">reindexdb</span> (VaibhaveS, Michael Paquier, Fujii Masao, Nathan Bossart) <a class=\"ulink\" href=\"https://postgr.es/c/60c618216\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/88e1153cb\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/ce6f27857\" target=\"_top\">§</a></p>",
    "<p>Reindexing other sessions' temporary tables cannot work, but the check to skip them was missing in some code paths, leading to unwanted failures.</p>"
  ],
  [
    "<p>Allow inspection of sequence relations in relevant functions of <code class=\"filename\">contrib/pageinspect</code> and <code class=\"filename\">contrib/pgstattuple</code> (Nathan Bossart, Ayush Vatsa) <a class=\"ulink\" href=\"https://postgr.es/c/8a94af8a2\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/0970889e3\" target=\"_top\">§</a></p>",
    "<p>This had been allowed in the past, but it got broken during the introduction of non-default access methods for tables.</p>"
  ],
  [
    "<p>Fix incorrect LLVM-generated code on ARM64 platforms (Thomas Munro, Anthonin Bonnefoy) <a class=\"ulink\" href=\"https://postgr.es/c/0b022ddf3\" target=\"_top\">§</a></p>",
    "<p>When using JIT compilation on ARM platforms, the generated code could not support relocation distances exceeding 32 bits, allowing unlucky placement of generated code to cause server crashes on large-memory systems.</p>"
  ],
  [
    "<p>Fix a few places that assumed that process start time (represented as a <code class=\"type\">time_t</code>) will fit into a <code class=\"type\">long</code> value (Max Johnson, Nathan Bossart) <a class=\"ulink\" href=\"https://postgr.es/c/5cea7168d\" target=\"_top\">§</a></p>",
    "<p>On platforms where <code class=\"type\">long</code> is 32 bits (notably Windows), this coding would fail after Y2038. Most of the failures appear only cosmetic, but notably <code class=\"literal\">pg_ctl start</code> would hang.</p>"
  ],
  [
    "<p>Prevent <span class=\"quote\">“<span class=\"quote\">nothing provides perl(PostgreSQL::Test::Utils)</span>”</span> failures while building RPM packages of <span class=\"productname\">PostgreSQL</span> (Noah Misch) <a class=\"ulink\" href=\"https://postgr.es/c/ecf7c4846\" target=\"_top\">§</a></p>"
  ],
  [
    "<p>Fix building with Strawberry Perl on Windows (Andrew Dunstan) <a class=\"ulink\" href=\"https://postgr.es/c/9f7749464\" target=\"_top\">§</a></p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2024b (Tom Lane) <a class=\"ulink\" href=\"https://postgr.es/c/dedced73e\" target=\"_top\">§</a> <a class=\"ulink\" href=\"https://postgr.es/c/b27622c90\" target=\"_top\">§</a></p>",
    "<p>This <span class=\"application\">tzdata</span> release changes the old System-V-compatibility zone names to duplicate the corresponding geographic zones; for example <code class=\"literal\">PST8PDT</code> is now an alias for <code class=\"literal\">America/Los_Angeles</code>. The main visible consequence is that for timestamps before the introduction of standardized time zones, the zone is considered to represent local mean solar time for the named location. For example, in <code class=\"literal\">PST8PDT</code>, <code class=\"type\">timestamptz</code> input such as <code class=\"literal\">1801-01-01 00:00</code> would previously have been rendered as <code class=\"literal\">1801-01-01 00:00:00-08</code>, but now it is rendered as <code class=\"literal\">1801-01-01 00:00:00-07:52:58</code>.</p>",
    "<p>Also, historical corrections for Mexico, Mongolia, and Portugal. Notably, <code class=\"literal\">Asia/Choibalsan</code> is now an alias for <code class=\"literal\">Asia/Ulaanbaatar</code> rather than being a separate zone, mainly because the differences between those zones were found to be based on untrustworthy data.</p>"
  ]
]