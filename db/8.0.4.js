[
  [
    "<p>Fix error that allowed <code class=\"command\">VACUUM</code> to remove <code class=\"literal\">ctid</code> chains too soon, and add more\n          checking in code that follows <code class=\"literal\">ctid</code> links</p>",
    "<p>This fixes a long-standing problem that could cause\n          crashes in very rare circumstances.</p>"
  ],
  [
    "<p>Fix <code class=\"type\">CHAR()</code> to properly pad\n          spaces to the specified length when using a multiple-byte\n          character set (Yoshiyuki Asaba)</p>",
    "<p>In prior releases, the padding of <code class=\"type\">CHAR()</code> was incorrect because it only padded\n          to the specified number of bytes without considering how\n          many characters were stored.</p>"
  ],
  [
    "<p>Force a checkpoint before committing <code class=\"command\">CREATE DATABASE</code></p>",
    "<p>This should fix recent reports of <span class=\"quote\">&#x201C;<span class=\"quote\">index is not a\n          btree</span>&#x201D;</span> failures when a crash occurs shortly\n          after <code class=\"command\">CREATE DATABASE</code>.</p>"
  ],
  [
    "<p>Fix the sense of the test for read-only transaction in\n          <code class=\"command\">COPY</code></p>",
    "<p>The code formerly prohibited <code class=\"command\">COPY TO</code>, where it should prohibit\n          <code class=\"command\">COPY FROM</code>.</p>"
  ],
  [
    "<p>Handle consecutive embedded newlines in <code class=\"command\">COPY</code> CSV-mode input</p>"
  ],
  [
    "<p>Fix <code class=\"function\">date_trunc(week)</code> for\n          dates near year end</p>"
  ],
  [
    "<p>Fix planning problem with outer-join ON clauses that\n          reference only the inner-side relation</p>"
  ],
  [
    "<p>Further fixes for <code class=\"literal\">x FULL JOIN y\n          ON true</code> corner cases</p>"
  ],
  [
    "<p>Fix overenthusiastic optimization of <code class=\"literal\">x IN (SELECT DISTINCT ...)</code> and related\n          cases</p>"
  ],
  [
    "<p>Fix mis-planning of queries with small <code class=\"literal\">LIMIT</code> values due to poorly thought out\n          <span class=\"quote\">&#x201C;<span class=\"quote\">fuzzy</span>&#x201D;</span> cost comparison</p>"
  ],
  [
    "<p>Make <code class=\"function\">array_in</code> and\n          <code class=\"function\">array_recv</code> more paranoid\n          about validating their OID parameter</p>"
  ],
  [
    "<p>Fix missing rows in queries like <code class=\"literal\">UPDATE a=... WHERE a...</code> with GiST index\n          on column <code class=\"literal\">a</code></p>"
  ],
  [
    "<p>Improve robustness of datetime parsing</p>"
  ],
  [
    "<p>Improve checking for partially-written WAL pages</p>"
  ],
  [
    "<p>Improve robustness of signal handling when SSL is\n          enabled</p>"
  ],
  [
    "<p>Improve MIPS and M68K spinlock code</p>"
  ],
  [
    "<p>Don't try to open more than <code class=\"literal\">max_files_per_process</code> files during\n          postmaster startup</p>"
  ],
  [
    "<p>Various memory leakage fixes</p>"
  ],
  [
    "<p>Various portability improvements</p>"
  ],
  [
    "<p>Update timezone data files</p>"
  ],
  [
    "<p>Improve handling of DLL load failures on Windows</p>"
  ],
  [
    "<p>Improve random-number generation on Windows</p>"
  ],
  [
    "<p>Make <code class=\"literal\">psql -f filename</code>\n          return a nonzero exit code when opening the file\n          fails</p>"
  ],
  [
    "<p>Change <span class=\"application\">pg_dump</span> to\n          handle inherited check constraints more reliably</p>"
  ],
  [
    "<p>Fix password prompting in <span class=\"application\">pg_restore</span> on Windows</p>"
  ],
  [
    "<p>Fix PL/pgSQL to handle <code class=\"literal\">var :=\n          var</code> correctly when the variable is of\n          pass-by-reference type</p>"
  ],
  [
    "<p>Fix PL/Perl <code class=\"literal\">%_SHARED</code> so\n          it's actually shared</p>"
  ],
  [
    "<p>Fix <code class=\"filename\">contrib/pg_autovacuum</code> to allow sleep\n          intervals over 2000 sec</p>"
  ],
  [
    "<p>Update <code class=\"filename\">contrib/tsearch2</code>\n          to use current Snowball code</p>"
  ]
]