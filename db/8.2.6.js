[
  [
    "<p>Prevent functions in indexes from executing with the\n          privileges of the user running <code class=\"command\">VACUUM</code>, <code class=\"command\">ANALYZE</code>, etc (Tom)</p>",
    "<p>Functions used in index expressions and partial-index\n          predicates are evaluated whenever a new table entry is\n          made. It has long been understood that this poses a risk\n          of trojan-horse code execution if one modifies a table\n          owned by an untrustworthy user. (Note that triggers,\n          defaults, check constraints, etc. pose the same type of\n          risk.) But functions in indexes pose extra danger because\n          they will be executed by routine maintenance operations\n          such as <code class=\"command\">VACUUM FULL</code>, which\n          are commonly performed automatically under a superuser\n          account. For example, a nefarious user can execute code\n          with superuser privileges by setting up a trojan-horse\n          index definition and waiting for the next routine vacuum.\n          The fix arranges for standard maintenance operations\n          (including <code class=\"command\">VACUUM</code>,\n          <code class=\"command\">ANALYZE</code>, <code class=\"command\">REINDEX</code>, and <code class=\"command\">CLUSTER</code>) to execute as the table owner\n          rather than the calling user, using the same\n          privilege-switching mechanism already used for\n          <code class=\"literal\">SECURITY DEFINER</code> functions.\n          To prevent bypassing this security measure, execution of\n          <code class=\"command\">SET SESSION AUTHORIZATION</code>\n          and <code class=\"command\">SET ROLE</code> is now\n          forbidden within a <code class=\"literal\">SECURITY\n          DEFINER</code> context. (CVE-2007-6600)</p>"
  ],
  [
    "<p>Repair assorted bugs in the regular-expression package\n          (Tom, Will Drewry)</p>",
    "<p>Suitably crafted regular-expression patterns could\n          cause crashes, infinite or near-infinite looping, and/or\n          massive memory consumption, all of which pose\n          denial-of-service hazards for applications that accept\n          regex search patterns from untrustworthy sources.\n          (CVE-2007-4769, CVE-2007-4772, CVE-2007-6067)</p>"
  ],
  [
    "<p>Require non-superusers who use <code class=\"filename\">/contrib/dblink</code> to use only password\n          authentication, as a security measure (Joe)</p>",
    "<p>The fix that appeared for this in 8.2.5 was\n          incomplete, as it plugged the hole for only some\n          <code class=\"filename\">dblink</code> functions.\n          (CVE-2007-6601, CVE-2007-3278)</p>"
  ],
  [
    "<p>Fix bugs in WAL replay for GIN indexes (Teodor)</p>"
  ],
  [
    "<p>Fix GIN index build to work properly when <code class=\"varname\">maintenance_work_mem</code> is 4GB or more\n          (Tom)</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2007k (in particular,\n          recent Argentina changes) (Tom)</p>"
  ],
  [
    "<p>Improve planner's handling of LIKE/regex estimation in\n          non-C locales (Tom)</p>"
  ],
  [
    "<p>Fix planning-speed problem for deep outer-join nests,\n          as well as possible poor choice of join order (Tom)</p>"
  ],
  [
    "<p>Fix planner failure in some cases of <code class=\"literal\">WHERE false AND var IN (SELECT ...)</code>\n          (Tom)</p>"
  ],
  [
    "<p>Make <code class=\"command\">CREATE TABLE ...\n          SERIAL</code> and <code class=\"command\">ALTER SEQUENCE\n          ... OWNED BY</code> not change the <code class=\"function\">currval()</code> state of the sequence\n          (Tom)</p>"
  ],
  [
    "<p>Preserve the tablespace and storage parameters of\n          indexes that are rebuilt by <code class=\"command\">ALTER\n          TABLE ... ALTER COLUMN TYPE</code> (Tom)</p>"
  ],
  [
    "<p>Make archive recovery always start a new WAL timeline,\n          rather than only when a recovery stop time was used\n          (Simon)</p>",
    "<p>This avoids a corner-case risk of trying to overwrite\n          an existing archived copy of the last WAL segment, and\n          seems simpler and cleaner than the original\n          definition.</p>"
  ],
  [
    "<p>Make <code class=\"command\">VACUUM</code> not use all\n          of <code class=\"varname\">maintenance_work_mem</code> when\n          the table is too small for it to be useful (Alvaro)</p>"
  ],
  [
    "<p>Fix potential crash in <code class=\"function\">translate()</code> when using a multibyte\n          database encoding (Tom)</p>"
  ],
  [
    "<p>Make <code class=\"function\">corr()</code> return the\n          correct result for negative correlation values (Neil)</p>"
  ],
  [
    "<p>Fix overflow in <code class=\"literal\">extract(epoch\n          from interval)</code> for intervals exceeding 68 years\n          (Tom)</p>"
  ],
  [
    "<p>Fix PL/Perl to not fail when a UTF-8 regular\n          expression is used in a trusted function (Andrew)</p>"
  ],
  [
    "<p>Fix PL/Perl to cope when platform's Perl defines type\n          <code class=\"literal\">bool</code> as <code class=\"literal\">int</code> rather than <code class=\"literal\">char</code> (Tom)</p>",
    "<p>While this could theoretically happen anywhere, no\n          standard build of Perl did things this way ... until\n          <span class=\"productname\">macOS</span> 10.5.</p>"
  ],
  [
    "<p>Fix PL/Python to work correctly with Python 2.5 on\n          64-bit machines (Marko Kreen)</p>"
  ],
  [
    "<p>Fix PL/Python to not crash on long exception messages\n          (Alvaro)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_dump</span> to\n          correctly handle inheritance child tables that have\n          default expressions different from their parent's\n          (Tom)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">libpq</span> crash when\n          <code class=\"varname\">PGPASSFILE</code> refers to a file\n          that is not a plain file (Martin Pitt)</p>"
  ],
  [
    "<p><span class=\"application\">ecpg</span> parser fixes\n          (Michael)</p>"
  ],
  [
    "<p>Make <code class=\"filename\">contrib/pgcrypto</code>\n          defend against <span class=\"application\">OpenSSL</span>\n          libraries that fail on keys longer than 128 bits; which\n          is the case at least on some Solaris versions (Marko\n          Kreen)</p>"
  ],
  [
    "<p>Make <code class=\"filename\">contrib/tablefunc</code>'s\n          <code class=\"function\">crosstab()</code> handle NULL\n          rowid as a category in its own right, rather than\n          crashing (Joe)</p>"
  ],
  [
    "<p>Fix <code class=\"type\">tsvector</code> and\n          <code class=\"type\">tsquery</code> output routines to\n          escape backslashes correctly (Teodor, Bruce)</p>"
  ],
  [
    "<p>Fix crash of <code class=\"function\">to_tsvector()</code> on huge input strings\n          (Teodor)</p>"
  ],
  [
    "<p>Require a specific version of <span class=\"productname\">Autoconf</span> to be used when\n          re-generating the <code class=\"command\">configure</code>\n          script (Peter)</p>",
    "<p>This affects developers and packagers only. The change\n          was made to prevent accidental use of untested\n          combinations of <span class=\"productname\">Autoconf</span>\n          and <span class=\"productname\">PostgreSQL</span> versions.\n          You can remove the version check if you really want to\n          use a different <span class=\"productname\">Autoconf</span>\n          version, but it's your responsibility whether the result\n          works or not.</p>"
  ],
  [
    "<p>Update <code class=\"function\">gettimeofday</code>\n          configuration check so that <span class=\"productname\">PostgreSQL</span> can be built on newer\n          versions of <span class=\"productname\">MinGW</span>\n          (Magnus)</p>"
  ]
]