[
  [
    "<p>Fix failure to reset <span class=\"application\">libpq</span>'s\nstate fully between connection attempts (Tom Lane)</p>",
    "<p>An unprivileged user of <code class=\"filename\">dblink</code> or\n<code class=\"filename\">postgres_fdw</code> could bypass the checks\nintended to prevent use of server-side credentials, such as a\n<code class=\"filename\">~/.pgpass</code> file owned by the\noperating-system user running the server. Servers allowing peer\nauthentication on local connections are particularly vulnerable.\nOther attacks such as SQL injection into a <code class=\"filename\">postgres_fdw</code> session are also possible. Attacking\n<code class=\"filename\">postgres_fdw</code> in this way requires the\nability to create a foreign server object with selected connection\nparameters, but any user with access to <code class=\"filename\">dblink</code> could exploit the problem. In general, an\nattacker with the ability to select the connection parameters for a\n<span class=\"application\">libpq</span>-using application could\ncause mischief, though other plausible attack scenarios are harder\nto think of. Our thanks to Andrew Krasichkov for reporting this\nissue. (CVE-2018-10915)</p>"
  ],
  [
    "<p>Ensure that updates to the <code class=\"structfield\">relfrozenxid</code> and <code class=\"structfield\">relminmxid</code> values for <span class=\"quote\">&#x201C;<span class=\"quote\">nailed</span>&#x201D;</span> system catalogs\nare processed in a timely fashion (Andres Freund)</p>",
    "<p>Overoptimistic caching rules could prevent these updates from\nbeing seen by other sessions, leading to spurious errors and/or\ndata corruption. The problem was significantly worse for shared\ncatalogs, such as <code class=\"structname\">pg_authid</code>,\nbecause the stale cache data could persist into new sessions as\nwell as existing ones.</p>"
  ],
  [
    "<p>Fix case where a freshly-promoted standby crashes before having\ncompleted its first post-recovery checkpoint (Michael Paquier,\nKyotaro Horiguchi, Pavan Deolasee, &#xC1;lvaro Herrera)</p>",
    "<p>This led to a situation where the server did not think it had\nreached a consistent database state during subsequent WAL replay,\npreventing restart.</p>"
  ],
  [
    "<p>Avoid emitting a bogus WAL record when recycling an all-zero\nbtree page (Amit Kapila)</p>",
    "<p>This mistake has been seen to cause assertion failures, and\npotentially it could result in unnecessary query cancellations on\nhot standby servers.</p>"
  ],
  [
    "<p>Improve performance of WAL replay for transactions that drop\nmany relations (Fujii Masao)</p>",
    "<p>This change reduces the number of times that shared buffers are\nscanned, so that it is of most benefit when that setting is\nlarge.</p>"
  ],
  [
    "<p>Improve performance of lock releasing in standby server WAL\nreplay (Thomas Munro)</p>"
  ],
  [
    "<p>Ensure a table's cached index list is correctly rebuilt after an\nindex creation fails partway through (Peter Geoghegan)</p>",
    "<p>Previously, the failed index's OID could remain in the list,\ncausing problems later in the same session.</p>"
  ],
  [
    "<p>Fix misoptimization of equivalence classes involving\ncomposite-type columns (Tom Lane)</p>",
    "<p>This resulted in failure to recognize that an index on a\ncomposite column could provide the sort order needed for a\nmergejoin on that column.</p>"
  ],
  [
    "<p>Fix SQL-standard <code class=\"literal\">FETCH FIRST</code> syntax\nto allow parameters (<code class=\"literal\">$<em class=\"replaceable\"><code>n</code></em></code>), as the standard expects\n(Andrew Gierth)</p>"
  ],
  [
    "<p>Fix failure to schema-qualify some object names in <code class=\"function\">getObjectDescription</code> output (Kyotaro Horiguchi,\nTom Lane)</p>",
    "<p>Names of collations, conversions, and text search objects were\nnot schema-qualified when they should be.</p>"
  ],
  [
    "<p>Widen <code class=\"command\">COPY FROM</code>'s\ncurrent-line-number counter from 32 to 64 bits (David Rowley)</p>",
    "<p>This avoids two problems with input exceeding 4G lines:\n<code class=\"literal\">COPY FROM WITH HEADER</code> would drop a\nline every 4G lines, not only the first line, and error reports\ncould show a wrong line number.</p>"
  ],
  [
    "<p>Add a string freeing function to <span class=\"application\">ecpg</span>'s <code class=\"filename\">pgtypes</code>\nlibrary, so that cross-module memory management problems can be\navoided on Windows (Takayuki Tsunakawa)</p>",
    "<p>On Windows, crashes can ensue if the <code class=\"function\">free</code> call for a given chunk of memory is not made\nfrom the same DLL that <code class=\"function\">malloc</code>'ed the\nmemory. The <code class=\"filename\">pgtypes</code> library sometimes\nreturns strings that it expects the caller to free, making it\nimpossible to follow this rule. Add a <code class=\"function\">PGTYPESchar_free()</code> function that just wraps\n<code class=\"function\">free</code>, allowing applications to follow\nthis rule.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">ecpg</span>'s support for\n<code class=\"type\">long long</code> variables on Windows, as well\nas other platforms that declare <code class=\"function\">strtoll</code>/<code class=\"function\">strtoull</code>\nnonstandardly or not at all (Dang Minh Huong, Tom Lane)</p>"
  ],
  [
    "<p>Fix misidentification of SQL statement type in PL/pgSQL, when a\nrule change causes a change in the semantics of a statement\nintra-session (Tom Lane)</p>",
    "<p>This error led to assertion failures, or in rare cases, failure\nto enforce the <code class=\"literal\">INTO STRICT</code> option as\nexpected.</p>"
  ],
  [
    "<p>Fix password prompting in client programs so that echo is\nproperly disabled on Windows when <code class=\"literal\">stdin</code> is not the terminal (Matthew Stickney)</p>"
  ],
  [
    "<p>Further fix mis-quoting of values for list-valued GUC variables\nin dumps (Tom Lane)</p>",
    "<p>The previous fix for quoting of <code class=\"varname\">search_path</code> and other list-valued variables in\n<span class=\"application\">pg_dump</span> output turned out to\nmisbehave for empty-string list elements, and it risked truncation\nof long file paths.</p>"
  ],
  [
    "<p>Make <span class=\"application\">pg_upgrade</span> check that the\nold server was shut down cleanly (Bruce Momjian)</p>",
    "<p>The previous check could be fooled by an immediate-mode\nshutdown.</p>"
  ],
  [
    "<p>Fix crash in <code class=\"filename\">contrib/ltree</code>'s\n<code class=\"function\">lca()</code> function when the input array\nis empty (Pierre Ducroquet)</p>"
  ],
  [
    "<p>Fix various error-handling code paths in which an incorrect\nerror code might be reported (Michael Paquier, Tom Lane, Magnus\nHagander)</p>"
  ],
  [
    "<p>Rearrange makefiles to ensure that programs link to\nfreshly-built libraries (such as <code class=\"filename\">libpq.so</code>) rather than ones that might exist in\nthe system library directories (Tom Lane)</p>",
    "<p>This avoids problems when building on platforms that supply old\ncopies of <span class=\"productname\">PostgreSQL</span>\nlibraries.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2018e for DST law changes in\nNorth Korea, plus historical corrections for Czechoslovakia.</p>",
    "<p>This update includes a redefinition of <span class=\"quote\">&#x201C;<span class=\"quote\">daylight savings</span>&#x201D;</span> in\nIreland, as well as for some past years in Namibia and\nCzechoslovakia. In those jurisdictions, legally standard time is\nobserved in summer, and daylight savings time in winter, so that\nthe daylight savings offset is one hour behind standard time not\none hour ahead. This does not affect either the actual UTC offset\nor the timezone abbreviations in use; the only known effect is that\nthe <code class=\"structfield\">is_dst</code> column in the\n<code class=\"structname\">pg_timezone_names</code> view will now be\ntrue in winter and false in summer in these cases.</p>"
  ]
]