[
  [
    "<p>Show foreign tables in <code class=\"structname\">information_schema</code>.<code class=\"structname\">table_privileges</code> view (Peter\n          Eisentraut)</p>",
    "<p>All other relevant <code class=\"structname\">information_schema</code> views include\n          foreign tables, but this one ignored them.</p>",
    "<p>Since this view definition is installed by\n          <span class=\"application\">initdb</span>, merely upgrading\n          will not fix the problem. If you need to fix this in an\n          existing installation, you can, as a superuser, do this\n          in <span class=\"application\">psql</span>:</p>",
    "<pre class=\"programlisting\">\n          SET search_path TO information_schema;\nCREATE OR REPLACE VIEW table_privileges AS\n    SELECT CAST(u_grantor.rolname AS sql_identifier) AS grantor,\n           CAST(grantee.rolname AS sql_identifier) AS grantee,\n           CAST(current_database() AS sql_identifier) AS table_catalog,\n           CAST(nc.nspname AS sql_identifier) AS table_schema,\n           CAST(c.relname AS sql_identifier) AS table_name,\n           CAST(c.prtype AS character_data) AS privilege_type,\n           CAST(\n             CASE WHEN\n                  -- object owner always has grant options\n                  pg_has_role(grantee.oid, c.relowner, 'USAGE')\n                  OR c.grantable\n                  THEN 'YES' ELSE 'NO' END AS yes_or_no) AS is_grantable,\n           CAST(CASE WHEN c.prtype = 'SELECT' THEN 'YES' ELSE 'NO' END AS yes_or_no) AS with_hierarchy\n\n    FROM (\n            SELECT oid, relname, relnamespace, relkind, relowner, (aclexplode(coalesce(relacl, acldefault('r', relowner)))).* FROM pg_class\n         ) AS c (oid, relname, relnamespace, relkind, relowner, grantor, grantee, prtype, grantable),\n         pg_namespace nc,\n         pg_authid u_grantor,\n         (\n           SELECT oid, rolname FROM pg_authid\n           UNION ALL\n           SELECT 0::oid, 'PUBLIC'\n         ) AS grantee (oid, rolname)\n\n    WHERE c.relnamespace = nc.oid\n          AND c.relkind IN ('r', 'v', 'f')\n          AND c.grantee = grantee.oid\n          AND c.grantor = u_grantor.oid\n          AND c.prtype IN ('INSERT', 'SELECT', 'UPDATE', 'DELETE', 'TRUNCATE', 'REFERENCES', 'TRIGGER')\n          AND (pg_has_role(u_grantor.oid, 'USAGE')\n               OR pg_has_role(grantee.oid, 'USAGE')\n               OR grantee.rolname = 'PUBLIC');</pre>",
    "<p>This must be repeated in each database to be fixed,\n          including <code class=\"literal\">template0</code>.</p>"
  ],
  [
    "<p>Clean up handling of a fatal exit (e.g., due to\n          receipt of <span class=\"systemitem\">SIGTERM</span>) that\n          occurs while trying to execute a <code class=\"command\">ROLLBACK</code> of a failed transaction (Tom\n          Lane)</p>",
    "<p>This situation could result in an assertion failure.\n          In production builds, the exit would still occur, but it\n          would log an unexpected message about <span class=\"quote\">&#x201C;<span class=\"quote\">cannot drop active\n          portal</span>&#x201D;</span>.</p>"
  ],
  [
    "<p>Remove assertion that could trigger during a fatal\n          exit (Tom Lane)</p>"
  ],
  [
    "<p>Correctly identify columns that are of a range type or\n          domain type over a composite type or domain type being\n          searched for (Tom Lane)</p>",
    "<p>Certain <code class=\"command\">ALTER</code> commands\n          that change the definition of a composite type or domain\n          type are supposed to fail if there are any stored values\n          of that type in the database, because they lack the\n          infrastructure needed to update or check such values.\n          Previously, these checks could miss relevant values that\n          are wrapped inside range types or sub-domains, possibly\n          allowing the database to become inconsistent.</p>"
  ],
  [
    "<p>Change <span class=\"application\">ecpg</span>'s parser\n          to allow <code class=\"literal\">RETURNING</code> clauses\n          without attached C variables (Michael Meskes)</p>",
    "<p>This allows <span class=\"application\">ecpg</span>\n          programs to contain SQL constructs that use <code class=\"literal\">RETURNING</code> internally (for example,\n          inside a CTE) rather than using it to define values to be\n          returned to the client.</p>"
  ],
  [
    "<p>Improve selection of compiler flags for PL/Perl on\n          Windows (Tom Lane)</p>",
    "<p>This fix avoids possible crashes of PL/Perl due to\n          inconsistent assumptions about the width of <code class=\"type\">time_t</code> values. A side-effect that may be\n          visible to extension developers is that <code class=\"literal\">_USE_32BIT_TIME_T</code> is no longer defined\n          globally in <span class=\"productname\">PostgreSQL</span>\n          Windows builds. This is not expected to cause problems,\n          because type <code class=\"type\">time_t</code> is not used\n          in any <span class=\"productname\">PostgreSQL</span> API\n          definitions.</p>"
  ]
]