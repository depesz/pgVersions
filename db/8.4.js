[
  [
    "<p>Improve optimizer statistics calculations (Jan\n            Urbanski, Tom)</p>",
    "<p>In particular, estimates for full-text-search\n            operators are greatly improved.</p>"
  ],
  [
    "<p>Allow <code class=\"command\">SELECT DISTINCT</code>\n            and <code class=\"literal\">UNION</code>/<code class=\"literal\">INTERSECT</code>/<code class=\"literal\">EXCEPT</code> to use hashing (Tom)</p>",
    "<p>This means that these types of queries no longer\n            automatically produce sorted output.</p>"
  ],
  [
    "<p>Create explicit concepts of semi-joins and\n            anti-joins (Tom)</p>",
    "<p>This work formalizes our previous ad-hoc treatment\n            of <code class=\"literal\">IN (SELECT ...)</code>\n            clauses, and extends it to <code class=\"literal\">EXISTS</code> and <code class=\"literal\">NOT\n            EXISTS</code> clauses. It should result in\n            significantly better planning of <code class=\"literal\">EXISTS</code> and <code class=\"literal\">NOT\n            EXISTS</code> queries. In general, logically equivalent\n            <code class=\"literal\">IN</code> and <code class=\"literal\">EXISTS</code> clauses should now have similar\n            performance, whereas previously <code class=\"literal\">IN</code> often won.</p>"
  ],
  [
    "<p>Improve optimization of sub-selects beneath outer\n            joins (Tom)</p>",
    "<p>Formerly, a sub-select or view could not be\n            optimized very well if it appeared within the nullable\n            side of an outer join and contained non-strict\n            expressions (for instance, constants) in its result\n            list.</p>"
  ],
  [
    "<p>Improve the performance of <code class=\"function\">text_position()</code> and related functions\n            by using Boyer-Moore-Horspool searching (David\n            Rowley)</p>",
    "<p>This is particularly helpful for long search\n            patterns.</p>"
  ],
  [
    "<p>Reduce I/O load of writing the statistics collection\n            file by writing the file only when requested (Martin\n            Pihlak)</p>"
  ],
  [
    "<p>Improve performance for bulk inserts (Robert Haas,\n            Simon)</p>"
  ],
  [
    "<p>Increase the default value of <code class=\"varname\">default_statistics_target</code> from\n            <code class=\"literal\">10</code> to <code class=\"literal\">100</code> (Greg Sabino Mullane, Tom)</p>",
    "<p>The maximum value was also increased from\n            <code class=\"literal\">1000</code> to <code class=\"literal\">10000</code>.</p>"
  ],
  [
    "<p>Perform <code class=\"varname\">constraint_exclusion</code> checking by\n            default in queries involving inheritance or\n            <code class=\"literal\">UNION ALL</code> (Tom)</p>",
    "<p>A new <code class=\"varname\">constraint_exclusion</code> setting,\n            <code class=\"literal\">partition</code>, was added to\n            specify this behavior.</p>"
  ],
  [
    "<p>Allow I/O read-ahead for bitmap index scans (Greg\n            Stark)</p>",
    "<p>The amount of read-ahead is controlled by\n            <code class=\"varname\">effective_io_concurrency</code>.\n            This feature is available only if the kernel has\n            <code class=\"function\">posix_fadvise()</code>\n            support.</p>"
  ],
  [
    "<p>Inline simple set-returning <acronym class=\"acronym\">SQL</acronym> functions in <code class=\"literal\">FROM</code> clauses (Richard Rowell)</p>"
  ],
  [
    "<p>Improve performance of multi-batch hash joins by\n            providing a special case for join key values that are\n            especially common in the outer relation (Bryce Cutt,\n            Ramon Lawrence)</p>"
  ],
  [
    "<p>Reduce volume of temporary data in multi-batch hash\n            joins by suppressing <span class=\"quote\">&#x201C;<span class=\"quote\">physical tlist</span>&#x201D;</span> optimization\n            (Michael Henderson, Ramon Lawrence)</p>"
  ],
  [
    "<p>Avoid waiting for idle-in-transaction sessions\n            during <code class=\"command\">CREATE INDEX\n            CONCURRENTLY</code> (Simon)</p>"
  ],
  [
    "<p>Improve performance of shared cache invalidation\n            (Tom)</p>"
  ],
  [
    "<p>Convert many <code class=\"filename\">postgresql.conf</code> settings to\n              enumerated values so that <code class=\"literal\">pg_settings</code> can display the valid\n              values (Magnus)</p>"
  ],
  [
    "<p>Add <code class=\"varname\">cursor_tuple_fraction</code> parameter to\n              control the fraction of a cursor's rows that the\n              planner assumes will be fetched (Robert Hell)</p>"
  ],
  [
    "<p>Allow underscores in the names of custom variable\n              classes in <code class=\"filename\">postgresql.conf</code> (Tom)</p>"
  ],
  [
    "<p>Remove support for the (insecure) <code class=\"literal\">crypt</code> authentication method\n              (Magnus)</p>",
    "<p>This effectively obsoletes pre-<span class=\"productname\">PostgreSQL</span> 7.2 client libraries,\n              as there is no longer any non-plaintext password\n              method that they can use.</p>"
  ],
  [
    "<p>Support regular expressions in <code class=\"filename\">pg_ident.conf</code> (Magnus)</p>"
  ],
  [
    "<p>Allow <span class=\"productname\">Kerberos</span>/<acronym class=\"acronym\">GSSAPI</acronym> parameters to be changed\n              without restarting the postmaster (Magnus)</p>"
  ],
  [
    "<p>Support <acronym class=\"acronym\">SSL</acronym>\n              certificate chains in server certificate file (Andrew\n              Gierth)</p>",
    "<p>Including the full certificate chain makes the\n              client able to verify the certificate without having\n              all intermediate CA certificates present in the local\n              store, which is often the case for commercial\n              CAs.</p>"
  ],
  [
    "<p>Report appropriate error message for combination\n              of <code class=\"literal\">MD5</code> authentication\n              and <code class=\"varname\">db_user_namespace</code>\n              enabled (Bruce)</p>"
  ],
  [
    "<p>Change all authentication options to use\n              <code class=\"literal\">name=value</code> syntax\n              (Magnus)</p>",
    "<p>This makes incompatible changes to the\n              <code class=\"literal\">ldap</code>, <code class=\"literal\">pam</code> and <code class=\"literal\">ident</code> authentication methods. All\n              <code class=\"filename\">pg_hba.conf</code> entries\n              with these methods need to be rewritten using the new\n              format.</p>"
  ],
  [
    "<p>Remove the <code class=\"literal\">ident\n              sameuser</code> option, instead making that behavior\n              the default if no usermap is specified (Magnus)</p>"
  ],
  [
    "<p>Allow a usermap parameter for all external\n              authentication methods (Magnus)</p>",
    "<p>Previously a usermap was only supported for\n              <code class=\"literal\">ident</code>\n              authentication.</p>"
  ],
  [
    "<p>Add <code class=\"literal\">clientcert</code> option\n              to control requesting of a client certificate\n              (Magnus)</p>",
    "<p>Previously this was controlled by the presence of\n              a root certificate file in the server's data\n              directory.</p>"
  ],
  [
    "<p>Add <code class=\"literal\">cert</code>\n              authentication method to allow <span class=\"emphasis\"><em>user</em></span> authentication via\n              <acronym class=\"acronym\">SSL</acronym> certificates\n              (Magnus)</p>",
    "<p>Previously <acronym class=\"acronym\">SSL</acronym>\n              certificates could only verify that the client had\n              access to a certificate, not authenticate a user.</p>"
  ],
  [
    "<p>Allow <code class=\"literal\">krb5</code>,\n              <code class=\"literal\">gssapi</code> and <code class=\"literal\">sspi</code> realm and <code class=\"literal\">krb5</code> host settings to be specified\n              in <code class=\"filename\">pg_hba.conf</code>\n              (Magnus)</p>",
    "<p>These override the settings in <code class=\"filename\">postgresql.conf</code>.</p>"
  ],
  [
    "<p>Add <code class=\"varname\">include_realm</code>\n              parameter for <code class=\"literal\">krb5</code>,\n              <code class=\"literal\">gssapi</code>, and <code class=\"literal\">sspi</code> methods (Magnus)</p>",
    "<p>This allows identical usernames from different\n              realms to be authenticated as different database\n              users using usermaps.</p>"
  ],
  [
    "<p>Parse <code class=\"filename\">pg_hba.conf</code>\n              fully when it is loaded, so that errors are reported\n              immediately (Magnus)</p>",
    "<p>Previously, most errors in the file wouldn't be\n              detected until clients tried to connect, so an\n              erroneous file could render the system unusable. With\n              the new behavior, if an error is detected during\n              reload then the bad file is rejected and the\n              postmaster continues to use its old copy.</p>"
  ],
  [
    "<p>Show all parsing errors in <code class=\"filename\">pg_hba.conf</code> instead of aborting\n              after the first one (Selena Deckelmann)</p>"
  ],
  [
    "<p>Support <code class=\"literal\">ident</code>\n              authentication over Unix-domain sockets on\n              <span class=\"productname\">Solaris</span> (Garick\n              Hamlin)</p>"
  ],
  [
    "<p>Provide an option to <code class=\"function\">pg_start_backup()</code> to force its\n              implied checkpoint to finish as quickly as possible\n              (Tom)</p>",
    "<p>The default behavior avoids excess I/O\n              consumption, but that is pointless if no concurrent\n              query activity is going on.</p>"
  ],
  [
    "<p>Make <code class=\"function\">pg_stop_backup()</code> wait for modified\n              <acronym class=\"acronym\">WAL</acronym> files to be\n              archived (Simon)</p>",
    "<p>This guarantees that the backup is valid at the\n              time <code class=\"function\">pg_stop_backup()</code>\n              completes.</p>"
  ],
  [
    "<p>When archiving is enabled, rotate the last WAL\n              segment at shutdown so that all transactions can be\n              archived immediately (Guillaume Smet, Heikki)</p>"
  ],
  [
    "<p>Delay <span class=\"quote\">&#x201C;<span class=\"quote\">smart</span>&#x201D;</span> shutdown while a\n              continuous archiving base backup is in progress\n              (Laurenz Albe)</p>"
  ],
  [
    "<p>Cancel a continuous archiving base backup if\n              <span class=\"quote\">&#x201C;<span class=\"quote\">fast</span>&#x201D;</span> shutdown is requested\n              (Laurenz Albe)</p>"
  ],
  [
    "<p>Allow <code class=\"filename\">recovery.conf</code>\n              boolean variables to take the same range of string\n              values as <code class=\"filename\">postgresql.conf</code> boolean variables\n              (Bruce)</p>"
  ],
  [
    "<p>Add <code class=\"function\">pg_conf_load_time()</code> to report when\n              the <span class=\"productname\">PostgreSQL</span>\n              configuration files were last loaded (George\n              Gensure)</p>"
  ],
  [
    "<p>Add <code class=\"function\">pg_terminate_backend()</code> to safely\n              terminate a backend (the <code class=\"literal\">SIGTERM</code> signal works also) (Tom,\n              Bruce)</p>",
    "<p>While it's always been possible to <code class=\"literal\">SIGTERM</code> a single backend, this was\n              previously considered unsupported; and testing of the\n              case found some bugs that are now fixed.</p>"
  ],
  [
    "<p>Add ability to track user-defined functions' call\n              counts and runtimes (Martin Pihlak)</p>",
    "<p>Function statistics appear in a new system view,\n              <code class=\"literal\">pg_stat_user_functions</code>.\n              Tracking is controlled by the new parameter\n              <code class=\"varname\">track_functions</code>.</p>"
  ],
  [
    "<p>Allow specification of the maximum query string\n              size in <code class=\"literal\">pg_stat_activity</code>\n              via new <code class=\"varname\">track_activity_query_size</code> parameter\n              (Thomas Lee)</p>"
  ],
  [
    "<p>Increase the maximum line length sent to\n              <span class=\"application\">syslog</span>, in hopes of\n              improving performance (Tom)</p>"
  ],
  [
    "<p>Add read-only configuration variables <code class=\"varname\">segment_size</code>, <code class=\"varname\">wal_block_size</code>, and <code class=\"varname\">wal_segment_size</code> (Bernd Helmle)</p>"
  ],
  [
    "<p>When reporting a deadlock, report the text of all\n              queries involved in the deadlock to the server log\n              (Itagaki Takahiro)</p>"
  ],
  [
    "<p>Add <code class=\"function\">pg_stat_get_activity(pid)</code> function\n              to return information about a specific process id\n              (Magnus)</p>"
  ],
  [
    "<p>Allow the location of the server's statistics file\n              to be specified via <code class=\"varname\">stats_temp_directory</code> (Magnus)</p>",
    "<p>This allows the statistics file to be placed in a\n              <acronym class=\"acronym\">RAM</acronym>-resident\n              directory to reduce I/O requirements. On\n              startup/shutdown, the file is copied to its\n              traditional location (<code class=\"literal\">$PGDATA/global/</code>) so it is preserved\n              across restarts.</p>"
  ],
  [
    "<p>Add support for <code class=\"literal\">WINDOW</code>\n            functions (Hitoshi Harada)</p>"
  ],
  [
    "<p>Add support for <code class=\"literal\">WITH</code>\n            clauses (CTEs), including <code class=\"literal\">WITH\n            RECURSIVE</code> (Yoshiyuki Asaba, Tatsuo Ishii,\n            Tom)</p>"
  ],
  [
    "<p>Add <code class=\"command\">TABLE</code> command\n            (Peter)</p>",
    "<p><code class=\"literal\">TABLE tablename</code> is a\n            SQL standard short-hand for <code class=\"literal\">SELECT * FROM tablename</code>.</p>"
  ],
  [
    "<p>Allow <code class=\"literal\">AS</code> to be optional\n            when specifying a <code class=\"command\">SELECT</code>\n            (or <code class=\"literal\">RETURNING</code>) column\n            output label (Hiroshi Saito)</p>",
    "<p>This works so long as the column label is not any\n            <span class=\"productname\">PostgreSQL</span> keyword;\n            otherwise <code class=\"literal\">AS</code> is still\n            needed.</p>"
  ],
  [
    "<p>Support set-returning functions in <code class=\"command\">SELECT</code> result lists even for functions\n            that return their result via a tuplestore (Tom)</p>",
    "<p>In particular, this means that functions written in\n            PL/pgSQL and other PL languages can now be called this\n            way.</p>"
  ],
  [
    "<p>Support set-returning functions in the output of\n            aggregation and grouping queries (Tom)</p>"
  ],
  [
    "<p>Allow <code class=\"command\">SELECT FOR\n            UPDATE</code>/<code class=\"literal\">SHARE</code> to\n            work on inheritance trees (Tom)</p>"
  ],
  [
    "<p>Add infrastructure for <acronym class=\"acronym\">SQL/MED</acronym> (Martin Pihlak, Peter)</p>",
    "<p>There are no remote or external <acronym class=\"acronym\">SQL/MED</acronym> capabilities yet, but this\n            change provides a standardized and future-proof system\n            for managing connection information for modules like\n            <code class=\"filename\">dblink</code> and <code class=\"filename\">plproxy</code>.</p>"
  ],
  [
    "<p>Invalidate cached plans when referenced schemas,\n            functions, operators, or operator classes are modified\n            (Martin Pihlak, Tom)</p>",
    "<p>This improves the system's ability to respond to\n            on-the-fly DDL changes.</p>"
  ],
  [
    "<p>Allow comparison of composite types and allow arrays\n            of anonymous composite types (Tom)</p>",
    "<p>This allows constructs such as <code class=\"literal\">row(1, 1.1) = any (array[row(7, 7.7), row(1,\n            1.0)])</code>. This is particularly useful in recursive\n            queries.</p>"
  ],
  [
    "<p>Add support for Unicode string literal and\n            identifier specifications using code points, e.g.\n            <code class=\"literal\">U&amp;'d\\0061t\\+000061'</code>\n            (Peter)</p>"
  ],
  [
    "<p>Reject <code class=\"literal\">\\000</code> in string\n            literals and <code class=\"command\">COPY</code> data\n            (Tom)</p>",
    "<p>Previously, this was accepted but had the effect of\n            terminating the string contents.</p>"
  ],
  [
    "<p>Improve the parser's ability to report error\n            locations (Tom)</p>",
    "<p>An error location is now reported for many semantic\n            errors, such as mismatched datatypes, that previously\n            could not be localized.</p>"
  ],
  [
    "<p>Support statement-level <code class=\"literal\">ON\n              TRUNCATE</code> triggers (Simon)</p>"
  ],
  [
    "<p>Add <code class=\"literal\">RESTART</code>/<code class=\"literal\">CONTINUE IDENTITY</code> options for\n              <code class=\"command\">TRUNCATE TABLE</code> (Zoltan\n              Boszormenyi)</p>",
    "<p>The start value of a sequence can be changed by\n              <code class=\"command\">ALTER SEQUENCE START\n              WITH</code>.</p>"
  ],
  [
    "<p>Allow <code class=\"command\">TRUNCATE tab1,\n              tab1</code> to succeed (Bruce)</p>"
  ],
  [
    "<p>Add a separate <code class=\"command\">TRUNCATE</code> permission (Robert\n              Haas)</p>"
  ],
  [
    "<p>Make <code class=\"command\">EXPLAIN VERBOSE</code>\n              show the output columns of each plan node (Tom)</p>",
    "<p>Previously <code class=\"command\">EXPLAIN\n              VERBOSE</code> output an internal representation of\n              the query plan. (That behavior is now available via\n              <code class=\"varname\">debug_print_plan</code>.)</p>"
  ],
  [
    "<p>Make <code class=\"command\">EXPLAIN</code> identify\n              subplans and initplans with individual labels\n              (Tom)</p>"
  ],
  [
    "<p>Make <code class=\"command\">EXPLAIN</code> honor\n              <code class=\"varname\">debug_print_plan</code>\n              (Tom)</p>"
  ],
  [
    "<p>Allow <code class=\"command\">EXPLAIN</code> on\n              <code class=\"command\">CREATE TABLE AS</code>\n              (Peter)</p>"
  ],
  [
    "<p>Allow sub-selects in <code class=\"literal\">LIMIT</code> and <code class=\"literal\">OFFSET</code> (Tom)</p>"
  ],
  [
    "<p>Add <acronym class=\"acronym\">SQL</acronym>-standard syntax for\n              <code class=\"literal\">LIMIT</code>/<code class=\"literal\">OFFSET</code> capabilities (Peter)</p>",
    "<p>To wit, <code class=\"literal\">OFFSET num\n              {ROW|ROWS} FETCH {FIRST|NEXT} [num] {ROW|ROWS}\n              ONLY</code>.</p>"
  ],
  [
    "<p>Add support for column-level privileges (Stephen\n            Frost, KaiGai Kohei)</p>"
  ],
  [
    "<p>Refactor multi-object <code class=\"command\">DROP</code> operations to reduce the need for\n            <code class=\"literal\">CASCADE</code> (Alex\n            Hunsaker)</p>",
    "<p>For example, if table <code class=\"literal\">B</code>\n            has a dependency on table <code class=\"literal\">A</code>, the command <code class=\"literal\">DROP TABLE A, B</code> no longer requires the\n            <code class=\"literal\">CASCADE</code> option.</p>"
  ],
  [
    "<p>Fix various problems with concurrent <code class=\"command\">DROP</code> commands by ensuring that locks\n            are taken before we begin to drop dependencies of an\n            object (Tom)</p>"
  ],
  [
    "<p>Improve reporting of dependencies during\n            <code class=\"command\">DROP</code> commands (Tom)</p>"
  ],
  [
    "<p>Add <code class=\"literal\">WITH [NO] DATA</code>\n            clause to <code class=\"command\">CREATE TABLE AS</code>,\n            per the <acronym class=\"acronym\">SQL</acronym> standard\n            (Peter, Tom)</p>"
  ],
  [
    "<p>Add support for user-defined I/O conversion casts\n            (Heikki)</p>"
  ],
  [
    "<p>Allow <code class=\"command\">CREATE AGGREGATE</code>\n            to use an <code class=\"type\">internal</code> transition\n            datatype (Tom)</p>"
  ],
  [
    "<p>Add <code class=\"literal\">LIKE</code> clause to\n            <code class=\"command\">CREATE TYPE</code> (Tom)</p>",
    "<p>This simplifies creation of data types that use the\n            same internal representation as an existing type.</p>"
  ],
  [
    "<p>Allow specification of the type category and\n            <span class=\"quote\">&#x201C;<span class=\"quote\">preferred</span>&#x201D;</span> status for\n            user-defined base types (Tom)</p>",
    "<p>This allows more control over the coercion behavior\n            of user-defined types.</p>"
  ],
  [
    "<p>Allow <code class=\"command\">CREATE OR REPLACE\n            VIEW</code> to add columns to the end of a view (Robert\n            Haas)</p>"
  ],
  [
    "<p>Add <code class=\"command\">ALTER TYPE RENAME</code>\n              (Petr Jelinek)</p>"
  ],
  [
    "<p>Add <code class=\"command\">ALTER SEQUENCE ...\n              RESTART</code> (with no parameter) to reset a\n              sequence to its initial value (Zoltan\n              Boszormenyi)</p>"
  ],
  [
    "<p>Modify the <code class=\"command\">ALTER\n              TABLE</code> syntax to allow all reasonable\n              combinations for tables, indexes, sequences, and\n              views (Tom)</p>",
    "<p>This change allows the following new syntaxes:</p>",
    "<div class=\"itemizedlist\">\n                <ul class=\"itemizedlist\" style=\"list-style-type: circle;\">\n                  <li class=\"listitem\">\n                    <p><code class=\"command\">ALTER SEQUENCE OWNER\n                    TO</code></p>\n                  </li>\n                  <li class=\"listitem\">\n                    <p><code class=\"command\">ALTER VIEW ALTER\n                    COLUMN SET/DROP DEFAULT</code></p>\n                  </li>\n                  <li class=\"listitem\">\n                    <p><code class=\"command\">ALTER VIEW OWNER\n                    TO</code></p>\n                  </li>\n                  <li class=\"listitem\">\n                    <p><code class=\"command\">ALTER VIEW SET\n                    SCHEMA</code></p>\n                  </li>\n                </ul>\n              </div>",
    "<p>There is no actual new functionality here, but\n              formerly you had to say <code class=\"command\">ALTER\n              TABLE</code> to do these things, which was\n              confusing.</p>"
  ],
  [
    "<p>\n  <code class=\"command\">ALTER SEQUENCE OWNER\n                    TO</code>\n</p>"
  ],
  [
    "<p>\n  <code class=\"command\">ALTER VIEW ALTER\n                    COLUMN SET/DROP DEFAULT</code>\n</p>"
  ],
  [
    "<p>\n  <code class=\"command\">ALTER VIEW OWNER\n                    TO</code>\n</p>"
  ],
  [
    "<p>\n  <code class=\"command\">ALTER VIEW SET\n                    SCHEMA</code>\n</p>"
  ],
  [
    "<p>Add support for the syntax <code class=\"command\">ALTER TABLE ... ALTER COLUMN ... SET DATA\n              TYPE</code> (Peter)</p>",
    "<p>This is <acronym class=\"acronym\">SQL</acronym>-standard syntax for\n              functionality that was already supported.</p>"
  ],
  [
    "<p>Make <code class=\"command\">ALTER TABLE SET WITHOUT\n              OIDS</code> rewrite the table to physically remove\n              <code class=\"type\">OID</code> values (Tom)</p>",
    "<p>Also, add <code class=\"command\">ALTER TABLE SET\n              WITH OIDS</code> to rewrite the table to add\n              <code class=\"type\">OID</code>s.</p>"
  ],
  [
    "<p>Improve reporting of <code class=\"command\">CREATE</code>/<code class=\"command\">DROP</code>/<code class=\"command\">RENAME\n              DATABASE</code> failure when uncommitted prepared\n              transactions are the cause (Tom)</p>"
  ],
  [
    "<p>Make <code class=\"varname\">LC_COLLATE</code> and\n              <code class=\"varname\">LC_CTYPE</code> into\n              per-database settings (Radek Strnad, Heikki)</p>",
    "<p>This makes collation similar to encoding, which\n              was always configurable per database.</p>"
  ],
  [
    "<p>Improve checks that the database encoding,\n              collation (<code class=\"varname\">LC_COLLATE</code>),\n              and character classes (<code class=\"varname\">LC_CTYPE</code>) match (Heikki, Tom)</p>",
    "<p>Note in particular that a new database's encoding\n              and locale settings can be changed only when copying\n              from <code class=\"literal\">template0</code>. This\n              prevents possibly copying data that doesn't match the\n              settings.</p>"
  ],
  [
    "<p>Add <code class=\"command\">ALTER DATABASE SET\n              TABLESPACE</code> to move a database to a new\n              tablespace (Guillaume Lelarge, Bernd Helmle)</p>"
  ],
  [
    "<p>Add a <code class=\"literal\">VERBOSE</code> option to\n            the <code class=\"command\">CLUSTER</code> command and\n            <span class=\"application\">clusterdb</span> (Jim\n            Cox)</p>"
  ],
  [
    "<p>Decrease memory requirements for recording pending\n            trigger events (Tom)</p>"
  ],
  [
    "<p>Dramatically improve the speed of building and\n              accessing hash indexes (Tom Raney, Shreya\n              Bhargava)</p>",
    "<p>This allows hash indexes to be sometimes faster\n              than btree indexes. However, hash indexes are still\n              not crash-safe.</p>"
  ],
  [
    "<p>Make hash indexes store only the hash code, not\n              the full value of the indexed column (Xiao Meng)</p>",
    "<p>This greatly reduces the size of hash indexes for\n              long indexed values, improving performance.</p>"
  ],
  [
    "<p>Implement fast update option for GIN indexes\n              (Teodor, Oleg)</p>",
    "<p>This option greatly improves update speed at a\n              small penalty in search speed.</p>"
  ],
  [
    "<p><code class=\"literal\">xxx_pattern_ops</code>\n              indexes can now be used for simple equality\n              comparisons, not only for <code class=\"literal\">LIKE</code> (Tom)</p>"
  ],
  [
    "<p>Remove the requirement to use <code class=\"literal\">@@@</code> when doing <acronym class=\"acronym\">GIN</acronym> weighted lookups on full text\n              indexes (Tom, Teodor)</p>",
    "<p>The normal <code class=\"literal\">@@</code> text\n              search operator can be used instead.</p>"
  ],
  [
    "<p>Add an optimizer selectivity function for\n              <code class=\"literal\">@@</code> text search\n              operations (Jan Urbanski)</p>"
  ],
  [
    "<p>Allow prefix matching in full text searches\n              (Teodor Sigaev, Oleg Bartunov)</p>"
  ],
  [
    "<p>Support multi-column <acronym class=\"acronym\">GIN</acronym> indexes (Teodor Sigaev)</p>"
  ],
  [
    "<p>Improve support for Nepali language and Devanagari\n              alphabet (Teodor)</p>"
  ],
  [
    "<p>Track free space in separate per-relation\n              <span class=\"quote\">&#x201C;<span class=\"quote\">fork</span>&#x201D;</span> files (Heikki)</p>",
    "<p>Free space discovered by <code class=\"command\">VACUUM</code> is now recorded in\n              <code class=\"filename\">*_fsm</code> files, rather\n              than in a fixed-sized shared memory area. The\n              <code class=\"varname\">max_fsm_pages</code> and\n              <code class=\"varname\">max_fsm_relations</code>\n              settings have been removed, greatly simplifying\n              administration of free space management.</p>"
  ],
  [
    "<p>Add a visibility map to track pages that do not\n              require vacuuming (Heikki)</p>",
    "<p>This allows <code class=\"command\">VACUUM</code> to\n              avoid scanning all of a table when only a portion of\n              the table needs vacuuming. The visibility map is\n              stored in per-relation <span class=\"quote\">&#x201C;<span class=\"quote\">fork</span>&#x201D;</span>\n              files.</p>"
  ],
  [
    "<p>Add <code class=\"varname\">vacuum_freeze_table_age</code> parameter to\n              control when <code class=\"command\">VACUUM</code>\n              should ignore the visibility map and do a full table\n              scan to freeze tuples (Heikki)</p>"
  ],
  [
    "<p>Track transaction snapshots more carefully\n              (Alvaro)</p>",
    "<p>This improves <code class=\"command\">VACUUM</code>'s ability to reclaim space in\n              the presence of long-running transactions.</p>"
  ],
  [
    "<p>Add ability to specify per-relation autovacuum and\n              <acronym class=\"acronym\">TOAST</acronym> parameters\n              in <code class=\"command\">CREATE TABLE</code> (Alvaro,\n              Euler Taveira de Oliveira)</p>",
    "<p>Autovacuum options used to be stored in a system\n              table.</p>"
  ],
  [
    "<p>Add <code class=\"literal\">--freeze</code> option\n              to <span class=\"application\">vacuumdb</span>\n              (Bruce)</p>"
  ],
  [
    "<p>Add a <code class=\"literal\">CaseSensitive</code>\n            option for text search synonym dictionaries (Simon)</p>"
  ],
  [
    "<p>Improve the precision of <code class=\"type\">NUMERIC</code> division (Tom)</p>"
  ],
  [
    "<p>Add basic arithmetic operators for <code class=\"type\">int2</code> with <code class=\"type\">int8</code>\n            (Tom)</p>",
    "<p>This eliminates the need for explicit casting in\n            some situations.</p>"
  ],
  [
    "<p>Allow <code class=\"type\">UUID</code> input to accept\n            an optional hyphen after every fourth digit (Robert\n            Haas)</p>"
  ],
  [
    "<p>Allow <code class=\"literal\">on</code>/<code class=\"literal\">off</code> as input for the boolean data type\n            (Itagaki Takahiro)</p>"
  ],
  [
    "<p>Allow spaces around <code class=\"literal\">NaN</code>\n            in the input string for type <code class=\"type\">numeric</code> (Sam Mason)</p>"
  ],
  [
    "<p>Reject year <code class=\"literal\">0 BC</code> and\n              years <code class=\"literal\">000</code> and\n              <code class=\"literal\">0000</code> (Tom)</p>",
    "<p>Previously these were interpreted as <code class=\"literal\">1 BC</code>. (Note: years <code class=\"literal\">0</code> and <code class=\"literal\">00</code> are still assumed to be the year\n              2000.)</p>"
  ],
  [
    "<p>Include <code class=\"literal\">SGT</code>\n              (Singapore time) in the default list of known time\n              zone abbreviations (Tom)</p>"
  ],
  [
    "<p>Support <code class=\"literal\">infinity</code> and\n              <code class=\"literal\">-infinity</code> as values of\n              type <code class=\"type\">date</code> (Tom)</p>"
  ],
  [
    "<p>Make parsing of <code class=\"type\">interval</code>\n              literals more standard-compliant (Tom, Ron Mayer)</p>",
    "<p>For example, <code class=\"literal\">INTERVAL '1'\n              YEAR</code> now does what it's supposed to.</p>"
  ],
  [
    "<p>Allow <code class=\"type\">interval</code>\n              fractional-seconds precision to be specified after\n              the <code class=\"literal\">second</code> keyword, for\n              <acronym class=\"acronym\">SQL</acronym> standard\n              compliance (Tom)</p>",
    "<p>Formerly the precision had to be specified after\n              the keyword <code class=\"type\">interval</code>. (For\n              backwards compatibility, this syntax is still\n              supported, though deprecated.) Data type definitions\n              will now be output using the standard format.</p>"
  ],
  [
    "<p>Support the <acronym class=\"acronym\">IS0\n              8601</acronym> <code class=\"type\">interval</code>\n              syntax (Ron Mayer, Kevin Grittner)</p>",
    "<p>For example, <code class=\"literal\">INTERVAL\n              'P1Y2M3DT4H5M6.7S'</code> is now supported.</p>"
  ],
  [
    "<p>Add <code class=\"varname\">IntervalStyle</code>\n              parameter which controls how <code class=\"type\">interval</code> values are output (Ron\n              Mayer)</p>",
    "<p>Valid values are: <code class=\"literal\">postgres</code>, <code class=\"literal\">postgres_verbose</code>, <code class=\"literal\">sql_standard</code>, <code class=\"literal\">iso_8601</code>. This setting also controls\n              the handling of negative <code class=\"type\">interval</code> input when only some fields\n              have positive/negative designations.</p>"
  ],
  [
    "<p>Improve consistency of handling of fractional\n              seconds in <code class=\"type\">timestamp</code> and\n              <code class=\"type\">interval</code> output (Ron\n              Mayer)</p>"
  ],
  [
    "<p>Improve the handling of casts applied to\n              <code class=\"literal\">ARRAY[]</code> constructs, such\n              as <code class=\"literal\">ARRAY[...]::integer[]</code>\n              (Brendan Jurd)</p>",
    "<p>Formerly <span class=\"productname\">PostgreSQL</span> attempted to\n              determine a data type for the <code class=\"literal\">ARRAY[]</code> construct without reference\n              to the ensuing cast. This could fail unnecessarily in\n              many cases, in particular when the <code class=\"literal\">ARRAY[]</code> construct was empty or\n              contained only ambiguous entries such as <code class=\"literal\">NULL</code>. Now the cast is consulted to\n              determine the type that the array elements must\n              be.</p>"
  ],
  [
    "<p>Make <acronym class=\"acronym\">SQL</acronym>-syntax\n              <code class=\"type\">ARRAY</code> dimensions optional\n              to match the <acronym class=\"acronym\">SQL</acronym>\n              standard (Peter)</p>"
  ],
  [
    "<p>Add <code class=\"function\">array_ndims()</code> to\n              return the number of dimensions of an array (Robert\n              Haas)</p>"
  ],
  [
    "<p>Add <code class=\"function\">array_length()</code>\n              to return the length of an array for a specified\n              dimension (Jim Nasby, Robert Haas, Peter\n              Eisentraut)</p>"
  ],
  [
    "<p>Add aggregate function <code class=\"function\">array_agg()</code>, which returns all\n              aggregated values as a single array (Robert Haas,\n              Jeff Davis, Peter)</p>"
  ],
  [
    "<p>Add <code class=\"function\">unnest()</code>, which\n              converts an array to individual row values (Tom)</p>",
    "<p>This is the opposite of <code class=\"function\">array_agg()</code>.</p>"
  ],
  [
    "<p>Add <code class=\"function\">array_fill()</code> to\n              create arrays initialized with a value (Pavel\n              Stehule)</p>"
  ],
  [
    "<p>Add <code class=\"function\">generate_subscripts()</code> to simplify\n              generating the range of an array's subscripts (Pavel\n              Stehule)</p>"
  ],
  [
    "<p>Consider <acronym class=\"acronym\">TOAST</acronym>\n              compression on values as short as 32 bytes\n              (previously 256 bytes) (Greg Stark)</p>"
  ],
  [
    "<p>Require 25% minimum space savings before using\n              <acronym class=\"acronym\">TOAST</acronym> compression\n              (previously 20% for small values and\n              any-savings-at-all for large values) (Greg)</p>"
  ],
  [
    "<p>Improve <acronym class=\"acronym\">TOAST</acronym>\n              heuristics for rows that have a mix of large and\n              small toastable fields, so that we prefer to push\n              large values out of line and don't compress small\n              values unnecessarily (Greg, Tom)</p>"
  ],
  [
    "<p>Document that <code class=\"function\">setseed()</code> allows values from\n            <code class=\"literal\">-1</code> to <code class=\"literal\">1</code> (not just <code class=\"literal\">0</code> to <code class=\"literal\">1</code>),\n            and enforce the valid range (Kris Jurka)</p>"
  ],
  [
    "<p>Add server-side function <code class=\"function\">lo_import(filename, oid)</code> (Tatsuo)</p>"
  ],
  [
    "<p>Add <code class=\"function\">quote_nullable()</code>,\n            which behaves like <code class=\"function\">quote_literal()</code> but returns the\n            string <code class=\"literal\">NULL</code> for a null\n            argument (Brendan Jurd)</p>"
  ],
  [
    "<p>Improve full text search <code class=\"function\">headline()</code> function to allow\n            extracting several fragments of text (Sushant\n            Sinha)</p>"
  ],
  [
    "<p>Add <code class=\"function\">suppress_redundant_updates_trigger()</code>\n            trigger function to avoid overhead for\n            non-data-changing updates (Andrew)</p>"
  ],
  [
    "<p>Add <code class=\"function\">div(numeric,\n            numeric)</code> to perform <code class=\"type\">numeric</code> division without rounding\n            (Tom)</p>"
  ],
  [
    "<p>Add <code class=\"type\">timestamp</code> and\n            <code class=\"type\">timestamptz</code> versions of\n            <code class=\"function\">generate_series()</code>\n            (Hitoshi Harada)</p>"
  ],
  [
    "<p>Implement <code class=\"function\">current_query()</code> for use by\n              functions that need to know the currently running\n              query (Tomas Doran)</p>"
  ],
  [
    "<p>Add <code class=\"function\">pg_get_keywords()</code> to return a list\n              of the parser keywords (Dave Page)</p>"
  ],
  [
    "<p>Add <code class=\"function\">pg_get_functiondef()</code> to see a\n              function's definition (Abhijit Menon-Sen)</p>"
  ],
  [
    "<p>Allow the second argument of <code class=\"function\">pg_get_expr()</code> to be zero when\n              deparsing an expression that does not contain\n              variables (Tom)</p>"
  ],
  [
    "<p>Modify <code class=\"function\">pg_relation_size()</code> to use\n              <code class=\"literal\">regclass</code> (Heikki)</p>",
    "<p><code class=\"function\">pg_relation_size(data_type_name)</code> no\n              longer works.</p>"
  ],
  [
    "<p>Add <code class=\"literal\">boot_val</code> and\n              <code class=\"literal\">reset_val</code> columns to\n              <code class=\"literal\">pg_settings</code> output (Greg\n              Smith)</p>"
  ],
  [
    "<p>Add source file name and line number columns to\n              <code class=\"literal\">pg_settings</code> output for\n              variables set in a configuration file (Magnus,\n              Alvaro)</p>",
    "<p>For security reasons, these columns are only\n              visible to superusers.</p>"
  ],
  [
    "<p>Add support for <code class=\"varname\">CURRENT_CATALOG</code>, <code class=\"varname\">CURRENT_SCHEMA</code>, <code class=\"varname\">SET CATALOG</code>, <code class=\"varname\">SET SCHEMA</code> (Peter)</p>",
    "<p>These provide <acronym class=\"acronym\">SQL</acronym>-standard syntax for existing\n              features.</p>"
  ],
  [
    "<p>Add <code class=\"function\">pg_typeof()</code>\n              which returns the data type of any value (Brendan\n              Jurd)</p>"
  ],
  [
    "<p>Make <code class=\"function\">version()</code>\n              return information about whether the server is a 32-\n              or 64-bit binary (Bruce)</p>"
  ],
  [
    "<p>Fix the behavior of information schema columns\n              <code class=\"structfield\">is_insertable_into</code>\n              and <code class=\"structfield\">is_updatable</code> to\n              be consistent (Peter)</p>"
  ],
  [
    "<p>Improve the behavior of information schema\n              <code class=\"structfield\">datetime_precision</code>\n              columns (Peter)</p>",
    "<p>These columns now show zero for <code class=\"type\">date</code> columns, and 6 (the default\n              precision) for <code class=\"type\">time</code>,\n              <code class=\"type\">timestamp</code>, and <code class=\"type\">interval</code> without a declared precision,\n              rather than showing null as formerly.</p>"
  ],
  [
    "<p>Convert remaining builtin set-returning functions\n              to use <code class=\"literal\">OUT</code> parameters\n              (Jaime Casanova)</p>",
    "<p>This makes it possible to call these functions\n              without specifying a column list: <code class=\"function\">pg_show_all_settings()</code>,\n              <code class=\"function\">pg_lock_status()</code>,\n              <code class=\"function\">pg_prepared_xact()</code>,\n              <code class=\"function\">pg_prepared_statement()</code>,\n              <code class=\"function\">pg_cursor()</code></p>"
  ],
  [
    "<p>Make <code class=\"function\">pg_*_is_visible()</code> and <code class=\"function\">has_*_privilege()</code> functions return\n              <code class=\"literal\">NULL</code> for invalid OIDs,\n              rather than reporting an error (Tom)</p>"
  ],
  [
    "<p>Extend <code class=\"function\">has_*_privilege()</code> functions to\n              allow inquiring about the OR of multiple privileges\n              in one call (Stephen Frost, Tom)</p>"
  ],
  [
    "<p>Add <code class=\"function\">has_column_privilege()</code> and\n              <code class=\"function\">has_any_column_privilege()</code>\n              functions (Stephen Frost, Tom)</p>"
  ],
  [
    "<p>Support variadic functions (functions with a\n              variable number of arguments) (Pavel Stehule)</p>",
    "<p>Only trailing arguments can be optional, and they\n              all must be of the same data type.</p>"
  ],
  [
    "<p>Support default values for function arguments\n              (Pavel Stehule)</p>"
  ],
  [
    "<p>Add <code class=\"command\">CREATE FUNCTION ...\n              RETURNS TABLE</code> clause (Pavel Stehule)</p>"
  ],
  [
    "<p>Allow <acronym class=\"acronym\">SQL</acronym>-language functions to return\n              the output of an <code class=\"command\">INSERT</code>/<code class=\"command\">UPDATE</code>/<code class=\"command\">DELETE</code> <code class=\"literal\">RETURNING</code> clause (Tom)</p>"
  ],
  [
    "<p>Support <code class=\"literal\">EXECUTE USING</code>\n              for easier insertion of data values into a dynamic\n              query string (Pavel Stehule)</p>"
  ],
  [
    "<p>Allow looping over the results of a cursor using a\n              <code class=\"literal\">FOR</code> loop (Pavel\n              Stehule)</p>"
  ],
  [
    "<p>Support <code class=\"literal\">RETURN QUERY\n              EXECUTE</code> (Pavel Stehule)</p>"
  ],
  [
    "<p>Improve the <code class=\"literal\">RAISE</code>\n              command (Pavel Stehule)</p>",
    "<div class=\"itemizedlist\">\n                <ul class=\"itemizedlist\" style=\"list-style-type: circle;\">\n                  <li class=\"listitem\">\n                    <p>Support <code class=\"literal\">DETAIL</code>\n                    and <code class=\"literal\">HINT</code>\n                    fields</p>\n                  </li>\n                  <li class=\"listitem\">\n                    <p>Support specification of the <code class=\"literal\">SQLSTATE</code> error code</p>\n                  </li>\n                  <li class=\"listitem\">\n                    <p>Support an exception name parameter</p>\n                  </li>\n                  <li class=\"listitem\">\n                    <p>Allow <code class=\"literal\">RAISE</code>\n                    without parameters in an exception block to\n                    re-throw the current error</p>\n                  </li>\n                </ul>\n              </div>"
  ],
  [
    "<p>Support <code class=\"literal\">DETAIL</code>\n                    and <code class=\"literal\">HINT</code>\n                    fields</p>"
  ],
  [
    "<p>Support specification of the <code class=\"literal\">SQLSTATE</code> error code</p>"
  ],
  [
    "<p>Support an exception name parameter</p>"
  ],
  [
    "<p>Allow <code class=\"literal\">RAISE</code>\n                    without parameters in an exception block to\n                    re-throw the current error</p>"
  ],
  [
    "<p>Allow specification of <code class=\"varname\">SQLSTATE</code> codes in <code class=\"literal\">EXCEPTION</code> lists (Pavel Stehule)</p>",
    "<p>This is useful for handling custom <code class=\"varname\">SQLSTATE</code> codes.</p>"
  ],
  [
    "<p>Support the <code class=\"literal\">CASE</code>\n              statement (Pavel Stehule)</p>"
  ],
  [
    "<p>Make <code class=\"command\">RETURN QUERY</code> set\n              the special <code class=\"literal\">FOUND</code> and\n              <code class=\"command\">GET DIAGNOSTICS</code>\n              <code class=\"literal\">ROW_COUNT</code> variables\n              (Pavel Stehule)</p>"
  ],
  [
    "<p>Make <code class=\"command\">FETCH</code> and\n              <code class=\"command\">MOVE</code> set the\n              <code class=\"command\">GET DIAGNOSTICS</code>\n              <code class=\"literal\">ROW_COUNT</code> variable\n              (Andrew Gierth)</p>"
  ],
  [
    "<p>Make <code class=\"command\">EXIT</code> without a\n              label always exit the innermost loop (Tom)</p>",
    "<p>Formerly, if there were a <code class=\"literal\">BEGIN</code> block more closely nested than\n              any loop, it would exit that block instead. The new\n              behavior matches Oracle(TM) and is also what was\n              previously stated by our own documentation.</p>"
  ],
  [
    "<p>Make processing of string literals and nested\n              block comments match the main SQL parser's processing\n              (Tom)</p>",
    "<p>In particular, the format string in <code class=\"command\">RAISE</code> now works the same as any\n              other string literal, including being subject to\n              <code class=\"varname\">standard_conforming_strings</code>. This\n              change also fixes other cases in which valid commands\n              would fail when <code class=\"varname\">standard_conforming_strings</code> is\n              on.</p>"
  ],
  [
    "<p>Avoid memory leakage when the same function is\n              called at varying exception-block nesting depths\n              (Tom)</p>"
  ],
  [
    "<p>Fix <code class=\"literal\">pg_ctl restart</code> to\n            preserve command-line arguments (Bruce)</p>"
  ],
  [
    "<p>Add <code class=\"literal\">-w</code>/<code class=\"literal\">--no-password</code> option that prevents\n            password prompting in all utilities that have a\n            <code class=\"literal\">-W</code>/<code class=\"literal\">--password</code> option (Peter)</p>"
  ],
  [
    "<p>Remove <code class=\"option\">-q</code> (quiet) option\n            of <span class=\"application\">createdb</span>,\n            <span class=\"application\">createuser</span>,\n            <span class=\"application\">dropdb</span>, <span class=\"application\">dropuser</span> (Peter)</p>",
    "<p>These options have had no effect since <span class=\"productname\">PostgreSQL</span> 8.3.</p>"
  ],
  [
    "<p>Remove verbose startup banner; now just suggest\n              <code class=\"literal\">help</code> (Joshua Drake)</p>"
  ],
  [
    "<p>Make <code class=\"literal\">help</code> show common\n              backslash commands (Greg Sabino Mullane)</p>"
  ],
  [
    "<p>Add <code class=\"literal\">\\pset format\n              wrapped</code> mode to wrap output to the screen\n              width, or file/pipe output too if <code class=\"literal\">\\pset columns</code> is set (Bryce\n              Nesbitt)</p>"
  ],
  [
    "<p>Allow all supported spellings of boolean values in\n              <code class=\"command\">\\pset</code>, rather than just\n              <code class=\"literal\">on</code> and <code class=\"literal\">off</code> (Bruce)</p>",
    "<p>Formerly, any string other than <span class=\"quote\">&#x201C;<span class=\"quote\">off</span>&#x201D;</span> was\n              silently taken to mean <code class=\"literal\">true</code>. <span class=\"application\">psql</span> will now complain about\n              unrecognized spellings (but still take them as\n              <code class=\"literal\">true</code>).</p>"
  ],
  [
    "<p>Use the pager for wide output (Bruce)</p>"
  ],
  [
    "<p>Require a space between a one-letter backslash\n              command and its first argument (Bernd Helmle)</p>",
    "<p>This removes a historical source of ambiguity.</p>"
  ],
  [
    "<p>Improve tab completion support for\n              schema-qualified and quoted identifiers (Greg Sabino\n              Mullane)</p>"
  ],
  [
    "<p>Add optional <code class=\"literal\">on</code>/<code class=\"literal\">off</code>\n              argument for <code class=\"command\">\\timing</code>\n              (David Fetter)</p>"
  ],
  [
    "<p>Display access control rights on multiple lines\n              (Brendan Jurd, Andreas Scherbaum)</p>"
  ],
  [
    "<p>Make <code class=\"command\">\\l</code> show database\n              access privileges (Andrew Gilligan)</p>"
  ],
  [
    "<p>Make <code class=\"command\">\\l+</code> show\n              database sizes, if permissions allow (Andrew\n              Gilligan)</p>"
  ],
  [
    "<p>Add the <code class=\"command\">\\ef</code> command\n              to edit function definitions (Abhijit Menon-Sen)</p>"
  ],
  [
    "<p>Make <code class=\"command\">\\d*</code> commands\n              that do not have a pattern argument show system\n              objects only if the <code class=\"literal\">S</code>\n              modifier is specified (Greg Sabino Mullane,\n              Bruce)</p>",
    "<p>The former behavior was inconsistent across\n              different variants of <code class=\"command\">\\d</code>, and in most cases it provided no\n              easy way to see just user objects.</p>"
  ],
  [
    "<p>Improve <code class=\"command\">\\d*</code> commands\n              to work with older <span class=\"productname\">PostgreSQL</span> server versions (back\n              to 7.4), not only the current server version\n              (Guillaume Lelarge)</p>"
  ],
  [
    "<p>Make <code class=\"command\">\\d</code> show\n              foreign-key constraints that reference the selected\n              table (Kenneth D'Souza)</p>"
  ],
  [
    "<p>Make <code class=\"command\">\\d</code> on a sequence\n              show its column values (Euler Taveira de\n              Oliveira)</p>"
  ],
  [
    "<p>Add column storage type and other relation options\n              to the <code class=\"command\">\\d+</code> display\n              (Gregory Stark, Euler Taveira de Oliveira)</p>"
  ],
  [
    "<p>Show relation size in <code class=\"command\">\\dt+</code> output (Dickson S. Guedes)</p>"
  ],
  [
    "<p>Show the possible values of <code class=\"literal\">enum</code> types in <code class=\"command\">\\dT+</code> (David Fetter)</p>"
  ],
  [
    "<p>Allow <code class=\"command\">\\dC</code> to accept a\n              wildcard pattern, which matches either datatype\n              involved in the cast (Tom)</p>"
  ],
  [
    "<p>Add a function type column to <code class=\"command\">\\df</code>'s output, and add options to\n              list only selected types of functions (David\n              Fetter)</p>"
  ],
  [
    "<p>Make <code class=\"command\">\\df</code> not hide\n              functions that take or return type <code class=\"type\">cstring</code> (Tom)</p>",
    "<p>Previously, such functions were hidden because\n              most of them are datatype I/O functions, which were\n              deemed uninteresting. The new policy about hiding\n              system functions by default makes this wart\n              unnecessary.</p>"
  ],
  [
    "<p>Add a <code class=\"literal\">--no-tablespaces</code> option to\n              <span class=\"application\">pg_dump</span>/<span class=\"application\">pg_dumpall</span>/<span class=\"application\">pg_restore</span> so that dumps can be\n              restored to clusters that have non-matching\n              tablespace layouts (Gavin Roy)</p>"
  ],
  [
    "<p>Remove <code class=\"option\">-d</code> and\n              <code class=\"option\">-D</code> options from\n              <span class=\"application\">pg_dump</span> and\n              <span class=\"application\">pg_dumpall</span> (Tom)</p>",
    "<p>These options were too frequently confused with\n              the option to select a database name in other\n              <span class=\"productname\">PostgreSQL</span> client\n              applications. The functionality is still available,\n              but you must now spell out the long option name\n              <code class=\"option\">--inserts</code> or <code class=\"option\">--column-inserts</code>.</p>"
  ],
  [
    "<p>Remove <code class=\"option\">-i</code>/<code class=\"option\">--ignore-version</code> option from\n              <span class=\"application\">pg_dump</span> and\n              <span class=\"application\">pg_dumpall</span> (Tom)</p>",
    "<p>Use of this option does not throw an error, but it\n              has no effect. This option was removed because the\n              version checks are necessary for safety.</p>"
  ],
  [
    "<p>Disable <code class=\"varname\">statement_timeout</code> during dump and\n              restore (Joshua Drake)</p>"
  ],
  [
    "<p>Add <span class=\"application\">pg_dump</span>/<span class=\"application\">pg_dumpall</span> option <code class=\"option\">--lock-wait-timeout</code> (David Gould)</p>",
    "<p>This allows dumps to fail if unable to acquire a\n              shared lock within the specified amount of time.</p>"
  ],
  [
    "<p>Reorder <span class=\"application\">pg_dump</span>\n              <code class=\"literal\">--data-only</code> output to\n              dump tables referenced by foreign keys before the\n              referencing tables (Tom)</p>",
    "<p>This allows data loads when foreign keys are\n              already present. If circular references make a safe\n              ordering impossible, a <code class=\"literal\">NOTICE</code> is issued.</p>"
  ],
  [
    "<p>Allow <span class=\"application\">pg_dump</span>,\n              <span class=\"application\">pg_dumpall</span>, and\n              <span class=\"application\">pg_restore</span> to use a\n              specified role (Benedek L&#xE1;szl&#xF3;)</p>"
  ],
  [
    "<p>Allow <span class=\"application\">pg_restore</span>\n              to use multiple concurrent connections to do the\n              restore (Andrew)</p>",
    "<p>The number of concurrent connections is controlled\n              by the option <code class=\"literal\">--jobs</code>.\n              This is supported only for custom-format\n              archives.</p>"
  ],
  [
    "<p>Allow the <code class=\"type\">OID</code> to be\n              specified when importing a large object, via new\n              function <code class=\"function\">lo_import_with_oid()</code> (Tatsuo)</p>"
  ],
  [
    "<p>Add <span class=\"quote\">&#x201C;<span class=\"quote\">events</span>&#x201D;</span> support (Andrew\n              Chernow, Merlin Moncure)</p>",
    "<p>This adds the ability to register callbacks to\n              manage private data associated with <code class=\"structname\">PGconn</code> and <code class=\"structname\">PGresult</code> objects.</p>"
  ],
  [
    "<p>Improve error handling to allow the return of\n              multiple error messages as multi-line error reports\n              (Magnus)</p>"
  ],
  [
    "<p>Make <code class=\"function\">PQexecParams()</code>\n              and related functions return <code class=\"varname\">PGRES_EMPTY_QUERY</code> for an empty query\n              (Tom)</p>",
    "<p>They previously returned <code class=\"varname\">PGRES_COMMAND_OK</code>.</p>"
  ],
  [
    "<p>Document how to avoid the overhead of <code class=\"function\">WSACleanup()</code> on Windows (Andrew\n              Chernow)</p>"
  ],
  [
    "<p>Do not rely on Kerberos tickets to determine the\n              default database username (Magnus)</p>",
    "<p>Previously, a Kerberos-capable build of libpq\n              would use the principal name from any available\n              Kerberos ticket as default database username, even if\n              the connection wasn't using Kerberos authentication.\n              This was deemed inconsistent and confusing. The\n              default username is now determined the same way with\n              or without Kerberos. Note however that the database\n              username must still match the ticket when Kerberos\n              authentication is used.</p>"
  ],
  [
    "<p>Fix certificate validation for <acronym class=\"acronym\">SSL</acronym> connections (Magnus)</p>",
    "<p><span class=\"application\">libpq</span> now\n              supports verifying both the certificate and the name\n              of the server when making <acronym class=\"acronym\">SSL</acronym> connections. If a root\n              certificate is not available to use for verification,\n              <acronym class=\"acronym\">SSL</acronym> connections\n              will fail. The <code class=\"literal\">sslmode</code>\n              parameter is used to enable certificate verification\n              and set the level of checking. The default is still\n              not to do any verification, allowing connections to\n              SSL-enabled servers without requiring a root\n              certificate on the client.</p>"
  ],
  [
    "<p>Support wildcard server certificates (Magnus)</p>",
    "<p>If a certificate <acronym class=\"acronym\">CN</acronym> starts with <code class=\"literal\">*</code>, it will be treated as a wildcard\n              when matching the hostname, allowing the use of the\n              same certificate for multiple servers.</p>"
  ],
  [
    "<p>Allow the file locations for client certificates\n              to be specified (Mark Woodward, Alvaro, Magnus)</p>"
  ],
  [
    "<p>Add a <code class=\"function\">PQinitOpenSSL</code>\n              function to allow greater control over\n              OpenSSL/libcrypto initialization (Andrew Chernow)</p>"
  ],
  [
    "<p>Make <span class=\"application\">libpq</span>\n              unregister its <span class=\"application\">OpenSSL</span> callbacks when no\n              database connections remain open (Bruce, Magnus,\n              Russell Smith)</p>",
    "<p>This is required for applications that unload the\n              libpq library, otherwise invalid <span class=\"application\">OpenSSL</span> callbacks will\n              remain.</p>"
  ],
  [
    "<p>Add localization support for messages (Euler\n              Taveira de Oliveira)</p>"
  ],
  [
    "<p>ecpg parser is now automatically generated from\n              the server parser (Michael)</p>",
    "<p>Previously the ecpg parser was\n              hand-maintained.</p>"
  ],
  [
    "<p>Add support for single-use plans with out-of-line\n              parameters (Tom)</p>"
  ],
  [
    "<p>Add new <code class=\"varname\">SPI_OK_REWRITTEN</code> return code for\n              <code class=\"function\">SPI_execute()</code>\n              (Heikki)</p>",
    "<p>This is used when a command is rewritten to\n              another type of command.</p>"
  ],
  [
    "<p>Remove unnecessary inclusions from <code class=\"filename\">executor/spi.h</code> (Tom)</p>",
    "<p>SPI-using modules might need to add some\n              <code class=\"literal\">#include</code> lines if they\n              were depending on <code class=\"filename\">spi.h</code>\n              to include things for them.</p>"
  ],
  [
    "<p>Update build system to use <span class=\"productname\">Autoconf</span> 2.61 (Peter)</p>"
  ],
  [
    "<p>Require <span class=\"productname\">GNU bison</span>\n            for source code builds (Peter)</p>",
    "<p>This has effectively been required for several\n            years, but now there is no infrastructure claiming to\n            support other parser tools.</p>"
  ],
  [
    "<p>Add <span class=\"application\">pg_config</span>\n            <code class=\"literal\">--htmldir</code> option\n            (Peter)</p>"
  ],
  [
    "<p>Pass <code class=\"type\">float4</code> by value\n            inside the server (Zoltan Boszormenyi)</p>",
    "<p>Add <span class=\"application\">configure</span>\n            option <code class=\"literal\">--disable-float4-byval</code> to use the old\n            behavior. External C functions that use old-style\n            (version 0) call convention and pass or return\n            <code class=\"type\">float4</code> values will be broken\n            by this change, so you may need the <span class=\"application\">configure</span> option if you have such\n            functions and don't want to update them.</p>"
  ],
  [
    "<p>Pass <code class=\"type\">float8</code>, <code class=\"type\">int8</code>, and related datatypes by value\n            inside the server on 64-bit platforms (Zoltan\n            Boszormenyi)</p>",
    "<p>Add <span class=\"application\">configure</span>\n            option <code class=\"literal\">--disable-float8-byval</code> to use the old\n            behavior. As above, this change might break old-style\n            external C functions.</p>"
  ],
  [
    "<p>Add configure options <code class=\"literal\">--with-segsize</code>, <code class=\"literal\">--with-blocksize</code>, <code class=\"literal\">--with-wal-blocksize</code>, <code class=\"literal\">--with-wal-segsize</code> (Zdenek Kotala,\n            Tom)</p>",
    "<p>This simplifies build-time control over several\n            constants that previously could only be changed by\n            editing <code class=\"filename\">pg_config_manual.h</code>.</p>"
  ],
  [
    "<p>Allow threaded builds on <span class=\"productname\">Solaris</span> 2.5 (Bruce)</p>"
  ],
  [
    "<p>Use the system's <code class=\"function\">getopt_long()</code> on <span class=\"productname\">Solaris</span> (Zdenek Kotala, Tom)</p>",
    "<p>This makes option processing more consistent with\n            what Solaris users expect.</p>"
  ],
  [
    "<p>Add support for the <span class=\"productname\">Sun\n            Studio</span> compiler on <span class=\"productname\">Linux</span> (Julius Stroffek)</p>"
  ],
  [
    "<p>Append the major version number to the backend\n            <span class=\"application\">gettext</span> domain, and\n            the <code class=\"literal\">soname</code> major version\n            number to libraries' <span class=\"application\">gettext</span> domain (Peter)</p>",
    "<p>This simplifies parallel installations of multiple\n            versions.</p>"
  ],
  [
    "<p>Add support for code coverage testing with\n            <span class=\"application\">gcov</span> (Michelle\n            Caisse)</p>"
  ],
  [
    "<p>Allow out-of-tree builds on <span class=\"productname\">Mingw</span> and <span class=\"productname\">Cygwin</span> (Richard Evans)</p>"
  ],
  [
    "<p>Fix the use of <span class=\"productname\">Mingw</span> as a cross-compiling source\n            platform (Peter)</p>"
  ],
  [
    "<p>Support 64-bit time zone data files (Heikki)</p>",
    "<p>This adds support for daylight saving time\n            (<acronym class=\"acronym\">DST</acronym>) calculations\n            beyond the year 2038.</p>"
  ],
  [
    "<p>Deprecate use of platform's <code class=\"type\">time_t</code> data type (Tom)</p>",
    "<p>Some platforms have migrated to 64-bit <code class=\"type\">time_t</code>, some have not, and Windows can't\n            make up its mind what it's doing. Define <code class=\"type\">pg_time_t</code> to have the same meaning as\n            <code class=\"type\">time_t</code>, but always be 64 bits\n            (unless the platform has no 64-bit integer type), and\n            use that type in all module APIs and on-disk data\n            formats.</p>"
  ],
  [
    "<p>Fix bug in handling of the time zone database when\n            cross-compiling (Richard Evans)</p>"
  ],
  [
    "<p>Link backend object files in one step, rather than\n            in stages (Peter)</p>"
  ],
  [
    "<p>Improve <span class=\"application\">gettext</span>\n            support to allow better translation of plurals\n            (Peter)</p>"
  ],
  [
    "<p>Add message translation support to the PL languages\n            (Alvaro, Peter)</p>"
  ],
  [
    "<p>Add more <span class=\"application\">DTrace</span>\n            probes (Robert Lor)</p>"
  ],
  [
    "<p>Enable <span class=\"application\">DTrace</span>\n            support on <span class=\"application\">macOS\n            Leopard</span> and other non-Solaris platforms (Robert\n            Lor)</p>"
  ],
  [
    "<p>Simplify and standardize conversions between C\n            strings and <code class=\"type\">text</code> datums, by\n            providing common functions for the purpose (Brendan\n            Jurd, Tom)</p>"
  ],
  [
    "<p>Clean up the <code class=\"filename\">include/catalog/</code> header files so that\n            frontend programs can include them without including\n            <code class=\"filename\">postgres.h</code> (Zdenek\n            Kotala)</p>"
  ],
  [
    "<p>Make <code class=\"type\">name</code> char-aligned,\n            and suppress zero-padding of <code class=\"type\">name</code> entries in indexes (Tom)</p>"
  ],
  [
    "<p>Recover better if dynamically-loaded code executes\n            <code class=\"function\">exit()</code> (Tom)</p>"
  ],
  [
    "<p>Add a hook to let plug-ins monitor the executor\n            (Itagaki Takahiro)</p>"
  ],
  [
    "<p>Add a hook to allow the planner's statistics lookup\n            behavior to be overridden (Simon Riggs)</p>"
  ],
  [
    "<p>Add <code class=\"function\">shmem_startup_hook()</code> for custom\n            shared memory requirements (Tom)</p>"
  ],
  [
    "<p>Replace the index access method <code class=\"function\">amgetmulti</code> entry point with\n            <code class=\"function\">amgetbitmap</code>, and extend\n            the API for <code class=\"function\">amgettuple</code> to\n            support run-time determination of operator lossiness\n            (Heikki, Tom, Teodor)</p>",
    "<p>The API for GIN and GiST opclass <code class=\"function\">consistent</code> functions has been\n            extended as well.</p>"
  ],
  [
    "<p>Add support for partial-match searches in\n            <acronym class=\"acronym\">GIN</acronym> indexes (Teodor\n            Sigaev, Oleg Bartunov)</p>"
  ],
  [
    "<p>Replace <code class=\"structname\">pg_class</code>\n            column <code class=\"structfield\">reltriggers</code>\n            with boolean <code class=\"structfield\">relhastriggers</code> (Simon)</p>",
    "<p>Also remove unused <code class=\"structname\">pg_class</code> columns <code class=\"structfield\">relukeys</code>, <code class=\"structfield\">relfkeys</code>, and <code class=\"structfield\">relrefs</code>.</p>"
  ],
  [
    "<p>Add a <code class=\"structfield\">relistemp</code>\n            column to <code class=\"structname\">pg_class</code> to\n            ease identification of temporary tables (Tom)</p>"
  ],
  [
    "<p>Move platform <acronym class=\"acronym\">FAQ</acronym>s into the main documentation\n            (Peter)</p>"
  ],
  [
    "<p>Prevent parser input files from being built with any\n            conflicts (Peter)</p>"
  ],
  [
    "<p>Add support for the <code class=\"literal\">KOI8U</code> (Ukrainian) encoding (Peter)</p>"
  ],
  [
    "<p>Add Japanese message translations (Japan PostgreSQL\n            Users Group)</p>",
    "<p>This used to be maintained as a separate\n            project.</p>"
  ],
  [
    "<p>Fix problem when setting <code class=\"varname\">LC_MESSAGES</code> on <span class=\"application\">MSVC</span>-built systems (Hiroshi Inoue,\n            Hiroshi Saito, Magnus)</p>"
  ],
  [
    "<p>Add <code class=\"filename\">contrib/auto_explain</code> to automatically\n            run <code class=\"command\">EXPLAIN</code> on queries\n            exceeding a specified duration (Itagaki Takahiro,\n            Tom)</p>"
  ],
  [
    "<p>Add <code class=\"filename\">contrib/btree_gin</code>\n            to allow GIN indexes to handle more datatypes (Oleg,\n            Teodor)</p>"
  ],
  [
    "<p>Add <code class=\"filename\">contrib/citext</code> to\n            provide a case-insensitive, multibyte-aware text data\n            type (David Wheeler)</p>"
  ],
  [
    "<p>Add <code class=\"filename\">contrib/pg_stat_statements</code> for\n            server-wide tracking of statement execution statistics\n            (Itagaki Takahiro)</p>"
  ],
  [
    "<p>Add duration and query mode options to <code class=\"filename\">contrib/pgbench</code> (Itagaki\n            Takahiro)</p>"
  ],
  [
    "<p>Make <code class=\"filename\">contrib/pgbench</code>\n            use table names <code class=\"structname\">pgbench_accounts</code>, <code class=\"structname\">pgbench_branches</code>, <code class=\"structname\">pgbench_history</code>, and <code class=\"structname\">pgbench_tellers</code>, rather than just\n            <code class=\"structname\">accounts</code>, <code class=\"structname\">branches</code>, <code class=\"structname\">history</code>, and <code class=\"structname\">tellers</code> (Tom)</p>",
    "<p>This is to reduce the risk of accidentally\n            destroying real data by running <span class=\"application\">pgbench</span>.</p>"
  ],
  [
    "<p>Fix <code class=\"filename\">contrib/pgstattuple</code> to handle tables\n            and indexes with over 2 billion pages (Tatsuhito\n            Kasahara)</p>"
  ],
  [
    "<p>In <code class=\"filename\">contrib/fuzzystrmatch</code>, add a version\n            of the Levenshtein string-distance function that allows\n            the user to specify the costs of insertion, deletion,\n            and substitution (Volkan Yazici)</p>"
  ],
  [
    "<p>Make <code class=\"filename\">contrib/ltree</code>\n            support multibyte encodings (laser)</p>"
  ],
  [
    "<p>Enable <code class=\"filename\">contrib/dblink</code>\n            to use connection information stored in the SQL/MED\n            catalogs (Joe Conway)</p>"
  ],
  [
    "<p>Improve <code class=\"filename\">contrib/dblink</code>'s reporting of errors\n            from the remote server (Joe Conway)</p>"
  ],
  [
    "<p>Make <code class=\"filename\">contrib/dblink</code>\n            set <code class=\"varname\">client_encoding</code> to\n            match the local database's encoding (Joe Conway)</p>",
    "<p>This prevents encoding problems when communicating\n            with a remote database that uses a different\n            encoding.</p>"
  ],
  [
    "<p>Make sure <code class=\"filename\">contrib/dblink</code> uses a password\n            supplied by the user, and not accidentally taken from\n            the server's <code class=\"filename\">.pgpass</code> file\n            (Joe Conway)</p>",
    "<p>This is a minor security enhancement.</p>"
  ],
  [
    "<p>Add <code class=\"function\">fsm_page_contents()</code> to <code class=\"filename\">contrib/pageinspect</code> (Heikki)</p>"
  ],
  [
    "<p>Modify <code class=\"function\">get_raw_page()</code>\n            to support free space map (<code class=\"filename\">*_fsm</code>) files. Also update\n            <code class=\"filename\">contrib/pg_freespacemap</code>.</p>"
  ],
  [
    "<p>Add support for multibyte encodings to <code class=\"filename\">contrib/pg_trgm</code> (Teodor)</p>"
  ],
  [
    "<p>Rewrite <code class=\"filename\">contrib/intagg</code>\n            to use new functions <code class=\"function\">array_agg()</code> and <code class=\"function\">unnest()</code> (Tom)</p>"
  ],
  [
    "<p>Make <code class=\"filename\">contrib/pg_standby</code> recover all\n            available WAL before failover (Fujii Masao, Simon,\n            Heikki)</p>",
    "<p>To make this work safely, you now need to set the\n            new <code class=\"literal\">recovery_end_command</code>\n            option in <code class=\"filename\">recovery.conf</code>\n            to clean up the trigger file after failover.\n            <span class=\"application\">pg_standby</span> will no\n            longer remove the trigger file itself.</p>"
  ],
  [
    "<p><code class=\"filename\">contrib/pg_standby</code>'s\n            <code class=\"option\">-l</code> option is now a no-op,\n            because it is unsafe to use a symlink (Simon)</p>"
  ]
]