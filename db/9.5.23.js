[
  [
    "<p>Make contrib modules' installation scripts more secure (Tom Lane)</p>",
    "<p>Attacks similar to those described in CVE-2018-1058 could be carried out against an extension installation script, if the attacker can create objects in either the extension's target schema or the schema of some prerequisite extension. Since extensions often require superuser privilege to install, this can open a path to obtaining superuser privilege. To mitigate this risk, be more careful about the <tt class=\"VARNAME\">search_path</tt> used to run an installation script; disable <tt class=\"VARNAME\">check_function_bodies</tt> within the script; and fix catalog-adjustment queries used in some contrib modules to ensure they are secure. Also provide documentation to help third-party extension authors make their installation scripts secure. This is not a complete solution; extensions that depend on other extensions can still be at risk if installed carelessly. (CVE-2020-14350)</p>"
  ],
  [
    "<p>In logical replication walsender, fix failure to send feedback messages after sending a keepalive message (Álvaro Herrera)</p>",
    "<p>This is a relatively minor problem when using built-in logical replication, because the built-in walreceiver will send a feedback reply (which clears the incorrect state) fairly frequently anyway. But with some other replication systems, such as <span class=\"APPLICATION\">pglogical</span>, it causes significant performance issues.</p>"
  ],
  [
    "<p>Ensure the <code class=\"FUNCTION\">repeat()</code> function can be interrupted by query cancel (Joe Conway)</p>"
  ],
  [
    "<p>Undo double-quoting of index names in <tt class=\"COMMAND\">EXPLAIN</tt>'s non-text output formats (Tom Lane, Euler Taveira)</p>"
  ],
  [
    "<p>Fix timing of constraint revalidation in <tt class=\"COMMAND\">ALTER TABLE</tt> (David Rowley)</p>",
    "<p>If <tt class=\"COMMAND\">ALTER TABLE</tt> needs to fully rewrite the table's contents (for example, due to change of a column's data type) and also needs to scan the table to re-validate foreign keys or <tt class=\"LITERAL\">CHECK</tt> constraints, it sometimes did things in the wrong order, leading to odd errors such as <span class=\"QUOTE\">\"could not read block 0 in file \"base/nnnnn/nnnnn\": read only 0 of 8192 bytes\"</span>.</p>"
  ],
  [
    "<p>Cope with <tt class=\"LITERAL\">LATERAL</tt> references in restriction clauses attached to an un-flattened sub-<tt class=\"LITERAL\">SELECT</tt> in the <tt class=\"LITERAL\">FROM</tt> clause (Tom Lane)</p>",
    "<p>This oversight could result in assertion failures or crashes at query execution.</p>"
  ],
  [
    "<p>Avoid believing that a never-analyzed foreign table has zero tuples (Tom Lane)</p>",
    "<p>This primarily affected the planner's estimate of the number of groups that would be obtained by <tt class=\"LITERAL\">GROUP BY</tt>.</p>"
  ],
  [
    "<p>Improve error handling in the server's <tt class=\"FILENAME\">buffile</tt> module (Thomas Munro)</p>",
    "<p>Fix some cases where I/O errors were indistinguishable from reaching EOF, or were not reported at all. Also add details such as block numbers and byte counts where appropriate.</p>"
  ],
  [
    "<p>Fix conflict-checking anomalies in <tt class=\"LITERAL\">SERIALIZABLE</tt> isolation mode (Peter Geoghegan)</p>",
    "<p>If a concurrently-inserted tuple was updated by a different concurrent transaction, and neither tuple version was visible to the current transaction's snapshot, serialization conflict checking could draw the wrong conclusions about whether the tuple was relevant to the results of the current transaction. This could allow a serializable transaction to commit when it should have failed with a serialization error.</p>"
  ],
  [
    "<p>Avoid repeated marking of dead btree index entries as dead (Masahiko Sawada)</p>",
    "<p>While functionally harmless, this led to useless WAL traffic when checksums are enabled or <tt class=\"VARNAME\">wal_log_hints</tt> is on.</p>"
  ],
  [
    "<p>Fix failure of some code paths to acquire the correct lock before modifying <tt class=\"FILENAME\">pg_control</tt> (Nathan Bossart, Fujii Masao)</p>",
    "<p>This oversight could allow <tt class=\"FILENAME\">pg_control</tt> to be written out with an inconsistent checksum, possibly causing trouble later, including inability to restart the database if it crashed before the next <tt class=\"FILENAME\">pg_control</tt> update.</p>"
  ],
  [
    "<p>Fix errors in <code class=\"FUNCTION\">currtid()</code> and <code class=\"FUNCTION\">currtid2()</code> (Michael Paquier)</p>",
    "<p>These functions (which are undocumented and used only by ancient versions of the ODBC driver) contained coding errors that could result in crashes, or in confusing error messages such as <span class=\"QUOTE\">\"could not open file\"</span> when applied to a relation having no storage.</p>"
  ],
  [
    "<p>Avoid calling <code class=\"FUNCTION\">elog()</code> or <code class=\"FUNCTION\">palloc()</code> while holding a spinlock (Michael Paquier, Tom Lane)</p>",
    "<p>Logic associated with replication slots had several violations of this coding rule. While the odds of trouble are quite low, an error in the called function would lead to a stuck spinlock.</p>"
  ],
  [
    "<p>Report out-of-disk-space errors properly in <span class=\"APPLICATION\">pg_dump</span> and <span class=\"APPLICATION\">pg_basebackup</span> (Justin Pryzby, Tom Lane, Álvaro Herrera)</p>",
    "<p>Some code paths could produce silly reports like <span class=\"QUOTE\">\"could not write file: Success\"</span>.</p>"
  ],
  [
    "<p>Fix parallel restore of tables having both table-level privileges and per-column privileges (Tom Lane)</p>",
    "<p>The table-level privilege grants have to be applied first, but a parallel restore did not reliably order them that way; this could lead to <span class=\"QUOTE\">\"tuple concurrently updated\"</span> errors, or to disappearance of some per-column privilege grants. The fix for this is to include dependency links between such entries in the archive file, meaning that a new dump has to be taken with a corrected <span class=\"APPLICATION\">pg_dump</span> to ensure that the problem will not recur.</p>"
  ],
  [
    "<p>Ensure that <span class=\"APPLICATION\">pg_upgrade</span> runs with <tt class=\"VARNAME\">vacuum_defer_cleanup_age</tt> set to zero in the target cluster (Bruce Momjian)</p>",
    "<p>If the target cluster's configuration has been modified to set <tt class=\"VARNAME\">vacuum_defer_cleanup_age</tt> to a nonzero value, that prevented freezing of the system catalogs from working properly, which caused the upgrade to fail in confusing ways. Ensure that any such setting is overridden for the duration of the upgrade.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_recvlogical</span> to drain pending messages before exiting (Noah Misch)</p>",
    "<p>Without this, the replication sender might detect a send failure and exit without making the expected final update to the replication slot's LSN position. That led to re-transmitting data after the next connection. It was also possible to miss error messages sent after the last data that <span class=\"APPLICATION\">pg_recvlogical</span> wants to consume.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_rewind</span>'s handling of just-deleted files in the source data directory (Justin Pryzby, Michael Paquier)</p>",
    "<p>When working with an on-line source database, concurrent file deletions are possible, but <span class=\"APPLICATION\">pg_rewind</span> would get confused if deletion happened between seeing a file's directory entry and examining it with <code class=\"FUNCTION\">stat()</code>.</p>"
  ],
  [
    "<p>Make <span class=\"APPLICATION\">pg_test_fsync</span> use binary I/O mode on Windows (Michael Paquier)</p>",
    "<p>Previously it wrote the test file in text mode, which is not an accurate reflection of <span class=\"PRODUCTNAME\">PostgreSQL</span>'s actual usage.</p>"
  ],
  [
    "<p>Fix failure to initialize local state correctly in <tt class=\"FILENAME\">contrib/dblink</tt> (Joe Conway)</p>",
    "<p>With the right combination of circumstances, this could lead to <code class=\"FUNCTION\">dblink_close()</code> issuing an unexpected remote <tt class=\"COMMAND\">COMMIT</tt>.</p>"
  ],
  [
    "<p>Fix <tt class=\"FILENAME\">contrib/pgcrypto</tt>'s misuse of <code class=\"FUNCTION\">deflate()</code> (Tom Lane)</p>",
    "<p>The <code class=\"FUNCTION\">pgp_sym_encrypt</code> functions could produce incorrect compressed data due to mishandling of <span class=\"APPLICATION\">zlib</span>'s API requirements. We have no reports of this error manifesting with stock <span class=\"APPLICATION\">zlib</span>, but it can be seen when using IBM's <span class=\"APPLICATION\">zlibNX</span> implementation.</p>"
  ],
  [
    "<p>Fix corner case in decompression logic in <tt class=\"FILENAME\">contrib/pgcrypto</tt>'s <code class=\"FUNCTION\">pgp_sym_decrypt</code> functions (Kyotaro Horiguchi, Michael Paquier)</p>",
    "<p>A compressed stream can validly end with an empty packet, but the decompressor failed to handle this and would complain about corrupt data.</p>"
  ],
  [
    "<p>Use POSIX-standard <code class=\"FUNCTION\">strsignal()</code> in place of the BSD-ish <tt class=\"LITERAL\">sys_siglist[]</tt> (Tom Lane)</p>",
    "<p>This avoids build failures with very recent versions of <span class=\"APPLICATION\">glibc</span>.</p>"
  ],
  [
    "<p>Support building our NLS code with Microsoft Visual Studio 2015 or later (Juan José Santamaría Flecha, Davinder Singh, Amit Kapila)</p>"
  ],
  [
    "<p>Avoid possible failure of our MSVC install script when there is a file named <tt class=\"FILENAME\">configure</tt> several levels above the source code tree (Arnold Müller)</p>",
    "<p>This could confuse some logic that looked for <tt class=\"FILENAME\">configure</tt> to identify the top level of the source tree.</p>"
  ]
]