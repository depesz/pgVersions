[
  [
    "<p>Allow a standby server to accept read-only queries\n              (Simon Riggs, Heikki Linnakangas)</p>",
    "<p>This feature is called Hot Standby. There are new\n              <code class=\"filename\">postgresql.conf</code> and\n              <code class=\"filename\">recovery.conf</code> settings\n              to control this feature, as well as extensive\n              <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/hot-standby.html\" title=\"26.5.&#xA0;Hot Standby\">documentation</a>.</p>"
  ],
  [
    "<p>Allow write-ahead log (<acronym class=\"acronym\">WAL</acronym>) data to be streamed to a\n              standby server (Fujii Masao, Heikki Linnakangas)</p>",
    "<p>This feature is called Streaming Replication.\n              Previously <acronym class=\"acronym\">WAL</acronym>\n              data could be sent to standby servers only in units\n              of entire <acronym class=\"acronym\">WAL</acronym>\n              files (normally 16 megabytes each). Streaming\n              Replication eliminates this inefficiency and allows\n              updates on the master to be propagated to standby\n              servers with very little delay. There are new\n              <code class=\"filename\">postgresql.conf</code> and\n              <code class=\"filename\">recovery.conf</code> settings\n              to control this feature, as well as extensive\n              <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/warm-standby.html#STREAMING-REPLICATION\" title=\"26.2.5.&#xA0;Streaming Replication\">documentation</a>.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-admin.html#FUNCTIONS-RECOVERY-INFO-TABLE\" title=\"Table&#xA0;9.80.&#xA0;Recovery Information Functions\">\n              <code class=\"function\">pg_last_xlog_receive_location()</code></a>\n              and <code class=\"function\">pg_last_xlog_replay_location()</code>,\n              which can be used to monitor standby server\n              <acronym class=\"acronym\">WAL</acronym> activity\n              (Simon Riggs, Fujii Masao, Heikki Linnakangas)</p>"
  ],
  [
    "<p>Allow per-tablespace values to be set for\n              sequential and random page cost estimates\n              (<code class=\"varname\">seq_page_cost</code>/<code class=\"varname\">random_page_cost</code>) via <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-altertablespace.html\" title=\"ALTER TABLESPACE\"><code class=\"command\">ALTER\n              TABLESPACE ... SET/RESET</code></a> (Robert Haas)</p>"
  ],
  [
    "<p>Improve performance and reliability of\n              EvalPlanQual rechecks in join queries (Tom Lane)</p>",
    "<p><code class=\"command\">UPDATE</code>, <code class=\"command\">DELETE</code>, and <code class=\"command\">SELECT FOR UPDATE/SHARE</code> queries that\n              involve joins will now behave much better when\n              encountering freshly-updated rows.</p>"
  ],
  [
    "<p>Improve performance of <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-truncate.html\" title=\"TRUNCATE\"><code class=\"command\">TRUNCATE</code></a> when the table was\n              created or truncated earlier in the same transaction\n              (Tom Lane)</p>"
  ],
  [
    "<p>Improve performance of finding inheritance child\n              tables (Tom Lane)</p>"
  ],
  [
    "<p>Remove unnecessary <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/queries-table-expressions.html#QUERIES-JOIN\" title=\"7.2.1.1.&#xA0;Joined Tables\">outer joins</a> (Robert\n              Haas)</p>",
    "<p>Outer joins where the inner side is unique and not\n              referenced above the join are unnecessary and are\n              therefore now removed. This will accelerate many\n              automatically generated queries, such as those\n              created by object-relational mappers (ORMs).</p>"
  ],
  [
    "<p>Allow <code class=\"literal\">IS NOT NULL</code>\n              restrictions to use indexes (Tom Lane)</p>",
    "<p>This is particularly useful for finding\n              <code class=\"function\">MAX()</code>/<code class=\"function\">MIN()</code> values in indexes that\n              contain many null values.</p>"
  ],
  [
    "<p>Improve the optimizer's choices about when to use\n              materialize nodes, and when to use sorting versus\n              hashing for <code class=\"literal\">DISTINCT</code>\n              (Tom Lane)</p>"
  ],
  [
    "<p>Improve the optimizer's equivalence detection for\n              expressions involving <code class=\"type\">boolean</code> <code class=\"literal\">&lt;&gt;</code> operators (Tom Lane)</p>"
  ],
  [
    "<p>Use the same random seed every time GEQO plans a\n              query (Andres Freund)</p>",
    "<p>While the Genetic Query Optimizer (GEQO) still\n              selects random plans, it now always selects the same\n              random plans for identical queries, thus giving more\n              consistent performance. You can modify <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-query.html#GUC-GEQO-SEED\"><code class=\"varname\">\n              geqo_seed</code></a> to experiment with alternative\n              plans.</p>"
  ],
  [
    "<p>Improve GEQO plan selection (Tom Lane)</p>",
    "<p>This avoids the rare error <span class=\"quote\">&#x201C;<span class=\"quote\">failed to make a valid\n              plan</span>&#x201D;</span>, and should also improve planning\n              speed.</p>"
  ],
  [
    "<p>Improve <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-analyze.html\" title=\"ANALYZE\"><code class=\"command\">ANALYZE</code></a> to support\n              inheritance-tree statistics (Tom Lane)</p>",
    "<p>This is particularly useful for partitioned\n              tables. However, autovacuum does not yet\n              automatically re-analyze parent tables when child\n              tables change.</p>"
  ],
  [
    "<p>Improve <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/routine-vacuuming.html\" title=\"24.1.&#xA0;Routine Vacuuming\">autovacuum</a>'s\n              detection of when re-analyze is necessary (Tom\n              Lane)</p>"
  ],
  [
    "<p>Improve optimizer's estimation for\n              greater/less-than comparisons (Tom Lane)</p>",
    "<p>When looking up statistics for greater/less-than\n              comparisons, if the comparison value is in the first\n              or last histogram bucket, use an index (if available)\n              to fetch the current actual column minimum or\n              maximum. This greatly improves the accuracy of\n              estimates for comparison values near the ends of the\n              data range, particularly if the range is constantly\n              changing due to addition of new data.</p>"
  ],
  [
    "<p>Allow setting of number-of-distinct-values\n              statistics using <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-altertable.html\" title=\"ALTER TABLE\"><code class=\"command\">ALTER\n              TABLE</code></a> (Robert Haas)</p>",
    "<p>This allows users to override the estimated number\n              or percentage of distinct values for a column. This\n              statistic is normally computed by <code class=\"command\">ANALYZE</code>, but the estimate can be\n              poor, especially on tables with very large numbers of\n              rows.</p>"
  ],
  [
    "<p>Add support for <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/auth-methods.html#AUTH-RADIUS\" title=\"20.3.8.&#xA0;RADIUS Authentication\"><acronym class=\"acronym\">RADIUS</acronym></a> (Remote Authentication\n              Dial In User Service) authentication (Magnus\n              Hagander)</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/auth-methods.html#AUTH-LDAP\" title=\"20.3.7.&#xA0;LDAP Authentication\"><acronym class=\"acronym\">LDAP</acronym></a> (Lightweight Directory\n              Access Protocol) authentication to operate in\n              <span class=\"quote\">&#x201C;<span class=\"quote\">search/bind</span>&#x201D;</span> mode (Robert\n              Fleming, Magnus Hagander)</p>",
    "<p>This allows the user to be looked up first, then\n              the system uses the <acronym class=\"acronym\">DN</acronym> (Distinguished Name) returned\n              for that user.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/auth-pg-hba-conf.html\" title=\"20.1.&#xA0;The pg_hba.conf File\"><code class=\"literal\">samehost</code></a> and <code class=\"literal\">samenet</code> designations to <code class=\"filename\">pg_hba.conf</code> (Stef Walter)</p>",
    "<p>These match the server's <acronym class=\"acronym\">IP</acronym> address and subnet address\n              respectively.</p>"
  ],
  [
    "<p>Pass trusted SSL root certificate names to the\n              client so the client can return an appropriate client\n              certificate (Craig Ringer)</p>"
  ],
  [
    "<p>Add the ability for clients to set an <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/libpq-connect.html#LIBPQ-CONNECT-APPLICATION-NAME\">application\n              name</a>, which is displayed in <code class=\"structname\">pg_stat_activity</code> (Dave Page)</p>",
    "<p>This allows administrators to characterize\n              database traffic and troubleshoot problems by source\n              application.</p>"
  ],
  [
    "<p>Add a SQLSTATE option (<code class=\"literal\">%e</code>) to <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-logging.html#GUC-LOG-LINE-PREFIX\"><code class=\"varname\">\n              log_line_prefix</code></a> (Guillaume Smet)</p>",
    "<p>This allows users to compile statistics on errors\n              and messages by error code number.</p>"
  ],
  [
    "<p>Write to the Windows event log in <acronym class=\"acronym\">UTF16</acronym> encoding (Itagaki\n              Takahiro)</p>",
    "<p>Now there is true multilingual support for\n              PostgreSQL log messages on Windows.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/monitoring-stats.html#MONITORING-STATS-FUNCS-TABLE\" title=\"Table&#xA0;28.19.&#xA0;Additional Statistics Functions\">\n              <code class=\"function\">pg_stat_reset_shared('bgwriter')</code></a>\n              to reset the cluster-wide shared statistics for the\n              background writer (Greg Smith)</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/monitoring-stats.html#MONITORING-STATS-FUNCS-TABLE\" title=\"Table&#xA0;28.19.&#xA0;Additional Statistics Functions\">\n              <code class=\"function\">pg_stat_reset_single_table_counters()</code></a>\n              and <code class=\"function\">pg_stat_reset_single_function_counters()</code>\n              to allow resetting the statistics counters for\n              individual tables and functions (Magnus Hagander)</p>"
  ],
  [
    "<p>Allow setting of configuration parameters based on\n              <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-alterrole.html\" title=\"ALTER ROLE\">database/role combinations</a> (Alvaro\n              Herrera)</p>",
    "<p>Previously only per-database and per-role settings\n              were possible, not combinations. All role and\n              database settings are now stored in the new\n              <code class=\"structname\">pg_db_role_setting</code>\n              system catalog. A new <span class=\"application\">psql</span> command <code class=\"literal\">\\drds</code> shows these settings. The\n              legacy system views <code class=\"structname\">pg_roles</code>, <code class=\"structname\">pg_shadow</code>, and <code class=\"structname\">pg_user</code> do not show combination\n              settings, and therefore no longer completely\n              represent the configuration for a user or\n              database.</p>"
  ],
  [
    "<p>Add server parameter <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-connection.html#GUC-BONJOUR\"><code class=\"varname\">\n              bonjour</code></a>, which controls whether a\n              Bonjour-enabled server advertises itself via\n              <span class=\"productname\">Bonjour</span> (Tom\n              Lane)</p>",
    "<p>The default is off, meaning it does not advertise.\n              This allows packagers to distribute Bonjour-enabled\n              builds without worrying that individual users might\n              not want the feature.</p>"
  ],
  [
    "<p>Add server parameter <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-query.html#GUC-ENABLE-MATERIAL\"><code class=\"varname\">\n              enable_material</code></a>, which controls the use of\n              materialize nodes in the optimizer (Robert Haas)</p>",
    "<p>The default is on. When off, the optimizer will\n              not add materialize nodes purely for performance\n              reasons, though they will still be used when\n              necessary for correctness.</p>"
  ],
  [
    "<p>Change server parameter <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-logging.html#GUC-LOG-TEMP-FILES\"><code class=\"varname\">\n              log_temp_files</code></a> to use default file size\n              units of kilobytes (Robert Haas)</p>",
    "<p>Previously this setting was interpreted in bytes\n              if no units were specified.</p>"
  ],
  [
    "<p>Log changes of parameter values when <code class=\"filename\">postgresql.conf</code> is reloaded (Peter\n              Eisentraut)</p>",
    "<p>This lets administrators and security staff audit\n              changes of database settings, and is also very\n              convenient for checking the effects of <code class=\"filename\">postgresql.conf</code> edits.</p>"
  ],
  [
    "<p>Properly enforce superuser permissions for custom\n              server parameters (Tom Lane)</p>",
    "<p>Non-superusers can no longer issue <code class=\"command\">ALTER ROLE</code>/<code class=\"command\">DATABASE SET</code> for parameters that are\n              not currently known to the server. This allows the\n              server to correctly check that superuser-only\n              parameters are only set by superusers. Previously,\n              the <code class=\"literal\">SET</code> would be allowed\n              and then ignored at session start, making\n              superuser-only custom parameters much less useful\n              than they should be.</p>"
  ],
  [
    "<p>Perform <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-select.html#SQL-FOR-UPDATE-SHARE\" title=\"The Locking Clause\"><code class=\"command\">SELECT FOR\n            UPDATE</code>/<code class=\"literal\">SHARE</code></a>\n            processing after applying <code class=\"literal\">LIMIT</code>, so the number of rows returned\n            is always predictable (Tom Lane)</p>",
    "<p>Previously, changes made by concurrent transactions\n            could cause a <code class=\"command\">SELECT FOR\n            UPDATE</code> to unexpectedly return fewer rows than\n            specified by its <code class=\"literal\">LIMIT</code>.\n            <code class=\"literal\">FOR UPDATE</code> in combination\n            with <code class=\"literal\">ORDER BY</code> can still\n            produce surprising results, but that can be corrected\n            by placing <code class=\"literal\">FOR UPDATE</code> in a\n            subquery.</p>"
  ],
  [
    "<p>Allow mixing of traditional and SQL-standard\n            <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-select.html#SQL-LIMIT\" title=\"LIMIT Clause\"><code class=\"literal\">LIMIT</code>/<code class=\"literal\">OFFSET</code></a> syntax (Tom Lane)</p>"
  ],
  [
    "<p>Extend the supported frame options in <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-select.html#SQL-WINDOW\" title=\"WINDOW Clause\">window functions</a> (Hitoshi\n            Harada)</p>",
    "<p>Frames can now start with <code class=\"literal\">CURRENT ROW</code>, and the <code class=\"literal\">ROWS <em class=\"replaceable\"><code>n</code></em>\n            PRECEDING</code>/<code class=\"literal\">FOLLOWING</code>\n            options are now supported.</p>"
  ],
  [
    "<p>Make <code class=\"command\">SELECT INTO</code> and\n            <code class=\"command\">CREATE TABLE AS</code> return row\n            counts to the client in their command tags (Boszormenyi\n            Zoltan)</p>",
    "<p>This can save an entire round-trip to the client,\n            allowing result counts and pagination to be calculated\n            without an additional <code class=\"command\">COUNT</code> query.</p>"
  ],
  [
    "<p>Support Unicode surrogate pairs (dual 16-bit\n              representation) in <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-syntax-lexical.html#SQL-SYNTAX-STRINGS-UESCAPE\" title=\"4.1.2.3.&#xA0;String Constants with Unicode Escapes\">\n              <code class=\"literal\">U&amp;</code></a> strings and\n              identifiers (Peter Eisentraut)</p>"
  ],
  [
    "<p>Support Unicode escapes in <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-syntax-lexical.html#SQL-SYNTAX-STRINGS-ESCAPE\" title=\"4.1.2.2.&#xA0;String Constants with C-style Escapes\">\n              <code class=\"literal\">E'...'</code></a> strings\n              (Marko Kreen)</p>"
  ],
  [
    "<p>Speed up <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-createdatabase.html\" title=\"CREATE DATABASE\"><code class=\"command\">CREATE\n            DATABASE</code></a> by deferring flushes to disk\n            (Andres Freund, Greg Stark)</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-comment.html\" title=\"COMMENT\">comments</a> on columns of tables, views, and\n            composite types only, not other relation types such as\n            indexes and <acronym class=\"acronym\">TOAST</acronym>\n            tables (Tom Lane)</p>"
  ],
  [
    "<p>Allow the creation of <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-createtype.html#SQL-CREATETYPE-ENUM\" title=\"Enumerated Types\">enumerated types</a> containing no\n            values (Bruce Momjian)</p>"
  ],
  [
    "<p>Let values of columns having storage type\n            <code class=\"literal\">MAIN</code> remain on the main\n            heap page unless the row cannot fit on a page (Kevin\n            Grittner)</p>",
    "<p>Previously <code class=\"literal\">MAIN</code> values\n            were forced out to <acronym class=\"acronym\">TOAST</acronym> tables until the row size was\n            less than one-quarter of the page size.</p>"
  ],
  [
    "<p>Implement <code class=\"literal\">IF EXISTS</code>\n              for <code class=\"literal\">ALTER TABLE DROP\n              COLUMN</code> and <code class=\"literal\">ALTER TABLE\n              DROP CONSTRAINT</code> (Andres Freund)</p>"
  ],
  [
    "<p>Allow <code class=\"command\">ALTER TABLE</code>\n              commands that rewrite tables to skip <acronym class=\"acronym\">WAL</acronym> logging (Itagaki\n              Takahiro)</p>",
    "<p>Such operations either produce a new copy of the\n              table or are rolled back, so <acronym class=\"acronym\">WAL</acronym> archiving can be skipped,\n              unless running in continuous archiving mode. This\n              reduces I/O overhead and improves performance.</p>"
  ],
  [
    "<p>Fix failure of <code class=\"literal\">ALTER TABLE\n              <em class=\"replaceable\"><code>table</code></em> ADD\n              COLUMN <em class=\"replaceable\"><code>col</code></em>\n              serial</code> when done by non-owner of table (Tom\n              Lane)</p>"
  ],
  [
    "<p>Add support for copying <code class=\"literal\">COMMENTS</code> and <code class=\"literal\">STORAGE</code> settings in <code class=\"command\">CREATE TABLE ... LIKE</code> commands\n              (Itagaki Takahiro)</p>"
  ],
  [
    "<p>Add a shortcut for copying all properties in\n              <code class=\"command\">CREATE TABLE ... LIKE</code>\n              commands (Itagaki Takahiro)</p>"
  ],
  [
    "<p>Add the SQL-standard <code class=\"literal\">CREATE\n              TABLE ... OF <em class=\"replaceable\"><code>type</code></em></code> command\n              (Peter Eisentraut)</p>",
    "<p>This allows creation of a table that matches an\n              existing composite type. Additional constraints and\n              defaults can be specified in the command.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-createtable.html#SQL-CREATETABLE-COMPATIBILITY\" title=\"Compatibility\">deferrable unique\n              constraints</a> (Dean Rasheed)</p>",
    "<p>This allows mass updates, such as <code class=\"literal\">UPDATE tab SET col = col + 1</code>, to\n              work reliably on columns that have unique indexes or\n              are marked as primary keys. If the constraint is\n              specified as <code class=\"literal\">DEFERRABLE</code>\n              it will be checked at the end of the statement,\n              rather than after each row is updated. The constraint\n              check can also be deferred until the end of the\n              current transaction, allowing such updates to be\n              spread over multiple SQL commands.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/ddl-constraints.html#DDL-CONSTRAINTS-EXCLUSION\" title=\"5.3.6.&#xA0;Exclusion Constraints\">exclusion\n              constraints</a> (Jeff Davis)</p>",
    "<p>Exclusion constraints generalize uniqueness\n              constraints by allowing arbitrary comparison\n              operators, not just equality. They are created with\n              the <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-createtable.html#SQL-CREATETABLE-EXCLUDE\"><code class=\"command\">\n              CREATE TABLE CONSTRAINT ... EXCLUDE</code></a>\n              clause. The most common use of exclusion constraints\n              is to specify that column entries must not overlap,\n              rather than simply not be equal. This is useful for\n              time periods and other ranges, as well as arrays.\n              This feature enhances checking of data integrity for\n              many calendaring, time-management, and scientific\n              applications.</p>"
  ],
  [
    "<p>Improve uniqueness-constraint violation error\n              messages to report the values causing the failure\n              (Itagaki Takahiro)</p>",
    "<p>For example, a uniqueness constraint violation\n              might now report <code class=\"literal\">Key (x)=(2)\n              already exists</code>.</p>"
  ],
  [
    "<p>Add the ability to make mass permission changes\n              across a whole schema using the new <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-grant.html\" title=\"GRANT\"><code class=\"command\">GRANT</code>/<code class=\"command\">REVOKE\n              IN SCHEMA</code></a> clause (Petr Jelinek)</p>",
    "<p>This simplifies management of object permissions\n              and makes it easier to utilize database roles for\n              application data security.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-alterdefaultprivileges.html\" title=\"ALTER DEFAULT PRIVILEGES\"><code class=\"command\">ALTER DEFAULT PRIVILEGES</code></a> command\n              to control privileges of objects created later (Petr\n              Jelinek)</p>",
    "<p>This greatly simplifies the assignment of object\n              privileges in a complex database application. Default\n              privileges can be set for tables, views, sequences,\n              and functions. Defaults may be assigned on a\n              per-schema basis, or database-wide.</p>"
  ],
  [
    "<p>Add the ability to control large object (BLOB)\n              permissions with <code class=\"command\">GRANT</code>/<code class=\"command\">REVOKE</code> (KaiGai Kohei)</p>",
    "<p>Formerly, any database user could read or modify\n              any large object. Read and write permissions can now\n              be granted and revoked per large object, and the\n              ownership of large objects is tracked.</p>"
  ],
  [
    "<p>Make <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-listen.html\" title=\"LISTEN\"><code class=\"command\">LISTEN</code></a>/<a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-notify.html\" title=\"NOTIFY\"><code class=\"command\">NOTIFY</code></a> store pending events in a\n            memory queue, rather than in a system table (Joachim\n            Wieland)</p>",
    "<p>This substantially improves performance, while\n            retaining the existing features of transactional\n            support and guaranteed delivery.</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-notify.html\" title=\"NOTIFY\"><code class=\"command\">NOTIFY</code></a> to\n            pass an optional <span class=\"quote\">&#x201C;<span class=\"quote\">payload</span>&#x201D;</span> string to listeners\n            (Joachim Wieland)</p>",
    "<p>This greatly improves the usefulness of <code class=\"command\">LISTEN</code>/<code class=\"command\">NOTIFY</code> as a general-purpose event\n            queue system.</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-cluster.html\" title=\"CLUSTER\"><code class=\"command\">CLUSTER</code></a> on\n            all per-database system catalogs (Tom Lane)</p>",
    "<p>Shared catalogs still cannot be clustered.</p>"
  ],
  [
    "<p>Accept <code class=\"literal\">COPY ... CSV FORCE\n              QUOTE *</code> (Itagaki Takahiro)</p>",
    "<p>Now <code class=\"literal\">*</code> can be used as\n              shorthand for <span class=\"quote\">&#x201C;<span class=\"quote\">all columns</span>&#x201D;</span> in the\n              <code class=\"literal\">FORCE QUOTE</code> clause.</p>"
  ],
  [
    "<p>Add new <code class=\"command\">COPY</code> syntax\n              that allows options to be specified inside\n              parentheses (Robert Haas, Emmanuel Cecchet)</p>",
    "<p>This allows greater flexibility for future\n              <code class=\"command\">COPY</code> options. The old\n              syntax is still supported, but only for pre-existing\n              options.</p>"
  ],
  [
    "<p>Allow <code class=\"command\">EXPLAIN</code> to\n              output in <acronym class=\"acronym\">XML</acronym>,\n              <acronym class=\"acronym\">JSON</acronym>, or\n              <acronym class=\"acronym\">YAML</acronym> format\n              (Robert Haas, Greg Sabino Mullane)</p>",
    "<p>The new output formats are easily\n              machine-readable, supporting the development of new\n              tools for analysis of <code class=\"command\">EXPLAIN</code> output.</p>"
  ],
  [
    "<p>Add new <code class=\"literal\">BUFFERS</code>\n              option to report query buffer usage during\n              <code class=\"command\">EXPLAIN ANALYZE</code> (Itagaki\n              Takahiro)</p>",
    "<p>This allows better query profiling for individual\n              queries. Buffer usage is no longer reported in the\n              output for <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-statistics.html#RUNTIME-CONFIG-STATISTICS-MONITOR\" title=\"19.9.2.&#xA0;Statistics Monitoring\">log_statement_stats</a>\n              and related settings.</p>"
  ],
  [
    "<p>Add hash usage information to <code class=\"command\">EXPLAIN</code> output (Robert Haas)</p>"
  ],
  [
    "<p>Add new <code class=\"command\">EXPLAIN</code>\n              syntax that allows options to be specified inside\n              parentheses (Robert Haas)</p>",
    "<p>This allows greater flexibility for future\n              <code class=\"command\">EXPLAIN</code> options. The old\n              syntax is still supported, but only for pre-existing\n              options.</p>"
  ],
  [
    "<p>Change <code class=\"command\">VACUUM FULL</code> to\n              rewrite the entire table and rebuild its indexes,\n              rather than moving individual rows around to compact\n              space (Itagaki Takahiro, Tom Lane)</p>",
    "<p>The previous method was usually slower and caused\n              index bloat. Note that the new method will use more\n              disk space transiently during <code class=\"command\">VACUUM FULL</code>; potentially as much as\n              twice the space normally occupied by the table and\n              its indexes.</p>"
  ],
  [
    "<p>Add new <code class=\"command\">VACUUM</code> syntax\n              that allows options to be specified inside\n              parentheses (Itagaki Takahiro)</p>",
    "<p>This allows greater flexibility for future\n              <code class=\"command\">VACUUM</code> options. The old\n              syntax is still supported, but only for pre-existing\n              options.</p>"
  ],
  [
    "<p>Allow an index to be named automatically by\n              omitting the index name in <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-createindex.html\" title=\"CREATE INDEX\"><code class=\"command\">CREATE\n              INDEX</code></a> (Tom Lane)</p>"
  ],
  [
    "<p>By default, multicolumn indexes are now named\n              after all their columns; and index expression columns\n              are now named based on their expressions (Tom\n              Lane)</p>"
  ],
  [
    "<p>Reindexing shared system catalogs is now fully\n              transactional and crash-safe (Tom Lane)</p>",
    "<p>Formerly, reindexing a shared index was only\n              allowed in standalone mode, and a crash during the\n              operation could leave the index in worse condition\n              than it was before.</p>"
  ],
  [
    "<p>Add <code class=\"literal\">point_ops</code>\n              operator class for <acronym class=\"acronym\">GiST</acronym> (Teodor Sigaev)</p>",
    "<p>This feature permits <acronym class=\"acronym\">GiST</acronym> indexing of <code class=\"type\">point</code> columns. The index can be used\n              for several types of queries such as <em class=\"replaceable\"><code>point</code></em> <code class=\"literal\">&lt;@</code> <em class=\"replaceable\"><code>polygon</code></em> (point is in\n              polygon). This should make many <span class=\"productname\">PostGIS</span> queries faster.</p>"
  ],
  [
    "<p>Use red-black binary trees for <acronym class=\"acronym\">GIN</acronym> index creation (Teodor\n              Sigaev)</p>",
    "<p>Red-black trees are self-balancing. This avoids\n              slowdowns in cases where the input is in nonrandom\n              order.</p>"
  ],
  [
    "<p>Allow <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/datatype-binary.html\" title=\"8.4.&#xA0;Binary Data Types\"><code class=\"type\">bytea</code></a> values to be written in hex\n            notation (Peter Eisentraut)</p>",
    "<p>The server parameter <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-client.html#GUC-BYTEA-OUTPUT\"><code class=\"varname\">\n            bytea_output</code></a> controls whether hex or\n            traditional format is used for <code class=\"type\">bytea</code> output. Libpq's <code class=\"function\">PQescapeByteaConn()</code> function\n            automatically uses the hex format when connected to\n            <span class=\"productname\">PostgreSQL</span> 9.0 or\n            newer servers. However, pre-9.0 libpq versions will not\n            correctly process hex format from newer servers.</p>",
    "<p>The new hex format will be directly compatible with\n            more applications that use binary data, allowing them\n            to store and retrieve it without extra conversion. It\n            is also significantly faster to read and write than the\n            traditional format.</p>"
  ],
  [
    "<p>Allow server parameter <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/runtime-config-client.html#GUC-EXTRA-FLOAT-DIGITS\">extra_float_digits</a>\n            to be increased to <code class=\"literal\">3</code> (Tom\n            Lane)</p>",
    "<p>The previous maximum <code class=\"varname\">extra_float_digits</code> setting was\n            <code class=\"literal\">2</code>. There are cases where 3\n            digits are needed to dump and restore <code class=\"type\">float4</code> values exactly. <span class=\"application\">pg_dump</span> will now use the setting\n            of 3 when dumping from a server that allows it.</p>"
  ],
  [
    "<p>Tighten input checking for <code class=\"type\">int2vector</code> values (Caleb Welton)</p>"
  ],
  [
    "<p>Add prefix support in <code class=\"literal\">synonym</code> dictionaries (Teodor\n              Sigaev)</p>"
  ],
  [
    "<p>Add <em class=\"firstterm\">filtering</em>\n              dictionaries (Teodor Sigaev)</p>",
    "<p>Filtering dictionaries allow tokens to be modified\n              then passed to subsequent dictionaries.</p>"
  ],
  [
    "<p>Allow underscores in email-address tokens (Teodor\n              Sigaev)</p>"
  ],
  [
    "<p>Use more standards-compliant rules for parsing\n              <acronym class=\"acronym\">URL</acronym> tokens (Tom\n              Lane)</p>"
  ],
  [
    "<p>Allow function calls to supply parameter names and\n            match them to named parameters in the function\n            definition (Pavel Stehule)</p>",
    "<p>For example, if a function is defined to take\n            parameters <code class=\"literal\">a</code> and\n            <code class=\"literal\">b</code>, it can be called with\n            <code class=\"literal\">func(a := 7, b := 12)</code> or\n            <code class=\"literal\">func(b := 12, a := 7)</code>.</p>"
  ],
  [
    "<p>Support locale-specific <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-matching.html#FUNCTIONS-POSIX-REGEXP\" title=\"9.7.3.&#xA0;POSIX Regular Expressions\">regular\n            expression</a> processing with <acronym class=\"acronym\">UTF-8</acronym> server encoding (Tom\n            Lane)</p>",
    "<p>Locale-specific regular expression functionality\n            includes case-insensitive matching and locale-specific\n            character classes. Previously, these features worked\n            correctly for non-<acronym class=\"acronym\">ASCII</acronym> characters only if the\n            database used a single-byte server encoding (such as\n            LATIN1). They will still misbehave in multi-byte\n            encodings other than <acronym class=\"acronym\">UTF-8</acronym>.</p>"
  ],
  [
    "<p>Add support for scientific notation in <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-formatting.html\" title=\"9.8.&#xA0;Data Type Formatting Functions\"><code class=\"function\">to_char()</code></a> (<a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-formatting.html#FUNCTIONS-FORMATTING-NUMERIC-TABLE\" title=\"Table&#xA0;9.26.&#xA0;Template Patterns for Numeric Formatting\"><code class=\"literal\">EEEE</code>\n            specification</a>) (Pavel Stehule, Brendan Jurd)</p>"
  ],
  [
    "<p>Make <code class=\"function\">to_char()</code> honor\n            <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-formatting.html#FUNCTIONS-FORMATTING-DATETIMEMOD-TABLE\" title=\"Table&#xA0;9.25.&#xA0;Template Pattern Modifiers for Date/Time Formatting\">\n            <code class=\"literal\">FM</code></a> (fill mode) in\n            <code class=\"literal\">Y</code>, <code class=\"literal\">YY</code>, and <code class=\"literal\">YYY</code> specifications (Bruce Momjian, Tom\n            Lane)</p>",
    "<p>It was already honored by <code class=\"literal\">YYYY</code>.</p>"
  ],
  [
    "<p>Fix <code class=\"function\">to_char()</code> to\n            output localized numeric and monetary strings in the\n            correct encoding on <span class=\"productname\">Windows</span> (Hiroshi Inoue, Itagaki\n            Takahiro, Bruce Momjian)</p>"
  ],
  [
    "<p>Correct calculations of <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-geometry.html#FUNCTIONS-GEOMETRY-OP-TABLE\" title=\"Table&#xA0;9.33.&#xA0;Geometric Operators\"><span class=\"quote\">\n            &#x201C;<span class=\"quote\">overlaps</span>&#x201D;</span></a> and\n            <span class=\"quote\">&#x201C;<span class=\"quote\">contains</span>&#x201D;</span> operations for polygons\n            (Teodor Sigaev)</p>",
    "<p>The polygon <code class=\"literal\">&amp;&amp;</code>\n            (overlaps) operator formerly just checked to see if the\n            two polygons' bounding boxes overlapped. It now does a\n            more correct check. The polygon <code class=\"literal\">@&gt;</code> and <code class=\"literal\">&lt;@</code> (contains/contained by)\n            operators formerly checked to see if one polygon's\n            vertexes were all contained in the other; this can\n            wrongly report <span class=\"quote\">&#x201C;<span class=\"quote\">true</span>&#x201D;</span> for some non-convex\n            polygons. Now they check that all line segments of one\n            polygon are contained in the other.</p>"
  ],
  [
    "<p>Allow aggregate functions to use <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-expressions.html#SYNTAX-AGGREGATES\" title=\"4.2.7.&#xA0;Aggregate Expressions\"><code class=\"literal\">ORDER BY</code></a> (Andrew Gierth)</p>",
    "<p>For example, this is now supported: <code class=\"literal\">array_agg(a ORDER BY b)</code>. This is\n              useful with aggregates for which the order of input\n              values is significant, and eliminates the need to use\n              a nonstandard subquery to determine the ordering.</p>"
  ],
  [
    "<p>Multi-argument aggregate functions can now use\n              <code class=\"literal\">DISTINCT</code> (Andrew\n              Gierth)</p>"
  ],
  [
    "<p>Add the <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-aggregate.html#FUNCTIONS-AGGREGATE-TABLE\" title=\"Table&#xA0;9.52.&#xA0;General-Purpose Aggregate Functions\">\n              <code class=\"function\">string_agg()</code></a>\n              aggregate function to combine values into a single\n              string (Pavel Stehule)</p>"
  ],
  [
    "<p>Aggregate functions that are called with\n              <code class=\"literal\">DISTINCT</code> are now passed\n              NULL values if the aggregate transition function is\n              not marked as <code class=\"literal\">STRICT</code>\n              (Andrew Gierth)</p>",
    "<p>For example, <code class=\"literal\">agg(DISTINCT\n              x)</code> might pass a NULL <code class=\"literal\">x</code> value to <code class=\"function\">agg()</code>. This is more consistent with\n              the behavior in non-<code class=\"literal\">DISTINCT</code> cases.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-binarystring.html#FUNCTIONS-BINARYSTRING-OTHER\" title=\"Table&#xA0;9.12.&#xA0;Other Binary String Functions\">\n              <code class=\"function\">get_bit()</code></a> and\n              <code class=\"function\">set_bit()</code> functions for\n              <code class=\"type\">bit</code> strings, mirroring\n              those for <code class=\"type\">bytea</code> (Leonardo\n              F)</p>"
  ],
  [
    "<p>Implement <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-string.html#FUNCTIONS-STRING-SQL\" title=\"Table&#xA0;9.8.&#xA0;SQL String Functions and Operators\">\n              <code class=\"function\">OVERLAY()</code></a> (replace)\n              for <code class=\"type\">bit</code> strings and\n              <code class=\"type\">bytea</code> (Leonardo F)</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-admin.html#FUNCTIONS-ADMIN-DBSIZE\" title=\"Table&#xA0;9.84.&#xA0;Database Object Size Functions\">\n              <code class=\"function\">pg_table_size()</code></a> and\n              <code class=\"function\">pg_indexes_size()</code> to\n              provide a more user-friendly interface to the\n              <code class=\"function\">pg_relation_size()</code>\n              function (Bernd Helmle)</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/functions-info.html#FUNCTIONS-INFO-ACCESS-TABLE\" title=\"Table&#xA0;9.61.&#xA0;Access Privilege Inquiry Functions\">\n              <code class=\"function\">has_sequence_privilege()</code></a> for\n              sequence permission checking (Abhijit Menon-Sen)</p>"
  ],
  [
    "<p>Update the <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/information-schema.html\" title=\"Chapter&#xA0;36.&#xA0;The Information Schema\">information_schema</a>\n              views to conform to SQL:2008 (Peter Eisentraut)</p>"
  ],
  [
    "<p>Make the <code class=\"literal\">information_schema</code> views correctly\n              display maximum octet lengths for <code class=\"type\">char</code> and <code class=\"type\">varchar</code> columns (Peter Eisentraut)</p>"
  ],
  [
    "<p>Speed up <code class=\"literal\">information_schema</code> privilege views\n              (Joachim Wieland)</p>"
  ],
  [
    "<p>Support execution of anonymous code blocks using\n              the <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-do.html\" title=\"DO\"><code class=\"command\">DO</code></a> statement\n              (Petr Jelinek, Joshua Tolley, Hannu Valtonen)</p>",
    "<p>This allows execution of server-side code without\n              the need to create and delete a temporary function\n              definition. Code can be executed in any language for\n              which the user has permissions to define a\n              function.</p>"
  ],
  [
    "<p>Implement SQL-standard-compliant <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-createtrigger.html\" title=\"CREATE TRIGGER\">per-column triggers</a> (Itagaki\n              Takahiro)</p>",
    "<p>Such triggers are fired only when the specified\n              column(s) are affected by the query, e.g. appear in\n              an <code class=\"command\">UPDATE</code>'s <code class=\"literal\">SET</code> list.</p>"
  ],
  [
    "<p>Add the <code class=\"literal\">WHEN</code> clause\n              to <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-createtrigger.html\" title=\"CREATE TRIGGER\"><code class=\"command\">CREATE\n              TRIGGER</code></a> to allow control over whether a\n              trigger is fired (Itagaki Takahiro)</p>",
    "<p>While the same type of check can always be\n              performed inside the trigger, doing it in an external\n              <code class=\"literal\">WHEN</code> clause can have\n              performance benefits.</p>"
  ],
  [
    "<p>Add the <code class=\"literal\">OR REPLACE</code>\n            clause to <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/sql-createlanguage.html\" title=\"CREATE LANGUAGE\"><code class=\"command\">CREATE\n            LANGUAGE</code></a> (Tom Lane)</p>",
    "<p>This is helpful to optionally install a language if\n            it does not already exist, and is particularly helpful\n            now that PL/pgSQL is installed by default.</p>"
  ],
  [
    "<p>Install PL/pgSQL by default (Bruce Momjian)</p>",
    "<p>The language can still be removed from a\n              particular database if the administrator has security\n              or performance concerns about making it\n              available.</p>"
  ],
  [
    "<p>Improve handling of cases where PL/pgSQL variable\n              names conflict with identifiers used in queries\n              within a function (Tom Lane)</p>",
    "<p>The default behavior is now to throw an error when\n              there is a conflict, so as to avoid surprising\n              behaviors. This can be modified, via the\n              configuration parameter <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/plpgsql-implementation.html#PLPGSQL-VAR-SUBST\" title=\"42.10.1.&#xA0;Variable Substitution\"><code class=\"varname\">\n              plpgsql.variable_conflict</code></a> or the\n              per-function option <code class=\"literal\">#variable_conflict</code>, to allow either\n              the variable or the query-supplied column to be used.\n              In any case PL/pgSQL will no longer attempt to\n              substitute variables in places where they would not\n              be syntactically valid.</p>"
  ],
  [
    "<p>Make PL/pgSQL use the main lexer, rather than its\n              own version (Tom Lane)</p>",
    "<p>This ensures accurate tracking of the main\n              system's behavior for details such as string\n              escaping. Some user-visible details, such as the set\n              of keywords considered reserved in PL/pgSQL, have\n              changed in consequence.</p>"
  ],
  [
    "<p>Avoid throwing an unnecessary error for an invalid\n              record reference (Tom Lane)</p>",
    "<p>An error is now thrown only if the reference is\n              actually fetched, rather than whenever the enclosing\n              expression is reached. For example, many people have\n              tried to do this in triggers:</p>",
    "<pre class=\"programlisting\">\n              if TG_OP = 'INSERT' and NEW.col1 = ... then</pre>",
    "<p>This will now actually work as expected.</p>"
  ],
  [
    "<p>Improve PL/pgSQL's ability to handle row types\n              with dropped columns (Pavel Stehule)</p>"
  ],
  [
    "<p>Allow input parameters to be assigned values\n              within PL/pgSQL functions (Steve Prentice)</p>",
    "<p>Formerly, input parameters were treated as being\n              declared <code class=\"literal\">CONST</code>, so the\n              function's code could not change their values. This\n              restriction has been removed to simplify porting of\n              functions from other DBMSes that do not impose the\n              equivalent restriction. An input parameter now acts\n              like a local variable initialized to the passed-in\n              value.</p>"
  ],
  [
    "<p>Improve error location reporting in PL/pgSQL (Tom\n              Lane)</p>"
  ],
  [
    "<p>Add <em class=\"replaceable\"><code>count</code></em> and\n              <code class=\"literal\">ALL</code> options to\n              <code class=\"command\">MOVE\n              FORWARD</code>/<code class=\"literal\">BACKWARD</code>\n              in PL/pgSQL (Pavel Stehule)</p>"
  ],
  [
    "<p>Allow PL/pgSQL's <code class=\"literal\">WHERE\n              CURRENT OF</code> to use a cursor variable (Tom\n              Lane)</p>"
  ],
  [
    "<p>Allow PL/pgSQL's <code class=\"command\">OPEN\n              <em class=\"replaceable\"><code>cursor</code></em> FOR\n              EXECUTE</code> to use parameters (Pavel Stehule,\n              Itagaki Takahiro)</p>",
    "<p>This is accomplished with a new <code class=\"literal\">USING</code> clause.</p>"
  ],
  [
    "<p>Add new PL/Perl functions: <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/plperl-builtins.html#PLPERL-UTILITY-FUNCTIONS\" title=\"44.3.2.&#xA0;Utility Functions in PL/Perl\"><code class=\"function\">\n              quote_literal()</code></a>, <code class=\"function\">quote_nullable()</code>, <code class=\"function\">quote_ident()</code>, <code class=\"function\">encode_bytea()</code>, <code class=\"function\">decode_bytea()</code>, <code class=\"function\">looks_like_number()</code>, <code class=\"function\">encode_array_literal()</code>,\n              <code class=\"function\">encode_array_constructor()</code>\n              (Tim Bunce)</p>"
  ],
  [
    "<p>Add server parameter <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/plperl-under-the-hood.html#GUC-PLPERL-ON-INIT\"><code class=\"varname\">\n              plperl.on_init</code></a> to specify a PL/Perl\n              initialization function (Tim Bunce)</p>",
    "<p><a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/plperl-under-the-hood.html#GUC-PLPERL-ON-PLPERL-INIT\">\n              <code class=\"varname\">plperl.on_plperl_init</code></a> and\n              <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/plperl-under-the-hood.html#GUC-PLPERL-ON-PLPERL-INIT\">\n              <code class=\"varname\">plperl.on_plperlu_init</code></a> are also\n              available for initialization that is specific to the\n              trusted or untrusted language respectively.</p>"
  ],
  [
    "<p>Support <code class=\"command\">END</code> blocks in\n              PL/Perl (Tim Bunce)</p>",
    "<p><code class=\"command\">END</code> blocks do not\n              currently allow database access.</p>"
  ],
  [
    "<p>Allow <code class=\"command\">use strict</code> in\n              PL/Perl (Tim Bunce)</p>",
    "<p>Perl <code class=\"literal\">strict</code> checks\n              can also be globally enabled with the new server\n              parameter <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/plperl-under-the-hood.html#GUC-PLPERL-USE-STRICT\"><code class=\"varname\">\n              plperl.use_strict</code></a>.</p>"
  ],
  [
    "<p>Allow <code class=\"command\">require</code> in\n              PL/Perl (Tim Bunce)</p>",
    "<p>This basically tests to see if the module is\n              loaded, and if not, generates an error. It will not\n              allow loading of modules that the administrator has\n              not preloaded via the initialization parameters.</p>"
  ],
  [
    "<p>Allow <code class=\"command\">use feature</code> in\n              PL/Perl if Perl version 5.10 or later is used (Tim\n              Bunce)</p>"
  ],
  [
    "<p>Verify that PL/Perl return values are valid in the\n              server encoding (Andrew Dunstan)</p>"
  ],
  [
    "<p>Add Unicode support in PL/Python (Peter\n              Eisentraut)</p>",
    "<p>Strings are automatically converted from/to the\n              server encoding as necessary.</p>"
  ],
  [
    "<p>Improve <code class=\"type\">bytea</code> support in\n              PL/Python (Caleb Welton)</p>",
    "<p><code class=\"type\">Bytea</code> values passed into\n              PL/Python are now represented as binary, rather than\n              the PostgreSQL <code class=\"type\">bytea</code> text\n              format. <code class=\"type\">Bytea</code> values\n              containing null bytes are now also output properly\n              from PL/Python. Passing of boolean, integer, and\n              float values was also improved.</p>"
  ],
  [
    "<p>Support <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/plpython-data.html#PLPYTHON-ARRAYS\" title=\"45.3.3.&#xA0;Arrays, Lists\">arrays</a> as parameters\n              and return values in PL/Python (Peter Eisentraut)</p>"
  ],
  [
    "<p>Improve mapping of SQL domains to Python types\n              (Peter Eisentraut)</p>"
  ],
  [
    "<p>Add <span class=\"application\">Python</span> 3\n              support to PL/Python (Peter Eisentraut)</p>",
    "<p>The new server-side language is called <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/plpython-python23.html\" title=\"45.1.&#xA0;Python 2 vs. Python 3\"><code class=\"literal\">plpython3u</code></a>. This cannot be used\n              in the same session with the <span class=\"application\">Python</span> 2 server-side\n              language.</p>"
  ],
  [
    "<p>Improve error location and exception reporting in\n              PL/Python (Peter Eisentraut)</p>"
  ],
  [
    "<p>Add an <code class=\"option\">--analyze-only</code>\n            option to <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/app-vacuumdb.html\" title=\"vacuumdb\"><code class=\"command\">vacuumdb</code></a>, to analyze without\n            vacuuming (Bruce Momjian)</p>"
  ],
  [
    "<p>Add support for quoting/escaping the values of\n              <span class=\"application\">psql</span> <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/app-psql.html#APP-PSQL-VARIABLES\" title=\"Variables\">variables</a> as SQL strings or\n              identifiers (Pavel Stehule, Robert Haas)</p>",
    "<p>For example, <code class=\"literal\">:'var'</code>\n              will produce the value of <code class=\"literal\">var</code> quoted and properly escaped as a\n              literal string, while <code class=\"literal\">:\"var\"</code> will produce its value quoted\n              and escaped as an identifier.</p>"
  ],
  [
    "<p>Ignore a leading UTF-8-encoded Unicode byte-order\n              marker in script files read by <span class=\"application\">psql</span> (Itagaki Takahiro)</p>",
    "<p>This is enabled when the client encoding is\n              <acronym class=\"acronym\">UTF-8</acronym>. It improves\n              compatibility with certain editors, mostly on\n              Windows, that insist on inserting such markers.</p>"
  ],
  [
    "<p>Fix <code class=\"command\">psql --file -</code> to\n              properly honor <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/app-psql.html#R1-APP-PSQL-3\" title=\"Options\"><code class=\"option\">--single-transaction</code></a> (Bruce\n              Momjian)</p>"
  ],
  [
    "<p>Avoid overwriting of <span class=\"application\">psql</span>'s command-line history when\n              two <span class=\"application\">psql</span> sessions\n              are run concurrently (Tom Lane)</p>"
  ],
  [
    "<p>Improve <span class=\"application\">psql</span>'s\n              tab completion support (Itagaki Takahiro)</p>"
  ],
  [
    "<p>Show <code class=\"literal\">\\timing</code> output\n              when it is enabled, regardless of <span class=\"quote\">&#x201C;<span class=\"quote\">quiet</span>&#x201D;</span>\n              mode (Peter Eisentraut)</p>"
  ],
  [
    "<p>Improve display of wrapped columns in\n                <span class=\"application\">psql</span> (Roger\n                Leigh)</p>",
    "<p>This behavior is now the default. The previous\n                formatting is available by using <code class=\"command\">\\pset linestyle old-ascii</code>.</p>"
  ],
  [
    "<p>Allow <span class=\"application\">psql</span> to\n                use fancy Unicode line-drawing characters via\n                <code class=\"command\">\\pset linestyle\n                unicode</code> (Roger Leigh)</p>"
  ],
  [
    "<p>Make <code class=\"command\">\\d</code> show child\n                tables that inherit from the specified parent\n                (Damien Clochard)</p>",
    "<p><code class=\"command\">\\d</code> shows only the\n                number of child tables, while <code class=\"command\">\\d+</code> shows the names of all child\n                tables.</p>"
  ],
  [
    "<p>Show definitions of index columns in\n                <code class=\"command\">\\d index_name</code> (Khee\n                Chin)</p>",
    "<p>The definition is useful for expression\n                indexes.</p>"
  ],
  [
    "<p>Show a view's defining query only in\n                <code class=\"command\">\\d+</code>, not in\n                <code class=\"command\">\\d</code> (Peter\n                Eisentraut)</p>",
    "<p>Always including the query was deemed overly\n                verbose.</p>"
  ],
  [
    "<p>Make <span class=\"application\">pg_dump</span>/<span class=\"application\">pg_restore</span> <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/app-pgdump.html#PG-DUMP-OPTIONS\" title=\"Options\"><code class=\"option\">--clean</code></a>\n              also remove large objects (Itagaki Takahiro)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_dump</span> to\n              properly dump large objects when <code class=\"literal\">standard_conforming_strings</code> is\n              enabled (Tom Lane)</p>",
    "<p>The previous coding could fail when dumping to an\n              archive file and then generating script output from\n              <span class=\"application\">pg_restore</span>.</p>"
  ],
  [
    "<p><span class=\"application\">pg_restore</span> now\n              emits large-object data in hex format when generating\n              script output (Tom Lane)</p>",
    "<p>This could cause compatibility problems if the\n              script is then loaded into a pre-9.0 server. To work\n              around that, restore directly to the server,\n              instead.</p>"
  ],
  [
    "<p>Allow <span class=\"application\">pg_dump</span> to\n              dump comments attached to columns of composite types\n              (Taro Minowa (Higepon))</p>"
  ],
  [
    "<p>Make <span class=\"application\">pg_dump</span>\n              <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/app-pgdump.html#PG-DUMP-OPTIONS\" title=\"Options\"><code class=\"option\">--verbose</code></a>\n              output the <span class=\"application\">pg_dump</span>\n              and server versions in text output mode (Jim Cox, Tom\n              Lane)</p>",
    "<p>These were already provided in custom output\n              mode.</p>"
  ],
  [
    "<p><span class=\"application\">pg_restore</span> now\n              complains if any command-line arguments remain after\n              the switches and optional file name (Tom Lane)</p>",
    "<p>Previously, it silently ignored any such\n              arguments.</p>"
  ],
  [
    "<p>Allow <span class=\"application\">pg_ctl</span> to\n              be used safely to start the <span class=\"application\">postmaster</span> during a system\n              reboot (Tom Lane)</p>",
    "<p>Previously, <span class=\"application\">pg_ctl</span>'s parent process could\n              have been mistakenly identified as a running\n              <span class=\"application\">postmaster</span> based on\n              a stale <span class=\"application\">postmaster</span>\n              lock file, resulting in a transient failure to start\n              the database.</p>"
  ],
  [
    "<p>Give <span class=\"application\">pg_ctl</span> the\n              ability to initialize the database (by invoking\n              <span class=\"application\">initdb</span>) (Zdenek\n              Kotala)</p>"
  ],
  [
    "<p>Add new <span class=\"application\">libpq</span>\n              functions <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/libpq-connect.html\" title=\"33.1.&#xA0;Database Connection Control Functions\"><code class=\"function\">\n              PQconnectdbParams()</code></a> and <code class=\"function\">PQconnectStartParams()</code> (Guillaume\n              Lelarge)</p>",
    "<p>These functions are similar to <code class=\"function\">PQconnectdb()</code> and <code class=\"function\">PQconnectStart()</code> except that they\n              accept a null-terminated array of connection options,\n              rather than requiring all options to be provided in a\n              single string.</p>"
  ],
  [
    "<p>Add <span class=\"application\">libpq</span>\n              functions <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/libpq-exec.html#LIBPQ-EXEC-ESCAPE-STRING\" title=\"33.3.4.&#xA0;Escaping Strings for Inclusion in SQL Commands\">\n              <code class=\"function\">PQescapeLiteral()</code></a>\n              and <code class=\"function\">PQescapeIdentifier()</code> (Robert\n              Haas)</p>",
    "<p>These functions return appropriately quoted and\n              escaped SQL string literals and identifiers. The\n              caller is not required to pre-allocate the string\n              result, as is required by <code class=\"function\">PQescapeStringConn()</code>.</p>"
  ],
  [
    "<p>Add support for a per-user service file (<a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/libpq-pgservice.html\" title=\"33.16.&#xA0;The Connection Service File\"><code class=\"filename\">.pg_service.conf</code></a>),\n              which is checked before the site-wide service file\n              (Peter Eisentraut)</p>"
  ],
  [
    "<p>Properly report an error if the specified\n              <span class=\"application\">libpq</span> service cannot\n              be found (Peter Eisentraut)</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/libpq-connect.html#LIBPQ-KEEPALIVES\">TCP keepalive\n              settings</a> in libpq (Tollef Fog Heen, Fujii Masao,\n              Robert Haas)</p>",
    "<p>Keepalive settings were already supported on the\n              server end of TCP connections.</p>"
  ],
  [
    "<p>Avoid extra system calls to block and unblock\n              <code class=\"literal\">SIGPIPE</code> in <span class=\"application\">libpq</span>, on platforms that offer\n              alternative methods (Jeremy Kerr)</p>"
  ],
  [
    "<p>When a <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/libpq-pgpass.html\" title=\"33.15.&#xA0;The Password File\"><code class=\"filename\">.pgpass</code></a>-supplied password\n              fails, mention where the password came from in the\n              error message (Bruce Momjian)</p>"
  ],
  [
    "<p>Load all SSL certificates given in the client\n              certificate file (Tom Lane)</p>",
    "<p>This improves support for indirectly-signed SSL\n              certificates.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/ecpg-descriptors.html\" title=\"35.7.&#xA0;Using Descriptor Areas\"><acronym class=\"acronym\">SQLDA</acronym></a> (SQL Descriptor Area)\n              support to <span class=\"application\">ecpg</span>\n              (Boszormenyi Zoltan)</p>"
  ],
  [
    "<p>Add the <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/ecpg-descriptors.html\" title=\"35.7.&#xA0;Using Descriptor Areas\"><code class=\"command\">DESCRIBE</code> [ <code class=\"literal\">OUTPUT</code> ]</a> statement to\n              <span class=\"application\">ecpg</span> (Boszormenyi\n              Zoltan)</p>"
  ],
  [
    "<p>Add an <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/ecpg-library.html\" title=\"35.11.&#xA0;Library Functions\">ECPGtransactionStatus</a>\n              function to return the current transaction status\n              (Bernd Helmle)</p>"
  ],
  [
    "<p>Add the <code class=\"literal\">string</code> data\n              type in <span class=\"application\">ecpg</span>\n              Informix-compatibility mode (Boszormenyi Zoltan)</p>"
  ],
  [
    "<p>Allow <span class=\"application\">ecpg</span> to use\n              <code class=\"literal\">new</code> and <code class=\"literal\">old</code> variable names without\n              restriction (Michael Meskes)</p>"
  ],
  [
    "<p>Allow <span class=\"application\">ecpg</span> to use\n              variable names in <code class=\"function\">free()</code> (Michael Meskes)</p>"
  ],
  [
    "<p>Make <code class=\"function\">ecpg_dynamic_type()</code> return zero for\n              non-SQL3 data types (Michael Meskes)</p>",
    "<p>Previously it returned the negative of the data\n              type OID. This could be confused with valid type\n              OIDs, however.</p>"
  ],
  [
    "<p>Support <code class=\"type\">long long</code> types\n              on platforms that already have 64-bit <code class=\"type\">long</code> (Michael Meskes)</p>"
  ],
  [
    "<p>Add out-of-scope cursor support in <span class=\"application\">ecpg</span>'s native mode\n                (Boszormenyi Zoltan)</p>",
    "<p>This allows <code class=\"command\">DECLARE</code>\n                to use variables that are not in scope when\n                <code class=\"command\">OPEN</code> is called. This\n                facility already existed in <span class=\"application\">ecpg</span>'s Informix-compatibility\n                mode.</p>"
  ],
  [
    "<p>Allow dynamic cursor names in <span class=\"application\">ecpg</span> (Boszormenyi Zoltan)</p>"
  ],
  [
    "<p>Allow <span class=\"application\">ecpg</span> to\n                use noise words <code class=\"literal\">FROM</code>\n                and <code class=\"literal\">IN</code> in <code class=\"command\">FETCH</code> and <code class=\"command\">MOVE</code> (Boszormenyi Zoltan)</p>"
  ],
  [
    "<p>Enable client thread safety by default (Bruce\n            Momjian)</p>",
    "<p>The thread-safety option can be disabled with\n            <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/install-procedure.html#CONFIGURE\" title=\"Configuration\"><code class=\"literal\">configure</code></a> <code class=\"option\">--disable-thread-safety</code>.</p>"
  ],
  [
    "<p>Add support for controlling the Linux out-of-memory\n            killer (Alex Hunsaker, Tom Lane)</p>",
    "<p>Now that <code class=\"filename\">/proc/self/oom_adj</code> allows disabling\n            of the <span class=\"productname\">Linux</span>\n            out-of-memory (<acronym class=\"acronym\">OOM</acronym>)\n            killer, it's recommendable to disable OOM kills for the\n            postmaster. It may then be desirable to re-enable OOM\n            kills for the postmaster's child processes. The new\n            compile-time option <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/kernel-resources.html#LINUX-MEMORY-OVERCOMMIT\" title=\"18.4.4.&#xA0;Linux Memory Overcommit\"><code class=\"literal\">LINUX_OOM_ADJ</code></a> allows the killer to\n            be reactivated for child processes.</p>"
  ],
  [
    "<p>New <code class=\"filename\">Makefile</code> targets\n              <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/install-procedure.html#BUILD\" title=\"Build\"><code class=\"literal\">world</code></a>,\n              <code class=\"literal\">install-world</code>, and\n              <code class=\"literal\">installcheck-world</code>\n              (Andrew Dunstan)</p>",
    "<p>These are similar to the existing <code class=\"literal\">all</code>, <code class=\"literal\">install</code>, and <code class=\"literal\">installcheck</code> targets, but they also\n              build the <acronym class=\"acronym\">HTML</acronym>\n              documentation, build and test <code class=\"filename\">contrib</code>, and test server-side\n              languages and <span class=\"application\">ecpg</span>.</p>"
  ],
  [
    "<p>Add data and documentation installation location\n              control to <acronym class=\"acronym\">PGXS</acronym>\n              Makefiles (Mark Cave-Ayland)</p>"
  ],
  [
    "<p>Add Makefile rules to build the <span class=\"productname\">PostgreSQL</span> documentation as a\n              single <acronym class=\"acronym\">HTML</acronym> file\n              or as a single plain-text file (Peter Eisentraut,\n              Bruce Momjian)</p>"
  ],
  [
    "<p>Support compiling on <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/install-windows.html\" title=\"Chapter&#xA0;17.&#xA0;Installation from Source Code on Windows\">\n              64-bit <span class=\"productname\">Windows</span></a>\n              and running in 64-bit mode (Tsutomu Yamada, Magnus\n              Hagander)</p>",
    "<p>This allows for large shared memory sizes on\n              <span class=\"productname\">Windows</span>.</p>"
  ],
  [
    "<p>Support server builds using <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/install-windows-full.html\" title=\"17.1.&#xA0;Building with Visual C++ or the Microsoft Windows SDK\">\n              <span class=\"productname\">Visual Studio\n              2008</span></a> (Magnus Hagander)</p>"
  ],
  [
    "<p>Distribute prebuilt documentation in a subdirectory\n            tree, rather than as tar archive files inside the\n            distribution tarball (Peter Eisentraut)</p>",
    "<p>For example, the prebuilt <acronym class=\"acronym\">HTML</acronym> documentation is now in\n            <code class=\"filename\">doc/src/sgml/html/</code>; the\n            manual pages are packaged similarly.</p>"
  ],
  [
    "<p>Make the server's lexer reentrant (Tom Lane)</p>",
    "<p>This was needed for use of the lexer by\n            PL/pgSQL.</p>"
  ],
  [
    "<p>Improve speed of memory allocation (Tom Lane, Greg\n            Stark)</p>"
  ],
  [
    "<p>User-defined constraint triggers now have entries in\n            <code class=\"structname\">pg_constraint</code> as well\n            as <code class=\"structname\">pg_trigger</code> (Tom\n            Lane)</p>",
    "<p>Because of this change, <code class=\"structname\">pg_constraint</code>.<code class=\"structfield\">pgconstrname</code> is now redundant and\n            has been removed.</p>"
  ],
  [
    "<p>Add system catalog columns <code class=\"structname\">pg_constraint</code>.<code class=\"structfield\">conindid</code> and <code class=\"structname\">pg_trigger</code>.<code class=\"structfield\">tgconstrindid</code> to better document\n            the use of indexes for constraint enforcement (Tom\n            Lane)</p>"
  ],
  [
    "<p>Allow multiple conditions to be communicated to\n            backends using a single operating system signal (Fujii\n            Masao)</p>",
    "<p>This allows new features to be added without a\n            platform-specific constraint on the number of signal\n            conditions.</p>"
  ],
  [
    "<p>Improve source code test coverage, including\n            <code class=\"filename\">contrib</code>, PL/Python, and\n            PL/Perl (Peter Eisentraut, Andrew Dunstan)</p>"
  ],
  [
    "<p>Remove the use of flat files for system table\n            bootstrapping (Tom Lane, Alvaro Herrera)</p>",
    "<p>This improves performance when using many roles or\n            databases, and eliminates some possible failure\n            conditions.</p>"
  ],
  [
    "<p>Automatically generate the initial contents of\n            <code class=\"structname\">pg_attribute</code> for\n            <span class=\"quote\">&#x201C;<span class=\"quote\">bootstrapped</span>&#x201D;</span> catalogs (John\n            Naylor)</p>",
    "<p>This greatly simplifies changes to these\n            catalogs.</p>"
  ],
  [
    "<p>Split the processing of <code class=\"command\">INSERT</code>/<code class=\"command\">UPDATE</code>/<code class=\"command\">DELETE</code> operations out of <code class=\"filename\">execMain.c</code> (Marko Tiikkaja)</p>",
    "<p>Updates are now executed in a separate ModifyTable\n            node. This change is necessary infrastructure for\n            future improvements.</p>"
  ],
  [
    "<p>Simplify translation of <span class=\"application\">psql</span>'s SQL help text (Peter\n            Eisentraut)</p>"
  ],
  [
    "<p>Reduce the lengths of some file names so that all\n            file paths in the distribution tarball are less than\n            100 characters (Tom Lane)</p>",
    "<p>Some decompression programs have problems with\n            longer file paths.</p>"
  ],
  [
    "<p>Add a new <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/errcodes-appendix.html#ERRCODES-TABLE\" title=\"Table&#xA0;A.1.&#xA0;PostgreSQL Error Codes\"><code class=\"literal\">\n            ERRCODE_INVALID_PASSWORD</code></a> <code class=\"literal\">SQLSTATE</code> error code (Bruce\n            Momjian)</p>"
  ],
  [
    "<p>With authors' permissions, remove the few remaining\n            personal source code copyright notices (Bruce\n            Momjian)</p>",
    "<p>The personal copyright notices were insignificant\n            but the community occasionally had to answer questions\n            about them.</p>"
  ],
  [
    "<p>Add new documentation <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/non-durability.html\" title=\"14.5.&#xA0;Non-Durable Settings\">section</a> about\n            running <span class=\"productname\">PostgreSQL</span> in\n            non-durable mode to improve performance (Bruce\n            Momjian)</p>"
  ],
  [
    "<p>Restructure the <acronym class=\"acronym\">HTML</acronym> documentation <code class=\"filename\">Makefile</code> rules to make their\n            dependency checks work correctly, avoiding unnecessary\n            rebuilds (Peter Eisentraut)</p>"
  ],
  [
    "<p>Use <span class=\"productname\">DocBook</span>\n            <acronym class=\"acronym\">XSL</acronym> stylesheets for\n            man page building, rather than <span class=\"productname\">Docbook2X</span> (Peter Eisentraut)</p>",
    "<p>This changes the set of tools needed to build the\n            man pages.</p>"
  ],
  [
    "<p>Improve PL/Perl code structure (Tim Bunce)</p>"
  ],
  [
    "<p>Improve error context reports in PL/Perl (Alexey\n            Klyukin)</p>"
  ],
  [
    "<p>Require <span class=\"application\">Autoconf</span>\n              2.63 to build <span class=\"application\">configure</span> (Peter Eisentraut)</p>"
  ],
  [
    "<p>Require <span class=\"application\">Flex</span>\n              2.5.31 or later to build from a <acronym class=\"acronym\">CVS</acronym> checkout (Tom Lane)</p>"
  ],
  [
    "<p>Require <span class=\"application\">Perl</span>\n              version 5.8 or later to build from a <acronym class=\"acronym\">CVS</acronym> checkout (John Naylor, Andrew\n              Dunstan)</p>"
  ],
  [
    "<p>Use a more modern <acronym class=\"acronym\">API</acronym> for <span class=\"application\">Bonjour</span> (Tom Lane)</p>",
    "<p>Bonjour support now requires <span class=\"productname\">macOS</span> 10.3 or later. The older\n              API has been deprecated by Apple.</p>"
  ],
  [
    "<p>Add spinlock support for the <span class=\"productname\">SuperH</span> architecture (Nobuhiro\n              Iwamatsu)</p>"
  ],
  [
    "<p>Allow non-<span class=\"application\">GCC</span>\n              compilers to use inline functions if they support\n              them (Kurt Harriman)</p>"
  ],
  [
    "<p>Remove support for platforms that don't have a\n              working 64-bit integer data type (Tom Lane)</p>"
  ],
  [
    "<p>Restructure use of <code class=\"literal\">LDFLAGS</code> to be more consistent across\n              platforms (Tom Lane)</p>",
    "<p><code class=\"literal\">LDFLAGS</code> is now used\n              for linking both executables and shared libraries,\n              and we add on <code class=\"literal\">LDFLAGS_EX</code>\n              when linking executables, or <code class=\"literal\">LDFLAGS_SL</code> when linking shared\n              libraries.</p>"
  ],
  [
    "<p>Make backend header files safe to include in\n              <span class=\"productname\">C++</span> (Kurt Harriman,\n              Peter Eisentraut)</p>",
    "<p>These changes remove keyword conflicts that\n              previously made <span class=\"productname\">C++</span>\n              usage difficult in backend code. However, there are\n              still other complexities when using <span class=\"productname\">C++</span> for backend functions.\n              <code class=\"literal\">extern \"C\" { }</code> is still\n              necessary in appropriate places, and memory\n              management and error handling are still\n              problematic.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/xaggr.html\" title=\"37.10.&#xA0;User-defined Aggregates\"><code class=\"function\">AggCheckCallContext()</code></a> for use\n              in detecting if a <span class=\"productname\">C</span>\n              function is being called as an aggregate (Hitoshi\n              Harada)</p>"
  ],
  [
    "<p>Change calling convention for <code class=\"function\">SearchSysCache()</code> and related\n              functions to avoid hard-wiring the maximum number of\n              cache keys (Robert Haas)</p>",
    "<p>Existing calls will still work for the moment, but\n              can be expected to break in 9.1 or later if not\n              converted to the new style.</p>"
  ],
  [
    "<p>Require calls of <code class=\"function\">fastgetattr()</code> and <code class=\"function\">heap_getattr()</code> backend macros to\n              provide a non-NULL fourth argument (Robert Haas)</p>"
  ],
  [
    "<p>Custom typanalyze functions should no longer rely\n              on <code class=\"structname\">VacAttrStats</code>.<code class=\"structfield\">attr</code> to determine the type of\n              data they will be passed (Tom Lane)</p>",
    "<p>This was changed to allow collection of statistics\n              on index columns for which the storage type is\n              different from the underlying column data type. There\n              are new fields that tell the actual datatype being\n              analyzed.</p>"
  ],
  [
    "<p>Add parser hooks for processing ColumnRef and\n              ParamRef nodes (Tom Lane)</p>"
  ],
  [
    "<p>Add a ProcessUtility hook so loadable modules can\n              control utility commands (Itagaki Takahiro)</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/pgupgrade.html\" title=\"pg_upgrade\"><code class=\"filename\">contrib/pg_upgrade</code></a> to support\n              in-place upgrades (Bruce Momjian)</p>",
    "<p>This avoids the requirement of dumping/reloading\n              the database when upgrading to a new major release of\n              PostgreSQL, thus reducing downtime by orders of\n              magnitude. It supports upgrades to 9.0 from\n              PostgreSQL 8.3 and 8.4.</p>"
  ],
  [
    "<p>Add support for preserving relation <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/catalog-pg-class.html\" title=\"51.11.&#xA0;pg_class\"><code class=\"structname\">relfilenode</code></a> values during\n              binary upgrades (Bruce Momjian)</p>"
  ],
  [
    "<p>Add support for preserving <code class=\"structname\">pg_type</code> and <code class=\"structname\">pg_enum</code> OIDs during binary\n              upgrades (Bruce Momjian)</p>"
  ],
  [
    "<p>Move data files within tablespaces into\n              <span class=\"productname\">PostgreSQL</span>-version-specific\n              subdirectories (Bruce Momjian)</p>",
    "<p>This simplifies binary upgrades.</p>"
  ],
  [
    "<p>Add multithreading option (<code class=\"option\">-j</code>) to <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/pgbench.html\" title=\"pgbench\"><code class=\"filename\">contrib/pgbench</code></a> (Itagaki\n            Takahiro)</p>",
    "<p>This allows multiple <acronym class=\"acronym\">CPU</acronym>s to be used by pgbench,\n            reducing the risk of pgbench itself becoming the test\n            bottleneck.</p>"
  ],
  [
    "<p>Add <code class=\"command\">\\shell</code> and\n            <code class=\"command\">\\setshell</code> meta commands to\n            <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/pgbench.html\" title=\"pgbench\"><code class=\"filename\">contrib/pgbench</code></a> (Michael\n            Paquier)</p>"
  ],
  [
    "<p>New features for <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/dict-xsyn.html\" title=\"F.13.&#xA0;dict_xsyn\"><code class=\"filename\">contrib/dict_xsyn</code></a> (Sergey\n            Karpov)</p>",
    "<p>The new options are <code class=\"literal\">matchorig</code>, <code class=\"literal\">matchsynonyms</code>, and <code class=\"literal\">keepsynonyms</code>.</p>"
  ],
  [
    "<p>Add full text dictionary <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/unaccent.html\" title=\"F.44.&#xA0;unaccent\"><code class=\"filename\">contrib/unaccent</code></a>\n            (Teodor Sigaev)</p>",
    "<p>This filtering dictionary removes accents from\n            letters, which makes full-text searches over multiple\n            languages much easier.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/contrib-dblink-get-notify.html\" title=\"dblink_get_notify\"><code class=\"function\">dblink_get_notify()</code></a> to\n            <code class=\"filename\">contrib/dblink</code> (Marcus\n            Kempe)</p>",
    "<p>This allows asynchronous notifications in\n            <span class=\"productname\">dblink</span>.</p>"
  ],
  [
    "<p>Improve <code class=\"filename\">contrib/dblink</code>'s handling of dropped\n            columns (Tom Lane)</p>",
    "<p>This affects <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/contrib-dblink-build-sql-insert.html\" title=\"dblink_build_sql_insert\"><code class=\"function\">dblink_build_sql_insert()</code></a> and\n            related functions. These functions now number columns\n            according to logical not physical column numbers.</p>"
  ],
  [
    "<p>Greatly increase <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/hstore.html\" title=\"F.17.&#xA0;hstore\"><code class=\"filename\">contrib/hstore</code></a>'s data length\n            limit, and add B-tree and hash support so <code class=\"literal\">GROUP BY</code> and <code class=\"literal\">DISTINCT</code> operations are possible on\n            <code class=\"type\">hstore</code> columns (Andrew\n            Gierth)</p>",
    "<p>New functions and operators were also added. These\n            improvements make <code class=\"type\">hstore</code> a\n            full-function key-value store embedded in <span class=\"productname\">PostgreSQL</span>.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/passwordcheck.html\" title=\"F.24.&#xA0;passwordcheck\"><code class=\"filename\">contrib/passwordcheck</code></a> to support\n            site-specific password strength policies (Laurenz\n            Albe)</p>",
    "<p>The source code of this module should be modified to\n            implement site-specific password policies.</p>"
  ],
  [
    "<p>Add <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/pgarchivecleanup.html\" title=\"pg_archivecleanup\"><code class=\"filename\">contrib/pg_archivecleanup</code></a> tool\n            (Simon Riggs)</p>",
    "<p>This is designed to be used in the <code class=\"literal\">archive_cleanup_command</code> server\n            parameter, to remove no-longer-needed archive\n            files.</p>"
  ],
  [
    "<p>Add query text to <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/auto-explain.html\" title=\"F.4.&#xA0;auto_explain\"><code class=\"filename\">contrib/auto_explain</code></a> output\n            (Andrew Dunstan)</p>"
  ],
  [
    "<p>Add buffer access counters to <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/pgstatstatements.html\" title=\"F.30.&#xA0;pg_stat_statements\"><code class=\"filename\">contrib/pg_stat_statements</code></a>\n            (Itagaki Takahiro)</p>"
  ],
  [
    "<p>Update <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/server-start.html\" title=\"18.3.&#xA0;Starting the Database Server\"><code class=\"filename\">contrib/start-scripts/linux</code></a> to\n            use <code class=\"filename\">/proc/self/oom_adj</code> to\n            disable the <a class=\"link\" href=\"https://www.postgresql.org/docs/current/static/kernel-resources.html#LINUX-MEMORY-OVERCOMMIT\" title=\"18.4.4.&#xA0;Linux Memory Overcommit\"><span class=\"productname\">Linux</span> out-of-memory</a>\n            (<acronym class=\"acronym\">OOM</acronym>) killer (Alex\n            Hunsaker, Tom Lane)</p>"
  ]
]