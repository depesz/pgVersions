[
  [
    "<p>Fix possible mis-evaluation of nested <code class=\"literal\">CASE</code>-<code class=\"literal\">WHEN</code>\n          expressions (Heikki Linnakangas, Michael Paquier, Tom\n          Lane)</p>",
    "<p>A <code class=\"literal\">CASE</code> expression\n          appearing within the test value subexpression of another\n          <code class=\"literal\">CASE</code> could become confused\n          about whether its own test value was null or not. Also,\n          inlining of a SQL function implementing the equality\n          operator used by a <code class=\"literal\">CASE</code>\n          expression could result in passing the wrong test value\n          to functions called within a <code class=\"literal\">CASE</code> expression in the SQL function's\n          body. If the test values were of different data types, a\n          crash might result; moreover such situations could be\n          abused to allow disclosure of portions of server memory.\n          (CVE-2016-5423)</p>"
  ],
  [
    "<p>Fix client programs' handling of special characters in\n          database and role names (Noah Misch, Nathan Bossart,\n          Michael Paquier)</p>",
    "<p>Numerous places in <span class=\"application\">vacuumdb</span> and other client programs\n          could become confused by database and role names\n          containing double quotes or backslashes. Tighten up\n          quoting rules to make that safe. Also, ensure that when a\n          conninfo string is used as a database name parameter to\n          these programs, it is correctly treated as such\n          throughout.</p>",
    "<p>Fix handling of paired double quotes in <span class=\"application\">psql</span>'s <code class=\"command\">\\connect</code> and <code class=\"command\">\\password</code> commands to match the\n          documentation.</p>",
    "<p>Introduce a new <code class=\"option\">-reuse-previous</code> option in <span class=\"application\">psql</span>'s <code class=\"command\">\\connect</code> command to allow explicit\n          control of whether to re-use connection parameters from a\n          previous connection. (Without this, the choice is based\n          on whether the database name looks like a conninfo\n          string, as before.) This allows secure handling of\n          database names containing special characters in\n          <span class=\"application\">pg_dumpall</span> scripts.</p>",
    "<p><span class=\"application\">pg_dumpall</span> now\n          refuses to deal with database and role names containing\n          carriage returns or newlines, as it seems impractical to\n          quote those characters safely on Windows. In future we\n          may reject such names on the server side, but that step\n          has not been taken yet.</p>",
    "<p>These are considered security fixes because crafted\n          object names containing special characters could have\n          been used to execute commands with superuser privileges\n          the next time a superuser executes <span class=\"application\">pg_dumpall</span> or other routine\n          maintenance operations. (CVE-2016-5424)</p>"
  ],
  [
    "<p>Fix corner-case misbehaviors for <code class=\"literal\">IS NULL</code>/<code class=\"literal\">IS NOT\n          NULL</code> applied to nested composite values (Andrew\n          Gierth, Tom Lane)</p>",
    "<p>The SQL standard specifies that <code class=\"literal\">IS NULL</code> should return TRUE for a row of\n          all null values (thus <code class=\"literal\">ROW(NULL,NULL) IS NULL</code> yields TRUE), but\n          this is not meant to apply recursively (thus <code class=\"literal\">ROW(NULL, ROW(NULL,NULL)) IS NULL</code> yields\n          FALSE). The core executor got this right, but certain\n          planner optimizations treated the test as recursive (thus\n          producing TRUE in both cases), and <code class=\"filename\">contrib/postgres_fdw</code> could produce\n          remote queries that misbehaved similarly.</p>"
  ],
  [
    "<p>Make the <code class=\"type\">inet</code> and\n          <code class=\"type\">cidr</code> data types properly reject\n          IPv6 addresses with too many colon-separated fields (Tom\n          Lane)</p>"
  ],
  [
    "<p>Prevent crash in <code class=\"function\">close_ps()</code> (the <code class=\"type\">point</code> <code class=\"literal\">##</code>\n          <code class=\"type\">lseg</code> operator) for NaN input\n          coordinates (Tom Lane)</p>",
    "<p>Make it return NULL instead of crashing.</p>"
  ],
  [
    "<p>Fix several one-byte buffer over-reads in <code class=\"function\">to_number()</code> (Peter Eisentraut)</p>",
    "<p>In several cases the <code class=\"function\">to_number()</code> function would read one\n          more character than it should from the input string.\n          There is a small chance of a crash, if the input happens\n          to be adjacent to the end of memory.</p>"
  ],
  [
    "<p>Avoid unsafe intermediate state during expensive paths\n          through <code class=\"function\">heap_update()</code>\n          (Masahiko Sawada, Andres Freund)</p>",
    "<p>Previously, these cases locked the target tuple (by\n          setting its XMAX) but did not WAL-log that action, thus\n          risking data integrity problems if the page were spilled\n          to disk and then a database crash occurred before the\n          tuple update could be completed.</p>"
  ],
  [
    "<p>Avoid consuming a transaction ID during <code class=\"command\">VACUUM</code> (Alexander Korotkov)</p>",
    "<p>Some cases in <code class=\"command\">VACUUM</code>\n          unnecessarily caused an XID to be assigned to the current\n          transaction. Normally this is negligible, but if one is\n          up against the XID wraparound limit, consuming more XIDs\n          during anti-wraparound vacuums is a very bad thing.</p>"
  ],
  [
    "<p>Avoid canceling hot-standby queries during\n          <code class=\"command\">VACUUM FREEZE</code> (Simon Riggs,\n          &#xC1;lvaro Herrera)</p>",
    "<p><code class=\"command\">VACUUM FREEZE</code> on an\n          otherwise-idle master server could result in unnecessary\n          cancellations of queries on its standby servers.</p>"
  ],
  [
    "<p>When a manual <code class=\"command\">ANALYZE</code>\n          specifies a column list, don't reset the table's\n          <code class=\"literal\">changes_since_analyze</code>\n          counter (Tom Lane)</p>",
    "<p>If we're only analyzing some columns, we should not\n          prevent routine auto-analyze from happening for the other\n          columns.</p>"
  ],
  [
    "<p>Fix <code class=\"command\">ANALYZE</code>'s\n          overestimation of <code class=\"literal\">n_distinct</code>\n          for a unique or nearly-unique column with many null\n          entries (Tom Lane)</p>",
    "<p>The nulls could get counted as though they were\n          themselves distinct values, leading to serious planner\n          misestimates in some types of queries.</p>"
  ],
  [
    "<p>Prevent autovacuum from starting multiple workers for\n          the same shared catalog (&#xC1;lvaro Herrera)</p>",
    "<p>Normally this isn't much of a problem because the\n          vacuum doesn't take long anyway; but in the case of a\n          severely bloated catalog, it could result in all but one\n          worker uselessly waiting instead of doing useful work on\n          other tables.</p>"
  ],
  [
    "<p>Fix <code class=\"filename\">contrib/btree_gin</code> to\n          handle the smallest possible <code class=\"type\">bigint</code> value correctly (Peter\n          Eisentraut)</p>"
  ],
  [
    "<p>Teach libpq to correctly decode server version from\n          future servers (Peter Eisentraut)</p>",
    "<p>It's planned to switch to two-part instead of\n          three-part server version numbers for releases after 9.6.\n          Make sure that <code class=\"function\">PQserverVersion()</code> returns the correct\n          value for such cases.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">ecpg</span>'s code for\n          <code class=\"literal\">unsigned long long</code> array\n          elements (Michael Meskes)</p>"
  ],
  [
    "<p>Make <span class=\"application\">pg_basebackup</span>\n          accept <code class=\"literal\">-Z 0</code> as specifying no\n          compression (Fujii Masao)</p>"
  ],
  [
    "<p>Revert to the old heuristic timeout for <code class=\"literal\">pg_ctl start -w</code> (Tom Lane)</p>",
    "<p>The new method adopted as of release 9.1.20 does not\n          work when <code class=\"varname\">silent_mode</code> is\n          enabled, so go back to the old way.</p>"
  ],
  [
    "<p>Fix makefiles' rule for building AIX shared libraries\n          to be safe for parallel make (Noah Misch)</p>"
  ],
  [
    "<p>Fix TAP tests and MSVC scripts to work when build\n          directory's path name contains spaces (Michael Paquier,\n          Kyotaro Horiguchi)</p>"
  ],
  [
    "<p>Make regression tests safe for Danish and Welsh\n          locales (Jeff Janes, Tom Lane)</p>",
    "<p>Change some test data that triggered the unusual\n          sorting rules of these locales.</p>"
  ],
  [
    "<p>Update our copy of the timezone code to match IANA's\n          <span class=\"application\">tzcode</span> release 2016c\n          (Tom Lane)</p>",
    "<p>This is needed to cope with anticipated future changes\n          in the time zone data files. It also fixes some\n          corner-case bugs in coping with unusual time zones.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2016f for DST law\n          changes in Kemerovo and Novosibirsk, plus historical\n          corrections for Azerbaijan, Belarus, and Morocco.</p>"
  ]
]