[
  [
    "<p>Require execute permission on the trigger function for\n          <code class=\"command\">CREATE TRIGGER</code> (Robert\n          Haas)</p>",
    "<p>This missing check could allow another user to execute\n          a trigger function with forged input data, by installing\n          it on a table he owns. This is only of significance for\n          trigger functions marked <code class=\"literal\">SECURITY\n          DEFINER</code>, since otherwise trigger functions run as\n          the table owner anyway. (CVE-2012-0866)</p>"
  ],
  [
    "<p>Remove arbitrary limitation on length of common name\n          in SSL certificates (Heikki Linnakangas)</p>",
    "<p>Both <span class=\"application\">libpq</span> and the\n          server truncated the common name extracted from an SSL\n          certificate at 32 bytes. Normally this would cause\n          nothing worse than an unexpected verification failure,\n          but there are some rather-implausible scenarios in which\n          it might allow one certificate holder to impersonate\n          another. The victim would have to have a common name\n          exactly 32 bytes long, and the attacker would have to\n          persuade a trusted CA to issue a certificate in which the\n          common name has that string as a prefix. Impersonating a\n          server would also require some additional exploit to\n          redirect client connections. (CVE-2012-0867)</p>"
  ],
  [
    "<p>Convert newlines to spaces in names written in\n          <span class=\"application\">pg_dump</span> comments (Robert\n          Haas)</p>",
    "<p><span class=\"application\">pg_dump</span> was\n          incautious about sanitizing object names that are emitted\n          within SQL comments in its output script. A name\n          containing a newline would at least render the script\n          syntactically incorrect. Maliciously crafted object names\n          could present a SQL injection risk when the script is\n          reloaded. (CVE-2012-0868)</p>"
  ],
  [
    "<p>Fix btree index corruption from insertions concurrent\n          with vacuuming (Tom Lane)</p>",
    "<p>An index page split caused by an insertion could\n          sometimes cause a concurrently-running <code class=\"command\">VACUUM</code> to miss removing index entries\n          that it should remove. After the corresponding table rows\n          are removed, the dangling index entries would cause\n          errors (such as <span class=\"quote\">&#x201C;<span class=\"quote\">could not read block N in file\n          ...</span>&#x201D;</span>) or worse, silently wrong query\n          results after unrelated rows are re-inserted at the\n          now-free table locations. This bug has been present since\n          release 8.2, but occurs so infrequently that it was not\n          diagnosed until now. If you have reason to suspect that\n          it has happened in your database, reindexing the affected\n          index will fix things.</p>"
  ],
  [
    "<p>Fix transient zeroing of shared buffers during WAL\n          replay (Tom Lane)</p>",
    "<p>The replay logic would sometimes zero and refill a\n          shared buffer, so that the contents were transiently\n          invalid. In hot standby mode this can result in a query\n          that's executing in parallel seeing garbage data. Various\n          symptoms could result from that, but the most common one\n          seems to be <span class=\"quote\">&#x201C;<span class=\"quote\">invalid memory alloc request\n          size</span>&#x201D;</span>.</p>"
  ],
  [
    "<p>Fix postmaster to attempt restart after a hot-standby\n          crash (Tom Lane)</p>",
    "<p>A logic error caused the postmaster to terminate,\n          rather than attempt to restart the cluster, if any\n          backend process crashed while operating in hot standby\n          mode.</p>"
  ],
  [
    "<p>Fix <code class=\"command\">CLUSTER</code>/<code class=\"command\">VACUUM FULL</code> handling of toast values\n          owned by recently-updated rows (Tom Lane)</p>",
    "<p>This oversight could lead to <span class=\"quote\">&#x201C;<span class=\"quote\">duplicate key value violates\n          unique constraint</span>&#x201D;</span> errors being reported\n          against the toast table's index during one of these\n          commands.</p>"
  ],
  [
    "<p>Update per-column permissions, not only per-table\n          permissions, when changing table owner (Tom Lane)</p>",
    "<p>Failure to do this meant that any previously granted\n          column permissions were still shown as having been\n          granted by the old owner. This meant that neither the new\n          owner nor a superuser could revoke the\n          now-untraceable-to-table-owner permissions.</p>"
  ],
  [
    "<p>Support foreign data wrappers and foreign servers in\n          <code class=\"command\">REASSIGN OWNED</code> (Alvaro\n          Herrera)</p>",
    "<p>This command failed with <span class=\"quote\">&#x201C;<span class=\"quote\">unexpected\n          classid</span>&#x201D;</span> errors if it needed to change the\n          ownership of any such objects.</p>"
  ],
  [
    "<p>Allow non-existent values for some settings in\n          <code class=\"command\">ALTER USER/DATABASE SET</code>\n          (Heikki Linnakangas)</p>",
    "<p>Allow <code class=\"varname\">default_text_search_config</code>, <code class=\"varname\">default_tablespace</code>, and <code class=\"varname\">temp_tablespaces</code> to be set to names that\n          are not known. This is because they might be known in\n          another database where the setting is intended to be\n          used, or for the tablespace cases because the tablespace\n          might not be created yet. The same issue was previously\n          recognized for <code class=\"varname\">search_path</code>,\n          and these settings now act like that one.</p>"
  ],
  [
    "<p>Avoid crashing when we have problems deleting table\n          files post-commit (Tom Lane)</p>",
    "<p>Dropping a table should lead to deleting the\n          underlying disk files only after the transaction commits.\n          In event of failure then (for instance, because of wrong\n          file permissions) the code is supposed to just emit a\n          warning message and go on, since it's too late to abort\n          the transaction. This logic got broken as of release 8.4,\n          causing such situations to result in a PANIC and an\n          unrestartable database.</p>"
  ],
  [
    "<p>Recover from errors occurring during WAL replay of\n          <code class=\"command\">DROP TABLESPACE</code> (Tom\n          Lane)</p>",
    "<p>Replay will attempt to remove the tablespace's\n          directories, but there are various reasons why this might\n          fail (for example, incorrect ownership or permissions on\n          those directories). Formerly the replay code would panic,\n          rendering the database unrestartable without manual\n          intervention. It seems better to log the problem and\n          continue, since the only consequence of failure to remove\n          the directories is some wasted disk space.</p>"
  ],
  [
    "<p>Fix race condition in logging AccessExclusiveLocks for\n          hot standby (Simon Riggs)</p>",
    "<p>Sometimes a lock would be logged as being held by\n          <span class=\"quote\">&#x201C;<span class=\"quote\">transaction\n          zero</span>&#x201D;</span>. This is at least known to produce\n          assertion failures on slave servers, and might be the\n          cause of more serious problems.</p>"
  ],
  [
    "<p>Track the OID counter correctly during WAL replay,\n          even when it wraps around (Tom Lane)</p>",
    "<p>Previously the OID counter would remain stuck at a\n          high value until the system exited replay mode. The\n          practical consequences of that are usually nil, but there\n          are scenarios wherein a standby server that's been\n          promoted to master might take a long time to advance the\n          OID counter to a reasonable value once values are\n          needed.</p>"
  ],
  [
    "<p>Prevent emitting misleading <span class=\"quote\">&#x201C;<span class=\"quote\">consistent recovery state\n          reached</span>&#x201D;</span> log message at the beginning of\n          crash recovery (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Fix initial value of <code class=\"structname\">pg_stat_replication</code>.<code class=\"structfield\">replay_location</code> (Fujii Masao)</p>",
    "<p>Previously, the value shown would be wrong until at\n          least one WAL record had been replayed.</p>"
  ],
  [
    "<p>Fix regular expression back-references with\n          <code class=\"literal\">*</code> attached (Tom Lane)</p>",
    "<p>Rather than enforcing an exact string match, the code\n          would effectively accept any string that satisfies the\n          pattern sub-expression referenced by the back-reference\n          symbol.</p>",
    "<p>A similar problem still afflicts back-references that\n          are embedded in a larger quantified expression, rather\n          than being the immediate subject of the quantifier. This\n          will be addressed in a future <span class=\"productname\">PostgreSQL</span> release.</p>"
  ],
  [
    "<p>Fix recently-introduced memory leak in processing of\n          <code class=\"type\">inet</code>/<code class=\"type\">cidr</code> values (Heikki Linnakangas)</p>",
    "<p>A patch in the December 2011 releases of <span class=\"productname\">PostgreSQL</span> caused memory leakage in\n          these operations, which could be significant in scenarios\n          such as building a btree index on such a column.</p>"
  ],
  [
    "<p>Fix dangling pointer after <code class=\"command\">CREATE TABLE AS</code>/<code class=\"command\">SELECT INTO</code> in a SQL-language function\n          (Tom Lane)</p>",
    "<p>In most cases this only led to an assertion failure in\n          assert-enabled builds, but worse consequences seem\n          possible.</p>"
  ],
  [
    "<p>Avoid double close of file handle in syslogger on\n          Windows (MauMau)</p>",
    "<p>Ordinarily this error was invisible, but it would\n          cause an exception when running on a debug version of\n          Windows.</p>"
  ],
  [
    "<p>Fix I/O-conversion-related memory leaks in plpgsql\n          (Andres Freund, Jan Urbanski, Tom Lane)</p>",
    "<p>Certain operations would leak memory until the end of\n          the current function.</p>"
  ],
  [
    "<p>Improve <span class=\"application\">pg_dump</span>'s\n          handling of inherited table columns (Tom Lane)</p>",
    "<p><span class=\"application\">pg_dump</span> mishandled\n          situations where a child column has a different default\n          expression than its parent column. If the default is\n          textually identical to the parent's default, but not\n          actually the same (for instance, because of schema search\n          path differences) it would not be recognized as\n          different, so that after dump and restore the child would\n          be allowed to inherit the parent's default. Child columns\n          that are <code class=\"literal\">NOT NULL</code> where\n          their parent is not could also be restored subtly\n          incorrectly.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_restore</span>'s\n          direct-to-database mode for INSERT-style table data (Tom\n          Lane)</p>",
    "<p>Direct-to-database restores from archive files made\n          with <code class=\"option\">--inserts</code> or\n          <code class=\"option\">--column-inserts</code> options fail\n          when using <span class=\"application\">pg_restore</span>\n          from a release dated September or December 2011, as a\n          result of an oversight in a fix for another problem. The\n          archive file itself is not at fault, and text-mode output\n          is okay.</p>"
  ],
  [
    "<p>Allow <span class=\"application\">pg_upgrade</span> to\n          process tables containing <code class=\"type\">regclass</code> columns (Bruce Momjian)</p>",
    "<p>Since <span class=\"application\">pg_upgrade</span> now\n          takes care to preserve <code class=\"structname\">pg_class</code> OIDs, there was no longer\n          any reason for this restriction.</p>"
  ],
  [
    "<p>Make <span class=\"application\">libpq</span> ignore\n          <code class=\"literal\">ENOTDIR</code> errors when looking\n          for an SSL client certificate file (Magnus Hagander)</p>",
    "<p>This allows SSL connections to be established, though\n          without a certificate, even when the user's home\n          directory is set to something like <code class=\"literal\">/dev/null</code>.</p>"
  ],
  [
    "<p>Fix some more field alignment issues in <span class=\"application\">ecpg</span>'s SQLDA area (Zoltan\n          Boszormenyi)</p>"
  ],
  [
    "<p>Allow <code class=\"literal\">AT</code> option in\n          <span class=\"application\">ecpg</span> <code class=\"literal\">DEALLOCATE</code> statements (Michael\n          Meskes)</p>",
    "<p>The infrastructure to support this has been there for\n          awhile, but through an oversight there was still an error\n          check rejecting the case.</p>"
  ],
  [
    "<p>Do not use the variable name when defining a varchar\n          structure in ecpg (Michael Meskes)</p>"
  ],
  [
    "<p>Fix <code class=\"filename\">contrib/auto_explain</code>'s JSON output mode\n          to produce valid JSON (Andrew Dunstan)</p>",
    "<p>The output used brackets at the top level, when it\n          should have used braces.</p>"
  ],
  [
    "<p>Fix error in <code class=\"filename\">contrib/intarray</code>'s <code class=\"literal\">int[] &amp; int[]</code> operator (Guillaume\n          Lelarge)</p>",
    "<p>If the smallest integer the two input arrays have in\n          common is 1, and there are smaller values in either\n          array, then 1 would be incorrectly omitted from the\n          result.</p>"
  ],
  [
    "<p>Fix error detection in <code class=\"filename\">contrib/pgcrypto</code>'s <code class=\"function\">encrypt_iv()</code> and <code class=\"function\">decrypt_iv()</code> (Marko Kreen)</p>",
    "<p>These functions failed to report certain types of\n          invalid-input errors, and would instead return random\n          garbage values for incorrect input.</p>"
  ],
  [
    "<p>Fix one-byte buffer overrun in <code class=\"filename\">contrib/test_parser</code> (Paul Guyot)</p>",
    "<p>The code would try to read one more byte than it\n          should, which would crash in corner cases. Since\n          <code class=\"filename\">contrib/test_parser</code> is only\n          example code, this is not a security issue in itself, but\n          bad example code is still bad.</p>"
  ],
  [
    "<p>Use <code class=\"function\">__sync_lock_test_and_set()</code> for\n          spinlocks on ARM, if available (Martin Pitt)</p>",
    "<p>This function replaces our previous use of the\n          <code class=\"literal\">SWPB</code> instruction, which is\n          deprecated and not available on ARMv6 and later. Reports\n          suggest that the old code doesn't fail in an obvious way\n          on recent ARM boards, but simply doesn't interlock\n          concurrent accesses, leading to bizarre failures in\n          multiprocess operation.</p>"
  ],
  [
    "<p>Use <code class=\"option\">-fexcess-precision=standard</code> option when\n          building with gcc versions that accept it (Andrew\n          Dunstan)</p>",
    "<p>This prevents assorted scenarios wherein recent\n          versions of gcc will produce creative results.</p>"
  ],
  [
    "<p>Allow use of threaded Python on FreeBSD (Chris\n          Rees)</p>",
    "<p>Our configure script previously believed that this\n          combination wouldn't work; but FreeBSD fixed the problem,\n          so remove that error check.</p>"
  ]
]