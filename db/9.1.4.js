[
  [
    "<p>Fix incorrect password transformation in <code class=\"filename\">contrib/pgcrypto</code>'s DES <code class=\"function\">crypt()</code> function (Solar Designer)</p>",
    "<p>If a password string contained the byte value\n          <code class=\"literal\">0x80</code>, the remainder of the\n          password was ignored, causing the password to be much\n          weaker than it appeared. With this fix, the rest of the\n          string is properly included in the DES hash. Any stored\n          password values that are affected by this bug will thus\n          no longer match, so the stored values may need to be\n          updated. (CVE-2012-2143)</p>"
  ],
  [
    "<p>Ignore <code class=\"literal\">SECURITY DEFINER</code>\n          and <code class=\"literal\">SET</code> attributes for a\n          procedural language's call handler (Tom Lane)</p>",
    "<p>Applying such attributes to a call handler could crash\n          the server. (CVE-2012-2655)</p>"
  ],
  [
    "<p>Make <code class=\"filename\">contrib/citext</code>'s\n          upgrade script fix collations of <code class=\"type\">citext</code> arrays and domains over <code class=\"type\">citext</code> (Tom Lane)</p>",
    "<p>Release 9.1.2 provided a fix for collations of\n          <code class=\"type\">citext</code> columns and indexes in\n          databases upgraded or reloaded from pre-9.1\n          installations, but that fix was incomplete: it neglected\n          to handle arrays and domains over <code class=\"type\">citext</code>. This release extends the module's\n          upgrade script to handle these cases. As before, if you\n          have already run the upgrade script, you'll need to run\n          the collation update commands by hand instead. See the\n          9.1.2 release notes for more information about doing\n          this.</p>"
  ],
  [
    "<p>Allow numeric timezone offsets in <code class=\"type\">timestamp</code> input to be up to 16 hours away\n          from UTC (Tom Lane)</p>",
    "<p>Some historical time zones have offsets larger than 15\n          hours, the previous limit. This could result in dumped\n          data values being rejected during reload.</p>"
  ],
  [
    "<p>Fix timestamp conversion to cope when the given time\n          is exactly the last DST transition time for the current\n          timezone (Tom Lane)</p>",
    "<p>This oversight has been there a long time, but was not\n          noticed previously because most DST-using zones are\n          presumed to have an indefinite sequence of future DST\n          transitions.</p>"
  ],
  [
    "<p>Fix <code class=\"type\">text</code> to <code class=\"type\">name</code> and <code class=\"type\">char</code> to\n          <code class=\"type\">name</code> casts to perform string\n          truncation correctly in multibyte encodings (Karl\n          Schnaitter)</p>"
  ],
  [
    "<p>Fix memory copying bug in <code class=\"function\">to_tsquery()</code> (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Ensure <code class=\"function\">txid_current()</code>\n          reports the correct epoch when executed in hot standby\n          (Simon Riggs)</p>"
  ],
  [
    "<p>Fix planner's handling of outer PlaceHolderVars within\n          subqueries (Tom Lane)</p>",
    "<p>This bug concerns sub-SELECTs that reference variables\n          coming from the nullable side of an outer join of the\n          surrounding query. In 9.1, queries affected by this bug\n          would fail with <span class=\"quote\">&#x201C;<span class=\"quote\">ERROR: Upper-level PlaceHolderVar found where not\n          expected</span>&#x201D;</span>. But in 9.0 and 8.4, you'd\n          silently get possibly-wrong answers, since the value\n          transmitted into the subquery wouldn't go to null when it\n          should.</p>"
  ],
  [
    "<p>Fix planning of <code class=\"literal\">UNION ALL</code>\n          subqueries with output columns that are not simple\n          variables (Tom Lane)</p>",
    "<p>Planning of such cases got noticeably worse in 9.1 as\n          a result of a misguided fix for <span class=\"quote\">&#x201C;<span class=\"quote\">MergeAppend child's\n          targetlist doesn't match MergeAppend</span>&#x201D;</span>\n          errors. Revert that fix and do it another way.</p>"
  ],
  [
    "<p>Fix slow session startup when <code class=\"structname\">pg_attribute</code> is very large (Tom\n          Lane)</p>",
    "<p>If <code class=\"structname\">pg_attribute</code>\n          exceeds one-fourth of <code class=\"varname\">shared_buffers</code>, cache rebuilding code\n          that is sometimes needed during session start would\n          trigger the synchronized-scan logic, causing it to take\n          many times longer than normal. The problem was\n          particularly acute if many new sessions were starting at\n          once.</p>"
  ],
  [
    "<p>Ensure sequential scans check for query cancel\n          reasonably often (Merlin Moncure)</p>",
    "<p>A scan encountering many consecutive pages that\n          contain no live tuples would not respond to interrupts\n          meanwhile.</p>"
  ],
  [
    "<p>Ensure the Windows implementation of <code class=\"function\">PGSemaphoreLock()</code> clears <code class=\"varname\">ImmediateInterruptOK</code> before returning\n          (Tom Lane)</p>",
    "<p>This oversight meant that a query-cancel interrupt\n          received later in the same query could be accepted at an\n          unsafe time, with unpredictable but not good\n          consequences.</p>"
  ],
  [
    "<p>Show whole-row variables safely when printing views or\n          rules (Abbas Butt, Tom Lane)</p>",
    "<p>Corner cases involving ambiguous names (that is, the\n          name could be either a table or column name of the query)\n          were printed in an ambiguous way, risking that the view\n          or rule would be interpreted differently after dump and\n          reload. Avoid the ambiguous case by attaching a no-op\n          cast.</p>"
  ],
  [
    "<p>Fix <code class=\"command\">COPY FROM</code> to properly\n          handle null marker strings that correspond to invalid\n          encoding (Tom Lane)</p>",
    "<p>A null marker string such as <code class=\"literal\">E'\\\\0'</code> should work, and did work in the\n          past, but the case got broken in 8.4.</p>"
  ],
  [
    "<p>Fix <code class=\"command\">EXPLAIN VERBOSE</code> for\n          writable CTEs containing <code class=\"literal\">RETURNING</code> clauses (Tom Lane)</p>"
  ],
  [
    "<p>Fix <code class=\"command\">PREPARE TRANSACTION</code>\n          to work correctly in the presence of advisory locks (Tom\n          Lane)</p>",
    "<p>Historically, <code class=\"command\">PREPARE\n          TRANSACTION</code> has simply ignored any session-level\n          advisory locks the session holds, but this case was\n          accidentally broken in 9.1.</p>"
  ],
  [
    "<p>Fix truncation of unlogged tables (Robert Haas)</p>"
  ],
  [
    "<p>Ignore missing schemas during non-interactive\n          assignments of <code class=\"varname\">search_path</code>\n          (Tom Lane)</p>",
    "<p>This re-aligns 9.1's behavior with that of older\n          branches. Previously 9.1 would throw an error for\n          nonexistent schemas mentioned in <code class=\"varname\">search_path</code> settings obtained from\n          places such as <code class=\"command\">ALTER DATABASE\n          SET</code>.</p>"
  ],
  [
    "<p>Fix bugs with temporary or transient tables used in\n          extension scripts (Tom Lane)</p>",
    "<p>This includes cases such as a rewriting <code class=\"command\">ALTER TABLE</code> within an extension update\n          script, since that uses a transient table behind the\n          scenes.</p>"
  ],
  [
    "<p>Ensure autovacuum worker processes perform stack depth\n          checking properly (Heikki Linnakangas)</p>",
    "<p>Previously, infinite recursion in a function invoked\n          by auto-<code class=\"command\">ANALYZE</code> could crash\n          worker processes.</p>"
  ],
  [
    "<p>Fix logging collector to not lose log coherency under\n          high load (Andrew Dunstan)</p>",
    "<p>The collector previously could fail to reassemble\n          large messages if it got too busy.</p>"
  ],
  [
    "<p>Fix logging collector to ensure it will restart file\n          rotation after receiving <span class=\"systemitem\">SIGHUP</span> (Tom Lane)</p>"
  ],
  [
    "<p>Fix <span class=\"quote\">&#x201C;<span class=\"quote\">too many\n          LWLocks taken</span>&#x201D;</span> failure in GiST indexes\n          (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Fix WAL replay logic for GIN indexes to not fail if\n          the index was subsequently dropped (Tom Lane)</p>"
  ],
  [
    "<p>Correctly detect SSI conflicts of prepared\n          transactions after a crash (Dan Ports)</p>"
  ],
  [
    "<p>Avoid synchronous replication delay when committing a\n          transaction that only modified temporary tables (Heikki\n          Linnakangas)</p>",
    "<p>In such a case the transaction's commit record need\n          not be flushed to standby servers, but some of the code\n          didn't know that and waited for it to happen anyway.</p>"
  ],
  [
    "<p>Fix error handling in <span class=\"application\">pg_basebackup</span> (Thomas Ogrisegg,\n          Fujii Masao)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">walsender</span> to not\n          go into a busy loop if connection is terminated (Fujii\n          Masao)</p>"
  ],
  [
    "<p>Fix memory leak in PL/pgSQL's <code class=\"command\">RETURN NEXT</code> command (Joe Conway)</p>"
  ],
  [
    "<p>Fix PL/pgSQL's <code class=\"command\">GET\n          DIAGNOSTICS</code> command when the target is the\n          function's first variable (Tom Lane)</p>"
  ],
  [
    "<p>Ensure that PL/Perl package-qualifies the <code class=\"varname\">_TD</code> variable (Alex Hunsaker)</p>",
    "<p>This bug caused trigger invocations to fail when they\n          are nested within a function invocation that changes the\n          current package.</p>"
  ],
  [
    "<p>Fix PL/Python functions returning composite types to\n          accept a string for their result value (Jan Urbanski)</p>",
    "<p>This case was accidentally broken by the 9.1 additions\n          to allow a composite result value to be supplied in other\n          formats, such as dictionaries.</p>"
  ],
  [
    "<p>Fix potential access off the end of memory in\n          <span class=\"application\">psql</span>'s expanded display\n          (<code class=\"command\">\\x</code>) mode (Peter\n          Eisentraut)</p>"
  ],
  [
    "<p>Fix several performance problems in <span class=\"application\">pg_dump</span> when the database contains\n          many objects (Jeff Janes, Tom Lane)</p>",
    "<p><span class=\"application\">pg_dump</span> could get\n          very slow if the database contained many schemas, or if\n          many objects are in dependency loops, or if there are\n          many owned sequences.</p>"
  ],
  [
    "<p>Fix memory and file descriptor leaks in <span class=\"application\">pg_restore</span> when reading a\n          directory-format archive (Peter Eisentraut)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_upgrade</span> for\n          the case that a database stored in a non-default\n          tablespace contains a table in the cluster's default\n          tablespace (Bruce Momjian)</p>"
  ],
  [
    "<p>In <span class=\"application\">ecpg</span>, fix rare\n          memory leaks and possible overwrite of one byte after the\n          <code class=\"structname\">sqlca_t</code> structure (Peter\n          Eisentraut)</p>"
  ],
  [
    "<p>Fix <code class=\"filename\">contrib/dblink</code>'s\n          <code class=\"function\">dblink_exec()</code> to not leak\n          temporary database connections upon error (Tom Lane)</p>"
  ],
  [
    "<p>Fix <code class=\"filename\">contrib/dblink</code> to\n          report the correct connection name in error messages\n          (Kyotaro Horiguchi)</p>"
  ],
  [
    "<p>Fix <code class=\"filename\">contrib/vacuumlo</code> to\n          use multiple transactions when dropping many large\n          objects (Tim Lewis, Robert Haas, Tom Lane)</p>",
    "<p>This change avoids exceeding <code class=\"varname\">max_locks_per_transaction</code> when many\n          objects need to be dropped. The behavior can be adjusted\n          with the new <code class=\"literal\">-l</code> (limit)\n          option.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2012c for DST law\n          changes in Antarctica, Armenia, Chile, Cuba, Falkland\n          Islands, Gaza, Haiti, Hebron, Morocco, Syria, and Tokelau\n          Islands; also historical corrections for Canada.</p>"
  ]
]