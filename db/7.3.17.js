[
  [
    "<p><code class=\"function\">to_number()</code> and\n          <code class=\"function\">to_char(numeric)</code> are now\n          <code class=\"literal\">STABLE</code>, not <code class=\"literal\">IMMUTABLE</code>, for new <span class=\"application\">initdb</span> installs (Tom)</p>",
    "<p>This is because <code class=\"varname\">lc_numeric</code> can potentially change the\n          output of these functions.</p>"
  ],
  [
    "<p>Improve index usage of regular expressions that use\n          parentheses (Tom)</p>",
    "<p>This improves <span class=\"application\">psql</span>\n          <code class=\"literal\">\\d</code> performance also.</p>"
  ]
]