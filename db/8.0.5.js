[
  [
    "<p>Fix race condition in transaction log management</p>",
    "<p>There was a narrow window in which an I/O operation\n          could be initiated for the wrong page, leading to an\n          Assert failure or data corruption.</p>"
  ],
  [
    "<p>Fix bgwriter problems after recovering from errors\n          (Tom)</p>",
    "<p>The background writer was found to leak buffer pins\n          after write errors. While not fatal in itself, this might\n          lead to mysterious blockages of later VACUUM\n          commands.</p>"
  ],
  [
    "<p>Prevent failure if client sends Bind protocol message\n          when current transaction is already aborted</p>"
  ],
  [
    "<p><code class=\"filename\">/contrib/ltree</code> fixes\n          (Teodor)</p>"
  ],
  [
    "<p>AIX and HPUX compile fixes (Tom)</p>"
  ],
  [
    "<p>Retry file reads and writes after Windows\n          NO_SYSTEM_RESOURCES error (Qingqing Zhou)</p>"
  ],
  [
    "<p>Fix intermittent failure when <code class=\"varname\">log_line_prefix</code> includes <code class=\"literal\">%i</code></p>"
  ],
  [
    "<p>Fix <span class=\"application\">psql</span> performance\n          issue with long scripts on Windows (Merlin Moncure)</p>"
  ],
  [
    "<p>Fix missing updates of <code class=\"filename\">pg_group</code> flat file</p>"
  ],
  [
    "<p>Fix longstanding planning error for outer joins</p>",
    "<p>This bug sometimes caused a bogus error <span class=\"quote\">&#x201C;<span class=\"quote\">RIGHT JOIN is only supported\n          with merge-joinable join conditions</span>&#x201D;</span>.</p>"
  ],
  [
    "<p>Postpone timezone initialization until after\n          <code class=\"filename\">postmaster.pid</code> is\n          created</p>",
    "<p>This avoids confusing startup scripts that expect the\n          pid file to appear quickly.</p>"
  ],
  [
    "<p>Prevent core dump in <span class=\"application\">pg_autovacuum</span> when a table has been\n          dropped</p>"
  ],
  [
    "<p>Fix problems with whole-row references (<code class=\"literal\">foo.*</code>) to subquery results</p>"
  ]
]