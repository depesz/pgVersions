[
  [
    "<p>Ensure new-in-9.3 JSON functionality is added to the\n          <code class=\"literal\">hstore</code> extension during an\n          update (Andrew Dunstan)</p>",
    "<p>Users who upgraded a pre-9.3 database containing\n          <code class=\"literal\">hstore</code> should execute</p>",
    "<pre class=\"programlisting\">\n          ALTER EXTENSION hstore UPDATE;</pre>",
    "<p>after installing 9.3.1, to add two new JSON functions\n          and a cast. (If <code class=\"literal\">hstore</code> is\n          already up to date, this command does nothing.)</p>"
  ],
  [
    "<p>Fix memory leak when creating B-tree indexes on range\n          columns (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Fix memory leak caused by <code class=\"function\">lo_open()</code> failure (Heikki\n          Linnakangas)</p>"
  ],
  [
    "<p>Serializable snapshot fixes (Kevin Grittner, Heikki\n          Linnakangas)</p>"
  ],
  [
    "<p>Fix deadlock bug in libpq when using SSL (Stephen\n          Frost)</p>"
  ],
  [
    "<p>Fix timeline handling bugs in <span class=\"application\">pg_receivexlog</span> (Heikki Linnakangas,\n          Andrew Gierth)</p>"
  ],
  [
    "<p>Prevent <code class=\"command\">CREATE FUNCTION</code>\n          from checking <code class=\"command\">SET</code> variables\n          unless function body checking is enabled (Tom Lane)</p>"
  ],
  [
    "<p>Remove rare inaccurate warning during vacuum of\n          index-less tables (Heikki Linnakangas)</p>"
  ]
]