[
  [
    "<p>Fix a core dump of COPY TO when client/server\n          encodings don't match (Tom)</p>"
  ],
  [
    "<p>Allow <span class=\"application\">pg_dump</span> to work\n          with pre-7.2 servers (Philip)</p>"
  ],
  [
    "<p>contrib/adddepend fixes (Tom)</p>"
  ],
  [
    "<p>Fix problem with deletion of per-user/per-database\n          config settings (Tom)</p>"
  ],
  [
    "<p>contrib/vacuumlo fix (Tom)</p>"
  ],
  [
    "<p>Allow 'password' encryption even when pg_shadow\n          contains MD5 passwords (Bruce)</p>"
  ],
  [
    "<p>contrib/dbmirror fix (Steven Singer)</p>"
  ],
  [
    "<p>Optimizer fixes (Tom)</p>"
  ],
  [
    "<p>contrib/tsearch fixes (Teodor Sigaev, Magnus)</p>"
  ],
  [
    "<p>Allow locale names to be mixed case (Nicolai\n          Tufar)</p>"
  ],
  [
    "<p>Increment libpq library's major version number\n          (Bruce)</p>"
  ],
  [
    "<p>pg_hba.conf error reporting fixes (Bruce, Neil)</p>"
  ],
  [
    "<p>Add SCO Openserver 5.0.4 as a supported platform\n          (Bruce)</p>"
  ],
  [
    "<p>Prevent EXPLAIN from crashing server (Tom)</p>"
  ],
  [
    "<p>SSL fixes (Nathan Mueller)</p>"
  ],
  [
    "<p>Prevent composite column creation via ALTER TABLE\n          (Tom)</p>"
  ]
]