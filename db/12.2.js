[
  [
    "<p>Add missing permissions checks for <code class=\"command\">ALTER ... DEPENDS ON EXTENSION</code> (Álvaro Herrera)</p>",
    "<p>Marking an object as dependent on an extension did not have any privilege check whatsoever. This oversight allowed any user to mark routines, triggers, materialized views, or indexes as droppable by anyone able to drop an extension. Require that the calling user own the specified object (and hence have privilege to drop it). (CVE-2020-1720)</p>"
  ],
  [
    "<p>Fix <code class=\"command\">TRUNCATE ... CASCADE</code> to ensure all relevant partitions are truncated (Jehan-Guillaume de Rorthais)</p>",
    "<p>If a partition of a partitioned table is truncated with the <code class=\"literal\">CASCADE</code> option, and the partitioned table has a foreign-key reference from another table, that table must also be truncated. The need to check this was missed if the referencing table was itself partitioned, possibly allowing rows to survive that violate the foreign-key constraint.</p>",
    "<p>Hence, if you have foreign key constraints between partitioned tables, and you have done any partition-level <code class=\"command\">TRUNCATE</code> on the referenced table, you should check to see if any foreign key violations exist. The simplest way is to add a new instance of the foreign key constraint (and, once that succeeds, drop it or the original constraint). That may be prohibitive from a locking standpoint, however, in which case you might prefer to manually query for unmatched rows.</p>"
  ],
  [
    "<p>Fix failure to attach foreign key constraints to sub-partitions (Jehan-Guillaume de Rorthais)</p>",
    "<p>When adding a partition to a level below the first level of a multi-level partitioned table, foreign key constraints referencing the top partitioned table were not cloned to the new partition, leading to possible constraint violations later. Detaching and re-attaching the new partition is the cheapest way to fix this. However, if there are many partitions to be fixed, adding a new instance of the foreign key constraint might be preferable.</p>"
  ],
  [
    "<p>Fix possible crash during concurrent update on a partitioned table or inheritance tree (Tom Lane)</p>"
  ],
  [
    "<p>Ensure that row triggers on partitioned tables are correctly cloned to sub-partitions when appropriate (Álvaro Herrera)</p>",
    "<p>User-defined triggers (but not triggers for foreign key or deferred unique constraints) might be missed when creating or attaching a partition.</p>"
  ],
  [
    "<p>Fix logical replication subscriber code to execute per-column <code class=\"literal\">UPDATE</code> triggers when appropriate (Peter Eisentraut)</p>"
  ],
  [
    "<p>Avoid failure in logical decoding when a large transaction must be spilled into many separate temporary files (Amit Khandekar)</p>"
  ],
  [
    "<p>Fix possible crash or data corruption when a logical replication subscriber processes a row update (Tom Lane, Tomas Vondra)</p>",
    "<p>This bug caused visible problems only if the subscriber's table contained columns that were not being copied from the publisher and had pass-by-reference data types.</p>"
  ],
  [
    "<p>Fix crash in logical replication subscriber after DDL changes on a subscribed relation (Jehan-Guillaume de Rorthais, Vignesh C)</p>"
  ],
  [
    "<p>Fix failure in logical replication publisher after a database crash and restart (Vignesh C)</p>"
  ],
  [
    "<p>Ensure that the effect of <code class=\"function\">pg_replication_slot_advance()</code> on a physical replication slot will persist across restarts (Alexey Kondratov, Michael Paquier)</p>"
  ],
  [
    "<p>Improve efficiency of logical replication with <code class=\"literal\">REPLICA IDENTITY FULL</code> (Konstantin Knizhnik)</p>",
    "<p>When searching for an existing tuple during an update or delete operation, return the first matching tuple not the last one.</p>"
  ],
  [
    "<p>Fix base backup to handle database OIDs larger than <code class=\"literal\">INT32_MAX</code> (Peter Eisentraut)</p>"
  ],
  [
    "<p>Ensure parallel plans are always shut down at the correct time (Kyotaro Horiguchi)</p>",
    "<p>This oversight is known to result in <span class=\"quote\">“<span class=\"quote\">temporary file leak</span>”</span> warnings from multi-batch parallel hash joins.</p>"
  ],
  [
    "<p>Prevent premature shutdown of a Gather or GatherMerge plan node that is underneath a Limit node (Amit Kapila)</p>",
    "<p>This avoids failure if such a plan node needs to be scanned more than once, as for instance if it is on the inside of a nestloop.</p>"
  ],
  [
    "<p>Improve efficiency of parallel hash join on CPUs with many cores (Gang Deng, Thomas Munro)</p>"
  ],
  [
    "<p>Avoid crash in parallel <code class=\"command\">CREATE INDEX</code> when there are no free dynamic shared memory slots (Thomas Munro)</p>",
    "<p>Fall back to a non-parallel index build, instead.</p>"
  ],
  [
    "<p>Avoid memory leak when there are no free dynamic shared memory slots (Thomas Munro)</p>"
  ],
  [
    "<p>Ignore the <code class=\"literal\">CONCURRENTLY</code> option when performing an index creation, drop, or rebuild on a temporary table (Michael Paquier, Heikki Linnakangas, Andres Freund)</p>",
    "<p>This avoids strange failures if the temporary table has an <code class=\"literal\">ON COMMIT</code> action. There is no benefit in using <code class=\"literal\">CONCURRENTLY</code> for a temporary table anyway, since other sessions cannot access the table, making the extra processing pointless.</p>"
  ],
  [
    "<p>Fix possible failure when resetting expression indexes on temporary tables that are marked <code class=\"literal\">ON COMMIT DELETE ROWS</code> (Tom Lane)</p>"
  ],
  [
    "<p>Fix possible crash in BRIN index operations with <code class=\"type\">box</code>, <code class=\"type\">range</code> and <code class=\"type\">inet</code> data types (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Fix crash during recursive page split in GiST index build (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Fix handling of deleted pages in GIN indexes (Alexander Korotkov)</p>",
    "<p>Avoid possible deadlocks, incorrect updates of a deleted page's state, and failure to traverse through a recently-deleted page.</p>"
  ],
  [
    "<p>Fix possible crash with a SubPlan (sub-<code class=\"literal\">SELECT</code>) within a multi-row <code class=\"literal\">VALUES</code> list (Tom Lane)</p>"
  ],
  [
    "<p>Fix failure in <code class=\"command\">ALTER TABLE</code> when a column referenced in a <code class=\"literal\">GENERATED</code> expression has been added or changed in type earlier in the same <code class=\"command\">ALTER</code> command (Tom Lane)</p>"
  ],
  [
    "<p>Fix failure to insert default values for <span class=\"quote\">“<span class=\"quote\">missing</span>”</span> attributes during tuple conversion (Vik Fearing, Andrew Gierth)</p>",
    "<p>This could result in values incorrectly reading as NULL, when they come from columns that had been added by <code class=\"literal\">ALTER TABLE ADD COLUMN</code> with a constant default.</p>"
  ],
  [
    "<p>Fix unlikely panic in the checkpointer process, caused by opening relation segments that might already have been removed (Thomas Munro)</p>"
  ],
  [
    "<p>Fix crash after FileClose() failure (Noah Misch)</p>",
    "<p>This issue could only be observed with <code class=\"varname\">data_sync_retry</code> enabled, since otherwise FileClose() failure would be reported as a PANIC.</p>"
  ],
  [
    "<p>Fix handling of multiple <code class=\"literal\">AFTER ROW</code> triggers on a foreign table (Etsuro Fujita)</p>"
  ],
  [
    "<p>Fix unlikely crash with pass-by-reference aggregate transition states (Andres Freund, Teodor Sigaev)</p>"
  ],
  [
    "<p>Improve error reporting in <code class=\"function\">to_date()</code> and <code class=\"function\">to_timestamp()</code> (Tom Lane, Álvaro Herrera)</p>",
    "<p>Reports about incorrect month or day names in input strings could truncate the input in the middle of a multi-byte character, leading to an improperly encoded error message that could cause follow-on failures. Truncate at the next whitespace instead.</p>"
  ],
  [
    "<p>Fix off-by-one result for <code class=\"literal\">EXTRACT(ISOYEAR FROM <em class=\"replaceable\"><code>timestamp</code></em>)</code> for BC dates (Tom Lane)</p>"
  ],
  [
    "<p>Ensure that the <code class=\"literal\">&lt;&gt;</code> operator for type <code class=\"type\">char</code> reports indeterminate-collation errors as such, rather than as <span class=\"quote\">“<span class=\"quote\">cache lookup failed for collation 0</span>”</span> (Tom Lane)</p>"
  ],
  [
    "<p>Avoid treating TID scans as sequential scans (Tatsuhito Kasahara)</p>",
    "<p>A refactoring oversight caused TID scans (selection by CTID) to be counted as sequential scans in the statistics views, and to take whole-table predicate locks as sequential scans do. The latter behavior could cause unnecessary serialization errors in serializable transaction mode.</p>"
  ],
  [
    "<p>Avoid stack overflow in <code class=\"literal\">information_schema</code> views when a self-referential view exists in the system catalogs (Tom Lane)</p>",
    "<p>A self-referential view can't work; it will always result in infinite recursion. We handled that situation correctly when trying to execute the view, but not when inquiring whether it is automatically updatable.</p>"
  ],
  [
    "<p>Ensure that walsender processes always show NULL for transaction start time in <code class=\"structname\">pg_stat_activity</code> (Álvaro Herrera)</p>",
    "<p>Previously, the <code class=\"structfield\">xact_start</code> column would sometimes show the process start time.</p>"
  ],
  [
    "<p>Improve performance of hash joins with very large inner relations (Thomas Munro)</p>"
  ],
  [
    "<p>Reduce spinlock contention when there are many active walsender processes (Pierre Ducroquet)</p>"
  ],
  [
    "<p>Fix placement of <span class=\"quote\">“<span class=\"quote\">Subplans Removed</span>”</span> field in <code class=\"command\">EXPLAIN</code> output (Daniel Gustafsson, Tom Lane)</p>",
    "<p>In non-text output formats, this field was emitted inside the <span class=\"quote\">“<span class=\"quote\">Plans</span>”</span> sub-group, resulting in syntactically invalid output. Attach it to the parent Append or MergeAppend plan node as intended. This causes the field to change position in text output format too: if there are any InitPlans attached to the same plan node, <span class=\"quote\">“<span class=\"quote\">Subplans Removed</span>”</span> will now appear before those.</p>"
  ],
  [
    "<p>Fix <code class=\"command\">EXPLAIN</code>'s <code class=\"literal\">SETTINGS</code> option to print as empty in non-text output formats (Tom Lane)</p>",
    "<p>In the non-text output formats, fields are supposed to appear when requested, even if they have empty or zero values.</p>"
  ],
  [
    "<p>Allow the planner to apply potentially-leaky tests to child-table statistics, if the user can read the corresponding column of the table that's actually named in the query (Dilip Kumar, Amit Langote)</p>",
    "<p>This change fixes a performance problem for partitioned tables that was created by the fix for CVE-2017-7484. That security fix disallowed applying leaky operators to statistics for columns that the current user doesn't have permission to read directly. However, it's somewhat common to grant permissions only on the parent partitioned table and not bother to do so on individual partitions. In such cases, the user can read the column via the parent, so there's no point in this security restriction; it only results in poorer planner estimates than necessary.</p>"
  ],
  [
    "<p>Fix planner errors induced by overly-aggressive collapsing of joins to single-row subqueries (Tom Lane)</p>",
    "<p>This mistake led to errors such as <span class=\"quote\">“<span class=\"quote\">failed to construct the join relation</span>”</span>.</p>"
  ],
  [
    "<p>Fix <span class=\"quote\">“<span class=\"quote\">no = operator for opfamily <em class=\"replaceable\"><code>NNNN</code></em></span>”</span> planner error when trying to match a <code class=\"literal\">LIKE</code> or regex pattern-match operator to a binary-compatible index opclass (Tom Lane)</p>"
  ],
  [
    "<p>Fix edge-case crashes and misestimations in selectivity calculations for the <code class=\"literal\">&lt;@</code> and <code class=\"literal\">@&gt;</code> range operators (Michael Paquier, Andrey Borodin, Tom Lane)</p>"
  ],
  [
    "<p>Fix incorrect estimation for <code class=\"literal\">OR</code> clauses when using most-common-value extended statistics (Tomas Vondra)</p>"
  ],
  [
    "<p>Ignore system columns when applying most-common-value extended statistics (Tomas Vondra)</p>",
    "<p>This prevents <span class=\"quote\">“<span class=\"quote\">negative bitmapset member not allowed</span>”</span> planner errors for affected queries.</p>"
  ],
  [
    "<p>Fix BRIN index logic to support hypothetical BRIN indexes (Julien Rouhaud, Heikki Linnakangas)</p>",
    "<p>Previously, if an <span class=\"quote\">“<span class=\"quote\">index adviser</span>”</span> extension tried to get the planner to produce a plan involving a hypothetical BRIN index, that would fail, because the BRIN cost estimation code would always try to physically access the index's metapage. Now it checks to see if the index is only hypothetical, and uses default assumptions about the index parameters if so.</p>"
  ],
  [
    "<p>Improve error reporting for attempts to use automatic updating of views with conditional <code class=\"literal\">INSTEAD</code> rules (Dean Rasheed)</p>",
    "<p>This has never been supported, but previously the error was thrown only at execution time, so that it could be masked by planner errors.</p>"
  ],
  [
    "<p>Prevent a composite type from being included in itself indirectly via a range type (Tom Lane, Julien Rouhaud)</p>"
  ],
  [
    "<p>Disallow partition key expressions that return pseudo-types, such as <code class=\"type\">record</code> (Tom Lane)</p>"
  ],
  [
    "<p>Fix error reporting for index expressions of prohibited types (Amit Langote)</p>"
  ],
  [
    "<p>Fix dumping of views that contain only a <code class=\"literal\">VALUES</code> list to handle cases where a view output column has been renamed (Tom Lane)</p>"
  ],
  [
    "<p>Ensure that data types and collations used in <code class=\"literal\">XMLTABLE</code> constructs are accounted for when computing dependencies of a view or rule (Tom Lane)</p>",
    "<p>Previously it was possible to break a view using <code class=\"literal\">XMLTABLE</code> by dropping a type, if the type was not otherwise referenced in the view. This fix does not correct the dependencies already recorded for existing views, only for newly-created ones.</p>"
  ],
  [
    "<p>Prevent unwanted downcasing and truncation of RADIUS authentication parameters (Marcos David)</p>",
    "<p>The <code class=\"filename\">pg_hba.conf</code> parser mistakenly treated these fields as SQL identifiers, which in general they aren't.</p>"
  ],
  [
    "<p>Transmit incoming <code class=\"command\">NOTIFY</code> messages to the client before sending <code class=\"literal\">ReadyForQuery</code>, rather than after (Tom Lane)</p>",
    "<p>This change ensures that, with libpq and other client libraries that act similarly to it, any notifications received during a transaction will be available by the time the client thinks the transaction is complete. This probably makes no difference in practical applications (which would need to cope with asynchronous notifications in any case); but it makes it easier to build test cases with reproducible behavior.</p>"
  ],
  [
    "<p>Fix bugs in handling of non-blocking I/O when using GSSAPI encryption (Tom Lane)</p>",
    "<p>These errors could result in dropping data (usually leading to subsequent wire-protocol-violation errors) or in a <span class=\"quote\">“<span class=\"quote\">livelock</span>”</span> situation where a sending process goes to sleep although not all its data has been sent. Moreover, <span class=\"application\">libpq</span> failed to keep separate encryption state for each connection, creating the possibility for failures in applications using multiple encrypted database connections.</p>"
  ],
  [
    "<p>Allow <span class=\"application\">libpq</span> to parse all GSS-related connection parameters even when the GSSAPI code hasn't been compiled in (Tom Lane)</p>",
    "<p>This makes the behavior similar to our SSL support, where it was long ago deemed to be a good idea to always accept all the related parameters, even if some are ignored or restricted due to lack of the feature in a particular build.</p>"
  ],
  [
    "<p>Fix incorrect handling of <code class=\"literal\">%b</code> and <code class=\"literal\">%B</code> format codes in <span class=\"application\">ecpg</span>'s <code class=\"function\">PGTYPEStimestamp_fmt_asc()</code> function (Tomas Vondra)</p>",
    "<p>Due to an off-by-one error, these codes would print the wrong month name, or possibly crash.</p>"
  ],
  [
    "<p>Avoid crash after an out-of-memory failure in <span class=\"application\">ecpglib</span> (Tom Lane)</p>"
  ],
  [
    "<p>Fix parallel <span class=\"application\">pg_dump</span>/<span class=\"application\">pg_restore</span> to more gracefully handle failure to create worker processes (Tom Lane)</p>"
  ],
  [
    "<p>Prevent possible crash or lockup when attempting to terminate a parallel <span class=\"application\">pg_dump</span>/<span class=\"application\">pg_restore</span> run via a signal (Tom Lane)</p>"
  ],
  [
    "<p>In <span class=\"application\">pg_upgrade</span>, look inside arrays and ranges while searching for non-upgradable data types in tables (Tom Lane)</p>"
  ],
  [
    "<p>Apply more thorough syntax checking to <span class=\"application\">createuser</span>'s <code class=\"option\">--connection-limit</code> option (Álvaro Herrera)</p>"
  ],
  [
    "<p>Cope with changes of the specific type referenced by a PL/pgSQL composite-type variable in more cases (Ashutosh Sharma, Tom Lane)</p>",
    "<p>Dropping and re-creating the composite type referenced by a PL/pgSQL variable could lead to <span class=\"quote\">“<span class=\"quote\">could not open relation with OID <em class=\"replaceable\"><code>NNNN</code></em></span>”</span> errors.</p>"
  ],
  [
    "<p>Avoid crash in <code class=\"filename\">postgres_fdw</code> when trying to send a command like <code class=\"literal\">UPDATE remote_tab SET (x,y) = (SELECT ...)</code> to the remote server (Tom Lane)</p>"
  ],
  [
    "<p>In <code class=\"filename\">contrib/dict_int</code>, reject <code class=\"varname\">maxlen</code> settings less than one (Tomas Vondra)</p>",
    "<p>This prevents a possible crash with silly settings for that parameter.</p>"
  ],
  [
    "<p>Disallow NULL category values in <code class=\"filename\">contrib/tablefunc</code>'s <code class=\"function\">crosstab()</code> function (Joe Conway)</p>",
    "<p>This case never worked usefully, and it would crash on some platforms.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">configure</span>'s probe for OpenSSL's <code class=\"function\">SSL_clear_options()</code> function so that it works with OpenSSL versions before 1.1.0 (Michael Paquier, Daniel Gustafsson)</p>",
    "<p>This problem could lead to failure to set the SSL compression option as desired, when <span class=\"productname\">PostgreSQL</span> is built against an old version of OpenSSL.</p>"
  ],
  [
    "<p>Mark some timeout and statistics-tracking GUC variables as <code class=\"literal\">PGDLLIMPORT</code>, to allow extensions to access them on Windows (Pascal Legrand)</p>",
    "<p>This applies to <code class=\"literal\">idle_in_transaction_session_timeout</code>, <code class=\"literal\">lock_timeout</code>, <code class=\"literal\">statement_timeout</code>, <code class=\"literal\">track_activities</code>, <code class=\"literal\">track_counts</code>, and <code class=\"literal\">track_functions</code>.</p>"
  ],
  [
    "<p>Avoid memory leak in sanity checks for <span class=\"quote\">“<span class=\"quote\">slab</span>”</span> memory contexts (Tomas Vondra)</p>",
    "<p>This isn't an issue for production builds, since they wouldn't ordinarily have memory context checking enabled; but the leak could be quite severe in a debug build.</p>"
  ],
  [
    "<p>Fix multiple statistics entries reported by the LWLock statistics mechanism (Fujii Masao)</p>",
    "<p>The LWLock statistics code (which is not built by default; it requires compiling with <code class=\"option\">-DLWLOCK_STATS</code>) could report multiple entries for the same LWLock and backend process, as a result of faulty hashtable key creation.</p>"
  ],
  [
    "<p>Fix race condition that led to delayed delivery of interprocess signals on Windows (Amit Kapila)</p>",
    "<p>This caused visible timing oddities in <code class=\"command\">NOTIFY</code>, and perhaps other misbehavior.</p>"
  ],
  [
    "<p>Fix handling of a corner-case error result from Windows' <code class=\"function\">ReadFile()</code> function (Thomas Munro, Juan José Santamaría Flecha)</p>",
    "<p>So far as is known, this oversight just resulted in noisy log messages, not any actual query misbehavior.</p>"
  ],
  [
    "<p>On Windows, retry a few times after an <code class=\"literal\">ERROR_ACCESS_DENIED</code> file access failure (Alexander Lakhin, Tom Lane)</p>",
    "<p>This helps cope with cases where a file open attempt fails because the targeted file is flagged for deletion but not yet actually gone. <span class=\"application\">pg_ctl</span>, for example, frequently failed with such an error when probing to see if the postmaster had shut down yet.</p>"
  ],
  [
    "<p>On Windows, work around sharing violations for the postmaster's log file when <span class=\"application\">pg_ctl</span> is used to start the postmaster very shortly after it's been stopped, for example by <code class=\"literal\">pg_ctl restart</code> (Alexander Lakhin)</p>"
  ]
]