[
  [
    "<p>Fix corner-case failures in <code class=\"function\">has_<em class=\"replaceable\"><code>foo</code></em>_privilege()</code>\n          family of functions (Tom Lane)</p>",
    "<p>Return NULL rather than throwing an error when an\n          invalid object OID is provided. Some of these functions\n          got that right already, but not all. <code class=\"function\">has_column_privilege()</code> was additionally\n          capable of crashing on some platforms.</p>"
  ],
  [
    "<p>Avoid O(N^2) slowdown in regular expression\n          match/split functions on long strings (Andrew Gierth)</p>"
  ],
  [
    "<p>Avoid O(N^3) slowdown in lexer for long strings of\n          <code class=\"literal\">+</code> or <code class=\"literal\">-</code> characters (Andrew Gierth)</p>"
  ],
  [
    "<p>Fix mis-execution of SubPlans when the outer query is\n          being scanned backwards (Andrew Gierth)</p>"
  ],
  [
    "<p>Fix failure of <code class=\"command\">UPDATE/DELETE ...\n          WHERE CURRENT OF ...</code> after rewinding the\n          referenced cursor (Tom Lane)</p>",
    "<p>A cursor that scans multiple relations (particularly\n          an inheritance tree) could produce wrong behavior if\n          rewound to an earlier relation.</p>"
  ],
  [
    "<p>Fix <code class=\"function\">EvalPlanQual</code> to\n          handle conditionally-executed InitPlans properly (Andrew\n          Gierth, Tom Lane)</p>",
    "<p>This resulted in hard-to-reproduce crashes or wrong\n          answers in concurrent updates, if they contained code\n          such as an uncorrelated sub-<code class=\"literal\">SELECT</code> inside a <code class=\"literal\">CASE</code> construct.</p>"
  ],
  [
    "<p>Fix character-class checks to not fail on Windows for\n          Unicode characters above U+FFFF (Tom Lane, Kenji Uno)</p>",
    "<p>This bug affected full-text-search operations, as well\n          as <code class=\"filename\">contrib/ltree</code> and\n          <code class=\"filename\">contrib/pg_trgm</code>.</p>"
  ],
  [
    "<p>Ensure that sequences owned by a foreign table are\n          processed by <code class=\"literal\">ALTER OWNER</code> on\n          the table (Peter Eisentraut)</p>",
    "<p>The ownership change should propagate to such\n          sequences as well, but this was missed for foreign\n          tables.</p>"
  ],
  [
    "<p>Fix over-allocation of space for <code class=\"function\">array_out()</code>'s result string (Keiichi\n          Hirobe)</p>"
  ],
  [
    "<p>Fix memory leak in repeated SP-GiST index scans (Tom\n          Lane)</p>",
    "<p>This is only known to amount to anything significant\n          in cases where an exclusion constraint using SP-GiST\n          receives many new index entries in a single command.</p>"
  ],
  [
    "<p>Avoid crash if a utility command causes infinite\n          recursion (Tom Lane)</p>"
  ],
  [
    "<p>When initializing a hot standby, cope with duplicate\n          XIDs caused by two-phase transactions on the master\n          (Michael Paquier, Konstantin Knizhnik)</p>"
  ],
  [
    "<p>Randomize the <code class=\"function\">random()</code>\n          seed in bootstrap and standalone backends, and in\n          <span class=\"application\">initdb</span> (Noah Misch)</p>",
    "<p>The main practical effect of this change is that it\n          avoids a scenario where <span class=\"application\">initdb</span> might mistakenly conclude\n          that POSIX shared memory is not available, due to name\n          collisions caused by always using the same random\n          seed.</p>"
  ],
  [
    "<p>Ensure that hot standby processes use the correct WAL\n          consistency point (Alexander Kukushkin, Michael\n          Paquier)</p>",
    "<p>This prevents possible misbehavior just after a\n          standby server has reached a consistent database state\n          during WAL replay.</p>"
  ],
  [
    "<p>Don't run atexit callbacks when servicing <code class=\"literal\">SIGQUIT</code> (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Don't record foreign-server user mappings as members\n          of extensions (Tom Lane)</p>",
    "<p>If <code class=\"command\">CREATE USER MAPPING</code> is\n          executed in an extension script, an extension dependency\n          was created for the user mapping, which is unexpected.\n          Roles can't be extension members, so user mappings\n          shouldn't be either.</p>"
  ],
  [
    "<p>Make syslogger more robust against failures in opening\n          CSV log files (Tom Lane)</p>"
  ],
  [
    "<p>Fix possible inconsistency in <span class=\"application\">pg_dump</span>'s sorting of dissimilar\n          object names (Jacob Champion)</p>"
  ],
  [
    "<p>Ensure that <span class=\"application\">pg_restore</span> will schema-qualify the\n          table name when emitting <code class=\"literal\">DISABLE</code>/<code class=\"literal\">ENABLE\n          TRIGGER</code> commands (Tom Lane)</p>",
    "<p>This avoids failures due to the new policy of running\n          restores with restrictive search path.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_upgrade</span> to\n          handle event triggers in extensions correctly (Haribabu\n          Kommi)</p>",
    "<p><span class=\"application\">pg_upgrade</span> failed to\n          preserve an event trigger's extension-membership\n          status.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_upgrade</span>'s\n          cluster state check to work correctly on a standby server\n          (Bruce Momjian)</p>"
  ],
  [
    "<p>Enforce type <code class=\"type\">cube</code>'s\n          dimension limit in all <code class=\"filename\">contrib/cube</code> functions (Andrey\n          Borodin)</p>",
    "<p>Previously, some cube-related functions could\n          construct values that would be rejected by <code class=\"function\">cube_in()</code>, leading to dump/reload\n          failures.</p>"
  ],
  [
    "<p>Fix <code class=\"filename\">contrib/unaccent</code>'s\n          <code class=\"function\">unaccent()</code> function to use\n          the <code class=\"literal\">unaccent</code> text search\n          dictionary that is in the same schema as the function\n          (Tom Lane)</p>",
    "<p>Previously it tried to look up the dictionary using\n          the search path, which could fail if the search path has\n          a restrictive value.</p>"
  ],
  [
    "<p>Fix build problems on macOS 10.14 (Mojave) (Tom\n          Lane)</p>",
    "<p>Adjust <span class=\"application\">configure</span> to\n          add an <code class=\"option\">-isysroot</code> switch to\n          <code class=\"varname\">CPPFLAGS</code>; without this,\n          PL/Perl and PL/Tcl fail to configure or build on macOS\n          10.14. The specific sysroot used can be overridden at\n          configure time or build time by setting the <code class=\"varname\">PG_SYSROOT</code> variable in the arguments of\n          <span class=\"application\">configure</span> or\n          <span class=\"application\">make</span>.</p>",
    "<p>It is now recommended that Perl-related extensions\n          write <code class=\"literal\">$(perl_includespec)</code>\n          rather than <code class=\"literal\">-I$(perl_archlibexp)/CORE</code> in their\n          compiler flags. The latter continues to work on most\n          platforms, but not recent macOS.</p>",
    "<p>Also, it should no longer be necessary to specify\n          <code class=\"option\">--with-tclconfig</code> manually to\n          get PL/Tcl to build on recent macOS releases.</p>"
  ],
  [
    "<p>Fix MSVC build and regression-test scripts to work on\n          recent Perl versions (Andrew Dunstan)</p>",
    "<p>Perl no longer includes the current directory in its\n          search path by default; work around that.</p>"
  ],
  [
    "<p>Support building on Windows with Visual Studio 2015 or\n          Visual Studio 2017 (Michael Paquier, Haribabu Kommi)</p>"
  ],
  [
    "<p>Allow btree comparison functions to return\n          <code class=\"literal\">INT_MIN</code> (Tom Lane)</p>",
    "<p>Up to now, we've forbidden datatype-specific\n          comparison functions from returning <code class=\"literal\">INT_MIN</code>, which allows callers to invert\n          the sort order just by negating the comparison result.\n          However, this was never safe for comparison functions\n          that directly return the result of <code class=\"function\">memcmp()</code>, <code class=\"function\">strcmp()</code>, etc, as POSIX doesn't place\n          any such restriction on those functions. At least some\n          recent versions of <code class=\"function\">memcmp()</code>\n          can return <code class=\"literal\">INT_MIN</code>, causing\n          incorrect sort ordering. Hence, we've removed this\n          restriction. Callers must now use the <code class=\"literal\">INVERT_COMPARE_RESULT()</code> macro if they\n          wish to invert the sort order.</p>"
  ],
  [
    "<p>Fix recursion hazard in shared-invalidation message\n          processing (Tom Lane)</p>",
    "<p>This error could, for example, result in failure to\n          access a system catalog or index that had just been\n          processed by <code class=\"command\">VACUUM\n          FULL</code>.</p>",
    "<p>This change adds a new result code for <code class=\"function\">LockAcquire</code>, which might possibly\n          affect external callers of that function, though only\n          very unusual usage patterns would have an issue with it.\n          The API of <code class=\"function\">LockAcquireExtended</code> is also\n          changed.</p>"
  ],
  [
    "<p>Save and restore SPI's global variables during\n          <code class=\"function\">SPI_connect()</code> and\n          <code class=\"function\">SPI_finish()</code> (Chapman\n          Flack, Tom Lane)</p>",
    "<p>This prevents possible interference when one SPI-using\n          function calls another.</p>"
  ],
  [
    "<p>Provide <code class=\"literal\">ALLOCSET_DEFAULT_SIZES</code> and sibling\n          macros in back branches (Tom Lane)</p>",
    "<p>These macros have existed since 9.6, but there were\n          requests to add them to older branches to allow\n          extensions to rely on them without branch-specific\n          coding.</p>"
  ],
  [
    "<p>Avoid using potentially-under-aligned page buffers\n          (Tom Lane)</p>",
    "<p>Invent new union types <code class=\"type\">PGAlignedBlock</code> and <code class=\"type\">PGAlignedXLogBlock</code>, and use these in place\n          of plain char arrays, ensuring that the compiler can't\n          place the buffer at a misaligned start address. This\n          fixes potential core dumps on alignment-picky platforms,\n          and may improve performance even on platforms that allow\n          misalignment.</p>"
  ],
  [
    "<p>Make <code class=\"filename\">src/port/snprintf.c</code>\n          follow the C99 standard's definition of <code class=\"function\">snprintf()</code>'s result value (Tom\n          Lane)</p>",
    "<p>On platforms where this code is used (mostly Windows),\n          its pre-C99 behavior could lead to failure to detect\n          buffer overrun, if the calling code assumed C99\n          semantics.</p>"
  ],
  [
    "<p>When building on i386 with the <span class=\"application\">clang</span> compiler, require <code class=\"option\">-msse2</code> to be used (Andres Freund)</p>",
    "<p>This avoids problems with missed floating point\n          overflow checks.</p>"
  ],
  [
    "<p>Fix <span class=\"application\">configure</span>'s\n          detection of the result type of <code class=\"function\">strerror_r()</code> (Tom Lane)</p>",
    "<p>The previous coding got the wrong answer when building\n          with <span class=\"application\">icc</span> on Linux (and\n          perhaps in other cases), leading to <span class=\"application\">libpq</span> not returning useful error\n          messages for system-reported errors.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2018g for DST law\n          changes in Chile, Fiji, Morocco, and Russia (Volgograd),\n          plus historical corrections for China, Hawaii, Japan,\n          Macau, and North Korea.</p>"
  ]
]