[
  [
    "<p>Fix GiST index corruption due to marking the wrong\n          index entry <span class=\"quote\">&#x201C;<span class=\"quote\">dead</span>&#x201D;</span> after a deletion (Teodor)</p>",
    "<p>This would result in index searches failing to find\n          rows they should have found. Corrupted indexes can be\n          fixed with <code class=\"command\">REINDEX</code>.</p>"
  ],
  [
    "<p>Fix backend crash when the client encoding cannot\n          represent a localized error message (Tom)</p>",
    "<p>We have addressed similar issues before, but it would\n          still fail if the <span class=\"quote\">&#x201C;<span class=\"quote\">character has no equivalent</span>&#x201D;</span>\n          message itself couldn't be converted. The fix is to\n          disable localization and send the plain ASCII error\n          message when we detect such a situation.</p>"
  ],
  [
    "<p>Fix possible crash in <code class=\"type\">bytea</code>-to-XML mapping (Michael McMaster)</p>"
  ],
  [
    "<p>Fix possible crash when deeply nested functions are\n          invoked from a trigger (Tom)</p>"
  ],
  [
    "<p>Improve optimization of <em class=\"replaceable\"><code>expression</code></em> <code class=\"literal\">IN</code> (<em class=\"replaceable\"><code>expression-list</code></em>) queries\n          (Tom, per an idea from Robert Haas)</p>",
    "<p>Cases in which there are query variables on the\n          right-hand side had been handled less efficiently in\n          8.2.x and 8.3.x than in prior versions. The fix restores\n          8.1 behavior for such cases.</p>"
  ],
  [
    "<p>Fix mis-expansion of rule queries when a\n          sub-<code class=\"literal\">SELECT</code> appears in a\n          function call in <code class=\"literal\">FROM</code>, a\n          multi-row <code class=\"literal\">VALUES</code> list, or a\n          <code class=\"literal\">RETURNING</code> list (Tom)</p>",
    "<p>The usual symptom of this problem is an <span class=\"quote\">&#x201C;<span class=\"quote\">unrecognized node\n          type</span>&#x201D;</span> error.</p>"
  ],
  [
    "<p>Fix Assert failure during rescan of an <code class=\"literal\">IS NULL</code> search of a GiST index\n          (Teodor)</p>"
  ],
  [
    "<p>Fix memory leak during rescan of a hashed aggregation\n          plan (Neil)</p>"
  ],
  [
    "<p>Ensure an error is reported when a newly-defined\n          PL/pgSQL trigger function is invoked as a normal function\n          (Tom)</p>"
  ],
  [
    "<p>Force a checkpoint before <code class=\"command\">CREATE\n          DATABASE</code> starts to copy files (Heikki)</p>",
    "<p>This prevents a possible failure if files had recently\n          been deleted in the source database.</p>"
  ],
  [
    "<p>Prevent possible collision of <code class=\"structfield\">relfilenode</code> numbers when moving a\n          table to another tablespace with <code class=\"command\">ALTER SET TABLESPACE</code> (Heikki)</p>",
    "<p>The command tried to re-use the existing filename,\n          instead of picking one that is known unused in the\n          destination directory.</p>"
  ],
  [
    "<p>Fix incorrect text search headline generation when\n          single query item matches first word of text (Sushant\n          Sinha)</p>"
  ],
  [
    "<p>Fix improper display of fractional seconds in interval\n          values when using a non-ISO datestyle in an <code class=\"option\">--enable-integer-datetimes</code> build (Ron\n          Mayer)</p>"
  ],
  [
    "<p>Make <code class=\"literal\">ILIKE</code> compare\n          characters case-insensitively even when they're escaped\n          (Andrew)</p>"
  ],
  [
    "<p>Ensure <code class=\"command\">DISCARD</code> is handled\n          properly by statement logging (Tom)</p>"
  ],
  [
    "<p>Fix incorrect logging of last-completed-transaction\n          time during PITR recovery (Tom)</p>"
  ],
  [
    "<p>Ensure <code class=\"function\">SPI_getvalue</code> and\n          <code class=\"function\">SPI_getbinval</code> behave\n          correctly when the passed tuple and tuple descriptor have\n          different numbers of columns (Tom)</p>",
    "<p>This situation is normal when a table has had columns\n          added or removed, but these two functions didn't handle\n          it properly. The only likely consequence is an incorrect\n          error indication.</p>"
  ],
  [
    "<p>Mark <code class=\"varname\">SessionReplicationRole</code> as <code class=\"literal\">PGDLLIMPORT</code> so it can be used by\n          <span class=\"application\">Slony</span> on Windows\n          (Magnus)</p>"
  ],
  [
    "<p>Fix small memory leak when using <span class=\"application\">libpq</span>'s <code class=\"literal\">gsslib</code> parameter (Magnus)</p>",
    "<p>The space used by the parameter string was not freed\n          at connection close.</p>"
  ],
  [
    "<p>Ensure <span class=\"application\">libgssapi</span> is\n          linked into <span class=\"application\">libpq</span> if\n          needed (Markus Schaaf)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">ecpg</span>'s parsing of\n          <code class=\"command\">CREATE ROLE</code> (Michael)</p>"
  ],
  [
    "<p>Fix recent breakage of <code class=\"literal\">pg_ctl\n          restart</code> (Tom)</p>"
  ],
  [
    "<p>Ensure <code class=\"filename\">pg_control</code> is\n          opened in binary mode (Itagaki Takahiro)</p>",
    "<p><span class=\"application\">pg_controldata</span> and\n          <span class=\"application\">pg_resetxlog</span> did this\n          incorrectly, and so could fail on Windows.</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2008i (for DST law\n          changes in Argentina, Brazil, Mauritius, Syria)</p>"
  ]
]