[
  [
    "<p>Disallow <code class=\"command\">RESET ROLE</code> and\n          <code class=\"command\">RESET SESSION AUTHORIZATION</code>\n          inside security-definer functions (Tom, Heikki)</p>",
    "<p>This covers a case that was missed in the previous\n          patch that disallowed <code class=\"command\">SET\n          ROLE</code> and <code class=\"command\">SET SESSION\n          AUTHORIZATION</code> inside security-definer functions.\n          (See CVE-2007-6600)</p>"
  ],
  [
    "<p>Fix handling of sub-SELECTs appearing in the arguments\n          of an outer-level aggregate function (Tom)</p>"
  ],
  [
    "<p>Fix hash calculation for data type <code class=\"type\">interval</code> (Tom)</p>",
    "<p>This corrects wrong results for hash joins on interval\n          values. It also changes the contents of hash indexes on\n          interval columns. If you have any such indexes, you must\n          <code class=\"command\">REINDEX</code> them after\n          updating.</p>"
  ],
  [
    "<p>Treat <code class=\"function\">to_char(..., 'TH')</code>\n          as an uppercase ordinal suffix with <code class=\"literal\">'HH'</code>/<code class=\"literal\">'HH12'</code>\n          (Heikki)</p>",
    "<p>It was previously handled as <code class=\"literal\">'th'</code> (lowercase).</p>"
  ],
  [
    "<p>Fix overflow for <code class=\"literal\">INTERVAL\n          '<em class=\"replaceable\"><code>x</code></em> ms'</code>\n          when <em class=\"replaceable\"><code>x</code></em> is more\n          than 2 million and integer datetimes are in use (Alex\n          Hunsaker)</p>"
  ],
  [
    "<p>Fix calculation of distance between a point and a line\n          segment (Tom)</p>",
    "<p>This led to incorrect results from a number of\n          geometric operators.</p>"
  ],
  [
    "<p>Fix <code class=\"type\">money</code> data type to work\n          in locales where currency amounts have no fractional\n          digits, e.g. Japan (Itagaki Takahiro)</p>"
  ],
  [
    "<p>Properly round datetime input like <code class=\"literal\">00:12:57.9999999999999999999999999999</code>\n          (Tom)</p>"
  ],
  [
    "<p>Fix poor choice of page split point in GiST R-tree\n          operator classes (Teodor)</p>"
  ],
  [
    "<p>Fix portability issues in plperl initialization\n          (Andrew Dunstan)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pg_ctl</span> to not go\n          into an infinite loop if <code class=\"filename\">postgresql.conf</code> is empty (Jeff\n          Davis)</p>"
  ],
  [
    "<p>Fix <code class=\"filename\">contrib/xml2</code>'s\n          <code class=\"function\">xslt_process()</code> to properly\n          handle the maximum number of parameters (twenty)\n          (Tom)</p>"
  ],
  [
    "<p>Improve robustness of <span class=\"application\">libpq</span>'s code to recover from errors\n          during <code class=\"command\">COPY FROM STDIN</code>\n          (Tom)</p>"
  ],
  [
    "<p>Avoid including conflicting readline and editline\n          header files when both libraries are installed (Zdenek\n          Kotala)</p>"
  ],
  [
    "<p>Update time zone data files to <span class=\"application\">tzdata</span> release 2009l for DST law\n          changes in Bangladesh, Egypt, Jordan, Pakistan,\n          Argentina/San_Luis, Cuba, Jordan (historical correction\n          only), Mauritius, Morocco, Palestine, Syria, Tunisia.</p>"
  ]
]