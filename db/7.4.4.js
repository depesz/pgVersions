[
  [
    "<p>Prevent possible loss of committed transactions during\n          crash</p>",
    "<p>Due to insufficient interlocking between transaction\n          commit and checkpointing, it was possible for\n          transactions committed just before the most recent\n          checkpoint to be lost, in whole or in part, following a\n          database crash and restart. This is a serious bug that\n          has existed since <span class=\"productname\">PostgreSQL</span> 7.1.</p>"
  ],
  [
    "<p>Check HAVING restriction before evaluating result list\n          of an aggregate plan</p>"
  ],
  [
    "<p>Avoid crash when session's current user ID is\n          deleted</p>"
  ],
  [
    "<p>Fix hashed crosstab for zero-rows case (Joe)</p>"
  ],
  [
    "<p>Force cache update after renaming a column in a\n          foreign key</p>"
  ],
  [
    "<p>Pretty-print UNION queries correctly</p>"
  ],
  [
    "<p>Make psql handle <code class=\"literal\">\\r\\n</code>\n          newlines properly in COPY IN</p>"
  ],
  [
    "<p><span class=\"application\">pg_dump</span> handled ACLs\n          with grant options incorrectly</p>"
  ],
  [
    "<p>Fix thread support for macOS and Solaris</p>"
  ],
  [
    "<p>Updated JDBC driver (build 215) with various fixes</p>"
  ],
  [
    "<p>ECPG fixes</p>"
  ],
  [
    "<p>Translation updates (various contributors)</p>"
  ]
]