[
  [
    "<p>Further restrict visibility of <tt class=\"STRUCTNAME\">pg_user_mappings</tt>.<tt class=\"STRUCTFIELD\">umoptions</tt>, to protect passwords stored as user mapping options (Noah Misch)</p>",
    "<p>The fix for CVE-2017-7486 was incorrect: it allowed a user to see the options in her own user mapping, even if she did not have <tt class=\"LITERAL\">USAGE</tt> permission on the associated foreign server. Such options might include a password that had been provided by the server owner rather than the user herself. Since <tt class=\"STRUCTNAME\">information_schema.user_mapping_options</tt> does not show the options in such cases, <tt class=\"STRUCTNAME\">pg_user_mappings</tt> should not either. (CVE-2017-7547)</p>",
    "<p>By itself, this patch will only fix the behavior in newly initdb'd databases. If you wish to apply this change in an existing database, you will need to do the following:</p>",
    "<div class=\"PROCEDURE\">\n<ol type=\"1\">\n<li class=\"STEP\">\n<p>Restart the postmaster after adding <tt class=\"LITERAL\">allow_system_table_mods = true</tt> to <tt class=\"FILENAME\">postgresql.conf</tt>. (In versions supporting <tt class=\"COMMAND\">ALTER SYSTEM</tt>, you can use that to make the configuration change, but you'll still need a restart.)</p>\n</li>\n<li class=\"STEP\">\n<p>In <span class=\"emphasis EMPHASIS c2\">each</span> database of the cluster, run the following commands as superuser:</p>\n<pre class=\"PROGRAMLISTING\">SET search_path = pg_catalog;\nCREATE OR REPLACE VIEW pg_user_mappings AS\n    SELECT\n        U.oid       AS umid,\n        S.oid       AS srvid,\n        S.srvname   AS srvname,\n        U.umuser    AS umuser,\n        CASE WHEN U.umuser = 0 THEN\n            'public'\n        ELSE\n            A.rolname\n        END AS usename,\n        CASE WHEN (U.umuser &lt;&gt; 0 AND A.rolname = current_user\n                     AND (pg_has_role(S.srvowner, 'USAGE')\n                          OR has_server_privilege(S.oid, 'USAGE')))\n                    OR (U.umuser = 0 AND pg_has_role(S.srvowner, 'USAGE'))\n                    OR (SELECT rolsuper FROM pg_authid WHERE rolname = current_user)\n                    THEN U.umoptions\n                 ELSE NULL END AS umoptions\n    FROM pg_user_mapping U\n         LEFT JOIN pg_authid A ON (A.oid = U.umuser) JOIN\n        pg_foreign_server S ON (U.umserver = S.oid);</pre></li>\n<li class=\"STEP\">\n<p>Do not forget to include the <tt class=\"LITERAL\">template0</tt> and <tt class=\"LITERAL\">template1</tt> databases, or the vulnerability will still exist in databases you create later. To fix <tt class=\"LITERAL\">template0</tt>, you'll need to temporarily make it accept connections. In <span class=\"PRODUCTNAME\">PostgreSQL</span> 9.5 and later, you can use</p>\n<pre class=\"PROGRAMLISTING\">ALTER DATABASE template0 WITH ALLOW_CONNECTIONS true;</pre>\n<p>and then after fixing <tt class=\"LITERAL\">template0</tt>, undo that with</p>\n<pre class=\"PROGRAMLISTING\">ALTER DATABASE template0 WITH ALLOW_CONNECTIONS false;</pre>\n<p>In prior versions, instead use</p>\n<pre class=\"PROGRAMLISTING\">UPDATE pg_database SET datallowconn = true WHERE datname = 'template0';\nUPDATE pg_database SET datallowconn = false WHERE datname = 'template0';</pre></li>\n<li class=\"STEP\">\n<p>Finally, remove the <tt class=\"LITERAL\">allow_system_table_mods</tt> configuration setting, and again restart the postmaster.</p>\n</li>\n</ol>\n</div>"
  ],
  [
    "<p>Disallow empty passwords in all password-based authentication methods (Heikki Linnakangas)</p>",
    "<p><span class=\"APPLICATION\">libpq</span> ignores empty password specifications, and does not transmit them to the server. So, if a user's password has been set to the empty string, it's impossible to log in with that password via <span class=\"APPLICATION\">psql</span> or other <span class=\"APPLICATION\">libpq</span>-based clients. An administrator might therefore believe that setting the password to empty is equivalent to disabling password login. However, with a modified or non-<span class=\"APPLICATION\">libpq</span>-based client, logging in could be possible, depending on which authentication method is configured. In particular the most common method, <tt class=\"LITERAL\">md5</tt>, accepted empty passwords. Change the server to reject empty passwords in all cases. (CVE-2017-7546)</p>"
  ],
  [
    "<p>Make <code class=\"FUNCTION\">lo_put()</code> check for <tt class=\"LITERAL\">UPDATE</tt> privilege on the target large object (Tom Lane, Michael Paquier)</p>",
    "<p><code class=\"FUNCTION\">lo_put()</code> should surely require the same permissions as <code class=\"FUNCTION\">lowrite()</code>, but the check was missing, allowing any user to change the data in a large object. (CVE-2017-7548)</p>"
  ],
  [
    "<p>Correct the documentation about the process for upgrading standby servers with <span class=\"APPLICATION\">pg_upgrade</span> (Bruce Momjian)</p>",
    "<p>The previous documentation instructed users to start/stop the primary server after running <span class=\"APPLICATION\">pg_upgrade</span> but before syncing the standby servers. This sequence is unsafe.</p>"
  ],
  [
    "<p>Fix concurrent locking of tuple update chains (Álvaro Herrera)</p>",
    "<p>If several sessions concurrently lock a tuple update chain with nonconflicting lock modes using an old snapshot, and they all succeed, it was possible for some of them to nonetheless fail (and conclude there is no live tuple version) due to a race condition. This had consequences such as foreign-key checks failing to see a tuple that definitely exists but is being updated concurrently.</p>"
  ],
  [
    "<p>Fix potential data corruption when freezing a tuple whose XMAX is a multixact with exactly one still-interesting member (Teodor Sigaev)</p>"
  ],
  [
    "<p>Avoid integer overflow and ensuing crash when sorting more than one billion tuples in-memory (Sergey Koposov)</p>"
  ],
  [
    "<p>On Windows, retry process creation if we fail to reserve the address range for our shared memory in the new process (Tom Lane, Amit Kapila)</p>",
    "<p>This is expected to fix infrequent child-process-launch failures that are probably due to interference from antivirus products.</p>"
  ],
  [
    "<p>Fix low-probability corruption of shared predicate-lock hash table in Windows builds (Thomas Munro, Tom Lane)</p>"
  ],
  [
    "<p>Avoid logging clean closure of an SSL connection as though it were a connection reset (Michael Paquier)</p>"
  ],
  [
    "<p>Prevent sending SSL session tickets to clients (Tom Lane)</p>",
    "<p>This fix prevents reconnection failures with ticket-aware client-side SSL code.</p>"
  ],
  [
    "<p>Fix code for setting <a href=\"https://www.postgresql.org/docs/9.5/runtime-config-connection.html#GUC-TCP-KEEPALIVES-IDLE\">tcp_keepalives_idle</a> on Solaris (Tom Lane)</p>"
  ],
  [
    "<p>Fix statistics collector to honor inquiry messages issued just after a postmaster shutdown and immediate restart (Tom Lane)</p>",
    "<p>Statistics inquiries issued within half a second of the previous postmaster shutdown were effectively ignored.</p>"
  ],
  [
    "<p>Ensure that the statistics collector's receive buffer size is at least 100KB (Tom Lane)</p>",
    "<p>This reduces the risk of dropped statistics data on older platforms whose default receive buffer size is less than that.</p>"
  ],
  [
    "<p>Fix possible creation of an invalid WAL segment when a standby is promoted just after it processes an <tt class=\"LITERAL\">XLOG_SWITCH</tt> WAL record (Andres Freund)</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">walsender</span> to exit promptly when client requests shutdown (Tom Lane)</p>"
  ],
  [
    "<p>Fix <span class=\"SYSTEMITEM\">SIGHUP</span> and <span class=\"SYSTEMITEM\">SIGUSR1</span> handling in walsender processes (Petr Jelinek, Andres Freund)</p>"
  ],
  [
    "<p>Prevent walsender-triggered panics during shutdown checkpoints (Andres Freund, Michael Paquier)</p>"
  ],
  [
    "<p>Fix unnecessarily slow restarts of <span class=\"APPLICATION\">walreceiver</span> processes due to race condition in postmaster (Tom Lane)</p>"
  ],
  [
    "<p>Fix leakage of small subtransactions spilled to disk during logical decoding (Andres Freund)</p>",
    "<p>This resulted in temporary files consuming excessive disk space.</p>"
  ],
  [
    "<p>Reduce the work needed to build snapshots during creation of logical-decoding slots (Andres Freund, Petr Jelinek)</p>",
    "<p>The previous algorithm was infeasibly expensive on a server with a lot of open transactions.</p>"
  ],
  [
    "<p>Fix race condition that could indefinitely delay creation of logical-decoding slots (Andres Freund, Petr Jelinek)</p>"
  ],
  [
    "<p>Reduce overhead in processing syscache invalidation events (Tom Lane)</p>",
    "<p>This is particularly helpful for logical decoding, which triggers frequent cache invalidation.</p>"
  ],
  [
    "<p>Fix cases where an <tt class=\"COMMAND\">INSERT</tt> or <tt class=\"COMMAND\">UPDATE</tt> assigns to more than one element of a column that is of domain-over-array type (Tom Lane)</p>"
  ],
  [
    "<p>Allow window functions to be used in sub-<tt class=\"LITERAL\">SELECT</tt>s that are within the arguments of an aggregate function (Tom Lane)</p>"
  ],
  [
    "<p>Move autogenerated array types out of the way during <tt class=\"COMMAND\">ALTER ... RENAME</tt> (Vik Fearing)</p>",
    "<p>Previously, we would rename a conflicting autogenerated array type out of the way during <tt class=\"COMMAND\">CREATE</tt>; this fix extends that behavior to renaming operations.</p>"
  ],
  [
    "<p>Fix dangling pointer in <tt class=\"COMMAND\">ALTER TABLE</tt> when there is a comment on a constraint belonging to the table (David Rowley)</p>",
    "<p>Re-applying the comment to the reconstructed constraint could fail with a weird error message, or even crash.</p>"
  ],
  [
    "<p>Ensure that <tt class=\"COMMAND\">ALTER USER ... SET</tt> accepts all the syntax variants that <tt class=\"COMMAND\">ALTER ROLE ... SET</tt> does (Peter Eisentraut)</p>"
  ],
  [
    "<p>Properly update dependency info when changing a datatype I/O function's argument or return type from <tt class=\"TYPE\">opaque</tt> to the correct type (Heikki Linnakangas)</p>",
    "<p><tt class=\"COMMAND\">CREATE TYPE</tt> updates I/O functions declared in this long-obsolete style, but it forgot to record a dependency on the type, allowing a subsequent <tt class=\"COMMAND\">DROP TYPE</tt> to leave broken function definitions behind.</p>"
  ],
  [
    "<p>Reduce memory usage when <tt class=\"COMMAND\">ANALYZE</tt> processes a <tt class=\"TYPE\">tsvector</tt> column (Heikki Linnakangas)</p>"
  ],
  [
    "<p>Fix unnecessary precision loss and sloppy rounding when multiplying or dividing <tt class=\"TYPE\">money</tt> values by integers or floats (Tom Lane)</p>"
  ],
  [
    "<p>Tighten checks for whitespace in functions that parse identifiers, such as <code class=\"FUNCTION\">regprocedurein()</code> (Tom Lane)</p>",
    "<p>Depending on the prevailing locale, these functions could misinterpret fragments of multibyte characters as whitespace.</p>"
  ],
  [
    "<p>Use relevant <tt class=\"LITERAL\">#define</tt> symbols from Perl while compiling <span class=\"APPLICATION\">PL/Perl</span> (Ashutosh Sharma, Tom Lane)</p>",
    "<p>This avoids portability problems, typically manifesting as a <span class=\"QUOTE\">\"handshake\"</span> mismatch during library load, when working with recent Perl versions.</p>"
  ],
  [
    "<p>In <span class=\"APPLICATION\">libpq</span>, reset GSS/SASL and SSPI authentication state properly after a failed connection attempt (Michael Paquier)</p>",
    "<p>Failure to do this meant that when falling back from SSL to non-SSL connections, a GSS/SASL failure in the SSL attempt would always cause the non-SSL attempt to fail. SSPI did not fail, but it leaked memory.</p>"
  ],
  [
    "<p>In <span class=\"APPLICATION\">psql</span>, fix failure when <tt class=\"COMMAND\">COPY FROM STDIN</tt> is ended with a keyboard EOF signal and then another <tt class=\"COMMAND\">COPY FROM STDIN</tt> is attempted (Thomas Munro)</p>",
    "<p>This misbehavior was observed on BSD-derived platforms (including macOS), but not on most others.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_dump</span> and <span class=\"APPLICATION\">pg_restore</span> to emit <tt class=\"COMMAND\">REFRESH MATERIALIZED VIEW</tt> commands last (Tom Lane)</p>",
    "<p>This prevents errors during dump/restore when a materialized view refers to tables owned by a different user.</p>"
  ],
  [
    "<p>Improve <span class=\"APPLICATION\">pg_dump</span>/<span class=\"APPLICATION\">pg_restore</span>'s reporting of error conditions originating in <span class=\"APPLICATION\">zlib</span> (Vladimir Kunschikov, Álvaro Herrera)</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_dump</span> with the <tt class=\"OPTION\">--clean</tt> option to drop event triggers as expected (Tom Lane)</p>",
    "<p>It also now correctly assigns ownership of event triggers; before, they were restored as being owned by the superuser running the restore script.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_dump</span> to not emit invalid SQL for an empty operator class (Daniel Gustafsson)</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_dump</span> output to stdout on Windows (Kuntal Ghosh)</p>",
    "<p>A compressed plain-text dump written to stdout would contain corrupt data due to failure to put the file descriptor into binary mode.</p>"
  ],
  [
    "<p>Fix <code class=\"FUNCTION\">pg_get_ruledef()</code> to print correct output for the <tt class=\"LITERAL\">ON SELECT</tt> rule of a view whose columns have been renamed (Tom Lane)</p>",
    "<p>In some corner cases, <span class=\"APPLICATION\">pg_dump</span> relies on <code class=\"FUNCTION\">pg_get_ruledef()</code> to dump views, so that this error could result in dump/reload failures.</p>"
  ],
  [
    "<p>Fix dumping of outer joins with empty constraints, such as the result of a <tt class=\"LITERAL\">NATURAL LEFT JOIN</tt> with no common columns (Tom Lane)</p>"
  ],
  [
    "<p>Fix dumping of function expressions in the <tt class=\"LITERAL\">FROM</tt> clause in cases where the expression does not deparse into something that looks like a function call (Tom Lane)</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_basebackup</span> output to stdout on Windows (Haribabu Kommi)</p>",
    "<p>A backup written to stdout would contain corrupt data due to failure to put the file descriptor into binary mode.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_rewind</span> to correctly handle files exceeding 2GB (Kuntal Ghosh, Michael Paquier)</p>",
    "<p>Ordinarily such files won't appear in <span class=\"PRODUCTNAME\">PostgreSQL</span> data directories, but they could be present in some cases.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_upgrade</span> to ensure that the ending WAL record does not have <a href=\"https://www.postgresql.org/docs/9.5/runtime-config-wal.html#GUC-WAL-LEVEL\">wal_level</a> = <tt class=\"LITERAL\">minimum</tt> (Bruce Momjian)</p>",
    "<p>This condition could prevent upgraded standby servers from reconnecting.</p>"
  ],
  [
    "<p>Fix <span class=\"APPLICATION\">pg_xlogdump</span>'s computation of WAL record length (Andres Freund)</p>"
  ],
  [
    "<p>In <tt class=\"FILENAME\">postgres_fdw</tt>, re-establish connections to remote servers after <tt class=\"COMMAND\">ALTER SERVER</tt> or <tt class=\"COMMAND\">ALTER USER MAPPING</tt> commands (Kyotaro Horiguchi)</p>",
    "<p>This ensures that option changes affecting connection parameters will be applied promptly.</p>"
  ],
  [
    "<p>In <tt class=\"FILENAME\">postgres_fdw</tt>, allow cancellation of remote transaction control commands (Robert Haas, Rafia Sabih)</p>",
    "<p>This change allows us to quickly escape a wait for an unresponsive remote server in many more cases than previously.</p>"
  ],
  [
    "<p>Increase <tt class=\"LITERAL\">MAX_SYSCACHE_CALLBACKS</tt> to provide more room for extensions (Tom Lane)</p>"
  ],
  [
    "<p>Always use <tt class=\"OPTION\">-fPIC</tt>, not <tt class=\"OPTION\">-fpic</tt>, when building shared libraries with gcc (Tom Lane)</p>",
    "<p>This supports larger extension libraries on platforms where it makes a difference.</p>"
  ],
  [
    "<p>Fix unescaped-braces issue in our build scripts for Microsoft MSVC, to avoid a warning or error from recent Perl versions (Andrew Dunstan)</p>"
  ],
  [
    "<p>In MSVC builds, handle the case where the <span class=\"APPLICATION\">openssl</span> library is not within a <tt class=\"FILENAME\">VC</tt> subdirectory (Andrew Dunstan)</p>"
  ],
  [
    "<p>In MSVC builds, add proper include path for <span class=\"APPLICATION\">libxml2</span> header files (Andrew Dunstan)</p>",
    "<p>This fixes a former need to move things around in standard Windows installations of <span class=\"APPLICATION\">libxml2</span>.</p>"
  ],
  [
    "<p>In MSVC builds, recognize a Tcl library that is named <tt class=\"FILENAME\">tcl86.lib</tt> (Noah Misch)</p>"
  ],
  [
    "<p>In MSVC builds, honor <tt class=\"LITERAL\">PROVE_FLAGS</tt> settings on <tt class=\"FILENAME\">vcregress.pl</tt>'s command line (Andrew Dunstan)</p>"
  ]
]