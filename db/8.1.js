[
  [
    "<p>Improve GiST and R-tree index performance (Neil)</p>"
  ],
  [
    "<p>Improve the optimizer, including auto-resizing of\n            hash joins (Tom)</p>"
  ],
  [
    "<p>Overhaul internal API in several areas</p>"
  ],
  [
    "<p>Change WAL record CRCs from 64-bit to 32-bit\n            (Tom)</p>",
    "<p>We determined that the extra cost of computing\n            64-bit CRCs was significant, and the gain in\n            reliability too marginal to justify it.</p>"
  ],
  [
    "<p>Prevent writing large empty gaps in WAL pages\n            (Tom)</p>"
  ],
  [
    "<p>Improve spinlock behavior on SMP machines,\n            particularly Opterons (Tom)</p>"
  ],
  [
    "<p>Allow nonconsecutive index columns to be used in a\n            multicolumn index (Tom)</p>",
    "<p>For example, this allows an index on columns a,b,c\n            to be used in a query with <code class=\"command\">WHERE\n            a = 4 and c = 10</code>.</p>"
  ],
  [
    "<p>Skip WAL logging for <code class=\"command\">CREATE\n            TABLE AS</code> / <code class=\"command\">SELECT\n            INTO</code> (Simon)</p>",
    "<p>Since a crash during <code class=\"command\">CREATE\n            TABLE AS</code> would cause the table to be dropped\n            during recovery, there is no reason to WAL log as the\n            table is loaded. (Logging still happens if WAL\n            archiving is enabled, however.)</p>"
  ],
  [
    "<p>Allow concurrent GiST index access (Teodor,\n            Oleg)</p>"
  ],
  [
    "<p>Add configuration parameter <code class=\"varname\">full_page_writes</code> to control writing\n            full pages to WAL (Bruce)</p>",
    "<p>To prevent partial disk writes from corrupting the\n            database, <span class=\"productname\">PostgreSQL</span>\n            writes a complete copy of each database disk page to\n            WAL the first time it is modified after a checkpoint.\n            This option turns off that functionality for more\n            speed. This is safe to use with battery-backed disk\n            caches where partial page writes cannot happen.</p>"
  ],
  [
    "<p>Use <code class=\"literal\">O_DIRECT</code> if\n            available when using <code class=\"literal\">O_SYNC</code> for <code class=\"varname\">wal_sync_method</code> (Itagaki Takahiro)</p>",
    "<p><code class=\"literal\">O_DIRECT</code> causes disk\n            writes to bypass the kernel cache, and for WAL writes,\n            this improves performance.</p>"
  ],
  [
    "<p>Improve <code class=\"command\">COPY FROM</code>\n            performance (Alon Goldshuv)</p>",
    "<p>This was accomplished by reading <code class=\"command\">COPY</code> input in larger chunks, rather\n            than character by character.</p>"
  ],
  [
    "<p>Improve the performance of <code class=\"function\">COUNT()</code>, <code class=\"function\">SUM</code>, <code class=\"function\">AVG()</code>, <code class=\"function\">STDDEV()</code>, and <code class=\"function\">VARIANCE()</code> (Neil, Tom)</p>"
  ],
  [
    "<p>Prevent problems due to transaction ID (XID)\n            wraparound (Tom)</p>",
    "<p>The server will now warn when the transaction\n            counter approaches the wraparound point. If the counter\n            becomes too close to wraparound, the server will stop\n            accepting queries. This ensures that data is not lost\n            before needed vacuuming is performed.</p>"
  ],
  [
    "<p>Fix problems with object IDs (OIDs) conflicting with\n            existing system objects after the OID counter has\n            wrapped around (Tom)</p>"
  ],
  [
    "<p>Add warning about the need to increase <code class=\"varname\">max_fsm_relations</code> and <code class=\"varname\">max_fsm_pages</code> during <code class=\"command\">VACUUM</code> (Ron Mayer)</p>"
  ],
  [
    "<p>Add <code class=\"varname\">temp_buffers</code>\n            configuration parameter to allow users to determine the\n            size of the local buffer area for temporary table\n            access (Tom)</p>"
  ],
  [
    "<p>Add session start time and client IP address to\n            <code class=\"literal\">pg_stat_activity</code>\n            (Magnus)</p>"
  ],
  [
    "<p>Adjust <code class=\"literal\">pg_stat</code> views\n            for bitmap scans (Tom)</p>",
    "<p>The meanings of some of the fields have changed\n            slightly.</p>"
  ],
  [
    "<p>Enhance <code class=\"literal\">pg_locks</code> view\n            (Tom)</p>"
  ],
  [
    "<p>Log queries for client-side <code class=\"command\">PREPARE</code> and <code class=\"command\">EXECUTE</code> (Simon)</p>"
  ],
  [
    "<p>Allow Kerberos name and user name case sensitivity\n            to be specified in <code class=\"filename\">postgresql.conf</code> (Magnus)</p>"
  ],
  [
    "<p>Add configuration parameter <code class=\"varname\">krb_server_hostname</code> so that the server\n            host name can be specified as part of service principal\n            (Todd Kover)</p>",
    "<p>If not set, any service principal matching an entry\n            in the keytab can be used. This is new Kerberos\n            matching behavior in this release.</p>"
  ],
  [
    "<p>Add <code class=\"varname\">log_line_prefix</code>\n            options for millisecond timestamps (<code class=\"literal\">%m</code>) and remote host (<code class=\"literal\">%h</code>) (Ed L.)</p>"
  ],
  [
    "<p>Add WAL logging for GiST indexes (Teodor, Oleg)</p>",
    "<p>GiST indexes are now safe for crash and\n            point-in-time recovery.</p>"
  ],
  [
    "<p>Remove old <code class=\"filename\">*.backup</code>\n            files when we do <code class=\"function\">pg_stop_backup()</code> (Bruce)</p>",
    "<p>This prevents a large number of <code class=\"filename\">*.backup</code> files from existing in\n            <code class=\"filename\">pg_xlog/</code>.</p>"
  ],
  [
    "<p>Add configuration parameters to control TCP/IP\n            keep-alive times for idle, interval, and count (Oliver\n            Jowett)</p>",
    "<p>These values can be changed to allow more rapid\n            detection of lost client connections.</p>"
  ],
  [
    "<p>Add per-user and per-database connection limits\n            (Petr Jelinek)</p>",
    "<p>Using <code class=\"command\">ALTER USER</code> and\n            <code class=\"command\">ALTER DATABASE</code>, limits can\n            now be enforced on the maximum number of sessions that\n            can concurrently connect as a specific user or to a\n            specific database. Setting the limit to zero disables\n            user or database connections.</p>"
  ],
  [
    "<p>Allow more than two gigabytes of shared memory and\n            per-backend work memory on 64-bit machines (Koichi\n            Suzuki)</p>"
  ],
  [
    "<p>New system catalog <code class=\"structname\">pg_pltemplate</code> allows overriding\n            obsolete procedural-language definitions in dump files\n            (Tom)</p>"
  ],
  [
    "<p>Add temporary views (Koju Iijima, Neil)</p>"
  ],
  [
    "<p>Fix <code class=\"command\">HAVING</code> without any\n            aggregate functions or <code class=\"command\">GROUP\n            BY</code> so that the query returns a single group\n            (Tom)</p>",
    "<p>Previously, such a case would treat the <code class=\"command\">HAVING</code> clause the same as a\n            <code class=\"command\">WHERE</code> clause. This was not\n            per spec.</p>"
  ],
  [
    "<p>Add <code class=\"command\">USING</code> clause to\n            allow additional tables to be specified to <code class=\"command\">DELETE</code> (Euler Taveira de Oliveira,\n            Neil)</p>",
    "<p>In prior releases, there was no clear method for\n            specifying additional tables to be used for joins in a\n            <code class=\"command\">DELETE</code> statement.\n            <code class=\"command\">UPDATE</code> already has a\n            <code class=\"literal\">FROM</code> clause for this\n            purpose.</p>"
  ],
  [
    "<p>Add support for <code class=\"literal\">\\x</code> hex\n            escapes in backend and ecpg strings (Bruce)</p>",
    "<p>This is just like the standard C <code class=\"literal\">\\x</code> escape syntax. Octal escapes were\n            already supported.</p>"
  ],
  [
    "<p>Add <code class=\"command\">BETWEEN SYMMETRIC</code>\n            query syntax (Pavel Stehule)</p>",
    "<p>This feature allows <code class=\"command\">BETWEEN</code> comparisons without requiring\n            the first value to be less than the second. For\n            example, <code class=\"command\">2 BETWEEN [ASYMMETRIC] 3\n            AND 1</code> returns false, while <code class=\"command\">2 BETWEEN SYMMETRIC 3 AND 1</code> returns\n            true. <code class=\"command\">BETWEEN ASYMMETRIC</code>\n            was already supported.</p>"
  ],
  [
    "<p>Add <code class=\"command\">NOWAIT</code> option to\n            <code class=\"command\">SELECT ... FOR\n            UPDATE/SHARE</code> (Hans-Juergen Schoenig)</p>",
    "<p>While the <code class=\"varname\">statement_timeout</code> configuration\n            parameter allows a query taking more than a certain\n            amount of time to be canceled, the <code class=\"command\">NOWAIT</code> option allows a query to be\n            canceled as soon as a <code class=\"command\">SELECT ...\n            FOR UPDATE/SHARE</code> command cannot immediately\n            acquire a row lock.</p>"
  ],
  [
    "<p>Track dependencies of shared objects (Alvaro)</p>",
    "<p><span class=\"productname\">PostgreSQL</span> allows\n            global tables (users, databases, tablespaces) to\n            reference information in multiple databases. This\n            addition adds dependency information for global tables,\n            so, for example, user ownership can be tracked across\n            databases, so a user who owns something in any database\n            can no longer be removed. Dependency tracking already\n            existed for database-local objects.</p>"
  ],
  [
    "<p>Allow limited <code class=\"command\">ALTER\n            OWNER</code> commands to be performed by the object\n            owner (Stephen Frost)</p>",
    "<p>Prior releases allowed only superusers to change\n            object owners. Now, ownership can be transferred if the\n            user executing the command owns the object and would be\n            able to create it as the new owner (that is, the user\n            is a member of the new owning role and that role has\n            the CREATE permission that would be needed to create\n            the object afresh).</p>"
  ],
  [
    "<p>Add <code class=\"command\">ALTER</code> object\n            <code class=\"command\">SET SCHEMA</code> capability for\n            some object types (tables, functions, types) (Bernd\n            Helmle)</p>",
    "<p>This allows objects to be moved to different\n            schemas.</p>"
  ],
  [
    "<p>Add <code class=\"command\">ALTER TABLE ENABLE/DISABLE\n            TRIGGER</code> to disable triggers (Satoshi\n            Nagayasu)</p>"
  ],
  [
    "<p>Allow <code class=\"command\">TRUNCATE</code> to\n            truncate multiple tables in a single command\n            (Alvaro)</p>",
    "<p>Because of referential integrity checks, it is not\n            allowed to truncate a table that is part of a\n            referential integrity constraint. Using this new\n            functionality, <code class=\"command\">TRUNCATE</code>\n            can be used to truncate such tables, if both tables\n            involved in a referential integrity constraint are\n            truncated in a single <code class=\"command\">TRUNCATE</code> command.</p>"
  ],
  [
    "<p>Properly process carriage returns and line feeds in\n            <code class=\"command\">COPY CSV</code> mode (Andrew)</p>",
    "<p>In release 8.0, carriage returns and line feeds in\n            <code class=\"command\">CSV COPY TO</code> were processed\n            in an inconsistent manner. (This was documented on the\n            TODO list.)</p>"
  ],
  [
    "<p>Add <code class=\"command\">COPY WITH CSV\n            HEADER</code> to allow a header line as the first line\n            in <code class=\"command\">COPY</code> (Andrew)</p>",
    "<p>This allows handling of the common <code class=\"command\">CSV</code> usage of placing the column names\n            on the first line of the data file. For <code class=\"command\">COPY TO</code>, the first line contains the\n            column names, and for <code class=\"command\">COPY\n            FROM</code>, the first line is ignored.</p>"
  ],
  [
    "<p>On Windows, display better sub-second precision in\n            <code class=\"command\">EXPLAIN ANALYZE</code>\n            (Magnus)</p>"
  ],
  [
    "<p>Add trigger duration display to <code class=\"command\">EXPLAIN ANALYZE</code> (Tom)</p>",
    "<p>Prior releases included trigger execution time as\n            part of the total execution time, but did not show it\n            separately. It is now possible to see how much time is\n            spent in each trigger.</p>"
  ],
  [
    "<p>Add support for <code class=\"literal\">\\x</code> hex\n            escapes in <code class=\"command\">COPY</code> (Sergey\n            Ten)</p>",
    "<p>Previous releases only supported octal escapes.</p>"
  ],
  [
    "<p>Make <code class=\"command\">SHOW ALL</code> include\n            variable descriptions (Matthias Schmidt)</p>",
    "<p><code class=\"command\">SHOW</code> varname still only\n            displays the variable's value and does not include the\n            description.</p>"
  ],
  [
    "<p>Make <span class=\"application\">initdb</span> create\n            a new standard database called <code class=\"literal\">postgres</code>, and convert utilities to use\n            <code class=\"literal\">postgres</code> rather than\n            <code class=\"literal\">template1</code> for standard\n            lookups (Dave)</p>",
    "<p>In prior releases, <code class=\"literal\">template1</code> was used both as a default\n            connection for utilities like <span class=\"application\">createuser</span>, and as a template for\n            new databases. This caused <code class=\"command\">CREATE\n            DATABASE</code> to sometimes fail, because a new\n            database cannot be created if anyone else is in the\n            template database. With this change, the default\n            connection database is now <code class=\"literal\">postgres</code>, meaning it is much less\n            likely someone will be using <code class=\"literal\">template1</code> during <code class=\"command\">CREATE DATABASE</code>.</p>"
  ],
  [
    "<p>Create new <span class=\"application\">reindexdb</span> command-line utility by\n            moving <code class=\"filename\">/contrib/reindexdb</code>\n            into the server (Euler Taveira de Oliveira)</p>"
  ],
  [
    "<p>Add <code class=\"function\">MAX()</code> and\n            <code class=\"function\">MIN()</code> aggregates for\n            array types (Koju Iijima)</p>"
  ],
  [
    "<p>Fix <code class=\"function\">to_date()</code> and\n            <code class=\"function\">to_timestamp()</code> to behave\n            reasonably when <code class=\"literal\">CC</code> and\n            <code class=\"literal\">YY</code> fields are both used\n            (Karel Zak)</p>",
    "<p>If the format specification contains <code class=\"literal\">CC</code> and a year specification is\n            <code class=\"literal\">YYY</code> or longer, ignore the\n            <code class=\"literal\">CC</code>. If the year\n            specification is <code class=\"literal\">YY</code> or\n            shorter, interpret <code class=\"literal\">CC</code> as\n            the previous century.</p>"
  ],
  [
    "<p>Add <code class=\"function\">md5(bytea)</code>\n            (Abhijit Menon-Sen)</p>",
    "<p><code class=\"function\">md5(text)</code> already\n            existed.</p>"
  ],
  [
    "<p>Add support for <code class=\"command\">numeric ^\n            numeric</code> based on <code class=\"function\">power(numeric, numeric)</code></p>",
    "<p>The function already existed, but there was no\n            operator assigned to it.</p>"
  ],
  [
    "<p>Fix <code class=\"type\">NUMERIC</code> modulus by\n            properly truncating the quotient during computation\n            (Bruce)</p>",
    "<p>In previous releases, modulus for large values\n            sometimes returned negative results due to rounding of\n            the quotient.</p>"
  ],
  [
    "<p>Add a function <code class=\"function\">lastval()</code> (Dennis Bj&#xF6;rklund)</p>",
    "<p><code class=\"function\">lastval()</code> is a\n            simplified version of <code class=\"function\">currval()</code>. It automatically\n            determines the proper sequence name based on the most\n            recent <code class=\"function\">nextval()</code> or\n            <code class=\"function\">setval()</code> call performed\n            by the current session.</p>"
  ],
  [
    "<p>Add <code class=\"function\">to_timestamp(DOUBLE\n            PRECISION) (Michael Glaesemann)</code></p>",
    "<p>Converts Unix seconds since 1970 to a <code class=\"type\">TIMESTAMP WITH TIMEZONE</code>.</p>"
  ],
  [
    "<p>Add <code class=\"function\">pg_postmaster_start_time()</code> function\n            (Euler Taveira de Oliveira, Matthias Schmidt)</p>"
  ],
  [
    "<p>Allow the full use of time zone names in\n            <code class=\"command\">AT TIME ZONE</code>, not just the\n            short list previously available (Magnus)</p>",
    "<p>Previously, only a predefined list of time zone\n            names were supported by <code class=\"command\">AT TIME\n            ZONE</code>. Now any supported time zone name can be\n            used, e.g.:</p>",
    "<pre class=\"programlisting\">\n            SELECT CURRENT_TIMESTAMP AT TIME ZONE 'Europe/London';</pre>",
    "<p>In the above query, the time zone used is adjusted\n            based on the daylight saving time rules that were in\n            effect on the supplied date.</p>"
  ],
  [
    "<p>Add <code class=\"function\">GREATEST()</code> and\n            <code class=\"function\">LEAST()</code> variadic\n            functions (Pavel Stehule)</p>",
    "<p>These functions take a variable number of arguments\n            and return the greatest or least value among the\n            arguments.</p>"
  ],
  [
    "<p>Add <code class=\"function\">pg_column_size()</code>\n            (Mark Kirkwood)</p>",
    "<p>This returns storage size of a column, which might\n            be compressed.</p>"
  ],
  [
    "<p>Add <code class=\"function\">regexp_replace()</code>\n            (Atsushi Ogawa)</p>",
    "<p>This allows regular expression replacement, like\n            sed. An optional flag argument allows selection of\n            global (replace all) and case-insensitive modes.</p>"
  ],
  [
    "<p>Fix interval division and multiplication (Bruce)</p>",
    "<p>Previous versions sometimes returned unjustified\n            results, like <code class=\"command\">'4\n            months'::interval / 5</code> returning <code class=\"command\">'1 mon -6 days'</code>.</p>"
  ],
  [
    "<p>Fix roundoff behavior in timestamp, time, and\n            interval output (Tom)</p>",
    "<p>This fixes some cases in which the seconds field\n            would be shown as <code class=\"literal\">60</code>\n            instead of incrementing the higher-order fields.</p>"
  ],
  [
    "<p>Add a separate day field to type <code class=\"type\">interval</code> so a one day interval can be\n            distinguished from a 24 hour interval (Michael\n            Glaesemann)</p>",
    "<p>Days that contain a daylight saving time adjustment\n            are not 24 hours long, but typically 23 or 25 hours.\n            This change creates a conceptual distinction between\n            intervals of <span class=\"quote\">&#x201C;<span class=\"quote\">so many days</span>&#x201D;</span> and intervals of\n            <span class=\"quote\">&#x201C;<span class=\"quote\">so many\n            hours</span>&#x201D;</span>. Adding <code class=\"literal\">1\n            day</code> to a timestamp now gives the same local time\n            on the next day even if a daylight saving time\n            adjustment occurs between, whereas adding <code class=\"literal\">24 hours</code> will give a different local\n            time when this happens. For example, under US DST\n            rules:</p>",
    "<pre class=\"programlisting\">\n            '2005-04-03 00:00:00-05' + '1 day' = '2005-04-04 00:00:00-04'\n'2005-04-03 00:00:00-05' + '24 hours' = '2005-04-04 01:00:00-04'</pre>"
  ],
  [
    "<p>Add <code class=\"function\">justify_days()</code> and\n            <code class=\"function\">justify_hours()</code> (Michael\n            Glaesemann)</p>",
    "<p>These functions, respectively, adjust days to an\n            appropriate number of full months and days, and adjust\n            hours to an appropriate number of full days and\n            hours.</p>"
  ],
  [
    "<p>Move <code class=\"filename\">/contrib/dbsize</code>\n            into the backend, and rename some of the functions\n            (Dave Page, Andreas Pflug)</p>",
    "<div class=\"itemizedlist\">\n              <ul class=\"itemizedlist\" style=\"list-style-type: circle;\">\n                <li class=\"listitem\">\n                  <p><code class=\"function\">pg_tablespace_size()</code></p>\n                </li>\n                <li class=\"listitem\">\n                  <p><code class=\"function\">pg_database_size()</code></p>\n                </li>\n                <li class=\"listitem\">\n                  <p><code class=\"function\">pg_relation_size()</code></p>\n                </li>\n                <li class=\"listitem\">\n                  <p><code class=\"function\">pg_total_relation_size()</code></p>\n                </li>\n                <li class=\"listitem\">\n                  <p><code class=\"function\">pg_size_pretty()</code></p>\n                </li>\n              </ul>\n            </div>",
    "<p><code class=\"function\">pg_total_relation_size()</code> includes\n            indexes and TOAST tables.</p>"
  ],
  [
    "<p>\n  <code class=\"function\">pg_tablespace_size()</code>\n</p>"
  ],
  [
    "<p>\n  <code class=\"function\">pg_database_size()</code>\n</p>"
  ],
  [
    "<p>\n  <code class=\"function\">pg_relation_size()</code>\n</p>"
  ],
  [
    "<p>\n  <code class=\"function\">pg_total_relation_size()</code>\n</p>"
  ],
  [
    "<p>\n  <code class=\"function\">pg_size_pretty()</code>\n</p>"
  ],
  [
    "<p>Add functions for read-only file access to the\n            cluster directory (Dave Page, Andreas Pflug)</p>",
    "<div class=\"itemizedlist\">\n              <ul class=\"itemizedlist\" style=\"list-style-type: circle;\">\n                <li class=\"listitem\">\n                  <p><code class=\"function\">pg_stat_file()</code></p>\n                </li>\n                <li class=\"listitem\">\n                  <p><code class=\"function\">pg_read_file()</code></p>\n                </li>\n                <li class=\"listitem\">\n                  <p><code class=\"function\">pg_ls_dir()</code></p>\n                </li>\n              </ul>\n            </div>"
  ],
  [
    "<p>\n  <code class=\"function\">pg_stat_file()</code>\n</p>"
  ],
  [
    "<p>\n  <code class=\"function\">pg_read_file()</code>\n</p>"
  ],
  [
    "<p>\n  <code class=\"function\">pg_ls_dir()</code>\n</p>"
  ],
  [
    "<p>Add <code class=\"function\">pg_reload_conf()</code>\n            to force reloading of the configuration files (Dave\n            Page, Andreas Pflug)</p>"
  ],
  [
    "<p>Add <code class=\"function\">pg_rotate_logfile()</code> to force rotation\n            of the server log file (Dave Page, Andreas Pflug)</p>"
  ],
  [
    "<p>Change <code class=\"literal\">pg_stat_*</code> views\n            to include TOAST tables (Tom)</p>"
  ],
  [
    "<p>Rename some encodings to be more consistent and to\n            follow international standards (Bruce)</p>",
    "<div class=\"itemizedlist\">\n              <ul class=\"itemizedlist\" style=\"list-style-type: circle;\">\n                <li class=\"listitem\">\n                  <p><code class=\"literal\">UNICODE</code> is now\n                  <code class=\"literal\">UTF8</code></p>\n                </li>\n                <li class=\"listitem\">\n                  <p><code class=\"literal\">ALT</code> is now\n                  <code class=\"literal\">WIN866</code></p>\n                </li>\n                <li class=\"listitem\">\n                  <p><code class=\"literal\">WIN</code> is now\n                  <code class=\"literal\">WIN1251</code></p>\n                </li>\n                <li class=\"listitem\">\n                  <p><code class=\"literal\">TCVN</code> is now\n                  <code class=\"literal\">WIN1258</code></p>\n                </li>\n              </ul>\n            </div>",
    "<p>The original names still work.</p>"
  ],
  [
    "<p><code class=\"literal\">UNICODE</code> is now\n                  <code class=\"literal\">UTF8</code></p>"
  ],
  [
    "<p><code class=\"literal\">ALT</code> is now\n                  <code class=\"literal\">WIN866</code></p>"
  ],
  [
    "<p><code class=\"literal\">WIN</code> is now\n                  <code class=\"literal\">WIN1251</code></p>"
  ],
  [
    "<p><code class=\"literal\">TCVN</code> is now\n                  <code class=\"literal\">WIN1258</code></p>"
  ],
  [
    "<p>Add support for <code class=\"literal\">WIN1252</code>\n            encoding (Roland Volkmann)</p>"
  ],
  [
    "<p>Add support for four-byte <code class=\"literal\">UTF8</code> characters (John Hansen)</p>",
    "<p>Previously only one, two, and three-byte\n            <code class=\"literal\">UTF8</code> characters were\n            supported. This is particularly important for support\n            for some Chinese character sets.</p>"
  ],
  [
    "<p>Allow direct conversion between <code class=\"literal\">EUC_JP</code> and <code class=\"literal\">SJIS</code> to improve performance (Atsushi\n            Ogawa)</p>"
  ],
  [
    "<p>Allow the UTF8 encoding to work on Windows\n            (Magnus)</p>",
    "<p>This is done by mapping UTF8 to the Windows-native\n            UTF16 implementation.</p>"
  ],
  [
    "<p>Fix <code class=\"command\">ALTER LANGUAGE\n            RENAME</code> (Sergey Yatskevich)</p>"
  ],
  [
    "<p>Allow function characteristics, like strictness and\n            volatility, to be modified via <code class=\"command\">ALTER FUNCTION</code> (Neil)</p>"
  ],
  [
    "<p>Increase the maximum number of function arguments to\n            100 (Tom)</p>"
  ],
  [
    "<p>Allow SQL and PL/pgSQL functions to use <code class=\"command\">OUT</code> and <code class=\"command\">INOUT</code> parameters (Tom)</p>",
    "<p><code class=\"command\">OUT</code> is an alternate way\n            for a function to return values. Instead of using\n            <code class=\"command\">RETURN</code>, values can be\n            returned by assigning to parameters declared as\n            <code class=\"command\">OUT</code> or <code class=\"command\">INOUT</code>. This is notationally simpler in\n            some cases, particularly so when multiple values need\n            to be returned. While returning multiple values from a\n            function was possible in previous releases, this\n            greatly simplifies the process. (The feature will be\n            extended to other server-side languages in future\n            releases.)</p>"
  ],
  [
    "<p>Move language handler functions into the\n            <code class=\"literal\">pg_catalog</code> schema</p>",
    "<p>This makes it easier to drop the public schema if\n            desired.</p>"
  ],
  [
    "<p>Add <code class=\"function\">SPI_getnspname()</code>\n            to SPI (Neil)</p>"
  ],
  [
    "<p>Overhaul the memory management of PL/pgSQL functions\n            (Neil)</p>",
    "<p>The parsetree of each function is now stored in a\n            separate memory context. This allows this memory to be\n            easily reclaimed when it is no longer needed.</p>"
  ],
  [
    "<p>Check function syntax at <code class=\"command\">CREATE FUNCTION</code> time, rather than at\n            runtime (Neil)</p>",
    "<p>Previously, most syntax errors were reported only\n            when the function was executed.</p>"
  ],
  [
    "<p>Allow <code class=\"command\">OPEN</code> to open\n            non-<code class=\"command\">SELECT</code> queries like\n            <code class=\"command\">EXPLAIN</code> and <code class=\"command\">SHOW</code> (Tom)</p>"
  ],
  [
    "<p>No longer require functions to issue a <code class=\"command\">RETURN</code> statement (Tom)</p>",
    "<p>This is a byproduct of the newly added <code class=\"command\">OUT</code> and <code class=\"command\">INOUT</code> functionality. <code class=\"command\">RETURN</code> can be omitted when it is not\n            needed to provide the function's return value.</p>"
  ],
  [
    "<p>Add support for an optional <code class=\"command\">INTO</code> clause to PL/pgSQL's <code class=\"command\">EXECUTE</code> statement (Pavel Stehule,\n            Neil)</p>"
  ],
  [
    "<p>Make <code class=\"command\">CREATE TABLE AS</code>\n            set <code class=\"command\">ROW_COUNT</code> (Tom)</p>"
  ],
  [
    "<p>Define <code class=\"literal\">SQLSTATE</code> and\n            <code class=\"literal\">SQLERRM</code> to return the\n            <code class=\"literal\">SQLSTATE</code> and error message\n            of the current exception (Pavel Stehule, Neil)</p>",
    "<p>These variables are only defined inside exception\n            blocks.</p>"
  ],
  [
    "<p>Allow the parameters to the <code class=\"command\">RAISE</code> statement to be expressions\n            (Pavel Stehule, Neil)</p>"
  ],
  [
    "<p>Add a loop <code class=\"command\">CONTINUE</code>\n            statement (Pavel Stehule, Neil)</p>"
  ],
  [
    "<p>Allow block and loop labels (Pavel Stehule)</p>"
  ],
  [
    "<p>Allow large result sets to be returned efficiently\n            (Abhijit Menon-Sen)</p>",
    "<p>This allows functions to use <code class=\"function\">return_next()</code> to avoid building the\n            entire result set in memory.</p>"
  ],
  [
    "<p>Allow one-row-at-a-time retrieval of query results\n            (Abhijit Menon-Sen)</p>",
    "<p>This allows functions to use <code class=\"function\">spi_query()</code> and <code class=\"function\">spi_fetchrow()</code> to avoid accumulating\n            the entire result set in memory.</p>"
  ],
  [
    "<p>Force PL/Perl to handle strings as <code class=\"literal\">UTF8</code> if the server encoding is\n            <code class=\"literal\">UTF8</code> (David Kamholz)</p>"
  ],
  [
    "<p>Add a validator function for PL/Perl (Andrew)</p>",
    "<p>This allows syntax errors to be reported at\n            definition time, rather than execution time.</p>"
  ],
  [
    "<p>Allow PL/Perl to return a Perl array when the\n            function returns an array type (Andrew)</p>",
    "<p>This basically maps <span class=\"productname\">PostgreSQL</span> arrays to Perl\n            arrays.</p>"
  ],
  [
    "<p>Allow Perl nonfatal warnings to generate\n            <code class=\"command\">NOTICE</code> messages\n            (Andrew)</p>"
  ],
  [
    "<p>Allow Perl's <code class=\"literal\">strict</code>\n            mode to be enabled (Andrew)</p>"
  ],
  [
    "<p>Add <code class=\"command\">\\set\n            ON_ERROR_ROLLBACK</code> to allow statements in a\n            transaction to error without affecting the rest of the\n            transaction (Greg Sabino Mullane)</p>",
    "<p>This is basically implemented by wrapping every\n            statement in a sub-transaction.</p>"
  ],
  [
    "<p>Add support for <code class=\"literal\">\\x</code> hex\n            strings in <span class=\"application\">psql</span>\n            variables (Bruce)</p>",
    "<p>Octal escapes were already supported.</p>"
  ],
  [
    "<p>Add support for <code class=\"command\">troff\n            -ms</code> output format (Roger Leigh)</p>"
  ],
  [
    "<p>Allow the history file location to be controlled by\n            <code class=\"envar\">HISTFILE</code> (Andreas\n            Seltenreich)</p>",
    "<p>This allows configuration of per-database history\n            storage.</p>"
  ],
  [
    "<p>Prevent <code class=\"command\">\\x</code> (expanded\n            mode) from affecting the output of <code class=\"command\">\\d tablename</code> (Neil)</p>"
  ],
  [
    "<p>Add <code class=\"option\">-L</code> option to\n            <span class=\"application\">psql</span> to log sessions\n            (Lorne Sunley)</p>",
    "<p>This option was added because some operating systems\n            do not have simple command-line activity logging\n            functionality.</p>"
  ],
  [
    "<p>Make <code class=\"command\">\\d</code> show the\n            tablespaces of indexes (Qingqing Zhou)</p>"
  ],
  [
    "<p>Allow <span class=\"application\">psql</span> help\n            (<code class=\"command\">\\h</code>) to make a best guess\n            on the proper help information (Greg Sabino\n            Mullane)</p>",
    "<p>This allows the user to just add <code class=\"command\">\\h</code> to the front of the syntax error\n            query and get help on the supported syntax. Previously\n            any additional query text beyond the command name had\n            to be removed to use <code class=\"command\">\\h</code>.</p>"
  ],
  [
    "<p>Add <code class=\"command\">\\pset numericlocale</code>\n            to allow numbers to be output in a locale-aware format\n            (Eugen Nedelcu)</p>",
    "<p>For example, using <code class=\"literal\">C</code>\n            locale <code class=\"literal\">100000</code> would be\n            output as <code class=\"literal\">100,000.0</code> while\n            a European locale might output this value as\n            <code class=\"literal\">100.000,0</code>.</p>"
  ],
  [
    "<p>Make startup banner show both server version number\n            and <span class=\"application\">psql</span>'s version\n            number, when they are different (Bruce)</p>",
    "<p>Also, a warning will be shown if the server and\n            <span class=\"application\">psql</span> are from\n            different major releases.</p>"
  ],
  [
    "<p>Add <code class=\"option\">-n</code> / <code class=\"option\">--schema</code> switch to <span class=\"application\">pg_restore</span> (Richard van den\n            Berg)</p>",
    "<p>This allows just the objects in a specified schema\n            to be restored.</p>"
  ],
  [
    "<p>Allow <span class=\"application\">pg_dump</span> to\n            dump large objects even in text mode (Tom)</p>",
    "<p>With this change, large objects are now always\n            dumped; the former <code class=\"option\">-b</code>\n            switch is a no-op.</p>"
  ],
  [
    "<p>Allow <span class=\"application\">pg_dump</span> to\n            dump a consistent snapshot of large objects (Tom)</p>"
  ],
  [
    "<p>Dump comments for large objects (Tom)</p>"
  ],
  [
    "<p>Add <code class=\"option\">--encoding</code> to\n            <span class=\"application\">pg_dump</span> (Magnus\n            Hagander)</p>",
    "<p>This allows a database to be dumped in an encoding\n            that is different from the server's encoding. This is\n            valuable when transferring the dump to a machine with a\n            different encoding.</p>"
  ],
  [
    "<p>Rely on <code class=\"structname\">pg_pltemplate</code> for procedural\n            languages (Tom)</p>",
    "<p>If the call handler for a procedural language is in\n            the <code class=\"literal\">pg_catalog</code> schema,\n            <span class=\"application\">pg_dump</span> does not dump\n            the handler. Instead, it dumps the language using just\n            <code class=\"command\">CREATE LANGUAGE <em class=\"replaceable\"><code>name</code></em></code>, relying on\n            the <code class=\"structname\">pg_pltemplate</code>\n            catalog to provide the language's creation parameters\n            at load time.</p>"
  ],
  [
    "<p>Add a <code class=\"envar\">PGPASSFILE</code>\n            environment variable to specify the password file's\n            filename (Andrew)</p>"
  ],
  [
    "<p>Add <code class=\"function\">lo_create()</code>, that\n            is similar to <code class=\"function\">lo_creat()</code>\n            but allows the OID of the large object to be specified\n            (Tom)</p>"
  ],
  [
    "<p>Make <span class=\"application\">libpq</span>\n            consistently return an error to the client application\n            on <code class=\"function\">malloc()</code> failure\n            (Neil)</p>"
  ],
  [
    "<p>Fix <span class=\"application\">pgxs</span> to support\n            building against a relocated installation</p>"
  ],
  [
    "<p>Add spinlock support for the Itanium processor using\n            Intel compiler (Vikram Kalsi)</p>"
  ],
  [
    "<p>Add Kerberos 5 support for Windows (Magnus)</p>"
  ],
  [
    "<p>Add Chinese FAQ (laser@pgsqldb.com)</p>"
  ],
  [
    "<p>Rename Rendezvous to Bonjour to match OS/X feature\n            renaming (Bruce)</p>"
  ],
  [
    "<p>Add support for <code class=\"literal\">fsync_writethrough</code> on macOS (Chris\n            Campbell)</p>"
  ],
  [
    "<p>Streamline the passing of information within the\n            server, the optimizer, and the lock system (Tom)</p>"
  ],
  [
    "<p>Allow <span class=\"application\">pg_config</span> to\n            be compiled using MSVC (Andrew)</p>",
    "<p>This is required to build DBD::Pg using <span class=\"application\">MSVC</span>.</p>"
  ],
  [
    "<p>Remove support for Kerberos V4 (Magnus)</p>",
    "<p>Kerberos 4 had security vulnerabilities and is no\n            longer maintained.</p>"
  ],
  [
    "<p>Code cleanups (Coverity static analysis performed by\n            EnterpriseDB)</p>"
  ],
  [
    "<p>Modify <code class=\"filename\">postgresql.conf</code>\n            to use documentation defaults <code class=\"literal\">on</code>/<code class=\"literal\">off</code>\n            rather than <code class=\"literal\">true</code>/<code class=\"literal\">false</code> (Bruce)</p>"
  ],
  [
    "<p>Enhance <span class=\"application\">pg_config</span>\n            to be able to report more build-time values (Tom)</p>"
  ],
  [
    "<p>Allow <span class=\"application\">libpq</span> to be\n            built thread-safe on Windows (Dave Page)</p>"
  ],
  [
    "<p>Allow IPv6 connections to be used on Windows\n            (Andrew)</p>"
  ],
  [
    "<p>Add Server Administration documentation about I/O\n            subsystem reliability (Bruce)</p>"
  ],
  [
    "<p>Move private declarations from <code class=\"filename\">gist.h</code> to <code class=\"filename\">gist_private.h</code> (Neil)</p>",
    "<p>In previous releases, <code class=\"filename\">gist.h</code> contained both the public GiST\n            API (intended for use by authors of GiST index\n            implementations) as well as some private declarations\n            used by the implementation of GiST itself. The latter\n            have been moved to a separate file, <code class=\"filename\">gist_private.h</code>. Most GiST index\n            implementations should be unaffected.</p>"
  ],
  [
    "<p>Overhaul GiST memory management (Neil)</p>",
    "<p>GiST methods are now always invoked in a short-lived\n            memory context. Therefore, memory allocated via\n            <code class=\"function\">palloc()</code> will be\n            reclaimed automatically, so GiST index implementations\n            do not need to manually release allocated memory via\n            <code class=\"function\">pfree()</code>.</p>"
  ],
  [
    "<p>Add <code class=\"filename\">/contrib/pg_buffercache</code> contrib\n            module (Mark Kirkwood)</p>",
    "<p>This displays the contents of the buffer cache, for\n            debugging and performance tuning purposes.</p>"
  ],
  [
    "<p>Remove <code class=\"filename\">/contrib/array</code>\n            because it is obsolete (Tom)</p>"
  ],
  [
    "<p>Clean up the <code class=\"filename\">/contrib/lo</code> module (Tom)</p>"
  ],
  [
    "<p>Move <code class=\"filename\">/contrib/findoidjoins</code> to <code class=\"filename\">/src/tools</code> (Tom)</p>"
  ],
  [
    "<p>Remove the <code class=\"literal\">&lt;&lt;</code>,\n            <code class=\"literal\">&gt;&gt;</code>, <code class=\"literal\">&amp;&lt;</code>, and <code class=\"literal\">&amp;&gt;</code> operators from <code class=\"filename\">/contrib/cube</code></p>",
    "<p>These operators were not useful.</p>"
  ],
  [
    "<p>Improve <code class=\"filename\">/contrib/btree_gist</code> (Janko\n            Richter)</p>"
  ],
  [
    "<p>Improve <code class=\"filename\">/contrib/pgbench</code> (Tomoaki Sato,\n            Tatsuo)</p>",
    "<p>There is now a facility for testing with SQL command\n            scripts given by the user, instead of only a hard-wired\n            command sequence.</p>"
  ],
  [
    "<p>Improve <code class=\"filename\">/contrib/pgcrypto</code> (Marko Kreen)</p>",
    "<div class=\"itemizedlist\">\n              <ul class=\"itemizedlist\" style=\"list-style-type: circle;\">\n                <li class=\"listitem\">\n                  <p>Implementation of OpenPGP symmetric-key and\n                  public-key encryption</p>\n                  <p>Both RSA and Elgamal public-key algorithms are\n                  supported.</p>\n                </li>\n                <li class=\"listitem\">\n                  <p>Stand alone build: include SHA256/384/512\n                  hashes, Fortuna PRNG</p>\n                </li>\n                <li class=\"listitem\">\n                  <p>OpenSSL build: support 3DES, use internal AES\n                  with OpenSSL &lt; 0.9.7</p>\n                </li>\n                <li class=\"listitem\">\n                  <p>Take build parameters (OpenSSL, zlib) from\n                  <code class=\"filename\">configure</code>\n                  result</p>\n                  <p>There is no need to edit the <code class=\"filename\">Makefile</code> anymore.</p>\n                </li>\n                <li class=\"listitem\">\n                  <p>Remove support for <code class=\"filename\">libmhash</code> and <code class=\"filename\">libmcrypt</code></p>\n                </li>\n              </ul>\n            </div>"
  ],
  [
    "<p>Implementation of OpenPGP symmetric-key and\n                  public-key encryption</p>",
    "<p>Both RSA and Elgamal public-key algorithms are\n                  supported.</p>"
  ],
  [
    "<p>Stand alone build: include SHA256/384/512\n                  hashes, Fortuna PRNG</p>"
  ],
  [
    "<p>OpenSSL build: support 3DES, use internal AES\n                  with OpenSSL &lt; 0.9.7</p>"
  ],
  [
    "<p>Take build parameters (OpenSSL, zlib) from\n                  <code class=\"filename\">configure</code>\n                  result</p>",
    "<p>There is no need to edit the <code class=\"filename\">Makefile</code> anymore.</p>"
  ],
  [
    "<p>Remove support for <code class=\"filename\">libmhash</code> and <code class=\"filename\">libmcrypt</code></p>"
  ]
]