#!/usr/bin/env ruby
# encoding: utf-8

require 'pathname'
require 'pp'
require 'net/http'
require 'uri'
require 'nokogiri'
require 'json'

def die( status, error_message )
  if 0 == status
    puts error_message
  else
    $stderr.puts error_message
  end
  exit status
end

def fetch(uri_str, limit = 10)
  # You should choose a better exception.
  raise ArgumentError, 'too many HTTP redirects' if limit == 0

  puts "Getting: #{uri_str}"
  uri = URI(uri_str)
  response = Net::HTTP.get_response(uri)

  case response
  when Net::HTTPSuccess then
    [response, uri_str]
  when Net::HTTPRedirection then
    location = response['location']
    fetch((uri + location).to_s, limit - 1)
  else
    [response.value, uri_str]
  end
end

def find_appropriate_ul_with_changes(ver, doc)
  return find_appropriate_ul_with_changes_10(doc) if Gem::Version.new(ver) >= Gem::Version.new('10')
  return find_appropriate_ul_with_changes_older(doc)
end

def find_appropriate_ul_with_changes_older(doc)
  uls = []
  doc.css( 'div.SECT2 > h2.SECT2 > a' ).each do |a|
    next unless a.text.match(%r{Changes|Migration})
    uls << a.parent.parent.css('ul')
  end
  return if uls.empty?
  return uls
end

def find_appropriate_ul_with_changes_10(doc)
  uls = []
  section_id = nil
  doc.css( 'div.toc a' ).each do |a|
    next unless a.text.match(%r{Changes|Migration})
    section_id = a['href'].sub(%r{.*#}, '')
    uls << doc.css( 'div' ).select { |div| section_id == div["id"] }.first.css('ul')
  end
  return if uls.empty?
  return uls
end

def parse_changes(ver, doc, base_uri)
  content = doc.css( 'div#docContent' )
  release_date = content.css( 'div > p' ).first.children.last.text.strip

  changes = []

  usable_uls = find_appropriate_ul_with_changes(ver, doc)
  die(1, "Can't get usable ul for changes in #{base_uri}") if usable_uls.nil?

  usable_uls.each do |usable_ul|
    usable_ul.each do |ul|
      ul.children.each do |i|
        next unless 'li' == i.name
        current_changes = []
        i.children.each do |c|
          next if c.text? and c.to_s.strip == ''
          c.css( 'a' ).each do |a|
            a['href']=URI.join( base_uri, a['href'])
          end
          current_changes << c.to_xml
        end
        changes << current_changes unless current_changes.empty?
      end
    end
  end

  return release_date, changes
end

def handle_single_version (db_path, html_uri, ver)
  puts "handle_single_version(#{db_path} ; #{html_uri} ; #{ver})" if ENV['DEBUG']
  return if Gem::Version.new(ver) < Gem::Version.new('7.2')
  version_file = db_path + "#{ver}.js"
  if version_file.exist?
    puts "Skipping because it already exists!" if ENV['DEBUG']
    return false
  end
  puts "Working on it" if ENV['DEBUG']

  release_res = fetch(html_uri)[0]
  release_doc = Nokogiri::HTML( release_res.body )
  release_date, changes = parse_changes( ver, release_doc, html_uri )
  puts "  - #{release_date}, #{changes.size} changes" if ENV['DEBUG']
  return false if changes.empty?

  File.open( version_file, 'w' ) do | f |
    f.write JSON.pretty_generate(changes)
  end

  add_version_release( db_path, ver, html_uri, release_date, changes.size )
  return true
end

def add_version_release(db_path, version, doc_uri, release_date, change_count)
  puts "Updating index for version #{version}" if ENV['DEBUG']

  index_file = db_path + 'index.js'
  temp_file = db_path + 'index.js.tmp'
  if index_file.exist?
    index = JSON.load( File.open( index_file, 'r' ) )
  else
    index = {}
  end
  temp_file.delete if temp_file.exist?

  index[version] = {} unless index.include? version
  index[version] = [ release_date, change_count ]

  File.open( temp_file, "w" ) do |f|
    f.write( JSON.pretty_generate(index) )
  end

  temp_file.rename index_file
end

script_file=Pathname.new(__FILE__)
db_path=script_file.dirname.parent.realdirpath + 'db'
db_path.exist? or die(1, "There is no data dir: #{db_path}")

base_uri = 'https://www.postgresql.org/docs/current/index.html'

puts "Getting major version list" if ENV['DEBUG']
ret = fetch base_uri
base_uri = ret[1]
index_doc = Nokogiri::HTML( ret[0].body )

version_handled={}
anything_added=false

index_doc.css( 'a' ).each do | a |
  next unless a['href'].match %r{index\.html$}
  next unless a.text.match %r{^[1-9]\d*(\.[1-9]\d*)?$}
  next if a.parent.text.match %r{development}i;

  href = a['href'].sub( %r{/index\.html$}, '/release.html' )
  version_release_uri = URI.join( base_uri, href )

  puts "- Getting versions in major #{a.text} from #{version_release_uri}" if ENV["DEBUG"]

  releases = fetch version_release_uri
  releases_dom = Nokogiri::HTML( releases[0].body )
  releases_base = releases[1]

  releases_dom.css( 'a' ).each do | a |
    next if a['href'].nil?
    next unless a['href'].match %r{release-[1-9]\d*(-[1-9]\d*)*\.html$}
    release_uri = URI.join( releases_base, a['href'] )
    release_version = release_uri.path.split('/').last.sub( %r{^release-(.*)\.html$}, '\1' ).tr('-', '.')
    release_version.match(%r{^\d+(\.\d+){0,2}$}) or die(2, "Unparseable version: #{release_version}")
    next if version_handled.include? release_version
    version_handled[release_version]=1
    anything_added=true if handle_single_version( db_path, release_uri.to_s, release_version )
  end

end

exit 0 if anything_added
exit 1
