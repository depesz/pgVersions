#!/usr/bin/env perl

# UTF8 boilerplace, per http://stackoverflow.com/questions/6162484/why-does-modern-perl-avoid-utf-8-by-default/
use v5.14;
use strict;
use warnings;
use warnings qw( FATAL utf8 );
use utf8;
use open qw( :std :utf8 );
use Unicode::Normalize qw( NFC );
use Unicode::Collate;
use Encode qw( decode );

if ( grep /\P{ASCII}/ => @ARGV ) {
    @ARGV = map { decode( 'UTF-8', $_ ) } @ARGV;
}
use DBI;
use JSON;

my $dbh = DBI->connect( "dbi:Pg:dbname=postgres;host=/tmp", "postgres" );

my $sth = $dbh->prepare('select name, setting from pg_settings where name <> all(?)');

$sth->execute( [
    'TimeZone',        'client_encoding',     'config_file',   'data_directory',  'hba_file',
    'ident_file',      'lc_collate',          'lc_ctype',      'lc_messages',     'lc_monetary',
    'lc_numeric',      'lc_time',             'log_timezone',  'port',            'server_encoding',
    'server_version',  'server_version_num',
] );

my $data = {};

while (my $row = $sth->fetchrow_arrayref() ) {
    $data->{ $row->[ 0 ] } = $row->[ 1 ];
}

print JSON->new->canonical( 1 )->pretty->encode( $data );

exit;
