#!/usr/bin/env perl

# UTF8 boilerplace, per http://stackoverflow.com/questions/6162484/why-does-modern-perl-avoid-utf-8-by-default/
use v5.14;
use strict;
use warnings;
use warnings qw( FATAL utf8 );
use utf8;
use open qw( :std :utf8 );
use Unicode::Normalize qw( NFC );
use Unicode::Collate;
use Encode qw( decode );

if ( grep /\P{ASCII}/ => @ARGV ) {
    @ARGV = map { decode( 'UTF-8', $_ ) } @ARGV;
}

# If there is __DATA__,then uncomment next line:
# binmode( DATA, ':encoding(UTF-8)' );
# UTF8 boilerplace, per http://stackoverflow.com/questions/6162484/why-does-modern-perl-avoid-utf-8-by-default/

# Useful common code
use autodie;
use Carp         qw( carp croak confess cluck );
use English      qw( -no_match_vars );
use Data::Dumper qw( Dumper );

# give a full stack dump on any untrapped exceptions
local $SIG{ __DIE__ } = sub {
    confess "Uncaught exception: @_" unless $^S;
};

# now promote run-time warnings into stackdumped exceptions
#   *unless* we're in an try block, in which
#   case just generate a clucking stackdump instead
local $SIG{ __WARN__ } = sub {
    if   ( $^S ) { cluck "Trapped warning: @_" }
    else         { confess "Deadly warning: @_" }
};

# Useful common code

use Mojo::UserAgent;
use Mojo::DOM;
use Mojo::URL;
use Mojo::JSON;
use List::Util qw( uniq );

my $version = shift;
die "You have to provide version number?!\n" unless $version;

my $ua       = Mojo::UserAgent->new;
my $base_url = Mojo::URL->new( 'https://www.postgresql.org/docs/' . $version . '/runtime-config.html' );

our $all_data = { 'gucs' => {} };

my $all_pages = get_all_pages( $ua, $base_url );
while ( my ( $page, $dom ) = each %{ $all_pages } ) {
    extract_guc_links( $version, $page, $dom );
}

$all_data->{ 'pages' } = [ sort { $a cmp $b } uniq map { $_->[ 0 ] } values %{ $all_data->{ 'gucs' } } ];
my %page_no = ();
for my $i ( 0 .. $#{ $all_data->{ 'pages' } } ) {
    $page_no{ $all_data->{ 'pages' }->[ $i ] } = $i;
}
for my $e ( values %{ $all_data->{ 'gucs' } } ) {
    $e->[ 0 ] = $page_no{ $e->[ 0 ] };
}

print Mojo::JSON::encode_json( $all_data );

exit;

sub guc_exists {
    my $guc = shift;
    return defined $all_data->{ 'gucs' }->{ $guc };
}

sub add_guc {
    my ( $guc, $page, $fragment ) = @_;
    $all_data->{ 'gucs' }->{ $guc } = [ $page, $fragment ];
    return;
}

sub is_fragment_alike {
    my ( $guc_name, $fragment ) = @_;
    return unless defined $fragment;
    $guc_name = 'GUC' . uc( $guc_name );
    $fragment = uc( $fragment );
    $guc_name =~ s/[_-]//g;
    $fragment =~ s/[_-]//g;
    return $guc_name eq $fragment;
}

sub extract_guc_links {
    my ( $version, $filename, $dom ) = @_;

    return extract_guc_links_7_2( $filename, $dom ) if $version < 10;
    return extract_guc_links_10( $filename, $dom );
}

sub extract_guc_links_10 {
    my ( $filename, $dom ) = @_;
    for my $item ( $dom->find( 'dt > span[class=term] > code[class=varname]' )->each ) {
        my $guc_name = lc $item->text;

        my $dt = $item->ancestors( 'dt' )->first;
        if ( is_fragment_alike( $guc_name, $dt->attr( 'id' ) ) ) {
            add_guc( $guc_name, $filename, $dt->attr( 'id' ) );
            next;
        }
        next if guc_exists( $guc_name );
        my $a = $item->following( 'a[id]' )->first;
        next unless $a;
        add_guc( $guc_name, $filename, $a->attr( 'id' ) );
    }
    return;
}

sub extract_guc_links_7_2 {
    my ( $filename, $dom ) = @_;
    for my $item ( $dom->find( 'div > div[class=VARIABLELIST] > dl > dt > code[class=VARNAME], div > div[class=VARIABLELIST] > dl > dt > tt[class=VARNAME]' )->each ) {
        my $guc_name = lc $item->text;

        my $prev = $item->previous;

        if (   ( defined $prev )
            && ( $prev->tag eq 'a' )
            && ( is_fragment_alike( $guc_name, $prev->attr( 'id' ) ) ) )
        {
            add_guc( $guc_name, $filename, $prev->attr( 'id' ) );
            next;
        }

        my $sect_div = $item->ancestors( 'div.VARIABLELIST' )->first->parent;
        my $a        = $sect_div->find( 'h2 a[id], h3 a[id]' )->first;
        next unless $a;

        my $fragment = $a->attr( 'id' );
        next unless $fragment;

        add_guc( $guc_name, $filename, $fragment );
    }
    return;
}

sub get_all_pages {
    my ( $ua, $base_url ) = @_;

    my @urls = ( $base_url );
    my $seen = {};
    my $ret  = {};

    while ( my $url = shift @urls ) {
        my $file = $url->path->parts->[ -1 ];
        next if $seen->{ $file }++;

        my $dom = Mojo::DOM->new( get_page( $ua, $url ) );
        $ret->{ $file } = $dom;

        for my $href ( $dom->find( 'a[href]' )->map( attr => 'href' )->each ) {
            next unless $href =~ m{\Aruntime-config};
            my $new_url = Mojo::URL->new( $href )->base( $url )->to_abs;
            $new_url->fragment( undef );
            $new_url->query( undef );
            push @urls, $new_url;

        }
    }
    return $ret;
}

sub get_page {
    my ( $ua, $url ) = @_;
    my $res = $ua->get( $url )->result;

    return $res->body if $res->is_success;
    return get_page( $ua, Mojo::URL->new( $res->headers->location )->base( $base_url )->to_abs ) if $res->code == 301;
    return get_page( $ua, Mojo::URL->new( $res->headers->location )->base( $base_url )->to_abs ) if $res->code == 302;
    die "Can't get $url : " . $res->message . "\n";
}
