#!/usr/bin/env ruby
# encoding: utf-8

require 'pathname'
require 'pp'
require 'json'
require 'nokogiri'
require 'cgi'
require 'unicode_utils/downcase'

def die( status, error_message )
  if 0 == status
    puts error_message
  else
    $stderr.puts error_message
  end
  exit status
end

def get_words_from (changes)
  words = {}
  changes.each_with_index do |c, idx|

    change_single = c.join(' ')
    change_without_entities = CGI.unescapeHTML( change_single )
    change_text = Nokogiri::HTML( change_without_entities ).content

    all_words = change_text.scan(%r{[[:word:]]+})
    all_lc_words = all_words.map { |x| UnicodeUtils.downcase x }
    words_long = all_lc_words.select { |x| x.length > 1 }
    uniq_words = words_long.uniq

    uniq_words.each do |word|
      words[word] = [] unless words.include? word
      words[word] << idx
    end
  end

  words
end

script_file = Pathname.new(__FILE__)
db_path = script_file.dirname.parent.realdirpath + 'db'
db_path.exist? or die(1, "There is no data dir: #{db_path}")
Dir.chdir db_path

text_index = {}

index = JSON.load(File.open('index.js', 'r'))

all_versions = 0
index.keys.each do |ver|
  all_versions += 1
  changes = JSON.load(File.open(ver + '.js', 'r'))
  get_words_from(changes).each do |word, index|
    if text_index.include? word
      text_index[word][ver] = index
    else
      text_index[word] = { ver => index }
    end
  end
end

File.open( 'search.idx', 'w' ) do |f|
  f.write JSON.generate( text_index )
end
