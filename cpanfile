# This file is used by cpanm.
# To install dependencies, run >> cpanm --local-lib ~/perl5 --installdeps . <<, or something similar, depending on your setup.
#
# More details:
# https://metacpan.org/pod/cpanfile
# https://metacpan.org/pod/cpanm

requires 'Array::Utils';
requires 'Carp';
requires 'Clone';
requires 'Date::Simple';
requires 'English';
requires 'JSON';
requires 'Mojolicious';
requires 'Mojolicious::Plugin::Number::Commify';

# vim: set ft=perl:
