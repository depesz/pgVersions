( function( $ ) {
    var iFrom;
    var iTo;
    var optto = new Object;
    var version_index = new Object;

    // get_url_parameter lifted (with edits) from:
    // https://stackoverflow.com/questions/19491336/get-url-parameter-jquery-or-how-to-get-query-string-values-in-js
    var get_url_parameter = function (sParam) {
        var search_str = decodeURIComponent(window.location.search.substring(1));
        var search_variables = search_str.split('&');

        for (var i = 0; i < search_variables.length; i++) {
            var parts = search_variables[i].split('=');

            if (parts[0] === sParam) {
                return parts[1] === undefined ? true : parts[1];
            }
        }
    };

    var create_options = function() {
        var versions_count = versions.length;

        for ( var i = 0; i < versions_count; i++ ) {
            var v = versions[i];
            version_index[v] = i;

            var vto = $("<option />", {value: v, text: v});
            optto[v] = vto;
            iTo.append( vto );

            if ( i > 0 ) {
                var vfrom = $("<option />", {value: v, text: v});
                iFrom.append( vfrom );
            }
        }
        var use_versions = pick_versions_from_url();
        iFrom.val( use_versions[0] );
        iTo.val( use_versions[1] );
        update_visibility_from();
    };

    var update_visibility_from = function() {
        var from_version = iFrom.val();
        var to_version = iTo.val();
        var versions_count = versions.length;

        if (version_index[from_version] < version_index[to_version]) {
            iTo.val( versions[0] );
        }

        for ( var i = 0; i < versions_count; i++ ) {
            var v = versions[i];
            if ( version_index[from_version] > version_index[v] ) {
                optto[v].show();
            } else {
                optto[v].hide();
            }
        }
    };

    var pick_versions_from_url = function() {
        var use = [ versions[1], versions[0] ];

        var url_from = get_url_parameter('from');
        var url_to = get_url_parameter('to');
        if (
            (url_from) &&
            (url_from in version_index)
        ) {
            use[0] = url_from;
        }
        if (
            (url_to) &&
            (url_to in version_index)
        ) {
            use[1] = url_to;
        }
        if ( version_index[ use[0] ] < version_index[ use[1] ] ) {
            use.reverse();
        } else if ( version_index[ use[0] ] == version_index[ use[1] ] ) {
            use = [ versions[1], versions[0] ];
        }
        return use;
    };

    var tooltip_hover_in = function(e) {
        var ttsource = $(this);
        var ttdest_id = ttsource.data('tt-id');
        var ttdest = $( '#' + ttdest_id );
        ttdest.fadeIn();
    };
    var tooltip_hover_out = function(e) {
        var ttsource = $(this);
        var ttdest_id = ttsource.data('tt-id');
        var ttdest = $( '#' + ttdest_id );
        ttdest.fadeOut();
    };

    $( document ).ready( function() {
        iFrom = $("#upfrom");
        iTo = $("#upto");

        create_options();
        update_visibility_from();
        iFrom.change(update_visibility_from);

        $('pre').each(function(i, block) {
            hljs.highlightBlock(block);
        });

        $( '.with-tooltip' ).each(function() {
            var ttsource = $(this);
            var ttdest_id = ttsource.data('tt-id');
            var ttdest = $( '#' + ttdest_id );
            var pos = ttsource.position();
            pos['top'] = pos['top'] + ttsource.outerHeight() + 5;
            pos['left'] = pos['left'] + 5;
            ttdest.css( pos );
        });
        $('.with-tooltip').hover( tooltip_hover_in, tooltip_hover_out );
    });

} )( jQuery );
